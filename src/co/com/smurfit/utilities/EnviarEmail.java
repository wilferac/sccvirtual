package co.com.smurfit.utilities;

import java.util.Properties;
import java.util.Vector;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;

public class EnviarEmail {
    private ArchivoPropiedades archivoPropiedades;
	private Properties props;
	private String usuario;
	private String password;
	
	public EnviarEmail()  {
		super();			
	}
	
	/*public String enviar(String remitente, String mensaje, Vector<String> destinatarios) throws Exception{
		configurarConexion();
		
		usuario=archivoPropiedades.getProperty("USER");
		password=archivoPropiedades.getProperty("PASSWORD");
		
		// Preparamos la sesion
        //Session session = Session.getDefaultInstance(props);
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usuario, password);
            }
        });
		
		Address[] destinos = new Address[destinatarios.size()];
        for(int i=0; i<destinos.length; i++){
        	System.out.println(destinatarios.get(i));
        	if(destinatarios.get(i) != null)
        		destinos[i] = new InternetAddress(destinatarios.get(i));
        }
        
        //Construimos el mensaje
        MimeMessage email = new MimeMessage(session);
        email.setFrom(new InternetAddress(archivoPropiedades.getProperty("REMITENTE")));
        email.setSubject(archivoPropiedades.getProperty("SUBJECT"));
        email.addRecipients(Message.RecipientType.TO, destinos);
        
        email.setHeader("Content-Type", "text/html; charset=\"iso-8859-1\"");
        email.setContent(mensaje, "text/html");
        

        // Lo enviamos.
        Transport canal = session.getTransport("smtp");
        
        try{
        	canal.connect(archivoPropiedades.getProperty("USER"), archivoPropiedades.getProperty("PASSWORD"));
        }catch (Exception e) {
			System.out.println("Fallo conexion servidor");
        	e.printStackTrace();
        	return "Fallo la conexi�n con el servidor de correos smtp.";
        	//throw new Exception("Fallo al conexi�n con el servidor smtp.");
		}
        
        try{
        	canal.sendMessage(email, email.getAllRecipients());
        }catch (Exception e) {
        	System.out.println("Fallo envio correo");
        	e.printStackTrace();
        	return "Fallo el enviao del email.";
        	//throw new Exception("Fallo el enviao del email.");
		}
        finally{
	        // Cierre.
	        canal.close();
        }
        
        return null;
	}*/	
	
	public String enviar(String remitente, String mensaje, Vector<String> destinatarios) throws Exception {
		configurarConexion();
		
		usuario=archivoPropiedades.getProperty("USER");
		password=archivoPropiedades.getProperty("PASSWORD");
		
		// Preparamos la sesion
		Session session = Session.getDefaultInstance(props);
		/*Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(usuario, password);
			}
		});*/

		Address[] destinos = new Address[destinatarios.size()];
		for (int i = 0; i < destinos.length; i++) {
			System.out.println(destinatarios.get(i));
			if (destinatarios.get(i) != null)
				destinos[i] = new InternetAddress(destinatarios.get(i));
		}

		// Construimos el mensaje
		MimeMessage email = new MimeMessage(session);
        email.setFrom(new InternetAddress(archivoPropiedades.getProperty("REMITENTE")));
        email.setSubject(archivoPropiedades.getProperty("SUBJECT"));
		email.addRecipients(Message.RecipientType.TO, destinos);

		email.setHeader("Content-Type", "text/html; charset=\"iso-8859-1\"");
		email.setContent(mensaje, "text/html");

		// Lo enviamos.
		Transport canal = session.getTransport("smtp");

		try {
			canal.connect();
		} catch (Exception e) {
			System.out.println("Fallo conexion servidor");
			e.printStackTrace();
			return "Fallo la conexi�n con el servidor de correos smtp.";
			// throw new Exception("Fallo al conexi�n con el servidor smtp.");
		}

		try {
			canal.sendMessage(email, email.getAllRecipients());
		} catch (Exception e) {
			System.out.println("Fallo envio correo");
			e.printStackTrace();
			return "Fallo el enviao del email.";
			// throw new Exception("Fallo el enviao del email.");
		} finally {
			// Cierre.
			canal.close();
		}

		return null;
	}
	
	public String enviar(String remitente, String mensaje, Vector<String> destinatarios, String subject) throws Exception{
		configurarConexion();
		
		usuario=archivoPropiedades.getProperty("USER");
		password=archivoPropiedades.getProperty("PASSWORD");
		Authenticator authenticator = new Authenticator();
		// Preparamos la sesion
        /*Session session = Session.getDefaultInstance(props);*/
		Session session = Session.getInstance(props, authenticator);
           

        Address[] destinos = new Address[destinatarios.size()];
        for(int i=0; i<destinos.length; i++){
        	if(destinatarios.get(i) != null)
        		destinos[i] = new InternetAddress(destinatarios.get(i));
        }
        
        //Construimos el mensaje
        MimeMessage email = new MimeMessage(session);
        email.setFrom(new InternetAddress(archivoPropiedades.getProperty("REMITENTE")));
        email.setSubject(subject);
        email.addRecipients(Message.RecipientType.TO, destinos);
        
        email.setHeader("Content-Type", "text/html; charset=\"iso-8859-1\"");
        email.setContent(mensaje, "text/html");
        

        // Lo enviamos.
        Transport canal = session.getTransport("smtp");
        
        try{
        	canal.connect(archivoPropiedades.getProperty("USER"), archivoPropiedades.getProperty("PASSWORD"));
        }catch (Exception e) {
			System.out.println("Fallo conexion servidor correo");
        	e.printStackTrace();
        	return "Fallo la conexi�n con el servidor de correos smtp.";	
        	//throw new Exception("Fallo al conexi�n con el servidor smtp.");
		}
        
        try{
        	canal.sendMessage(email, email.getAllRecipients());
        }catch (Exception e) {
        	System.out.println("Fallo envio correo");
        	e.printStackTrace();
        	return "Fallo el enviao del email.";
        	//throw new Exception("Fallo el enviao del email.");
		}
        finally{
	        // Cierre.
	        canal.close();
        }
        
        return null;
	}
	
	public void configurarConexion() throws Exception{
		archivoPropiedades = new ArchivoPropiedades();
		
		//Propiedades de la conexi�n
        props = new Properties();
        props.setProperty("mail.smtp.host", archivoPropiedades.getProperty("HOST"));
        props.setProperty("mail.transport.protocol", archivoPropiedades.getProperty("PROTOCOLO"));   
        props.setProperty("mail.smtp.submitter", archivoPropiedades.getProperty("USER"));
        props.setProperty("mail.smtp.port", archivoPropiedades.getProperty("PORT"));
        props.setProperty("mail.smtp.auth", "false");
        //props.setProperty("mail.smtp.starttls.enable", "true");
	}
	
	private class Authenticator extends javax.mail.Authenticator {
		private PasswordAuthentication authentication;

		public Authenticator() throws Exception{
			ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
			String username=archivoPropiedades.getProperty("USER");	
			String password=archivoPropiedades.getProperty("PASSWORD");
			System.out.println("USUARIO ENVIADO:"+username+", PASWORD ENVIADO:"+password);
			
		authentication = new PasswordAuthentication(username, password);
		}

		protected PasswordAuthentication getPasswordAuthentication() {
		return authentication;
		}
		}
}
