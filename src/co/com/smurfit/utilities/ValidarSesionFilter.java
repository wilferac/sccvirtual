package co.com.smurfit.utilities;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ValidarSesionFilter implements Filter {
    private FilterConfig _filterConfig = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        _filterConfig = filterConfig;
    }

    public void destroy() {
        _filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, 
                         FilterChain chain) throws IOException, ServletException { 
        HttpSession session;
        
        if (request instanceof HttpServletRequest ){
            session = ((HttpServletRequest)request).getSession(false);
            //Obtenemos el comando para saber si el de iniciar session o cerrar session
            if ((session !=null && session.getAttribute(Utilities.LLAVE_USUARIO)!=null && !session.isNew()) ){
            	//se eliminan los manageBean de sesion
                /*if(request.getParameter("clearSession") != null && request.getParameter("clearSession").equals("true")){
	            	try {
						session.removeAttribute("bvpDetPedView");
						session.removeAttribute("sapDetPedView");
						session.removeAttribute("tsccvProductosPedidoView");
						//session.removeAttribute("planDeEntregaView");
						//session.removeAttribute("consultaPedidosView");
						session.removeAttribute("estadoDeCuentaView");
					} catch (Exception e) {
						
					}
            	}*/
            	chain.doFilter(request,response);
            }else{
                    ((HttpServletResponse)response).sendRedirect(((HttpServletRequest)request).getContextPath()+"/sesionInvalida.jspx");
            }
        }
        //chain.doFilter(request, response);
                         
                         
    }
}
