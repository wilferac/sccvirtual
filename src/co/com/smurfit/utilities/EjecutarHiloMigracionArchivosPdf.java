package co.com.smurfit.utilities;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EjecutarHiloMigracionArchivosPdf extends HttpServlet{
	
	public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // Se ejecuta el hilo.
        System.out.println("INICIA EL HILO A LAS: "+new Date());
        MigracionArchivosPdf migracion=new MigracionArchivosPdf();
        migracion.start();
        System.out.println("El servlet continua inicializacion");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//response.setContentType(CONTENT_TYPE);
    	//doPost(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
    	// TODO Auto-generated method stub
    	super.doPost(req, resp);
    }
}