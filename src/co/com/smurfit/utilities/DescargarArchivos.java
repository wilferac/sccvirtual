package co.com.smurfit.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;

public class DescargarArchivos extends HttpServlet {

	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, IOException {response.setContentType(CONTENT_TYPE);
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType(CONTENT_TYPE);
    	FileInputStream archivo = null;
    	ServletOutputStream ouputStream = null;
    	
        try {
        	//Objeto para leer parametros del properties
        	ArchivoPropiedades ar= new ArchivoPropiedades();
        	String directorioArchivo = null;
        	
        	//Obtenemos de la session los datos del reporte a descargar
            HttpSession session= request.getSession();
            String nombreArhivo = request.getParameter("nombreArchivo");
            if(nombreArhivo == null || nombreArhivo.equals("")){
            	//si no logra sacar el nombre archivo de la sesion es por que no es un reporte generado por lo cual debe venir como un parametro en el request
            	nombreArhivo= (String)session.getAttribute(Utilities.LLAVE_NOMBRE_ARCHIVO);
            }

            //Obtenemos del request el formato del archivo a descargar
            String formato= request.getParameter("formato");
            
            if(nombreArhivo.indexOf("RC") != -1){
            	//el RC lo asigno en una validacionhecha en el logic carlos arboleda
        		//si el nombreArhivo contiene la clave RC, el archivo a descargar es un reporte de calidad asi que se coloca la RUTA_REPORTE_CALIDAD y se elimina la clave RC
            	directorioArchivo = ar.getProperty("RUTA_REPORTE_CALIDAD");
            	String[] resolverNombre=nombreArhivo.split("-");
            	nombreArhivo=resolverNombre[1];
        		nombreArhivo = nombreArhivo.replace("RC", "");
        		directorioArchivo+=resolverNombre[0]+"\\";
        		formato = "pdf";//formato por defecto
        	}
            else if(formato == null){
                //si el formato viene nulo y el nombre de archivo no conriene el RC es por que el archivo a descargar es un pdf de producto y el formato por defecto es pdf, 
                //por el contrario se obtiene el nombre del archivo
            	directorioArchivo = ar.getProperty("RUTA_PDF");
            	formato = "pdf";//formato por defecto
            } 
            else{//de lo contrario se asume que es un reporte generado por la aplicacion, asi que se coloca la RUTA_REPORTES_GENERADOS que es donde quedan todos los reportes generados con jasper            		
        		directorioArchivo = ar.getProperty("RUTA_REPORTES_GENERADOS");
            }
            
            File destFile = new File(directorioArchivo+nombreArhivo+"."+formato);

            //Se exporta el reporte al usuario
            archivo = new FileInputStream(destFile);

            int longitud;

            longitud = archivo.available();

            byte datos[] = new byte[longitud];
            archivo.read(datos);
            response.setContentType("application/" + formato);
            response.setContentLength(datos.length);
            response.setHeader("Content-Disposition", 
                               "attachment;filename=" + nombreArhivo + "." +formato);
            ouputStream = response.getOutputStream();
            ouputStream.write(datos);
            ouputStream.flush();            
        } catch (FileNotFoundException e) {
            System.out.println("EXCEPCION POR: "+e.getMessage());
        	e.printStackTrace();
        } catch (IOException e) {
        	System.out.println("EXCEPCION POR 2: "+e.getMessage());
        	e.printStackTrace();
        }
        catch(Exception e){
        	System.out.println("EXCEPCION POR 3: "+e.getMessage());
        	e.printStackTrace();
        }
        finally{
        	archivo.close();
        	ouputStream.close();
        }
    }

}
