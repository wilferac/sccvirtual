package co.com.smurfit.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.jboss.util.propertyeditor.BlockingModeEditor;

import com.lowagie.tools.concat_pdf;

import co.com.smurfit.dataaccess.dao.TsccvEmpresasDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;

public class CargarEmpresas {   
	private StringBuffer logErrores;
	
	public String cargar(File archivo) throws BMBaseException, Exception{
		try{
			logErrores = new StringBuffer();
			
			HSSFWorkbook workbook = null;
			ITsccvEmpresasLogic tsccvEmpresasLogic = new TsccvEmpresasLogic();
			TsccvEmpresas tsccvEmpresas = null;
			boolean insertar = true;
			StringBuffer causas = null;
			int numCausa = 1;
			try{
				//se carga el documento
				POIFSFileSystem poifsFileSystem = new POIFSFileSystem(new FileInputStream(archivo));
				
				//se crear un workbook que representa todo el documento XLS
				workbook = new HSSFWorkbook(poifsFileSystem);
			}catch (Exception e) {
				throw new Exception("Error al cargar el archivo: "+archivo.getName()+" ");
			}
			
			//for(int i=0; i<workbook.getNumberOfSheets()-2; i++){//ciclo para recorrer las hojas del documento
				//Creamos un objeto sheet con la hoja del documento a leer. 
				//se recuperar la hoja por el indice
				HSSFSheet sheet = workbook.getSheetAt(0);
				HSSFRow row = null;
			  
				for(int j=0; j<=sheet.getLastRowNum(); j++) {//ciclo para recorrer cada fila de la hoja
					row = sheet.getRow(j);
					insertar = true;
					numCausa = 1;
					
					if(row != null) {
						//System.out.print("\n" + j + "\t");
						tsccvEmpresas = new TsccvEmpresas();
						
						tsccvEmpresas.setNombre(obtenerValorCelda(row.getCell((short)0)).toUpperCase());
						tsccvEmpresas.setCodSap(obtenerValorCelda(row.getCell((short)1)));
						tsccvEmpresas.setCodMas(obtenerValorCelda(row.getCell((short)2)));
						tsccvEmpresas.setCodVpe(obtenerValorCelda(row.getCell((short)3)));
						tsccvEmpresas.setActivo(obtenerValorCelda(row.getCell((short)4)));
						tsccvEmpresas.setFechaCreacion(new Date());
						
						causas = new StringBuffer("Nombre Empresa: "+tsccvEmpresas.getNombre()+"\n");
						
						if (tsccvEmpresas.getCodSap() != null) {
		            		List resultado = JPADaoFactory.getInstance().getTsccvEmpresasDAO().findByCodSap(tsccvEmpresas.getCodSap());
		            		if(resultado != null && resultado.size() > 0){
		            			insertar = false;
		            			causas.append((numCausa++)+". El C�digo SAP ya est� registrado en el sistema\n");
		            			//throw new Exception("El C�digo SAP ya est� registrado en el sistema");
		            		}
		            	}
						else{
							causas.append((numCausa++)+". El C�digo SAP es obligatorio\n");
							insertar = false;
						}
		            	
						//valida campos obligatorios
		            	if((tsccvEmpresas.getCodMas() == null && tsccvEmpresas.getCodVpe() == null) || 
		            			tsccvEmpresas.getNombre() == null || tsccvEmpresas.getActivo() == null){
		            		insertar = false;
		            		causas.append((numCausa++)+". Campo C�digo MAS O VPE sin valor\n");
		            		//throw new BMBaseException(36, null);
		            	}
		            	/*else if(tsccvEmpresas.getCodMas() != null && tsccvEmpresas.getCodVpe() != null){
		            		insertar = false;
		            		causas.append((numCausa++)+". No se puede ingresar el C�digo MAS y el C�digo VPE simultaneamente\n");
		            		//throw new BMBaseException(38, null);
		            	}*/
		            	else{
		            		if(insertar && tsccvEmpresas.getCodMas() != null){
		            			//tsccvEmpresas.setCodVpe(null);
		            			if(BusinessDelegatorView.empresaIsRegistrada(tsccvEmpresas.getNombre(), tsccvEmpresas.getCodSap(), tsccvEmpresas.getCodMas(), tsccvEmpresas.getCodVpe())){
		            				insertar = false;
		            				causas.append((numCausa++)+". El Nombre o la combinaci�n C�digo SAP y C�digo MAS ya est� registrada en el sistema\n");
		            				//throw new Exception("El Nombre o la combinacion C�digo SAP y C�digo MAS ya est� registrada en el sistema");
		            			}
		                	}
		            		if(insertar && tsccvEmpresas.getCodVpe() != null){
		            			//tsccvEmpresas.setCodMas(null);
		            			if(BusinessDelegatorView.empresaIsRegistrada(tsccvEmpresas.getNombre(), tsccvEmpresas.getCodSap(), tsccvEmpresas.getCodMas(), tsccvEmpresas.getCodVpe())){
		            				insertar = false;
		            				causas.append((numCausa++)+". El Nombre o la combinaci�n C�digo SAP y C�digo VPE ya est� registrada en el sistema\n");
		            				//throw new Exception("El Nombre o la combinacion C�digo SAP y C�digo VPE ya est� registrada en el sistema");
		            			}
		            		}
		            	}
						
		            	if(insertar){
		            		tsccvEmpresasLogic.saveTsccvEmpresas(tsccvEmpresas.getActivo(), tsccvEmpresas.getCodMas(), tsccvEmpresas.getCodSap(),
	            								tsccvEmpresas.getCodVpe(), tsccvEmpresas.getFechaCreacion(), null, tsccvEmpresas.getNombre(), null, null);
		            	}
		            	else{
		            		logErrores.append(causas.toString()+"\n------------------------------------------------------------");
		            	}
					}
				}
			//}
			
			return logErrores.toString();
		}catch(Exception e) {
			throw e;
		}
	}
	
	@SuppressWarnings("deprecation")
	public String obtenerValorCelda(HSSFCell cell){
		String valor = "";
		DecimalFormat df = new DecimalFormat("#");
		
		if(cell != null){
			//se valida el tipo de dato de la columna
			if(HSSFCell.CELL_TYPE_STRING == cell.getCellType()){
				valor = cell.getStringCellValue();
			}
			else if(HSSFCell.CELL_TYPE_NUMERIC == cell.getCellType()){
				valor = String.valueOf(df.format(cell.getNumericCellValue()));
			}
		}
		else{
			valor = null;
		}
		
		return valor;
	}
}







