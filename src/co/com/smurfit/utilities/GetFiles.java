package co.com.smurfit.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;

/**
 * Servlet implementation class GetFiles
 */
public class GetFiles extends HttpServlet {
	private static final long serialVersionUID = 1L;
	   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetFiles() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		FileInputStream archivo = null;
    	ServletOutputStream ouputStream = null;
    	
        try {
        	//Objeto para leer parametros del properties
        	ArchivoPropiedades ar= new ArchivoPropiedades();
        	String directorioArchivo = null;
        	
        	//Obtenemos de la session los datos del reporte a descargar
            String nombreArhivo = request.getParameter("file");
            String type = request.getParameter("type");
            if(type.equals("1")){
            	directorioArchivo = ar.getProperty("RUTA_PDF");
            }
            else if(type.equals("2")){
            	directorioArchivo = ar.getProperty("RUTA_REPORTE_CALIDAD");
            }
            else if(type.equals("3")){
            	directorioArchivo = ar.getProperty("RUTA_REPORTES_GENERADOS");
            }
            
            //Obtenemos el formato con la extension del archivo
            String[] fileName=nombreArhivo.split("\\.");
            String formato=fileName[fileName.length-1];
            
            System.out.println(request.getContextPath());
           
            File destFile = new File(directorioArchivo+nombreArhivo);

            //Se exporta el reporte al usuario
            archivo = new FileInputStream(destFile);

            int longitud;

            longitud = archivo.available();

            byte datos[] = new byte[longitud];
            archivo.read(datos);
            response.setContentType("application/"+formato);
            response.setContentLength(datos.length);
            response.setHeader("Content-Disposition", 
                               "attachment;filename=" + nombreArhivo);
            ouputStream = response.getOutputStream();
            ouputStream.write(datos);
            ouputStream.flush();            
        }
        catch(Exception e){
        	System.out.println("EXCEPCION POR 3: "+e.getMessage());
        	e.printStackTrace();
        }
        finally{
        	archivo.close();
        	ouputStream.close();
        }

	}

}
