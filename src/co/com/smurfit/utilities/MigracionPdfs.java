package co.com.smurfit.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.Vector;

import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.ConstruirEmailConfirmacionOrden;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import com.icesoft.faces.component.ext.HtmlOutputText;

public class MigracionPdfs {
	private static String codPlantaAcual;
	private static String nombreDirectorioPlantaAcual;
	private static final String destino = "D:/PDFs/";//directorio destino de los pdfs
	//private static final String destino = "C:/PDFS/";//directorio destino de los pdfs
	private HtmlOutputText texto;
	private int migrados=0;
	
	public MigracionPdfs(){
		/*try{
			//Se envia el correo de confirmacion al usuario
			EnviarEmail email= new EnviarEmail();
			ConstruirEmailConfirmacionOrden enviarEmail= new ConstruirEmailConfirmacionOrden();
			enviarEmail.textoMensajeConfirmacionUsuarioHV("Jhon Andrey"+ " "+"Loaiza Tasc�n");
			String mensaje= enviarEmail.getMensajeHV();
			Vector emailUsario= new Vector();
			emailUsario.add("jhonandrey@hotmail.com");
			email.enviar("jhonandrey@gmail.com",mensaje,emailUsario,"Confirmaci�n Envio Hoja de Vida Smurfit Kappa Cart�n de Colombia");
		}catch(Exception e){
			System.out.println("Excepcion");
			
		}*/
	}
	
	public String migrar(){
		
		try{
			ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades(); 			
			File dirPdfs = new File("D:/Apache Software Foundation/Apache2.2/htdocs/PDF");//directorio anterior de los pdfs
			int indice=1;
			
			if(dirPdfs.canRead()){
				//System.out.print("|-"+dirPdfs.getName()+"\n");
				
				File dirs[] = dirPdfs.listFiles();
				for(int i=0; i<dirs.length; i++){
					//System.out.print("  |-"+dirs[i].getName()+"\n");
					if(dirs[i].getName().equals("reportes_calidad") || dirs[i].getName().equals("Palmira-borrar")){
						continue;
					}
					
					if(dirs[i].getName().indexOf("sacos") == -1){
						codPlantaAcual = getCodigoPlanta(dirs[i].getName(), true);//archivoPropiedades.getProperty("PRODUCTO_CORRUGADO");
						nombreDirectorioPlantaAcual = "CORRUGADO "+dirs[i].getName().toUpperCase()+"/";
					}
					else{
						codPlantaAcual = getCodigoPlanta(dirs[i].getName(), false);//archivoPropiedades.getProperty("PRODUCTO_SACOS");
						nombreDirectorioPlantaAcual = "SACOS "+dirs[i].getName().replace("-sacos", "").toUpperCase()+"/";
					}
					
					File archivos[] = dirs[i].listFiles();
					for(int j=0; j<archivos.length; j++){
						//se valida que el archivo pueda ser leido y que el nombre contenga solo numeros
						if(archivos[j].canRead() && (archivos[j].getName().replace(".pdf", "").matches("[0-9]*") || archivos[j].getName().replace(".PDF", "").matches("[0-9]*"))){
							//System.out.print("   |-"+archivos[j].getName()+"\n");
							//se transfiere el archivo
							transferirArchivo(archivos[j]);
							migrados++;
							//se guarda el registro en la bd
							try {
								if(archivos[j].getName().indexOf(".PDF") != -1){
									BusinessDelegatorView.saveTsccvPdfsProductosSinArchivo(archivos[j].getName().replace(".PDF", ""), codPlantaAcual, new Date(), codPlantaAcual);
								}
								else{
									BusinessDelegatorView.saveTsccvPdfsProductosSinArchivo(archivos[j].getName().replace(".pdf", ""), codPlantaAcual, new Date(), codPlantaAcual);
									
								}
							} catch (Exception e) {
								System.out.println("ERROR GUARDANDO PDF. PLANTA: "+codPlantaAcual+", ARCHIVO: "+archivos[j].getName()+", ERROR NUMERO: "+indice);
								indice++;
								e.printStackTrace();
							}
						}
					}
				}
			}
			texto.setValue("Se han migrado "+migrados+" pdfs de forma satisfactoria");
			texto.setRendered(true);
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void main(String args[]){
		try{
			ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades(); 			
			File dirPdfs = new File("D:/Apache Software Foundation/Apache2.2/htdocs/PDF");//directorio anterior de los pdfs
			
			if(dirPdfs.canRead()){
				//System.out.print("|-"+dirPdfs.getName()+"\n");
				
				File dirs[] = dirPdfs.listFiles();
				for(int i=0; i<dirs.length; i++){
					//System.out.print("  |-"+dirs[i].getName()+"\n");
					if(dirs[1].getName().equals("reportes_calidad") || dirs[1].getName().equals("Palmira-borrar")){
						continue;
					}
					
					if(dirs[i].getName().indexOf("sacos") == -1){
						codPlantaAcual = archivoPropiedades.getProperty("PRODUCTO_CORRUGADO");
						nombreDirectorioPlantaAcual = "CORRUGADO "+dirs[i].getName().toUpperCase()+"/";
					}
					else{
						codPlantaAcual = archivoPropiedades.getProperty("PRODUCTO_SACOS");
						nombreDirectorioPlantaAcual = "SACOS "+dirs[i].getName().replace("-sacos", "").toUpperCase()+"/";
					}
					
					File archivos[] = dirs[i].listFiles();
					for(int j=0; j<archivos.length; j++){
						//se valida que el archivo pueda ser leido y que el nombre contenga solo numeros
						if(archivos[j].canRead() && archivos[j].getName().replace(".pdf", "").matches("[0-9]*")){
							//System.out.print("   |-"+archivos[j].getName()+"\n");
							//se transfiere el archivo
							transferirArchivo(archivos[j]);
							
							//se guarda el registro en la bd
							BusinessDelegatorView.saveTsccvPdfsProductosSinArchivo(archivos[j].getName().replace(".pdf", ""), codPlantaAcual, new Date(), codPlantaAcual);
						}
					}
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	//metodo encargado de transferir los archivos
	public static void transferirArchivo(File fuente) {
		FileInputStream fis = null;
        FileOutputStream fos = null;
        
		try{
			//si no existe se crea el directorio
			new File(destino+nombreDirectorioPlantaAcual).mkdir();
			
			fis = new FileInputStream(fuente);
	        fos = new FileOutputStream(destino+nombreDirectorioPlantaAcual+codPlantaAcual+"-"+fuente.getName());
	        
	        //se abren los canales
			FileChannel canalFuente = fis.getChannel();
	        FileChannel canalDestino = fos.getChannel();
	        
	        //se transfiere el archivo
	        canalFuente.transferTo(0, canalFuente.size(), canalDestino);
		}catch(IOException e){
			e.printStackTrace();
		}
        finally {
        	try{
		        fis.close();
		        fos.close();
        	}catch(IOException e){
    			e.printStackTrace();
    		}
        }
	}
	
	public HtmlOutputText getTexto() {
		return texto;
	}

	public void setTexto(HtmlOutputText texto) {
		this.texto = texto;
	}
	
	public String getCodigoPlanta(String nombreCarpeta, boolean isCorrugado){
		if(isCorrugado){
			if(nombreCarpeta.toUpperCase().lastIndexOf("CALI") != -1){
				return "3";
				
			}
			else if(nombreCarpeta.toUpperCase().lastIndexOf("MEDELLIN") != -1){
				
					return "4";
			}
			else if(nombreCarpeta.toUpperCase().lastIndexOf("BOGOTA") != -1){
				
				return "2";
			}
			else if(nombreCarpeta.toUpperCase().lastIndexOf("BARRANQUILLA") != -1){
				
				return "1";
			}
		}
		else{
			if(nombreCarpeta.toUpperCase().lastIndexOf("CALI") != -1){
				return "5";
				
			}
			else if(nombreCarpeta.toUpperCase().lastIndexOf("PALMIRA") != -1){
				
					return "6";
			}
		}
		
		return null;
	} 
}






