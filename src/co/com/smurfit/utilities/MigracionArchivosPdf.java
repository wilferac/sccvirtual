package co.com.smurfit.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;

public class MigracionArchivosPdf extends Thread{
	
	private static String codPlantaAcual;
	private static String nombreDirectorioPlantaAcual;
	private static String destino = "D:/PDFs/";//directorio destino de los pdfs
	private static String directorioCarguePdfs="";
	private ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	private File dirPdfs;
	/*
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		
		dirPdfs=new File("D:/Apache Software Foundation/Apache2.2/htdocs/PDF"); //directorio anterior de los pdfs
		// JRUIZ: Se obtiene el tiempo en el cual se debe ejecutar el hilo.
		SimpleDateFormat formato=new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Calendar calendar=Calendar.getInstance();
		Date fechaEjecucionHilo=null;
		long timeSleepThread=0;
		int dia=0;
		int mes=1;
		
		try{
			while(true){
				dia=0;
				Date fechaActual=formato.parse(formato.format(Calendar.getInstance().getTime()));
				String []horas=archivoPropiedades.getProperty("TIEMPO_EJECUCION_HILO").split(",");
				
				// Se valida si la ultima hora de ejecucion del hilo es superior a la hora actual, de lo contrario se tendra en cuenta la primer hora del dia siguiente.
				fechaEjecucionHilo=formato.parse(calendar.get(Calendar.DATE)+"/"+(calendar.get(Calendar.MONTH)+mes)+"/"+calendar.get(Calendar.YEAR)+" "+(horas[(horas.length-1)].toString()));
				if(fechaEjecucionHilo.getTime()<fechaActual.getTime()){
					dia=1;
				}
				
				// Se recorre las horas a ejecutar el hilo y se valida cual es la proxima hora.
				for(int a=0; a<horas.length; a++){
					fechaEjecucionHilo=formato.parse((calendar.get(Calendar.DATE)+dia)+"/"+(calendar.get(Calendar.MONTH)+mes)+"/"+calendar.get(Calendar.YEAR)+" "+(horas[a].toString()));
					
					// Se valida si la fecha de ejecucion del hilo es mayor a la fecha actual, para determinar el tiempo que debe dormir el hilo.
					if(fechaEjecucionHilo.getTime()>fechaActual.getTime()){
						timeSleepThread=(fechaEjecucionHilo.getTime()-fechaActual.getTime());
						System.out.println("EL HILO DUERME DESDE: "+new Date()+" , HASTA: "+timeSleepThread);
						sleep(timeSleepThread);
						break;
					}
				}
				//migracionArchivos();
				System.out.println("SE EJECUTA: "+new Date());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	*/
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		
		//Obtenmos las ruta
		try {
			ArchivoPropiedades prop=new ArchivoPropiedades();
			destino=prop.getProperty("RUTA_PDF");
			directorioCarguePdfs=prop.getProperty("RUTA_CARGUE_PDFS_PROCESO");
		} catch (Exception e1) {
			// TODO Auto-generated cSatch block
			e1.printStackTrace();
		}
		//System.out.println("variables hilo"+destino+"-"+directorioCarguePdfs);
		
		dirPdfs=new File(directorioCarguePdfs); //directorio anterior de los pdfs
		// JRUIZ: Se obtiene el tiempo en el cual se debe ejecutar el hilo.
		SimpleDateFormat formato=new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Calendar calendar=Calendar.getInstance();
		Date fechaEjecucionHilo=null;
		long timeSleepThread=0;
		int dia=0;
		int mes=1;
		boolean primeraEjecucion=true;
		String horaEjecucion=null;
		try{
			while(true){
				if(primeraEjecucion){
					primeraEjecucion=false;
					dia=0;
					Date fechaActual=formato.parse(formato.format(Calendar.getInstance().getTime()));
					String []horas=archivoPropiedades.getProperty("TIEMPO_EJECUCION_HILO").split(",");
					
					// Se valida si la ultima hora de ejecucion del hilo es superior a la hora actual, de lo contrario se tendra en cuenta la primer hora del dia siguiente.
					fechaEjecucionHilo=formato.parse(calendar.get(Calendar.DATE)+"/"+(calendar.get(Calendar.MONTH)+mes)+"/"+calendar.get(Calendar.YEAR)+" "+(horas[(horas.length-1)].toString()));
					if(fechaEjecucionHilo.getTime()<fechaActual.getTime()){
						dia=1;
					}
					
					// Se recorre las horas a ejecutar el hilo y se valida cual es la proxima hora.
					for(int a=0; a<horas.length; a++){
						fechaEjecucionHilo=formato.parse((calendar.get(Calendar.DATE)+dia)+"/"+(calendar.get(Calendar.MONTH)+mes)+"/"+calendar.get(Calendar.YEAR)+" "+(horas[a].toString()));
						
						// Se valida si la fecha de ejecucion del hilo es mayor a la fecha actual, para determinar el tiempo que debe dormir el hilo.
						if(fechaEjecucionHilo.getTime()>fechaActual.getTime()){
							timeSleepThread=(fechaEjecucionHilo.getTime()-fechaActual.getTime());
							System.out.println("EL HILO DUERME DESDE: "+new Date()+" , HASTA: "+timeSleepThread);
							sleep(timeSleepThread);
							break;
						}
					}
				}
				else{
					Calendar ahora= Calendar.getInstance();
					//Date fechaActual=formato.parse(formato.format(Calendar.getInstance().getTime()));
					String []horas=archivoPropiedades.getProperty("TIEMPO_EJECUCION_HILO").split(",");
					String []tiemposDuerme=archivoPropiedades.getProperty("TIEMPO_DUERME_HILO").split(",");
					
					// Se recorre las horas a ejecutar el hilo y se valida cual es la proxima hora.
					for(int a=0; a<horas.length; a++){
						String horaDescompuesta[]= horas[a].split(":");
						horaEjecucion=horaDescompuesta[0];
						//Validamos con cual hora corresponde la hora actual
						if(ahora.get(Calendar.HOUR) == Integer.parseInt(horaEjecucion)){
							timeSleepThread=Long.parseLong(tiemposDuerme[a]);
							System.out.println("EL HILO DUERME DESDE: "+new Date()+" , HASTA: "+timeSleepThread);
							sleep(timeSleepThread);
							break;
						}
						else  if(a == horas.length -1){
							if((ahora.get(Calendar.HOUR) -1) == Integer.parseInt(horaEjecucion)){
								timeSleepThread=Long.parseLong(tiemposDuerme[a]);
								System.out.println("EL HILO DUERME DESDE: "+new Date()+" , HASTA: "+timeSleepThread);
								sleep(timeSleepThread);
								break;
							}
							else{
								//Dormir de seguridad
								System.out.println("SE EJECUTA EL DORMIR DE SEGURIDAD");
								sleep(1800000);
								break;
							}
						}
					}
				}
				migracionArchivos();
				System.out.println("SE EJECUTA: "+new Date());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void migracionArchivos() {
		System.out.println("SE EJECUTA EL HILO: "+new Date());
		try{
			if(dirPdfs.canRead()){
				//System.out.print("|-"+dirPdfs.getName()+"\n");
				File dirs[] = dirPdfs.listFiles();
				for(int i=0; i<dirs.length; i++){
					//System.out.print("  |-"+dirs[i].getName()+"\n");
					if(dirs[i].getName().equals("reportes_calidad") || dirs[i].getName().equals("Palmira-borrar")){
						continue;
					}
					
					if(dirs[i].getName().indexOf("sacos") == -1){
						codPlantaAcual = getCodigoPlanta(dirs[i].getName(), true);//archivoPropiedades.getProperty("PRODUCTO_CORRUGADO");
						nombreDirectorioPlantaAcual = "CORRUGADO "+dirs[i].getName().toUpperCase()+"/";
					}
					else{
						codPlantaAcual = getCodigoPlanta(dirs[i].getName(), false);//archivoPropiedades.getProperty("PRODUCTO_SACOS");
						nombreDirectorioPlantaAcual = "SACOS "+dirs[i].getName().replace("-sacos", "").toUpperCase()+"/";
					}
					
					File archivos[] = dirs[i].listFiles();
					for(int j=0; j<archivos.length; j++){
						//se valida que el archivo pueda ser leido y que el nombre contenga solo numeros
						if(archivos[j].canRead() && (archivos[j].getName().replace(".pdf", "").matches("[0-9]*") || archivos[j].getName().replace(".PDF", "").matches("[0-9]*"))){
							//System.out.print("   |-"+archivos[j].getName()+"\n");
							//se transfiere el archivo
							transferirArchivo(archivos[j]);
							//se guarda el registro en la bd
							try {
								if(archivos[j].getName().indexOf(".PDF") != -1){
									BusinessDelegatorView.saveTsccvPdfsProductosSinArchivo(archivos[j].getName().replace(".PDF", ""), codPlantaAcual, new Date(), codPlantaAcual);
								}
								else{
									BusinessDelegatorView.saveTsccvPdfsProductosSinArchivo(archivos[j].getName().replace(".pdf", ""), codPlantaAcual, new Date(), codPlantaAcual);
									
								}
							} catch (Exception e) {
								System.out.println("ERROR GUARDANDO PDF. PLANTA: "+codPlantaAcual+", ARCHIVO: "+archivos[j].getName());
								e.printStackTrace();
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//metodo encargado de transferir los archivos
	public static void transferirArchivo(File fuente) {
		FileInputStream fis = null;
        FileOutputStream fos = null;
        
		try{
			//si no existe se crea el directorio
			new File(destino+nombreDirectorioPlantaAcual).mkdir();
			
			fis = new FileInputStream(fuente);
	        fos = new FileOutputStream(destino+nombreDirectorioPlantaAcual+codPlantaAcual+"-"+fuente.getName());
	        
	        //se abren los canales
			FileChannel canalFuente = fis.getChannel();
	        FileChannel canalDestino = fos.getChannel();
	        
	        //se transfiere el archivo
	        canalFuente.transferTo(0, canalFuente.size(), canalDestino);
		}catch(IOException e){
			e.printStackTrace();
		}
        finally {
        	try{
		        fis.close();
		        fos.close();
		        
		        // Se elimina el archivo transferido.
		        if(fuente.isFile() && fuente.delete()){
		        	System.out.print("El archivo ["+fuente.getName()+"] se elimino de forma correcta.");
		        }
		        
        	}catch(IOException e){
    			e.printStackTrace();
    		}
        }
	}
	
	public String getCodigoPlanta(String nombreCarpeta, boolean isCorrugado){
		if(isCorrugado){
			if(nombreCarpeta.toUpperCase().lastIndexOf("CALI") != -1){
				return "3";
				
			}
			else if(nombreCarpeta.toUpperCase().lastIndexOf("MEDELLIN") != -1){
				
					return "4";
			}
			else if(nombreCarpeta.toUpperCase().lastIndexOf("BOGOTA") != -1){
				
				return "2";
			}
			else if(nombreCarpeta.toUpperCase().lastIndexOf("BARRANQUILLA") != -1){
				
				return "1";
			}
		}
		else{
			if(nombreCarpeta.toUpperCase().lastIndexOf("CALI") != -1){
				return "5";
				
			}
			else if(nombreCarpeta.toUpperCase().lastIndexOf("PALMIRA") != -1){
				
					return "6";
			}
		}
		
		return null;
	} 
	
	public static void main(String[] args) {
		MigracionArchivosPdf migracion=new MigracionArchivosPdf();
        migracion.start();
	}
}