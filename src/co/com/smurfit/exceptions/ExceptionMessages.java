package  co.com.smurfit.exceptions;

public class ExceptionMessages {
	public static String VARIABLE_NULL = "Campo obligatorio sin valor: ";
	public static String VARIABLE_LENGTH = "Formato o longitud no valido para la variable: ";
	public static String ENTITY_NULL = "Entidad Nula";
	public static String ENTITY_WITHSAMEKEY = "Ya existe otro registro con el mismo Id";
	public static String ENTITY_NOENTITYTOUPDATE = "No se encontro registro para actualizar";
	public static String ENTITY_SUCCESFULLYSAVED = "Registro Almacenado de forma Satisfatoria";
	public static String ENTITY_SUCCESFULLYDELETED = "Registro Eliminado de forma Satisfatoria";
	public static String ENTITY_SUCCESFULLYMODIFIED = "Registro Modificado de forma Satisfatoria";

}