package co.com.smurfit.exceptions;


import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.HashMap;

import javax.servlet.http.HttpServlet;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;

public class BMBaseException extends Exception {
    
    private int codigoError = 0;
    private String descripcion = null;
    private String correccion = null;
    private String dato = null;
    
    private co.com.smurfit.sccvirtual.entidades.TbmBaseErrores errores;
    
    private static Logger log4j = Logger.getLogger(BMBaseException.class);
    
    public BMBaseException() {
    }
    
    //Constructor para las Excepciones controladas
    public BMBaseException(int codigoError, String dato){
        try{
        	errores = JPADaoFactory.getInstance().getTbmBaseErroresDAO().findById(new Long (codigoError));
        }catch(Exception e){
        }
        
        if(errores == null ){
            descripcion="Codigo de error "+codigoError+" no existe en base de datos.";
            correccion="Cree el codigo de error en la base de datos.";
        }else{
            if(errores.getDescripcion() != null){
                descripcion= errores.getDescripcion();
            }
            
            if(errores.getSolucion() != null){
                correccion = errores.getSolucion();
            }
        }
        
        this.dato = dato;
        this.codigoError = codigoError;
    }
    
    
    
    
    //Constructor para las Excepciones no controladas
    public BMBaseException(Exception e){
        //TODO IMPLEMENTAR LOG4J
    	//Manejo del archivo donde se almacenaran las excepciones no controladas
        /*BasicConfigurator.configure();
        FileAppender appender = null;
        SimpleLayout layout = new SimpleLayout();
        
        String rutalog = "F:\\BM SOFTWARE\\DEVELOPMENT\\Smurfit\\SCCVirtual\\Tecnico\\Fuentes\\sccvirtual\\WebRoot\\WEB-INF\\Log0.txt";
        String nombreArchivoLog = new String();
        String extension = ".txt";        
        
        File archivo01 = new File(rutalog);
        System.out.println(archivo01.isFile());
        
        try {
            for(int a=0; a<50000; a++){
                nombreArchivoLog = new String("Log"+a);
                
                //Se obtiene el archivo para determinar si el tama�o es superioro al especificado.
                File archivo = new File(rutalog+nombreArchivoLog+extension);
                if(archivo.isFile()){
                    if(archivo.length() > 30000){
                        continue;
                    }else{
                        break;
                    }
                }else{
                    break;
                }
            }
            appender= new FileAppender(layout,"", true);
        } catch (Exception f) {
            // TODO
            System.out.println(f.getMessage());
        }
        
        log4j.addAppender(appender);
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        String stacktrace = sw.toString();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy");
        log4j.error("----------------------------------EXCEPCION GENERADA EL:"+formatoFecha.format(new Date())+"--------------------------------------------\r\n"+stacktrace+"------------------------------------------------FIN DE LA EXCEPCION---------------------------------------------------\r\n\r\n");
        //Hasta aqui
        
        /*descripcion="Ha ocurrido un error inesperado en la aplicaci�n.";
        correccion="Por favor informe al departamento de sistemas indicando el siguiente mensaje: "+e.getMessage();*/
    	correccion=e.getMessage();
    }
    
    
    
    
    
    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public int getCodigoError() {
        return codigoError;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setCorreccion(String correccion) {
        this.correccion = correccion;
    }

    public String getCorreccion() {
        return correccion;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public String getDato() {
        return dato;
    }
}