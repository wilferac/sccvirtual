package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TbmBaseSubopcionesOpcDAO;
import co.com.smurfit.dataaccess.dao.TsccvPlantasUsuarioDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TbmBaseSubopcionesOpcDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TbmBaseSubopcionesOpcView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtIdOpci_TbmBaseOpciones;
    private HtmlInputText txtIdSuop_TbmBaseSubopciones;
    private HtmlInputText txtIdOpci;
    private HtmlInputText txtIdSuop;
    private HtmlInputText txtNombreVisible;
    private SelectInputDate txtFechaCreacion;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TbmBaseSubopcionesOpc> tbmBaseSubopcionesOpc;
    private List<TbmBaseSubopcionesOpcDTO> tbmBaseSubopcionesOpcDTO;

    //listas desplegables
    private List<TbmBaseSubopcionesOpc> listadoSubOpcionesOpci;
    private HtmlSelectOneMenu somOpcion;
    private List<SelectItem> siOpcion;   
    private HtmlSelectOneMenu somSubOpcion;
    private List<SelectItem> siSubOpcion;
    
    public TbmBaseSubopcionesOpcView() {
        super("");

        //Se crea un objeto del bundle
        ArchivoBundle archivoBundle = new ArchivoBundle();
        
        try{
			//se crea el primer item de las listas
        	SelectItem siNulo = new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
        	
			//se inicializan los componentes de las listas
        	txtIdOpci_TbmBaseOpciones = new HtmlInputText();
            txtIdSuop_TbmBaseSubopciones = new HtmlInputText();         
            siOpcion = new ArrayList<SelectItem>();
            siSubOpcion = new ArrayList<SelectItem>();
            
			//se crear los listados       
            List<TbmBaseOpciones> listadoOpciones = null;
            List<TbmBaseSubopciones> listadoSubopciones = null;
            listadoSubOpcionesOpci = new ArrayList<TbmBaseSubopcionesOpc>();
        	
			//se cargan los valores de las listas
            listadoOpciones = BusinessDelegatorView.getTbmBaseOpciones();
            listadoSubopciones = BusinessDelegatorView.getTbmBaseSubopciones();
            
        	//se carga lista de opciones
            siOpcion.add(siNulo);
        	for(TbmBaseOpciones objetoOpcion: listadoOpciones){
        		if(objetoOpcion.getActivo().equals("1")){
        			continue;
        		}
        		
        		SelectItem siOpci = new SelectItem();        		
        		siOpci.setLabel(objetoOpcion.getNombre());
        		siOpci.setValue(objetoOpcion.getIdOpci());
        		
        		siOpcion.add(siOpci);
        	}
        	
        	//se carga lista de subOpciones
        	siSubOpcion.add(siNulo);
        	for(TbmBaseSubopciones objetoSubOpcion : listadoSubopciones){
        		//se valida is la subOpcion esta inactiva para no agregarla a la lista
        		if(objetoSubOpcion.getActivo().equals("1")){
        			continue;
        		}
        		SelectItem siSubOpci = new SelectItem();        		
        		siSubOpci.setLabel(objetoSubOpcion.getNombre());
        		siSubOpci.setValue(objetoSubOpcion.getIdSuop());
        		
        		siSubOpcion.add(siSubOpci);
        	}
        }catch(Exception e){
        	FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
        }
    }

    public String action_clear() {
        txtIdOpci_TbmBaseOpciones.setValue(null);
        txtIdOpci_TbmBaseOpciones.setDisabled(false);
        txtIdOpci.setValue(null);
        txtIdOpci.setDisabled(false);
        somOpcion.setValue(new Long(-1));
        
        txtIdSuop_TbmBaseSubopciones.setValue(null);
        txtIdSuop_TbmBaseSubopciones.setDisabled(false);
        txtIdSuop.setValue(null);
        txtIdSuop.setDisabled(false);
        somSubOpcion.setValue(new Long(-1));
                
        txtNombreVisible.setValue(null);
        txtNombreVisible.setDisabled(false);

        txtFechaCreacion.setValue(null);
        txtFechaCreacion.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);
        
        listadoSubOpcionesOpci.clear();

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TbmBaseSubopcionesOpc entity = null;

            try {
                TbmBaseSubopcionesOpcId id = new TbmBaseSubopcionesOpcId();
                id.setIdOpci((((txtIdOpci.getValue()) == null) ||
                    (txtIdOpci.getValue()).equals("")) ? null
                                                       : new Long(
                        txtIdOpci.getValue().toString()));
                id.setIdSuop((((txtIdSuop.getValue()) == null) ||
                    (txtIdSuop.getValue()).equals("")) ? null
                                                       : new Long(
                        txtIdSuop.getValue().toString()));
                id.setNombreVisible((((txtNombreVisible.getValue()) == null) ||
                    (txtNombreVisible.getValue()).equals("")) ? null
                                                              : new String(
                        txtNombreVisible.getValue().toString()));
                id.setFechaCreacion((((txtFechaCreacion.getValue()) == null) ||
                    (txtFechaCreacion.getValue()).equals("")) ? null
                                                              : (Date) txtFechaCreacion.getValue());

                entity = BusinessDelegatorView.getTbmBaseSubopcionesOpc(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtIdOpci_TbmBaseOpciones.setDisabled(false);
                txtIdSuop_TbmBaseSubopciones.setDisabled(false);

                txtIdOpci.setDisabled(false);
                txtIdSuop.setDisabled(false);
                txtNombreVisible.setDisabled(false);

                txtFechaCreacion.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtIdOpci_TbmBaseOpciones.setValue(entity.getTbmBaseOpciones()
                                                         .getIdOpci());
                txtIdOpci_TbmBaseOpciones.setDisabled(false);
                txtIdSuop_TbmBaseSubopciones.setValue(entity.getTbmBaseSubopciones()
                                                            .getIdSuop());
                txtIdSuop_TbmBaseSubopciones.setDisabled(false);

                txtIdOpci.setValue(entity.getId().getIdOpci());
                txtIdOpci.setDisabled(true);
                txtIdSuop.setValue(entity.getId().getIdSuop());
                txtIdSuop.setDisabled(true);
                txtNombreVisible.setValue(entity.getId().getNombreVisible());
                txtNombreVisible.setDisabled(true);
                txtFechaCreacion.setValue(entity.getId().getFechaCreacion());
                txtFechaCreacion.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
        	txtIdOpci.setValue(new Long(somOpcion.getValue().toString()) != -1 ? somOpcion.getValue() : null);
    		txtIdOpci_TbmBaseOpciones.setValue(new Long(somOpcion.getValue().toString()) != -1 ? somOpcion.getValue() : null);
    		//txtFechaCreacion.setValue(new Date());
    		
    		if(listadoSubOpcionesOpci.size() > 0){
	    		int subOpcionesOpciNuevas = 0;
	        	for(TbmBaseSubopcionesOpc subOpcionesOpci : listadoSubOpcionesOpci){
	        		if(subOpcionesOpci.getTbmBaseSubopciones().getActivo().equals("e")){
	        			subOpcionesOpciNuevas++;
	        		}
	        	}
	        	
	        	if(subOpcionesOpciNuevas == 0){
	        		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	              	mensaje.mostrarMensaje("No se agrego ninguna Sub. Opci�n nueva", MBMensaje.MENSAJE_ALERTA);
	              	
	              	return "";
	            }
    		}
    		else{
    			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("Por favor agregue al menos una Sub. Opci�n", MBMensaje.MENSAJE_ALERTA);
              	return "";
        	}
    		
            BusinessDelegatorView.saveTbmBaseSubopcionesOpc((((txtIdOpci.getValue()) == null) ||
                (txtIdOpci.getValue()).equals("")) ? null
                                                   : new Long(
                    txtIdOpci.getValue().toString()),
                (((txtIdSuop.getValue()) == null) ||
                (txtIdSuop.getValue()).equals("")) ? null
                                                   : new Long(
                    txtIdSuop.getValue().toString()),
                (((txtNombreVisible.getValue()) == null) ||
                (txtNombreVisible.getValue()).equals("")) ? null
                                                          : new String(
                    txtNombreVisible.getValue().toString()).toUpperCase(),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdOpci_TbmBaseOpciones.getValue()) == null) ||
                (txtIdOpci_TbmBaseOpciones.getValue()).equals("")) ? null
                                                                   : new Long(
                    txtIdOpci_TbmBaseOpciones.getValue().toString()),
                (((txtIdSuop_TbmBaseSubopciones.getValue()) == null) ||
                (txtIdSuop_TbmBaseSubopciones.getValue()).equals("")) ? null
                                                                      : new Long(
                    txtIdSuop_TbmBaseSubopciones.getValue().toString()), listadoSubOpcionesOpci);

            MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
          	mensaje.mostrarMensaje("Sub. Opciones adicionadas de forma satisfactoria", MBMensaje.MENSAJE_OK);

        } catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTbmBaseSubopcionesOpc((((txtIdOpci.getValue()) == null) ||
                (txtIdOpci.getValue()).equals("")) ? null
                                                   : new Long(
                    txtIdOpci.getValue().toString()),
                (((txtIdSuop.getValue()) == null) ||
                (txtIdSuop.getValue()).equals("")) ? null
                                                   : new Long(
                    txtIdSuop.getValue().toString()),
                (((txtNombreVisible.getValue()) == null) ||
                (txtNombreVisible.getValue()).equals("")) ? null
                                                          : new String(
                    txtNombreVisible.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(), new Long(-1));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateTbmBaseSubopcionesOpc((((txtIdOpci.getValue()) == null) ||
                (txtIdOpci.getValue()).equals("")) ? null
                                                   : new Long(
                    txtIdOpci.getValue().toString()),
                (((txtIdSuop.getValue()) == null) ||
                (txtIdSuop.getValue()).equals("")) ? null
                                                   : new Long(
                    txtIdSuop.getValue().toString()),
                (((txtNombreVisible.getValue()) == null) ||
                (txtNombreVisible.getValue()).equals("")) ? null
                                                          : new String(
                    txtNombreVisible.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdOpci_TbmBaseOpciones.getValue()) == null) ||
                (txtIdOpci_TbmBaseOpciones.getValue()).equals("")) ? null
                                                                   : new Long(
                    txtIdOpci_TbmBaseOpciones.getValue().toString()),
                (((txtIdSuop_TbmBaseSubopciones.getValue()) == null) ||
                (txtIdSuop_TbmBaseSubopciones.getValue()).equals("")) ? null
                                                                      : new Long(
                    txtIdSuop_TbmBaseSubopciones.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Long idOpci, Long idSuop,
        String nombreVisible, Date fechaCreacion, Long idOpci_TbmBaseOpciones,
        Long idSuop_TbmBaseSubopciones) throws Exception {
        try {
            BusinessDelegatorView.updateTbmBaseSubopcionesOpc(idOpci, idSuop,
                nombreVisible, fechaCreacion, idOpci_TbmBaseOpciones,
                idSuop_TbmBaseSubopciones);
            renderManager.getOnDemandRenderer("TbmBaseSubopcionesOpcView")
                         .requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TbmBaseSubopcionesOpcView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TbmBaseSubopcionesOpc> getTbmBaseSubopcionesOpc() {
        if (flag) {
            try {
                tbmBaseSubopcionesOpc = BusinessDelegatorView.getTbmBaseSubopcionesOpc();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tbmBaseSubopcionesOpc;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTbmBaseSubopcionesOpc(
        List<TbmBaseSubopcionesOpc> tbmBaseSubopcionesOpc) {
        this.tbmBaseSubopcionesOpc = tbmBaseSubopcionesOpc;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTbmBaseSubopcionesOpc() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TbmBaseSubopcionesOpc> getDataPage(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTbmBaseSubopcionesOpc = 0;

        try {
            totalNumberTbmBaseSubopcionesOpc = BusinessDelegatorView.findTotalNumberTbmBaseSubopcionesOpc()
                                                                    .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTbmBaseSubopcionesOpc) {
            endIndex = totalNumberTbmBaseSubopcionesOpc;
        }

        try {
            tbmBaseSubopcionesOpc = BusinessDelegatorView.findPageTbmBaseSubopcionesOpc(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TbmBaseSubopcionesOpc>(totalNumberTbmBaseSubopcionesOpc,
            startRow, tbmBaseSubopcionesOpc);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TbmBaseSubopcionesOpcDTO> getDataPageDTO(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTbmBaseSubopcionesOpc = 0;

        try {
            totalNumberTbmBaseSubopcionesOpc = BusinessDelegatorView.findTotalNumberTbmBaseSubopcionesOpc()
                                                                    .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTbmBaseSubopcionesOpc) {
            endIndex = totalNumberTbmBaseSubopcionesOpc;
        }

        try {
            tbmBaseSubopcionesOpc = BusinessDelegatorView.findPageTbmBaseSubopcionesOpc(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tbmBaseSubopcionesOpc)) {
                tbmBaseSubopcionesOpcDTO = new ArrayList<TbmBaseSubopcionesOpcDTO>();

                for (TbmBaseSubopcionesOpc tbmBaseSubopcionesOpcTmp : tbmBaseSubopcionesOpc) {
                    TbmBaseSubopcionesOpcDTO tbmBaseSubopcionesOpcDTO2 = new TbmBaseSubopcionesOpcDTO();
                    tbmBaseSubopcionesOpcDTO2.setIdOpci(tbmBaseSubopcionesOpcTmp.getId()
                                                                                .getIdOpci()
                                                                                .toString());
                    tbmBaseSubopcionesOpcDTO2.setIdSuop(tbmBaseSubopcionesOpcTmp.getId()
                                                                                .getIdSuop()
                                                                                .toString());
                    tbmBaseSubopcionesOpcDTO2.setNombreVisible(tbmBaseSubopcionesOpcTmp.getId()
                                                                                       .getNombreVisible()
                                                                                       .toString());
                    tbmBaseSubopcionesOpcDTO2.setFechaCreacion(tbmBaseSubopcionesOpcTmp.getId()
                                                                                       .getFechaCreacion());

                    tbmBaseSubopcionesOpcDTO2.setIdOpci_TbmBaseOpciones((tbmBaseSubopcionesOpcTmp.getTbmBaseOpciones()
                                                                                                 .getIdOpci() != null)
                        ? tbmBaseSubopcionesOpcTmp.getTbmBaseOpciones()
                                                  .getIdOpci().toString() : null);
                    tbmBaseSubopcionesOpcDTO2.setIdSuop_TbmBaseSubopciones((tbmBaseSubopcionesOpcTmp.getTbmBaseSubopciones()
                                                                                                    .getIdSuop() != null)
                        ? tbmBaseSubopcionesOpcTmp.getTbmBaseSubopciones()
                                                  .getIdSuop().toString() : null);
                    tbmBaseSubopcionesOpcDTO2.setTbmBaseSubopcionesOpc(tbmBaseSubopcionesOpcTmp);
                    tbmBaseSubopcionesOpcDTO2.setTbmBaseSubopcionesOpcView(this);
                    tbmBaseSubopcionesOpcDTO.add(tbmBaseSubopcionesOpcDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TbmBaseSubopcionesOpcDTO>(totalNumberTbmBaseSubopcionesOpc,
            startRow, tbmBaseSubopcionesOpcDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tbmBaseSubopcionesOpcDTO)) {
            for (TbmBaseSubopcionesOpcDTO tbmBaseSubopcionesOpcTmp : tbmBaseSubopcionesOpcDTO) {
                renderManager.getOnDemandRenderer("TbmBaseSubopcionesOpcView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tbmBaseSubopcionesOpcDTO)) {
            for (TbmBaseSubopcionesOpcDTO tbmBaseSubopcionesOpcTmp : tbmBaseSubopcionesOpcDTO) {
                renderManager.getOnDemandRenderer("TbmBaseSubopcionesOpcView")
                             .add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtIdOpci_TbmBaseOpciones() {
        return txtIdOpci_TbmBaseOpciones;
    }

    public void setTxtIdOpci_TbmBaseOpciones(
        HtmlInputText txtIdOpci_TbmBaseOpciones) {
        this.txtIdOpci_TbmBaseOpciones = txtIdOpci_TbmBaseOpciones;
    }

    public HtmlInputText getTxtIdSuop_TbmBaseSubopciones() {
        return txtIdSuop_TbmBaseSubopciones;
    }

    public void setTxtIdSuop_TbmBaseSubopciones(
        HtmlInputText txtIdSuop_TbmBaseSubopciones) {
        this.txtIdSuop_TbmBaseSubopciones = txtIdSuop_TbmBaseSubopciones;
    }

    public HtmlInputText getTxtIdOpci() {
        return txtIdOpci;
    }

    public void setTxtIdOpci(HtmlInputText txtIdOpci) {
        this.txtIdOpci = txtIdOpci;
    }

    public HtmlInputText getTxtIdSuop() {
        return txtIdSuop;
    }

    public void setTxtIdSuop(HtmlInputText txtIdSuop) {
        this.txtIdSuop = txtIdSuop;
    }

    public HtmlInputText getTxtNombreVisible() {
        return txtNombreVisible;
    }

    public void setTxtNombreVisible(HtmlInputText txtNombreVisible) {
        this.txtNombreVisible = txtNombreVisible;
    }

    public SelectInputDate getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(SelectInputDate txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TbmBaseSubopcionesOpcDTO> getTbmBaseSubopcionesOpcDTO() {
        return tbmBaseSubopcionesOpcDTO;
    }

    public void setTbmBaseSubopcionesOpcDTO(
        List<TbmBaseSubopcionesOpcDTO> tbmBaseSubopcionesOpcDTO) {
        this.tbmBaseSubopcionesOpcDTO = tbmBaseSubopcionesOpcDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }
    
    
    
    public List<TbmBaseSubopcionesOpc> getListadoSubOpcionesOpci() {
		return listadoSubOpcionesOpci;
	}

	public void setListadoSubOpcionesOpci(List<TbmBaseSubopcionesOpc> listadoSubOpcionesOpci) {
		this.listadoSubOpcionesOpci = listadoSubOpcionesOpci;
	}

	public HtmlSelectOneMenu getSomOpcion() {
		return somOpcion;
	}

	public void setSomOpcion(HtmlSelectOneMenu somOpcion) {
		this.somOpcion = somOpcion;
	}

	public List<SelectItem> getSiOpcion() {
		return siOpcion;
	}

	public void setSiOpcion(List<SelectItem> siOpcion) {
		this.siOpcion = siOpcion;
	}

	public HtmlSelectOneMenu getSomSubOpcion() {
		return somSubOpcion;
	}

	public void setSomSubOpcion(HtmlSelectOneMenu somSubOpcion) {
		this.somSubOpcion = somSubOpcion;
	}

	public List<SelectItem> getSiSubOpcion() {
		return siSubOpcion;
	}

	public void setSiSubOpcion(List<SelectItem> siSubOpcion) {
		this.siSubOpcion = siSubOpcion;
	}
    

    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TbmBaseSubopcionesOpc> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TbmBaseSubopcionesOpcDTO> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }

	
    public void consultarSubOpcionesPorOpcion(ValueChangeEvent event){
    	if (event == null || (event.getNewValue() != null && !event.getNewValue().equals(""))) {	    	
	    	Long idOpci = new Long(new Long(somOpcion.getValue().toString()) != -1 ? somOpcion.getValue().toString() : "0");
	    	
	    	listadoSubOpcionesOpci = new ArrayList<TbmBaseSubopcionesOpc>();
	    	
	    	idOpci = idOpci.equals(new Long(0)) ? null : idOpci;
	    	
	    	if(idOpci != null){    		
	    		try{
	    			//Se consulta las sub. opciones para la opcion seleccionado
	    			List<TbmBaseSubopcionesOpc> listadoSubopci = BusinessDelegatorView.findByPropertySubopcionesOpc(TbmBaseSubopcionesOpcDAO.OPCION, idOpci);	    			
	    			
	    			//Se recorre el listado de detalles y se asigna cada una de los usuaros a la tabla
	    			for(TbmBaseSubopcionesOpc objetoSubOpcionesOpci : listadoSubopci){
	    				listadoSubOpcionesOpci.add(objetoSubOpcionesOpci);
	    			}
	    			
	    			btnSave.setDisabled(false);
	    		}catch(Exception e){
	    			//FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
	    			//Mensaje mensaje = (Mensaje) FacesUtils.getManagedBean("mensaje");
	              	//mensaje.mostrarMensaje(e.getMessage(), Mensaje.MENSAJE_ERROR);
	    		}
	    	}
	    	else{
	    		btnSave.setDisabled(true);
	    	}
    	}
    }
    
    //metodo que agrega las sub. opciones a la lista
    public void agregarSubOpcion(ActionEvent event){
    	//Se valida si la sub. opciones seleccionada es valida
    	try{    		    		
	    	if((somSubOpcion.getValue() != null) && (new Long(somSubOpcion.getValue().toString()) != -1)){	    		
	    		//Se valida si la planta ya se agrego
	    		for(TbmBaseSubopcionesOpc objetoSubOpcionesOpci: listadoSubOpcionesOpci){
	    			if(objetoSubOpcionesOpci.getTbmBaseSubopciones().getIdSuop().equals(new Long(somSubOpcion.getValue().toString()))){
	    				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	    	          	mensaje.mostrarMensaje("Esta Sub. Opci�n ya fue agregada", MBMensaje.MENSAJE_ALERTA);
	    	          	
	    	          	return;
	    			}
	    		}
	    		
	    		if((txtNombreVisible.getValue() != null && !txtNombreVisible.getValue().equals(""))){
		    		if(BusinessDelegatorView.subOpcionOpciIsRegistrada(txtNombreVisible.getValue().toString().toUpperCase(), new Long(somOpcion.getValue().toString()))){
		    			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			          	mensaje.mostrarMensaje("Ya se encuentra registrada una Sub. Opci�n Opci�n con Nombre Visible \""+txtNombreVisible.getValue().toString().toUpperCase()+"\" ", MBMensaje.MENSAJE_ALERTA);
			          	
			          	return;
		            }
	    		}else{
		    		//FacesContext.getCurrentInstance().addMessage("", new FacesMessage("Campo obligatorio sin valor: Nombre Visible"));
		    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
		          	mensaje.mostrarMensaje("Campo obligatorio sin valor: Nombre Visible", MBMensaje.MENSAJE_ALERTA);
		          	
		          	return;
		    	}
	    		
	    		TbmBaseSubopcionesOpc subOpcionesOpci = new TbmBaseSubopcionesOpc();
	    		TbmBaseSubopciones subOpcionSeleccionada = new TbmBaseSubopciones();
	    		TbmBaseSubopcionesOpcId subOpciOpciId = new TbmBaseSubopcionesOpcId();
	    		subOpcionSeleccionada = BusinessDelegatorView.getTbmBaseSubopciones(new Long(somSubOpcion.getValue().toString()));
	    		//se setea el valor "0" para diferenciar las sub. opciones que estan en memoria de las que esta en la BD 
	    		subOpcionSeleccionada.setActivo("e");	    		
	    		subOpcionesOpci.setTbmBaseSubopciones(subOpcionSeleccionada);
	    		subOpciOpciId.setNombreVisible((txtNombreVisible.getValue() != null && !txtNombreVisible.getValue().equals(""))
	    				? txtNombreVisible.getValue().toString().toUpperCase() : null);
	    		subOpciOpciId.setFechaCreacion(new Date());
	    		subOpcionesOpci.setId(subOpciOpciId);
	    		
	    		listadoSubOpcionesOpci.add(subOpcionesOpci);
	    	}else{
	    		//FacesContext.getCurrentInstance().addMessage("", new FacesMessage("Favor selecciona una Planta valida"));
	    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	          	mensaje.mostrarMensaje("Por favor seleccione una Sub. Opci�n valida", MBMensaje.MENSAJE_ALERTA);
	    	}
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    	
		somSubOpcion.setValue(new Long(-1));
		somSubOpcion.setDisabled(false);
		txtNombreVisible.setValue(null);
        txtNombreVisible.setDisabled(false);
    }
    
    //metodo que elimina las sub. opciones de la lista en memoria o de la BD
    public void eliminarSubOpcion(ActionEvent event){
    	try {    		
    		TbmBaseSubopcionesOpc subOpcionesOpci = (TbmBaseSubopcionesOpc) event.getComponent().getAttributes().get("subOpcionesOpci");
    		if(subOpcionesOpci.getTbmBaseSubopciones().getActivo().equals("e")){  
    			listadoSubOpcionesOpci.remove(subOpcionesOpci);
    		}
    		else {
	    		if(new Long(somOpcion.getValue().toString()) != -1) {
	    			txtIdOpci.setValue(somOpcion.getValue());
	    			txtIdOpci_TbmBaseOpciones.setValue(somOpcion.getValue());
	    			txtIdSuop.setValue(subOpcionesOpci.getTbmBaseSubopciones().getIdSuop());
	    			txtIdSuop_TbmBaseSubopciones.setValue(subOpcionesOpci.getTbmBaseSubopciones().getIdSuop());
		        	
	    			BusinessDelegatorView.deleteTbmBaseSubopcionesOpc((((txtIdOpci.getValue()) == null) ||
    	                (txtIdOpci.getValue()).equals("")) ? null
    	                                                   : new Long(
    	                    txtIdOpci.getValue().toString()),
    	                (((txtIdSuop.getValue()) == null) ||
    	                (txtIdSuop.getValue()).equals("")) ? null
    	                                                   : new Long(
    	                    txtIdSuop.getValue().toString()),
    	                    subOpcionesOpci.getId().getNombreVisible(),
    	                    subOpcionesOpci.getId().getFechaCreacion(),
    	                    subOpcionesOpci.getId().getPosicion());
	    			
		            consultarSubOpcionesPorOpcion(null);
				}
				else {
					MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
		          	mensaje.mostrarMensaje("Debe seleccionar una Opci�n", MBMensaje.MENSAJE_ALERTA);
	    		}
    		}    		
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    }
    
    
    //metodo encargado de modificar la posicion de la sub. opcion
    public void cambiarPosicion(ActionEvent event){
    	try{
	    	TbmBaseSubopcionesOpc tbmBaseSubopcionesOpc = (TbmBaseSubopcionesOpc) event.getComponent().getAttributes().get("tbmBaseSubopcionesOpci");
	    	String opcion = FacesUtils.getRequestParameter("opcion");
	    	
	    	BusinessDelegatorView.cambiarPosicion(tbmBaseSubopcionesOpc, opcion);
	    	
	    	consultarSubOpcionesPorOpcion(null);
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    }
    
}
