package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.dto.SapDetDspDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.servlet.http.HttpSession;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class SapDetDspView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtSubdsp;
    private HtmlInputText txtNumdsp;
    private HtmlInputText txtItmdsp;
    private HtmlInputText txtCodcli;
    private HtmlInputText txtNumped;
    private HtmlInputText txtItmped;
    private HtmlInputText txtSubitm;
    private HtmlInputText txtFecdsp;
    private HtmlInputText txtCandsp;
    private HtmlInputText txtUndmed;
    private HtmlInputText txtEmptra;
    private HtmlInputText txtPlaca;
    private HtmlInputText txtNomcon;
    private HtmlInputText txtDirdsp;
    private HtmlInputText txtCankgs;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<SapDetDsp> sapDetDsp;
    private List<SapDetDspDTO> sapDetDspDTO;

    public SapDetDspView() {
        super("");
        
        txtNumped = new HtmlInputText();
        txtItmped = new HtmlInputText();
    	txtCodcli = new HtmlInputText();
    	consultarDetalleDespacho();
    }

    public String action_clear() {
        txtSubdsp.setValue(null);
        txtSubdsp.setDisabled(false);
        txtNumdsp.setValue(null);
        txtNumdsp.setDisabled(false);
        txtItmdsp.setValue(null);
        txtItmdsp.setDisabled(false);
        txtCodcli.setValue(null);
        txtCodcli.setDisabled(false);
        txtNumped.setValue(null);
        txtNumped.setDisabled(false);
        txtItmped.setValue(null);
        txtItmped.setDisabled(false);
        txtSubitm.setValue(null);
        txtSubitm.setDisabled(false);
        txtFecdsp.setValue(null);
        txtFecdsp.setDisabled(false);
        txtCandsp.setValue(null);
        txtCandsp.setDisabled(false);
        txtUndmed.setValue(null);
        txtUndmed.setDisabled(false);
        txtEmptra.setValue(null);
        txtEmptra.setDisabled(false);
        txtPlaca.setValue(null);
        txtPlaca.setDisabled(false);
        txtNomcon.setValue(null);
        txtNomcon.setDisabled(false);
        txtDirdsp.setValue(null);
        txtDirdsp.setDisabled(false);
        txtCankgs.setValue(null);
        txtCankgs.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            SapDetDsp entity = null;

            try {
                SapDetDspId id = new SapDetDspId();
                id.setSubdsp((((txtSubdsp.getValue()) == null) ||
                    (txtSubdsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtSubdsp.getValue().toString()));
                id.setNumdsp((((txtNumdsp.getValue()) == null) ||
                    (txtNumdsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtNumdsp.getValue().toString()));
                id.setItmdsp((((txtItmdsp.getValue()) == null) ||
                    (txtItmdsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtItmdsp.getValue().toString()));
                id.setCodcli((((txtCodcli.getValue()) == null) ||
                    (txtCodcli.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodcli.getValue().toString()));
                id.setNumped((((txtNumped.getValue()) == null) ||
                    (txtNumped.getValue()).equals("")) ? null
                                                       : new String(
                        txtNumped.getValue().toString()));
                id.setItmped((((txtItmped.getValue()) == null) ||
                    (txtItmped.getValue()).equals("")) ? null
                                                       : new String(
                        txtItmped.getValue().toString()));
                id.setSubitm((((txtSubitm.getValue()) == null) ||
                    (txtSubitm.getValue()).equals("")) ? null
                                                       : new String(
                        txtSubitm.getValue().toString()));
                id.setFecdsp((((txtFecdsp.getValue()) == null) ||
                    (txtFecdsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtFecdsp.getValue().toString()));
                id.setCandsp((((txtCandsp.getValue()) == null) ||
                    (txtCandsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtCandsp.getValue().toString()));
                id.setUndmed((((txtUndmed.getValue()) == null) ||
                    (txtUndmed.getValue()).equals("")) ? null
                                                       : new String(
                        txtUndmed.getValue().toString()));
                id.setEmptra((((txtEmptra.getValue()) == null) ||
                    (txtEmptra.getValue()).equals("")) ? null
                                                       : new String(
                        txtEmptra.getValue().toString()));
                id.setPlaca((((txtPlaca.getValue()) == null) ||
                    (txtPlaca.getValue()).equals("")) ? null
                                                      : new String(
                        txtPlaca.getValue().toString()));
                id.setNomcon((((txtNomcon.getValue()) == null) ||
                    (txtNomcon.getValue()).equals("")) ? null
                                                       : new String(
                        txtNomcon.getValue().toString()));
                id.setDirdsp((((txtDirdsp.getValue()) == null) ||
                    (txtDirdsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtDirdsp.getValue().toString()));
                id.setCankgs((((txtCankgs.getValue()) == null) ||
                    (txtCankgs.getValue()).equals("")) ? null
                                                       : new String(
                        txtCankgs.getValue().toString()));

                entity = BusinessDelegatorView.getSapDetDsp(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtSubdsp.setDisabled(false);
                txtNumdsp.setDisabled(false);
                txtItmdsp.setDisabled(false);
                txtCodcli.setDisabled(false);
                txtNumped.setDisabled(false);
                txtItmped.setDisabled(false);
                txtSubitm.setDisabled(false);
                txtFecdsp.setDisabled(false);
                txtCandsp.setDisabled(false);
                txtUndmed.setDisabled(false);
                txtEmptra.setDisabled(false);
                txtPlaca.setDisabled(false);
                txtNomcon.setDisabled(false);
                txtDirdsp.setDisabled(false);
                txtCankgs.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtSubdsp.setValue(entity.getId().getSubdsp());
                txtSubdsp.setDisabled(true);
                txtNumdsp.setValue(entity.getId().getNumdsp());
                txtNumdsp.setDisabled(true);
                txtItmdsp.setValue(entity.getId().getItmdsp());
                txtItmdsp.setDisabled(true);
                txtCodcli.setValue(entity.getId().getCodcli());
                txtCodcli.setDisabled(true);
                txtNumped.setValue(entity.getId().getNumped());
                txtNumped.setDisabled(true);
                txtItmped.setValue(entity.getId().getItmped());
                txtItmped.setDisabled(true);
                txtSubitm.setValue(entity.getId().getSubitm());
                txtSubitm.setDisabled(true);
                txtFecdsp.setValue(entity.getId().getFecdsp());
                txtFecdsp.setDisabled(true);
                txtCandsp.setValue(entity.getId().getCandsp());
                txtCandsp.setDisabled(true);
                txtUndmed.setValue(entity.getId().getUndmed());
                txtUndmed.setDisabled(true);
                txtEmptra.setValue(entity.getId().getEmptra());
                txtEmptra.setDisabled(true);
                txtPlaca.setValue(entity.getId().getPlaca());
                txtPlaca.setDisabled(true);
                txtNomcon.setValue(entity.getId().getNomcon());
                txtNomcon.setDisabled(true);
                txtDirdsp.setValue(entity.getId().getDirdsp());
                txtDirdsp.setDisabled(true);
                txtCankgs.setValue(entity.getId().getCankgs());
                txtCankgs.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveSapDetDsp((((txtSubdsp.getValue()) == null) ||
                (txtSubdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtSubdsp.getValue().toString()),
                (((txtNumdsp.getValue()) == null) ||
                (txtNumdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumdsp.getValue().toString()),
                (((txtItmdsp.getValue()) == null) ||
                (txtItmdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmdsp.getValue().toString()),
                (((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtNumped.getValue()) == null) ||
                (txtNumped.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumped.getValue().toString()),
                (((txtItmped.getValue()) == null) ||
                (txtItmped.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmped.getValue().toString()),
                (((txtSubitm.getValue()) == null) ||
                (txtSubitm.getValue()).equals("")) ? null
                                                   : new String(
                    txtSubitm.getValue().toString()),
                (((txtFecdsp.getValue()) == null) ||
                (txtFecdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecdsp.getValue().toString()),
                (((txtCandsp.getValue()) == null) ||
                (txtCandsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtCandsp.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtEmptra.getValue()) == null) ||
                (txtEmptra.getValue()).equals("")) ? null
                                                   : new String(
                    txtEmptra.getValue().toString()),
                (((txtPlaca.getValue()) == null) ||
                (txtPlaca.getValue()).equals("")) ? null
                                                  : new String(
                    txtPlaca.getValue().toString()),
                (((txtNomcon.getValue()) == null) ||
                (txtNomcon.getValue()).equals("")) ? null
                                                   : new String(
                    txtNomcon.getValue().toString()),
                (((txtDirdsp.getValue()) == null) ||
                (txtDirdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtDirdsp.getValue().toString()),
                (((txtCankgs.getValue()) == null) ||
                (txtCankgs.getValue()).equals("")) ? null
                                                   : new String(
                    txtCankgs.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteSapDetDsp((((txtSubdsp.getValue()) == null) ||
                (txtSubdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtSubdsp.getValue().toString()),
                (((txtNumdsp.getValue()) == null) ||
                (txtNumdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumdsp.getValue().toString()),
                (((txtItmdsp.getValue()) == null) ||
                (txtItmdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmdsp.getValue().toString()),
                (((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtNumped.getValue()) == null) ||
                (txtNumped.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumped.getValue().toString()),
                (((txtItmped.getValue()) == null) ||
                (txtItmped.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmped.getValue().toString()),
                (((txtSubitm.getValue()) == null) ||
                (txtSubitm.getValue()).equals("")) ? null
                                                   : new String(
                    txtSubitm.getValue().toString()),
                (((txtFecdsp.getValue()) == null) ||
                (txtFecdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecdsp.getValue().toString()),
                (((txtCandsp.getValue()) == null) ||
                (txtCandsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtCandsp.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtEmptra.getValue()) == null) ||
                (txtEmptra.getValue()).equals("")) ? null
                                                   : new String(
                    txtEmptra.getValue().toString()),
                (((txtPlaca.getValue()) == null) ||
                (txtPlaca.getValue()).equals("")) ? null
                                                  : new String(
                    txtPlaca.getValue().toString()),
                (((txtNomcon.getValue()) == null) ||
                (txtNomcon.getValue()).equals("")) ? null
                                                   : new String(
                    txtNomcon.getValue().toString()),
                (((txtDirdsp.getValue()) == null) ||
                (txtDirdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtDirdsp.getValue().toString()),
                (((txtCankgs.getValue()) == null) ||
                (txtCankgs.getValue()).equals("")) ? null
                                                   : new String(
                    txtCankgs.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateSapDetDsp((((txtSubdsp.getValue()) == null) ||
                (txtSubdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtSubdsp.getValue().toString()),
                (((txtNumdsp.getValue()) == null) ||
                (txtNumdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumdsp.getValue().toString()),
                (((txtItmdsp.getValue()) == null) ||
                (txtItmdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmdsp.getValue().toString()),
                (((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtNumped.getValue()) == null) ||
                (txtNumped.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumped.getValue().toString()),
                (((txtItmped.getValue()) == null) ||
                (txtItmped.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmped.getValue().toString()),
                (((txtSubitm.getValue()) == null) ||
                (txtSubitm.getValue()).equals("")) ? null
                                                   : new String(
                    txtSubitm.getValue().toString()),
                (((txtFecdsp.getValue()) == null) ||
                (txtFecdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecdsp.getValue().toString()),
                (((txtCandsp.getValue()) == null) ||
                (txtCandsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtCandsp.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtEmptra.getValue()) == null) ||
                (txtEmptra.getValue()).equals("")) ? null
                                                   : new String(
                    txtEmptra.getValue().toString()),
                (((txtPlaca.getValue()) == null) ||
                (txtPlaca.getValue()).equals("")) ? null
                                                  : new String(
                    txtPlaca.getValue().toString()),
                (((txtNomcon.getValue()) == null) ||
                (txtNomcon.getValue()).equals("")) ? null
                                                   : new String(
                    txtNomcon.getValue().toString()),
                (((txtDirdsp.getValue()) == null) ||
                (txtDirdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtDirdsp.getValue().toString()),
                (((txtCankgs.getValue()) == null) ||
                (txtCankgs.getValue()).equals("")) ? null
                                                   : new String(
                    txtCankgs.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String subdsp, String numdsp,
        String itmdsp, String codcli, String numped, String itmped,
        String subitm, String fecdsp, String candsp, String undmed,
        String emptra, String placa, String nomcon, String dirdsp, String cankgs)
        throws Exception {
        try {
            BusinessDelegatorView.updateSapDetDsp(subdsp, numdsp, itmdsp,
                codcli, numped, itmped, subitm, fecdsp, candsp, undmed, emptra,
                placa, nomcon, dirdsp, cankgs);
            renderManager.getOnDemandRenderer("SapDetDspView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("SapDetDspView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<SapDetDsp> getSapDetDsp() {
        if (flag) {
            try {
                sapDetDsp = BusinessDelegatorView.getSapDetDsp();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return sapDetDsp;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setSapDetDsp(List<SapDetDsp> sapDetDsp) {
        this.sapDetDsp = sapDetDsp;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberSapDetDsp() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<SapDetDsp> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberSapDetDsp = 0;

        try {
            totalNumberSapDetDsp = BusinessDelegatorView.findTotalNumberSapDetDsp()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberSapDetDsp) {
            endIndex = totalNumberSapDetDsp;
        }

        try {
            sapDetDsp = BusinessDelegatorView.findPageSapDetDsp(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<SapDetDsp>(totalNumberSapDetDsp, startRow, sapDetDsp);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<SapDetDspDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberSapDetDsp = 0;

        try {
            totalNumberSapDetDsp = BusinessDelegatorView.findTotalNumberSapDetDsp()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberSapDetDsp) {
            endIndex = totalNumberSapDetDsp;
        }

        try {
            sapDetDsp = BusinessDelegatorView.findPageSapDetDsp(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(sapDetDsp)) {
                sapDetDspDTO = new ArrayList<SapDetDspDTO>();

                for (SapDetDsp sapDetDspTmp : sapDetDsp) {
                    SapDetDspDTO sapDetDspDTO2 = new SapDetDspDTO();
                    sapDetDspDTO2.setSubdsp(sapDetDspTmp.getId().getSubdsp()
                                                        .toString());
                    sapDetDspDTO2.setNumdsp(sapDetDspTmp.getId().getNumdsp()
                                                        .toString());
                    sapDetDspDTO2.setItmdsp(sapDetDspTmp.getId().getItmdsp()
                                                        .toString());
                    sapDetDspDTO2.setCodcli(sapDetDspTmp.getId().getCodcli()
                                                        .toString());
                    sapDetDspDTO2.setNumped(sapDetDspTmp.getId().getNumped()
                                                        .toString());
                    sapDetDspDTO2.setItmped(sapDetDspTmp.getId().getItmped()
                                                        .toString());
                    sapDetDspDTO2.setSubitm(sapDetDspTmp.getId().getSubitm()
                                                        .toString());
                    sapDetDspDTO2.setFecdsp(sapDetDspTmp.getId().getFecdsp()
                                                        .toString());
                    sapDetDspDTO2.setCandsp(sapDetDspTmp.getId().getCandsp()
                                                        .toString());
                    sapDetDspDTO2.setUndmed(sapDetDspTmp.getId().getUndmed()
                                                        .toString());
                    sapDetDspDTO2.setEmptra(sapDetDspTmp.getId().getEmptra()
                                                        .toString());
                    sapDetDspDTO2.setPlaca(sapDetDspTmp.getId().getPlaca()
                                                       .toString());
                    sapDetDspDTO2.setNomcon(sapDetDspTmp.getId().getNomcon()
                                                        .toString());
                    sapDetDspDTO2.setDirdsp(sapDetDspTmp.getId().getDirdsp()
                                                        .toString());
                    sapDetDspDTO2.setCankgs(sapDetDspTmp.getId().getCankgs()
                                                        .toString());

                    sapDetDspDTO2.setSapDetDsp(sapDetDspTmp);
                    sapDetDspDTO2.setSapDetDspView(this);
                    sapDetDspDTO.add(sapDetDspDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<SapDetDspDTO>(totalNumberSapDetDsp, startRow,
            sapDetDspDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(sapDetDspDTO)) {
            for (SapDetDspDTO sapDetDspTmp : sapDetDspDTO) {
                renderManager.getOnDemandRenderer("SapDetDspView").remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(sapDetDspDTO)) {
            for (SapDetDspDTO sapDetDspTmp : sapDetDspDTO) {
                renderManager.getOnDemandRenderer("SapDetDspView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtSubdsp() {
        return txtSubdsp;
    }

    public void setTxtSubdsp(HtmlInputText txtSubdsp) {
        this.txtSubdsp = txtSubdsp;
    }

    public HtmlInputText getTxtNumdsp() {
        return txtNumdsp;
    }

    public void setTxtNumdsp(HtmlInputText txtNumdsp) {
        this.txtNumdsp = txtNumdsp;
    }

    public HtmlInputText getTxtItmdsp() {
        return txtItmdsp;
    }

    public void setTxtItmdsp(HtmlInputText txtItmdsp) {
        this.txtItmdsp = txtItmdsp;
    }

    public HtmlInputText getTxtCodcli() {
        return txtCodcli;
    }

    public void setTxtCodcli(HtmlInputText txtCodcli) {
        this.txtCodcli = txtCodcli;
    }

    public HtmlInputText getTxtNumped() {
        return txtNumped;
    }

    public void setTxtNumped(HtmlInputText txtNumped) {
        this.txtNumped = txtNumped;
    }

    public HtmlInputText getTxtItmped() {
        return txtItmped;
    }

    public void setTxtItmped(HtmlInputText txtItmped) {
        this.txtItmped = txtItmped;
    }

    public HtmlInputText getTxtSubitm() {
        return txtSubitm;
    }

    public void setTxtSubitm(HtmlInputText txtSubitm) {
        this.txtSubitm = txtSubitm;
    }

    public HtmlInputText getTxtFecdsp() {
        return txtFecdsp;
    }

    public void setTxtFecdsp(HtmlInputText txtFecdsp) {
        this.txtFecdsp = txtFecdsp;
    }

    public HtmlInputText getTxtCandsp() {
        return txtCandsp;
    }

    public void setTxtCandsp(HtmlInputText txtCandsp) {
        this.txtCandsp = txtCandsp;
    }

    public HtmlInputText getTxtUndmed() {
        return txtUndmed;
    }

    public void setTxtUndmed(HtmlInputText txtUndmed) {
        this.txtUndmed = txtUndmed;
    }

    public HtmlInputText getTxtEmptra() {
        return txtEmptra;
    }

    public void setTxtEmptra(HtmlInputText txtEmptra) {
        this.txtEmptra = txtEmptra;
    }

    public HtmlInputText getTxtPlaca() {
        return txtPlaca;
    }

    public void setTxtPlaca(HtmlInputText txtPlaca) {
        this.txtPlaca = txtPlaca;
    }

    public HtmlInputText getTxtNomcon() {
        return txtNomcon;
    }

    public void setTxtNomcon(HtmlInputText txtNomcon) {
        this.txtNomcon = txtNomcon;
    }

    public HtmlInputText getTxtDirdsp() {
        return txtDirdsp;
    }

    public void setTxtDirdsp(HtmlInputText txtDirdsp) {
        this.txtDirdsp = txtDirdsp;
    }

    public HtmlInputText getTxtCankgs() {
        return txtCankgs;
    }

    public void setTxtCankgs(HtmlInputText txtCankgs) {
        this.txtCankgs = txtCankgs;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<SapDetDspDTO> getSapDetDspDTO() {
        return sapDetDspDTO;
    }

    public void setSapDetDspDTO(List<SapDetDspDTO> sapDetDspDTO) {
        this.sapDetDspDTO = sapDetDspDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<SapDetDsp> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<SapDetDspDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
    
    
    //metodo encrgado de consultar el detalle despacho
    public void consultarDetalleDespacho(){
    	List<SapDetDsp> registros=null;    	
    	
    	try {
    		HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);    		
    		txtNumped.setValue((String)session.getAttribute("txtNumped"));  
    		txtItmped.setValue((String)session.getAttribute("txtItmped"));
        	txtCodcli.setValue((String)session.getAttribute("codMas"));
        	
        	if(txtNumped.getValue() != null){
	    		registros = BusinessDelegatorView.consultarDetalleDespachoSap(txtNumped.getValue().toString(), txtItmped.getValue().toString(), txtCodcli.getValue().toString());
	    		
	    		//se elimina el atributo de la sesion
	    		session.removeAttribute("txtNumped");
	    		session.removeAttribute("txtItmped");
	    		session.removeAttribute("codMas");
	            
	            if(registros == null || registros.size() == 0){            	
	            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	              	mensaje.mostrarMensaje("No se encontraron despachos relacionados para el pedido seleccionado.", MBMensaje.MENSAJE_ALERTA);
	            	
	              	sapDetDsp = new ArrayList<SapDetDsp>();
	              	
	              	/*onePageDataModel.setDirtyData(false);
	              	flag = false;
	              	onePageDataModel.setPage(new DataPage<BvpDetDsp>(0, 0, registros));*/
	              	return;
	            }
	            
	            sapDetDsp = registros;
	            
	            /*onePageDataModel.setDirtyData(false);
	            flag = true;
	            onePageDataModel.setPage(new DataPage<BvpDetDsp>(registros.size(), 0, registros));*/
            }        	
            
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    	
    	return;
    }
}
