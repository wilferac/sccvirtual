package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.dto.BvpDirDspDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class BvpDirDspView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtCodcli;
    private HtmlInputText txtCoddir;
    private HtmlInputText txtCodpla;
    private HtmlInputText txtDesdir;
    private HtmlInputText txtNumfax;
    private HtmlInputText txtDesprv;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<BvpDirDsp> bvpDirDsp;
    private List<BvpDirDspDTO> bvpDirDspDTO;

    public BvpDirDspView() {
        super("");
    }

    public String action_clear() {
        txtCodcli.setValue(null);
        txtCodcli.setDisabled(false);
        txtCoddir.setValue(null);
        txtCoddir.setDisabled(false);
        txtCodpla.setValue(null);
        txtCodpla.setDisabled(false);
        txtDesdir.setValue(null);
        txtDesdir.setDisabled(false);
        txtNumfax.setValue(null);
        txtNumfax.setDisabled(false);
        txtDesprv.setValue(null);
        txtDesprv.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            BvpDirDsp entity = null;

            try {
                BvpDirDspId id = new BvpDirDspId();
                id.setCodcli((((txtCodcli.getValue()) == null) ||
                    (txtCodcli.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodcli.getValue().toString()));
                id.setCoddir((((txtCoddir.getValue()) == null) ||
                    (txtCoddir.getValue()).equals("")) ? null
                                                       : new String(
                        txtCoddir.getValue().toString()));
                id.setCodpla((((txtCodpla.getValue()) == null) ||
                    (txtCodpla.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodpla.getValue().toString()));
                id.setDesdir((((txtDesdir.getValue()) == null) ||
                    (txtDesdir.getValue()).equals("")) ? null
                                                       : new String(
                        txtDesdir.getValue().toString()));
                id.setNumfax((((txtNumfax.getValue()) == null) ||
                    (txtNumfax.getValue()).equals("")) ? null
                                                       : new String(
                        txtNumfax.getValue().toString()));
                id.setDesprv((((txtDesprv.getValue()) == null) ||
                    (txtDesprv.getValue()).equals("")) ? null
                                                       : new String(
                        txtDesprv.getValue().toString()));

                entity = BusinessDelegatorView.getBvpDirDsp(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtCodcli.setDisabled(false);
                txtCoddir.setDisabled(false);
                txtCodpla.setDisabled(false);
                txtDesdir.setDisabled(false);
                txtNumfax.setDisabled(false);
                txtDesprv.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtCodcli.setValue(entity.getId().getCodcli());
                txtCodcli.setDisabled(true);
                txtCoddir.setValue(entity.getId().getCoddir());
                txtCoddir.setDisabled(true);
                txtCodpla.setValue(entity.getId().getCodpla());
                txtCodpla.setDisabled(true);
                txtDesdir.setValue(entity.getId().getDesdir());
                txtDesdir.setDisabled(true);
                txtNumfax.setValue(entity.getId().getNumfax());
                txtNumfax.setDisabled(true);
                txtDesprv.setValue(entity.getId().getDesprv());
                txtDesprv.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveBvpDirDsp((((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtCoddir.getValue()) == null) ||
                (txtCoddir.getValue()).equals("")) ? null
                                                   : new String(
                    txtCoddir.getValue().toString()),
                (((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtDesdir.getValue()) == null) ||
                (txtDesdir.getValue()).equals("")) ? null
                                                   : new String(
                    txtDesdir.getValue().toString()),
                (((txtNumfax.getValue()) == null) ||
                (txtNumfax.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumfax.getValue().toString()),
                (((txtDesprv.getValue()) == null) ||
                (txtDesprv.getValue()).equals("")) ? null
                                                   : new String(
                    txtDesprv.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteBvpDirDsp((((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtCoddir.getValue()) == null) ||
                (txtCoddir.getValue()).equals("")) ? null
                                                   : new String(
                    txtCoddir.getValue().toString()),
                (((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtDesdir.getValue()) == null) ||
                (txtDesdir.getValue()).equals("")) ? null
                                                   : new String(
                    txtDesdir.getValue().toString()),
                (((txtNumfax.getValue()) == null) ||
                (txtNumfax.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumfax.getValue().toString()),
                (((txtDesprv.getValue()) == null) ||
                (txtDesprv.getValue()).equals("")) ? null
                                                   : new String(
                    txtDesprv.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateBvpDirDsp((((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtCoddir.getValue()) == null) ||
                (txtCoddir.getValue()).equals("")) ? null
                                                   : new String(
                    txtCoddir.getValue().toString()),
                (((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtDesdir.getValue()) == null) ||
                (txtDesdir.getValue()).equals("")) ? null
                                                   : new String(
                    txtDesdir.getValue().toString()),
                (((txtNumfax.getValue()) == null) ||
                (txtNumfax.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumfax.getValue().toString()),
                (((txtDesprv.getValue()) == null) ||
                (txtDesprv.getValue()).equals("")) ? null
                                                   : new String(
                    txtDesprv.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String codcli, String coddir,
        String codpla, String desdir, String numfax, String desprv)
        throws Exception {
        try {
            BusinessDelegatorView.updateBvpDirDsp(codcli, coddir, codpla,
                desdir, numfax, desprv);
            renderManager.getOnDemandRenderer("BvpDirDspView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("BvpDirDspView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<BvpDirDsp> getBvpDirDsp() {
        if (flag) {
            try {
                bvpDirDsp = BusinessDelegatorView.getBvpDirDsp();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return bvpDirDsp;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setBvpDirDsp(List<BvpDirDsp> bvpDirDsp) {
        this.bvpDirDsp = bvpDirDsp;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberBvpDirDsp() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<BvpDirDsp> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberBvpDirDsp = 0;

        try {
            totalNumberBvpDirDsp = BusinessDelegatorView.findTotalNumberBvpDirDsp()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberBvpDirDsp) {
            endIndex = totalNumberBvpDirDsp;
        }

        try {
            bvpDirDsp = BusinessDelegatorView.findPageBvpDirDsp(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<BvpDirDsp>(totalNumberBvpDirDsp, startRow, bvpDirDsp);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<BvpDirDspDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberBvpDirDsp = 0;

        try {
            totalNumberBvpDirDsp = BusinessDelegatorView.findTotalNumberBvpDirDsp()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberBvpDirDsp) {
            endIndex = totalNumberBvpDirDsp;
        }

        try {
            bvpDirDsp = BusinessDelegatorView.findPageBvpDirDsp(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(bvpDirDsp)) {
                bvpDirDspDTO = new ArrayList<BvpDirDspDTO>();

                for (BvpDirDsp bvpDirDspTmp : bvpDirDsp) {
                    BvpDirDspDTO bvpDirDspDTO2 = new BvpDirDspDTO();
                    bvpDirDspDTO2.setCodcli(bvpDirDspTmp.getId().getCodcli()
                                                        .toString());
                    bvpDirDspDTO2.setCoddir(bvpDirDspTmp.getId().getCoddir()
                                                        .toString());
                    bvpDirDspDTO2.setCodpla(bvpDirDspTmp.getId().getCodpla()
                                                        .toString());
                    bvpDirDspDTO2.setDesdir(bvpDirDspTmp.getId().getDesdir()
                                                        .toString());
                    bvpDirDspDTO2.setNumfax(bvpDirDspTmp.getId().getNumfax()
                                                        .toString());
                    bvpDirDspDTO2.setDesprv(bvpDirDspTmp.getId().getDesprv()
                                                        .toString());

                    bvpDirDspDTO2.setBvpDirDsp(bvpDirDspTmp);
                    bvpDirDspDTO2.setBvpDirDspView(this);
                    bvpDirDspDTO.add(bvpDirDspDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<BvpDirDspDTO>(totalNumberBvpDirDsp, startRow,
            bvpDirDspDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(bvpDirDspDTO)) {
            for (BvpDirDspDTO bvpDirDspTmp : bvpDirDspDTO) {
                renderManager.getOnDemandRenderer("BvpDirDspView").remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(bvpDirDspDTO)) {
            for (BvpDirDspDTO bvpDirDspTmp : bvpDirDspDTO) {
                renderManager.getOnDemandRenderer("BvpDirDspView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtCodcli() {
        return txtCodcli;
    }

    public void setTxtCodcli(HtmlInputText txtCodcli) {
        this.txtCodcli = txtCodcli;
    }

    public HtmlInputText getTxtCoddir() {
        return txtCoddir;
    }

    public void setTxtCoddir(HtmlInputText txtCoddir) {
        this.txtCoddir = txtCoddir;
    }

    public HtmlInputText getTxtCodpla() {
        return txtCodpla;
    }

    public void setTxtCodpla(HtmlInputText txtCodpla) {
        this.txtCodpla = txtCodpla;
    }

    public HtmlInputText getTxtDesdir() {
        return txtDesdir;
    }

    public void setTxtDesdir(HtmlInputText txtDesdir) {
        this.txtDesdir = txtDesdir;
    }

    public HtmlInputText getTxtNumfax() {
        return txtNumfax;
    }

    public void setTxtNumfax(HtmlInputText txtNumfax) {
        this.txtNumfax = txtNumfax;
    }

    public HtmlInputText getTxtDesprv() {
        return txtDesprv;
    }

    public void setTxtDesprv(HtmlInputText txtDesprv) {
        this.txtDesprv = txtDesprv;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<BvpDirDspDTO> getBvpDirDspDTO() {
        return bvpDirDspDTO;
    }

    public void setBvpDirDspDTO(List<BvpDirDspDTO> bvpDirDspDTO) {
        this.bvpDirDspDTO = bvpDirDspDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<BvpDirDsp> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<BvpDirDspDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
}
