package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TsccvDetalleGrupoEmpresaDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.dto.BvpDetPedDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlOutputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */

//Pantalla consulta de pedidos
public class ConsultaPedidosView {
    private HtmlInputText txtNumped;
    private HtmlInputText txtCodcli;
    private HtmlInputText txtCodpro;
    private HtmlInputText txtOrdcom;
    private SelectInputDate txtFechaInicial;
    private SelectInputDate txtFechaFinal;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    //private boolean renderDataTableVpe;
    //private boolean renderDataTableSap;
    
    private HtmlSelectOneMenu somPlanta;
	private List<SelectItem> siPlanta;
	private HtmlInputText txtNombreEmpresa;
	private HtmlInputText txtIdEmpresa;
	
	private HtmlSelectOneMenu somEstadoPedido;
	private List<SelectItem> siEstadoPedido;
	private String[] valorParametroEstadoPed;
    
    private String styleDataTableVpe;
    private String styleDataTableSap;
    private boolean renderLupa;    
	

    public ConsultaPedidosView() {
    	styleDataTableVpe = "display:none";
		styleDataTableSap = "display:none";
		
		//Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();
        //Se crea un objeto del archivo propiedades		
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();        
		
		try{
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
        	txtNombreEmpresa = new HtmlInputText();
        	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
        	txtIdEmpresa = new HtmlInputText();
        	siPlanta = new ArrayList<SelectItem>();   
        	somEstadoPedido = new HtmlSelectOneMenu();
        	somEstadoPedido.setStyle("display:none");//inicialmente se deja oculta
        	siEstadoPedido = new ArrayList<SelectItem>();
			
			//se crear los listados
			List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			String valorParametroEstPed[] = null;
			
			//se cargan los valores de las listas
			valorParametroEstPed = getValorParametroEstProd();
			//listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
			
        	//se obtiene el usuario en sesion
        	MBUsuario mbUsu = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
        	TsccvUsuario userLogin = mbUsu.getUsuario();
        	
        	//se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo se muestra la lupa para la busqueda de la empresa
        		renderLupa = true;
        	}
        	else{//si es una empresa normal se colocan el nombre y el id
        		txtIdEmpresa.setValue(userLogin.getTsccvEmpresas().getIdEmpr());
        		txtNombreEmpresa.setValue(userLogin.getTsccvEmpresas().getNombre());        		
        	}	
        	
        	//lista de plantas
        	siPlanta.add(siNulo);
        	if(renderLupa == false){//si la empresa del usuario no es un grupo
        		//si la empresa del usuario tiene cod_mas se consultan las planta con tipo de producto corrugado o sacos
            	if(userLogin.getTsccvEmpresas().getCodVpe() != null || !userLogin.getTsccvEmpresas().getCodVpe().equals("")){
    				listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("vpe", new Long(archivoPropiedades.getProperty("APLICACION"))));
    			}
    			//si la empresa del usuario tiene cod_mas se consultan las planta con tipo de producto molinos
    			if((userLogin.getTsccvEmpresas().getCodMas() != null && !userLogin.getTsccvEmpresas().getCodMas().equals(""))){
    				listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("mas", new Long(archivoPropiedades.getProperty("APLICACION"))));
    			}
    			
	        	for(TsccvPlanta objetoPlanta: listadoPlantas){	        		
	        		SelectItem siPlan = new SelectItem();        		
	        		siPlan.setLabel(objetoPlanta.getDespla());
	        		siPlan.setValue(objetoPlanta.getCodpla());
	        		
	        		siPlanta.add(siPlan);
	        	}
			} 
        	
        	//lista de estadoPedido			
        	siEstadoPedido.add(siNulo);        	
            for(int i=0; i<valorParametroEstPed.length; i++){
        		if(valorParametroEstPed[i] == null || valorParametroEstPed[i].equals("")){
        			continue;
        		}
            	SelectItem siEstPed = new SelectItem();        		
            	siEstPed.setLabel(valorParametroEstPed[i]);
            	siEstPed.setValue(new Long(i));
        		
        		siEstadoPedido.add(siEstPed);
        	}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    }

    public String action_clear() {
    	try{
    		//se obtiene el usuario en sesion
        	MBUsuario mbUsu = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
        	TsccvUsuario userLogin = mbUsu.getUsuario();
        	
        	//se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo de empresas se limpian el nombre y el id
    	        ArchivoBundle archivoBundle = new ArchivoBundle();
    	    	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
    	    	txtIdEmpresa.setValue(null);
        	}
        	
	    	somPlanta.setValue(new Long(-1));
	    	txtNumped.setValue(null);
	    	txtOrdcom.setValue(null);
	    	txtCodpro.setValue(null);
	    	txtFechaInicial.setValue(null);
	    	txtFechaFinal.setValue(null);
	    	//somEstadoPedido.setValue(new Long(-1));
	    	
	    	//se limpia la lista de BvpDetPedView 
			BvpDetPedView bvpDetPedView = (BvpDetPedView) FacesUtils.getManagedBean("bvpDetPedView");
			if(bvpDetPedView != null && bvpDetPedView.getOnePageDataModel() != null){
				bvpDetPedView.getOnePageDataModel().setPage(new DataPage<BvpDetPed>(0, 0, new ArrayList<BvpDetPed>(0)));
			}
	
			//se limpia la lista de SapDetPedView
			SapDetPedView sapDetPedView = (SapDetPedView) FacesUtils.getManagedBean("sapDetPedView");
			if(sapDetPedView != null && sapDetPedView.getOnePageDataModel() != null){
				sapDetPedView.getOnePageDataModel().setPage(new DataPage<SapDetPed>(0, 0, new ArrayList<SapDetPed>(0)));
			}
	    	
	        return "";
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
    }
    
    
    public HtmlInputText getTxtNumped() {
		return txtNumped;
	}

	public void setTxtNumped(HtmlInputText txtNumped) {
		this.txtNumped = txtNumped;
	}

	public HtmlInputText getTxtCodcli() {
		return txtCodcli;
	}

	public void setTxtCodcli(HtmlInputText txtCodcli) {
		this.txtCodcli = txtCodcli;
	}
	
	public HtmlInputText getTxtCodpro() {
		return txtCodpro;
	}

	public void setTxtCodpro(HtmlInputText txtCodpro) {
		this.txtCodpro = txtCodpro;
	}

	public HtmlInputText getTxtOrdcom() {
		return txtOrdcom;
	}

	public void setTxtOrdcom(HtmlInputText txtOrdcom) {
		this.txtOrdcom = txtOrdcom;
	}

	public SelectInputDate getTxtFechaInicial() {
		return txtFechaInicial;
	}

	public void setTxtFechaInicial(SelectInputDate txtFechaInicial) {
		this.txtFechaInicial = txtFechaInicial;
	}

	public SelectInputDate getTxtFechaFinal() {
		return txtFechaFinal;
	}

	public void setTxtFechaFinal(SelectInputDate txtFechaFinal) {
		this.txtFechaFinal = txtFechaFinal;
	}

	public HtmlCommandButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(HtmlCommandButton btnSave) {
		this.btnSave = btnSave;
	}

	public HtmlCommandButton getBtnModify() {
		return btnModify;
	}

	public void setBtnModify(HtmlCommandButton btnModify) {
		this.btnModify = btnModify;
	}

	public HtmlCommandButton getBtnDelete() {
		return btnDelete;
	}

	public void setBtnDelete(HtmlCommandButton btnDelete) {
		this.btnDelete = btnDelete;
	}

	public HtmlCommandButton getBtnClear() {
		return btnClear;
	}

	public void setBtnClear(HtmlCommandButton btnClear) {
		this.btnClear = btnClear;
	}
	
	public String getStyleDataTableVpe() {
		return styleDataTableVpe;
	}

	public void setStyleDataTableVpe(String styleDataTableVpe) {
		this.styleDataTableVpe = styleDataTableVpe;
	}

	public String getStyleDataTableSap() {
		return styleDataTableSap;
	}

	public void setStyleDataTableSap(String styleDataTableSap) {
		this.styleDataTableSap = styleDataTableSap;
	}
	
	public HtmlSelectOneMenu getSomPlanta() {
		return somPlanta;
	}

	public void setSomPlanta(HtmlSelectOneMenu somPlanta) {
		this.somPlanta = somPlanta;
	}

	public List<SelectItem> getSiPlanta() {
		return siPlanta;
	}

	public void setSiPlanta(List<SelectItem> siPlanta) {
		this.siPlanta = siPlanta;
	}
	
	public HtmlInputText getTxtNombreEmpresa() {
		return txtNombreEmpresa;
	}

	public void setTxtNombreEmpresa(HtmlInputText txtNombreEmpresa) {
		this.txtNombreEmpresa = txtNombreEmpresa;
	}

	public boolean isRenderLupa() {
		return renderLupa;
	}

	public void setRenderLupa(boolean renderLupa) {
		this.renderLupa = renderLupa;
	}

	public HtmlInputText getTxtIdEmpresa() {
		return txtIdEmpresa;
	}

	public void setTxtIdEmpresa(HtmlInputText txtIdEmpresa) {
		this.txtIdEmpresa = txtIdEmpresa;
	}

	public HtmlSelectOneMenu getSomEstadoPedido() {
		return somEstadoPedido;
	}

	public void setSomEstadoPedido(HtmlSelectOneMenu somEstadoPedido) {
		this.somEstadoPedido = somEstadoPedido;
	}

	public List<SelectItem> getSiEstadoPedido() {
		return siEstadoPedido;
	}

	public void setSiEstadoPedido(List<SelectItem> siEstadoPedido) {
		this.siEstadoPedido = siEstadoPedido;
	}

	public String[] getValorParametroEstadoPed() {
		return valorParametroEstadoPed;
	}

	public void setValorParametroEstadoPed(String[] valorParametroEstadoPed) {
		this.valorParametroEstadoPed = valorParametroEstadoPed;
	}

	public TimeZone getTimeZone(){
		return TimeZone.getDefault();
	}

	//metodo agregado para consultar los pedidos de acuerdo a la informacion ingresada
    public void consultarPedidos(){
    	try {
	    	//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
    		
    		String codPlanta = new Long(somPlanta.getValue().toString()) != -1 ? somPlanta.getValue().toString() : null;
    		TsccvPlanta tsccvPlanta = null;
    		
    		Integer codCli = obtenerEmpresaUsuario();
    		TsccvEmpresas tsccvEmpresas = null;
    		
    		if(codPlanta != null && codCli != null){
    			tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(codPlanta);
    			tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(codCli);
    			
    			if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_CORRUGADO"))) ||
    				tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_SACOS")))){
    				BvpDetPedView bvpDetPedView = (BvpDetPedView) FacesUtils.getManagedBean("bvpDetPedView");
        			
    				String codplaSacosCali=null;
    				String codplaCorrugadoCali=null;
    				TsccvPlanta plantaConsulta=null;
    				codplaSacosCali= archivoPropiedades.getProperty("planta_sacos_cali");
	    	    	if(codplaSacosCali.equals(tsccvPlanta.getCodpla())){
	    	    		codplaCorrugadoCali= archivoPropiedades.getProperty("planta_sacos_cali_reemplaza");
	    	    		plantaConsulta= BusinessDelegatorView.getTsccvPlanta(codplaCorrugadoCali);
	    	    		//Pero debemos asignarle el tipo de producto sacos
	    	    		TsccvTipoProducto tipo= new TsccvTipoProducto();
	    	    		tipo.setActivo("0");
	    	    		tipo.setDescripcion("SACOS");
	    	    		tipo.setIdTipr(new Long(2));
	    	    		plantaConsulta.setTsccvTipoProducto(tipo);
	    	    	}
	    	    	else{
	    	    		plantaConsulta= tsccvPlanta;
	    	    	}
    				
    				
    				
    				bvpDetPedView.setTxtNumped(txtNumped);
    				bvpDetPedView.setTxtOrdcom(txtOrdcom);
    				bvpDetPedView.setTxtCodpro(txtCodpro);
    				bvpDetPedView.setTxtFechaInicial(txtFechaInicial);
    				bvpDetPedView.setTxtFechaFinal(txtFechaFinal);
    				bvpDetPedView.setTsccvPlanta(plantaConsulta);
    				bvpDetPedView.setTsccvEmpresas(tsccvEmpresas);
    				bvpDetPedView.consultarPedidosBvp();
    				
    				styleDataTableVpe = "display:block";
    				styleDataTableSap = "display:none";
            	}
            	else{//productos molinos
            		SapDetPedView sapDetPedView = (SapDetPedView) FacesUtils.getManagedBean("sapDetPedView");
            		
            		sapDetPedView.setTxtNumped(txtNumped);
            		sapDetPedView.setTxtOrdcom(txtOrdcom);
    				sapDetPedView.setTxtCodpro(txtCodpro);
    				sapDetPedView.setTxtFechaInicial(txtFechaInicial);
    				sapDetPedView.setTxtFechaFinal(txtFechaFinal);
    				sapDetPedView.setTsccvEmpresas(tsccvEmpresas);
    				sapDetPedView.setEstadoPedido(new Long(somEstadoPedido.getValue().toString()) != -1 ? somEstadoPedido.getValue().toString() : null);
            		
            		sapDetPedView.consultarPedidosSap();
    				
            		styleDataTableVpe = "display:none";
            		styleDataTableSap = "display:block";
            	}
    		}
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    	
    	return;
    }
    
    public Integer obtenerEmpresaUsuario(){
		String codigoCli = null;
		codigoCli = txtIdEmpresa.getValue().toString();
		return !codigoCli.equals("") ? new Integer(codigoCli) : null;
	}
	
	//metodo encargado mostrar el panel para la busqueda y seleccion de la empresa
	public void buscarEmpresa(){
		styleDataTableVpe = "display:none";
		styleDataTableSap = "display:none";
		
		//se obtiene el managebean
		BusquedaEmpresaView busquedaEmpresaView = (BusquedaEmpresaView) FacesUtils.getManagedBean("busquedaEmpresaView");
		
		//se setea el managebean que realiza el llamado this
		busquedaEmpresaView.setManageBeanLlmado(this);
		busquedaEmpresaView.mostrarPanel();
	}
	
	//metodo encargado de setear a los componentes de la pantalla el nombre y el id de la empresa seleccionada 
	public void colocarSeleccion(Integer idEmpr, String nombreEmpr){
		txtIdEmpresa.setValue(idEmpr);
		txtNombreEmpresa.setValue(nombreEmpr);	
		refrescarListaPlantas();
	}
	
	//metodo encargado de colocar las plantas de acuerdo a la empresa seleccionada considarando:
	//1. si la empresa tiene cod_vpe solo se cargan plantas con tipo de producto corrugado o sacos
	//2. si la empresa tiene cod_mas solo se cargan plantas con tipo de producto molinos
	public void refrescarListaPlantas(){
		try{
			//Se crea un objeto del bundle		
	        ArchivoBundle archivoBundle = new ArchivoBundle();
	        //Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        
			List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			//listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
			siPlanta = new ArrayList<SelectItem>();
			
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
	    	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
	    	siNulo.setValue(new Long(-1));
	    	siPlanta.add(siNulo);
	    	
	    	TsccvEmpresas tsccvEmpresas = null;
	    	Long tipoProdPlanta = new Long(0);
	    	if(!txtIdEmpresa.getValue().equals("")){
	    		tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(new Integer(txtIdEmpresa.getValue().toString()));	    	
			
	    		//se consultan las plantas del cliente
	        	if(tsccvEmpresas.getCodVpe() != null && !tsccvEmpresas.getCodVpe().equals("")){
					listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("vpe", new Long(archivoPropiedades.getProperty("APLICACION"))));
				}
				//si la la empresa del usuario tiene cod_mas elimina las planta con tipo de producto corrugado o sacos
				if((tsccvEmpresas.getCodMas() != null && !tsccvEmpresas.getCodMas().equals(""))){
					listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("mas", new Long(archivoPropiedades.getProperty("APLICACION"))));
				}
	        	
	        	for(TsccvPlanta objetoPlanta: listadoPlantas){	        		
	        		SelectItem siPlan = new SelectItem();        		
	        		siPlan.setLabel(objetoPlanta.getDespla());
	        		siPlan.setValue(objetoPlanta.getCodpla());
	        		
	        		siPlanta.add(siPlan);
				}
		    	
		    	if((tsccvEmpresas.getCodMas() != null && !tsccvEmpresas.getCodMas().equals("")) &&
					tipoProdPlanta.equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
		    		somEstadoPedido.setStyle("display:block");
		    	}
		    	else{
		    		somEstadoPedido.setStyle("display:none");
		    	}
	    	}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	
	//metodo encargado de limpiar las listas cuando cambie la planta o la empresa
	public void cambiaPlanta(ValueChangeEvent event){
		try{			
			if(new Long(somPlanta.getValue().toString()) != -1){
				//Se crea un objeto del archivo propiedades		
		        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		        
				String codPlanta = new Long(somPlanta.getValue().toString()) != -1 ? somPlanta.getValue().toString() : null;
	    		TsccvPlanta tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(codPlanta);
				
	    		//si la planta tiene tipo de producto molinos se muestra el campo  estado
				if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
		    		somEstadoPedido.setStyle("display:block");
		    	}
		    	else{
		    		somEstadoPedido.setStyle("display:none");
		    	}
			}
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	
	//Metodo encargado de consultar el parametro para el campo estadoPed
    public String[] getValorParametroEstProd(){    	
    	//ARCHIVO PORPIEDADES
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String temp[] = null;
        try {
        	temp = archivoPropiedades.getProperty("LISTA_ESTADO_PEDIDO").toString().split(",");
        	
        	if(temp != null){
        		valorParametroEstadoPed = new String[temp.length+1];
	        	for(int i=0; i<temp.length; i++){
	        		valorParametroEstadoPed[i] = temp[i];
	        	}
        	}
			return valorParametroEstadoPed;
        } catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return null;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return null;
		}          
    }
}
