package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.dto.SapPrecioDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.ext.RowSelectorEvent;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
//UTILIZADO PARA EL INGRESO DE ORDENES
public class SapPrecioView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtDespro;
    private HtmlInputText txtDiamtr;
    private HtmlInputText txtEje;
    private HtmlInputText txtMoneda;
    private HtmlInputText txtTiprec;
    private HtmlInputText txtUndmed;
    private HtmlInputText txtCodcli;
    private HtmlInputText txtCodpro;
    private HtmlInputText txtPrecio;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<SapPrecio> sapPrecio;
    private List<SapPrecioDTO> sapPrecioDTO;

    
    private HtmlInputText txtClaveBusqueda;
	private HtmlSelectOneMenu somClaveBusqueda;
	private List<SelectItem> siClaveBusqueda;
	private List<ProductoVO> listaProductos;
	private List<ProductoVO> listaProductosSeleccionados;	
    
    public SapPrecioView() {
        super("");
        
        //Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();
		
		try{
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
        	siClaveBusqueda = new ArrayList<SelectItem>();
        	listaProductos = new ArrayList<ProductoVO>();
        	listaProductosSeleccionados = new ArrayList<ProductoVO>();        	
			
			//se crear los listados
			String[] listadoClaBusq = null;
			List<BvpPrecio> listaSapPrecio = null;
			
			//se cargan los valores de las listas
			listadoClaBusq = getValorParametroClaBusq(); 
			listaSapPrecio = BusinessDelegatorView.getBvpPrecio();
			
        	//lista de clave busqueda
        	siClaveBusqueda.add(siNulo);
        	for(int i=0; i<listadoClaBusq.length; i++){
        		if(listadoClaBusq[i] == null || listadoClaBusq[i].equals("")){
        			continue;
        		}
        		
            	SelectItem siClaBusq = new SelectItem();        		
            	siClaBusq.setLabel(listadoClaBusq[i]);
            	siClaBusq.setValue(new Long(i));
        		
            	siClaveBusqueda.add(siClaBusq);
        	}
        	
        	//lista de productos
			for(BvpPrecio bvpPrecio : listaSapPrecio){
				ProductoVO objPedido = new ProductoVO();
				
				objPedido.setBvpPrecio(bvpPrecio);
				objPedido.setSeleccionado(false);
				
				listaProductos.add(objPedido);
			}
			
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
    }

    public String action_clear() {
        txtDespro.setValue(null);
        txtDespro.setDisabled(true);
        txtDiamtr.setValue(null);
        txtDiamtr.setDisabled(true);
        txtEje.setValue(null);
        txtEje.setDisabled(true);
        txtMoneda.setValue(null);
        txtMoneda.setDisabled(true);
        txtTiprec.setValue(null);
        txtTiprec.setDisabled(true);
        txtUndmed.setValue(null);
        txtUndmed.setDisabled(true);

        txtCodcli.setValue(null);
        txtCodcli.setDisabled(false);
        txtCodpro.setValue(null);
        txtCodpro.setDisabled(false);
        txtPrecio.setValue(null);
        txtPrecio.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            SapPrecio entity = null;

            try {
                SapPrecioId id = new SapPrecioId();
                id.setCodcli((((txtCodcli.getValue()) == null) ||
                    (txtCodcli.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodcli.getValue().toString()));
                id.setCodpro((((txtCodpro.getValue()) == null) ||
                    (txtCodpro.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodpro.getValue().toString()));
                id.setPrecio((((txtPrecio.getValue()) == null) ||
                    (txtPrecio.getValue()).equals("")) ? null
                                                       : new String(
                        txtPrecio.getValue().toString()));

                entity = BusinessDelegatorView.getSapPrecio(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtDespro.setDisabled(false);
                txtDiamtr.setDisabled(false);
                txtEje.setDisabled(false);
                txtMoneda.setDisabled(false);
                txtTiprec.setDisabled(false);
                txtUndmed.setDisabled(false);

                txtCodcli.setDisabled(false);
                txtCodpro.setDisabled(false);
                txtPrecio.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtDespro.setValue(entity.getDespro());
                txtDespro.setDisabled(false);
                txtDiamtr.setValue(entity.getDiamtr());
                txtDiamtr.setDisabled(false);
                txtEje.setValue(entity.getEje());
                txtEje.setDisabled(false);
                txtMoneda.setValue(entity.getMoneda());
                txtMoneda.setDisabled(false);
                txtTiprec.setValue(entity.getTiprec());
                txtTiprec.setDisabled(false);
                txtUndmed.setValue(entity.getUndmed());
                txtUndmed.setDisabled(false);

                txtCodcli.setValue(entity.getId().getCodcli());
                txtCodcli.setDisabled(true);
                txtCodpro.setValue(entity.getId().getCodpro());
                txtCodpro.setDisabled(true);
                txtPrecio.setValue(entity.getId().getPrecio());
                txtPrecio.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveSapPrecio((((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtPrecio.getValue()) == null) ||
                (txtPrecio.getValue()).equals("")) ? null
                                                   : new String(
                    txtPrecio.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtDiamtr.getValue()) == null) ||
                (txtDiamtr.getValue()).equals("")) ? null
                                                   : new String(
                    txtDiamtr.getValue().toString()),
                (((txtEje.getValue()) == null) ||
                (txtEje.getValue()).equals("")) ? null
                                                : new String(
                    txtEje.getValue().toString()),
                (((txtMoneda.getValue()) == null) ||
                (txtMoneda.getValue()).equals("")) ? null
                                                   : new String(
                    txtMoneda.getValue().toString()),
                (((txtTiprec.getValue()) == null) ||
                (txtTiprec.getValue()).equals("")) ? null
                                                   : new String(
                    txtTiprec.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteSapPrecio((((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtPrecio.getValue()) == null) ||
                (txtPrecio.getValue()).equals("")) ? null
                                                   : new String(
                    txtPrecio.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateSapPrecio((((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtPrecio.getValue()) == null) ||
                (txtPrecio.getValue()).equals("")) ? null
                                                   : new String(
                    txtPrecio.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtDiamtr.getValue()) == null) ||
                (txtDiamtr.getValue()).equals("")) ? null
                                                   : new String(
                    txtDiamtr.getValue().toString()),
                (((txtEje.getValue()) == null) ||
                (txtEje.getValue()).equals("")) ? null
                                                : new String(
                    txtEje.getValue().toString()),
                (((txtMoneda.getValue()) == null) ||
                (txtMoneda.getValue()).equals("")) ? null
                                                   : new String(
                    txtMoneda.getValue().toString()),
                (((txtTiprec.getValue()) == null) ||
                (txtTiprec.getValue()).equals("")) ? null
                                                   : new String(
                    txtTiprec.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String codcli, String codpro,
        String precio, String despro, String diamtr, String eje, String moneda,
        String tiprec, String undmed) throws Exception {
        try {
            BusinessDelegatorView.updateSapPrecio(codcli, codpro, precio,
                despro, diamtr, eje, moneda, tiprec, undmed);
            renderManager.getOnDemandRenderer("SapPrecioView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("SapPrecioView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<SapPrecio> getSapPrecio() {
        if (flag) {
            try {
                sapPrecio = BusinessDelegatorView.getSapPrecio();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return sapPrecio;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setSapPrecio(List<SapPrecio> sapPrecio) {
        this.sapPrecio = sapPrecio;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberSapPrecio() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<SapPrecio> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberSapPrecio = 0;

        try {
            totalNumberSapPrecio = BusinessDelegatorView.findTotalNumberSapPrecio()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberSapPrecio) {
            endIndex = totalNumberSapPrecio;
        }

        try {
            sapPrecio = BusinessDelegatorView.findPageSapPrecio(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<SapPrecio>(totalNumberSapPrecio, startRow, sapPrecio);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<SapPrecioDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberSapPrecio = 0;

        try {
            totalNumberSapPrecio = BusinessDelegatorView.findTotalNumberSapPrecio()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberSapPrecio) {
            endIndex = totalNumberSapPrecio;
        }

        try {
            sapPrecio = BusinessDelegatorView.findPageSapPrecio(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(sapPrecio)) {
                sapPrecioDTO = new ArrayList<SapPrecioDTO>();

                for (SapPrecio sapPrecioTmp : sapPrecio) {
                    SapPrecioDTO sapPrecioDTO2 = new SapPrecioDTO();
                    sapPrecioDTO2.setCodcli(sapPrecioTmp.getId().getCodcli()
                                                        .toString());
                    sapPrecioDTO2.setCodpro(sapPrecioTmp.getId().getCodpro()
                                                        .toString());
                    sapPrecioDTO2.setPrecio(sapPrecioTmp.getId().getPrecio()
                                                        .toString());

                    sapPrecioDTO2.setDespro((sapPrecioTmp.getDespro() != null)
                        ? sapPrecioTmp.getDespro().toString() : null);
                    sapPrecioDTO2.setDiamtr((sapPrecioTmp.getDiamtr() != null)
                        ? sapPrecioTmp.getDiamtr().toString() : null);
                    sapPrecioDTO2.setEje((sapPrecioTmp.getEje() != null)
                        ? sapPrecioTmp.getEje().toString() : null);
                    sapPrecioDTO2.setMoneda((sapPrecioTmp.getMoneda() != null)
                        ? sapPrecioTmp.getMoneda().toString() : null);
                    sapPrecioDTO2.setTiprec((sapPrecioTmp.getTiprec() != null)
                        ? sapPrecioTmp.getTiprec().toString() : null);
                    sapPrecioDTO2.setUndmed((sapPrecioTmp.getUndmed() != null)
                        ? sapPrecioTmp.getUndmed().toString() : null);
                    sapPrecioDTO2.setSapPrecio(sapPrecioTmp);
                    sapPrecioDTO2.setSapPrecioView(this);
                    sapPrecioDTO.add(sapPrecioDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<SapPrecioDTO>(totalNumberSapPrecio, startRow,
            sapPrecioDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(sapPrecioDTO)) {
            for (SapPrecioDTO sapPrecioTmp : sapPrecioDTO) {
                renderManager.getOnDemandRenderer("SapPrecioView").remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(sapPrecioDTO)) {
            for (SapPrecioDTO sapPrecioTmp : sapPrecioDTO) {
                renderManager.getOnDemandRenderer("SapPrecioView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtDespro() {
        return txtDespro;
    }

    public void setTxtDespro(HtmlInputText txtDespro) {
        this.txtDespro = txtDespro;
    }

    public HtmlInputText getTxtDiamtr() {
        return txtDiamtr;
    }

    public void setTxtDiamtr(HtmlInputText txtDiamtr) {
        this.txtDiamtr = txtDiamtr;
    }

    public HtmlInputText getTxtEje() {
        return txtEje;
    }

    public void setTxtEje(HtmlInputText txtEje) {
        this.txtEje = txtEje;
    }

    public HtmlInputText getTxtMoneda() {
        return txtMoneda;
    }

    public void setTxtMoneda(HtmlInputText txtMoneda) {
        this.txtMoneda = txtMoneda;
    }

    public HtmlInputText getTxtTiprec() {
        return txtTiprec;
    }

    public void setTxtTiprec(HtmlInputText txtTiprec) {
        this.txtTiprec = txtTiprec;
    }

    public HtmlInputText getTxtUndmed() {
        return txtUndmed;
    }

    public void setTxtUndmed(HtmlInputText txtUndmed) {
        this.txtUndmed = txtUndmed;
    }

    public HtmlInputText getTxtCodcli() {
        return txtCodcli;
    }

    public void setTxtCodcli(HtmlInputText txtCodcli) {
        this.txtCodcli = txtCodcli;
    }

    public HtmlInputText getTxtCodpro() {
        return txtCodpro;
    }

    public void setTxtCodpro(HtmlInputText txtCodpro) {
        this.txtCodpro = txtCodpro;
    }

    public HtmlInputText getTxtPrecio() {
        return txtPrecio;
    }

    public void setTxtPrecio(HtmlInputText txtPrecio) {
        this.txtPrecio = txtPrecio;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<SapPrecioDTO> getSapPrecioDTO() {
        return sapPrecioDTO;
    }

    public void setSapPrecioDTO(List<SapPrecioDTO> sapPrecioDTO) {
        this.sapPrecioDTO = sapPrecioDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    
    public HtmlInputText getTxtClaveBusqueda() {
		return txtClaveBusqueda;
	}

	public void setTxtClaveBusqueda(HtmlInputText txtClaveBusqueda) {
		this.txtClaveBusqueda = txtClaveBusqueda;
	}

	public HtmlSelectOneMenu getSomClaveBusqueda() {
		return somClaveBusqueda;
	}

	public void setSomClaveBusqueda(HtmlSelectOneMenu somClaveBusqueda) {
		this.somClaveBusqueda = somClaveBusqueda;
	}

	public List<SelectItem> getSiClaveBusqueda() {
		return siClaveBusqueda;
	}

	public void setSiClaveBusqueda(List<SelectItem> siClaveBusqueda) {
		this.siClaveBusqueda = siClaveBusqueda;
	}
    
	public List<ProductoVO> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProductoVO> listaProductos) {
		this.listaProductos = listaProductos;
	}
	
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<SapPrecio> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<SapPrecioDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }    
    
    //Metodo encargado de consultar el parametro para el campo claveBusqueda
    public String[] getValorParametroClaBusq(){    	
    	//ARCHIVO PORPIEDADES
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String temp[] = null;
        try {
        	temp = archivoPropiedades.getProperty("LISTA_CLAVE_BUSQUEDA").toString().split(",");
        	
			return temp;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
		return null;           
    }
    
    //metodo encargado de adiconar los productos seleccionados a la una segunda lista
	public void seleccionar(RowSelectorEvent event){
		listaProductosSeleccionados.clear();
		
		for(ProductoVO objPedido : listaProductos){			
			if(objPedido.getSeleccionado()){
				listaProductosSeleccionados.add(objPedido);
			}			
		}		
	}
	
	
	public String generarPedido(){
		TsccvPedidos pedido = new TsccvPedidos();
		TsccvProductosPedido prodPed = null;
		Set<TsccvProductosPedido> listaProdsPedido = new HashSet<TsccvProductosPedido>();
		
		for(ProductoVO objPedido : listaProductosSeleccionados){			
			prodPed = new TsccvProductosPedido();
			//prodPed.set
		}
		
		return "";
	}
	
}
