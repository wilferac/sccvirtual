package co.com.smurfit.presentation.backEndBeans;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.inputfile.FileInfo;
import com.icesoft.faces.component.inputfile.InputFile;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ArchivoPDFVO;
import co.com.smurfit.utilities.FacesUtils;

public class CrudPDFsView {
	private HtmlSelectOneMenu somPlantas;
    private List<SelectItem> siPlantas;
    private HtmlInputText txtCodigoProducto;
    private HashMap archivosCargados;
    private String nombreArchivo;
    private List<ArchivoPDFVO> listadoPDFs;
    private InputFile archivo;
    
	public CrudPDFsView(){
		String estadoActivo=null;
		Long idApli=null;
		ArchivoPropiedades propiedades= null;
		List<TsccvPlanta> plantas = null;
		String itemVacio=null;
		ArchivoBundle etiquetas=null;
		
		try {
			/*Consultamos las plantas activas y de la aplicacion adecuada para cargar la lista de plantas*/
			//Consultamos los parametros necesarios para realizar la consulta
			propiedades= new ArchivoPropiedades();
			etiquetas= new ArchivoBundle();
			estadoActivo= propiedades.getProperty("ESTADO_ACTIVO");
			idApli= Long.parseLong(propiedades.getProperty("APLICACION"));
			itemVacio= etiquetas.getProperty("seleccionarRegistro");
			plantas= BusinessDelegatorView.findByActivoAplicacion(estadoActivo, idApli);
			
			somPlantas = new HtmlSelectOneMenu();
	        siPlantas = new ArrayList<SelectItem>();
			//Cargamos el item vacio de la lista
			SelectItem si= new SelectItem();
        	si.setLabel(itemVacio);
        	si.setValue(new Long(-1)); 
        	siPlantas.add(si);
        	for(TsccvPlanta objetoPlanta: plantas){
        		//se agrega validacion para que solo aparescan plantas con tipo de producto corrugado o sacos (agregado el 2010-03-19)
        		if(!objetoPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(propiedades.getProperty("PRODUCTO_CORRUGADO"))) &&
         		   !objetoPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(propiedades.getProperty("PRODUCTO_SACOS")))){
         			continue;
         		}
        		
        		SelectItem siCr = new SelectItem();
        		siCr.setLabel(objetoPlanta.getDespla());
        		siCr.setValue(objetoPlanta.getCodpla());
        		
        		siPlantas.add(siCr);
        	}
        	
        	//Instanciamos el hashmap en donde se cargaran los archivos
        	archivosCargados= new HashMap();
        	
        	//Instanciamos la lista que contendra los datos de los pdfs adicionados
        	listadoPDFs = new ArrayList<ArchivoPDFVO>();
        	
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public HtmlInputText getTxtCodigoProducto() {
		return txtCodigoProducto;
	}

	public void setTxtCodigoProducto(HtmlInputText txtCodigoProducto) {
		this.txtCodigoProducto = txtCodigoProducto;
	}

	public HtmlSelectOneMenu getSomPlantas() {
		return somPlantas;
	}

	public InputFile getArchivo() {
		return archivo;
	}

	public void setArchivo(InputFile archivo) {
		this.archivo = archivo;
	}

	public void setSomPlantas(HtmlSelectOneMenu somPlantas) {
		this.somPlantas = somPlantas;
	}

	public List<SelectItem> getSiPlantas() {
		return siPlantas;
	}

	public void setSiPlantas(List<SelectItem> siPlantas) {
		this.siPlantas = siPlantas;
	}
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	
	public List<ArchivoPDFVO> getListadoPDFs() {
		return listadoPDFs;
	}

	public void setListadoPDFs(List<ArchivoPDFVO> listadoPDFs) {
		this.listadoPDFs = listadoPDFs;
	}
	
	
	public void uploadFile(ActionEvent event) {
		try{
			InputFile inputFile = (InputFile) event.getSource();
	        FileInfo fileInfo = inputFile.getFileInfo();
			if (fileInfo.getStatus() == FileInfo.SAVED) {
				//se valida el tipo de archivo enviado
	            if(!inputFile.getFileInfo().getContentType().equals("application/pdf")){
	            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
					mensaje.mostrarMensaje("Solo es posible cargar archivos formato pdf", MBMensaje.MENSAJE_ALERTA);
					archivo.reset();
					return;
	            }
	            archivo = inputFile;
	        }
		}catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
	}
	
	
	public String limpiar(){
		somPlantas.resetValue();
		somPlantas.setSubmittedValue(null);
		txtCodigoProducto.setSubmittedValue(null);
		txtCodigoProducto.resetValue();
		archivo.reset();
    	
    	return "";
	}
	
	
	/*
	public void uploadFile(ActionEvent event) {
        InputFile inputFile = (InputFile) event.getSource();
        FileInfo fileInfo = inputFile.getFileInfo();
        if (fileInfo.getStatus() == FileInfo.SAVED) {
            //Validamos que se haya seleccionado la planta y se haya ingresado el codigo del producto
        	if (somPlantas.getValue() == null
					|| somPlantas.getValue().toString() == "-1"
					|| txtCodigoProducto.getValue() == null
					|| txtCodigoProducto.getValue().toString().equals("")) {
        		//TODO Parametrizar mensaje
        		MBMensaje msg= new MBMensaje();
        		msg.mostrarMensaje("Debe seleccionar y/o ingresar el c�digo del producto", MBMensaje.MENSAJE_ERROR);
        		return;	
        	}
        	
        	//Validamos que no se haya ya cargado un archivo para la misma planta y el mismo codigo
        	if(archivosCargados.get(somPlantas.getValue().toString()+"-"+txtCodigoProducto.getValue().toString()) != null){
        		//TODO Parametrizar mensaje
        		MBMensaje msg= new MBMensaje();
        		msg.mostrarMensaje("Ya se cargo un archivo para la planta y c�digo de producto seleccionados", MBMensaje.MENSAJE_ERROR);
        		return;
        	}
        	
        	
        	
        	
        	//Adicionamos el archivo al hasmap de archivos
        	archivosCargados.put(somPlantas.getValue().toString()+"-"+txtCodigoProducto.getValue().toString(), inputFile.getFileInfo().getFile());
            nombreArchivo= inputFile.getFileInfo().getFileName();
        }
        
      	//Creamos el entity
		TsccvPdfsProductos datosPDF= new TsccvPdfsProductos();
		datosPDF.setFechaCreacion(new Date());
		TsccvPdfsProductosId pk = new TsccvPdfsProductosId();
		pk.setCodpla(somPlantas.getValue().toString());
		pk.setCodScc(txtCodigoProducto.getValue().toString());
		datosPDF.setId(pk);
		
		//Asignamos la descripcion de la planta
		TsccvPlanta  pl= new TsccvPlanta();
		pl.setCodpla(somPlantas.getValue().toString());
		//Buscamos la descripcion de la planta
		for(SelectItem si : siPlantas){
			if(si.getValue().toString().equals(somPlantas.getValue().toString()) ){
				pl.setDespla(si.getLabel());
				break;
			}
		}
		datosPDF.setTsccvPlanta(pl);
		
		
		//Creamos el vo que representara el registro
		ArchivoPDFVO arc= new ArchivoPDFVO();
		arc.setNombreArchivo(nombreArchivo);
		arc.setPdf(datosPDF);
		arc.setEstado("Nuevo");
		
		//Adicionamos el nuevo registro a la lista
		listadoPDFs.add(arc);
    }*/

	//metodo encargado de adicionar los archivo a la lista
	public void adicionarArchivo(ActionEvent event){
		try{
			if(archivo.getFileInfo().getFile() != null){
				//Validamos que se haya seleccionado la planta y se haya ingresado el codigo del producto
		    	if (somPlantas.getValue() == null || new Long(somPlantas.getValue().toString()) == -1 || 
		    			txtCodigoProducto.getValue() == null || txtCodigoProducto.getValue().toString().equals("")) {
		    		
		    		//TODO Parametrizar mensaje
		    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
		    		mensaje.mostrarMensaje("Debe seleccionar y/o ingresar el c�digo del producto", MBMensaje.MENSAJE_ERROR);
		    		return;	
		    	}
		    	else{
		    		//se valida que sea un codigo de producto que este registrado en el sistema
		    		if(!BusinessDelegatorView.precioIsRegistrado(txtCodigoProducto.getValue().toString(), somPlantas.getValue().toString())){
		    			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	    				mensaje.mostrarMensaje("El c�digo de producto no se encuentra registrado en el sistema", MBMensaje.MENSAJE_ALERTA);
	    				return;
		    		}
		    		
		    		//se valida si el pdf ya fue cargado
		    		TsccvPdfsProductosId tsccvPdfsProductosId = new TsccvPdfsProductosId();
		    		tsccvPdfsProductosId.setCodpla(somPlantas.getValue().toString());
		    		tsccvPdfsProductosId.setCodScc(txtCodigoProducto.getValue().toString());
		    		
		    		//se valida si ya se agrego el archivo a la listaPdf
		    		for(ArchivoPDFVO arcPdf : listadoPDFs){
		    			if(arcPdf.getPdf().getId().equals(tsccvPdfsProductosId)){
		    				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
		    				mensaje.mostrarMensaje("Ya se cargo un archivo para la planta y c�digo de producto seleccionados", MBMensaje.MENSAJE_ERROR);
		    				return;
		    			}
		    		}
		    		
		    		//Creamos el vo que representara el registro
		    		ArchivoPDFVO archivoPDF = new ArchivoPDFVO();
		    		TsccvPdfsProductos tsccvPdfsProductos = new TsccvPdfsProductos();	
		    		tsccvPdfsProductos.setId(tsccvPdfsProductosId);
		    		tsccvPdfsProductos.setFechaCreacion(new Date());	    	
		    		TsccvPlanta tsccvPlanta = new TsccvPlanta();		
		    		tsccvPlanta.setCodpla(somPlantas.getValue().toString());
		    		
		    		//Buscamos la descripcion de la planta
		    		for(SelectItem si : siPlantas){
		    			if(si.getValue().toString().equals(somPlantas.getValue().toString())){
		    				tsccvPlanta.setDespla(si.getLabel());
		    				break;
		    			}
		    		}
		    		
		    		tsccvPdfsProductos.setTsccvPlanta(tsccvPlanta);
		    		archivoPDF.setPdf(tsccvPdfsProductos);	    			    	
		    		archivoPDF.setNombreArchivo(archivo.getFileInfo().getFileName());
		    		archivoPDF.setUrl(archivo.getFileInfo().getFile().getPath());
		    		archivoPDF.setEstado("Nuevo");
		    		
		    		//se valida si el archivo ya existe para colocar el estado
		    		if(BusinessDelegatorView.getTsccvPdfsProductos(tsccvPdfsProductosId) != null){
		    			archivoPDF.setEstado("Existente");
		    		}
		    		
		    		listadoPDFs.add(archivoPDF);
		    		
		    		//Limpiarmos los valores de los campos
		    		somPlantas.resetValue();
		    		somPlantas.setSubmittedValue(null);
		    		txtCodigoProducto.setSubmittedValue(null);
		    		txtCodigoProducto.resetValue();
		    		archivo.reset();
				} 
			}
			else{
				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensaje("Debe seleccionar y enviar un archivo", MBMensaje.MENSAJE_ERROR);
			}
		}catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
	}
	
	/*
	public void adicionarArchivo(ActionEvent event){
		//Creamos el entity
		TsccvPdfsProductos datosPDF= new TsccvPdfsProductos();
		datosPDF.setFechaCreacion(new Date());
		TsccvPdfsProductosId pk = new TsccvPdfsProductosId();
		pk.setCodpla(somPlantas.getValue().toString());
		pk.setCodScc(txtCodigoProducto.getValue().toString());
		
		//Asignamos la descripcion de la planta
		TsccvPlanta  pl= new TsccvPlanta();
		pl.setCodpla(somPlantas.getValue().toString());
		//Buscamos la descripcion de la planta
		for(SelectItem si : siPlantas){
			if(si.getValue().toString().equals(somPlantas.getValue().toString()) ){
				pl.setDespla(si.getLabel());
				break;
			}
		}
		datosPDF.setTsccvPlanta(pl);
		
		
		//Creamos el vo que representara el registro
		ArchivoPDFVO arc= new ArchivoPDFVO();
		arc.setNombreArchivo(nombreArchivo);
		arc.setPdf(datosPDF);
		arc.setEstado("Nuevo");
		
		//Adicionamos el nuevo registro a la lista
		listadoPDFs.add(arc);
		
		//Limpiarmos los valores de los campos
		somPlantas.resetValue();
		somPlantas.setSubmittedValue(null);
		txtCodigoProducto.setSubmittedValue(null);
		txtCodigoProducto.resetValue();
		archivo.reset();
		
	}
	*/
	
	//metodo encargado de crear los pdfs en la BD
	public void adicionarPdfs(){
		try{
			if(listadoPDFs == null || listadoPDFs.size() == 0){
				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensaje("Na hay ningun Archivo agregado.", MBMensaje.MENSAJE_ALERTA);
				return;
			}
			
			//se mandan los demas parametros en null ya que toda la informacion necesaria va en cada elemento de la lista 
			BusinessDelegatorView.saveTsccvPdfsProductos(null, null, null, null, listadoPDFs);
			
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("Archivo adicionados de forma satisfactoria.", MBMensaje.MENSAJE_OK);
			
			listadoPDFs.clear();
		}catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
	}
	
	public void eliminarPdfExistente(ActionEvent event){
		try{
			ArchivoPropiedades propiedades = new ArchivoPropiedades();
			ArchivoPDFVO archivoPDF = (ArchivoPDFVO)event.getComponent().getAttributes().get("archivo");
			
			BusinessDelegatorView.deleteTsccvPdfsProductos(archivoPDF.getPdf().getId().getCodScc(), archivoPDF.getPdf().getId().getCodpla());
			listadoPDFs.remove(archivoPDF);
			
			File archivo = new File(propiedades.getProperty("RUTA_PDF")+
					archivoPDF.getPdf().getTsccvPlanta().getDespla().trim()+"\\"+archivoPDF.getPdf().getTsccvPlanta().getCodpla().trim()+
					"-"+archivoPDF.getPdf().getId().getCodScc().trim()+".pdf");
			
			//se valida si elarchivo existe
			if(archivo.exists()){
				archivo.delete();
			}		
			
			//Limpiarmos los valores de los campos
    		somPlantas.resetValue();
    		somPlantas.setSubmittedValue(null);
    		txtCodigoProducto.setSubmittedValue(null);
    		txtCodigoProducto.resetValue();
    		this.archivo.reset();
			
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("Archivo eliminado de forma satisfactoria.", MBMensaje.MENSAJE_OK);
		}catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
	}
	
	//elimina los archivos a nivel depantalla
	public void eliminarPdf(ActionEvent event){
		ArchivoPDFVO archivoPDF = (ArchivoPDFVO)event.getComponent().getAttributes().get("archivo");
		listadoPDFs.remove(archivoPDF);
		
		//Limpiarmos los valores de los campos
		somPlantas.resetValue();
		somPlantas.setSubmittedValue(null);
		txtCodigoProducto.setSubmittedValue(null);
		txtCodigoProducto.resetValue();
		archivo.reset();
	}
}








