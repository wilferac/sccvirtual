package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TsccvEntregasPedidoDAO;
import co.com.smurfit.dataaccess.dao.TsccvProductosPedidoDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ArchivoPDFVO;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;
import com.sun.jndi.toolkit.ctx.Continuation;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class PlanDeEntregaView {
    private boolean renderDataTable;  
    private List<ProductoVO> listaProductosSeleccionados;
    private TsccvPedidos pedido;
    private List<ProductosPedidoVO> listadoProductosPedido;
    private Map listadoEscalasProductos;
    private HtmlInputText numeroEntregas;
    
    //componentes especificos para los productos de molinos
    private Boolean isMolinos;//flag utilizado en la jspx para saber cuando el producto es de molinos
    private HtmlSelectOneMenu somUnidadMedida;
    private List<SelectItem> siUnidadMedida;  

    public PlanDeEntregaView() {        		
		try{	
			//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        
			//se crean los listados
			listadoProductosPedido = new ArrayList<ProductosPedidoVO>();
			
			//si el tipo de producto de la planta del pedido es molinos se crea la lista de unidades de medida 
			if(pedido.getTsccvPlanta().getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
				isMolinos = true;//se coloca el flag en true para indicar que el producto es para molinos
				siUnidadMedida = new ArrayList<SelectItem>();
				
				String listaUmed[] = {"/M2", "/Kg", "/RM", "/RE", "/Un"};
				for(int i=0; i<listaUmed.length; i++){
					SelectItem siUndmed = new SelectItem();
					
					siUndmed.setLabel(listaUmed[i]);
					siUndmed.setValue(listaUmed[i]);
				}
			}
		}catch(Exception e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensajeError(e.getMessage(), "", null);
		}
    }

	public boolean isRenderDataTable() {
		return renderDataTable;
	}

	public void setRenderDataTable(boolean renderDataTable) {
		this.renderDataTable = renderDataTable;		
	}

	public List<ProductoVO> getListaProductosSeleccionados() {
		return listaProductosSeleccionados;
	}

	public void setListaProductosSeleccionados(
			List<ProductoVO> listaProductosSeleccionados) {
		this.listaProductosSeleccionados = listaProductosSeleccionados;
	}	
	
	public List<ProductosPedidoVO> getListadoProductosPedido() {
		return listadoProductosPedido;
	}

	public void setListadoProductosPedido(
			List<ProductosPedidoVO> listadoProductosPedido) {
		this.listadoProductosPedido = listadoProductosPedido;
	}

	public TsccvPedidos getPedido() {
		return pedido;
	}

	public void setPedido(TsccvPedidos pedido) {
		this.pedido = pedido;
	}

	public HtmlInputText getNumeroEntregas() {
		return numeroEntregas;
	}

	public void setNumeroEntregas(HtmlInputText numeroEntregas) {
		this.numeroEntregas = numeroEntregas;
	}
	
	public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }
	
	
	
	public Boolean getIsMolinos() {
		return isMolinos;
	}

	public void setIsMolinos(Boolean isMolinos) {
		this.isMolinos = isMolinos;
	}

	public HtmlSelectOneMenu getSomUnidadMedida() {
		return somUnidadMedida;
	}

	public void setSomUnidadMedida(HtmlSelectOneMenu somUnidadMedida) {
		this.somUnidadMedida = somUnidadMedida;
	}

	public List<SelectItem> getSiUnidadMedida() {
		return siUnidadMedida;
	}

	public void setSiUnidadMedida(List<SelectItem> siUnidadMedida) {
		this.siUnidadMedida = siUnidadMedida;
	}

	
	
	//elimina los productosPedido a nivel de la pantalla
	public void eliminarProdPed(ActionEvent event){
		try{
			ProductosPedidoVO prodPedVO = (ProductosPedidoVO)event.getComponent().getAttributes().get("prodPedVO");			
			BusinessDelegatorView.deleteTsccvProductosPedido(prodPedVO.getTsccvProductosPedido().getIdPrpe());
			listadoProductosPedido.remove(prodPedVO);
			
			//se consulta nuevamente para refrescar la tabla
			consultarProductosPedido();
			
			if(listadoProductosPedido.size() == 0){
				renderDataTable = false;
				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensaje("No hay productos selecciondos para generar el pedido", MBMensaje.MENSAJE_ALERTA);
			}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	public String seguirComprando(){
		try{
			HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			sesion.setAttribute("listaProductosRegistrados", listadoProductosPedido);
			
			//se envia todos los parametros en null ya que toda la informacion va en la lista
			BusinessDelegatorView.updateTsccvProductosPedido(null, null, null, null, null, null, null, null, null, null, null, 
															null, null, null, null, null, listadoProductosPedido);
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
		return "goSeguirComprando";
	}
	
	//metodo en cargado de guardar un pedido nuevo y los productos seleccionados para este
	public void guardarPedido(){
		try{
			//se vuelve a inicializar la lista de productos pedido
			listadoProductosPedido = new ArrayList<ProductosPedidoVO>();
			
			HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			String codPla = sesion.getAttribute("planta").toString();
			
			//se extrae el usuario de la sesion 
			MBUsuario mbUsu = (MBUsuario)FacesUtils.getManagedBean("MBUsuario");
            TsccvUsuario userLogin = mbUsu.getUsuario();
			
            ProductosPedidoVO prodPedVO = null;			
            TsccvProductosPedido prodPed = null;
            
            //se rrecorre la lista de productos seleccionados y se adiciona a la lista de productosPedido
			for(ProductoVO producto : listaProductosSeleccionados){
				prodPedVO = new ProductosPedidoVO();
				prodPed = new TsccvProductosPedido();
				prodPed.setTsccvEntregasPedidos(null);
				prodPedVO.setTsccvProductosPedido(prodPed);
				
				if(producto.getBvpPrecio() != null){
					prodPed.setCodScc(producto.getBvpPrecio().getId().getCodScc());
					prodPed.setCodCli(producto.getBvpPrecio().getCodpro());					
					prodPed.setPrecio(producto.getBvpPrecio().getId().getPrecio());
					prodPed.setDespro(producto.getBvpPrecio().getDespro());
					
					prodPed.setUndmed(producto.getBvpPrecio().getUndmed());
					prodPed.setMoneda(producto.getBvpPrecio().getMoneda());
				}
				else{
					prodPed.setCodCli(producto.getSapPrecio().getId().getCodpro());
					prodPed.setPrecio(producto.getSapPrecio().getId().getPrecio());
					prodPed.setDespro(producto.getSapPrecio().getDespro());
					
					prodPed.setUndmed(producto.getSapPrecio().getUndmed());
					prodPed.setMoneda(producto.getSapPrecio().getMoneda());
					prodPed.setEje(producto.getSapPrecio().getEje());
					prodPed.setDiamtr(producto.getSapPrecio().getDiamtr());
				}
				
				prodPed.setActivo("0");
				prodPed.setCodpla(codPla);
				prodPed.setCantidadTotal(0);
				prodPed.setFechaPrimeraEntrega(null);
				prodPed.setOrdenCompra("");
				
				listadoProductosPedido.add(prodPedVO);
			}
			
			//se guarda el pedido y sus productos
			//NOTA: inicialmente el pedido se guarda como no resgistrado (1)
			BusinessDelegatorView.saveTsccvPedidos("0", new Date(), null, (Integer)sesion.getAttribute("codCliente"),
												   null, "1", codPla, userLogin.getIdUsua(), listadoProductosPedido);
			//se consulta el ultimo pedido creado
			pedido = BusinessDelegatorView.consultarUltimoPedido();
			
			//se refresca la lista de productos
			listadoEscalasProductos = null;
			consultarProductosPedido();
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	//metodo en cargado de adicionar productos a un pedido existente
	public void adicionarProductosPedido(){
		try{
			//se limpia la lista
			listadoProductosPedido = new ArrayList<ProductosPedidoVO>();
			HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			String planta = sesion.getAttribute("planta").toString();
	    	
			ProductosPedidoVO prodPedVO = null;			
            TsccvProductosPedido prodPed = null;
            
            //se rrecorre la lista de productos seleccionados y se adiciona a la lista de productosPedido
			for(ProductoVO producto : listaProductosSeleccionados){
				prodPedVO = new ProductosPedidoVO();
				prodPed = new TsccvProductosPedido();
				prodPed.setTsccvEntregasPedidos(null);
				prodPedVO.setTsccvProductosPedido(prodPed);
				
				if(producto.getBvpPrecio() != null){//productos corrugado o sacos
					prodPed.setCodScc(producto.getBvpPrecio().getId().getCodScc());
					prodPed.setCodCli(producto.getBvpPrecio().getCodpro());					
					prodPed.setPrecio(producto.getBvpPrecio().getId().getPrecio());
					prodPed.setDespro(producto.getBvpPrecio().getDespro());
					
					prodPedVO.getTsccvProductosPedido().setUndmed(producto.getBvpPrecio().getUndmed());					
					prodPedVO.getTsccvProductosPedido().setMoneda(producto.getBvpPrecio().getMoneda());
				}
				else{//productos molinos
					prodPed.setCodCli(producto.getSapPrecio().getId().getCodpro());
					prodPed.setPrecio(producto.getSapPrecio().getId().getPrecio());
					prodPed.setDespro(producto.getSapPrecio().getDespro());
					
					prodPedVO.getTsccvProductosPedido().setUndmed(producto.getSapPrecio().getUndmed());
					prodPedVO.getTsccvProductosPedido().setMoneda(producto.getSapPrecio().getMoneda());
					prodPedVO.getTsccvProductosPedido().setEje(producto.getSapPrecio().getEje());
					prodPedVO.getTsccvProductosPedido().setDiamtr(producto.getSapPrecio().getDiamtr());
				}
				
				prodPed.setActivo("0");
				prodPed.setCodpla(planta);
				prodPed.setCantidadTotal(0);
				prodPed.setFechaCreacion(new Date());
				prodPed.setFechaPrimeraEntrega(null);
				prodPed.setOrdenCompra("");
				prodPed.setTsccvPedidos(pedido);
				
				listadoProductosPedido.add(prodPedVO);
			}
			
			//se obtiene el usuario logueado
			MBUsuario mbUsu = (MBUsuario)FacesUtils.getManagedBean("MBUsuario");
            TsccvUsuario userLogin = mbUsu.getUsuario();
			
			//se actualiza el pedido con los productos adicionados
			BusinessDelegatorView.updateTsccvPedidos("0", pedido.getFechaCreacion(), pedido.getIdPedi(), pedido.getTsccvEmpresas().getIdEmpr(),
					   								pedido.getObservaciones(), pedido.getRegistrado(), pedido.getTsccvPlanta().getCodpla(), 
					   								userLogin.getIdUsua(), listadoProductosPedido);
						
			//se consulta nuevamente para refrescar la tabla
			listadoEscalasProductos = null;
			consultarProductosPedido();
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	//metodo encargado de enviar el producto pedido y llamar el despliegue del panel para especificar las entregas  
	public void definirEntregas(ActionEvent event){
		try{
			//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        
			HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			
			//se obtiene el producto pedido seleccionado
			ProductosPedidoVO prodPedVO = (ProductosPedidoVO)event.getComponent().getAttributes().get("prodPedVO");
	        //se deja en la sesion
			sesion.setAttribute("prodPedVO", prodPedVO);
	        
			//se obtiene el manage bean de entregas pedido
			TsccvEntregasPedidoView tsccvEntregasPedidoView = (TsccvEntregasPedidoView) FacesUtils.getManagedBean("tsccvEntregasPedidoView");

			//se setea el numero de entregas
			tsccvEntregasPedidoView.setNumeroEntregas(prodPedVO.getNumeroEntregas());
			
			//si el tipo de producto de la planta del pedido es corrugado o sacos se setea la escala correspondiente para el producto 
			if(!pedido.getTsccvPlanta().getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
				tsccvEntregasPedidoView.setEscalaProducto((Vector<String>)listadoEscalasProductos.get(prodPedVO.getTsccvProductosPedido().getCodScc()));
			}			
			
			//se despliega el panel
			tsccvEntregasPedidoView.mostrarPanel();					
		}catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	//metodo encargado de consultar los productos pedido
	public void consultarProductosPedido(){
		try{
			//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        
			//se consultan los productos ya seleccionados para el pedido
			listadoProductosPedido = new ArrayList<ProductosPedidoVO>();
			listadoProductosPedido = BusinessDelegatorView.consultarProductosPedido(pedido.getIdPedi());
			
			//si el tipo de producto de la planta del pedido es corrugado o sacos se consultan las escalas de precios 
			if(listadoEscalasProductos == null || listadoEscalasProductos.size() == 0)
				if(!pedido.getTsccvPlanta().getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
	    			listadoEscalasProductos = BusinessDelegatorView.consultarEscalasBvp(listadoProductosPedido, pedido.getTsccvEmpresas().getCodVpe());    			
				}
		}catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
	}
	
	//metodo de redireccionar a la pantalla de confirmacion de orden
	public String aceptar(){
		try{
			//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
			Date fechaActual = new Date();
			boolean error = false;
			//se recorre la lista de productod pedido validando que se haya ingresado la informacion necesaria en cada uno
			for(ProductosPedidoVO prodPedVO : listadoProductosPedido){
				/*if((prodPedVO.getTsccvProductosPedido().getOrdenCompra().trim()).equals("")){
					MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
					mensaje.mostrarMensaje("Hay productos sin Orden de Compra, por favor ingresela o eliminelo para continuar.", MBMensaje.MENSAJE_ALERTA);
					
					return "";
				}*/
				
				/*if(prodPedVO.getTsccvProductosPedido().getCantidadTotal() == 0){
					MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
					mensaje.mostrarMensaje("Hay productos sin Cantidad, por favor ingresela o eliminelo para continuar.", MBMensaje.MENSAJE_ALERTA);
					
					return "";
				}*/
				
				long diff = (prodPedVO.getTsccvProductosPedido().getFechaPrimeraEntrega().getTime() - fechaActual.getTime())/( 1000 * 60 * 60 );
    			
    			if(diff < 24){
    				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
    				mensaje.mostrarMensaje("No se aceptan pedidos con fecha de solicitud menor a 24 horas.", MBMensaje.MENSAJE_ALERTA);
    				error = true;
    				break;
    			}
				
				if(error)
					break;
			}		
			
			if(error)
				return "";
			
			HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			sesion.setAttribute("pedido", pedido);
			
			//se envia todos los parametros en null ya que toda la informacion va en la lista
			BusinessDelegatorView.updateTsccvProductosPedido(null, null, null, null, null, null, null, null, null, null, null, 
															null, null, null, null, null, listadoProductosPedido);
			
			//si producto pedido es para las plantas corrugado o sacos se validan las escalas de los precios para los productos pedido
			if(!pedido.getTsccvPlanta().getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
				String codCliente = sesion.getAttribute("codCliente") != null ? sesion.getAttribute("codCliente").toString() : null;
				listadoProductosPedido = BusinessDelegatorView.consultarPrecioPorCantidadTotal(listadoProductosPedido, codCliente);
			}
			
			return "goConfirmacionDeOrden";
			
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
	}
	
	
	//este metodso es invocado desde la pantalla ingresoOrdenes si el pedido es para una planta con tipo producto molinos
	//y se encarga de cargar la lista de unidades de medida y cambiar el falg (isMolinos) a true para que los componentes en la jspx sean desplegados 
	public void configurarParaMolinos(){
		try{	
			//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();	        			
	        
			//si el tipo de producto de la planta del pedido es molinos se crea la lista de unidades de medida 
			if(pedido.getTsccvPlanta().getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
				isMolinos = true;//se coloca el flag en true para indicar que el producto es para molinos
				siUnidadMedida = new ArrayList<SelectItem>();
				
				String listaUmed[] = {"Seleccionar UM", "/M2", "/Kg", "/RM", "/RE", "/Un"};
				for(int i=0; i<listaUmed.length; i++){
					SelectItem siUndmed = new SelectItem();
					
					if(i == 0){
						siUndmed.setLabel(listaUmed[i]);
						siUndmed.setValue("-");
					}
					else{
						siUndmed.setLabel(listaUmed[i]);
						siUndmed.setValue(listaUmed[i]);
					}
					
					siUnidadMedida.add(siUndmed);
				}
			}
			else{
				isMolinos = false;//se coloca el flag en false para indicar que el producto no es para molinos
			}
		}catch(Exception e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensajeError(e.getMessage(), "", null);
		}
	}
}



