package co.com.smurfit.presentation.backEndBeans;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

import co.com.smurfit.dataaccess.dao.TsccvDetalleGrupoEmpresaDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.utilities.DescargarArchivos;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlCommandLink;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlOutputLink;
import com.icesoft.faces.component.ext.HtmlOutputText;
import com.icesoft.faces.component.ext.HtmlPanelGroup;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.ext.RowSelectorEvent;
import com.icesoft.faces.component.outputresource.OutputResource;
import com.icesoft.faces.context.Resource;

public class ListaPreciosView {
	
	private HtmlInputText txtClaveBusqueda;
	private HtmlSelectOneMenu somClaveBusqueda;
	private List<SelectItem> siClaveBusqueda;
	private HtmlSelectOneMenu somPlanta;
	private List<SelectItem> siPlanta;
	private HtmlInputText txtNombreEmpresa;
	private HtmlInputText txtIdEmpresa;
	
	private List<ProductoVO> listaProductos;	
	private boolean renderDataTableVpeCorrugado;
	private boolean renderDataTableVpeSacos;
	private boolean renderDataTableMas;
	
	public boolean ascendente;//utilizado para el orden por los campos en la tabla
	private boolean renderLupa;
	
	private HtmlPanelGroup panelPdf;
    private HtmlPanelGroup panelXls;
    private HtmlCommandButton btnDescargar;
    private HtmlOutputLink descargaPdf;
    private HtmlOutputLink descargaExcel;
    
	
	public ListaPreciosView() {		
		//Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();     
        //Se crea un objeto del archivo propiedades		
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        
		try{			
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
        	siClaveBusqueda = new ArrayList<SelectItem>();
        	listaProductos = new ArrayList<ProductoVO>();      	
        	txtNombreEmpresa = new HtmlInputText();
        	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
        	txtIdEmpresa = new HtmlInputText();
        	somPlanta = new HtmlSelectOneMenu();
        	siPlanta = new ArrayList<SelectItem>();
        	btnDescargar =  new HtmlCommandButton();        	
        	btnDescargar.setDisabled(true);
        	
			//se crear los listados
			String[] listadoClaBusq = null;
			List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			
			//se cargan los valores de las listas
			listadoClaBusq = getValorParametroClaBusq();
			//listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
			
        	//lista de clave busqueda
        	siClaveBusqueda.add(siNulo);
        	for(int i=0; i<listadoClaBusq.length; i++){
        		if(listadoClaBusq[i] == null || listadoClaBusq[i].equals("")){
        			continue;
        		}
        		
            	SelectItem siClaBusq = new SelectItem();        		
            	siClaBusq.setLabel(listadoClaBusq[i]);
            	siClaBusq.setValue(new Long(i));
        		
            	siClaveBusqueda.add(siClaBusq);
        	}      
        	
            //se valida la empresa del usuario logeado para en caso tal crear la lista de empresas del grupo empresa
        	MBUsuario mbUsu = (MBUsuario)FacesUtils.getManagedBean("MBUsuario");
            TsccvUsuario userLogin = mbUsu.getUsuario();
            
            //se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo se muestra la lupa para la busqueda de la empresa
        		renderLupa = true;
        	}
        	else{//si es una empresa normal se colocan el nombre y el id
        		txtIdEmpresa.setValue(userLogin.getTsccvEmpresas().getIdEmpr());
        		txtNombreEmpresa.setValue(userLogin.getTsccvEmpresas().getNombre());        		
        	}
        	
        	//lista de plantas
        	siPlanta.add(siNulo);
        	if(renderLupa == false){//si la empresa del usuario no es un grupo
    			//si la empresa del usuario tiene cod_mas se consultan las planta con tipo de producto corrugado o sacos
            	if(userLogin.getTsccvEmpresas().getCodVpe() != null || !userLogin.getTsccvEmpresas().getCodVpe().equals("")){
    				listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("vpe", new Long(archivoPropiedades.getProperty("APLICACION"))));
    			}
    			//si la empresa del usuario tiene cod_mas se consultan las planta con tipo de producto molinos
    			if((userLogin.getTsccvEmpresas().getCodMas() != null && !userLogin.getTsccvEmpresas().getCodMas().equals(""))){
    				listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("mas", new Long(archivoPropiedades.getProperty("APLICACION"))));
    			}
    			
        		for(TsccvPlanta objetoPlanta: listadoPlantas){
	        		SelectItem siPlan = new SelectItem();        		
	        		siPlan.setLabel(objetoPlanta.getDespla());
	        		siPlan.setValue(objetoPlanta.getCodpla());
	        		
	        		siPlanta.add(siPlan);
	        	}
			}
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
	}
	
	
	public boolean isRenderDataTableMas() {
		return renderDataTableMas;
	}

	public void setRenderDataTableMas(boolean renderDataTableMas) {
		this.renderDataTableMas = renderDataTableMas;
	}

	public HtmlOutputLink getDescargaPdf() {
		return descargaPdf;
	}


	public void setDescargaPdf(HtmlOutputLink descargaPdf) {
		this.descargaPdf = descargaPdf;
	}


	public HtmlOutputLink getDescargaExcel() {
		return descargaExcel;
	}


	public void setDescargaExcel(HtmlOutputLink descargaExcel) {
		this.descargaExcel = descargaExcel;
	}


	public boolean isRenderDataTableVpeCorrugado() {
		return renderDataTableVpeCorrugado;
	}

	public void setRenderDataTableVpeCorrugado(boolean renderDataTableVpeCorrugado) {
		this.renderDataTableVpeCorrugado = renderDataTableVpeCorrugado;
	}

	public HtmlInputText getTxtClaveBusqueda() {
		return txtClaveBusqueda;
	}

	public void setTxtClaveBusqueda(HtmlInputText txtClaveBusqueda) {
		this.txtClaveBusqueda = txtClaveBusqueda;
	}

	public HtmlSelectOneMenu getSomClaveBusqueda() {
		return somClaveBusqueda;
	}

	public void setSomClaveBusqueda(HtmlSelectOneMenu somClaveBusqueda) {
		this.somClaveBusqueda = somClaveBusqueda;
	}

	public List<SelectItem> getSiClaveBusqueda() {
		return siClaveBusqueda;
	}

	public void setSiClaveBusqueda(List<SelectItem> siClaveBusqueda) {
		this.siClaveBusqueda = siClaveBusqueda;
	}
    
	public HtmlSelectOneMenu getSomPlanta() {
		return somPlanta;
	}

	public void setSomPlanta(HtmlSelectOneMenu somPlanta) {
		this.somPlanta = somPlanta;
	}

	public List<SelectItem> getSiPlanta() {
		return siPlanta;
	}

	public void setSiPlanta(List<SelectItem> siPlanta) {
		this.siPlanta = siPlanta;
	}

	public HtmlInputText getTxtNombreEmpresa() {
		return txtNombreEmpresa;
	}

	public void setTxtNombreEmpresa(HtmlInputText txtNombreEmpresa) {
		this.txtNombreEmpresa = txtNombreEmpresa;
	}

	public boolean isAscendente() {
		return ascendente;
	}

	public void setAscendente(boolean ascendente) {
		this.ascendente = ascendente;
	}

	public boolean isRenderLupa() {
		return renderLupa;
	}

	public void setRenderLupa(boolean renderLupa) {
		this.renderLupa = renderLupa;
	}

	public HtmlInputText getTxtIdEmpresa() {
		return txtIdEmpresa;
	}

	public void setTxtIdEmpresa(HtmlInputText txtIdEmpresa) {
		this.txtIdEmpresa = txtIdEmpresa;
	}

	public List<ProductoVO> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProductoVO> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public void limpiar(ValueChangeEvent event){
		txtClaveBusqueda.setValue(null);
	}

	public boolean isRenderDataTableVpeSacos() {
		return renderDataTableVpeSacos;
	}

	public void setRenderDataTableVpeSacos(boolean renderDataTableVpeSacos) {
		this.renderDataTableVpeSacos = renderDataTableVpeSacos;
	}

	public HtmlPanelGroup getPanelPdf() {
		return panelPdf;
	}

	public void setPanelPdf(HtmlPanelGroup panelPdf) {
		this.panelPdf = panelPdf;
	}

	public HtmlPanelGroup getPanelXls() {
		return panelXls;
	}

	public void setPanelXls(HtmlPanelGroup panelXls) {
		this.panelXls = panelXls;
	}

	public HtmlCommandButton getBtnDescargar() {
		return btnDescargar;
	}

	public void setBtnDescargar(HtmlCommandButton btnDescargar) {
		this.btnDescargar = btnDescargar;
	}
	
	public String limpiar(){
		try{
			//se obtiene el usuario en sesion
        	MBUsuario mbUsu = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
        	TsccvUsuario userLogin = mbUsu.getUsuario();
        	
        	//se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo de empresas se limpian el nombre y el id
    	        ArchivoBundle archivoBundle = new ArchivoBundle();
    	    	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
    	    	txtIdEmpresa.setValue(null);
    	    	
    	    	refrescarListaPlantas();
        	}
        	
	    	somPlanta.setValue(new Long(-1));
	    	somClaveBusqueda.setValue(new Long(-1));
	    	txtClaveBusqueda.setValue(null);
	    	listaProductos.clear();
	    	
	    	panelPdf.setRendered(false);
            panelXls.setRendered(false);
            btnDescargar.setDisabled(true);
	    	
	    	return "";
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
	}
	

	//Metodo encargado de consultar el parametro para el campo claveBusqueda
    public String[] getValorParametroClaBusq(){    	
    	//ARCHIVO PORPIEDADES
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String temp[] = null;
        try {
        	temp = archivoPropiedades.getProperty("LISTA_CLAVE_BUSQUEDA").toString().split(",");
        	
			return temp;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
		return null;           
    }    
	
	public void consultarPrecios(){
		ArchivoPropiedades arc=null;
		String codplaSacosCali=null;
		String codplaCorrugadoCali=null;
		try{
			//se limpia la lista actual
			listaProductos.clear();
			
			Integer codigoCli = obtenerEmpresaUsuario();
			
			ITsccvPlantaLogic logicTsccvPlanta = new TsccvPlantaLogic();
	    	TsccvPlanta tsccvPlanta = logicTsccvPlanta.getTsccvPlanta(somPlanta.getValue().toString());
			
	    	//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        TsccvPlanta plantaConsulta=null;
	    	
	    	//Validamos si la planta no es sacos ya que deberia cambiarse
	    	arc= new ArchivoPropiedades();
	    	codplaSacosCali= arc.getProperty("planta_sacos_cali");
	    	
	    	if(codplaSacosCali.equals(tsccvPlanta.getCodpla())){
	    		codplaCorrugadoCali= arc.getProperty("planta_sacos_cali_reemplaza");
	    		plantaConsulta= BusinessDelegatorView.getTsccvPlanta(codplaCorrugadoCali);
	    		//Pero debemos asignarle el tipo de producto sacos
	    		TsccvTipoProducto tipo= new TsccvTipoProducto();
	    		tipo.setActivo("0");
	    		tipo.setDescripcion("SACOS");
	    		tipo.setIdTipr(new Long(2));
	    		plantaConsulta.setTsccvTipoProducto(tipo);
	    		//Y tambien la descripcion
	    		plantaConsulta.setDespla("SACOS - CALI");
	    	}
	    	else{
	    		plantaConsulta= tsccvPlanta;
	    	}
	        
			List<ProductoVO> listaResuldatoConsulta = BusinessDelegatorView.consultarPrecios(
					(new Long(somClaveBusqueda.getValue().toString()) == 0 ? txtClaveBusqueda.getValue().toString() : null),
					(new Long(somClaveBusqueda.getValue().toString()) == 1 ? txtClaveBusqueda.getValue().toString() : null), 
					codigoCli.toString(), plantaConsulta, FacesUtils.getRequestParameter("nombreColumna"), (ascendente = !ascendente));
						
			if(listaResuldatoConsulta != null && listaResuldatoConsulta.size() > 0){
				btnDescargar.setDisabled(false);
				
				for(ProductoVO producto : listaResuldatoConsulta){
					listaProductos.add(producto);
	    		}
			}
			else{
				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensajeAlerta("No existen productos en la lista de precios. Seleccione otra planta o modifique los par�metros de su b�squeda.");
				listaProductos = new ArrayList<ProductoVO>(0);
				btnDescargar.setDisabled(true);
				return;
			}
    		
    		if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_CORRUGADO")))){//productos corrugado
    			renderDataTableVpeCorrugado = true;
    			renderDataTableVpeSacos = false;
				renderDataTableMas = false;
        	}
    		else if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_SACOS")))){//productos sacos
    			renderDataTableVpeCorrugado = false;
    			renderDataTableVpeSacos = true;
				renderDataTableMas = false;
    		}
        	else if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){//productos molinos
        		renderDataTableVpeCorrugado = false;
    			renderDataTableVpeSacos = false;
				renderDataTableMas = true;
        	}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
	}
	
	public void cambiarPlanta(ValueChangeEvent event){
		try{
			panelPdf.setRendered(false);
            panelXls.setRendered(false);
		} catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	//metodo encargado de generar el reporte
	public void generarReporte(ActionEvent event){
		Connection con=null;
        Map parametrosM;
        String nombreArchivo=null;
        String tipoProdNombreArchivo = null;//se utiliza para completar el nombre del archivo segun el tipo de producto de la planta
        String descargarReporte=null;
        HttpSession session=null;
        ArchivoPropiedades ar=null;
        try {
        	//se inicialiazn los HashMap
        	parametrosM = new HashMap();
          
        	//se obtiene la conexion a la DB
        	con = Utilities.getConnection();

        	ar= new ArchivoPropiedades();
          
        	//variables en las cuales se cargaran los reportes
        	JasperReport reporteMaestro = null;
          
        	//se obtiene la planta seleccionada
        	String codPlanta = new Long(somPlanta.getValue().toString()) != -1 ? somPlanta.getValue().toString() : null;
	  	  	TsccvPlanta tsccvPlanta = null;
	  	  
	  	  	//se obtiene la empresa seleccionada
	  	  	Integer codCli = obtenerEmpresaUsuario();
	  	  	TsccvEmpresas tsccvEmpresas = null;
	      
	  	  	//se consultan la planta y la empresa en la BD
	  	  	tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(codPlanta);
	  	  	tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(codCli);
                              
	  	  	//se llena el HashMap con el primer subreporte y con los valores para la consulta
	  	  	if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(ar.getProperty("PRODUCTO_MOLINOS")))){//productos molinos
		  	  	parametrosM.put("COD_MAS", tsccvEmpresas.getCodMas());
		  	  	parametrosM.put("NOMBRE_EMPR", tsccvEmpresas.getNombre());
		  	  	parametrosM.put("DESPLA", tsccvPlanta.getDespla());
		  	  	parametrosM.put("NOMBRE_TIPPRO", tsccvPlanta.getTsccvTipoProducto().getDescripcion());
	  	  		tipoProdNombreArchivo = "Sap";
	  	  	}
	  	  	else{//corrugado y sacos
	  	  		
	  	  		ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		  	  String codplaSacosCali=null;
		  	  String codplaCorrugadoCali=null;
		  	  TsccvPlanta plantaConsulta=null;

		  	  codplaSacosCali= archivoPropiedades.getProperty("planta_sacos_cali");
	  	      	    	
	  	    	if(codplaSacosCali.equals(tsccvPlanta.getCodpla())){
	  	    		codplaCorrugadoCali= archivoPropiedades.getProperty("planta_sacos_cali_reemplaza");
	  	    		plantaConsulta= BusinessDelegatorView.getTsccvPlanta(codplaCorrugadoCali);
	  	    	}
	  	    	else{
	  	    		plantaConsulta= tsccvPlanta;
	  	    	}
	  	    	
	  	  		parametrosM.put("COD_VPE", tsccvEmpresas.getCodVpe());
	  	  		parametrosM.put("NOMBRE_EMPR", tsccvEmpresas.getNombre());
		  	  	parametrosM.put("COD_PLA", plantaConsulta.getCodpla());
		  	  	parametrosM.put("DESPLA", tsccvPlanta.getDespla());
		  	  	parametrosM.put("TIP_PROD", Utilities.TIPOS_PRODUCTO[Integer.parseInt(tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString())-1]);
		  	  	parametrosM.put("NOMBRE_TIPPRO", tsccvPlanta.getTsccvTipoProducto().getDescripcion());
	  	  		tipoProdNombreArchivo = "Bvp";
	  	  	}            	  
          
	  	  	//Obtenemos la ruta jasper del archivo de propiedades
	  	  	reporteMaestro = (JasperReport)JRLoader.loadObject(ar.getProperty("RUTA_JASPER") + "reporteListaPrecios"+tipoProdNombreArchivo+".jasper");
          
	  	  	//se ejecuta el reporte maestro y el subreporte Maestro
	  	  	JasperPrint jasperPrint = JasperFillManager.fillReport(reporteMaestro,parametrosM,con);
	  	  	nombreArchivo = "reporteListaPrecios"+tipoProdNombreArchivo+new Date().getTime();
	  	  	String archivoPdf=nombreArchivo+".pdf";
	  	  	String archivoXls=nombreArchivo+".xls";
	  	  	descargarReporte = ar.getProperty("RUTA_REPORTES_GENERADOS") + archivoPdf;
          
	  	  	//se exporta el reporte
	  	  	JasperExportManager.exportReportToPdfFile(jasperPrint,descargarReporte);
           
	  	  	OutputStream ouputStream
                = new FileOutputStream(new File(ar.getProperty("RUTA_REPORTES_GENERADOS")+archivoXls));
            ByteArrayOutputStream byteArrayOutputStream
                = new ByteArrayOutputStream();
                
            JRXlsExporter exporterXLS = new JRXlsExporter();
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,
                                     jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
                                     byteArrayOutputStream);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER,Boolean.valueOf(true));
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.valueOf(true));
            
            exporterXLS.exportReport();

            ouputStream.write(byteArrayOutputStream.toByteArray()); 
            ouputStream.flush();
            ouputStream.close();
          
            //se muestra el enlace y se setea el destino
            panelPdf.setRendered(true);
            panelXls.setRendered(true);
            
          //Le asignamos los valores a los enlaces de descarga
  			descargaPdf.setValue(Utilities.RUTA_SERVER+archivoPdf);
  			descargaExcel.setValue(Utilities.RUTA_SERVER+archivoXls);
          
            //Colocamos en la session el nombre del archivo
           /* session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            session.setAttribute(Utilities.LLAVE_NOMBRE_ARCHIVO, nombreArchivo);*/
          
            //Mostramos el mensaje de OK
            MBMensaje mb=(MBMensaje)FacesUtils.getManagedBean("MBMensaje");
            mb.mostrarMensaje("El reporte se ha generado de forma exitosa", MBMensaje.MENSAJE_OK);          
        }catch(Exception e){
        	e.printStackTrace();
        	panelPdf.setRendered(false);
            panelXls.setRendered(false);
            BMBaseException ex = new BMBaseException(e);
            MBMensaje msg= (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
            msg.mostrarMensaje(ex);
        }
        finally{
            //se cierra la conexion a la DB
            try {
                con.close();
            }
            catch (SQLException e) {
                System.out.println("Error cerrando conexion");
            }
        }
	}
	
	
	public Integer obtenerEmpresaUsuario(){
		String codigoCli = null;
		codigoCli = txtIdEmpresa.getValue().toString();
		return !codigoCli.equals("") ? new Integer(codigoCli) : null;
	}
	
	//metodo encargado mostrar el panel para la busqueda y seleccion de la empresa
	public void buscarEmpresa(){
		listaProductos.clear();
		panelPdf.setRendered(false);
        panelXls.setRendered(false);
        btnDescargar.setDisabled(true);
        
		//se obtiene el managebean
		BusquedaEmpresaView busquedaEmpresaView = (BusquedaEmpresaView) FacesUtils.getManagedBean("busquedaEmpresaView");
		
		//se setea el managebean que realiza el llamado this
		busquedaEmpresaView.setManageBeanLlmado(this);
		busquedaEmpresaView.mostrarPanel();
	}
	
	//metodo encargado de setear a los componentes de la pantalla el nombre y el id de la empresa seleccionada 
	public void colocarSeleccion(Integer idEmpr, String nombreEmpr){
		txtIdEmpresa.setValue(idEmpr);
		txtNombreEmpresa.setValue(nombreEmpr);	
		refrescarListaPlantas();
	}
	
	//metodo encargado de colocar las plantas de acuerdo a la empresa seleccionada considarando:
	//1. si la empresa tiene cod_vpe solo se cargan plantas con tipo de producto corrugado o sacos
	//2. si la empresa tiene cod_mas solo se cargan plantas con tipo de producto molinos
	public void refrescarListaPlantas(){
		try{
			//Se crea un objeto del bundle		
	        ArchivoBundle archivoBundle = new ArchivoBundle();
	        //Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        
			List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			//listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
			siPlanta = new ArrayList<SelectItem>();
			
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
	    	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
	    	siNulo.setValue(new Long(-1));
	    	siPlanta.add(siNulo);
	    	
	    	TsccvEmpresas tsccvEmpresas = null;
	    	if(txtIdEmpresa.getValue() != null && !txtIdEmpresa.getValue().equals("")){
	    		tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(new Integer(txtIdEmpresa.getValue().toString()));	    	
			
	    		//se consultan las plantas del cliente
	        	if(tsccvEmpresas.getCodVpe() != null && !tsccvEmpresas.getCodVpe().equals("")){
					listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("vpe", new Long(archivoPropiedades.getProperty("APLICACION"))));
				}
				//si la la empresa del usuario tiene cod_mas elimina las planta con tipo de producto corrugado o sacos
				if((tsccvEmpresas.getCodMas() != null && !tsccvEmpresas.getCodMas().equals(""))){
					listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("mas", new Long(archivoPropiedades.getProperty("APLICACION"))));
				}
	        	
	        	for(TsccvPlanta objetoPlanta: listadoPlantas){	        		
	        		SelectItem siPlan = new SelectItem();        		
	        		siPlan.setLabel(objetoPlanta.getDespla());
	        		siPlan.setValue(objetoPlanta.getCodpla());
	        		
	        		siPlanta.add(siPlan);
				}
	    	}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
}
