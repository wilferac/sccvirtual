package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TsccvDetalleGrupoEmpresaDAO;
import co.com.smurfit.dataaccess.dao.TsccvProductosPedidoDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ArchivoPDFVO;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlInputTextarea;
import com.icesoft.faces.component.ext.HtmlOutputLink;
import com.icesoft.faces.component.ext.HtmlOutputText;
import com.icesoft.faces.component.ext.HtmlPanelGroup;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class EstadoDeCuentaView {

	private List<Vector<Object>> resumenCartera;
	private HtmlInputText txtNombreEmpresa;
	private HtmlInputText txtIdEmpresa;
	private Date fecha;
	private HtmlPanelGroup panelPdf;
    private HtmlPanelGroup panelXls;
    
    private HtmlCommandButton btnDescargar;
    private boolean renderLupa;
    private HtmlOutputLink descargaPdf;
    private HtmlOutputLink descargaExcel;
    
    public EstadoDeCuentaView() {        		
    	//Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();       
		
		try{
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
        	txtNombreEmpresa = new HtmlInputText();
        	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
        	txtIdEmpresa = new HtmlInputText();
        	btnDescargar = new HtmlCommandButton();
			
        	btnDescargar.setDisabled(true);        
        	
        	resumenCartera = new ArrayList<Vector<Object>>();
        	
            //se valida la empresa del usuario logeado para en caso tal crear la lista de empresas del grupo empresa
        	MBUsuario mbUsu = (MBUsuario)FacesUtils.getManagedBean("MBUsuario");
            TsccvUsuario userLogin = mbUsu.getUsuario();
            
            //se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo se muestra la lupa para la busqueda de la empresa
        		renderLupa = true;
        	}
        	else{//si es una empresa normal se colocan el nombre y el id
        		txtIdEmpresa.setValue(userLogin.getTsccvEmpresas().getIdEmpr());
        		txtNombreEmpresa.setValue(userLogin.getTsccvEmpresas().getNombre());        		
        	}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}   
    }

	public List<Vector<Object>> getResumenCartera() {
		return resumenCartera;
	}

	public void setResumenCartera(List<Vector<Object>> resumenCartera) {
		this.resumenCartera = resumenCartera;
	}
	
	public TimeZone getTimeZone(){
    	return TimeZone.getDefault();
    }		

	public HtmlInputText getTxtNombreEmpresa() {
		return txtNombreEmpresa;
	}

	public void setTxtNombreEmpresa(HtmlInputText txtNombreEmpresa) {
		this.txtNombreEmpresa = txtNombreEmpresa;
	}

	public boolean isRenderLupa() {
		return renderLupa;
	}

	public void setRenderLupa(boolean renderLupa) {
		this.renderLupa = renderLupa;
	}

	public HtmlInputText getTxtIdEmpresa() {
		return txtIdEmpresa;
	}

	public void setTxtIdEmpresa(HtmlInputText txtIdEmpresa) {
		this.txtIdEmpresa = txtIdEmpresa;
	}
	
	public Date getFecha() {
		fecha = new Date();
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}	

	public HtmlPanelGroup getPanelPdf() {
		return panelPdf;
	}

	public void setPanelPdf(HtmlPanelGroup panelPdf) {
		this.panelPdf = panelPdf;
	}

	public HtmlPanelGroup getPanelXls() {
		return panelXls;
	}

	public void setPanelXls(HtmlPanelGroup panelXls) {
		this.panelXls = panelXls;
	}

	public HtmlCommandButton getBtnDescargar() {
		return btnDescargar;
	}

	public void setBtnDescargar(HtmlCommandButton btnDescargar) {
		this.btnDescargar = btnDescargar;
	}
	
	
	public String limpiar(){
		try{
			//se obtiene el usuario en sesion
        	MBUsuario mbUsu = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
        	TsccvUsuario userLogin = mbUsu.getUsuario();
        	
        	//se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo de empresas se limpian el nombre y el id
    	        ArchivoBundle archivoBundle = new ArchivoBundle();
    	    	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
    	    	txtIdEmpresa.setValue(null);
        	}
        	
	    	resumenCartera.clear();
	    	btnDescargar.setDisabled(true);
	    	
	    	return "";
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
	}
	

	//metodo encargado consultar el resumen cartera dependiendo de la emprasa seleccionada
	public void consultarCartera(){
		try{
			List<Vector<Object>> resultado = null;
			//se consulta el detalle cartera
			resultado = BusinessDelegatorView.consultarCartera(obtenerEmpresaUsuario());
			
			if(resultado == null){
				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("No se encontraron Facturas Relacionadas", MBMensaje.MENSAJE_ALERTA);
              	return;
			}
			else{
				//por defecto se deshabilita el boton de descarga
				btnDescargar.setDisabled(true);
				//se recorre el resumen cartera para verificar que hayan sumatorias mayores a cero
				for(Vector<Object> resCart : resultado){					
					if(!resCart.get(2).toString().equals("0.0")){//si hay valores mayores a cero se habilita el boton de descarga
						btnDescargar.setDisabled(false);
						break;
					}
				}
				
				resumenCartera = resultado;
			}
			
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}  
	}
	
	
	public Integer obtenerEmpresaUsuario(){
		String codigoCli = null;
		codigoCli = txtIdEmpresa.getValue().toString();
		return !codigoCli.equals("") ? new Integer(codigoCli) : null;
	}
	
	//metodo encargado de redireccionar a la pantalla donde se muestra el detalle de cada resumen cartera
	public String detalleFactura(){
		//se obtiene la sesion y se setea el atributo con el numero del rango
    	HttpSession session= (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    	session.setAttribute("rango", FacesUtils.getRequestParameter("rango"));
    	session.setAttribute("codCli", obtenerEmpresaUsuario());
    	
    	return "goDetalleFactura";
	}
	
	//metodo encargado de generar el reporte con la empresa o cliente seleccionados
	public void generarReporte(ActionEvent ev){		
		Connection con=null;
        Map parametrosM;
        String nombreArchivo=null;
        String descargarReporte=null;
        HttpSession session=null;
        ArchivoPropiedades ar=null;
        
        try {
          //se inicialiazn los HashMap
          parametrosM = new HashMap();
          
          //se obtiene la conexion a la DB
          con = Utilities.getConnection();
          
          //variables en las cuales se cargaran los reportes
          JasperReport reporteMaestro = null;                   
          
          Integer codCli = obtenerEmpresaUsuario();
          TsccvEmpresas tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(codCli);
          
          
          //se llena el HashMap con el primer subreporte y con los valores para la consulta
          parametrosM.put("COD_SAP", tsccvEmpresas.getCodSap());
          parametrosM.put("NOMBRE_EMPR", tsccvEmpresas.getNombre());
          
          //Obtenemos la ruta jasper del archivo de propiedades
          ar= new ArchivoPropiedades();
          
          reporteMaestro = (JasperReport)JRLoader.loadObject(ar.getProperty("RUTA_JASPER") + "reporteResumenCartera.jasper");
          
          //se ejecuta el reporte maestro y el subreporte Maestro
          JasperPrint jasperPrint = JasperFillManager.fillReport(reporteMaestro,parametrosM,con);
          nombreArchivo = "reporteResumenCartera"+new Date().getTime();
          String archivoPdf=nombreArchivo+".pdf";
          String archivoXls=nombreArchivo+".xls";
          descargarReporte = ar.getProperty("RUTA_REPORTES_GENERADOS") + archivoPdf;
          
          //se exporta el reporte
          JasperExportManager.exportReportToPdfFile(jasperPrint,descargarReporte);
           
          OutputStream ouputStream
                = new FileOutputStream(new File(ar.getProperty("RUTA_REPORTES_GENERADOS")+archivoXls));
            ByteArrayOutputStream byteArrayOutputStream
                = new ByteArrayOutputStream();
                
            JRXlsExporter exporterXLS = new JRXlsExporter();
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,
                                     jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
                                     byteArrayOutputStream);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER,Boolean.valueOf(true));
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.valueOf(true));
            
            exporterXLS.exportReport();

            ouputStream.write(byteArrayOutputStream.toByteArray()); 
            ouputStream.flush();
            ouputStream.close();
          
          //se muestra el enlace y se setea el destino
          panelPdf.setRendered(true);
          panelXls.setRendered(true);
          
        //Le asignamos los valores a los enlaces de descarga
			descargaPdf.setValue(Utilities.RUTA_SERVER+archivoPdf);
			descargaExcel.setValue(Utilities.RUTA_SERVER+archivoXls);
          
          //Colocamos en la session el nombre del archivo
          /*session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
          session.setAttribute(Utilities.LLAVE_NOMBRE_ARCHIVO, nombreArchivo);*/          
          
          //Mostramos el mensaje de OK
          MBMensaje mb=(MBMensaje)FacesUtils.getManagedBean("MBMensaje");
          mb.mostrarMensaje("El reporte se ha generado de forma exitosa", MBMensaje.MENSAJE_OK);
          
        }catch(Exception e){
        	e.printStackTrace();
        	panelPdf.setRendered(false);
            panelXls.setRendered(false);
          BMBaseException ex = new BMBaseException(e);
          MBMensaje msg= (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
          msg.mostrarMensaje(ex);
        }
        finally{
            //se cierra la conexion a la DB
            try {
                con.close();
            }
            catch (SQLException e) {
                System.out.println("Error cerrando conexion");
            }
        }
	}
	
	//metodo encargado de reestablecer la pantalla cuando el usuario cambie de empresa
	public void cambiaEmpresa(ValueChangeEvent event){
		resumenCartera.clear();
		btnDescargar.setDisabled(true);
		panelPdf.setRendered(false);
		panelXls.setRendered(false);
	}
	
	//metodo encargado mostrar el panel para la busqueda y seleccion de la empresa
	public void buscarEmpresa(){
		resumenCartera.clear();
		btnDescargar.setDisabled(true);
		//se obtiene el managebean
		BusquedaEmpresaView busquedaEmpresaView = (BusquedaEmpresaView) FacesUtils.getManagedBean("busquedaEmpresaView");
		
		//se setea el managebean que realiza el llamado this
		busquedaEmpresaView.setManageBeanLlmado(this);
		busquedaEmpresaView.mostrarPanel();
	}
	
	//metodo encargado de setear a los componentes de la pantalla el nombre y el id de la empresa seleccionada 
	public void colocarSeleccion(Integer idEmpr, String nombreEmpr){
		txtIdEmpresa.setValue(idEmpr);
		txtNombreEmpresa.setValue(nombreEmpr);
		
	}

	public HtmlOutputLink getDescargaPdf() {
		return descargaPdf;
	}

	public void setDescargaPdf(HtmlOutputLink descargaPdf) {
		this.descargaPdf = descargaPdf;
	}

	public HtmlOutputLink getDescargaExcel() {
		return descargaExcel;
	}

	public void setDescargaExcel(HtmlOutputLink descargaExcel) {
		this.descargaExcel = descargaExcel;
	}
}



