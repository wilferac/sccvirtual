package co.com.smurfit.presentation.backEndBeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.FactoryFinder;
import javax.faces.application.ApplicationFactory;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.RowSelectorEvent;
import com.lowagie.tools.concat_pdf;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.vo.DetalleGrupoEmpresaVO;
import co.com.smurfit.utilities.FacesUtils;

public class ArmarGrupoEmpresasView {
    private HtmlInputText txtCodMasBusq;
    private HtmlInputText txtCodSapBusq;
    private HtmlInputText txtCodVpeBusq;
	private HtmlInputText txtNombreBusq;
	private boolean visible;
    private String managedBeanDestino;
    private List<DetalleGrupoEmpresaVO> listadoDetGrupEmpresasVO;
    private List<DetalleGrupoEmpresaVO> listadoDetGrupEmpresasVOSeleccionadas;
    private Object manageBeanLlmado;//contiene el managebean que realizo el llamado
    
    public ArmarGrupoEmpresasView() {
    	visible = false;
    	
    	listadoDetGrupEmpresasVO = new ArrayList<DetalleGrupoEmpresaVO>();
    	listadoDetGrupEmpresasVOSeleccionadas = new ArrayList<DetalleGrupoEmpresaVO>();
    	txtNombreBusq = new HtmlInputText();
    	txtCodMasBusq = new HtmlInputText();
    	txtCodSapBusq = new HtmlInputText();
    	txtCodVpeBusq = new HtmlInputText();
    	
    	//buscarEmpresa();
	}
    
    
    
	
	public HtmlInputText getTxtCodMasBusq() {
		return txtCodMasBusq;
	}

	public void setTxtCodMasBusq(HtmlInputText txtCodMasBusq) {
		this.txtCodMasBusq = txtCodMasBusq;
	}

	public HtmlInputText getTxtCodSapBusq() {
		return txtCodSapBusq;
	}

	public void setTxtCodSapBusq(HtmlInputText txtCodSapBusq) {
		this.txtCodSapBusq = txtCodSapBusq;
	}

	public HtmlInputText getTxtCodVpeBusq() {
		return txtCodVpeBusq;
	}

	public void setTxtCodVpeBusq(HtmlInputText txtCodVpeBusq) {
		this.txtCodVpeBusq = txtCodVpeBusq;
	}

	public HtmlInputText getTxtNombreBusq() {
		return txtNombreBusq;
	}

	public void setTxtNombreBusq(HtmlInputText txtNombreBusq) {
		this.txtNombreBusq = txtNombreBusq;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getManagedBeanDestino() {
		return managedBeanDestino;
	}

	public void setManagedBeanDestino(String managedBeanDestino) {
		this.managedBeanDestino = managedBeanDestino;
	}

	public List<DetalleGrupoEmpresaVO> getListadoDetGrupEmpresasVO() {
		return listadoDetGrupEmpresasVO;
	}

	public void setListadoDetGrupEmpresasVO(
			List<DetalleGrupoEmpresaVO> listadoDetGrupEmpresasVO) {
		this.listadoDetGrupEmpresasVO = listadoDetGrupEmpresasVO;
	}

	public List<DetalleGrupoEmpresaVO> getListadoDetGrupEmpresasVOSeleccionadas() {
		return listadoDetGrupEmpresasVOSeleccionadas;
	}

	public void setListadoDetGrupEmpresasVOSeleccionadas(
			List<DetalleGrupoEmpresaVO> listadoDetGrupEmpresasVOSeleccionadas) {
		this.listadoDetGrupEmpresasVOSeleccionadas = listadoDetGrupEmpresasVOSeleccionadas;
	}

	public Object getManageBeanLlmado() {
		return manageBeanLlmado;
	}

	public void setManageBeanLlmado(Object manageBeanLlmado) {
		this.manageBeanLlmado = manageBeanLlmado;
	}
	
	
	//metodo encargado de limpiar el campo de ingreso
	public void limpiar(){
		txtNombreBusq.setValue(null);
		txtCodSapBusq.setValue(null);
		txtCodMasBusq.setValue(null);
		txtCodVpeBusq.setValue(null);
		
		listadoDetGrupEmpresasVO.clear();
	}
	
    //metodo encargado de desplegar el panel
    public void mostrarPanel(){
    	this.visible = true;
    }

	public String ocultarPanel() {
		txtNombreBusq.setValue(null);
		txtCodSapBusq.setValue(null);
		txtCodMasBusq.setValue(null);
		txtCodVpeBusq.setValue(null);
		
    	String gui = "";
		Object objReturn = null;
		visible = false;
		String strDestino = this.getManagedBeanDestino();
		if (strDestino != null && strDestino.length() > 0) {
			MethodBinding metodo = getMethodBinding(strDestino, null);
			objReturn = metodo.invoke(FacesContext.getCurrentInstance(), null);
			if (objReturn != null) {
				gui = (String) objReturn;
			}
		}
		//FacesContext.getCurrentInstance().notifyAll();
		
		return gui;	
	}
    
    public static MethodBinding getMethodBinding(String bindingName,
			Class params[]) {
		ApplicationFactory factory = (ApplicationFactory) FactoryFinder
				.getFactory(FactoryFinder.APPLICATION_FACTORY);
		MethodBinding binding = factory.getApplication().createMethodBinding(
				bindingName, params);
		return binding;
	}

    
    public void rowSelectionListener(RowSelectorEvent event) {
    	//se lismpia la lista de seleccionados
    	//listadoDetGrupEmpresasVOSeleccionadas.clear();
		
    	Map idsDetalleSeleccionados = new HashMap(0);        		
		//se crea un hashMap con las empresas que estan seleccionadas para facilitar la validacion de si ya esta en la lista de seleccionadas
		for(DetalleGrupoEmpresaVO objetoDetalleGrupoEmpresaVO : listadoDetGrupEmpresasVOSeleccionadas){
			idsDetalleSeleccionados.put(objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr(), 
					objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr());
		}
    	
        for(DetalleGrupoEmpresaVO objetoDetGrupEmpresaVO : listadoDetGrupEmpresasVO){
    		//si la empresa no esta seleccionada se elimina de la lista de seleccionadas
        	if(!objetoDetGrupEmpresaVO.getSeleccionada()){
    			objetoDetGrupEmpresaVO.setSeleccionada(false);
    			eliminarEmpresa(objetoDetGrupEmpresaVO);
    			continue;
    		}
        	
        	//si la empresa esta seleccionada y no esta en la lista de seleccionadas se adiciona
        	else if(objetoDetGrupEmpresaVO.getSeleccionada() && !idsDetalleSeleccionados.containsKey(objetoDetGrupEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr())){
    			listadoDetGrupEmpresasVOSeleccionadas.add(objetoDetGrupEmpresaVO);
    		}
    	}
        
        //colocarSeleccion();
    }
    
    
    //metodo encargado de consultar las empresas por el nombre
    public void buscarEmpresa() {
    	List<TsccvEmpresas> registros=null;
    	try {  		    
    		listadoDetGrupEmpresasVO.clear();
    		
            registros = BusinessDelegatorView.buscarEmpresa(
                    (((txtCodMasBusq.getValue()) == null) ||
                    (txtCodMasBusq.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodMasBusq.getValue().toString()),
                    (((txtCodSapBusq.getValue()) == null) ||
                    (txtCodSapBusq.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodSapBusq.getValue().toString()),
                    (((txtCodVpeBusq.getValue()) == null) ||
                    (txtCodVpeBusq.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodVpeBusq.getValue().toString()),
                    (((txtNombreBusq.getValue()) == null) ||
                    (txtNombreBusq.getValue()).equals("")) ? null
                                                       : new String(
        		    txtNombreBusq.getValue().toString()).toUpperCase(), null, new Object());
			
			if(registros==null || registros.size()==0){
				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	          	mensaje.mostrarMensaje("No se encontraron registros", MBMensaje.MENSAJE_ALERTA);				
				return;
			}
			
			//se agrega el ciclo para elimiar las emmpresas que son grupos
			for(TsccvEmpresas objetoEmpresa: registros){        		
        		TsccvDetalleGrupoEmpresa detalleGrupEmpresa = new TsccvDetalleGrupoEmpresa();
        		detalleGrupEmpresa.setTsccvEmpresas(objetoEmpresa);
        		DetalleGrupoEmpresaVO detalleVO = new DetalleGrupoEmpresaVO();	    		
	    		detalleVO.setTsccvDetalleGrupoEmpresa(detalleGrupEmpresa);
        		
	    		listadoDetGrupEmpresasVO.add(detalleVO);
        	}			
			
			marcarSeleccionadas();
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    }
    
    
    public void desmarcarTodas(){
    	for(DetalleGrupoEmpresaVO objetoDetGrupEmpresaVO : listadoDetGrupEmpresasVO){
    		objetoDetGrupEmpresaVO.setSeleccionada(false);
    	}
    }
    
    //metodo encargado de eliminar la empresa de la lista de seleccionadas
    public void eliminarEmpresa(DetalleGrupoEmpresaVO empresaEli){
    	List<DetalleGrupoEmpresaVO> listadoSeleccionadasCopia = new ArrayList<DetalleGrupoEmpresaVO>(listadoDetGrupEmpresasVOSeleccionadas);
    	listadoDetGrupEmpresasVOSeleccionadas.clear();
    	
    	for(DetalleGrupoEmpresaVO objetoDetGrupEmpresaVO : listadoSeleccionadasCopia){    		
    		if(!objetoDetGrupEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr().equals(empresaEli.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr())){
    			listadoDetGrupEmpresasVOSeleccionadas.add(objetoDetGrupEmpresaVO);
    		}
    	}
    }
    
    //metodo encargado de seleccionar en la lista general de empresas las empresas que ya hacen parte del grupo 
    public void marcarSeleccionadas(){
    	for(DetalleGrupoEmpresaVO objetoDetGrupEmpresaVO : listadoDetGrupEmpresasVO){
    		for(DetalleGrupoEmpresaVO objetoDetGrupEmpresaVO2 : listadoDetGrupEmpresasVOSeleccionadas){	    		
	    		if(objetoDetGrupEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr().equals(objetoDetGrupEmpresaVO2.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr())){
	    			objetoDetGrupEmpresaVO.setSeleccionada(true);
	    		}
    		}
    	}
    }
}
