package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TsccvDetalleGrupoEmpresaDAO;
import co.com.smurfit.dataaccess.dao.TsccvProductosPedidoDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ArchivoPDFVO;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlInputTextarea;
import com.icesoft.faces.component.ext.HtmlOutputLink;
import com.icesoft.faces.component.ext.HtmlOutputText;
import com.icesoft.faces.component.ext.HtmlPanelGroup;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class EstadisticasDeCompraView {
	private HtmlSelectOneMenu somPlanta;
	private List<SelectItem> siPlanta;
	private HtmlInputText txtNombreEmpresa;
	private HtmlInputText txtIdEmpresa;
	private SelectInputDate txtPeriodo;
	
	private String styleDataTableBvp;
    private String styleDataTableSap;
    private boolean renderNoHayDespachos;
    private String minPeriodo;
    private HtmlCommandButton btnDescargar;
    private boolean renderLupa;
    
    private HtmlPanelGroup panelPdf;
    private HtmlPanelGroup panelXls;
    private HtmlOutputLink descargaPdf;
    private HtmlOutputLink descargaExcel;
    
    public EstadisticasDeCompraView() {        		
    	//Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle(); 

        //Se crea un objeto del archivo propiedades		
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();        
		
        styleDataTableBvp = "display:none";
        styleDataTableSap = "display:none";
        
		try{
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
        	txtNombreEmpresa = new HtmlInputText();
        	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
        	txtIdEmpresa = new HtmlInputText();
        	siPlanta = new ArrayList<SelectItem>();
        	btnDescargar =  new HtmlCommandButton();
        	
        	btnDescargar.setDisabled(true);
			
			//se crear los listados
			List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			
			//se cargan los valores de las listas
			//listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));			
			        	
			
        	//se obtiene el usuario en sesion
        	MBUsuario mbUsu = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
        	TsccvUsuario userLogin = mbUsu.getUsuario();
        	
        	//se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo se muestra la lupa para la busqueda de la empresa
        		renderLupa = true;
        	}
        	else{//si es una empresa normal se colocan el nombre y el id
        		txtIdEmpresa.setValue(userLogin.getTsccvEmpresas().getIdEmpr());
        		txtNombreEmpresa.setValue(userLogin.getTsccvEmpresas().getNombre());        		
        	}   
        	
        	//lista de plantas
        	siPlanta.add(siNulo);
        	if(renderLupa == false){//si la empresa del usuario no es un grupo
        		//si la empresa del usuario tiene cod_mas se consultan las planta con tipo de producto corrugado o sacos
            	if(userLogin.getTsccvEmpresas().getCodVpe() != null || !userLogin.getTsccvEmpresas().getCodVpe().equals("")){
    				listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("vpe", new Long(archivoPropiedades.getProperty("APLICACION"))));
    			}
    			//si la empresa del usuario tiene cod_mas se consultan las planta con tipo de producto molinos
    			if((userLogin.getTsccvEmpresas().getCodMas() != null && !userLogin.getTsccvEmpresas().getCodMas().equals(""))){
    				listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("mas", new Long(archivoPropiedades.getProperty("APLICACION"))));
    			}
    			
	        	for(TsccvPlanta objetoPlanta: listadoPlantas){	        		
	        		SelectItem siPlan = new SelectItem();        		
	        		siPlan.setLabel(objetoPlanta.getDespla());
	        		siPlan.setValue(objetoPlanta.getCodpla());
	        		
	        		siPlanta.add(siPlan);
	        	}
			}
        	
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}   
    }		
	
	public HtmlSelectOneMenu getSomPlanta() {
		return somPlanta;
	}

	public HtmlOutputLink getDescargaPdf() {
		return descargaPdf;
	}

	public void setDescargaPdf(HtmlOutputLink descargaPdf) {
		this.descargaPdf = descargaPdf;
	}

	public HtmlOutputLink getDescargaExcel() {
		return descargaExcel;
	}

	public void setDescargaExcel(HtmlOutputLink descargaExcel) {
		this.descargaExcel = descargaExcel;
	}

	public void setSomPlanta(HtmlSelectOneMenu somPlanta) {
		this.somPlanta = somPlanta;
	}

	public List<SelectItem> getSiPlanta() {
		return siPlanta;
	}

	public void setSiPlanta(List<SelectItem> siPlanta) {
		this.siPlanta = siPlanta;
	}	
	
	public HtmlInputText getTxtNombreEmpresa() {
		return txtNombreEmpresa;
	}

	public void setTxtNombreEmpresa(HtmlInputText txtNombreEmpresa) {
		this.txtNombreEmpresa = txtNombreEmpresa;
	}

	public HtmlInputText getTxtIdEmpresa() {
		return txtIdEmpresa;
	}

	public void setTxtIdEmpresa(HtmlInputText txtIdEmpresa) {
		this.txtIdEmpresa = txtIdEmpresa;
	}

	public boolean isRenderLupa() {
		return renderLupa;
	}

	public void setRenderLupa(boolean renderLupa) {
		this.renderLupa = renderLupa;
	}

	public SelectInputDate getTxtPeriodo() {
		return txtPeriodo;
	}

	public void setTxtPeriodo(SelectInputDate txtPeriodo) {
		this.txtPeriodo = txtPeriodo;
	}

	public String getStyleDataTableBvp() {
		return styleDataTableBvp;
	}

	public void setStyleDataTableBvp(String styleDataTableBvp) {
		this.styleDataTableBvp = styleDataTableBvp;
	}

	public String getStyleDataTableSap() {
		return styleDataTableSap;
	}

	public void setStyleDataTableSap(String styleDataTableSap) {
		this.styleDataTableSap = styleDataTableSap;
	}	

	public boolean isRenderNoHayDespachos() {
		return renderNoHayDespachos;
	}

	public void setRenderNoHayDespachos(boolean renderNoHayDespachos) {
		this.renderNoHayDespachos = renderNoHayDespachos;
	}

	public String getMinPeriodo() {
		return minPeriodo;
	}

	public void setMinPeriodo(String minPeriodo) {
		this.minPeriodo = minPeriodo;
	}

	public HtmlCommandButton getBtnDescargar() {
		return btnDescargar;
	}

	public void setBtnDescargar(HtmlCommandButton btnDescargar) {
		this.btnDescargar = btnDescargar;
	}

	public HtmlPanelGroup getPanelPdf() {
		return panelPdf;
	}

	public void setPanelPdf(HtmlPanelGroup panelPdf) {
		this.panelPdf = panelPdf;
	}

	public HtmlPanelGroup getPanelXls() {
		return panelXls;
	}

	public void setPanelXls(HtmlPanelGroup panelXls) {
		this.panelXls = panelXls;
	}

	public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }
	
	
	public String limpiar(){
		try{
			//se obtiene el usuario en sesion
        	MBUsuario mbUsu = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
        	TsccvUsuario userLogin = mbUsu.getUsuario();
        	
        	//se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo de empresas se limpian el nombre y el id
    	        ArchivoBundle archivoBundle = new ArchivoBundle();
    	    	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
    	    	txtIdEmpresa.setValue(null);
    	    	
    	    	refrescarListaPlantas();
        	}
        	
	    	somPlanta.setValue(new Long(-1));
	    	txtPeriodo.setValue(null);
	    	
	    	//se limpia la lista de BvpHistDespachos 
			BvpHistDespachosView bvpHistDespachosView = (BvpHistDespachosView) FacesUtils.getManagedBean("bvpHistDespachosView");
			bvpHistDespachosView.setBvpHistDespachos(new ArrayList<BvpHistDespachos>(0));

			//se limpia la lista de BvpHistDespachos SapHistDespachos
			SapHistDespachosView sapHistDespachosView = (SapHistDespachosView) FacesUtils.getManagedBean("sapHistDespachosView");
			sapHistDespachosView.setSapHistDespachos(new ArrayList<SapHistDespachos>(0));
	    	
			panelPdf.setRendered(false);
            panelXls.setRendered(false);
            btnDescargar.setDisabled(true);
			
	    	return "";
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
	}
	
	//metodo agregado para consultar los pedidos de acuerdo a la informacion ingresada
    public void consultarHistoricoDespacho(){
    	try {
	    	//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        String codplaSacosCali=null;
			String codplaCorrugadoCali=null;
			TsccvPlanta plantaConsulta=null;
    		
    		String codPlanta = new Long(somPlanta.getValue().toString()) != -1 ? somPlanta.getValue().toString() : null;
    		TsccvPlanta tsccvPlanta = null;
    		
    		Integer codCli = obtenerEmpresaUsuario();
    		TsccvEmpresas tsccvEmpresas = null;
    		
    		if(codPlanta != null && codCli != null && obtenerPeriodoSeleccionado() != null){
    			tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(codPlanta);
    			tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(codCli);
    			
    			codplaSacosCali= archivoPropiedades.getProperty("planta_sacos_cali");
    	    	
    	    	if(codplaSacosCali.equals(tsccvPlanta.getCodpla())){
    	    		codplaCorrugadoCali= archivoPropiedades.getProperty("planta_sacos_cali_reemplaza");
    	    		plantaConsulta= BusinessDelegatorView.getTsccvPlanta(codplaCorrugadoCali);
    	    		//Pero debemos asignarle el tipo de producto sacos
    	    		TsccvTipoProducto tipo= new TsccvTipoProducto();
    	    		tipo.setActivo("0");
    	    		tipo.setDescripcion("SACOS");
    	    		tipo.setIdTipr(new Long(2));
    	    		plantaConsulta.setTsccvTipoProducto(tipo);
    	    	}
    	    	else{
    	    		plantaConsulta= tsccvPlanta;
    	    	}
    			
    			if (tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(
						new Long(archivoPropiedades
								.getProperty("PRODUCTO_CORRUGADO")))
						|| tsccvPlanta
								.getTsccvTipoProducto()
								.getIdTipr()
								.equals(
										new Long(archivoPropiedades
												.getProperty("PRODUCTO_SACOS")))) {//productos corrugado
    	        	//se consulta el primer despacho del los producos tipo corrugado  
    				//minPeriodo = BusinessDelegatorView.minPeriodo(tsccvEmpresas.getCodVpe(), tsccvPlanta);
    				minPeriodo = BusinessDelegatorView.minPeriodo(tsccvEmpresas.getCodVpe(), plantaConsulta);
    				
    				BvpHistDespachosView bvpHistDespachosView = (BvpHistDespachosView) FacesUtils.getManagedBean("bvpHistDespachosView");
        			
    				bvpHistDespachosView.setTsccvEmpresas(tsccvEmpresas);
    				//bvpHistDespachosView.setTsccvPlanta(tsccvPlanta);
    				bvpHistDespachosView.setTsccvPlanta(plantaConsulta);
    				bvpHistDespachosView.setPeriodo(obtenerPeriodoSeleccionado());
    				
    				renderNoHayDespachos = bvpHistDespachosView.consultarHistDespachosBvp();
    				
    				styleDataTableBvp = "display:block";
    				if(!renderNoHayDespachos){//si no hay despachos se oculta la tabla
    					styleDataTableBvp = "display:none";
    					MBMensaje mensaje=(MBMensaje)FacesUtils.getManagedBean("MBMensaje");
    					mensaje.mostrarMensaje("No existen despachos para el mes solicitado", MBMensaje.MENSAJE_ALERTA); 
    				}
    				styleDataTableSap = "display:none";
            	}
            	else{//productos molinos
            		SapHistDespachosView sapHistDespachosView = (SapHistDespachosView) FacesUtils.getManagedBean("sapHistDespachosView");
    	        	//se consulta el primer despacho del los producos tipo molinos
    	        	minPeriodo = BusinessDelegatorView.minPeriodo(tsccvEmpresas.getCodMas());
    	        	
            		sapHistDespachosView.setTsccvEmpresas(tsccvEmpresas);
            		sapHistDespachosView.setPeriodo(obtenerPeriodoSeleccionado());
            		
            		renderNoHayDespachos = sapHistDespachosView.consultarHistDespachosSap();
    				
            		styleDataTableSap = "display:block";
            		if(!renderNoHayDespachos){//si no hay despachos se oculta la tabla
            			styleDataTableSap = "display:none";
            			MBMensaje mensaje=(MBMensaje)FacesUtils.getManagedBean("MBMensaje");
            			mensaje.mostrarMensaje("No existen despachos para el mes solicitado", MBMensaje.MENSAJE_ALERTA);   
    				}
            		styleDataTableBvp = "display:none";
            	}
    		}    
    		//se le asigna al render del boton descargar el retorno del metodo de consulta
			btnDescargar.setDisabled(!renderNoHayDespachos);
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    	
    	return;
    }
	
    //metodo encargado de obtener la empresa que el usuario tiene seleccionada dependiendo si es un grupo o no
	public Integer obtenerEmpresaUsuario(){
		String codigoCli = null;
		codigoCli = txtIdEmpresa.getValue().toString();
		return !codigoCli.equals("") ? new Integer(codigoCli) : null;
	}	
	
	//metodo encargado de permitir el desplazamiento en tre los meses (primer despacho y ultimo despacho)
	public void cambiaPeriodo(ValueChangeEvent event){
		//se ocultan los paneles para los iconos de descarga de reportes
		panelPdf.setRendered(false);
        panelXls.setRendered(false);
        
        if(new Long(somPlanta.getValue().toString()) != -1 && obtenerEmpresaUsuario() != null){
        	consultarHistoricoDespacho();
        }
	}
	
	//metodo encargado de consultar el historico despacho cada vez que se cambie la planta o la empresa seleccionada  
	/*public void cambiaPlantaOEmpresa(ValueChangeEvent event){
		try{
			//se ocultan las tablas
			styleDataTableBvp = "display:none";
	        styleDataTableSap = "display:none";
	        
	        //se ocultan los paneles para los iconos de descarga de reportes
			panelPdf.setRendered(false);
	        panelXls.setRendered(false);           
						
			String codPlanta = new Long(somPlanta.getValue().toString()) != -1 ? somPlanta.getValue().toString() : null;
    		TsccvPlanta tsccvPlanta = null;
    		
    		Integer codCli = obtenerEmpresaUsuario();
    		TsccvEmpresas tsccvEmpresas = null;
    		
    		if(codPlanta != null && codCli != null){
    			tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(codPlanta);
    			tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(codCli);
    					
    			btnDescargar.setStyle("display:block");
    			
    			//Se crea un objeto del archivo propiedades		
    	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
    	        if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_CORRUGADO")))){//productos corrugado
    	        	//se consulta el primer despacho del los producos tipo corrugado  
    	        	minPeriodo = BusinessDelegatorView.minPeriodo(tsccvEmpresas.getCodVpe(), tsccvPlanta);
    	        }
    	        else{
    	        	//se consulta el primer despacho del los producos tipo molinos
    	        	minPeriodo = BusinessDelegatorView.minPeriodo(tsccvEmpresas.getCodMas());
    	        }
    		}
    		consultarHistoricoDespacho();
			
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}*/
	
	//metodo encargado de formar el perido
	public String obtenerPeriodoSeleccionado(){
		if(txtPeriodo.getValue() != null){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
			String periodoSel = sdf.format(((Date)txtPeriodo.getValue())).replace("-", "");
			return periodoSel;
		}
		return null;
	}
	
	
	public void generarReporte(ActionEvent event){
		Connection con=null;
        Map parametrosM;
        String nombreArchivo=null;
        String tipoProdNombreArchivo = null;//se utiliza para completar el nombre del archivo segun el tipo de producto de la planta
        String descargarReporte=null;
        HttpSession session=null;
        ArchivoPropiedades ar=null;
        try {
        	//se inicialiazn los HashMap
        	parametrosM = new HashMap();
          
        	//se obtiene la conexion a la DB
        	con = Utilities.getConnection();

        	ar= new ArchivoPropiedades();
          
        	//variables en las cuales se cargaran los reportes
        	JasperReport reporteMaestro = null;
          
        	//se obtiene la planta seleccionada
        	String codPlanta = new Long(somPlanta.getValue().toString()) != -1 ? somPlanta.getValue().toString() : null;
	  	  	TsccvPlanta tsccvPlanta = null;
	  	  
	  	  	//se obtiene la empresa seleccionada
	  	  	Integer codCli = obtenerEmpresaUsuario();
	  	  	TsccvEmpresas tsccvEmpresas = null;
	      
	  	  	//se consultan la planta y la empresa en la BD
	  	  	tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(codPlanta);
	  	  	tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(codCli);
                              
	  	  	//se llena el HashMap con el primer subreporte y con los valores para la consulta
	  	  	if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(ar.getProperty("PRODUCTO_MOLINOS")))){//productos molinos
		  	  	parametrosM.put("COD_MAS", tsccvEmpresas.getCodMas());
	  	  		tipoProdNombreArchivo = "Sap";
	  	  	}
	  	  	else{//corrugado y sacos
	  	  		ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		  	  String codplaSacosCali=null;
		  	  String codplaCorrugadoCali=null;
		  	  TsccvPlanta plantaConsulta=null;

		  	  codplaSacosCali= archivoPropiedades.getProperty("planta_sacos_cali");
	  	      	    	
	  	    	if(codplaSacosCali.equals(tsccvPlanta.getCodpla())){
	  	    		codplaCorrugadoCali= archivoPropiedades.getProperty("planta_sacos_cali_reemplaza");
	  	    		plantaConsulta= BusinessDelegatorView.getTsccvPlanta(codplaCorrugadoCali);
	  	    	}
	  	    	else{
	  	    		plantaConsulta= tsccvPlanta;
	  	    	}
	  	  		
	  	  		parametrosM.put("COD_VPE", tsccvEmpresas.getCodVpe());
	  	  		parametrosM.put("COD_PLA", plantaConsulta.getCodpla());
	  	  		parametrosM.put("TIPO_PROD", Utilities.TIPOS_PRODUCTO[Integer.parseInt(tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString())-1]);
	  	  		tipoProdNombreArchivo = "Bvp";	  	  		
	  	  	}
          
	  	  	parametrosM.put("NUMMES", obtenerPeriodoSeleccionado());
	  	  	parametrosM.put("DESPLA", tsccvPlanta.getDespla());
	  	  	parametrosM.put("NOMBRE_EMPR", tsccvEmpresas.getNombre());
	  	  	parametrosM.put("PERIODO", ((Date)txtPeriodo.getValue()));
          
	  	  	//Obtenemos la ruta jasper del archivo de propiedades
	  	  	reporteMaestro = (JasperReport)JRLoader.loadObject(ar.getProperty("RUTA_JASPER") + "estadisticasDeCompra"+tipoProdNombreArchivo+".jasper");
          
	  	  	//se ejecuta el reporte maestro y el subreporte Maestro
	  	  	JasperPrint jasperPrint = JasperFillManager.fillReport(reporteMaestro,parametrosM,con);
	  	  	nombreArchivo = "estadisticasDeCompra"+tipoProdNombreArchivo+new Date().getTime();
	  	  	String archivoPdf=nombreArchivo+".pdf";
	  	  	String archivoXls=nombreArchivo+".xls";
	  	  	descargarReporte = ar.getProperty("RUTA_REPORTES_GENERADOS") + archivoPdf;
          
	  	  	//se exporta el reporte
	  	  	JasperExportManager.exportReportToPdfFile(jasperPrint,descargarReporte);
           
	  	  	OutputStream ouputStream
                = new FileOutputStream(new File(ar.getProperty("RUTA_REPORTES_GENERADOS")+archivoXls));
            ByteArrayOutputStream byteArrayOutputStream
                = new ByteArrayOutputStream();
                
            JRXlsExporter exporterXLS = new JRXlsExporter();
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,
                                     jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
                                     byteArrayOutputStream);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER,Boolean.valueOf(true));
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.valueOf(true));
            
            exporterXLS.exportReport();

            ouputStream.write(byteArrayOutputStream.toByteArray()); 
            ouputStream.flush();
            ouputStream.close();
          
            //se muestra el enlace y se setea el destino
            panelPdf.setRendered(true);
            panelXls.setRendered(true);
            
          //Le asignamos los valores a los enlaces de descarga
  			descargaPdf.setValue(Utilities.RUTA_SERVER+archivoPdf);
  			descargaExcel.setValue(Utilities.RUTA_SERVER+archivoXls);
          
            //Colocamos en la session el nombre del archivo
            /*session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            session.setAttribute(Utilities.LLAVE_NOMBRE_ARCHIVO, nombreArchivo);*/
          
            //Mostramos el mensaje de OK
            MBMensaje mb=(MBMensaje)FacesUtils.getManagedBean("MBMensaje");
            mb.mostrarMensaje("El reporte se ha generado de forma exitosa", MBMensaje.MENSAJE_OK);          
        }catch(Exception e){
        	e.printStackTrace();
        	panelPdf.setRendered(false);
            panelXls.setRendered(false);
            BMBaseException ex = new BMBaseException(e);
            MBMensaje msg= (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
            msg.mostrarMensaje(ex);
        }
        finally{
            //se cierra la conexion a la DB
            try {
                con.close();
            }
            catch (SQLException e) {
                System.out.println("Error cerrando conexion");
            }
        }
	}
	
	
	//metodo encargado mostrar el panel para la busqueda y seleccion de la empresa
	public void buscarEmpresa(){
		//se limpia la lista de BvpHistDespachos 
		BvpHistDespachosView bvpHistDespachosView = (BvpHistDespachosView) FacesUtils.getManagedBean("bvpHistDespachosView");
		bvpHistDespachosView.setBvpHistDespachos(new ArrayList<BvpHistDespachos>(0));

		//se limpia la lista de BvpHistDespachos SapHistDespachos
		SapHistDespachosView sapHistDespachosView = (SapHistDespachosView) FacesUtils.getManagedBean("sapHistDespachosView");
		sapHistDespachosView.setSapHistDespachos(new ArrayList<SapHistDespachos>(0));
		
		//se limpia el calendario
		txtPeriodo.resetValue();
		
		//se obtiene el managebean
		BusquedaEmpresaView busquedaEmpresaView = (BusquedaEmpresaView) FacesUtils.getManagedBean("busquedaEmpresaView");
		
		//se setea el managebean que realiza el llamado this
		busquedaEmpresaView.setManageBeanLlmado(this);
		busquedaEmpresaView.mostrarPanel();
	}
	
	//metodo encargado de setear a los componentes de la pantalla el nombre y el id de la empresa seleccionada 
	public void colocarSeleccion(Integer idEmpr, String nombreEmpr){
		txtIdEmpresa.setValue(idEmpr);
		txtNombreEmpresa.setValue(nombreEmpr);
		refrescarListaPlantas();
	}
	
	//metodo encargado de colocar las plantas de acuerdo a la empresa seleccionada considarando:
	//1. si la empresa tiene cod_vpe solo se cargan plantas con tipo de producto corrugado o sacos
	//2. si la empresa tiene cod_mas solo se cargan plantas con tipo de producto molinos
	public void refrescarListaPlantas(){
		try{
			//Se crea un objeto del bundle		
	        ArchivoBundle archivoBundle = new ArchivoBundle();
	        //Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        
			List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			//listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
			siPlanta = new ArrayList<SelectItem>();
			
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
	    	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
	    	siNulo.setValue(new Long(-1));
	    	siPlanta.add(siNulo);
	    	
	    	TsccvEmpresas tsccvEmpresas = null;
	    	if(txtIdEmpresa.getValue() != null && !txtIdEmpresa.getValue().equals("")){
	    		tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(new Integer(txtIdEmpresa.getValue().toString()));	    	
			
	    		//se consultan las plantas del cliente
	        	if(tsccvEmpresas.getCodVpe() != null && !tsccvEmpresas.getCodVpe().equals("")){
					listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("vpe", new Long(archivoPropiedades.getProperty("APLICACION"))));
				}
				//si la la empresa del usuario tiene cod_mas elimina las planta con tipo de producto corrugado o sacos
				if((tsccvEmpresas.getCodMas() != null && !tsccvEmpresas.getCodMas().equals(""))){
					listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("mas", new Long(archivoPropiedades.getProperty("APLICACION"))));
				}
	        	
	        	for(TsccvPlanta objetoPlanta: listadoPlantas){	        		
	        		SelectItem siPlan = new SelectItem();        		
	        		siPlan.setLabel(objetoPlanta.getDespla());
	        		siPlan.setValue(objetoPlanta.getCodpla());
	        		
	        		siPlanta.add(siPlan);
				}
	    	}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
}



