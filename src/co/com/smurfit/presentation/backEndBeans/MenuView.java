package co.com.smurfit.presentation.backEndBeans;

import java.util.ArrayList;
import java.util.List;

import co.com.smurfit.dataaccess.dao.TbmBaseOpcionesGruposDAO;
import co.com.smurfit.dataaccess.dao.TbmBaseSubopcionesOpcDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.utilities.FacesUtils;

public class MenuView {
	private List<TbmBaseOpcionesGrupos> opcionesUsuario;
	
	public MenuView() {
		armarMenu();
	}
	
	public void armarMenu(){
		try{
			opcionesUsuario = new ArrayList<TbmBaseOpcionesGrupos>();
			
			MBUsuario mbUsuario = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
			TsccvUsuario tsccvUsuario = mbUsuario.getUsuario();
			
			System.out.println("Inicia consulta");
			List<TbmBaseOpcionesGrupos> listadoOpciGrup = BusinessDelegatorView.findByPropertyOpcionesGrupos(TbmBaseOpcionesGruposDAO.GRUPO, tsccvUsuario.getTbmBaseGruposUsuarios().getIdGrup());
			System.out.println("Termina consulta");
			
			
			for(TbmBaseOpcionesGrupos opciGrup : listadoOpciGrup){
				if(opciGrup.getTbmBaseOpciones().getActivo().equals("1")){
					continue;
				}
				
				if(opciGrup.getTbmBaseOpciones().getUrl() == null){
					opciGrup.getTbmBaseOpciones().setUrl("");
				}
				
				System.out.println("Inicia consulta");
				List<TbmBaseSubopcionesOpc> listadoSubopci = BusinessDelegatorView.findByPropertySubopcionesOpc(TbmBaseSubopcionesOpcDAO.OPCION, opciGrup.getTbmBaseOpciones().getIdOpci());
				System.out.println("Termina consulta");
				
				//List<TbmBaseSubopcionesOpc> listadoSubopci2 = new ArrayList<TbmBaseSubopcionesOpc>(0);
				
				//se pasa el listado de sub. opciones opciones de un list a un set para setearselo a la opcion
				/*for(TbmBaseSubopcionesOpc subopci : listadoSubopci1){
					listadoSubopci2.add(indice,subopci);
					indice++;
				}*/
				
				opciGrup.getTbmBaseOpciones().setTbmBaseSubopcionesOpcs(listadoSubopci);
				opcionesUsuario.add(opciGrup);
			}
			
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
	}

	public List<TbmBaseOpcionesGrupos> getOpcionesUsuario() {
		return opcionesUsuario;
	}

	public void setOpcionesUsuario(List<TbmBaseOpcionesGrupos> opcionesUsuario) {
		this.opcionesUsuario = opcionesUsuario;
	}

}
