package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TbmBaseOpcionesDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TbmBaseOpcionesView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtActivo;
    private HtmlInputText txtDescripcion;
    private HtmlInputText txtImagenIcono;
    private HtmlInputText txtNombre;
    private HtmlInputText txtUrl;
    private HtmlInputText txtIdOpci;
    private SelectInputDate txtFechaCreacion;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnInactivar;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TbmBaseOpciones> tbmBaseOpciones;
    private List<TbmBaseOpcionesDTO> tbmBaseOpcionesDTO;
    
    //listas desplegables
    private HtmlSelectOneMenu somActivo;
    private List<SelectItem> siActivo;
    private String[] valorParametroActivo;

    public TbmBaseOpcionesView() {
        super("");
        
        //Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();
		
		try{
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
			siActivo = new ArrayList<SelectItem>();
			
			//se crear los listados
			String valorParametroActivo[] = null;
			
			//se cargan los valores de las listas
			valorParametroActivo =  getValorParametroAct();
            
			//lista de activo			
        	siActivo.add(siNulo);        	
            for(int i=0; i<valorParametroActivo.length; i++){
        		if(valorParametroActivo[i] == null || valorParametroActivo[i].equals("")){
        			continue;
        		}
            	SelectItem siAct = new SelectItem();        		
        		siAct.setLabel(valorParametroActivo[i]);
        		siAct.setValue(new Long(i));
        		
        		siActivo.add(siAct);
        	}
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
    }

    public String action_clear() {
        txtActivo.setValue(null);
        txtActivo.setDisabled(false);
        somActivo.setValue(new Long(-1));
        
        txtDescripcion.setValue(null);
        txtDescripcion.setDisabled(false);
        txtImagenIcono.setValue(null);
        txtImagenIcono.setDisabled(false);
        txtNombre.setValue(null);
        txtNombre.setDisabled(false);
        txtUrl.setValue(null);
        txtUrl.setDisabled(false);

        txtFechaCreacion.setValue(null);
        txtFechaCreacion.setDisabled(false);

        txtIdOpci.setValue(null);
        txtIdOpci.setDisabled(false);

        btnSave.setDisabled(false);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnInactivar.setDisabled(true);
        btnClear.setDisabled(false);
        
        onePageDataModel = null;
        flag = true;
        getData();

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TbmBaseOpciones entity = null;

            try {
                Long idOpci = new Long(txtIdOpci.getValue().toString());

                entity = BusinessDelegatorView.getTbmBaseOpciones(idOpci);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtActivo.setDisabled(false);
                txtDescripcion.setDisabled(false);
                txtImagenIcono.setDisabled(false);
                txtNombre.setDisabled(false);
                txtUrl.setDisabled(false);

                txtFechaCreacion.setDisabled(false);

                txtIdOpci.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnInactivar.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtActivo.setValue(entity.getActivo());
                txtActivo.setDisabled(false);
                somActivo.setValue(entity.getActivo());
                
                txtDescripcion.setValue(entity.getDescripcion());
                txtDescripcion.setDisabled(false);
                txtFechaCreacion.setValue(entity.getFechaCreacion());
                txtFechaCreacion.setDisabled(false);
                txtImagenIcono.setValue(entity.getImagenIcono());
                txtImagenIcono.setDisabled(false);
                txtNombre.setValue(entity.getNombre());
                txtNombre.setDisabled(false);
                txtUrl.setValue(entity.getUrl());
                txtUrl.setDisabled(false);

                txtIdOpci.setValue(entity.getIdOpci());
                txtIdOpci.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnInactivar.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
        	txtActivo.setValue(new Long(somActivo.getValue().toString()) != -1 ? somActivo.getValue() : null);
        	txtFechaCreacion.setValue(new Date());
        	
        	if(txtNombre.getValue() != null && !txtNombre.getValue().equals("")){
        		if(BusinessDelegatorView.opcionIsRegistrada(txtNombre.getValue().toString().toUpperCase())){
        			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
                  	mensaje.mostrarMensaje("La Opci�n ya se encuentra registrado", MBMensaje.MENSAJE_ALERTA);
                  	
                  	return "";
        		}
        	}
        	        	
            BusinessDelegatorView.saveTbmBaseOpciones((((txtActivo.getValue()) == null) ||
                (txtActivo.getValue()).equals("")) ? null
                                                   : new String(
                    txtActivo.getValue().toString()),
                (((txtDescripcion.getValue()) == null) ||
                (txtDescripcion.getValue()).equals("")) ? null
                                                        : new String(
                    txtDescripcion.getValue().toString()).toUpperCase(),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdOpci.getValue()) == null) ||
                (txtIdOpci.getValue()).equals("")) ? null
                                                   : new Long(
                    txtIdOpci.getValue().toString()),
                (((txtImagenIcono.getValue()) == null) ||
                (txtImagenIcono.getValue()).equals("")) ? null
                                                        : new String(
                    txtImagenIcono.getValue().toString()),
                (((txtNombre.getValue()) == null) ||
                (txtNombre.getValue()).equals("")) ? null
                                                   : new String(
                    txtNombre.getValue().toString()).toUpperCase(),
                (((txtUrl.getValue()) == null) ||
                (txtUrl.getValue()).equals("")) ? null
                                                : new String(
                    txtUrl.getValue().toString()));

            /*FacesContext.getCurrentInstance()
            .addMessage("", new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));*/
            
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("La Opci�n fue adicionada de forma satisfactoria", MBMensaje.MENSAJE_OK);
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTbmBaseOpciones((((txtIdOpci.getValue()) == null) ||
                (txtIdOpci.getValue()).equals("")) ? null
                                                   : new Long(
                    txtIdOpci.getValue().toString()));

            /*FacesContext.getCurrentInstance()
            .addMessage("", new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));*/
            
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("La Opci�n fue eliminado de forma satisfactoria", MBMensaje.MENSAJE_OK);
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
        	txtActivo.setValue(new Long(somActivo.getValue().toString()) != -1 ? somActivo.getValue() : null);
            //se asigna a la fecha un new Date()
        	txtFechaCreacion.setValue(new Date());
        	
            BusinessDelegatorView.updateTbmBaseOpciones((((txtActivo.getValue()) == null) ||
                    (txtActivo.getValue()).equals("")) ? null
                            : new String(
					txtActivo.getValue().toString()),
					(((txtDescripcion.getValue()) == null) ||
					(txtDescripcion.getValue()).equals("")) ? null
					                                 : new String(
					txtDescripcion.getValue().toString()).toUpperCase(),
					(((txtFechaCreacion.getValue()) == null) ||
					(txtFechaCreacion.getValue()).equals("")) ? null
					                                   : (Date) txtFechaCreacion.getValue(),
					(((txtIdOpci.getValue()) == null) ||
					(txtIdOpci.getValue()).equals("")) ? null
					                            : new Long(
					txtIdOpci.getValue().toString()),
					(((txtImagenIcono.getValue()) == null) ||
					(txtImagenIcono.getValue()).equals("")) ? null
					                                 : new String(
					txtImagenIcono.getValue().toString()),
					(((txtNombre.getValue()) == null) ||
					(txtNombre.getValue()).equals("")) ? null
					                            : new String(
					txtNombre.getValue().toString()).toUpperCase(),
					(((txtUrl.getValue()) == null) ||
					(txtUrl.getValue()).equals("")) ? null
					                         : new String(
					txtUrl.getValue().toString()));

            /*FacesContext.getCurrentInstance()
            .addMessage("", new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));*/
            
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("La Opci�n fue modificada de forma satisfactoria", MBMensaje.MENSAJE_OK);
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, String descripcion,
        Date fechaCreacion, Long idOpci, String imagenIcono, String nombre,
        String url) throws Exception {
        try {
            BusinessDelegatorView.updateTbmBaseOpciones(activo, descripcion,
                fechaCreacion, idOpci, imagenIcono, nombre, url);
            renderManager.getOnDemandRenderer("TbmBaseOpcionesView")
                         .requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TbmBaseOpcionesView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TbmBaseOpciones> getTbmBaseOpciones() {
        if (flag) {
            try {
                tbmBaseOpciones = BusinessDelegatorView.getTbmBaseOpciones();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tbmBaseOpciones;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTbmBaseOpciones(List<TbmBaseOpciones> tbmBaseOpciones) {
        this.tbmBaseOpciones = tbmBaseOpciones;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTbmBaseOpciones() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TbmBaseOpciones> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTbmBaseOpciones = 0;

        try {
            totalNumberTbmBaseOpciones = BusinessDelegatorView.findTotalNumberTbmBaseOpciones()
                                                              .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTbmBaseOpciones) {
            endIndex = totalNumberTbmBaseOpciones;
        }

        try {
            tbmBaseOpciones = BusinessDelegatorView.findPageTbmBaseOpciones(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TbmBaseOpciones>(totalNumberTbmBaseOpciones,
            startRow, tbmBaseOpciones);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TbmBaseOpcionesDTO> getDataPageDTO(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTbmBaseOpciones = 0;

        try {
            totalNumberTbmBaseOpciones = BusinessDelegatorView.findTotalNumberTbmBaseOpciones()
                                                              .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTbmBaseOpciones) {
            endIndex = totalNumberTbmBaseOpciones;
        }

        try {
            tbmBaseOpciones = BusinessDelegatorView.findPageTbmBaseOpciones(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tbmBaseOpciones)) {
                tbmBaseOpcionesDTO = new ArrayList<TbmBaseOpcionesDTO>();

                for (TbmBaseOpciones tbmBaseOpcionesTmp : tbmBaseOpciones) {
                    TbmBaseOpcionesDTO tbmBaseOpcionesDTO2 = new TbmBaseOpcionesDTO();
                    tbmBaseOpcionesDTO2.setIdOpci(tbmBaseOpcionesTmp.getIdOpci()
                                                                    .toString());

                    tbmBaseOpcionesDTO2.setActivo((tbmBaseOpcionesTmp.getActivo() != null)
                        ? tbmBaseOpcionesTmp.getActivo().toString() : null);
                    tbmBaseOpcionesDTO2.setDescripcion((tbmBaseOpcionesTmp.getDescripcion() != null)
                        ? tbmBaseOpcionesTmp.getDescripcion().toString() : null);
                    tbmBaseOpcionesDTO2.setFechaCreacion(tbmBaseOpcionesTmp.getFechaCreacion());
                    tbmBaseOpcionesDTO2.setImagenIcono((tbmBaseOpcionesTmp.getImagenIcono() != null)
                        ? tbmBaseOpcionesTmp.getImagenIcono().toString() : null);
                    tbmBaseOpcionesDTO2.setNombre((tbmBaseOpcionesTmp.getNombre() != null)
                        ? tbmBaseOpcionesTmp.getNombre().toString() : null);
                    tbmBaseOpcionesDTO2.setUrl((tbmBaseOpcionesTmp.getUrl() != null)
                        ? tbmBaseOpcionesTmp.getUrl().toString() : null);
                    tbmBaseOpcionesDTO2.setTbmBaseOpciones(tbmBaseOpcionesTmp);
                    tbmBaseOpcionesDTO2.setTbmBaseOpcionesView(this);
                    tbmBaseOpcionesDTO.add(tbmBaseOpcionesDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TbmBaseOpcionesDTO>(totalNumberTbmBaseOpciones,
            startRow, tbmBaseOpcionesDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tbmBaseOpcionesDTO)) {
            for (TbmBaseOpcionesDTO tbmBaseOpcionesTmp : tbmBaseOpcionesDTO) {
                renderManager.getOnDemandRenderer("TbmBaseOpcionesView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tbmBaseOpcionesDTO)) {
            for (TbmBaseOpcionesDTO tbmBaseOpcionesTmp : tbmBaseOpcionesDTO) {
                renderManager.getOnDemandRenderer("TbmBaseOpcionesView")
                             .add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(HtmlInputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public HtmlInputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(HtmlInputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public HtmlInputText getTxtImagenIcono() {
        return txtImagenIcono;
    }

    public void setTxtImagenIcono(HtmlInputText txtImagenIcono) {
        this.txtImagenIcono = txtImagenIcono;
    }

    public HtmlInputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(HtmlInputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public HtmlInputText getTxtUrl() {
        return txtUrl;
    }

    public void setTxtUrl(HtmlInputText txtUrl) {
        this.txtUrl = txtUrl;
    }

    public SelectInputDate getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(SelectInputDate txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public HtmlInputText getTxtIdOpci() {
        return txtIdOpci;
    }

    public void setTxtIdOpci(HtmlInputText txtIdOpci) {
        this.txtIdOpci = txtIdOpci;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnInactivar() {
		return btnInactivar;
	}

	public void setBtnInactivar(HtmlCommandButton btnInactivar) {
		this.btnInactivar = btnInactivar;
	}

	public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TbmBaseOpcionesDTO> getTbmBaseOpcionesDTO() {
        return tbmBaseOpcionesDTO;
    }

    public void setTbmBaseOpcionesDTO(
        List<TbmBaseOpcionesDTO> tbmBaseOpcionesDTO) {
        this.tbmBaseOpcionesDTO = tbmBaseOpcionesDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    
    

	public HtmlSelectOneMenu getSomActivo() {
		return somActivo;
	}

	public void setSomActivo(HtmlSelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}

	public List<SelectItem> getSiActivo() {
		return siActivo;
	}

	public void setSiActivo(List<SelectItem> siActivo) {
		this.siActivo = siActivo;
	}

	public String[] getValorParametroActivo() {
		return valorParametroActivo;
	}

	public void setValorParametroActivo(String[] valorParametroActivo) {
		this.valorParametroActivo = valorParametroActivo;
	}
    
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TbmBaseOpciones> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TbmBaseOpcionesDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
    
    
    //Metodo encargado de consultar el parametro para el campo activo
    public String[] getValorParametroAct(){    	
    	//ARCHIVO PORPIEDADES
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String temp[] = null;
        try {
        	temp = archivoPropiedades.getProperty("LISTA_ACTIVO").toString().split(",");
        	
        	if(temp != null){
        		valorParametroActivo = new String[temp.length+1];  
	        	for(int i=0; i<temp.length; i++){
	        		valorParametroActivo[i] = temp[i];
	        	}
        	}
			return valorParametroActivo;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
		return null;           
    }

    
    //consulta las opciones por tolos los campos
    public String consultarOpciones(){
    	List<TbmBaseOpciones> registros=null;    	
    	
    	try {
        	//ANTES DE GUARDAR SE ASIGNA EL VALOR DE LA LISTA SELECCIONADO AL CAMPO DE TEXTO RESPECTIVO
    		txtActivo.setValue(new Long(somActivo.getValue().toString()) != -1 ? somActivo.getValue() : null);       	        
        	
            registros = BusinessDelegatorView.consultarOpciones((((txtActivo.getValue()) == null) ||
                    (txtActivo.getValue()).equals("")) ? null
                            : new String(
					txtActivo.getValue().toString()),
					(((txtDescripcion.getValue()) == null) ||
					(txtDescripcion.getValue()).equals("")) ? null
					                                 : new String(
					txtDescripcion.getValue().toString()).toUpperCase(),
					(((txtFechaCreacion.getValue()) == null) ||
					(txtFechaCreacion.getValue()).equals("")) ? null
					                                   : (Date) txtFechaCreacion.getValue(),
					(((txtIdOpci.getValue()) == null) ||
					(txtIdOpci.getValue()).equals("")) ? null
					                            : new Long(
					txtIdOpci.getValue().toString()),
					(((txtImagenIcono.getValue()) == null) ||
					(txtImagenIcono.getValue()).equals("")) ? null
					                                 : new String(
					txtImagenIcono.getValue().toString()),
					(((txtNombre.getValue()) == null) ||
					(txtNombre.getValue()).equals("")) ? null
					                            : new String(
					txtNombre.getValue().toString()).toUpperCase(),
					(((txtUrl.getValue()) == null) ||
					(txtUrl.getValue()).equals("")) ? null
					                         : new String(
					txtUrl.getValue().toString()));
            
            if(registros == null || registros.size() == 0){            	
            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("No se encontraron registros", MBMensaje.MENSAJE_ALERTA);
            	
              	onePageDataModel.setDirtyData(false);
              	flag = false;
              	onePageDataModel.setPage(new DataPage<TbmBaseOpciones>(0, 0, registros));
              	return"";
            }
            
            onePageDataModel.setDirtyData(false);
            flag = true;
            onePageDataModel.setPage(new DataPage<TbmBaseOpciones>(registros.size(), 0, registros));
            
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}
    	
    	return "";
    }
    
    
    //SE AGREGA EL METODO ASOCIADO AL ENLACE EN LA TABLA 
    public void find_txtId(ActionEvent event) {
    	Long idOpci = new Long(FacesUtils.getRequestParameter("txtIdOpci").toString());
    	find(idOpci);
    }
    
    //METODO DE BUSQUEDA.
    public void find (Long idOpci){
    	TbmBaseOpciones entity = null;
    	
        try {
            entity = BusinessDelegatorView.getTbmBaseOpciones(idOpci);
        } catch (Exception e) {
            // TODO: handle exception
        }
        
        if (entity == null) {
            txtActivo.setDisabled(false);
            txtDescripcion.setDisabled(false);
            txtImagenIcono.setDisabled(false);
            txtNombre.setDisabled(false);
            txtUrl.setDisabled(false);

            txtFechaCreacion.setDisabled(false);

            txtIdOpci.setDisabled(false);

            btnSave.setDisabled(false);
            btnDelete.setDisabled(true);
            btnModify.setDisabled(true);
            btnInactivar.setDisabled(true);
            btnClear.setDisabled(false);
        } else {
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            somActivo.setValue(entity.getActivo());
            
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtImagenIcono.setValue(entity.getImagenIcono());
            txtImagenIcono.setDisabled(false);
            txtNombre.setValue(entity.getNombre());
            txtNombre.setDisabled(false);
            txtUrl.setValue(entity.getUrl());
            txtUrl.setDisabled(false);

            txtIdOpci.setValue(entity.getIdOpci());
            txtIdOpci.setDisabled(true);

            btnSave.setDisabled(true);
            btnDelete.setDisabled(false);
            btnModify.setDisabled(false);
            btnInactivar.setDisabled(false);
            btnClear.setDisabled(false);
        }
    }
    
    
    public String inactivarOpcion(){
    	try {
			TbmBaseOpciones tbmBaseOpciones = BusinessDelegatorView.getTbmBaseOpciones(new Long(txtIdOpci.getValue().toString()));
			
			if((tbmBaseOpciones != null)){
				if(!tbmBaseOpciones.getActivo().equals("1")){
					BusinessDelegatorView.inactivarOpcion(tbmBaseOpciones.getIdOpci());
				}
				else{
					MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	              	mensaje.mostrarMensaje("La Opci�n ya esta inactiva", MBMensaje.MENSAJE_ALERTA);
	              	return "";
				}
			}
			
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("La Opci�n fue inactivada de forma satisfactoria", MBMensaje.MENSAJE_OK);
						
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
		
		action_clear();
		
    	return "";
    }
}
