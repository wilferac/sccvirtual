package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.dto.SapDetPedDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.servlet.http.HttpSession;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class SapDetPedView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtNumped;
    private HtmlInputText txtItmped;
    private HtmlInputText txtSubitm;
    private HtmlInputText txtCodcli;
    private HtmlInputText txtDespro;
    private HtmlInputText txtCntped;
    private HtmlInputText txtUndmed;
    private HtmlInputText txtFecsol;
    private HtmlInputText txtFecdsp;
    private HtmlInputText txtCodpro;
    private HtmlInputText txtEstado;
    private HtmlInputText txtOrdcom;
    private HtmlInputText txtCntdsp;
    private HtmlInputText txtCntkgs;
    private HtmlInputText txtArollos;
    private HtmlInputText txtAhojas;
    private HtmlInputText txtLhojas;
    private HtmlInputText txtArollo;
    private SelectInputDate txtFechaInicial;
    private SelectInputDate txtFechaFinal;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<SapDetPed> sapDetPed;
    private List<SapDetPedDTO> sapDetPedDTO;
    
    private TsccvEmpresas tsccvEmpresas;
    private String estadoPedido;//filtroH
    
    private boolean ascendente;//utilizado para el orden por los campos en la tabla

    public SapDetPedView() {
        super("");
    }

    public String action_clear() {
        txtNumped.setValue(null);
        txtNumped.setDisabled(false);
        txtItmped.setValue(null);
        txtItmped.setDisabled(false);
        txtSubitm.setValue(null);
        txtSubitm.setDisabled(false);
        txtCodcli.setValue(null);
        txtCodcli.setDisabled(false);
        txtDespro.setValue(null);
        txtDespro.setDisabled(false);
        txtCntped.setValue(null);
        txtCntped.setDisabled(false);
        txtUndmed.setValue(null);
        txtUndmed.setDisabled(false);
        txtFecsol.setValue(null);
        txtFecsol.setDisabled(false);
        txtFecdsp.setValue(null);
        txtFecdsp.setDisabled(false);
        txtCodpro.setValue(null);
        txtCodpro.setDisabled(false);
        txtEstado.setValue(null);
        txtEstado.setDisabled(false);
        txtOrdcom.setValue(null);
        txtOrdcom.setDisabled(false);
        txtCntdsp.setValue(null);
        txtCntdsp.setDisabled(false);
        txtCntkgs.setValue(null);
        txtCntkgs.setDisabled(false);
        txtArollos.setValue(null);
        txtArollos.setDisabled(false);
        txtAhojas.setValue(null);
        txtAhojas.setDisabled(false);
        txtLhojas.setValue(null);
        txtLhojas.setDisabled(false);
        txtArollo.setValue(null);
        txtArollo.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            SapDetPed entity = null;

            try {
                SapDetPedId id = new SapDetPedId();
                id.setNumped((((txtNumped.getValue()) == null) ||
                    (txtNumped.getValue()).equals("")) ? null
                                                       : new String(
                        txtNumped.getValue().toString()));
                id.setItmped((((txtItmped.getValue()) == null) ||
                    (txtItmped.getValue()).equals("")) ? null
                                                       : new String(
                        txtItmped.getValue().toString()));
                id.setSubitm((((txtSubitm.getValue()) == null) ||
                    (txtSubitm.getValue()).equals("")) ? null
                                                       : new String(
                        txtSubitm.getValue().toString()));
                id.setCodcli((((txtCodcli.getValue()) == null) ||
                    (txtCodcli.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodcli.getValue().toString()));
                id.setDespro((((txtDespro.getValue()) == null) ||
                    (txtDespro.getValue()).equals("")) ? null
                                                       : new String(
                        txtDespro.getValue().toString()));
                id.setCntped((((txtCntped.getValue()) == null) ||
                    (txtCntped.getValue()).equals("")) ? null
                                                       : new String(
                        txtCntped.getValue().toString()));
                id.setUndmed((((txtUndmed.getValue()) == null) ||
                    (txtUndmed.getValue()).equals("")) ? null
                                                       : new String(
                        txtUndmed.getValue().toString()));
                id.setFecsol((((txtFecsol.getValue()) == null) ||
                    (txtFecsol.getValue()).equals("")) ? null
                                                       : new String(
                        txtFecsol.getValue().toString()));
                id.setFecdsp((((txtFecdsp.getValue()) == null) ||
                    (txtFecdsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtFecdsp.getValue().toString()));
                id.setCodpro((((txtCodpro.getValue()) == null) ||
                    (txtCodpro.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodpro.getValue().toString()));
                id.setEstado((((txtEstado.getValue()) == null) ||
                    (txtEstado.getValue()).equals("")) ? null
                                                       : new String(
                        txtEstado.getValue().toString()));
                id.setOrdcom((((txtOrdcom.getValue()) == null) ||
                    (txtOrdcom.getValue()).equals("")) ? null
                                                       : new String(
                        txtOrdcom.getValue().toString()));
                id.setCntdsp((((txtCntdsp.getValue()) == null) ||
                    (txtCntdsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtCntdsp.getValue().toString()));
                id.setCntkgs((((txtCntkgs.getValue()) == null) ||
                    (txtCntkgs.getValue()).equals("")) ? null
                                                       : new String(
                        txtCntkgs.getValue().toString()));
                id.setArollos((((txtArollos.getValue()) == null) ||
                    (txtArollos.getValue()).equals("")) ? null
                                                        : new String(
                        txtArollos.getValue().toString()));
                id.setAhojas((((txtAhojas.getValue()) == null) ||
                    (txtAhojas.getValue()).equals("")) ? null
                                                       : new String(
                        txtAhojas.getValue().toString()));
                id.setLhojas((((txtLhojas.getValue()) == null) ||
                    (txtLhojas.getValue()).equals("")) ? null
                                                       : new String(
                        txtLhojas.getValue().toString()));
                id.setArollo((((txtArollo.getValue()) == null) ||
                    (txtArollo.getValue()).equals("")) ? null
                                                       : new String(
                        txtArollo.getValue().toString()));

                entity = BusinessDelegatorView.getSapDetPed(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtNumped.setDisabled(false);
                txtItmped.setDisabled(false);
                txtSubitm.setDisabled(false);
                txtCodcli.setDisabled(false);
                txtDespro.setDisabled(false);
                txtCntped.setDisabled(false);
                txtUndmed.setDisabled(false);
                txtFecsol.setDisabled(false);
                txtFecdsp.setDisabled(false);
                txtCodpro.setDisabled(false);
                txtEstado.setDisabled(false);
                txtOrdcom.setDisabled(false);
                txtCntdsp.setDisabled(false);
                txtCntkgs.setDisabled(false);
                txtArollos.setDisabled(false);
                txtAhojas.setDisabled(false);
                txtLhojas.setDisabled(false);
                txtArollo.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtNumped.setValue(entity.getId().getNumped());
                txtNumped.setDisabled(true);
                txtItmped.setValue(entity.getId().getItmped());
                txtItmped.setDisabled(true);
                txtSubitm.setValue(entity.getId().getSubitm());
                txtSubitm.setDisabled(true);
                txtCodcli.setValue(entity.getId().getCodcli());
                txtCodcli.setDisabled(true);
                txtDespro.setValue(entity.getId().getDespro());
                txtDespro.setDisabled(true);
                txtCntped.setValue(entity.getId().getCntped());
                txtCntped.setDisabled(true);
                txtUndmed.setValue(entity.getId().getUndmed());
                txtUndmed.setDisabled(true);
                txtFecsol.setValue(entity.getId().getFecsol());
                txtFecsol.setDisabled(true);
                txtFecdsp.setValue(entity.getId().getFecdsp());
                txtFecdsp.setDisabled(true);
                txtCodpro.setValue(entity.getId().getCodpro());
                txtCodpro.setDisabled(true);
                txtEstado.setValue(entity.getId().getEstado());
                txtEstado.setDisabled(true);
                txtOrdcom.setValue(entity.getId().getOrdcom());
                txtOrdcom.setDisabled(true);
                txtCntdsp.setValue(entity.getId().getCntdsp());
                txtCntdsp.setDisabled(true);
                txtCntkgs.setValue(entity.getId().getCntkgs());
                txtCntkgs.setDisabled(true);
                txtArollos.setValue(entity.getId().getArollos());
                txtArollos.setDisabled(true);
                txtAhojas.setValue(entity.getId().getAhojas());
                txtAhojas.setDisabled(true);
                txtLhojas.setValue(entity.getId().getLhojas());
                txtLhojas.setDisabled(true);
                txtArollo.setValue(entity.getId().getArollo());
                txtArollo.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveSapDetPed((((txtNumped.getValue()) == null) ||
                (txtNumped.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumped.getValue().toString()),
                (((txtItmped.getValue()) == null) ||
                (txtItmped.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmped.getValue().toString()),
                (((txtSubitm.getValue()) == null) ||
                (txtSubitm.getValue()).equals("")) ? null
                                                   : new String(
                    txtSubitm.getValue().toString()),
                (((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtCntped.getValue()) == null) ||
                (txtCntped.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntped.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtFecsol.getValue()) == null) ||
                (txtFecsol.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecsol.getValue().toString()),
                (((txtFecdsp.getValue()) == null) ||
                (txtFecdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecdsp.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtEstado.getValue()) == null) ||
                (txtEstado.getValue()).equals("")) ? null
                                                   : new String(
                    txtEstado.getValue().toString()),
                (((txtOrdcom.getValue()) == null) ||
                (txtOrdcom.getValue()).equals("")) ? null
                                                   : new String(
                    txtOrdcom.getValue().toString()),
                (((txtCntdsp.getValue()) == null) ||
                (txtCntdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntdsp.getValue().toString()),
                (((txtCntkgs.getValue()) == null) ||
                (txtCntkgs.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntkgs.getValue().toString()),
                (((txtArollos.getValue()) == null) ||
                (txtArollos.getValue()).equals("")) ? null
                                                    : new String(
                    txtArollos.getValue().toString()),
                (((txtAhojas.getValue()) == null) ||
                (txtAhojas.getValue()).equals("")) ? null
                                                   : new String(
                    txtAhojas.getValue().toString()),
                (((txtLhojas.getValue()) == null) ||
                (txtLhojas.getValue()).equals("")) ? null
                                                   : new String(
                    txtLhojas.getValue().toString()),
                (((txtArollo.getValue()) == null) ||
                (txtArollo.getValue()).equals("")) ? null
                                                   : new String(
                    txtArollo.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteSapDetPed((((txtNumped.getValue()) == null) ||
                (txtNumped.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumped.getValue().toString()),
                (((txtItmped.getValue()) == null) ||
                (txtItmped.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmped.getValue().toString()),
                (((txtSubitm.getValue()) == null) ||
                (txtSubitm.getValue()).equals("")) ? null
                                                   : new String(
                    txtSubitm.getValue().toString()),
                (((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtCntped.getValue()) == null) ||
                (txtCntped.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntped.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtFecsol.getValue()) == null) ||
                (txtFecsol.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecsol.getValue().toString()),
                (((txtFecdsp.getValue()) == null) ||
                (txtFecdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecdsp.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtEstado.getValue()) == null) ||
                (txtEstado.getValue()).equals("")) ? null
                                                   : new String(
                    txtEstado.getValue().toString()),
                (((txtOrdcom.getValue()) == null) ||
                (txtOrdcom.getValue()).equals("")) ? null
                                                   : new String(
                    txtOrdcom.getValue().toString()),
                (((txtCntdsp.getValue()) == null) ||
                (txtCntdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntdsp.getValue().toString()),
                (((txtCntkgs.getValue()) == null) ||
                (txtCntkgs.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntkgs.getValue().toString()),
                (((txtArollos.getValue()) == null) ||
                (txtArollos.getValue()).equals("")) ? null
                                                    : new String(
                    txtArollos.getValue().toString()),
                (((txtAhojas.getValue()) == null) ||
                (txtAhojas.getValue()).equals("")) ? null
                                                   : new String(
                    txtAhojas.getValue().toString()),
                (((txtLhojas.getValue()) == null) ||
                (txtLhojas.getValue()).equals("")) ? null
                                                   : new String(
                    txtLhojas.getValue().toString()),
                (((txtArollo.getValue()) == null) ||
                (txtArollo.getValue()).equals("")) ? null
                                                   : new String(
                    txtArollo.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateSapDetPed((((txtNumped.getValue()) == null) ||
                (txtNumped.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumped.getValue().toString()),
                (((txtItmped.getValue()) == null) ||
                (txtItmped.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmped.getValue().toString()),
                (((txtSubitm.getValue()) == null) ||
                (txtSubitm.getValue()).equals("")) ? null
                                                   : new String(
                    txtSubitm.getValue().toString()),
                (((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtCntped.getValue()) == null) ||
                (txtCntped.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntped.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtFecsol.getValue()) == null) ||
                (txtFecsol.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecsol.getValue().toString()),
                (((txtFecdsp.getValue()) == null) ||
                (txtFecdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecdsp.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtEstado.getValue()) == null) ||
                (txtEstado.getValue()).equals("")) ? null
                                                   : new String(
                    txtEstado.getValue().toString()),
                (((txtOrdcom.getValue()) == null) ||
                (txtOrdcom.getValue()).equals("")) ? null
                                                   : new String(
                    txtOrdcom.getValue().toString()),
                (((txtCntdsp.getValue()) == null) ||
                (txtCntdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntdsp.getValue().toString()),
                (((txtCntkgs.getValue()) == null) ||
                (txtCntkgs.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntkgs.getValue().toString()),
                (((txtArollos.getValue()) == null) ||
                (txtArollos.getValue()).equals("")) ? null
                                                    : new String(
                    txtArollos.getValue().toString()),
                (((txtAhojas.getValue()) == null) ||
                (txtAhojas.getValue()).equals("")) ? null
                                                   : new String(
                    txtAhojas.getValue().toString()),
                (((txtLhojas.getValue()) == null) ||
                (txtLhojas.getValue()).equals("")) ? null
                                                   : new String(
                    txtLhojas.getValue().toString()),
                (((txtArollo.getValue()) == null) ||
                (txtArollo.getValue()).equals("")) ? null
                                                   : new String(
                    txtArollo.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String numped, String itmped,
        String subitm, String codcli, String despro, String cntped,
        String undmed, String fecsol, String fecdsp, String codpro,
        String estado, String ordcom, String cntdsp, String cntkgs,
        String arollos, String ahojas, String lhojas, String arollo)
        throws Exception {
        try {
            BusinessDelegatorView.updateSapDetPed(numped, itmped, subitm,
                codcli, despro, cntped, undmed, fecsol, fecdsp, codpro, estado,
                ordcom, cntdsp, cntkgs, arollos, ahojas, lhojas, arollo);
            renderManager.getOnDemandRenderer("SapDetPedView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("SapDetPedView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<SapDetPed> getSapDetPed() {
        if (flag) {
            try {
                sapDetPed = BusinessDelegatorView.getSapDetPed();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return sapDetPed;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setSapDetPed(List<SapDetPed> sapDetPed) {
        this.sapDetPed = sapDetPed;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberSapDetPed() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<SapDetPed> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        
    	//se comenta para que cuando se utilice la funcion de oreden enla tabla no se muestren los registros no filtrados
    	/*int totalNumberSapDetPed = 0;

        try {
            totalNumberSapDetPed = BusinessDelegatorView.findTotalNumberSapDetPed()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberSapDetPed) {
            endIndex = totalNumberSapDetPed;
        }*/

        try {
        	//tambien se comenta esta consulta
            /*sapDetPed = BusinessDelegatorView.findPageSapDetPed(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);*/

        	if(txtNumped == null){//si la pantalla no se ha cargado se detiene la ejecucion del metodo.
        		return new DataPage<SapDetPed>(0, 0, sapDetPed);
        	}
        	
    		sapDetPed = BusinessDelegatorView.consultarPedidosSap((((txtNumped.getValue()) == null) ||
					(txtNumped.getValue()).equals("")) ? null
                            : new String(
					txtNumped.getValue().toString()),
					(((txtOrdcom.getValue()) == null) ||
					(txtOrdcom.getValue()).equals("")) ? null
					                            : new String(
					txtOrdcom.getValue().toString()),
					tsccvEmpresas,//codigo cliente
					(((txtFechaInicial.getValue()) == null) ||
					(txtFechaInicial.getValue()).equals("")) ? null
					                      		: (Date)(txtFechaInicial.getValue()),
					(((txtFechaFinal.getValue()) == null) ||
					(txtFechaFinal.getValue()).equals("")) ? null
					                            : (Date)(txtFechaFinal.getValue()),
					(((txtCodpro.getValue()) == null) ||//codigo producto cliente
					(txtCodpro.getValue()).equals("")) ? null
					                            : new String(
					txtCodpro.getValue().toString()), "", sortColumnName, sortAscending);
        	
    		//se envia el valor del filtroH como ""
    		
            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        //return new DataPage<SapDetPed>(totalNumberSapDetPed, startRow, sapDetPed);
        return new DataPage<SapDetPed>(sapDetPed.size(), startRow, sapDetPed);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<SapDetPedDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberSapDetPed = 0;

        try {
            totalNumberSapDetPed = BusinessDelegatorView.findTotalNumberSapDetPed()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberSapDetPed) {
            endIndex = totalNumberSapDetPed;
        }

        try {
            sapDetPed = BusinessDelegatorView.findPageSapDetPed(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(sapDetPed)) {
                sapDetPedDTO = new ArrayList<SapDetPedDTO>();

                for (SapDetPed sapDetPedTmp : sapDetPed) {
                    SapDetPedDTO sapDetPedDTO2 = new SapDetPedDTO();
                    sapDetPedDTO2.setNumped(sapDetPedTmp.getId().getNumped()
                                                        .toString());
                    sapDetPedDTO2.setItmped(sapDetPedTmp.getId().getItmped()
                                                        .toString());
                    sapDetPedDTO2.setSubitm(sapDetPedTmp.getId().getSubitm()
                                                        .toString());
                    sapDetPedDTO2.setCodcli(sapDetPedTmp.getId().getCodcli()
                                                        .toString());
                    sapDetPedDTO2.setDespro(sapDetPedTmp.getId().getDespro()
                                                        .toString());
                    sapDetPedDTO2.setCntped(sapDetPedTmp.getId().getCntped()
                                                        .toString());
                    sapDetPedDTO2.setUndmed(sapDetPedTmp.getId().getUndmed()
                                                        .toString());
                    sapDetPedDTO2.setFecsol(sapDetPedTmp.getId().getFecsol()
                                                        .toString());
                    sapDetPedDTO2.setFecdsp(sapDetPedTmp.getId().getFecdsp()
                                                        .toString());
                    sapDetPedDTO2.setCodpro(sapDetPedTmp.getId().getCodpro()
                                                        .toString());
                    sapDetPedDTO2.setEstado(sapDetPedTmp.getId().getEstado()
                                                        .toString());
                    sapDetPedDTO2.setOrdcom(sapDetPedTmp.getId().getOrdcom()
                                                        .toString());
                    sapDetPedDTO2.setCntdsp(sapDetPedTmp.getId().getCntdsp()
                                                        .toString());
                    sapDetPedDTO2.setCntkgs(sapDetPedTmp.getId().getCntkgs()
                                                        .toString());
                    sapDetPedDTO2.setArollos(sapDetPedTmp.getId().getArollos()
                                                         .toString());
                    sapDetPedDTO2.setAhojas(sapDetPedTmp.getId().getAhojas()
                                                        .toString());
                    sapDetPedDTO2.setLhojas(sapDetPedTmp.getId().getLhojas()
                                                        .toString());
                    sapDetPedDTO2.setArollo(sapDetPedTmp.getId().getArollo()
                                                        .toString());

                    sapDetPedDTO2.setSapDetPed(sapDetPedTmp);
                    sapDetPedDTO2.setSapDetPedView(this);
                    sapDetPedDTO.add(sapDetPedDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<SapDetPedDTO>(totalNumberSapDetPed, startRow,
            sapDetPedDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(sapDetPedDTO)) {
            for (SapDetPedDTO sapDetPedTmp : sapDetPedDTO) {
                renderManager.getOnDemandRenderer("SapDetPedView").remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(sapDetPedDTO)) {
            for (SapDetPedDTO sapDetPedTmp : sapDetPedDTO) {
                renderManager.getOnDemandRenderer("SapDetPedView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtNumped() {
        return txtNumped;
    }

    public void setTxtNumped(HtmlInputText txtNumped) {
        this.txtNumped = txtNumped;
    }

    public HtmlInputText getTxtItmped() {
        return txtItmped;
    }

    public void setTxtItmped(HtmlInputText txtItmped) {
        this.txtItmped = txtItmped;
    }

    public HtmlInputText getTxtSubitm() {
        return txtSubitm;
    }

    public void setTxtSubitm(HtmlInputText txtSubitm) {
        this.txtSubitm = txtSubitm;
    }

    public HtmlInputText getTxtCodcli() {
        return txtCodcli;
    }

    public void setTxtCodcli(HtmlInputText txtCodcli) {
        this.txtCodcli = txtCodcli;
    }

    public HtmlInputText getTxtDespro() {
        return txtDespro;
    }

    public void setTxtDespro(HtmlInputText txtDespro) {
        this.txtDespro = txtDespro;
    }

    public HtmlInputText getTxtCntped() {
        return txtCntped;
    }

    public void setTxtCntped(HtmlInputText txtCntped) {
        this.txtCntped = txtCntped;
    }

    public HtmlInputText getTxtUndmed() {
        return txtUndmed;
    }

    public void setTxtUndmed(HtmlInputText txtUndmed) {
        this.txtUndmed = txtUndmed;
    }

    public HtmlInputText getTxtFecsol() {
        return txtFecsol;
    }

    public void setTxtFecsol(HtmlInputText txtFecsol) {
        this.txtFecsol = txtFecsol;
    }

    public HtmlInputText getTxtFecdsp() {
        return txtFecdsp;
    }

    public void setTxtFecdsp(HtmlInputText txtFecdsp) {
        this.txtFecdsp = txtFecdsp;
    }

    public HtmlInputText getTxtCodpro() {
        return txtCodpro;
    }

    public void setTxtCodpro(HtmlInputText txtCodpro) {
        this.txtCodpro = txtCodpro;
    }

    public HtmlInputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(HtmlInputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public HtmlInputText getTxtOrdcom() {
        return txtOrdcom;
    }

    public void setTxtOrdcom(HtmlInputText txtOrdcom) {
        this.txtOrdcom = txtOrdcom;
    }

    public HtmlInputText getTxtCntdsp() {
        return txtCntdsp;
    }

    public void setTxtCntdsp(HtmlInputText txtCntdsp) {
        this.txtCntdsp = txtCntdsp;
    }

    public HtmlInputText getTxtCntkgs() {
        return txtCntkgs;
    }

    public void setTxtCntkgs(HtmlInputText txtCntkgs) {
        this.txtCntkgs = txtCntkgs;
    }

    public HtmlInputText getTxtArollos() {
        return txtArollos;
    }

    public void setTxtArollos(HtmlInputText txtArollos) {
        this.txtArollos = txtArollos;
    }

    public HtmlInputText getTxtAhojas() {
        return txtAhojas;
    }

    public void setTxtAhojas(HtmlInputText txtAhojas) {
        this.txtAhojas = txtAhojas;
    }

    public HtmlInputText getTxtLhojas() {
        return txtLhojas;
    }

    public void setTxtLhojas(HtmlInputText txtLhojas) {
        this.txtLhojas = txtLhojas;
    }

    public HtmlInputText getTxtArollo() {
        return txtArollo;
    }

    public void setTxtArollo(HtmlInputText txtArollo) {
        this.txtArollo = txtArollo;
    }

    public SelectInputDate getTxtFechaInicial() {
		return txtFechaInicial;
	}

	public void setTxtFechaInicial(SelectInputDate txtFechaInicial) {
		this.txtFechaInicial = txtFechaInicial;
	}

	public SelectInputDate getTxtFechaFinal() {
		return txtFechaFinal;
	}

	public void setTxtFechaFinal(SelectInputDate txtFechaFinal) {
		this.txtFechaFinal = txtFechaFinal;
	}

	public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<SapDetPedDTO> getSapDetPedDTO() {
        return sapDetPedDTO;
    }

    public void setSapDetPedDTO(List<SapDetPedDTO> sapDetPedDTO) {
        this.sapDetPedDTO = sapDetPedDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    
	
	public TsccvEmpresas getTsccvEmpresas() {
		return tsccvEmpresas;
	}

	public void setTsccvEmpresas(TsccvEmpresas tsccvEmpresas) {
		this.tsccvEmpresas = tsccvEmpresas;
	}

	public String getEstadoPedido() {
		return estadoPedido;
	}

	public void setEstadoPedido(String estadoPedido) {
		this.estadoPedido = estadoPedido;
	}
	
	public boolean isAscendente() {
		return ascendente;
	}

	public void setAscendente(boolean ascendente) {
		this.ascendente = ascendente;
	}
    
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<SapDetPed> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<SapDetPedDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
    
    
    //metodo agregado para consultar los pedidos de acuerdo a la informacion ingresada
    public void consultarPedidosSap(){
    	List<SapDetPed> registros=null;
    	try {
    		registros = BusinessDelegatorView.consultarPedidosSap((((txtNumped.getValue()) == null) ||
					(txtNumped.getValue()).equals("")) ? null
                            : new String(
					txtNumped.getValue().toString()),
					(((txtOrdcom.getValue()) == null) ||
					(txtOrdcom.getValue()).equals("")) ? null
					                            : new String(
					txtOrdcom.getValue().toString()),
					tsccvEmpresas,//codigo cliente
					(((txtFechaInicial.getValue()) == null) ||
					(txtFechaInicial.getValue()).equals("")) ? null
					                      		: (Date)(txtFechaInicial.getValue()),
					(((txtFechaFinal.getValue()) == null) ||
					(txtFechaFinal.getValue()).equals("")) ? null
					                            : (Date)(txtFechaFinal.getValue()),
					(((txtCodpro.getValue()) == null) ||//codigo producto cliente
					(txtCodpro.getValue()).equals("")) ? null
					                            : new String(
					txtCodpro.getValue().toString()), estadoPedido, FacesUtils.getRequestParameter("nombreColumna"), (ascendente = !ascendente));
    		
    		//se envia el valor del filtroH como ""
            
            if(registros == null || registros.size() == 0){
            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("Para esta Planta no hay pedidos relacionados.", MBMensaje.MENSAJE_ALERTA);
            	
              	/*onePageDataModel.setDirtyData(false);
              	flag = false;
              	onePageDataModel.setPage(new DataPage<SapDetPed>(0, 0, registros));*/
              	sapDetPed = new ArrayList<SapDetPed>();
              	return;
            }
            
            /*onePageDataModel.setDirtyData(false);
            flag = true;
            onePageDataModel.setPage(new DataPage<SapDetPed>(registros.size(), 0, registros));*/
            sapDetPed = registros;
            
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    	
    	return;
    }
    
    //metodo encargado de hacer el llamado a la pantalla que muestra el detalle despacho 
    public String detalleDespacho(){
    	//se obtiene la sesion y se setea el atributo con el numero del pedido
    	HttpSession session= (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    	session.setAttribute("txtNumped", FacesUtils.getRequestParameter("txtNumped").toString());
    	session.setAttribute("txtItmped", FacesUtils.getRequestParameter("txtItmped").toString());
    	session.setAttribute("codMas", tsccvEmpresas.getCodMas());
    	
    	return "goDetalleDespachoSap";
    }


}
