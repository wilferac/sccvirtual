package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TsccvPlantasUsuarioDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ConstruirEmailCreacionDeUsuario;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TsccvUsuarioDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.EnviarEmail;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;
import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputSecret;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvUsuarioView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtActivo;
    private HtmlInputText txtApellido;
    private HtmlInputText txtCedula;
    private HtmlInputText txtEmail;
    private HtmlInputText txtLogin;
    private HtmlInputText txtNombre;
    private HtmlInputSecret txtPassword;
    private HtmlInputSecret txtConfirmacionPassword;
    private HtmlInputText txtIdGrup_TbmBaseGruposUsuarios;
    private HtmlInputText txtIdEmpr_TsccvEmpresas;
    private HtmlInputText txtCodpla_TsccvPlanta;
    private HtmlInputText txtIdUsua;
    private SelectInputDate txtFechaCreacion;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnInactivar;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TsccvUsuario> tsccvUsuario;
    private List<TsccvUsuarioDTO> tsccvUsuarioDTO;
        
    //listas desplegables
    private HtmlSelectOneMenu somActivo;
    private List<SelectItem> siActivo;
    private String[] valorParametroActivo;
    private HtmlSelectOneMenu somPlanta;
    private List<SelectItem> siPlanta;
    private HtmlSelectOneMenu somGrupo;
    private List<SelectItem> siGrupo;
    //se reemplaza por el nombre de la empresa ya que agrego el lov
    /*private HtmlSelectOneMenu somEmpresa;
    private List<SelectItem> siEmpresa;*/
    private HtmlInputText txtNombreEmpresa;

    public TsccvUsuarioView() {
        super("");

        //Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		
		try{			
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
			siActivo = new ArrayList<SelectItem>();
			siPlanta = new ArrayList<SelectItem>();
			siGrupo = new ArrayList<SelectItem>();
			txtNombreEmpresa = new HtmlInputText();
        	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
			
			//se crear los listados
			String valorParametroActivo[] = null;
			List<TbmBaseGruposUsuarios> listadoGrupos = new ArrayList<TbmBaseGruposUsuarios>();
        	//se comenta ya que se agrego el lov
			//List<TsccvEmpresas> listadoEmpresas = new ArrayList<TsccvEmpresas>();
			
			//se cargan los valores de las listas
			valorParametroActivo =  getValorParametroAct();			
			listadoGrupos = BusinessDelegatorView.getTbmBaseGruposUsuarios();
        	//se comenta ya que se agrego el lov
			//listadoEmpresas = BusinessDelegatorView.getTsccvEmpresas();
            
			//lista de activo			
        	siActivo.add(siNulo);        	
            for(int i=0; i<valorParametroActivo.length; i++){
        		if(valorParametroActivo[i] == null || valorParametroActivo[i].equals("")){
        			continue;
        		}
            	SelectItem siAct = new SelectItem();        		
        		siAct.setLabel(valorParametroActivo[i]);
        		siAct.setValue(new Long(i));
        		
        		siActivo.add(siAct);
        	}
                        
            //se valida el grupo usuario del usuario logeado para filtrar las plantas
            MBUsuario mbUsu= (MBUsuario)FacesUtils.getManagedBean("MBUsuario");
            TsccvUsuario userLogin = mbUsu.getUsuario();
            
            siPlanta.add(siNulo);
            //validacion para plantas de usuarios
            if(userLogin.getTbmBaseGruposUsuarios() != null){
            	List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
	            if(userLogin.getTbmBaseGruposUsuarios().getIdGrup().equals(new Long(archivoPropiedades.getProperty("SUPER_ADMINISTRADOR")))){	            		            	
	            	listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
	            }
	            else if(userLogin.getTbmBaseGruposUsuarios().getIdGrup().equals(new Long(archivoPropiedades.getProperty("ADMINISTRADOR_PLANTAS")))){
	            	//List<TsccvPlantasUsuario> listadoPlantasUsuario = BusinessDelegatorView.findByPropertyPlantasUsuario(TsccvPlantasUsuarioDAO.USUARIO, userLogin.getIdUsua());
	            	//se consultan las plantas de aplicacion para el usuario
	            	listadoPlantas = BusinessDelegatorView.findByUsuarioAndAplicacion(userLogin.getIdUsua(), new Integer(archivoPropiedades.getProperty("APLICACION")), "0");	            	
	            }
	            //lista de plantas
            	for(TsccvPlanta objetoPlanta: listadoPlantas){        		
            		SelectItem siPlan = new SelectItem();        		
            		siPlan.setLabel(objetoPlanta.getDespla());
            		siPlan.setValue(objetoPlanta.getCodpla());
            		
            		siPlanta.add(siPlan);
            	}
            }
            
        	//lista de grupos
        	siGrupo.add(siNulo);
        	for(TbmBaseGruposUsuarios objetoGrupo: listadoGrupos){
        		if(objetoGrupo.getActivo().equals("1")){
        			continue;
        		}
        		
        		SelectItem siGrup = new SelectItem();        		
        		siGrup.setLabel(objetoGrupo.getDescripcion());
        		siGrup.setValue(objetoGrupo.getIdGrup());
        		
        		siGrupo.add(siGrup);
        	}
        	
        	//se comenta ya que se agrego el lov
        	/*lista de empresa
        	siEmpresa.add(siNulo);
        	for(TsccvEmpresas objetoEmpresa: listadoEmpresas){
        		if(objetoEmpresa.getActivo().equals("1")){
        			continue;
        		}
        		
        		SelectItem siEmp = new SelectItem();        		
        		siEmp.setLabel(objetoEmpresa.getNombre());
        		siEmp.setValue(objetoEmpresa.getIdEmpr());
        		
        		siEmpresa.add(siEmp);
        	}*/        	
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    }

    public String action_clear() {
        txtActivo.setValue(null);
        txtActivo.setDisabled(false);
        somActivo.setValue(new Long(-1));
        
        txtApellido.setValue(null);
        txtApellido.setDisabled(false);
        txtCedula.setValue(null);
        txtCedula.setDisabled(false);
        txtEmail.setValue(null);
        txtEmail.setDisabled(false);
        txtLogin.setValue(null);
        txtLogin.setDisabled(false);
        txtNombre.setValue(null);
        txtNombre.setDisabled(false);
        txtPassword.setValue(null);
        txtPassword.setDisabled(false);
        txtConfirmacionPassword.setValue(null);
        txtConfirmacionPassword.setDisabled(false);
        txtIdGrup_TbmBaseGruposUsuarios.setValue(null);
        txtIdGrup_TbmBaseGruposUsuarios.setDisabled(false);
        somGrupo.setValue(new Long(-1));
        
        txtIdEmpr_TsccvEmpresas.setValue(null);
        txtIdEmpr_TsccvEmpresas.setDisabled(false);
        try{
	        //Se crea un objeto del bundle		
	        ArchivoBundle archivoBundle = new ArchivoBundle();
	        txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
        }catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}
        //se comenta ya que se agrego el lov
        //somEmpresa.setValue(new Long(-1));
        
        txtCodpla_TsccvPlanta.setValue(null);
        txtCodpla_TsccvPlanta.setDisabled(false);
        somPlanta.setValue(new Long(-1));

        txtFechaCreacion.setValue(null);
        txtFechaCreacion.setDisabled(false);

        txtIdUsua.setValue(null);
        txtIdUsua.setDisabled(false);

        btnSave.setDisabled(false);
        btnDelete.setDisabled(true);
        btnInactivar.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        onePageDataModel = null;
        flag = true;
        getData();
        
        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TsccvUsuario entity = null;

            try {
                Integer idUsua = new Integer(txtIdUsua.getValue().toString());

                entity = BusinessDelegatorView.getTsccvUsuario(idUsua);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtActivo.setDisabled(false);
                txtApellido.setDisabled(false);
                txtCedula.setDisabled(false);
                txtEmail.setDisabled(false);
                txtLogin.setDisabled(false);
                txtNombre.setDisabled(false);
                txtPassword.setDisabled(false);
                txtIdGrup_TbmBaseGruposUsuarios.setDisabled(false);
                txtIdEmpr_TsccvEmpresas.setDisabled(false);
                txtCodpla_TsccvPlanta.setDisabled(false);

                txtFechaCreacion.setDisabled(false);

                txtIdUsua.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnInactivar.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtActivo.setValue(entity.getActivo());
                txtActivo.setDisabled(false);
                somActivo.setValue(entity.getActivo());
                
                txtApellido.setValue(entity.getApellido());
                txtApellido.setDisabled(false);
                txtCedula.setValue(entity.getCedula());
                txtCedula.setDisabled(false);
                txtEmail.setValue(entity.getEmail());
                txtEmail.setDisabled(false);
                txtFechaCreacion.setValue(entity.getFechaCreacion());
                txtFechaCreacion.setDisabled(false);
                txtLogin.setValue(entity.getLogin());
                txtLogin.setDisabled(false);
                txtNombre.setValue(entity.getNombre());
                txtNombre.setDisabled(false);
                txtPassword.setValue(entity.getPassword());
                txtPassword.setDisabled(false);
                txtConfirmacionPassword.setValue(entity.getPassword());
                txtConfirmacionPassword.setDisabled(false);
                txtIdGrup_TbmBaseGruposUsuarios.setValue(entity.getTbmBaseGruposUsuarios() != null ? entity.getTbmBaseGruposUsuarios().getIdGrup() : null);
                txtIdGrup_TbmBaseGruposUsuarios.setDisabled(false);
                somGrupo.setValue(entity.getTbmBaseGruposUsuarios() != null ? entity.getTbmBaseGruposUsuarios().getIdGrup() : null);
                
                txtIdEmpr_TsccvEmpresas.setValue(entity.getTsccvEmpresas() != null ? entity.getTsccvEmpresas().getIdEmpr() : null);
                txtIdEmpr_TsccvEmpresas.setDisabled(false);
            	txtNombreEmpresa.setValue(entity.getTsccvEmpresas().getNombre());
                //se comenta ya que se agrego el lov
                //somEmpresa.setValue(entity.getTsccvEmpresas() != null ? entity.getTsccvEmpresas().getIdEmpr() : null);
                
                txtCodpla_TsccvPlanta.setValue(entity.getTsccvPlanta().getCodpla());
                txtCodpla_TsccvPlanta.setDisabled(false);
                somPlanta.setValue(entity.getTsccvPlanta().getCodpla());

                txtIdUsua.setValue(entity.getIdUsua());
                txtIdUsua.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnInactivar.setDisabled(false);
                btnModify.setDisabled(false);                
                btnClear.setDisabled(false);
            }
        }
    }
    
    
    public String action_save() {
    	txtActivo.setValue(new Long(somActivo.getValue().toString()) != -1 ? somActivo.getValue() : null);
    	txtIdGrup_TbmBaseGruposUsuarios.setValue(new Long(somGrupo.getValue().toString()) != -1 ? somGrupo.getValue() : null);
        //se comenta ya que se agrego el lov
    	//txtIdEmpr_TsccvEmpresas.setValue(new Long(somEmpresa.getValue().toString()) != -1 ? somEmpresa.getValue() : null);
        if(somPlanta.getValue() != null){
        	txtCodpla_TsccvPlanta.setValue(new Long(somPlanta.getValue().toString()) != -1 ? somPlanta.getValue() : null);
        }
        
        //se asigna a la fecha un new Date()
        txtFechaCreacion.setValue(new Date());
    	
        try {
        	if((txtNombre.getValue() != null && !txtNombre.getValue().equals("")) && (txtApellido.getValue() != null && !txtApellido.getValue().equals("")) &&
        		(txtLogin.getValue() != null && !txtLogin.getValue().equals("")) && (txtCedula.getValue() != null && !txtCedula.getValue().equals(""))){
        		if(BusinessDelegatorView.usuarioIsRegistrado(txtLogin.getValue().toString(), new Integer(txtCedula.getValue().toString()),
        									txtIdEmpr_TsccvEmpresas.getValue() != null ? new Integer(txtIdEmpr_TsccvEmpresas.getValue().toString()) : null)){
        			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
                  	mensaje.mostrarMensaje("El Usuario ya se encuentra registrado", MBMensaje.MENSAJE_ALERTA);
                  	
                  	return "";
        		}
        	}
        	
        	if(!comprarContrasenas()){
	    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensajeAlerta("La Contrase�a es distinta a la confirmaci�n de �sta. Por favor ingr�sela correctamente.");
				return "";
	    	}
        	
            String msg = BusinessDelegatorView.saveTsccvUsuario((((txtActivo.getValue()) == null) ||
                (txtActivo.getValue()).equals("")) ? null
                                                   : new String(
                    txtActivo.getValue().toString()),
                (((txtApellido.getValue()) == null) ||
                (txtApellido.getValue()).equals("")) ? null
                                                     : new String(
                    txtApellido.getValue().toString()).toUpperCase(),
                (((txtCedula.getValue()) == null) ||
                (txtCedula.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtCedula.getValue().toString()),
                (((txtEmail.getValue()) == null) ||
                (txtEmail.getValue()).equals("")) ? null
                                                  : new String(
                    txtEmail.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdUsua.getValue()) == null) ||
                (txtIdUsua.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdUsua.getValue().toString()),
                (((txtLogin.getValue()) == null) ||
                (txtLogin.getValue()).equals("")) ? null
                                                  : new String(
                    txtLogin.getValue().toString()),
                (((txtNombre.getValue()) == null) ||
                (txtNombre.getValue()).equals("")) ? null
                                                   : new String(
                    txtNombre.getValue().toString()).toUpperCase(),
                (((txtPassword.getValue()) == null) ||
                (txtPassword.getValue()).equals("")) ? null
                                                     : new String(
                    txtPassword.getValue().toString()),
                (((txtIdGrup_TbmBaseGruposUsuarios.getValue()) == null) ||
                (txtIdGrup_TbmBaseGruposUsuarios.getValue()).equals("")) ? null
                                                                         : new Long(
                    txtIdGrup_TbmBaseGruposUsuarios.getValue().toString()),
                (((txtIdEmpr_TsccvEmpresas.getValue()) == null) ||
                (txtIdEmpr_TsccvEmpresas.getValue()).equals("")) ? null
                                                                 : new Integer(
                    txtIdEmpr_TsccvEmpresas.getValue().toString()),
                (((txtCodpla_TsccvPlanta.getValue()) == null) ||
                (txtCodpla_TsccvPlanta.getValue()).equals("")) ? null
                                                               : new String(
                    txtCodpla_TsccvPlanta.getValue().toString()));

            /*FacesContext.getCurrentInstance()
            .addMessage("", new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));*/
            
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("El Usuario fue adicionado de forma satisfactoria"+(msg != null ? msg : ""), (msg != null ? MBMensaje.MENSAJE_ALERTA : MBMensaje.MENSAJE_OK));
			
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTsccvUsuario((((txtIdUsua.getValue()) == null) ||
                (txtIdUsua.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdUsua.getValue().toString()));

            /*FacesContext.getCurrentInstance()
            .addMessage("", new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));*/
            
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("El Usuario fue eliminado de forma satisfactoria", MBMensaje.MENSAJE_OK);
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_modify() {
    	txtActivo.setValue(new Long(somActivo.getValue().toString()) != -1 ? somActivo.getValue() : null);
    	txtIdGrup_TbmBaseGruposUsuarios.setValue(new Long(somGrupo.getValue().toString()) != -1 ? somGrupo.getValue() : null);
        //se comenta ya que se agrego el lov
    	//txtIdEmpr_TsccvEmpresas.setValue(new Long(somEmpresa.getValue().toString()) != -1 ? somEmpresa.getValue() : null);
        txtCodpla_TsccvPlanta.setValue(new Long(somPlanta.getValue().toString()) != -1 ? somPlanta.getValue() : null);
        
        try {     
        	
        	if(!comprarContrasenas()){
	    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensajeAlerta("La Contrase�a es distinta a la confirmaci�n de �sta. Por favor ingr�sela correctamente.");
				return "";
	    	}
        	
        	String msg = BusinessDelegatorView.updateTsccvUsuario((((txtActivo.getValue()) == null) ||
                (txtActivo.getValue()).equals("")) ? null
                                                   : new String(
                    txtActivo.getValue().toString()),
                (((txtApellido.getValue()) == null) ||
                (txtApellido.getValue()).equals("")) ? null
                                                     : new String(
                    txtApellido.getValue().toString()).toUpperCase(),
                (((txtCedula.getValue()) == null) ||
                (txtCedula.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtCedula.getValue().toString()),
                (((txtEmail.getValue()) == null) ||
                (txtEmail.getValue()).equals("")) ? null
                                                  : new String(
                    txtEmail.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdUsua.getValue()) == null) ||
                (txtIdUsua.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdUsua.getValue().toString()),
                (((txtLogin.getValue()) == null) ||
                (txtLogin.getValue()).equals("")) ? null
                                                  : new String(
                    txtLogin.getValue().toString()),
                (((txtNombre.getValue()) == null) ||
                (txtNombre.getValue()).equals("")) ? null
                                                   : new String(
                    txtNombre.getValue().toString()).toUpperCase(),
                (((txtPassword.getValue()) == null) ||
                (txtPassword.getValue()).equals("")) ? null
                                                     : new String(
                    txtPassword.getValue().toString()),
                (((txtIdGrup_TbmBaseGruposUsuarios.getValue()) == null) ||
                (txtIdGrup_TbmBaseGruposUsuarios.getValue()).equals("")) ? null
                                                                         : new Long(
                    txtIdGrup_TbmBaseGruposUsuarios.getValue().toString()),
                (((txtIdEmpr_TsccvEmpresas.getValue()) == null) ||
                (txtIdEmpr_TsccvEmpresas.getValue()).equals("")) ? null
                                                                 : new Integer(
                    txtIdEmpr_TsccvEmpresas.getValue().toString()),
                (((txtCodpla_TsccvPlanta.getValue()) == null) ||
                (txtCodpla_TsccvPlanta.getValue()).equals("")) ? null
                                                               : new String(
                    txtCodpla_TsccvPlanta.getValue().toString()));

            /*FacesContext.getCurrentInstance()
            .addMessage("", new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));*/
            
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("El Usuario fue modificado de forma satisfactoria"+(msg != null ? msg : ""), (msg != null ? MBMensaje.MENSAJE_ALERTA : MBMensaje.MENSAJE_OK));
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, String apellido,
        Integer cedula, String email, Date fechaCreacion, Integer idUsua,
        String login, String nombre, String password,
        Long idGrup_TbmBaseGruposUsuarios, Integer idEmpr_TsccvEmpresas,
        String codpla_TsccvPlanta) throws Exception {
        try {
            BusinessDelegatorView.updateTsccvUsuario(activo, apellido, cedula,
                email, fechaCreacion, idUsua, login, nombre, password,
                idGrup_TbmBaseGruposUsuarios, idEmpr_TsccvEmpresas,
                codpla_TsccvPlanta);
            renderManager.getOnDemandRenderer("TsccvUsuarioView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TsccvUsuarioView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TsccvUsuario> getTsccvUsuario() {
        if (flag) {
            try {
                tsccvUsuario = BusinessDelegatorView.getTsccvUsuario();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tsccvUsuario;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTsccvUsuario(List<TsccvUsuario> tsccvUsuario) {
        this.tsccvUsuario = tsccvUsuario;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTsccvUsuario() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvUsuario> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvUsuario = 0;

        try {
            totalNumberTsccvUsuario = BusinessDelegatorView.findTotalNumberTsccvUsuario()
                                                           .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvUsuario) {
            endIndex = totalNumberTsccvUsuario;
        }

        try {
            tsccvUsuario = BusinessDelegatorView.findPageTsccvUsuario(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvUsuario>(totalNumberTsccvUsuario, startRow,
            tsccvUsuario);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvUsuarioDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvUsuario = 0;

        try {
            totalNumberTsccvUsuario = BusinessDelegatorView.findTotalNumberTsccvUsuario()
                                                           .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvUsuario) {
            endIndex = totalNumberTsccvUsuario;
        }

        try {
            tsccvUsuario = BusinessDelegatorView.findPageTsccvUsuario(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tsccvUsuario)) {
                tsccvUsuarioDTO = new ArrayList<TsccvUsuarioDTO>();

                for (TsccvUsuario tsccvUsuarioTmp : tsccvUsuario) {
                    TsccvUsuarioDTO tsccvUsuarioDTO2 = new TsccvUsuarioDTO();
                    tsccvUsuarioDTO2.setIdUsua(tsccvUsuarioTmp.getIdUsua()
                                                              .toString());

                    tsccvUsuarioDTO2.setActivo((tsccvUsuarioTmp.getActivo() != null)
                        ? tsccvUsuarioTmp.getActivo().toString() : null);
                    tsccvUsuarioDTO2.setApellido((tsccvUsuarioTmp.getApellido() != null)
                        ? tsccvUsuarioTmp.getApellido().toString() : null);
                    tsccvUsuarioDTO2.setCedula((tsccvUsuarioTmp.getCedula() != null)
                        ? tsccvUsuarioTmp.getCedula().toString() : null);
                    tsccvUsuarioDTO2.setEmail((tsccvUsuarioTmp.getEmail() != null)
                        ? tsccvUsuarioTmp.getEmail().toString() : null);
                    tsccvUsuarioDTO2.setFechaCreacion(tsccvUsuarioTmp.getFechaCreacion());
                    tsccvUsuarioDTO2.setLogin((tsccvUsuarioTmp.getLogin() != null)
                        ? tsccvUsuarioTmp.getLogin().toString() : null);
                    tsccvUsuarioDTO2.setNombre((tsccvUsuarioTmp.getNombre() != null)
                        ? tsccvUsuarioTmp.getNombre().toString() : null);
                    tsccvUsuarioDTO2.setPassword((tsccvUsuarioTmp.getPassword() != null)
                        ? tsccvUsuarioTmp.getPassword().toString() : null);
                    tsccvUsuarioDTO2.setIdGrup_TbmBaseGruposUsuarios((tsccvUsuarioTmp.getTbmBaseGruposUsuarios()
                                                                                     .getIdGrup() != null)
                        ? tsccvUsuarioTmp.getTbmBaseGruposUsuarios().getIdGrup()
                                         .toString() : null);
                    tsccvUsuarioDTO2.setIdEmpr_TsccvEmpresas((tsccvUsuarioTmp.getTsccvEmpresas()
                                                                             .getIdEmpr() != null)
                        ? tsccvUsuarioTmp.getTsccvEmpresas().getIdEmpr()
                                         .toString() : null);
                    tsccvUsuarioDTO2.setCodpla_TsccvPlanta((tsccvUsuarioTmp.getTsccvPlanta()
                                                                           .getCodpla() != null)
                        ? tsccvUsuarioTmp.getTsccvPlanta().getCodpla().toString()
                        : null);
                    tsccvUsuarioDTO2.setTsccvUsuario(tsccvUsuarioTmp);
                    tsccvUsuarioDTO2.setTsccvUsuarioView(this);
                    tsccvUsuarioDTO.add(tsccvUsuarioDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvUsuarioDTO>(totalNumberTsccvUsuario, startRow,
            tsccvUsuarioDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tsccvUsuarioDTO)) {
            for (TsccvUsuarioDTO tsccvUsuarioTmp : tsccvUsuarioDTO) {
                renderManager.getOnDemandRenderer("TsccvUsuarioView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tsccvUsuarioDTO)) {
            for (TsccvUsuarioDTO tsccvUsuarioTmp : tsccvUsuarioDTO) {
                renderManager.getOnDemandRenderer("TsccvUsuarioView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(HtmlInputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public HtmlInputText getTxtApellido() {
        return txtApellido;
    }

    public void setTxtApellido(HtmlInputText txtApellido) {
        this.txtApellido = txtApellido;
    }

    public HtmlInputText getTxtCedula() {
        return txtCedula;
    }

    public void setTxtCedula(HtmlInputText txtCedula) {
        this.txtCedula = txtCedula;
    }

    public HtmlInputText getTxtEmail() {
        return txtEmail;
    }

    public void setTxtEmail(HtmlInputText txtEmail) {
        this.txtEmail = txtEmail;
    }

    public HtmlInputText getTxtLogin() {
        return txtLogin;
    }

    public void setTxtLogin(HtmlInputText txtLogin) {
        this.txtLogin = txtLogin;
    }

    public HtmlInputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(HtmlInputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public HtmlInputSecret getTxtPassword() {
        return txtPassword;
    }

    public void setTxtPassword(HtmlInputSecret txtPassword) {
        this.txtPassword = txtPassword;
    }
    
    public HtmlInputSecret getTxtConfirmacionPassword() {
		return txtConfirmacionPassword;
	}
    
    public void setTxtConfirmacionPassword(
			HtmlInputSecret txtConfirmacionPassword) {
		this.txtConfirmacionPassword = txtConfirmacionPassword;
	}

    public HtmlInputText getTxtIdGrup_TbmBaseGruposUsuarios() {
        return txtIdGrup_TbmBaseGruposUsuarios;
    }

    public void setTxtIdGrup_TbmBaseGruposUsuarios(
        HtmlInputText txtIdGrup_TbmBaseGruposUsuarios) {
        this.txtIdGrup_TbmBaseGruposUsuarios = txtIdGrup_TbmBaseGruposUsuarios;
    }

    public HtmlInputText getTxtIdEmpr_TsccvEmpresas() {
        return txtIdEmpr_TsccvEmpresas;
    }

    public void setTxtIdEmpr_TsccvEmpresas(
        HtmlInputText txtIdEmpr_TsccvEmpresas) {
        this.txtIdEmpr_TsccvEmpresas = txtIdEmpr_TsccvEmpresas;
    }

    public HtmlInputText getTxtCodpla_TsccvPlanta() {
        return txtCodpla_TsccvPlanta;
    }

    public void setTxtCodpla_TsccvPlanta(HtmlInputText txtCodpla_TsccvPlanta) {
        this.txtCodpla_TsccvPlanta = txtCodpla_TsccvPlanta;
    }

    public SelectInputDate getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(SelectInputDate txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public HtmlInputText getTxtIdUsua() {
        return txtIdUsua;
    }

    public void setTxtIdUsua(HtmlInputText txtIdUsua) {
        this.txtIdUsua = txtIdUsua;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

	public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnInactivar() {
		return btnInactivar;
	}

	public void setBtnInactivar(HtmlCommandButton btnInactivar) {
		this.btnInactivar = btnInactivar;
	}

	public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TsccvUsuarioDTO> getTsccvUsuarioDTO() {
        return tsccvUsuarioDTO;
    }

    public void setTsccvUsuarioDTO(List<TsccvUsuarioDTO> tsccvUsuarioDTO) {
        this.tsccvUsuarioDTO = tsccvUsuarioDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    
    
    
    
    public HtmlSelectOneMenu getSomActivo() {
		return somActivo;
	}

	public void setSomActivo(HtmlSelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}

	public List<SelectItem> getSiActivo() {
		return siActivo;
	}

	public void setSiActivo(List<SelectItem> siActivo) {
		this.siActivo = siActivo;
	}

	public String[] getValorParametroActivo() {
		return valorParametroActivo;
	}

	public void setValorParametroActivo(String[] valorParametroActivo) {
		this.valorParametroActivo = valorParametroActivo;
	}

	public HtmlSelectOneMenu getSomPlanta() {
		return somPlanta;
	}

	public void setSomPlanta(HtmlSelectOneMenu somPlanta) {
		this.somPlanta = somPlanta;
	}

	public List<SelectItem> getSiPlanta() {
		return siPlanta;
	}

	public void setSiPlanta(List<SelectItem> siPlanta) {
		this.siPlanta = siPlanta;
	}

	public HtmlSelectOneMenu getSomGrupo() {
		return somGrupo;
	}

	public void setSomGrupo(HtmlSelectOneMenu somGrupo) {
		this.somGrupo = somGrupo;
	}

	public List<SelectItem> getSiGrupo() {
		return siGrupo;
	}

	public void setSiGrupo(List<SelectItem> siGrupo) {
		this.siGrupo = siGrupo;
	}
	
	public HtmlInputText getTxtNombreEmpresa() {
		return txtNombreEmpresa;
	}

	public void setTxtNombreEmpresa(HtmlInputText txtNombreEmpresa) {
		this.txtNombreEmpresa = txtNombreEmpresa;
	}
    
    
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvUsuario> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvUsuarioDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }

    
    //Metodo encargado de consultar el parametro para el campo activo
    public String[] getValorParametroAct(){    	
    	//ARCHIVO PORPIEDADES
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String temp[] = null;
        try {
        	temp = archivoPropiedades.getProperty("LISTA_ACTIVO").toString().split(",");
        	
        	if(temp != null){
        		valorParametroActivo = new String[temp.length+1];  
	        	for(int i=0; i<temp.length; i++){
	        		valorParametroActivo[i] = temp[i];
	        	}
        	}
			return valorParametroActivo;
        } catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return null;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return null;
		}        
    }
    
    
    //SE AGREGA EL METODO ASOCIADO AL ENLACE EN LA TABLA 
    public void find_txtId(ActionEvent event) {
    	Integer idUsua = Integer.parseInt(FacesUtils.getRequestParameter("txtIdUsua").toString());
    	find(idUsua);
    }
    //METODO DE BUSQUEDA.
    public void find (Integer idUsua){
    	TsccvUsuario entity = null;
    	
        try {
            entity = BusinessDelegatorView.getTsccvUsuario(idUsua);
        } catch (Exception e) {
            // TODO: handle exception
        }
        
        if (entity == null) {
            txtActivo.setDisabled(false);
            txtApellido.setDisabled(false);
            txtCedula.setDisabled(false);
            txtEmail.setDisabled(false);
            txtLogin.setDisabled(false);
            txtNombre.setDisabled(false);
            txtPassword.setDisabled(false);
            txtIdGrup_TbmBaseGruposUsuarios.setDisabled(false);
            txtIdEmpr_TsccvEmpresas.setDisabled(false);
            txtCodpla_TsccvPlanta.setDisabled(false);

            txtFechaCreacion.setDisabled(false);

            txtIdUsua.setDisabled(false);

            btnSave.setDisabled(false);
            btnDelete.setDisabled(true);
            btnInactivar.setDisabled(true);
            btnModify.setDisabled(true);
            btnClear.setDisabled(false);
        } else {
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            somActivo.setValue(entity.getActivo());
            
            txtApellido.setValue(entity.getApellido());
            txtApellido.setDisabled(false);
            txtCedula.setValue(entity.getCedula());
            txtCedula.setDisabled(false);
            txtEmail.setValue(entity.getEmail());
            txtEmail.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtLogin.setValue(entity.getLogin());
            txtLogin.setDisabled(true);
            txtNombre.setValue(entity.getNombre());
            txtNombre.setDisabled(false);
            txtPassword.setValue(entity.getPassword());
            txtPassword.setDisabled(false);
            txtConfirmacionPassword.setValue(entity.getPassword());
            txtConfirmacionPassword.setDisabled(false);
            txtIdGrup_TbmBaseGruposUsuarios.setValue(entity.getTbmBaseGruposUsuarios() != null ? entity.getTbmBaseGruposUsuarios().getIdGrup() : null);
            txtIdGrup_TbmBaseGruposUsuarios.setDisabled(false);
            somGrupo.setValue(entity.getTbmBaseGruposUsuarios() != null ? entity.getTbmBaseGruposUsuarios().getIdGrup() : null);
            
            txtIdEmpr_TsccvEmpresas.setValue(entity.getTsccvEmpresas() != null ? entity.getTsccvEmpresas().getIdEmpr() : null);
            txtIdEmpr_TsccvEmpresas.setDisabled(false);
            txtNombreEmpresa.setValue(entity.getTsccvEmpresas().getNombre());
            //se comenta ya que se agrego el lov
            //somEmpresa.setValue(entity.getTsccvEmpresas() != null ? entity.getTsccvEmpresas().getIdEmpr() : null);
            
            txtCodpla_TsccvPlanta.setValue(entity.getTsccvPlanta().getCodpla());
            txtCodpla_TsccvPlanta.setDisabled(false);
            somPlanta.setValue(entity.getTsccvPlanta().getCodpla());

            txtIdUsua.setValue(entity.getIdUsua());
            txtIdUsua.setDisabled(true);

            btnSave.setDisabled(true);
            btnDelete.setDisabled(false);
            btnInactivar.setDisabled(false);
            btnModify.setDisabled(false);                
            btnClear.setDisabled(false);
        }
    }
    
    
    //consulta los usuarios por tolos los campos
    public String consultarUsuarios(){
    	List<TsccvUsuario> registros=null;    	
    	
    	try {
        	//ANTES DE GUARDAR SE ASIGNA EL VALOR DE LA LISTA SELECCIONADO AL CAMPO DE TEXTO RESPECTIVO
    		txtActivo.setValue(new Long(somActivo.getValue().toString()) != -1 ? somActivo.getValue() : null);
        	txtIdGrup_TbmBaseGruposUsuarios.setValue(new Long(somGrupo.getValue().toString()) != -1 ? somGrupo.getValue() : null);
            //se comenta ya que se agrego el lov
        	//txtIdEmpr_TsccvEmpresas.setValue(new Long(somEmpresa.getValue().toString()) != -1 ? somEmpresa.getValue() : null);
            txtCodpla_TsccvPlanta.setValue(new Long(somPlanta.getValue().toString()) != -1 ? somPlanta.getValue() : null);
        	
            registros = BusinessDelegatorView.consultarUsuarios((((txtActivo.getValue()) == null) ||
                    (txtActivo.getValue()).equals("")) ? null
                            : new String(
					txtActivo.getValue().toString()),
					(((txtApellido.getValue()) == null) ||
					(txtApellido.getValue()).equals("")) ? null
					                              : new String(
					txtApellido.getValue().toString()),
					(((txtCedula.getValue()) == null) ||
					(txtCedula.getValue()).equals("")) ? null
					                            : new Integer(
					txtCedula.getValue().toString()),
					(((txtEmail.getValue()) == null) ||
					(txtEmail.getValue()).equals("")) ? null
					                           : new String(
					txtEmail.getValue().toString()),
					(((txtFechaCreacion.getValue()) == null) ||
					(txtFechaCreacion.getValue()).equals("")) ? null
					                                   : (Date) txtFechaCreacion.getValue(),
					(((txtIdUsua.getValue()) == null) ||
					(txtIdUsua.getValue()).equals("")) ? null
					                            : new Integer(
					txtIdUsua.getValue().toString()),
					(((txtLogin.getValue()) == null) ||
					(txtLogin.getValue()).equals("")) ? null
					                           : new String(
					txtLogin.getValue().toString()),
					(((txtNombre.getValue()) == null) ||
					(txtNombre.getValue()).equals("")) ? null
					                            : new String(
					txtNombre.getValue().toString()),
					(((txtPassword.getValue()) == null) ||
					(txtPassword.getValue()).equals("")) ? null
					                              : new String(
					txtPassword.getValue().toString()),
					(((txtIdGrup_TbmBaseGruposUsuarios.getValue()) == null) ||
					(txtIdGrup_TbmBaseGruposUsuarios.getValue()).equals("")) ? null
					                                                  : new Long(
					txtIdGrup_TbmBaseGruposUsuarios.getValue().toString()),
					(((txtIdEmpr_TsccvEmpresas.getValue()) == null) ||
					(txtIdEmpr_TsccvEmpresas.getValue()).equals("")) ? null
					                                          : new Integer(
					txtIdEmpr_TsccvEmpresas.getValue().toString()),
					(((txtCodpla_TsccvPlanta.getValue()) == null) ||
					(txtCodpla_TsccvPlanta.getValue()).equals("")) ? null
					                                        : new String(
					txtCodpla_TsccvPlanta.getValue().toString()));
            
            if(registros == null || registros.size() == 0){            	
            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("No se encontraron registros", MBMensaje.MENSAJE_ALERTA);
            	
              	onePageDataModel.setDirtyData(false);
              	flag = false;
              	onePageDataModel.setPage(new DataPage<TsccvUsuario>(0, 0, registros));
              	return"";
            }
            
            onePageDataModel.setDirtyData(false);
            flag = true;
            onePageDataModel.setPage(new DataPage<TsccvUsuario>(registros.size(), 0, registros));
            
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}
    	
    	return "";
    }
    
    public String inactivarUsuario(){
    	try {
			TsccvUsuario tsccvUsuario = BusinessDelegatorView.getTsccvUsuario(new Integer(txtIdUsua.getValue().toString()));
			
			if((tsccvUsuario != null)){
				if(!tsccvUsuario.getActivo().equals("1")){
					BusinessDelegatorView.inactivarUsuario(tsccvUsuario.getIdUsua());
				}
				else{
					MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	              	mensaje.mostrarMensaje("El Usuario ya esta inactivo", MBMensaje.MENSAJE_ALERTA);
	              	return "";
				}
			}
			
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("El Usuario fue inactivado de forma satisfactoria", MBMensaje.MENSAJE_OK);
						
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
		
		action_clear();
		
    	return "";
    }
    
    
    //metodo encargado mostrar el panel para la busqueda y seleccion de la empresa
	public void buscarEmpresa(){	
		//se obtiene el managebean
		BusquedaEmpresaView busquedaEmpresaView = (BusquedaEmpresaView) FacesUtils.getManagedBean("busquedaEmpresaView");
		
		//se setea el managebean que realiza el llamado this
		busquedaEmpresaView.setManageBeanLlmado(this);
		busquedaEmpresaView.mostrarPanel();
	}
	
	//metodo encargado de setear a los componentes de la pantalla el nombre y el id de la empresa seleccionada 
	public void colocarSeleccion(Integer idEmpr, String nombreEmpr){
		txtIdEmpr_TsccvEmpresas.setValue(idEmpr);
		txtNombreEmpresa.setValue(nombreEmpr);
	}
	
	//metodo encargado de comparar el password y la confirmacion
	public Boolean comprarContrasenas(){
		if(txtPassword.getValue().toString().equals(txtConfirmacionPassword.getValue().toString())){
			return true;
		}
		
		return false;
	}
}
