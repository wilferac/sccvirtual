package co.com.smurfit.presentation.backEndBeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.dto.BvpPrecioDTO;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class PdfsFaltantesView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtAlto;
    private HtmlInputText txtAncho;
    private HtmlInputText txtAncsac;
    private HtmlInputText txtClave;
    private HtmlInputText txtCntmax;
    private HtmlInputText txtCntmin;
    private HtmlInputText txtCodpro;
    private HtmlInputText txtDespro;
    private HtmlInputText txtEstilo;
    private HtmlInputText txtFuelle;
    private HtmlInputText txtHijpro;
    private HtmlInputText txtKitpro;
    private HtmlInputText txtLargo;
    private HtmlInputText txtLarsac;
    private HtmlInputText txtMoneda;
    private HtmlInputText txtNumcol;
    private HtmlInputText txtRefere;
    private HtmlInputText txtTippro;
    private HtmlInputText txtUndmed;
    private HtmlInputText txtCodCli;
    private HtmlInputText txtCodpla;
    private HtmlInputText txtCodScc;
    private HtmlInputText txtPrecio;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<BvpPrecio> bvpPrecio;
    private List<BvpPrecioDTO> bvpPrecioDTO;

    
    private HtmlCommandButton btnConcultar;
    private HtmlSelectOneMenu somPlanta;
    private List<SelectItem> siPlanta;
	private List<ProductoVO> listaProductos;
	
	private String styleDataTableVpe;
    private String styleDivFechaGeneracion;
    private Date fechaGeneracion;

    public PdfsFaltantesView() {
        super("");
        //Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        
        styleDataTableVpe = "display:none";
		styleDivFechaGeneracion = "display:none";
		
		try{
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
        	siPlanta = new ArrayList<SelectItem>();
        	listaProductos = new ArrayList<ProductoVO>();   
			
			//se crear los listados
        	List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			
			//se cargan los valores de las listas
			listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
			//listaBvpPrecio = BusinessDelegatorView.getBvpPrecio();
			        
			//lista de plantas
        	siPlanta.add(siNulo);
        	for(TsccvPlanta objetoPlanta: listadoPlantas){        		
        		//se agrega validacion para que solo aparescan plantas con tipo de producto corrugado o sacos (agregado el 2010-03-19)
        		if(!objetoPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_CORRUGADO"))) &&
        		   !objetoPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_SACOS")))){
        			continue;
        		}
        		
        		SelectItem siPlan = new SelectItem();        		
        		siPlan.setLabel(objetoPlanta.getDespla());
        		siPlan.setValue(objetoPlanta.getCodpla());
        		
        		siPlanta.add(siPlan);
        	}
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
    }

    public String action_clear() {
        txtAlto.setValue(null);
        txtAlto.setDisabled(true);
        txtAncho.setValue(null);
        txtAncho.setDisabled(true);
        txtAncsac.setValue(null);
        txtAncsac.setDisabled(true);
        txtClave.setValue(null);
        txtClave.setDisabled(true);
        txtCntmax.setValue(null);
        txtCntmax.setDisabled(true);
        txtCntmin.setValue(null);
        txtCntmin.setDisabled(true);
        txtCodpro.setValue(null);
        txtCodpro.setDisabled(true);
        txtDespro.setValue(null);
        txtDespro.setDisabled(true);
        txtEstilo.setValue(null);
        txtEstilo.setDisabled(true);
        txtFuelle.setValue(null);
        txtFuelle.setDisabled(true);
        txtHijpro.setValue(null);
        txtHijpro.setDisabled(true);
        txtKitpro.setValue(null);
        txtKitpro.setDisabled(true);
        txtLargo.setValue(null);
        txtLargo.setDisabled(true);
        txtLarsac.setValue(null);
        txtLarsac.setDisabled(true);
        txtMoneda.setValue(null);
        txtMoneda.setDisabled(true);
        txtNumcol.setValue(null);
        txtNumcol.setDisabled(true);
        txtRefere.setValue(null);
        txtRefere.setDisabled(true);
        txtTippro.setValue(null);
        txtTippro.setDisabled(true);
        txtUndmed.setValue(null);
        txtUndmed.setDisabled(true);

        txtCodCli.setValue(null);
        txtCodCli.setDisabled(false);
        txtCodpla.setValue(null);
        txtCodpla.setDisabled(false);
        txtCodScc.setValue(null);
        txtCodScc.setDisabled(false);
        txtPrecio.setValue(null);
        txtPrecio.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            BvpPrecio entity = null;

            try {
                BvpPrecioId id = new BvpPrecioId();
                id.setCodCli((((txtCodCli.getValue()) == null) ||
                    (txtCodCli.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodCli.getValue().toString()));
                id.setCodpla((((txtCodpla.getValue()) == null) ||
                    (txtCodpla.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodpla.getValue().toString()));
                id.setCodScc((((txtCodScc.getValue()) == null) ||
                    (txtCodScc.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodScc.getValue().toString()));
                id.setPrecio((((txtPrecio.getValue()) == null) ||
                    (txtPrecio.getValue()).equals("")) ? null
                                                       : new String(
                        txtPrecio.getValue().toString()));

                entity = BusinessDelegatorView.getBvpPrecio(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtAlto.setDisabled(false);
                txtAncho.setDisabled(false);
                txtAncsac.setDisabled(false);
                txtClave.setDisabled(false);
                txtCntmax.setDisabled(false);
                txtCntmin.setDisabled(false);
                txtCodpro.setDisabled(false);
                txtDespro.setDisabled(false);
                txtEstilo.setDisabled(false);
                txtFuelle.setDisabled(false);
                txtHijpro.setDisabled(false);
                txtKitpro.setDisabled(false);
                txtLargo.setDisabled(false);
                txtLarsac.setDisabled(false);
                txtMoneda.setDisabled(false);
                txtNumcol.setDisabled(false);
                txtRefere.setDisabled(false);
                txtTippro.setDisabled(false);
                txtUndmed.setDisabled(false);

                txtCodCli.setDisabled(false);
                txtCodpla.setDisabled(false);
                txtCodScc.setDisabled(false);
                txtPrecio.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtAlto.setValue(entity.getAlto());
                txtAlto.setDisabled(false);
                txtAncho.setValue(entity.getAncho());
                txtAncho.setDisabled(false);
                txtAncsac.setValue(entity.getAncsac());
                txtAncsac.setDisabled(false);
                txtClave.setValue(entity.getClave());
                txtClave.setDisabled(false);
                txtCntmax.setValue(entity.getCntmax());
                txtCntmax.setDisabled(false);
                txtCntmin.setValue(entity.getCntmin());
                txtCntmin.setDisabled(false);
                txtCodpro.setValue(entity.getCodpro());
                txtCodpro.setDisabled(false);
                txtDespro.setValue(entity.getDespro());
                txtDespro.setDisabled(false);
                txtEstilo.setValue(entity.getEstilo());
                txtEstilo.setDisabled(false);
                txtFuelle.setValue(entity.getFuelle());
                txtFuelle.setDisabled(false);
                txtHijpro.setValue(entity.getHijpro());
                txtHijpro.setDisabled(false);
                txtKitpro.setValue(entity.getKitpro());
                txtKitpro.setDisabled(false);
                txtLargo.setValue(entity.getLargo());
                txtLargo.setDisabled(false);
                txtLarsac.setValue(entity.getLarsac());
                txtLarsac.setDisabled(false);
                txtMoneda.setValue(entity.getMoneda());
                txtMoneda.setDisabled(false);
                txtNumcol.setValue(entity.getNumcol());
                txtNumcol.setDisabled(false);
                txtRefere.setValue(entity.getRefere());
                txtRefere.setDisabled(false);
                txtTippro.setValue(entity.getTippro());
                txtTippro.setDisabled(false);
                txtUndmed.setValue(entity.getUndmed());
                txtUndmed.setDisabled(false);

                txtCodCli.setValue(entity.getId().getCodCli());
                txtCodCli.setDisabled(true);
                txtCodpla.setValue(entity.getId().getCodpla());
                txtCodpla.setDisabled(true);
                txtCodScc.setValue(entity.getId().getCodScc());
                txtCodScc.setDisabled(true);
                txtPrecio.setValue(entity.getId().getPrecio());
                txtPrecio.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveBvpPrecio((((txtCodCli.getValue()) == null) ||
                (txtCodCli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodCli.getValue().toString()),
                (((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtCodScc.getValue()) == null) ||
                (txtCodScc.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodScc.getValue().toString()),
                (((txtPrecio.getValue()) == null) ||
                (txtPrecio.getValue()).equals("")) ? null
                                                   : new String(
                    txtPrecio.getValue().toString()),
                (((txtAlto.getValue()) == null) ||
                (txtAlto.getValue()).equals("")) ? null
                                                 : new String(
                    txtAlto.getValue().toString()),
                (((txtAncho.getValue()) == null) ||
                (txtAncho.getValue()).equals("")) ? null
                                                  : new String(
                    txtAncho.getValue().toString()),
                (((txtAncsac.getValue()) == null) ||
                (txtAncsac.getValue()).equals("")) ? null
                                                   : new String(
                    txtAncsac.getValue().toString()),
                (((txtClave.getValue()) == null) ||
                (txtClave.getValue()).equals("")) ? null
                                                  : new String(
                    txtClave.getValue().toString()),
                (((txtCntmax.getValue()) == null) ||
                (txtCntmax.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntmax.getValue().toString()),
                (((txtCntmin.getValue()) == null) ||
                (txtCntmin.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntmin.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtEstilo.getValue()) == null) ||
                (txtEstilo.getValue()).equals("")) ? null
                                                   : new String(
                    txtEstilo.getValue().toString()),
                (((txtFuelle.getValue()) == null) ||
                (txtFuelle.getValue()).equals("")) ? null
                                                   : new String(
                    txtFuelle.getValue().toString()),
                (((txtHijpro.getValue()) == null) ||
                (txtHijpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtHijpro.getValue().toString()),
                (((txtKitpro.getValue()) == null) ||
                (txtKitpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtKitpro.getValue().toString()),
                (((txtLargo.getValue()) == null) ||
                (txtLargo.getValue()).equals("")) ? null
                                                  : new String(
                    txtLargo.getValue().toString()),
                (((txtLarsac.getValue()) == null) ||
                (txtLarsac.getValue()).equals("")) ? null
                                                   : new String(
                    txtLarsac.getValue().toString()),
                (((txtMoneda.getValue()) == null) ||
                (txtMoneda.getValue()).equals("")) ? null
                                                   : new String(
                    txtMoneda.getValue().toString()),
                (((txtNumcol.getValue()) == null) ||
                (txtNumcol.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumcol.getValue().toString()),
                (((txtRefere.getValue()) == null) ||
                (txtRefere.getValue()).equals("")) ? null
                                                   : new String(
                    txtRefere.getValue().toString()),
                (((txtTippro.getValue()) == null) ||
                (txtTippro.getValue()).equals("")) ? null
                                                   : new String(
                    txtTippro.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteBvpPrecio((((txtCodCli.getValue()) == null) ||
                (txtCodCli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodCli.getValue().toString()),
                (((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtCodScc.getValue()) == null) ||
                (txtCodScc.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodScc.getValue().toString()),
                (((txtPrecio.getValue()) == null) ||
                (txtPrecio.getValue()).equals("")) ? null
                                                   : new String(
                    txtPrecio.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateBvpPrecio((((txtCodCli.getValue()) == null) ||
                (txtCodCli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodCli.getValue().toString()),
                (((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtCodScc.getValue()) == null) ||
                (txtCodScc.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodScc.getValue().toString()),
                (((txtPrecio.getValue()) == null) ||
                (txtPrecio.getValue()).equals("")) ? null
                                                   : new String(
                    txtPrecio.getValue().toString()),
                (((txtAlto.getValue()) == null) ||
                (txtAlto.getValue()).equals("")) ? null
                                                 : new String(
                    txtAlto.getValue().toString()),
                (((txtAncho.getValue()) == null) ||
                (txtAncho.getValue()).equals("")) ? null
                                                  : new String(
                    txtAncho.getValue().toString()),
                (((txtAncsac.getValue()) == null) ||
                (txtAncsac.getValue()).equals("")) ? null
                                                   : new String(
                    txtAncsac.getValue().toString()),
                (((txtClave.getValue()) == null) ||
                (txtClave.getValue()).equals("")) ? null
                                                  : new String(
                    txtClave.getValue().toString()),
                (((txtCntmax.getValue()) == null) ||
                (txtCntmax.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntmax.getValue().toString()),
                (((txtCntmin.getValue()) == null) ||
                (txtCntmin.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntmin.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtEstilo.getValue()) == null) ||
                (txtEstilo.getValue()).equals("")) ? null
                                                   : new String(
                    txtEstilo.getValue().toString()),
                (((txtFuelle.getValue()) == null) ||
                (txtFuelle.getValue()).equals("")) ? null
                                                   : new String(
                    txtFuelle.getValue().toString()),
                (((txtHijpro.getValue()) == null) ||
                (txtHijpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtHijpro.getValue().toString()),
                (((txtKitpro.getValue()) == null) ||
                (txtKitpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtKitpro.getValue().toString()),
                (((txtLargo.getValue()) == null) ||
                (txtLargo.getValue()).equals("")) ? null
                                                  : new String(
                    txtLargo.getValue().toString()),
                (((txtLarsac.getValue()) == null) ||
                (txtLarsac.getValue()).equals("")) ? null
                                                   : new String(
                    txtLarsac.getValue().toString()),
                (((txtMoneda.getValue()) == null) ||
                (txtMoneda.getValue()).equals("")) ? null
                                                   : new String(
                    txtMoneda.getValue().toString()),
                (((txtNumcol.getValue()) == null) ||
                (txtNumcol.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumcol.getValue().toString()),
                (((txtRefere.getValue()) == null) ||
                (txtRefere.getValue()).equals("")) ? null
                                                   : new String(
                    txtRefere.getValue().toString()),
                (((txtTippro.getValue()) == null) ||
                (txtTippro.getValue()).equals("")) ? null
                                                   : new String(
                    txtTippro.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String codCli, String codpla,
        String codScc, String precio, String alto, String ancho, String ancsac,
        String clave, String cntmax, String cntmin, String codpro,
        String despro, String estilo, String fuelle, String hijpro,
        String kitpro, String largo, String larsac, String moneda,
        String numcol, String refere, String tippro, String undmed)
        throws Exception {
        try {
            BusinessDelegatorView.updateBvpPrecio(codCli, codpla, codScc,
                precio, alto, ancho, ancsac, clave, cntmax, cntmin, codpro,
                despro, estilo, fuelle, hijpro, kitpro, largo, larsac, moneda,
                numcol, refere, tippro, undmed);
            renderManager.getOnDemandRenderer("BvpPrecioView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("BvpPrecioView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<BvpPrecio> getBvpPrecio() {
        if (flag) {
            try {
                bvpPrecio = BusinessDelegatorView.getBvpPrecio();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return bvpPrecio;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setBvpPrecio(List<BvpPrecio> bvpPrecio) {
        this.bvpPrecio = bvpPrecio;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberBvpPrecio() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<BvpPrecio> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberBvpPrecio = 0;

        try {
            totalNumberBvpPrecio = BusinessDelegatorView.findTotalNumberBvpPrecio()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberBvpPrecio) {
            endIndex = totalNumberBvpPrecio;
        }

        try {
            bvpPrecio = BusinessDelegatorView.findPageBvpPrecio(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<BvpPrecio>(totalNumberBvpPrecio, startRow, bvpPrecio);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<BvpPrecioDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberBvpPrecio = 0;

        try {
            totalNumberBvpPrecio = BusinessDelegatorView.findTotalNumberBvpPrecio()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberBvpPrecio) {
            endIndex = totalNumberBvpPrecio;
        }

        try {
            bvpPrecio = BusinessDelegatorView.findPageBvpPrecio(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(bvpPrecio)) {
                bvpPrecioDTO = new ArrayList<BvpPrecioDTO>();

                for (BvpPrecio bvpPrecioTmp : bvpPrecio) {
                    BvpPrecioDTO bvpPrecioDTO2 = new BvpPrecioDTO();
                    bvpPrecioDTO2.setCodCli(bvpPrecioTmp.getId().getCodCli()
                                                        .toString());
                    bvpPrecioDTO2.setCodpla(bvpPrecioTmp.getId().getCodpla()
                                                        .toString());
                    bvpPrecioDTO2.setCodScc(bvpPrecioTmp.getId().getCodScc()
                                                        .toString());
                    bvpPrecioDTO2.setPrecio(bvpPrecioTmp.getId().getPrecio()
                                                        .toString());

                    bvpPrecioDTO2.setAlto((bvpPrecioTmp.getAlto() != null)
                        ? bvpPrecioTmp.getAlto().toString() : null);
                    bvpPrecioDTO2.setAncho((bvpPrecioTmp.getAncho() != null)
                        ? bvpPrecioTmp.getAncho().toString() : null);
                    bvpPrecioDTO2.setAncsac((bvpPrecioTmp.getAncsac() != null)
                        ? bvpPrecioTmp.getAncsac().toString() : null);
                    bvpPrecioDTO2.setClave((bvpPrecioTmp.getClave() != null)
                        ? bvpPrecioTmp.getClave().toString() : null);
                    bvpPrecioDTO2.setCntmax((bvpPrecioTmp.getCntmax() != null)
                        ? bvpPrecioTmp.getCntmax().toString() : null);
                    bvpPrecioDTO2.setCntmin((bvpPrecioTmp.getCntmin() != null)
                        ? bvpPrecioTmp.getCntmin().toString() : null);
                    bvpPrecioDTO2.setCodpro((bvpPrecioTmp.getCodpro() != null)
                        ? bvpPrecioTmp.getCodpro().toString() : null);
                    bvpPrecioDTO2.setDespro((bvpPrecioTmp.getDespro() != null)
                        ? bvpPrecioTmp.getDespro().toString() : null);
                    bvpPrecioDTO2.setEstilo((bvpPrecioTmp.getEstilo() != null)
                        ? bvpPrecioTmp.getEstilo().toString() : null);
                    bvpPrecioDTO2.setFuelle((bvpPrecioTmp.getFuelle() != null)
                        ? bvpPrecioTmp.getFuelle().toString() : null);
                    bvpPrecioDTO2.setHijpro((bvpPrecioTmp.getHijpro() != null)
                        ? bvpPrecioTmp.getHijpro().toString() : null);
                    bvpPrecioDTO2.setKitpro((bvpPrecioTmp.getKitpro() != null)
                        ? bvpPrecioTmp.getKitpro().toString() : null);
                    bvpPrecioDTO2.setLargo((bvpPrecioTmp.getLargo() != null)
                        ? bvpPrecioTmp.getLargo().toString() : null);
                    bvpPrecioDTO2.setLarsac((bvpPrecioTmp.getLarsac() != null)
                        ? bvpPrecioTmp.getLarsac().toString() : null);
                    bvpPrecioDTO2.setMoneda((bvpPrecioTmp.getMoneda() != null)
                        ? bvpPrecioTmp.getMoneda().toString() : null);
                    bvpPrecioDTO2.setNumcol((bvpPrecioTmp.getNumcol() != null)
                        ? bvpPrecioTmp.getNumcol().toString() : null);
                    bvpPrecioDTO2.setRefere((bvpPrecioTmp.getRefere() != null)
                        ? bvpPrecioTmp.getRefere().toString() : null);
                    bvpPrecioDTO2.setTippro((bvpPrecioTmp.getTippro() != null)
                        ? bvpPrecioTmp.getTippro().toString() : null);
                    bvpPrecioDTO2.setUndmed((bvpPrecioTmp.getUndmed() != null)
                        ? bvpPrecioTmp.getUndmed().toString() : null);
                    bvpPrecioDTO2.setBvpPrecio(bvpPrecioTmp);
                    //bvpPrecioDTO2.setBvpPrecioView(this);
                    bvpPrecioDTO.add(bvpPrecioDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<BvpPrecioDTO>(totalNumberBvpPrecio, startRow,
            bvpPrecioDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(bvpPrecioDTO)) {
            for (BvpPrecioDTO bvpPrecioTmp : bvpPrecioDTO) {
                renderManager.getOnDemandRenderer("BvpPrecioView").remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(bvpPrecioDTO)) {
            for (BvpPrecioDTO bvpPrecioTmp : bvpPrecioDTO) {
                renderManager.getOnDemandRenderer("BvpPrecioView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtAlto() {
        return txtAlto;
    }

    public void setTxtAlto(HtmlInputText txtAlto) {
        this.txtAlto = txtAlto;
    }

    public HtmlInputText getTxtAncho() {
        return txtAncho;
    }

    public void setTxtAncho(HtmlInputText txtAncho) {
        this.txtAncho = txtAncho;
    }

    public HtmlInputText getTxtAncsac() {
        return txtAncsac;
    }

    public void setTxtAncsac(HtmlInputText txtAncsac) {
        this.txtAncsac = txtAncsac;
    }

    public HtmlInputText getTxtClave() {
        return txtClave;
    }

    public void setTxtClave(HtmlInputText txtClave) {
        this.txtClave = txtClave;
    }

    public HtmlInputText getTxtCntmax() {
        return txtCntmax;
    }

    public void setTxtCntmax(HtmlInputText txtCntmax) {
        this.txtCntmax = txtCntmax;
    }

    public HtmlInputText getTxtCntmin() {
        return txtCntmin;
    }

    public void setTxtCntmin(HtmlInputText txtCntmin) {
        this.txtCntmin = txtCntmin;
    }

    public HtmlInputText getTxtCodpro() {
        return txtCodpro;
    }

    public void setTxtCodpro(HtmlInputText txtCodpro) {
        this.txtCodpro = txtCodpro;
    }

    public HtmlInputText getTxtDespro() {
        return txtDespro;
    }

    public void setTxtDespro(HtmlInputText txtDespro) {
        this.txtDespro = txtDespro;
    }

    public HtmlInputText getTxtEstilo() {
        return txtEstilo;
    }

    public void setTxtEstilo(HtmlInputText txtEstilo) {
        this.txtEstilo = txtEstilo;
    }

    public HtmlInputText getTxtFuelle() {
        return txtFuelle;
    }

    public void setTxtFuelle(HtmlInputText txtFuelle) {
        this.txtFuelle = txtFuelle;
    }

    public HtmlInputText getTxtHijpro() {
        return txtHijpro;
    }

    public void setTxtHijpro(HtmlInputText txtHijpro) {
        this.txtHijpro = txtHijpro;
    }

    public HtmlInputText getTxtKitpro() {
        return txtKitpro;
    }

    public void setTxtKitpro(HtmlInputText txtKitpro) {
        this.txtKitpro = txtKitpro;
    }

    public HtmlInputText getTxtLargo() {
        return txtLargo;
    }

    public void setTxtLargo(HtmlInputText txtLargo) {
        this.txtLargo = txtLargo;
    }

    public HtmlInputText getTxtLarsac() {
        return txtLarsac;
    }

    public void setTxtLarsac(HtmlInputText txtLarsac) {
        this.txtLarsac = txtLarsac;
    }

    public HtmlInputText getTxtMoneda() {
        return txtMoneda;
    }

    public void setTxtMoneda(HtmlInputText txtMoneda) {
        this.txtMoneda = txtMoneda;
    }

    public HtmlInputText getTxtNumcol() {
        return txtNumcol;
    }

    public void setTxtNumcol(HtmlInputText txtNumcol) {
        this.txtNumcol = txtNumcol;
    }

    public HtmlInputText getTxtRefere() {
        return txtRefere;
    }

    public void setTxtRefere(HtmlInputText txtRefere) {
        this.txtRefere = txtRefere;
    }

    public HtmlInputText getTxtTippro() {
        return txtTippro;
    }

    public void setTxtTippro(HtmlInputText txtTippro) {
        this.txtTippro = txtTippro;
    }

    public HtmlInputText getTxtUndmed() {
        return txtUndmed;
    }

    public void setTxtUndmed(HtmlInputText txtUndmed) {
        this.txtUndmed = txtUndmed;
    }

    public HtmlInputText getTxtCodCli() {
        return txtCodCli;
    }

    public void setTxtCodCli(HtmlInputText txtCodCli) {
        this.txtCodCli = txtCodCli;
    }

    public HtmlInputText getTxtCodpla() {
        return txtCodpla;
    }

    public void setTxtCodpla(HtmlInputText txtCodpla) {
        this.txtCodpla = txtCodpla;
    }

    public HtmlInputText getTxtCodScc() {
        return txtCodScc;
    }

    public void setTxtCodScc(HtmlInputText txtCodScc) {
        this.txtCodScc = txtCodScc;
    }

    public HtmlInputText getTxtPrecio() {
        return txtPrecio;
    }

    public void setTxtPrecio(HtmlInputText txtPrecio) {
        this.txtPrecio = txtPrecio;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<BvpPrecioDTO> getBvpPrecioDTO() {
        return bvpPrecioDTO;
    }

    public void setBvpPrecioDTO(List<BvpPrecioDTO> bvpPrecioDTO) {
        this.bvpPrecioDTO = bvpPrecioDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }
    
    
    public HtmlCommandButton getBtnConcultar() {
		return btnConcultar;
	}

	public void setBtnConcultar(HtmlCommandButton btnConcultar) {
		this.btnConcultar = btnConcultar;
	}

	public HtmlSelectOneMenu getSomPlanta() {
		return somPlanta;
	}

	public void setSomPlanta(HtmlSelectOneMenu somPlanta) {
		this.somPlanta = somPlanta;
	}

	public List<SelectItem> getSiPlanta() {
		return siPlanta;
	}

	public void setSiPlanta(List<SelectItem> siPlanta) {
		this.siPlanta = siPlanta;
	}

	public List<ProductoVO> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProductoVO> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public String getStyleDataTableVpe() {
		return styleDataTableVpe;
	}

	public void setStyleDataTableVpe(String styleDataTableVpe) {
		this.styleDataTableVpe = styleDataTableVpe;
	}

	public Date getFechaGeneracion() {
		fechaGeneracion = new Date();
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public String getStyleDivFechaGeneracion() {
		return styleDivFechaGeneracion;
	}

	public void setStyleDivFechaGeneracion(String styleDivFechaGeneracion) {
		this.styleDivFechaGeneracion = styleDivFechaGeneracion;
	}
	
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<BvpPrecio> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<BvpPrecioDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
    
    
    //Metodo encargado de consultar el parametro para el campo claveBusqueda
    public String[] getValorParametroClaBusq(){    	
    	//ARCHIVO PORPIEDADES
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String temp[] = null;
        try {
        	temp = archivoPropiedades.getProperty("LISTA_CLAVE_BUSQUEDA").toString().split(",");
        	
			return temp;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
		return null;           
    }    
    
    public void ocultarFechaGeneracion(ValueChangeEvent event) {    	
        styleDivFechaGeneracion = "display:none";
        styleDataTableVpe = "display:none";
		
		listaProductos.clear();
    }
    

    public void consultarPdfsFaltantes(){
    	try{
    		TsccvPlanta tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(somPlanta.getValue().toString());
    		
    		listaProductos = BusinessDelegatorView.consultarPdfsFaltantes(tsccvPlanta);
    		
    		styleDataTableVpe = "display:block";    		
    		styleDivFechaGeneracion = "display:block";
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
		}catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    }


}



