package co.com.smurfit.presentation.backEndBeans;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import co.com.smurfit.dataaccess.dao.TsccvUsuarioDAO;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.utilities.Utilities;

public class InicioView {

	private String loginTxt;
	private String passTxt;
	
	//SE AGREGAN LOS VALORES DEL ROWS Y COLS DEL INPUTTEXTAREA
    private Integer rows = 6;
    private Integer cols = 30;
	
    public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public Integer getCols() {
		return cols;
	}

	public void setCols(Integer cols) {
		this.cols = cols;
	}
    
	public String getLoginTxt() {
		return loginTxt;
	}

	public void setLoginTxt(String loginTxt) {
		this.loginTxt = loginTxt;
	}

	public String getPassTxt() {
		return passTxt;
	}

	public void setPassTxt(String passTxt) {
		this.passTxt = passTxt;
	}

	public String doBotonEntrarAction(){
		String mensaje=null;
		//Cargarmos el properties para mostrar el titulo
        ArchivoBundle archivoBondle = new ArchivoBundle();
		try {
			//Validamos que hayan ingresado tanto el usuario como la contraseņa
			if(loginTxt == null || loginTxt.equals("") || passTxt == null || passTxt.equals("")){
				mensaje = archivoBondle.getProperty("login.mensajeerror");
				throw new Exception(mensaje);
			}
			
			System.out.println("***ENTRA VALIDACION USUARIO");
			//Validamos si el usuario y la contraseņa son correctos
			validacionUsuario(loginTxt, passTxt);
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("",new FacesMessage(FacesMessage.SEVERITY_INFO, e.getMessage(),null));
			return null;
		}
			
		//Adicionamos el flag a la session
		/*HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.setAttribute("isAuthenticated", "true");*/
		
		//Si no hay errores redireccionamos a la pantalla inicial de la aplicacion
		System.out.println("***ENVIA RESPUESTA");
		return "goMenu";
	}
	
	
	private void validacionUsuario(String usuario, String clave) throws Exception {		
		ArchivoBundle archivoBondle = new ArchivoBundle();
		boolean accesoAutorizado = false;
		String mensaje = null;
		HttpSession session =null;
		HttpServletRequest request=null;
		
		try {
			TsccvUsuario tsccvUsuario = null;
			request=(HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String URL= request.getRequestURL().toString();
			String[] datos= URL.split(":");
			if(datos[1].indexOf("skccvirtual") != -1){
				Utilities.RUTA_SERVER_BASE="http://skccvirtual/";
			}
			System.out.println("Inicia consulta usuario");
			tsccvUsuario = BusinessDelegatorView.consultarUsuarioLogin(TsccvUsuarioDAO.LOGIN, usuario);
			System.out.println("Termina consulta usuario");
						
			if(tsccvUsuario != null){
				if(tsccvUsuario.getPassword().equals(clave)){
					accesoAutorizado = true;
				}
				else{
					mensaje = archivoBondle.getProperty("login.contraseņa_incorrecta");
					throw new Exception(mensaje);
				}
				
				//Si el usuario esta inactivo se muestra mensaje
				if(tsccvUsuario.getActivo().equals("n") || tsccvUsuario.getActivo().equals("N")){
					mensaje = archivoBondle.getProperty("login.usuario_inactivo");
					throw new Exception(mensaje);
				}
			}
			else{
				accesoAutorizado = false;
			}
			
			
			if (!accesoAutorizado){
				mensaje = archivoBondle.getProperty("login.usuario_no_registrado");
				throw new Exception(mensaje);
			}//fin if
			
			//Colocamos el usuario en session
			session= (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
			session.setAttribute(Utilities.LLAVE_USUARIO, tsccvUsuario);
			
			//Eliminamos de la session el mb de usuario para evitar inconsistencias
			session.removeAttribute("MBUsuario");
			
			//registramos el acceso usuario
			//BusinessDelegatorView.saveTsccvAccesosUsuario(new Date(), null, tsccvUsuario.getIdUsua(), tsccvUsuario.getTsccvEmpresas().getIdEmpr());
			BusinessDelegatorView.saveTsccvEstadisticaAccPed(new Long(1), new Date(), null, new Long(0),
									tsccvUsuario.getTbmBaseGruposUsuarios().getIdGrup(), tsccvUsuario.getTsccvEmpresas().getIdEmpr(), 
									tsccvUsuario.getTsccvPlanta().getCodpla(), new Long(1), tsccvUsuario.getIdUsua());
		}catch (Exception e) {
			throw e;
		}
	}
	
	public String cerrarSession(){
		HttpSession session= (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		if(session != null){
			session.removeAttribute(Utilities.LLAVE_USUARIO);
			session.removeAttribute("MBUsuario");
			session.invalidate();
		}
		return "goInicio";
	}
	
	public String getNombreUsuario(){
		HttpSession session= (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		TsccvUsuario tsccvUsuario = (TsccvUsuario) session.getAttribute(Utilities.LLAVE_USUARIO);
		return tsccvUsuario != null ? tsccvUsuario.getNombre()+" "+tsccvUsuario.getApellido().toUpperCase() : "";
		
	}
	
}
