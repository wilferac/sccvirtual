package co.com.smurfit.presentation.backEndBeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.FactoryFinder;
import javax.faces.application.ApplicationFactory;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.event.ActionEvent;

import com.icesoft.faces.component.ext.HtmlInputText;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.utilities.FacesUtils;

public class BusquedaEmpresaView {
    private HtmlInputText txtCodMasBusq;
    private HtmlInputText txtCodSapBusq;
    private HtmlInputText txtCodVpeBusq;
	private HtmlInputText txtNombreBusq;
	private boolean visible;
    private String managedBeanDestino;
    private List<TsccvEmpresas> listadoEmpresas;
    private Object manageBeanLlmado;//contiene el managebean que realizo el llamado
    
    public BusquedaEmpresaView() {
    	visible = false;
    	
    	listadoEmpresas = new ArrayList<TsccvEmpresas>();
    	txtNombreBusq = new HtmlInputText();
    	txtCodMasBusq = new HtmlInputText();
    	txtCodSapBusq = new HtmlInputText();
    	txtCodVpeBusq = new HtmlInputText();
    	//buscarEmpresa();
	}
    
    
    
	
	public HtmlInputText getTxtCodMasBusq() {
		return txtCodMasBusq;
	}

	public void setTxtCodMasBusq(HtmlInputText txtCodMasBusq) {
		this.txtCodMasBusq = txtCodMasBusq;
	}

	public HtmlInputText getTxtCodSapBusq() {
		return txtCodSapBusq;
	}

	public void setTxtCodSapBusq(HtmlInputText txtCodSapBusq) {
		this.txtCodSapBusq = txtCodSapBusq;
	}

	public HtmlInputText getTxtCodVpeBusq() {
		return txtCodVpeBusq;
	}

	public void setTxtCodVpeBusq(HtmlInputText txtCodVpeBusq) {
		this.txtCodVpeBusq = txtCodVpeBusq;
	}

	public HtmlInputText getTxtNombreBusq() {
		return txtNombreBusq;
	}

	public void setTxtNombreBusq(HtmlInputText txtNombreBusq) {
		this.txtNombreBusq = txtNombreBusq;
	}
	
	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getManagedBeanDestino() {
		return managedBeanDestino;
	}

	public void setManagedBeanDestino(String managedBeanDestino) {
		this.managedBeanDestino = managedBeanDestino;
	}

	public List<TsccvEmpresas> getListadoEmpresas() {
		return listadoEmpresas;
	}

	public void setListadoEmpresas(List<TsccvEmpresas> listadoEmpresas) {
		this.listadoEmpresas = listadoEmpresas;
	}

	public Object getManageBeanLlmado() {
		return manageBeanLlmado;
	}

	public void setManageBeanLlmado(Object manageBeanLlmado) {
		this.manageBeanLlmado = manageBeanLlmado;
	}
	
	
	//metodo encargado de limpiar el campo de ingreso
	public void limpiar(){
		txtNombreBusq.setValue(null);
		txtCodSapBusq.setValue(null);
		txtCodMasBusq.setValue(null);
		txtCodVpeBusq.setValue(null);
		
		listadoEmpresas.clear();
	}
	
    //metodo encargado de desplegar el panel
    public void mostrarPanel(){
    	this.visible = true;
    	
    	//si el manageBean que realiza el llamado no es el de usuario entonces se muestran los registros inicialmente
    	if(!(manageBeanLlmado instanceof TsccvUsuarioView)){
    		buscarEmpresa();
    	}
    }

	public String ocultarPanel() {
		txtNombreBusq.setValue(null);
		txtCodSapBusq.setValue(null);
		txtCodMasBusq.setValue(null);
		txtCodVpeBusq.setValue(null);
		listadoEmpresas.clear();
		
    	String gui = "";
		Object objReturn = null;
		visible = false;
		String strDestino = this.getManagedBeanDestino();
		if (strDestino != null && strDestino.length() > 0) {
			MethodBinding metodo = getMethodBinding(strDestino, null);
			objReturn = metodo.invoke(FacesContext.getCurrentInstance(), null);
			if (objReturn != null) {
				gui = (String) objReturn;
			}
		}
		//FacesContext.getCurrentInstance().notifyAll();
		
		return gui;	
	}
    
    public static MethodBinding getMethodBinding(String bindingName,
			Class params[]) {
		ApplicationFactory factory = (ApplicationFactory) FactoryFinder
				.getFactory(FactoryFinder.APPLICATION_FACTORY);
		MethodBinding binding = factory.getApplication().createMethodBinding(
				bindingName, params);
		return binding;
	}

    //metodo encargado de consultar las empresas por el nombre
    public void buscarEmpresa() {
    	List<TsccvEmpresas> registros=null;
    	try {
    		//se obtiene el usuario de la sesion
    		MBUsuario mbUsu = (MBUsuario)FacesUtils.getManagedBean("MBUsuario");
    		TsccvUsuario userLogin = mbUsu.getUsuario();
    		
            //se limpia el contenido actual la lista 
            listadoEmpresas.clear();
            
            registros = BusinessDelegatorView.buscarEmpresa(
                    (((txtCodMasBusq.getValue()) == null) ||
                    (txtCodMasBusq.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodMasBusq.getValue().toString()),
                    (((txtCodSapBusq.getValue()) == null) ||
                    (txtCodSapBusq.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodSapBusq.getValue().toString()),
                    (((txtCodVpeBusq.getValue()) == null) ||
                    (txtCodVpeBusq.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodVpeBusq.getValue().toString()),
                    (((txtNombreBusq.getValue()) == null) ||
                    (txtNombreBusq.getValue()).equals("")) ? null
                                                       : new String(
        		    txtNombreBusq.getValue().toString()).toUpperCase(),
                    (userLogin != null && userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null) ? 
                    userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa().getIdGrue() : null, manageBeanLlmado);
            //se envia el manageBean que realiza el llamado para validar si se muestran los grupos o no
			
			if(registros==null || registros.size()==0){
				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	          	mensaje.mostrarMensaje("No se encontraron registros", MBMensaje.MENSAJE_ALERTA);				
				return;
			}
			
			listadoEmpresas = registros;
			
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    }
    
    //SE AGREGA EL METODO ASOCIADO AL ENLACE EN LA TABLA
    public void select_txtId(ActionEvent event) {
    	//se declaran los managebean que utilizan esta pantalla
    	IngresoOrdenesView ingresoOrdenesView = null;
    	TsccvUsuarioView tsccvUsuarioView = null;
    	ConsultaPedidosView consultaPedidosView = null;
    	EstadisticasDeCompraView estadisticasDeCompraView = null;    
    	ListaPreciosView listaPreciosView = null;
    	EstadoDeCuentaView estadoDeCuentaView = null;
    	    	    	
    	//se obtiene el atributo enviado mediante el link de la tabla
    	TsccvEmpresas tsccvEmpresa = (TsccvEmpresas) event.getComponent().getAttributes().get("tsccvEmpresa");

    	//se valida de que magebean es la instancia manageBeabLlmado
    	if(manageBeanLlmado instanceof IngresoOrdenesView){
        	//se castea el objeto manageBeabLlmado al tipo correto
    		ingresoOrdenesView = (IngresoOrdenesView) manageBeanLlmado;    		
    		
        	//se hace el llamedo al metodo que coloca la seleccion
    		ingresoOrdenesView.colocarSeleccion(tsccvEmpresa.getIdEmpr(), tsccvEmpresa.getNombre());
    	}
    	else if(manageBeanLlmado instanceof TsccvUsuarioView){
        	//se castea el objeto manageBeabLlmado al tipo correto
    		tsccvUsuarioView = (TsccvUsuarioView) manageBeanLlmado;
    		
        	//se hace el llamedo al metodo que coloca la seleccion
    		tsccvUsuarioView.colocarSeleccion(tsccvEmpresa.getIdEmpr(), tsccvEmpresa.getNombre());
    	}
    	else if(manageBeanLlmado instanceof ConsultaPedidosView){
        	//se castea el objeto manageBeabLlmado al tipo correto
    		consultaPedidosView = (ConsultaPedidosView) manageBeanLlmado;
    		
        	//se hace el llamedo al metodo que coloca la seleccion
    		consultaPedidosView.colocarSeleccion(tsccvEmpresa.getIdEmpr(), tsccvEmpresa.getNombre());
    	}
    	else if(manageBeanLlmado instanceof EstadisticasDeCompraView){
        	//se castea el objeto manageBeabLlmado al tipo correto
    		estadisticasDeCompraView = (EstadisticasDeCompraView) manageBeanLlmado;
    		
        	//se hace el llamedo al metodo que coloca la seleccion
    		estadisticasDeCompraView.colocarSeleccion(tsccvEmpresa.getIdEmpr(), tsccvEmpresa.getNombre());
    	}
    	else if(manageBeanLlmado instanceof ListaPreciosView){
        	//se castea el objeto manageBeabLlmado al tipo correto
    		listaPreciosView = (ListaPreciosView) manageBeanLlmado;
    		
        	//se hace el llamedo al metodo que coloca la seleccion
    		listaPreciosView.colocarSeleccion(tsccvEmpresa.getIdEmpr(), tsccvEmpresa.getNombre());
    	}
    	else if(manageBeanLlmado instanceof EstadoDeCuentaView){
        	//se castea el objeto manageBeabLlmado al tipo correto
    		estadoDeCuentaView = (EstadoDeCuentaView) manageBeanLlmado;
    		
        	//se hace el llamedo al metodo que coloca la seleccion
    		estadoDeCuentaView.getTxtIdEmpresa().setValue(tsccvEmpresa.getIdEmpr());
    		estadoDeCuentaView.getTxtNombreEmpresa().setValue(tsccvEmpresa.getNombre());
    		//estadisticasDeCompraView.colocarSeleccion(tsccvEmpresa.getIdEmpr(), tsccvEmpresa.getNombre());
    	}
    	
    	//se oculta el panel
    	ocultarPanel();
    }
}
