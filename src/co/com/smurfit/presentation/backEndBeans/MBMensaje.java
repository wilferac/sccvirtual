package co.com.smurfit.presentation.backEndBeans;

import javax.faces.FactoryFinder;
import javax.faces.application.ApplicationFactory;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;

import co.com.smurfit.exceptions.BMBaseException;

import com.icesoft.faces.component.ext.HtmlGraphicImage;
import com.icesoft.faces.component.ext.HtmlOutputText;

//import co.com.coomeva.sipas3.logica.excepciones.ExcepcionesSipas;

public class MBMensaje {

	private boolean visible = false;
	private String mensaje = "";
	private String solucion="";
	private String managedBeanDestino;
	//Variable a utilizar para los titulos de las pantallas
	private String tituloPantalla;
	public static int MENSAJE_OK=1;
    public static int MENSAJE_ERROR=2;
    public static int MENSAJE_ALERTA=3;
    private HtmlOutputText text1;
    private String titulook="Confirmación";
    private String tituloerror="Error";
    private String tituloalerta="Alerta";
    private HtmlGraphicImage image1;

    public MBMensaje(){
		super();
		text1= new HtmlOutputText();
		image1= new HtmlGraphicImage();
	}
    
	public HtmlGraphicImage getImage1() {
		return image1;
	}

	public void setImage1(HtmlGraphicImage image1) {
		this.image1 = image1;
	}

	public HtmlOutputText getText1() {
		return text1;
	}

	public void setText1(HtmlOutputText text1) {
		this.text1 = text1;
	}

	public String getTituloPantalla() { 
		return tituloPantalla;
	}

	public void setTituloPantalla(String tituloPantalla) {
		this.tituloPantalla = tituloPantalla;
	}

	
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

    public void mostrarMensaje(BMBaseException e){
        mostrarMensajeError(e.getDescripcion(), e.getCorreccion(), e.getDato());
    }
	
	public void mostrarMensaje(String mensaje, int codigo) {
		if(codigo ==  MENSAJE_ALERTA){
            solucion=null;
			mostrarMensajeAlerta(mensaje);
        }
        else if(codigo == MENSAJE_ERROR){
            mostrarMensajeError(mensaje,null,null);
        }
        else if(codigo ==  MENSAJE_OK){
            solucion=null;
        	mostrarMensajeOk(mensaje);
        }
		
		//FacesContext.getCurrentInstance().notify();
	}
	
	public void mostrarMensajeError(String descripcion, String solucion, String dato){
		// Muestra el mensaje en el panel popup
		this.mensaje = descripcion;
		this.solucion= solucion;
		if(dato != null){
			mensaje +=": "+dato;
		}
		visible = true;
		text1.setValue(tituloerror);
        image1.setUrl("/images/webdev-cancel-icon.png");
        
    }
    
    public void mostrarMensajeAlerta(String descripcion){
    	this.mensaje = descripcion;
		visible = true;
		text1.setValue(tituloalerta);
        image1.setUrl("/images/webdev-alert-icon.png");
    }
    
    public void mostrarMensajeOk(String descripcion){
    	this.mensaje = descripcion;
		visible = true;
		text1.setValue(titulook);
        image1.setUrl("/images/webdev-ok-icon.png");
    }

	public String aceptar() throws Exception {
		Object objReturn = null;
		try {
			MethodBinding metodo = getMethodBinding(this
					.getManagedBeanDestino(), new Class[] { String.class });
			objReturn = metodo.invoke(FacesContext.getCurrentInstance(),
					new Object[] { null });
		} catch (Exception e) {
			throw new Exception(
					"No se pudo insertar los datos seleccionados en los campos de destino");
		}
		//FacesContext.getCurrentInstance().notifyAll();
		return "ok";
	}

	public String ocultarMensaje() {
		String gui = "";
		Object objReturn = null;
		this.mensaje = "";
		visible = false;
		String strDestino = this.getManagedBeanDestino();
		if (strDestino != null && strDestino.length() > 0) {
			MethodBinding metodo = getMethodBinding(strDestino, null);
			objReturn = metodo.invoke(FacesContext.getCurrentInstance(), null);
			if (objReturn != null) {
				gui = (String) objReturn;
			}
		}
		//FacesContext.getCurrentInstance().notifyAll();
		return gui;
	}
	
	public String volverABandejaSolicitudes() {
		String gui = "";
		Object objReturn = null;
		this.mensaje = "";
		visible = false;
		String strDestino = "#{ingreso.volverABandeja}";
		if (strDestino != null && strDestino.length() > 0) {
			MethodBinding metodo = getMethodBinding(strDestino, null);
			objReturn = metodo.invoke(FacesContext.getCurrentInstance(), null);
			if (objReturn != null) {
				gui = (String) objReturn;
			}
		}
		FacesContext.getCurrentInstance().notifyAll();
		return gui;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public static MethodBinding getMethodBinding(String bindingName,
			Class params[]) {
		ApplicationFactory factory = (ApplicationFactory) FactoryFinder
				.getFactory(FactoryFinder.APPLICATION_FACTORY);
		MethodBinding binding = factory.getApplication().createMethodBinding(
				bindingName, params);
		return binding;
	}

	public String getManagedBeanDestino() {
		return managedBeanDestino;
	}

	public void setManagedBeanDestino(String managedBeanDestino) {
		this.managedBeanDestino = managedBeanDestino;
	}

	public String getSolucion() {
		return solucion;
	}

	public void setSolucion(String solucion) {
		this.solucion = solucion;
	}

}
