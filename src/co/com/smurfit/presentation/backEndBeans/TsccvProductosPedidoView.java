package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEstadisticaAccPedLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoAccionLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvEstadisticaAccPedLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoAccionLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TsccvProductosPedidoDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvEstadisticaAccPed;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoAccion;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvProductosPedidoView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtActivo;
    private HtmlInputText txtCantidadTotal;
    private HtmlInputText txtCodCli;
    private HtmlInputText txtCodScc;
    private HtmlInputText txtCodpla;
    private HtmlInputText txtDespro;
    private HtmlInputText txtDiamtr;
    private HtmlInputText txtEje;
    private HtmlInputText txtMoneda;
    private HtmlInputText txtOrdenCompra;
    private HtmlInputText txtPrecio;
    private HtmlInputText txtUndmed;
    private HtmlInputText txtIdPedi_TsccvPedidos;
    private HtmlInputText txtIdPrpe;
    private SelectInputDate txtFechaCreacion;
    private SelectInputDate txtFechaPrimeraEntrega;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TsccvProductosPedido> tsccvProductosPedido;
    private List<TsccvProductosPedidoDTO> tsccvProductosPedidoDTO;

    public TsccvProductosPedidoView() {
        super("");
    }

    public String action_clear() {
        txtActivo.setValue(null);
        txtActivo.setDisabled(true);
        txtCantidadTotal.setValue(null);
        txtCantidadTotal.setDisabled(true);
        txtCodCli.setValue(null);
        txtCodCli.setDisabled(true);
        txtCodScc.setValue(null);
        txtCodScc.setDisabled(true);
        txtCodpla.setValue(null);
        txtCodpla.setDisabled(true);
        txtDespro.setValue(null);
        txtDespro.setDisabled(true);
        txtDiamtr.setValue(null);
        txtDiamtr.setDisabled(true);
        txtEje.setValue(null);
        txtEje.setDisabled(true);
        txtMoneda.setValue(null);
        txtMoneda.setDisabled(true);
        txtOrdenCompra.setValue(null);
        txtOrdenCompra.setDisabled(true);
        txtPrecio.setValue(null);
        txtPrecio.setDisabled(true);
        txtUndmed.setValue(null);
        txtUndmed.setDisabled(true);
        txtIdPedi_TsccvPedidos.setValue(null);
        txtIdPedi_TsccvPedidos.setDisabled(true);

        txtFechaCreacion.setValue(null);
        txtFechaCreacion.setDisabled(true);
        txtFechaPrimeraEntrega.setValue(null);
        txtFechaPrimeraEntrega.setDisabled(true);

        txtIdPrpe.setValue(null);
        txtIdPrpe.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TsccvProductosPedido entity = null;

            try {
                Integer idPrpe = new Integer(txtIdPrpe.getValue().toString());

                entity = BusinessDelegatorView.getTsccvProductosPedido(idPrpe);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtActivo.setDisabled(false);
                txtCantidadTotal.setDisabled(false);
                txtCodCli.setDisabled(false);
                txtCodScc.setDisabled(false);
                txtCodpla.setDisabled(false);
                txtDespro.setDisabled(false);
                txtDiamtr.setDisabled(false);
                txtEje.setDisabled(false);
                txtMoneda.setDisabled(false);
                txtOrdenCompra.setDisabled(false);
                txtPrecio.setDisabled(false);
                txtUndmed.setDisabled(false);
                txtIdPedi_TsccvPedidos.setDisabled(false);

                txtFechaCreacion.setDisabled(false);
                txtFechaPrimeraEntrega.setDisabled(false);

                txtIdPrpe.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtActivo.setValue(entity.getActivo());
                txtActivo.setDisabled(false);
                txtCantidadTotal.setValue(entity.getCantidadTotal());
                txtCantidadTotal.setDisabled(false);
                txtCodCli.setValue(entity.getCodCli());
                txtCodCli.setDisabled(false);
                txtCodScc.setValue(entity.getCodScc());
                txtCodScc.setDisabled(false);
                txtCodpla.setValue(entity.getCodpla());
                txtCodpla.setDisabled(false);
                txtDespro.setValue(entity.getDespro());
                txtDespro.setDisabled(false);
                txtDiamtr.setValue(entity.getDiamtr());
                txtDiamtr.setDisabled(false);
                txtEje.setValue(entity.getEje());
                txtEje.setDisabled(false);
                txtFechaCreacion.setValue(entity.getFechaCreacion());
                txtFechaCreacion.setDisabled(false);
                txtFechaPrimeraEntrega.setValue(entity.getFechaPrimeraEntrega());
                txtFechaPrimeraEntrega.setDisabled(false);
                txtMoneda.setValue(entity.getMoneda());
                txtMoneda.setDisabled(false);
                txtOrdenCompra.setValue(entity.getOrdenCompra());
                txtOrdenCompra.setDisabled(false);
                txtPrecio.setValue(entity.getPrecio());
                txtPrecio.setDisabled(false);
                txtUndmed.setValue(entity.getUndmed());
                txtUndmed.setDisabled(false);
                txtIdPedi_TsccvPedidos.setValue(entity.getTsccvPedidos()
                                                      .getIdPedi());
                txtIdPedi_TsccvPedidos.setDisabled(false);

                txtIdPrpe.setValue(entity.getIdPrpe());
                txtIdPrpe.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveTsccvProductosPedido((((txtActivo.getValue()) == null) ||
                (txtActivo.getValue()).equals("")) ? null
                                                   : new String(
                    txtActivo.getValue().toString()),
                (((txtCantidadTotal.getValue()) == null) ||
                (txtCantidadTotal.getValue()).equals("")) ? null
                                                          : new Integer(
                    txtCantidadTotal.getValue().toString()),
                (((txtCodCli.getValue()) == null) ||
                (txtCodCli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodCli.getValue().toString()),
                (((txtCodScc.getValue()) == null) ||
                (txtCodScc.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodScc.getValue().toString()),
                (((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtDiamtr.getValue()) == null) ||
                (txtDiamtr.getValue()).equals("")) ? null
                                                   : new String(
                    txtDiamtr.getValue().toString()),
                (((txtEje.getValue()) == null) ||
                (txtEje.getValue()).equals("")) ? null
                                                : new String(
                    txtEje.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtFechaPrimeraEntrega.getValue()) == null) ||
                (txtFechaPrimeraEntrega.getValue()).equals("")) ? null
                                                                : (Date) txtFechaPrimeraEntrega.getValue(),
                (((txtIdPrpe.getValue()) == null) ||
                (txtIdPrpe.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdPrpe.getValue().toString()),
                (((txtMoneda.getValue()) == null) ||
                (txtMoneda.getValue()).equals("")) ? null
                                                   : new String(
                    txtMoneda.getValue().toString()),
                (((txtOrdenCompra.getValue()) == null) ||
                (txtOrdenCompra.getValue()).equals("")) ? null
                                                        : new String(
                    txtOrdenCompra.getValue().toString()),
                (((txtPrecio.getValue()) == null) ||
                (txtPrecio.getValue()).equals("")) ? null
                                                   : new String(
                    txtPrecio.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtIdPedi_TsccvPedidos.getValue()) == null) ||
                (txtIdPedi_TsccvPedidos.getValue()).equals("")) ? null
                                                                : new Integer(
                    txtIdPedi_TsccvPedidos.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTsccvProductosPedido((((txtIdPrpe.getValue()) == null) ||
                (txtIdPrpe.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdPrpe.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateTsccvProductosPedido((((txtActivo.getValue()) == null) ||
                (txtActivo.getValue()).equals("")) ? null
                                                   : new String(
                    txtActivo.getValue().toString()),
                (((txtCantidadTotal.getValue()) == null) ||
                (txtCantidadTotal.getValue()).equals("")) ? null
                                                          : new Integer(
                    txtCantidadTotal.getValue().toString()),
                (((txtCodCli.getValue()) == null) ||
                (txtCodCli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodCli.getValue().toString()),
                (((txtCodScc.getValue()) == null) ||
                (txtCodScc.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodScc.getValue().toString()),
                (((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtDiamtr.getValue()) == null) ||
                (txtDiamtr.getValue()).equals("")) ? null
                                                   : new String(
                    txtDiamtr.getValue().toString()),
                (((txtEje.getValue()) == null) ||
                (txtEje.getValue()).equals("")) ? null
                                                : new String(
                    txtEje.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtFechaPrimeraEntrega.getValue()) == null) ||
                (txtFechaPrimeraEntrega.getValue()).equals("")) ? null
                                                                : (Date) txtFechaPrimeraEntrega.getValue(),
                (((txtIdPrpe.getValue()) == null) ||
                (txtIdPrpe.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdPrpe.getValue().toString()),
                (((txtMoneda.getValue()) == null) ||
                (txtMoneda.getValue()).equals("")) ? null
                                                   : new String(
                    txtMoneda.getValue().toString()),
                (((txtOrdenCompra.getValue()) == null) ||
                (txtOrdenCompra.getValue()).equals("")) ? null
                                                        : new String(
                    txtOrdenCompra.getValue().toString()),
                (((txtPrecio.getValue()) == null) ||
                (txtPrecio.getValue()).equals("")) ? null
                                                   : new String(
                    txtPrecio.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtIdPedi_TsccvPedidos.getValue()) == null) ||
                (txtIdPedi_TsccvPedidos.getValue()).equals("")) ? null
                                                                : new Integer(
                    txtIdPedi_TsccvPedidos.getValue().toString()), null);

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, Integer cantidadTotal,
        String codCli, String codScc, String codpla, String despro,
        String diamtr, String eje, Date fechaCreacion,
        Date fechaPrimeraEntrega, Integer idPrpe, String moneda,
        String ordenCompra, String precio, String undmed,
        Integer idPedi_TsccvPedidos) throws Exception {
        try {
            BusinessDelegatorView.updateTsccvProductosPedido(activo,
                cantidadTotal, codCli, codScc, codpla, despro, diamtr, eje,
                fechaCreacion, fechaPrimeraEntrega, idPrpe, moneda,
                ordenCompra, precio, undmed, idPedi_TsccvPedidos, null);
            renderManager.getOnDemandRenderer("TsccvProductosPedidoView")
                         .requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TsccvProductosPedidoView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TsccvProductosPedido> getTsccvProductosPedido() {
        if (flag) {
            try {
                tsccvProductosPedido = BusinessDelegatorView.getTsccvProductosPedido();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tsccvProductosPedido;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTsccvProductosPedido(
        List<TsccvProductosPedido> tsccvProductosPedido) {
        this.tsccvProductosPedido = tsccvProductosPedido;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTsccvProductosPedido() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvProductosPedido> getDataPage(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvProductosPedido = 0;

        try {
            totalNumberTsccvProductosPedido = BusinessDelegatorView.findTotalNumberTsccvProductosPedido()
                                                                   .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvProductosPedido) {
            endIndex = totalNumberTsccvProductosPedido;
        }

        try {
            tsccvProductosPedido = BusinessDelegatorView.findPageTsccvProductosPedido(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvProductosPedido>(totalNumberTsccvProductosPedido,
            startRow, tsccvProductosPedido);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvProductosPedidoDTO> getDataPageDTO(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvProductosPedido = 0;

        try {
            totalNumberTsccvProductosPedido = BusinessDelegatorView.findTotalNumberTsccvProductosPedido()
                                                                   .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvProductosPedido) {
            endIndex = totalNumberTsccvProductosPedido;
        }

        try {
            tsccvProductosPedido = BusinessDelegatorView.findPageTsccvProductosPedido(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tsccvProductosPedido)) {
                tsccvProductosPedidoDTO = new ArrayList<TsccvProductosPedidoDTO>();

                for (TsccvProductosPedido tsccvProductosPedidoTmp : tsccvProductosPedido) {
                    TsccvProductosPedidoDTO tsccvProductosPedidoDTO2 = new TsccvProductosPedidoDTO();
                    tsccvProductosPedidoDTO2.setIdPrpe(tsccvProductosPedidoTmp.getIdPrpe()
                                                                              .toString());

                    tsccvProductosPedidoDTO2.setActivo((tsccvProductosPedidoTmp.getActivo() != null)
                        ? tsccvProductosPedidoTmp.getActivo().toString() : null);
                    tsccvProductosPedidoDTO2.setCantidadTotal((tsccvProductosPedidoTmp.getCantidadTotal() != null)
                        ? tsccvProductosPedidoTmp.getCantidadTotal().toString()
                        : null);
                    tsccvProductosPedidoDTO2.setCodCli((tsccvProductosPedidoTmp.getCodCli() != null)
                        ? tsccvProductosPedidoTmp.getCodCli().toString() : null);
                    tsccvProductosPedidoDTO2.setCodScc((tsccvProductosPedidoTmp.getCodScc() != null)
                        ? tsccvProductosPedidoTmp.getCodScc().toString() : null);
                    tsccvProductosPedidoDTO2.setCodpla((tsccvProductosPedidoTmp.getCodpla() != null)
                        ? tsccvProductosPedidoTmp.getCodpla().toString() : null);
                    tsccvProductosPedidoDTO2.setDespro((tsccvProductosPedidoTmp.getDespro() != null)
                        ? tsccvProductosPedidoTmp.getDespro().toString() : null);
                    tsccvProductosPedidoDTO2.setDiamtr((tsccvProductosPedidoTmp.getDiamtr() != null)
                        ? tsccvProductosPedidoTmp.getDiamtr().toString() : null);
                    tsccvProductosPedidoDTO2.setEje((tsccvProductosPedidoTmp.getEje() != null)
                        ? tsccvProductosPedidoTmp.getEje().toString() : null);
                    tsccvProductosPedidoDTO2.setFechaCreacion(tsccvProductosPedidoTmp.getFechaCreacion());
                    tsccvProductosPedidoDTO2.setFechaPrimeraEntrega(tsccvProductosPedidoTmp.getFechaPrimeraEntrega());
                    tsccvProductosPedidoDTO2.setMoneda((tsccvProductosPedidoTmp.getMoneda() != null)
                        ? tsccvProductosPedidoTmp.getMoneda().toString() : null);
                    tsccvProductosPedidoDTO2.setOrdenCompra((tsccvProductosPedidoTmp.getOrdenCompra() != null)
                        ? tsccvProductosPedidoTmp.getOrdenCompra().toString()
                        : null);
                    tsccvProductosPedidoDTO2.setPrecio((tsccvProductosPedidoTmp.getPrecio() != null)
                        ? tsccvProductosPedidoTmp.getPrecio().toString() : null);
                    tsccvProductosPedidoDTO2.setUndmed((tsccvProductosPedidoTmp.getUndmed() != null)
                        ? tsccvProductosPedidoTmp.getUndmed().toString() : null);
                    tsccvProductosPedidoDTO2.setIdPedi_TsccvPedidos((tsccvProductosPedidoTmp.getTsccvPedidos()
                                                                                            .getIdPedi() != null)
                        ? tsccvProductosPedidoTmp.getTsccvPedidos().getIdPedi()
                                                 .toString() : null);
                    tsccvProductosPedidoDTO2.setTsccvProductosPedido(tsccvProductosPedidoTmp);
                    tsccvProductosPedidoDTO2.setTsccvProductosPedidoView(this);
                    tsccvProductosPedidoDTO.add(tsccvProductosPedidoDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvProductosPedidoDTO>(totalNumberTsccvProductosPedido,
            startRow, tsccvProductosPedidoDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tsccvProductosPedidoDTO)) {
            for (TsccvProductosPedidoDTO tsccvProductosPedidoTmp : tsccvProductosPedidoDTO) {
                renderManager.getOnDemandRenderer("TsccvProductosPedidoView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tsccvProductosPedidoDTO)) {
            for (TsccvProductosPedidoDTO tsccvProductosPedidoTmp : tsccvProductosPedidoDTO) {
                renderManager.getOnDemandRenderer("TsccvProductosPedidoView")
                             .add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(HtmlInputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public HtmlInputText getTxtCantidadTotal() {
        return txtCantidadTotal;
    }

    public void setTxtCantidadTotal(HtmlInputText txtCantidadTotal) {
        this.txtCantidadTotal = txtCantidadTotal;
    }

    public HtmlInputText getTxtCodCli() {
        return txtCodCli;
    }

    public void setTxtCodCli(HtmlInputText txtCodCli) {
        this.txtCodCli = txtCodCli;
    }

    public HtmlInputText getTxtCodScc() {
        return txtCodScc;
    }

    public void setTxtCodScc(HtmlInputText txtCodScc) {
        this.txtCodScc = txtCodScc;
    }

    public HtmlInputText getTxtCodpla() {
        return txtCodpla;
    }

    public void setTxtCodpla(HtmlInputText txtCodpla) {
        this.txtCodpla = txtCodpla;
    }

    public HtmlInputText getTxtDespro() {
        return txtDespro;
    }

    public void setTxtDespro(HtmlInputText txtDespro) {
        this.txtDespro = txtDespro;
    }

    public HtmlInputText getTxtDiamtr() {
        return txtDiamtr;
    }

    public void setTxtDiamtr(HtmlInputText txtDiamtr) {
        this.txtDiamtr = txtDiamtr;
    }

    public HtmlInputText getTxtEje() {
        return txtEje;
    }

    public void setTxtEje(HtmlInputText txtEje) {
        this.txtEje = txtEje;
    }

    public HtmlInputText getTxtMoneda() {
        return txtMoneda;
    }

    public void setTxtMoneda(HtmlInputText txtMoneda) {
        this.txtMoneda = txtMoneda;
    }

    public HtmlInputText getTxtOrdenCompra() {
        return txtOrdenCompra;
    }

    public void setTxtOrdenCompra(HtmlInputText txtOrdenCompra) {
        this.txtOrdenCompra = txtOrdenCompra;
    }

    public HtmlInputText getTxtPrecio() {
        return txtPrecio;
    }

    public void setTxtPrecio(HtmlInputText txtPrecio) {
        this.txtPrecio = txtPrecio;
    }

    public HtmlInputText getTxtUndmed() {
        return txtUndmed;
    }

    public void setTxtUndmed(HtmlInputText txtUndmed) {
        this.txtUndmed = txtUndmed;
    }

    public HtmlInputText getTxtIdPedi_TsccvPedidos() {
        return txtIdPedi_TsccvPedidos;
    }

    public void setTxtIdPedi_TsccvPedidos(HtmlInputText txtIdPedi_TsccvPedidos) {
        this.txtIdPedi_TsccvPedidos = txtIdPedi_TsccvPedidos;
    }

    public SelectInputDate getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(SelectInputDate txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public SelectInputDate getTxtFechaPrimeraEntrega() {
        return txtFechaPrimeraEntrega;
    }

    public void setTxtFechaPrimeraEntrega(
        SelectInputDate txtFechaPrimeraEntrega) {
        this.txtFechaPrimeraEntrega = txtFechaPrimeraEntrega;
    }

    public HtmlInputText getTxtIdPrpe() {
        return txtIdPrpe;
    }

    public void setTxtIdPrpe(HtmlInputText txtIdPrpe) {
        this.txtIdPrpe = txtIdPrpe;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TsccvProductosPedidoDTO> getTsccvProductosPedidoDTO() {
        return tsccvProductosPedidoDTO;
    }

    public void setTsccvProductosPedidoDTO(
        List<TsccvProductosPedidoDTO> tsccvProductosPedidoDTO) {
        this.tsccvProductosPedidoDTO = tsccvProductosPedidoDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvProductosPedido> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvProductosPedidoDTO> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
}
