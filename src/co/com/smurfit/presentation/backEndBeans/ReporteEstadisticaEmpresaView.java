package co.com.smurfit.presentation.backEndBeans;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlOutputLink;
import com.icesoft.faces.component.ext.HtmlSelectManyMenu;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.ext.HtmlPanelGroup;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;

public class ReporteEstadisticaEmpresaView {
	private HtmlSelectOneMenu somPlantas;
    private List<SelectItem> siPlantas;
    private Boolean multiple;
    /** para seleccion multiple de grupos */
    private HtmlSelectManyMenu smmGruposUsuario;
    /** para seleccion uinca de grupos */
    private HtmlSelectOneMenu somGruposUsuario;
    private String[] gruposSeleccionados;
    private HtmlSelectOneMenu somEmpresas;
    private List<SelectItem> siGruposUsuario;
    private List<SelectItem> siTodos;
    private List<SelectItem> siEmpresas;
    private HtmlInputText txtMesesInactivacion;
    private HtmlPanelGroup panelPdf;
    private HtmlPanelGroup panelXls;
    private SelectInputDate fechaDesde;
    private SelectInputDate fechaHasta;
    private HtmlOutputLink descargaPdf;
    private HtmlOutputLink descargaExcel;
	
	public ReporteEstadisticaEmpresaView() {
		String estadoActivo=null;
		Long idApli=null;
		ArchivoPropiedades propiedades= null;
		List<TsccvPlanta> plantas = null;
		String itemVacio=null;
		ArchivoBundle etiquetas=null;
		List<TbmBaseGruposUsuarios> grupos=null;
		List<TsccvEmpresas> empresas=null;
		
		try {
			/*Consultamos las plantas activas y de la aplicacion adecuada para cargar la lista de plantas*/
			//Consultamos los parametros necesarios para realizar la consulta
			propiedades= new ArchivoPropiedades();
			etiquetas= new ArchivoBundle();
			estadoActivo= propiedades.getProperty("ESTADO_ACTIVO");
			idApli= Long.parseLong(propiedades.getProperty("APLICACION"));
			itemVacio= etiquetas.getProperty("seleccionarRegistro");
			plantas= BusinessDelegatorView.findByActivoAplicacion(estadoActivo, idApli);
			
			somPlantas = new HtmlSelectOneMenu();
	        siPlantas = new ArrayList<SelectItem>();
			//Cargamos el item vacio de la lista
			SelectItem si= new SelectItem();
        	si.setLabel(itemVacio);
        	si.setValue(new Long(-1)); 
        	siPlantas.add(si);
        	for(TsccvPlanta objetoPlanta: plantas){
        		SelectItem siCr = new SelectItem();
        		siCr.setLabel(objetoPlanta.getDespla());
        		siCr.setValue(objetoPlanta.getCodpla());
        		
        		siPlantas.add(siCr);
        	}
        	
        	//Consultamos los grupos de usuarios y cargamos la lista de seleccio
        	Object[] parametros= new Object[4];
        	parametros[0]="activo";
        	parametros[1]=Boolean.valueOf(false);
        	parametros[2]=estadoActivo;
        	parametros[3]="=";
        	grupos= BusinessDelegatorView.findByCriteriaInTbmBaseGruposUsuarios(parametros, null,null);
        	smmGruposUsuario= new HtmlSelectManyMenu();
        	somGruposUsuario= new HtmlSelectOneMenu();
        	siGruposUsuario= new ArrayList<SelectItem>();
        	this.siTodos = new ArrayList<SelectItem>();
        	
        	SelectItem siTodos = new SelectItem();
        	siTodos = new SelectItem();
        	siTodos.setLabel(etiquetas.getProperty("lblTodos"));
        	siTodos.setValue(new Long(-1));
        	
        	this.siTodos.add(siTodos);
        	for(TbmBaseGruposUsuarios g: grupos){
        		SelectItem siCr = new SelectItem();
        		siCr.setLabel(g.getDescripcion());
        		siCr.setValue(g.getIdGrup());
        		
        		siGruposUsuario.add(siCr);
        	}
        	
        	//Consultamos las empresas que no son grupos
        	Object[] param= new Object[8];
        	param[0]="activo";
        	param[1]=Boolean.valueOf(true);
        	param[2]=propiedades.getProperty("ESTADO_ACTIVO");
        	param[3]="=";
        	param[4]="tsccvGrupoEmpresa";
        	param[5]=Boolean.valueOf(false);
        	param[6]="null";
        	param[7]="is";
        	empresas=BusinessDelegatorView.findByCriteriaInTsccvEmpresas(param, null, null);
        	somEmpresas = new HtmlSelectOneMenu();
	        siEmpresas = new ArrayList<SelectItem>();
			//Cargamos el item vacio de la lista
			siEmpresas.add(si);
        	for(TsccvEmpresas objetoEmpresa: empresas){
        		SelectItem siCr = new SelectItem();
        		siCr.setLabel(objetoEmpresa.getNombre());
        		siCr.setValue(objetoEmpresa.getIdEmpr());
        		
        		siEmpresas.add(siCr);
        	}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public HtmlSelectOneMenu getSomPlantas() {
		return somPlantas;
	}

	public HtmlOutputLink getDescargaPdf() {
		return descargaPdf;
	}

	public void setDescargaPdf(HtmlOutputLink descargaPdf) {
		this.descargaPdf = descargaPdf;
	}

	public HtmlOutputLink getDescargaExcel() {
		return descargaExcel;
	}

	public void setDescargaExcel(HtmlOutputLink descargaExcel) {
		this.descargaExcel = descargaExcel;
	}

	public void setSomPlantas(HtmlSelectOneMenu somPlantas) {
		this.somPlantas = somPlantas;
	}

	public List<SelectItem> getSiPlantas() {
		return siPlantas;
	}

	public void setSiPlantas(List<SelectItem> siPlantas) {
		this.siPlantas = siPlantas;
	}

	public HtmlSelectOneMenu getSomGruposUsuario() {
		return somGruposUsuario;
	}

	public void setSomGruposUsuario(HtmlSelectOneMenu somGruposUsuario) {
		this.somGruposUsuario = somGruposUsuario;
	}

	public List<SelectItem> getSiGruposUsuario() {
		return siGruposUsuario;
	}

	public void setSiGruposUsuario(List<SelectItem> siGruposUsuario) {
		this.siGruposUsuario = siGruposUsuario;
	}

	public HtmlInputText getTxtMesesInactivacion() {
		return txtMesesInactivacion;
	}

	public void setTxtMesesInactivacion(HtmlInputText txtMesesInactivacion) {
		this.txtMesesInactivacion = txtMesesInactivacion;
	}
	
	public void generarReporte(ActionEvent ev){
		
		Connection con=null;
        Map parametrosM;
        String nombreArchivo=null;
        String descargarReporte=null;
        HttpSession session=null;
        String nombrePlanta=null;
        String nombreGrupo="";
        String nombreEmpresa=null;
        String idPlanta=null;
        //String idGrupo="";
        ArrayList<Integer> idsGrupos = new ArrayList<Integer>(0);
        String idEmpr=null;
        String fechaD=null;
        String fechaH=null;
        String mesesInactivacion=null;
        ArchivoPropiedades ar=null;
        SimpleDateFormat sd=null;
        try {
          //se inicialiazn los HashMap
          parametrosM = new HashMap();
          
          //se obtiene la conexion a la DB
          con = Utilities.getConnection();
          
          //variables en las cuales se cargaran los reportes
          JasperReport reporteMaestro = null;
          
          //Si se selecciono un planta buscamos en la lista la planta que corresponde
          if(somPlantas.getValue() != null && !((String)somPlantas.getValue()).equals("-1")){
        	  for(SelectItem si :  siPlantas){
        		  if(si.getValue().equals(somPlantas.getValue())){
        			  nombrePlanta=si.getLabel();
        			  idPlanta= somPlantas.getValue().toString();
        			  break;
        		  }
        	  }
          }
          else{
        	  nombrePlanta="TODAS";
        	  idPlanta="%";
          }
          
          //si selecciono multiple buscamos los grupos seleccionados
          if(multiple){
	          if(gruposSeleccionados != null && gruposSeleccionados.length > 0){
	        	  for(int i=0; i<gruposSeleccionados.length; i++){
	        		  for(SelectItem si :  siGruposUsuario){
	        			  if(gruposSeleccionados[i].equals(si.getValue().toString())){
		        			  nombreGrupo += si.getLabel();
		        			  idsGrupos.add(new Integer(gruposSeleccionados[i]));
		        			  
		        			  if(i < gruposSeleccionados.length-1){
		        				  nombreGrupo+=", ";
		        			  }
		        			  
		        			  break;
	        			  }
	        		  }
	        	  }
	          }
	          else{
	        	  //Mostramos el mensaje de alerta incicando que por ser seleccion multiple debe elegir almenos uno
	        	  MBMensaje mb=(MBMensaje)FacesUtils.getManagedBean("MBMensaje");
	        	  mb.mostrarMensaje("Debe seleccionar al menos un grupo", MBMensaje.MENSAJE_ALERTA);
	        	  
	        	  return;
	          }
	      //si no selecciono multiple buscamos en la lista el grupo que corresponde
          }else{
	          if(somGruposUsuario.getValue() != null && !((String)somGruposUsuario.getValue()).equals("-1")){
	        	  for(SelectItem si :  siGruposUsuario){
        			  if(somGruposUsuario.getValue().toString().equals(si.getValue().toString())){
	        			  nombreGrupo = si.getLabel();
	        			  idsGrupos.add(new Integer(si.getValue().toString()));
	        			  
	        			  break;
        			  }
        		  }
	          }
	          //si la seleccion es todos
	          else{
	        	  for(int i=0; i<siGruposUsuario.size(); i++){
	        		  SelectItem si = siGruposUsuario.get(i);
	        		  nombreGrupo += si.getLabel();
        			  idsGrupos.add(new Integer(si.getValue().toString()));
        			  
        			  if(i < siGruposUsuario.size()-1){
        				  nombreGrupo+=", ";
        			  }
        		  }
	          }
          }
          
          //Si se selecciono un grupo buscamos en la lista el grupo que corresponde
          if(somEmpresas.getValue() != null && !((String)somEmpresas.getValue()).equals("-1")){
        	  for(SelectItem si :  siEmpresas){
        		  if(somEmpresas.getValue().equals(si.getValue().toString())){
        			  nombreEmpresa=si.getLabel();
        			  idEmpr= somEmpresas.getValue().toString();
        		  }
        	  }
          }
          else{
        	  nombreEmpresa="TODAS";
        	  idEmpr="%";
          }

          
         //Obtenemos los valores de las fechas
          sd= new SimpleDateFormat("dd/MM/yyyy");
          fechaD= sd.format((Date)fechaDesde.getValue());
          fechaH= sd.format((Date)fechaHasta.getValue());

          //se llena el HashMap con el primer subreporte y con los valores para la consulta
          parametrosM.put("CODPLA",idPlanta);
          parametrosM.put("NOMBRE_PLANTA",nombrePlanta);
          parametrosM.put("IDS_GRUPOS", idsGrupos);
          parametrosM.put("NOMBRE_GRUPO", nombreGrupo);
          parametrosM.put("ID_EMPR",idEmpr);
          parametrosM.put("NOMBRE_EMPRESA", nombreEmpresa);
          parametrosM.put("FECHA_DESDE", fechaD);
          parametrosM.put("FECHA_HASTA", fechaH);
          
          //Obtenemos la ruta jasper del archivo de propiedades
          ar= new ArchivoPropiedades();
          
          reporteMaestro = (JasperReport)JRLoader.loadObject(ar.getProperty("RUTA_JASPER") + "estadisticasPlantaEmpresa.jasper");
          
          //se ejecuta el reporte maestro y el subreporte Maestro
          JasperPrint jasperPrint = JasperFillManager.fillReport(reporteMaestro,parametrosM,con);
          nombreArchivo = "reporteEstadisticasEmpresa"+new Date().getTime();
          String archivoPdf=nombreArchivo+".pdf";
          String archivoXls=nombreArchivo+".xls";
          descargarReporte = ar.getProperty("RUTA_REPORTES_GENERADOS") + archivoPdf;
          
          //se exporta el reporte
          JasperExportManager.exportReportToPdfFile(jasperPrint,descargarReporte);
           
          OutputStream ouputStream
                = new FileOutputStream(new File(ar.getProperty("RUTA_REPORTES_GENERADOS")+archivoXls));
            ByteArrayOutputStream byteArrayOutputStream
                = new ByteArrayOutputStream();
                
            JRXlsExporter exporterXLS = new JRXlsExporter();
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT,
                                     jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
                                     byteArrayOutputStream);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER,Boolean.valueOf(true));
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.valueOf(true));
            
            exporterXLS.exportReport();

            ouputStream.write(byteArrayOutputStream.toByteArray()); 
            ouputStream.flush();
            ouputStream.close();
          
          //se muestra el enlace y se setea el destino
          panelPdf.setRendered(true);
          panelXls.setRendered(true);
          
        //Le asignamos los valores a los enlaces de descarga
			descargaPdf.setValue(Utilities.RUTA_SERVER+archivoPdf);
			descargaExcel.setValue(Utilities.RUTA_SERVER+archivoXls);
          
          //Colocamos en la session el nombre del archivo
          /*session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
          session.setAttribute(Utilities.LLAVE_NOMBRE_ARCHIVO, nombreArchivo);*/
          
          somGruposUsuario.resetValue();
          smmGruposUsuario.resetValue();
          gruposSeleccionados = new String[0];
          somPlantas.resetValue();
          somEmpresas.resetValue();
          fechaDesde.resetValue();
          fechaHasta.resetValue();
          
          //Mostramos el mensaje de OK
          MBMensaje mb=(MBMensaje)FacesUtils.getManagedBean("MBMensaje");
          mb.mostrarMensaje("El reporte se ha generado de forma exitosa", MBMensaje.MENSAJE_OK);
          
        }catch(Exception e){
        	e.printStackTrace();
        	panelPdf.setRendered(false);
            panelXls.setRendered(false);
          BMBaseException ex = new BMBaseException(e);
          MBMensaje msg= (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
          msg.mostrarMensaje(ex);
        }
        finally{
            //se cierra la conexion a la DB
            try {
                con.close();
            }
            catch (SQLException e) {
                System.out.println("Error cerrando conexion");
            }
        }
	}

	public HtmlPanelGroup getPanelPdf() {
		return panelPdf;
	}

	public void setPanelPdf(HtmlPanelGroup panelPdf) {
		this.panelPdf = panelPdf;
	}

	public HtmlPanelGroup getPanelXls() {
		return panelXls;
	}

	public void setPanelXls(HtmlPanelGroup panelXls) {
		this.panelXls = panelXls;
	}
	
	public SelectInputDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(SelectInputDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public SelectInputDate getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(SelectInputDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public HtmlSelectOneMenu getSomEmpresas() {
		return somEmpresas;
	}

	public void setSomEmpresas(HtmlSelectOneMenu somEmpresas) {
		this.somEmpresas = somEmpresas;
	}

	public List<SelectItem> getSiEmpresas() {
		return siEmpresas;
	}

	public void setSiEmpresas(List<SelectItem> siEmpresas) {
		this.siEmpresas = siEmpresas;
	}

	public Boolean getMultiple() {
		return multiple;
	}

	public void setMultiple(Boolean multiple) {
		this.multiple = multiple;
	}

	public HtmlSelectManyMenu getSmmGruposUsuario() {
		return smmGruposUsuario;
	}

	public void setSmmGruposUsuario(HtmlSelectManyMenu smmGruposUsuario) {
		this.smmGruposUsuario = smmGruposUsuario;
	}

	public String[] getGruposSeleccionados() {
		return gruposSeleccionados;
	}

	public void setGruposSeleccionados(String[] gruposSeleccionados) {
		this.gruposSeleccionados = gruposSeleccionados;
	}

	public List<SelectItem> getSiTodos() {
		return siTodos;
	}

	public void setSiTodos(List<SelectItem> siTodos) {
		this.siTodos = siTodos;
	}
}
