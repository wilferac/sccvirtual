package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TbmBaseOpcionesGruposDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TbmBaseOpcionesGruposDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TbmBaseOpcionesGruposView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtIdGrup_TbmBaseGruposUsuarios;
    private HtmlInputText txtIdOpci_TbmBaseOpciones;
    private HtmlInputText txtIdOpci;
    private HtmlInputText txtIdGrup;
    private SelectInputDate txtFechaCreacion;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TbmBaseOpcionesGrupos> tbmBaseOpcionesGrupos;
    private List<TbmBaseOpcionesGruposDTO> tbmBaseOpcionesGruposDTO;
    
    private List<TbmBaseOpcionesGrupos> listadoOpcionesGrupo;
    private HtmlSelectOneMenu somGrupo;
    private List<SelectItem> siGrupo;   
    private HtmlSelectOneMenu somOpcion;
    private List<SelectItem> siOpcion;

    public TbmBaseOpcionesGruposView() {
        super("");
        
        ArchivoBundle archivoBundle = new ArchivoBundle();
        
        try{
        	//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
        	
        	txtIdGrup_TbmBaseGruposUsuarios = new HtmlInputText();
            txtIdOpci_TbmBaseOpciones = new HtmlInputText();
            txtFechaCreacion = new SelectInputDate();
            
            //se inicializan los componentes de las listas
            siGrupo = new ArrayList<SelectItem>();
            siOpcion = new ArrayList<SelectItem>();
            
			//se crear los listados
            List<TbmBaseGruposUsuarios> listadoGrupos = null;
            List<TbmBaseOpciones> listadoOpciones = null;
            listadoOpcionesGrupo = new ArrayList<TbmBaseOpcionesGrupos>();
        	
			//se cargan los valores de las listas        	
        	listadoGrupos = BusinessDelegatorView.getTbmBaseGruposUsuarios();
        	listadoOpciones = BusinessDelegatorView.getTbmBaseOpciones();
        	
			//lista de grupos
        	siGrupo.add(siNulo);
        	for(TbmBaseGruposUsuarios objetoGrupo: listadoGrupos){
        		//se valida is el grupo esta inactivo para no agregarlo a la lista
        		if(objetoGrupo.getActivo().equals("1")){
        			continue;
        		}
        		
        		SelectItem siGrup = new SelectItem();
        		
        		siGrup.setLabel(objetoGrupo.getDescripcion());
        		siGrup.setValue(objetoGrupo.getIdGrup());
        		
        		siGrupo.add(siGrup);
        	}
        	
			//lista de opciones
        	siOpcion.add(siNulo);
        	for(TbmBaseOpciones objetoOpcion: listadoOpciones){
        		//se valida is la opcion esta inactiva para no agregarla a la lista
        		if(objetoOpcion.getActivo().equals("1")){
        			continue;
        		}
        		SelectItem siOpci = new SelectItem();        		
        		siOpci.setLabel(objetoOpcion.getNombre());
        		siOpci.setValue(objetoOpcion.getIdOpci());
        		
        		siOpcion.add(siOpci);
        	}
        }catch(Exception e){
        	FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
        }
        
    }

    public String action_clear() {
        txtIdGrup_TbmBaseGruposUsuarios.setValue(null);
        txtIdGrup_TbmBaseGruposUsuarios.setDisabled(true);
        txtIdOpci_TbmBaseOpciones.setValue(null);
        txtIdOpci_TbmBaseOpciones.setDisabled(true);

        txtFechaCreacion.setValue(null);
        txtFechaCreacion.setDisabled(true);

        txtIdGrup.setValue(null);
        txtIdGrup.setDisabled(false);
        txtIdOpci.setValue(null);
        txtIdOpci.setDisabled(false);
        
        somGrupo.setValue(new Long(-1));
        somOpcion.setValue(new Long(-1));

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);
        
        //tambien agrego estas lineas para que refresque la tabla.
        listadoOpcionesGrupo.clear();
        
        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TbmBaseOpcionesGrupos entity = null;

            try {
                TbmBaseOpcionesGruposId id = new TbmBaseOpcionesGruposId();
                id.setIdGrup((((txtIdGrup.getValue()) == null) ||
                    (txtIdGrup.getValue()).equals("")) ? null
                                                       : new Long(
                        txtIdGrup.getValue().toString()));
                id.setIdOpci((((txtIdOpci.getValue()) == null) ||
                    (txtIdOpci.getValue()).equals("")) ? null
                                                       : new Long(
                        txtIdOpci.getValue().toString()));

                entity = BusinessDelegatorView.getTbmBaseOpcionesGrupos(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtIdGrup_TbmBaseGruposUsuarios.setDisabled(false);
                txtIdOpci_TbmBaseOpciones.setDisabled(false);

                txtFechaCreacion.setDisabled(false);

                txtIdGrup.setDisabled(false);
                txtIdOpci.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtFechaCreacion.setValue(entity.getFechaCreacion());
                txtFechaCreacion.setDisabled(false);
                txtIdGrup_TbmBaseGruposUsuarios.setValue(entity.getTbmBaseGruposUsuarios()
                                                               .getIdGrup());
                txtIdGrup_TbmBaseGruposUsuarios.setDisabled(false);
                txtIdOpci_TbmBaseOpciones.setValue(entity.getTbmBaseOpciones()
                                                         .getIdOpci());
                txtIdOpci_TbmBaseOpciones.setDisabled(false);

                txtIdGrup.setValue(entity.getId().getIdGrup());
                txtIdGrup.setDisabled(true);
                txtIdOpci.setValue(entity.getId().getIdOpci());
                txtIdOpci.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
        	txtIdGrup.setValue(new Long(somGrupo.getValue().toString()) != -1 ? somGrupo.getValue() : null);
            txtIdGrup_TbmBaseGruposUsuarios.setValue(new Long(somGrupo.getValue().toString()) != -1 ? somGrupo.getValue() : null);          
            txtFechaCreacion.setValue(new Date());
        	
            if(listadoOpcionesGrupo.size() > 0){
	            int opcionesNuevas = 0;
	            for(TbmBaseOpcionesGrupos opciGrup: listadoOpcionesGrupo){
	        		if(opciGrup.getTbmBaseOpciones().getActivo().equals("e")){
	        			opcionesNuevas++;
	        		}
	        	}
	        	
	        	if(opcionesNuevas == 0){
	        		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	              	mensaje.mostrarMensaje("No se agrego ninguna Opci�n nueva", MBMensaje.MENSAJE_ALERTA);
	              	
	              	return "";
	            }            
            }
            else{
    			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("Por favor agregue al menos una Opci�n", MBMensaje.MENSAJE_ALERTA);
              	return "";
        	}
            
        	BusinessDelegatorView.saveTbmBaseOpcionesGrupos((((txtIdOpci.getValue()) == null) ||
                    (txtIdOpci.getValue()).equals("")) ? null
                            : new Long(
					txtIdOpci.getValue().toString()),
					(((txtIdGrup.getValue()) == null) ||
					(txtIdGrup.getValue()).equals("")) ? null
					                            : new Long(
					txtIdGrup.getValue().toString()),
					(((txtFechaCreacion.getValue()) == null) ||
					(txtFechaCreacion.getValue()).equals("")) ? null
					                                   : (Date) txtFechaCreacion.getValue(),
					(((txtIdGrup_TbmBaseGruposUsuarios.getValue()) == null) ||
					(txtIdGrup_TbmBaseGruposUsuarios.getValue()).equals("")) ? null
					                                                  : new Long(
					txtIdGrup_TbmBaseGruposUsuarios.getValue().toString()),
					(((txtIdOpci_TbmBaseOpciones.getValue()) == null) ||
					(txtIdOpci_TbmBaseOpciones.getValue()).equals("")) ? null
					                                            : new Long(
					txtIdOpci_TbmBaseOpciones.getValue().toString()), listadoOpcionesGrupo);
        	
        	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
          	mensaje.mostrarMensaje("Opciones adicionadas de forma satisfactoria", MBMensaje.MENSAJE_OK);

        } catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}
		
		action_clear();

        return "";
    }

    public String action_delete() {
        try {
        	BusinessDelegatorView.deleteTbmBaseOpcionesGrupos((((txtIdOpci.getValue()) == null) ||
                    (txtIdOpci.getValue()).equals("")) ? null
                            : new Long(
					txtIdOpci.getValue().toString()),
					(((txtIdGrup.getValue()) == null) ||
					(txtIdGrup.getValue()).equals("")) ? null
					                            : new Long(
					txtIdGrup.getValue().toString()));

		    FacesContext.getCurrentInstance()
		            .addMessage("",
		    new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
		} catch (Exception e) {
		FacesContext.getCurrentInstance()
		            .addMessage("", new FacesMessage(e.getMessage()));
		}

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateTbmBaseOpcionesGrupos((((txtIdOpci.getValue()) == null) ||
                    (txtIdOpci.getValue()).equals("")) ? null
                            : new Long(
					txtIdOpci.getValue().toString()),
					(((txtIdGrup.getValue()) == null) ||
					(txtIdGrup.getValue()).equals("")) ? null
					                            : new Long(
					txtIdGrup.getValue().toString()),
					(((txtFechaCreacion.getValue()) == null) ||
					(txtFechaCreacion.getValue()).equals("")) ? null
					                                   : (Date) txtFechaCreacion.getValue(),
					(((txtIdGrup_TbmBaseGruposUsuarios.getValue()) == null) ||
					(txtIdGrup_TbmBaseGruposUsuarios.getValue()).equals("")) ? null
					                                                  : new Long(
					txtIdGrup_TbmBaseGruposUsuarios.getValue().toString()),
					(((txtIdOpci_TbmBaseOpciones.getValue()) == null) ||
					(txtIdOpci_TbmBaseOpciones.getValue()).equals("")) ? null
					                                            : new Long(
					txtIdOpci_TbmBaseOpciones.getValue().toString()));

            	FacesContext.getCurrentInstance()
			            .addMessage("",
			    new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
			} catch (Exception e) {
			FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));
			}

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Long idOpci, Long idGrup,
        Date fechaCreacion, Long idGrup_TbmBaseGruposUsuarios,
        Long idOpci_TbmBaseOpciones) throws Exception {
        try {
            BusinessDelegatorView.updateTbmBaseOpcionesGrupos(idOpci, idGrup,
                fechaCreacion, idGrup_TbmBaseGruposUsuarios,
                idOpci_TbmBaseOpciones);
            renderManager.getOnDemandRenderer("TbmBaseOpcionesGruposView")
                         .requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TbmBaseOpcionesGruposView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }


    public List<TbmBaseOpcionesGrupos> getTbmBaseOpcionesGrupos() {
        if (flag) {
            try {
                tbmBaseOpcionesGrupos = BusinessDelegatorView.getTbmBaseOpcionesGrupos();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tbmBaseOpcionesGrupos;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTbmBaseOpcionesGrupos(
        List<TbmBaseOpcionesGrupos> tbmBaseOpcionesGrupos) {
        this.tbmBaseOpcionesGrupos = tbmBaseOpcionesGrupos;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTbmBaseOpcionesGrupos() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TbmBaseOpcionesGrupos> getDataPage(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTbmBaseOpcionesGrupos = 0;

        try {
            totalNumberTbmBaseOpcionesGrupos = BusinessDelegatorView.findTotalNumberTbmBaseOpcionesGrupos()
                                                                    .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTbmBaseOpcionesGrupos) {
            endIndex = totalNumberTbmBaseOpcionesGrupos;
        }

        try {
            tbmBaseOpcionesGrupos = BusinessDelegatorView.findPageTbmBaseOpcionesGrupos(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TbmBaseOpcionesGrupos>(totalNumberTbmBaseOpcionesGrupos,
            startRow, tbmBaseOpcionesGrupos);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TbmBaseOpcionesGruposDTO> getDataPageDTO(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTbmBaseOpcionesGrupos = 0;

        try {
            totalNumberTbmBaseOpcionesGrupos = BusinessDelegatorView.findTotalNumberTbmBaseOpcionesGrupos()
                                                                    .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTbmBaseOpcionesGrupos) {
            endIndex = totalNumberTbmBaseOpcionesGrupos;
        }

        try {
            tbmBaseOpcionesGrupos = BusinessDelegatorView.findPageTbmBaseOpcionesGrupos(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tbmBaseOpcionesGrupos)) {
                tbmBaseOpcionesGruposDTO = new ArrayList<TbmBaseOpcionesGruposDTO>();

                for (TbmBaseOpcionesGrupos tbmBaseOpcionesGruposTmp : tbmBaseOpcionesGrupos) {
                    TbmBaseOpcionesGruposDTO tbmBaseOpcionesGruposDTO2 = new TbmBaseOpcionesGruposDTO();
                    tbmBaseOpcionesGruposDTO2.setIdOpci(tbmBaseOpcionesGruposTmp.getId()
                                                                                .getIdOpci()
                                                                                .toString());
                    tbmBaseOpcionesGruposDTO2.setIdGrup(tbmBaseOpcionesGruposTmp.getId()
                                                                                .getIdGrup()
                                                                                .toString());

                    tbmBaseOpcionesGruposDTO2.setFechaCreacion(tbmBaseOpcionesGruposTmp.getFechaCreacion());
                    tbmBaseOpcionesGruposDTO2.setIdGrup_TbmBaseGruposUsuarios((tbmBaseOpcionesGruposTmp.getTbmBaseGruposUsuarios()
                                                                                                       .getIdGrup() != null)
                        ? tbmBaseOpcionesGruposTmp.getTbmBaseGruposUsuarios()
                                                  .getIdGrup().toString() : null);
                    tbmBaseOpcionesGruposDTO2.setIdOpci_TbmBaseOpciones((tbmBaseOpcionesGruposTmp.getTbmBaseOpciones()
                                                                                                 .getIdOpci() != null)
                        ? tbmBaseOpcionesGruposTmp.getTbmBaseOpciones()
                                                  .getIdOpci().toString() : null);
                    tbmBaseOpcionesGruposDTO2.setTbmBaseOpcionesGrupos(tbmBaseOpcionesGruposTmp);
                    tbmBaseOpcionesGruposDTO2.setTbmBaseOpcionesGruposView(this);
                    tbmBaseOpcionesGruposDTO.add(tbmBaseOpcionesGruposDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TbmBaseOpcionesGruposDTO>(totalNumberTbmBaseOpcionesGrupos,
            startRow, tbmBaseOpcionesGruposDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tbmBaseOpcionesGruposDTO)) {
            for (TbmBaseOpcionesGruposDTO tbmBaseOpcionesGruposTmp : tbmBaseOpcionesGruposDTO) {
                renderManager.getOnDemandRenderer("TbmBaseOpcionesGruposView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tbmBaseOpcionesGruposDTO)) {
            for (TbmBaseOpcionesGruposDTO tbmBaseOpcionesGruposTmp : tbmBaseOpcionesGruposDTO) {
                renderManager.getOnDemandRenderer("TbmBaseOpcionesGruposView")
                             .add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtIdGrup_TbmBaseGruposUsuarios() {
        return txtIdGrup_TbmBaseGruposUsuarios;
    }

    public void setTxtIdGrup_TbmBaseGruposUsuarios(
        HtmlInputText txtIdGrup_TbmBaseGruposUsuarios) {
        this.txtIdGrup_TbmBaseGruposUsuarios = txtIdGrup_TbmBaseGruposUsuarios;
    }

    public HtmlInputText getTxtIdOpci_TbmBaseOpciones() {
        return txtIdOpci_TbmBaseOpciones;
    }

    public void setTxtIdOpci_TbmBaseOpciones(
        HtmlInputText txtIdOpci_TbmBaseOpciones) {
        this.txtIdOpci_TbmBaseOpciones = txtIdOpci_TbmBaseOpciones;
    }

    public SelectInputDate getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(SelectInputDate txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public HtmlInputText getTxtIdGrup() {
        return txtIdGrup;
    }

    public void setTxtIdGrup(HtmlInputText txtIdGrup) {
        this.txtIdGrup = txtIdGrup;
    }

    public HtmlInputText getTxtIdOpci() {
        return txtIdOpci;
    }

    public void setTxtIdOpci(HtmlInputText txtIdOpci) {
        this.txtIdOpci = txtIdOpci;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TbmBaseOpcionesGruposDTO> getTbmBaseOpcionesGruposDTO() {
        return tbmBaseOpcionesGruposDTO;
    }

    public void setTbmBaseOpcionesGruposDTO(
        List<TbmBaseOpcionesGruposDTO> tbmBaseOpcionesGruposDTO) {
        this.tbmBaseOpcionesGruposDTO = tbmBaseOpcionesGruposDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    
    

	public HtmlSelectOneMenu getSomGrupo() {
		return somGrupo;
	}

	public void setSomGrupo(HtmlSelectOneMenu somGrupo) {
		this.somGrupo = somGrupo;
	}

	public List<SelectItem> getSiGrupo() {
		return siGrupo;
	}

	public void setSiGrupo(List<SelectItem> siGrupo) {
		this.siGrupo = siGrupo;
	}

	public HtmlSelectOneMenu getSomOpcion() {
		return somOpcion;
	}

	public void setSomOpcion(HtmlSelectOneMenu somOpcion) {
		this.somOpcion = somOpcion;
	}

	public List<SelectItem> getSiOpcion() {
		return siOpcion;
	}

	public void setSiOpcion(List<SelectItem> siOpcion) {
		this.siOpcion = siOpcion;
	}
    
	public List<TbmBaseOpcionesGrupos> getListadoOpcionesGrupo() {
		return listadoOpcionesGrupo;
	}

	public void setListadoOpcionesGrupo(
			List<TbmBaseOpcionesGrupos> listadoOpcionesGrupo) {
		this.listadoOpcionesGrupo = listadoOpcionesGrupo;
	}
    
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TbmBaseOpcionesGrupos> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TbmBaseOpcionesGruposDTO> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
    
    //Metodo encargado de consultar las opciones pertenecientes al grupo seleccionado
    public void consultarOpcionesPorGrupo(ValueChangeEvent event){
     	if (event == null || (event.getNewValue() != null && !event.getNewValue().equals(""))) {	    	
	    	Long idGru = new Long(new Long(somGrupo.getValue().toString()) != -1 ? somGrupo.getValue().toString() : "0");
	    	
	    	listadoOpcionesGrupo = new ArrayList<TbmBaseOpcionesGrupos>();
	    	
	    	idGru = idGru.equals(new Long(0)) ? null : idGru;
	    	
	    	if(idGru != null){    		
	    		try{
	    			//Se consulta el detalle del grupo
	    			List<TbmBaseOpcionesGrupos> listadoOpciones = BusinessDelegatorView.findByPropertyOpcionesGrupos(TbmBaseOpcionesGruposDAO.GRUPO, idGru);
	    			
	    			//Se recorre el listado de detalles y se asigna cada una de las opciones a la tabla
	    			for(TbmBaseOpcionesGrupos objetoOpcion : listadoOpciones){
	    				listadoOpcionesGrupo.add(objetoOpcion);
	    			}
	    			btnSave.setDisabled(false);
	    		}catch(Exception e){
	    			//FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
	    			//Mensaje mensaje = (Mensaje) FacesUtils.getManagedBean("mensaje");
	              	//mensaje.mostrarMensaje(e.getMessage(), Mensaje.MENSAJE_ERROR);
	    		}
	    	}
	    	else{
	    		btnSave.setDisabled(true);
	    	}
    	}
    }
    
    //metodo que agrega las opciones a la lista
    public void agregarOpcion(ActionEvent event){
    	//Se valida si la opcion seleccionada es valida
    	try{
	    	if((somOpcion.getValue() != null) && (new Long(somOpcion.getValue().toString()) != -1)){
	    		//Se valida si la opcion ya se agrego
	    		for(TbmBaseOpcionesGrupos opciGrup: listadoOpcionesGrupo){
	    			if(opciGrup.getTbmBaseOpciones().getIdOpci().equals(new Long(somOpcion.getValue().toString()))){
	    				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	    	          	mensaje.mostrarMensaje("Esta Opci�n ya fue agregada", MBMensaje.MENSAJE_ALERTA);
	    	          	
	    	          	return;
	    			}
	    		}
	    		
	    		TbmBaseOpcionesGrupos opcionGrupo = new TbmBaseOpcionesGrupos();
	    		TbmBaseOpciones opcionSeleccionada = new TbmBaseOpciones();
	    		opcionSeleccionada = BusinessDelegatorView.getTbmBaseOpciones(new Long(somOpcion.getValue().toString().toString()));
	    		//se setea el valor "0" para diferenciar las opciones que estan en memoria de las que esta en la BD 
	    		opcionSeleccionada.setActivo("e");
	    		opcionGrupo.setTbmBaseOpciones(opcionSeleccionada);
	    		
	    		listadoOpcionesGrupo.add(opcionGrupo);
	    	}else{
	    		//FacesContext.getCurrentInstance().addMessage("", new FacesMessage("Favor selecciona una Planta valida"));
	    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	          	mensaje.mostrarMensaje("Por favor seleccione una Opci�n valida", MBMensaje.MENSAJE_ALERTA);
	    	}
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    	
		somOpcion.setValue(new Long(-1));
		somOpcion.setDisabled(false);
    }    
    
    
    //metodo que elimina las opciones de la lista en memoria o de la BD
    public void eliminarOpcion(ActionEvent event){
    	try {    		
    		TbmBaseOpcionesGrupos opciGrup = (TbmBaseOpcionesGrupos) event.getComponent().getAttributes().get("opciGrup");
    		if(opciGrup.getTbmBaseOpciones().getActivo().equals("e")){  
    			listadoOpcionesGrupo.remove(opciGrup);
    		}
    		else {
	    		if(new Long(somGrupo.getValue().toString()) != -1) {
	    			txtIdGrup.setValue(somGrupo.getValue());
	    			txtIdGrup_TbmBaseGruposUsuarios.setValue(somGrupo.getValue());
	    			txtIdOpci.setValue(opciGrup.getTbmBaseOpciones().getIdOpci());
	    			txtIdOpci_TbmBaseOpciones.setValue(opciGrup.getTbmBaseOpciones().getIdOpci());
		        	
	    			BusinessDelegatorView.deleteTbmBaseOpcionesGrupos((((txtIdOpci.getValue()) == null) ||
	                        (txtIdOpci.getValue()).equals("")) ? null
	                                : new Long(
	    					txtIdOpci.getValue().toString()),
	    					(((txtIdGrup.getValue()) == null) ||
	    					(txtIdGrup.getValue()).equals("")) ? null
	    					                            : new Long(
	    					txtIdGrup.getValue().toString()));
	    			
		            consultarOpcionesPorGrupo(null);
				}
				else {
					MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
		          	mensaje.mostrarMensaje("Debe seleccionar un Grupo", MBMensaje.MENSAJE_ALERTA);
	    		}
    		}    		
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    }
    
    
	//metodo encargado de modificar el orden de la opcion
    public void cambiarOrden(ActionEvent event){
    	try{
    		TbmBaseOpcionesGrupos tbmBaseOpcionGrupo = (TbmBaseOpcionesGrupos) event.getComponent().getAttributes().get("tbmBaseOpcionGrupo");
	    	String orden = FacesUtils.getRequestParameter("orden");
	    	
	    	BusinessDelegatorView.cambiarOrden(tbmBaseOpcionGrupo, orden);
	    	
	    	consultarOpcionesPorGrupo(null);
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    }
}
