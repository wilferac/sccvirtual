package co.com.smurfit.presentation.backEndBeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.dto.BvpPrecioDTO;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputSecret;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class CambioPasswordView {
    private HtmlInputSecret txtPasswordAnterior;
    private HtmlInputSecret txtPasswordNuevo;
    private HtmlInputSecret txtRePasswordNuevo;
    private TsccvUsuario userLogin;

    public CambioPasswordView() {
		
		try{
			MBUsuario mbUsu = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
			userLogin = mbUsu.getUsuario();
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    }

    public HtmlInputSecret getTxtPasswordAnterior() {
		return txtPasswordAnterior;
	}

	public void setTxtPasswordAnterior(HtmlInputSecret txtPasswordAnterior) {
		this.txtPasswordAnterior = txtPasswordAnterior;
	}

	public HtmlInputSecret getTxtPasswordNuevo() {
		return txtPasswordNuevo;
	}

	public void setTxtPasswordNuevo(HtmlInputSecret txtPasswordNuevo) {
		this.txtPasswordNuevo = txtPasswordNuevo;
	}

	public HtmlInputSecret getTxtRePasswordNuevo() {
		return txtRePasswordNuevo;
	}

	public void setTxtRePasswordNuevo(HtmlInputSecret txtRePasswordNuevo) {
		this.txtRePasswordNuevo = txtRePasswordNuevo;
	}

	public String action_clear() {
        txtPasswordAnterior.setValue(null);
        txtPasswordAnterior.setDisabled(false);
        txtPasswordNuevo.setValue(null);
        txtPasswordNuevo.setDisabled(false);
        txtRePasswordNuevo.setValue(null);
        txtRePasswordNuevo.setDisabled(false);

        return "";
    }
    
    public void cambiarPassword(){
    	try{
	    	if(txtPasswordNuevo.getValue().toString().equals(txtRePasswordNuevo.getValue().toString())){
	    		userLogin.setPassword(txtPasswordAnterior.getValue().toString());
	    		BusinessDelegatorView.cambiarPassword(userLogin, txtPasswordNuevo.getValue().toString());
	    		
	    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensaje("Su Contrase�a ha sido cambiada correctamente, el cambio se aplicara cuando inicie una nueva sesi�n.", MBMensaje.MENSAJE_OK);
	    	}
	    	else{
	    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensajeAlerta("La Nueva Contrase�a es distinta a la confirmaci�n de �sta. Por favor ingr�sela correctamente.");
				txtPasswordNuevo.setValue(null);
				txtRePasswordNuevo.setValue(null);
				return;
	    	}
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    }
}
    


