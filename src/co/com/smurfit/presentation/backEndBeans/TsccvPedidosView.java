package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TsccvPedidosDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvPedidosView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtActivo;
    private HtmlInputText txtObservaciones;
    private HtmlInputText txtRegistrado;
    private HtmlInputText txtCodpla_TsccvPlanta;
    private HtmlInputText txtIdUsua_TsccvUsuario;
    private HtmlInputText txtIdPedi;
    private SelectInputDate txtFechaCreacion;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TsccvPedidos> tsccvPedidos;
    private List<TsccvPedidosDTO> tsccvPedidosDTO;

    public TsccvPedidosView() {
        super("");
    }

    public String action_clear() {
        txtActivo.setValue(null);
        txtActivo.setDisabled(true);
        txtObservaciones.setValue(null);
        txtObservaciones.setDisabled(true);
        txtRegistrado.setValue(null);
        txtRegistrado.setDisabled(true);
        txtCodpla_TsccvPlanta.setValue(null);
        txtCodpla_TsccvPlanta.setDisabled(true);
        txtIdUsua_TsccvUsuario.setValue(null);
        txtIdUsua_TsccvUsuario.setDisabled(true);

        txtFechaCreacion.setValue(null);
        txtFechaCreacion.setDisabled(true);

        txtIdPedi.setValue(null);
        txtIdPedi.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TsccvPedidos entity = null;

            try {
                Integer idPedi = new Integer(txtIdPedi.getValue().toString());

                entity = BusinessDelegatorView.getTsccvPedidos(idPedi);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtActivo.setDisabled(false);
                txtObservaciones.setDisabled(false);
                txtRegistrado.setDisabled(false);
                txtCodpla_TsccvPlanta.setDisabled(false);
                txtIdUsua_TsccvUsuario.setDisabled(false);

                txtFechaCreacion.setDisabled(false);

                txtIdPedi.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtActivo.setValue(entity.getActivo());
                txtActivo.setDisabled(false);
                txtFechaCreacion.setValue(entity.getFechaCreacion());
                txtFechaCreacion.setDisabled(false);
                txtObservaciones.setValue(entity.getObservaciones());
                txtObservaciones.setDisabled(false);
                txtRegistrado.setValue(entity.getRegistrado());
                txtRegistrado.setDisabled(false);
                txtCodpla_TsccvPlanta.setValue(entity.getTsccvPlanta()
                                                     .getCodpla());
                txtCodpla_TsccvPlanta.setDisabled(false);
                txtIdUsua_TsccvUsuario.setValue(entity.getTsccvUsuario()
                                                      .getIdUsua());
                txtIdUsua_TsccvUsuario.setDisabled(false);

                txtIdPedi.setValue(entity.getIdPedi());
                txtIdPedi.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveTsccvPedidos((((txtActivo.getValue()) == null) ||
                (txtActivo.getValue()).equals("")) ? null
                                                   : new String(
                    txtActivo.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdPedi.getValue()) == null) ||
                (txtIdPedi.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdPedi.getValue().toString()), null,
                (((txtObservaciones.getValue()) == null) ||
                (txtObservaciones.getValue()).equals("")) ? null
                                                          : new String(
                    txtObservaciones.getValue().toString()),
                (((txtRegistrado.getValue()) == null) ||
                (txtRegistrado.getValue()).equals("")) ? null
                                                       : new String(
                    txtRegistrado.getValue().toString()),
                (((txtCodpla_TsccvPlanta.getValue()) == null) ||
                (txtCodpla_TsccvPlanta.getValue()).equals("")) ? null
                                                               : new String(
                    txtCodpla_TsccvPlanta.getValue().toString()),
                (((txtIdUsua_TsccvUsuario.getValue()) == null) ||
                (txtIdUsua_TsccvUsuario.getValue()).equals("")) ? null
                                                                : new Integer(
                    txtIdUsua_TsccvUsuario.getValue().toString()), null);
            //se envia null en el ultimo parametro ya que se agrego un parametro lista para los productos pedido

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTsccvPedidos((((txtIdPedi.getValue()) == null) ||
                (txtIdPedi.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdPedi.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateTsccvPedidos((((txtActivo.getValue()) == null) ||
                (txtActivo.getValue()).equals("")) ? null
                                                   : new String(
                    txtActivo.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdPedi.getValue()) == null) ||
                (txtIdPedi.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdPedi.getValue().toString()), null,
                (((txtObservaciones.getValue()) == null) ||
                (txtObservaciones.getValue()).equals("")) ? null
                                                          : new String(
                    txtObservaciones.getValue().toString()),
                (((txtRegistrado.getValue()) == null) ||
                (txtRegistrado.getValue()).equals("")) ? null
                                                       : new String(
                    txtRegistrado.getValue().toString()),
                (((txtCodpla_TsccvPlanta.getValue()) == null) ||
                (txtCodpla_TsccvPlanta.getValue()).equals("")) ? null
                                                               : new String(
                    txtCodpla_TsccvPlanta.getValue().toString()),
                (((txtIdUsua_TsccvUsuario.getValue()) == null) ||
                (txtIdUsua_TsccvUsuario.getValue()).equals("")) ? null
                                                                : new Integer(
                    txtIdUsua_TsccvUsuario.getValue().toString()), null);

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, Date fechaCreacion,
        Integer idPedi, Integer idEmpr_TsccvEmpresas, String observaciones, String registrado,
        String codpla_TsccvPlanta, Integer idUsua_TsccvUsuario, List<ProductosPedidoVO> listadoProductosPedido)
        throws Exception {
        try {
            BusinessDelegatorView.updateTsccvPedidos(activo, fechaCreacion,
                idPedi, idEmpr_TsccvEmpresas, observaciones, registrado, codpla_TsccvPlanta,
                idUsua_TsccvUsuario, listadoProductosPedido);
            renderManager.getOnDemandRenderer("TsccvPedidosView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TsccvPedidosView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TsccvPedidos> getTsccvPedidos() {
        if (flag) {
            try {
                tsccvPedidos = BusinessDelegatorView.getTsccvPedidos();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tsccvPedidos;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTsccvPedidos(List<TsccvPedidos> tsccvPedidos) {
        this.tsccvPedidos = tsccvPedidos;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTsccvPedidos() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvPedidos> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvPedidos = 0;

        try {
            totalNumberTsccvPedidos = BusinessDelegatorView.findTotalNumberTsccvPedidos()
                                                           .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvPedidos) {
            endIndex = totalNumberTsccvPedidos;
        }

        try {
            tsccvPedidos = BusinessDelegatorView.findPageTsccvPedidos(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvPedidos>(totalNumberTsccvPedidos, startRow,
            tsccvPedidos);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvPedidosDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvPedidos = 0;

        try {
            totalNumberTsccvPedidos = BusinessDelegatorView.findTotalNumberTsccvPedidos()
                                                           .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvPedidos) {
            endIndex = totalNumberTsccvPedidos;
        }

        try {
            tsccvPedidos = BusinessDelegatorView.findPageTsccvPedidos(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tsccvPedidos)) {
                tsccvPedidosDTO = new ArrayList<TsccvPedidosDTO>();

                for (TsccvPedidos tsccvPedidosTmp : tsccvPedidos) {
                    TsccvPedidosDTO tsccvPedidosDTO2 = new TsccvPedidosDTO();
                    tsccvPedidosDTO2.setIdPedi(tsccvPedidosTmp.getIdPedi()
                                                              .toString());

                    tsccvPedidosDTO2.setActivo((tsccvPedidosTmp.getActivo() != null)
                        ? tsccvPedidosTmp.getActivo().toString() : null);
                    tsccvPedidosDTO2.setFechaCreacion(tsccvPedidosTmp.getFechaCreacion());
                    tsccvPedidosDTO2.setObservaciones((tsccvPedidosTmp.getObservaciones() != null)
                        ? tsccvPedidosTmp.getObservaciones().toString() : null);
                    tsccvPedidosDTO2.setRegistrado((tsccvPedidosTmp.getRegistrado() != null)
                        ? tsccvPedidosTmp.getRegistrado().toString() : null);
                    tsccvPedidosDTO2.setCodpla_TsccvPlanta((tsccvPedidosTmp.getTsccvPlanta()
                                                                           .getCodpla() != null)
                        ? tsccvPedidosTmp.getTsccvPlanta().getCodpla().toString()
                        : null);
                    tsccvPedidosDTO2.setIdUsua_TsccvUsuario((tsccvPedidosTmp.getTsccvUsuario()
                                                                            .getIdUsua() != null)
                        ? tsccvPedidosTmp.getTsccvUsuario().getIdUsua()
                                         .toString() : null);
                    tsccvPedidosDTO2.setTsccvPedidos(tsccvPedidosTmp);
                    tsccvPedidosDTO2.setTsccvPedidosView(this);
                    tsccvPedidosDTO.add(tsccvPedidosDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvPedidosDTO>(totalNumberTsccvPedidos, startRow,
            tsccvPedidosDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tsccvPedidosDTO)) {
            for (TsccvPedidosDTO tsccvPedidosTmp : tsccvPedidosDTO) {
                renderManager.getOnDemandRenderer("TsccvPedidosView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tsccvPedidosDTO)) {
            for (TsccvPedidosDTO tsccvPedidosTmp : tsccvPedidosDTO) {
                renderManager.getOnDemandRenderer("TsccvPedidosView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(HtmlInputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public HtmlInputText getTxtObservaciones() {
        return txtObservaciones;
    }

    public void setTxtObservaciones(HtmlInputText txtObservaciones) {
        this.txtObservaciones = txtObservaciones;
    }

    public HtmlInputText getTxtRegistrado() {
        return txtRegistrado;
    }

    public void setTxtRegistrado(HtmlInputText txtRegistrado) {
        this.txtRegistrado = txtRegistrado;
    }

    public HtmlInputText getTxtCodpla_TsccvPlanta() {
        return txtCodpla_TsccvPlanta;
    }

    public void setTxtCodpla_TsccvPlanta(HtmlInputText txtCodpla_TsccvPlanta) {
        this.txtCodpla_TsccvPlanta = txtCodpla_TsccvPlanta;
    }

    public HtmlInputText getTxtIdUsua_TsccvUsuario() {
        return txtIdUsua_TsccvUsuario;
    }

    public void setTxtIdUsua_TsccvUsuario(HtmlInputText txtIdUsua_TsccvUsuario) {
        this.txtIdUsua_TsccvUsuario = txtIdUsua_TsccvUsuario;
    }

    public SelectInputDate getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(SelectInputDate txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public HtmlInputText getTxtIdPedi() {
        return txtIdPedi;
    }

    public void setTxtIdPedi(HtmlInputText txtIdPedi) {
        this.txtIdPedi = txtIdPedi;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TsccvPedidosDTO> getTsccvPedidosDTO() {
        return tsccvPedidosDTO;
    }

    public void setTsccvPedidosDTO(List<TsccvPedidosDTO> tsccvPedidosDTO) {
        this.tsccvPedidosDTO = tsccvPedidosDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvPedidos> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvPedidosDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
}
