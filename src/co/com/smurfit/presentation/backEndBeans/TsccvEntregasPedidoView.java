package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.BvpDirDspDAO;
import co.com.smurfit.dataaccess.dao.TsccvEntregasPedidoDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TsccvEntregasPedidoDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.FactoryFinder;
import javax.faces.application.ApplicationFactory;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.el.MethodBinding;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvEntregasPedidoView extends DataSource implements Renderable,
    DisposableBean {
	private HtmlInputText txtAnchoHojas;
    private HtmlInputText txtAnchoRollo;
    private HtmlInputText txtCantidad;
    private HtmlInputText txtDesdir;
    private HtmlInputText txtLargoHojas;
    private HtmlInputText txtIdPrpe_TsccvProductosPedido;
    private HtmlInputText txtIdEnpe;
    private SelectInputDate txtFechaEntrega;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TsccvEntregasPedido> tsccvEntregasPedido;
    private List<TsccvEntregasPedidoDTO> tsccvEntregasPedidoDTO;
    
    private boolean visible;
    private String managedBeanDestino;
    private ProductosPedidoVO prodPedVO;
    private List<TsccvEntregasPedido> listadoEntregasPedido;
    private Vector<String> escalaProducto;
    private Integer numeroEntregas;
    
    //listas desplegables
    private HtmlSelectOneMenu somDirecDespacho;
    private List<SelectItem> siDirecDespacho; 
    
    private Boolean isMolinos;//flag utilizado en la jspx para saber cuando el producto es de molinos

    public TsccvEntregasPedidoView() {
        super("");      
        visible = false;
    }

    public String action_clear() {
    	txtAnchoHojas.setValue(null);
        txtAnchoHojas.setDisabled(true);
        txtAnchoRollo.setValue(null);
        txtAnchoRollo.setDisabled(true);
        txtCantidad.setValue(null);
        txtCantidad.setDisabled(true);
        txtDesdir.setValue(null);
        txtDesdir.setDisabled(true);
        txtLargoHojas.setValue(null);
        txtLargoHojas.setDisabled(true);
        txtIdPrpe_TsccvProductosPedido.setValue(null);
        txtIdPrpe_TsccvProductosPedido.setDisabled(true);

        txtFechaEntrega.setValue(null);
        txtFechaEntrega.setDisabled(true);

        txtIdEnpe.setValue(null);
        txtIdEnpe.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TsccvEntregasPedido entity = null;

            try {
                Integer idEnpe = new Integer(txtIdEnpe.getValue().toString());

                entity = BusinessDelegatorView.getTsccvEntregasPedido(idEnpe);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtAnchoHojas.setDisabled(false);
                txtAnchoRollo.setDisabled(false);
                txtCantidad.setDisabled(false);
                txtDesdir.setDisabled(false);
                txtLargoHojas.setDisabled(false);
                txtIdPrpe_TsccvProductosPedido.setDisabled(false);

                txtFechaEntrega.setDisabled(false);

                txtIdEnpe.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtAnchoHojas.setValue(entity.getAnchoHojas());
                txtAnchoHojas.setDisabled(false);
                txtAnchoRollo.setValue(entity.getAnchoRollo());
                txtAnchoRollo.setDisabled(false);
                txtCantidad.setValue(entity.getCantidad());
                txtCantidad.setDisabled(false);
                txtDesdir.setValue(entity.getDesdir());
                txtDesdir.setDisabled(false);
                txtFechaEntrega.setValue(entity.getFechaEntrega());
                txtFechaEntrega.setDisabled(false);
                txtLargoHojas.setValue(entity.getLargoHojas());
                txtLargoHojas.setDisabled(false);
                txtIdPrpe_TsccvProductosPedido.setValue(entity.getTsccvProductosPedido()
                                                              .getIdPrpe());
                txtIdPrpe_TsccvProductosPedido.setDisabled(false);

                txtIdEnpe.setValue(entity.getIdEnpe());
                txtIdEnpe.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveTsccvEntregasPedido((((txtAnchoHojas.getValue()) == null) ||
                (txtAnchoHojas.getValue()).equals("")) ? null
                                                       : new String(
                    txtAnchoHojas.getValue().toString()),
                (((txtAnchoRollo.getValue()) == null) ||
                (txtAnchoRollo.getValue()).equals("")) ? null
                                                       : new String(
                    txtAnchoRollo.getValue().toString()),
                (((txtCantidad.getValue()) == null) ||
                (txtCantidad.getValue()).equals("")) ? null
                                                     : new Integer(
                    txtCantidad.getValue().toString()),
                (((txtDesdir.getValue()) == null) ||
                (txtDesdir.getValue()).equals("")) ? null
                                                   : new String(
                    txtDesdir.getValue().toString()),
                (((txtFechaEntrega.getValue()) == null) ||
                (txtFechaEntrega.getValue()).equals("")) ? null
                                                         : (Date) txtFechaEntrega.getValue(),
                (((txtIdEnpe.getValue()) == null) ||
                (txtIdEnpe.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdEnpe.getValue().toString()),
                (((txtLargoHojas.getValue()) == null) ||
                (txtLargoHojas.getValue()).equals("")) ? null
                                                       : new String(
                    txtLargoHojas.getValue().toString()),
                (((txtIdPrpe_TsccvProductosPedido.getValue()) == null) ||
                (txtIdPrpe_TsccvProductosPedido.getValue()).equals("")) ? null
                                                                        : new Integer(
                    txtIdPrpe_TsccvProductosPedido.getValue().toString()), null);

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTsccvEntregasPedido((((txtIdEnpe.getValue()) == null) ||
                (txtIdEnpe.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdEnpe.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
        	BusinessDelegatorView.updateTsccvEntregasPedido((((txtAnchoHojas.getValue()) == null) ||
                (txtAnchoHojas.getValue()).equals("")) ? null
                                                       : new String(
                    txtAnchoHojas.getValue().toString()),
                (((txtAnchoRollo.getValue()) == null) ||
                (txtAnchoRollo.getValue()).equals("")) ? null
                                                       : new String(
                    txtAnchoRollo.getValue().toString()),
                (((txtCantidad.getValue()) == null) ||
                (txtCantidad.getValue()).equals("")) ? null
                                                     : new Integer(
                    txtCantidad.getValue().toString()),
                (((txtDesdir.getValue()) == null) ||
                (txtDesdir.getValue()).equals("")) ? null
                                                   : new String(
                    txtDesdir.getValue().toString()),
                (((txtFechaEntrega.getValue()) == null) ||
                (txtFechaEntrega.getValue()).equals("")) ? null
                                                         : (Date) txtFechaEntrega.getValue(),
                (((txtIdEnpe.getValue()) == null) ||
                (txtIdEnpe.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdEnpe.getValue().toString()),
                (((txtLargoHojas.getValue()) == null) ||
                (txtLargoHojas.getValue()).equals("")) ? null
                                                       : new String(
                    txtLargoHojas.getValue().toString()),
                (((txtIdPrpe_TsccvProductosPedido.getValue()) == null) ||
                (txtIdPrpe_TsccvProductosPedido.getValue()).equals("")) ? null
                                                                        : new Integer(
                    txtIdPrpe_TsccvProductosPedido.getValue().toString()),null);

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String anchoHojas, String anchoRollo,
        Integer cantidad, String desdir, Date fechaEntrega, Integer idEnpe,
        String largoHojas, Integer idPrpe_TsccvProductosPedido)
        throws Exception {
        try {
            BusinessDelegatorView.updateTsccvEntregasPedido(anchoHojas,
                anchoRollo, cantidad, desdir, fechaEntrega, idEnpe, largoHojas,
                idPrpe_TsccvProductosPedido,null);
            renderManager.getOnDemandRenderer("TsccvEntregasPedidoView")
                         .requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TsccvEntregasPedidoView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TsccvEntregasPedido> getTsccvEntregasPedido() {
        if (flag) {
            try {
                tsccvEntregasPedido = BusinessDelegatorView.getTsccvEntregasPedido();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tsccvEntregasPedido;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTsccvEntregasPedido(
        List<TsccvEntregasPedido> tsccvEntregasPedido) {
        this.tsccvEntregasPedido = tsccvEntregasPedido;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTsccvEntregasPedido() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvEntregasPedido> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvEntregasPedido = 0;

        try {
            totalNumberTsccvEntregasPedido = BusinessDelegatorView.findTotalNumberTsccvEntregasPedido()
                                                                  .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvEntregasPedido) {
            endIndex = totalNumberTsccvEntregasPedido;
        }

        try {
            tsccvEntregasPedido = BusinessDelegatorView.findPageTsccvEntregasPedido(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvEntregasPedido>(totalNumberTsccvEntregasPedido,
            startRow, tsccvEntregasPedido);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvEntregasPedidoDTO> getDataPageDTO(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvEntregasPedido = 0;

        try {
            totalNumberTsccvEntregasPedido = BusinessDelegatorView.findTotalNumberTsccvEntregasPedido()
                                                                  .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvEntregasPedido) {
            endIndex = totalNumberTsccvEntregasPedido;
        }

        try {
            tsccvEntregasPedido = BusinessDelegatorView.findPageTsccvEntregasPedido(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tsccvEntregasPedido)) {
                tsccvEntregasPedidoDTO = new ArrayList<TsccvEntregasPedidoDTO>();

                for (TsccvEntregasPedido tsccvEntregasPedidoTmp : tsccvEntregasPedido) {
                    TsccvEntregasPedidoDTO tsccvEntregasPedidoDTO2 = new TsccvEntregasPedidoDTO();
                    tsccvEntregasPedidoDTO2.setIdEnpe(tsccvEntregasPedidoTmp.getIdEnpe()
                                                                            .toString());

                    tsccvEntregasPedidoDTO2.setCantidad((tsccvEntregasPedidoTmp.getCantidad() != null)
                        ? tsccvEntregasPedidoTmp.getCantidad().toString() : null);
                    tsccvEntregasPedidoDTO2.setDesdir((tsccvEntregasPedidoTmp.getDesdir() != null)
                        ? tsccvEntregasPedidoTmp.getDesdir().toString() : null);
                    tsccvEntregasPedidoDTO2.setFechaEntrega(tsccvEntregasPedidoTmp.getFechaEntrega());
                    tsccvEntregasPedidoDTO2.setIdPrpe_TsccvProductosPedido((tsccvEntregasPedidoTmp.getTsccvProductosPedido()
                                                                                                  .getIdPrpe() != null)
                        ? tsccvEntregasPedidoTmp.getTsccvProductosPedido()
                                                .getIdPrpe().toString() : null);
                    tsccvEntregasPedidoDTO2.setTsccvEntregasPedido(tsccvEntregasPedidoTmp);
                    tsccvEntregasPedidoDTO2.setTsccvEntregasPedidoView(this);
                    tsccvEntregasPedidoDTO.add(tsccvEntregasPedidoDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvEntregasPedidoDTO>(totalNumberTsccvEntregasPedido,
            startRow, tsccvEntregasPedidoDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tsccvEntregasPedidoDTO)) {
            for (TsccvEntregasPedidoDTO tsccvEntregasPedidoTmp : tsccvEntregasPedidoDTO) {
                renderManager.getOnDemandRenderer("TsccvEntregasPedidoView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tsccvEntregasPedidoDTO)) {
            for (TsccvEntregasPedidoDTO tsccvEntregasPedidoTmp : tsccvEntregasPedidoDTO) {
                renderManager.getOnDemandRenderer("TsccvEntregasPedidoView")
                             .add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtCantidad() {
        return txtCantidad;
    }

    public void setTxtCantidad(HtmlInputText txtCantidad) {
        this.txtCantidad = txtCantidad;
    }

    public HtmlInputText getTxtDesdir() {
        return txtDesdir;
    }

    public void setTxtDesdir(HtmlInputText txtDesdir) {
        this.txtDesdir = txtDesdir;
    }

    public HtmlInputText getTxtIdPrpe_TsccvProductosPedido() {
        return txtIdPrpe_TsccvProductosPedido;
    }

    public void setTxtIdPrpe_TsccvProductosPedido(
        HtmlInputText txtIdPrpe_TsccvProductosPedido) {
        this.txtIdPrpe_TsccvProductosPedido = txtIdPrpe_TsccvProductosPedido;
    }

    public SelectInputDate getTxtFechaEntrega() {
        return txtFechaEntrega;
    }

    public void setTxtFechaEntrega(SelectInputDate txtFechaEntrega) {
        this.txtFechaEntrega = txtFechaEntrega;
    }

    public HtmlInputText getTxtIdEnpe() {
        return txtIdEnpe;
    }

    public void setTxtIdEnpe(HtmlInputText txtIdEnpe) {
        this.txtIdEnpe = txtIdEnpe;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TsccvEntregasPedidoDTO> getTsccvEntregasPedidoDTO() {
        return tsccvEntregasPedidoDTO;
    }

    public void setTsccvEntregasPedidoDTO(
        List<TsccvEntregasPedidoDTO> tsccvEntregasPedidoDTO) {
        this.tsccvEntregasPedidoDTO = tsccvEntregasPedidoDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }


	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public HtmlSelectOneMenu getSomDirecDespacho() {
		return somDirecDespacho;
	}

	public void setSomDirecDespacho(HtmlSelectOneMenu somDirecDespacho) {
		this.somDirecDespacho = somDirecDespacho;
	}

	public List<SelectItem> getSiDirecDespacho() {
		return siDirecDespacho;
	}

	public void setSiDirecDespacho(List<SelectItem> siDirecDespacho) {
		this.siDirecDespacho = siDirecDespacho;
	}

	public Integer getNumeroEntregas() {
		return numeroEntregas;
	}

	public void setNumeroEntregas(Integer numeroEntregas) {
		this.numeroEntregas = numeroEntregas;
	}

	public List<TsccvEntregasPedido> getListadoEntregasPedido() {
		return listadoEntregasPedido;
	}

	public void setListadoEntregasPedido(
			List<TsccvEntregasPedido> listadoEntregasPedido) {
		this.listadoEntregasPedido = listadoEntregasPedido;
	}
    	
	public String getManagedBeanDestino() {
		return managedBeanDestino;
	}

	public void setManagedBeanDestino(String managedBeanDestino) {
		this.managedBeanDestino = managedBeanDestino;
	}

	public ProductosPedidoVO getProdPedVO() {
		return prodPedVO;
	}

	public void setProdPedVO(ProductosPedidoVO prodPedVO) {
		this.prodPedVO = prodPedVO;
	}

	public Vector<String> getEscalaProducto() {
		return escalaProducto;
	}

	public void setEscalaProducto(Vector<String> escalaProducto) {
		this.escalaProducto = escalaProducto;
	}

	public Boolean getIsMolinos() {
		return isMolinos;
	}

	public void setIsMolinos(Boolean isMolinos) {
		this.isMolinos = isMolinos;
	}
	
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvEntregasPedido> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvEntregasPedidoDTO> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
    
    //metodo encargado de llamar el metodo que guarda las entregas pedido
    public void aceptar(){
    	try{
    		//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        Date fechaActual = new Date();
    		for(TsccvEntregasPedido entrePed : listadoEntregasPedido){
    			/*
    			if(entrePed.getCantidad() == null || entrePed.getCantidad().equals(new Integer(0))){
    				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
    				mensaje.mostrarMensaje("Elimine las entregas sin cantidad para continuar.", MBMensaje.MENSAJE_ALERTA);    				
    				return;
        		}*/
    			//si el tipo de producto de la planta del pedido es corrugado o sacos se valida que haya precio definido para la cantidad ingresada, 
    			long cantidadTotalEntrega=0;
    			boolean encuentraEscala=false;
    			//para lo cual se valida la cantidad ingresada contra la escala del producto
    			for(TsccvEntregasPedido entrega : listadoEntregasPedido){
    				if(!entrePed.equals(entrega)){
    					if(entrePed.getFechaEntrega().equals(entrega.getFechaEntrega())){
    						cantidadTotalEntrega+= entrega.getCantidad();
    					}
    				}
    			}
    			if(!prodPedVO.getTsccvProductosPedido().getTsccvPedidos().getTsccvPlanta().getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
    				//Validamos si el producto se encuentra en alguna de las escalas considerando si existen pedidos para la misma fecha
    				for(int i=0;i< escalaProducto.size(); i=i+3){
	    				if(((entrePed.getCantidad() + cantidadTotalEntrega) >= new Double(escalaProducto.get(i).trim()) && (entrePed.getCantidad() + cantidadTotalEntrega) <= new Double(escalaProducto.get(i+1).trim()))){
	    					encuentraEscala=true;
	    					entrePed.setPrecio(escalaProducto.get(i+2).trim());
	    					break;
	    				}
    				}
    			}
    			
    			if(!encuentraEscala){
	    			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
					mensaje.mostrarMensaje("No existe precio definido para la cantidad ["+entrePed.getCantidad()+"].", MBMensaje.MENSAJE_ALERTA);
					
					return;
    			}
    			
    			//se valida que niguna da las fechas de entrega del pedido sea menor a la fecha actual
    			/*if((entrePed.getFechaEntrega().getYear() < fechaActual.getYear()) || (entrePed.getFechaEntrega().getYear() == fechaActual.getYear() && 
    					entrePed.getFechaEntrega().getMonth() == fechaActual.getMonth() &&
    					entrePed.getFechaEntrega().getDate() < (fechaActual.getDate()+1))){
    				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
    				mensaje.mostrarMensaje("No se aceptan pedidos con fecha de solicitud menor a 24 horas.", MBMensaje.MENSAJE_ALERTA);
    				
    				return;
        		}*/
    			
    			long diff = (entrePed.getFechaEntrega().getTime() - fechaActual.getTime())/( 1000 * 60 * 60 );
    			
    			if(diff < 24){
    				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
    				mensaje.mostrarMensaje("No se aceptan pedidos con fecha de solicitud menor a 24 horas.", MBMensaje.MENSAJE_ALERTA);
    				return;
    			}
    		}
    		
    		//se guardan las entregas del producto pedido
			BusinessDelegatorView.saveTsccvEntregasPedido(null, null, null, null, null,
	    			null, null, prodPedVO.getTsccvProductosPedido().getIdPrpe(), listadoEntregasPedido);

	    	//se obtiene el manage bean de planDeEntrega
	    	PlanDeEntregaView planDeEntregaView = (PlanDeEntregaView) FacesUtils.getManagedBean("planDeEntregaView");
	    	
	    	//se crea una lista con el elemento seleccionado ya que el metodo updateTsccvProductosPedido se modifico
	    	List<ProductosPedidoVO> listadoProdPed = new ArrayList<ProductosPedidoVO>();
	    	
	    	prodPedVO.getTsccvProductosPedido().setTsccvEntregasPedidos(new HashSet<TsccvEntregasPedido>(listadoEntregasPedido));
	    	listadoProdPed.add(prodPedVO);
	    	
	    	//se actualiza el elemento de la lista
	    	BusinessDelegatorView.updateTsccvProductosPedido(null, null, null, null, null, null, null, null, null, null, null,
	    													 null, null, null, null, null, listadoProdPed);
	    	//se llama el metodo consultar del planDeEntrega para refrescar la lista de productos pedido
	    	planDeEntregaView.consultarProductosPedido();
	    	
	    	//se oculta el panel
	    	ocultarPanel();
    	}catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}	
    }
    
    public void cancelar(){
    	listadoEntregasPedido.clear();
    	//se obtiene el manage bean de planDeEntrega
    	PlanDeEntregaView planDeEntregaView = (PlanDeEntregaView) FacesUtils.getManagedBean("planDeEntregaView");
    	//se llama el metodo consultar del planDeEntrega para refrescar la lista de productos pedido
    	planDeEntregaView.consultarProductosPedido();
    	ocultarPanel();
    }
    
    public void mostrarPanel(){
    	try{
	    	//se crea el listado
	        listadoEntregasPedido = new ArrayList<TsccvEntregasPedido>();
	        
	        HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	        prodPedVO = (ProductosPedidoVO)sesion.getAttribute("prodPedVO");
	        
	        //se carga la lista de direcciones de despacho
	    	cargarDireccionesDespacho();
	    	configurarParaMolinos();
	        
	    	List<TsccvEntregasPedido> entregasTemp = BusinessDelegatorView.findByPropertyEntregasPedido(TsccvEntregasPedidoDAO.PRODPED, prodPedVO.getTsccvProductosPedido().getIdPrpe());	    	
	    	
	        //si la lista de entregas del productoPedido esta vacia se crea un listado con la cantidad especificada
	    	if(entregasTemp == null || entregasTemp.size() == 0){
	        	for(int i = 1; i <= numeroEntregas; i++){
	        		TsccvEntregasPedido entrePed = new TsccvEntregasPedido();
	        		listadoEntregasPedido.add(entrePed);
	        	}        	
	        }
	        else{
	        	//si ya hay entregas especificadas se muestra este listado
	        	listadoEntregasPedido = entregasTemp;
	        	
	        	//si el numeroEntregas es mayor al especificado anteriormente se adicionan objetos para las nuevas entregas
	        	int numeroAnterior = entregasTemp.size();
	        	if(numeroEntregas > numeroAnterior){
	        		int cantNuevasEntregas = numeroEntregas - numeroAnterior;
	        		for(int i = 1; i <= cantNuevasEntregas; i++){
	            		TsccvEntregasPedido entrePed = new TsccvEntregasPedido();
	            		listadoEntregasPedido.add(entrePed);
	            	}
	        	}
	        }
	    	
	    	//se muestra el panel
	    	this.visible = true;
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    }
    
    public String ocultarPanel() {
    	String gui = "";
		Object objReturn = null;
		visible = false;
		String strDestino = this.getManagedBeanDestino();
		if (strDestino != null && strDestino.length() > 0) {
			MethodBinding metodo = getMethodBinding(strDestino, null);
			objReturn = metodo.invoke(FacesContext.getCurrentInstance(), null);
			if (objReturn != null) {
				gui = (String) objReturn;
			}
		}
		//FacesContext.getCurrentInstance().notifyAll();
		
		return gui;	
	}
    
    public static MethodBinding getMethodBinding(String bindingName,
			Class params[]) {
		ApplicationFactory factory = (ApplicationFactory) FactoryFinder
				.getFactory(FactoryFinder.APPLICATION_FACTORY);
		MethodBinding binding = factory.getApplication().createMethodBinding(
				bindingName, params);
		return binding;
	}
    
    
    public void eliminarEntrega(ActionEvent event){
    	try{
    		TsccvEntregasPedido entrePed = (TsccvEntregasPedido)event.getComponent().getAttributes().get("entrePed");
			
    		//si la entrega no se ha guardado en base de datos solo se elimina de la lista
    		if(entrePed.getIdEnpe() == null){
    			listadoEntregasPedido.remove(entrePed);
    		}
    		else{
    			BusinessDelegatorView.deleteTsccvEntregasPedido(entrePed.getIdEnpe());
    			listadoEntregasPedido.remove(entrePed);
    			

    			//se crea una lista con el elemento seleccionado ya que el metodo updateTsccvProductosPedido se modifico
    	    	List<ProductosPedidoVO> listadoProdPed = new ArrayList<ProductosPedidoVO>();
    	    	prodPedVO.getTsccvProductosPedido().setTsccvEntregasPedidos(new HashSet<TsccvEntregasPedido>(listadoEntregasPedido));
    	    	listadoProdPed.add(prodPedVO);
    	    	
    	    	//se actualiza el elemento de la lista
    	    	BusinessDelegatorView.updateTsccvProductosPedido(null, null, null, null, null, null, null, null, null, null, null,
    	    													 null, null, null, null, null, listadoProdPed);
    	    	
    	    	//se obtiene el manage bean de planDeEntrega
    	    	PlanDeEntregaView planDeEntregaView = (PlanDeEntregaView) FacesUtils.getManagedBean("planDeEntregaView");
    	    	//se llama el metodo consultar del planDeEntrega para refrescar la lista de productos pedido
    	    	planDeEntregaView.consultarProductosPedido();
    		}
    		
    		if(listadoEntregasPedido.size() == 0){
    			ocultarPanel();
    		}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    }
    
    //metodo encargado de cargar las direcciones de despacho depeendiendo del tipo de producto de la planta
    public void cargarDireccionesDespacho(){
    	try{
        	//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        //Se crea un objeto del bundle		
	        ArchivoBundle archivoBundle = new ArchivoBundle();
	        
	        //se inicializan los componentes de las listas
	    	siDirecDespacho = new ArrayList<SelectItem>();
	    	
	    	//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue("-");
        	siDirecDespacho.add(siNulo);
	    	
	    	//se crear los listados
			List<BvpDirDsp> listadoBvpDirecDespacho = null;
			List<SapDirDsp> listadoSapDirecDespacho = null;
			
			
			//se cargan los valores de las listas	
			TsccvPlanta tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(prodPedVO.getTsccvProductosPedido().getCodpla());
			
			if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_CORRUGADO"))) ||
	    	   tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_SACOS")))){
			   
				//se consultan las direcciones de despacho para la planta
				listadoBvpDirecDespacho = BusinessDelegatorView.consultarDireccionesClientePlanta(prodPedVO.getTsccvProductosPedido().getTsccvPedidos().getTsccvEmpresas().getCodVpe(), tsccvPlanta.getCodpla());
				
				//lista de direcciones de despacho
				for(BvpDirDsp objetoDirDsp: listadoBvpDirecDespacho){
	        		SelectItem siTipPro = new SelectItem();
	        		siTipPro.setLabel(objetoDirDsp.getId().getDesdir());
	        		siTipPro.setValue(objetoDirDsp.getId().getCoddir()+"-"+ objetoDirDsp.getId().getDesdir());
	        		
	        		siDirecDespacho.add(siTipPro);
	        	}
			}
			else{
				listadoSapDirecDespacho = BusinessDelegatorView.consultarDireccionesCliente(prodPedVO.getTsccvProductosPedido().getTsccvPedidos().getTsccvEmpresas().getCodMas());
				//lista de direcciones de despacho
				for(SapDirDsp objetoDirDsp: listadoSapDirecDespacho){
	        		SelectItem siTipPro = new SelectItem();
	        		siTipPro.setLabel(objetoDirDsp.getId().getDesdir());
	        		siTipPro.setValue(objetoDirDsp.getId().getDesdir());
	        		
	        		siDirecDespacho.add(siTipPro);
	        	}
			}
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
    	catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    }

    
    //este metodso es invocado desde la pantalla ingresoOrdenes si el pedido es para una planta con tipo producto molinos
	//y se encarga de cargar la lista de unidades de medida y cambiar el falg (isMolinos) a true para que los componentes en la jspx sean desplegados 
	public void configurarParaMolinos(){
		try{	
			//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();	        			
	        
			//si el tipo de producto de la planta del pedido es molinos se crea la lista de unidades de medida 
			if(prodPedVO.getTsccvProductosPedido().getTsccvPedidos().getTsccvPlanta().getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
				isMolinos = true;//se coloca el flag en true para indicar que el producto es para molinos
			}
			else{
				isMolinos = false;//se coloca el flag en false para indicar que el producto no es para molinos
			}
		}catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
    	catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
}
