package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TsccvDetalleGrupoEmpresaDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TsccvEmpresasDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.DetalleGrupoEmpresaVO;
import co.com.smurfit.utilities.CargarEmpresas;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlInputTextarea;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.inputfile.FileInfo;
import com.icesoft.faces.component.inputfile.InputFile;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvEmpresasView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtActivo;
    private HtmlInputText txtCodMas;
    private HtmlInputText txtCodSap;
    private HtmlInputText txtCodVpe;
    private HtmlInputText txtNombre;
    private HtmlInputText txtIdGrue_TsccvGrupoEmpresa;
    private HtmlInputText txtIdEmpr;
    private SelectInputDate txtFechaCreacion;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnInactivar;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TsccvEmpresas> tsccvEmpresas;
    private List<TsccvEmpresasDTO> tsccvEmpresasDTO;
    
    //listas desplegables
    private HtmlSelectOneMenu somActivo;
    private List<SelectItem> siActivo;
    private String[] valorParametroActivo;
    private HtmlSelectOneMenu somEmpresa;
    private List<SelectItem> siEmpresa;
    
    
    //componentes para el cargue de empresas
    private InputFile archivo;
    
    private List<DetalleGrupoEmpresaVO> listadoDetalleEmpresaVO;
    
    private HtmlInputTextarea txtLogErrores;

    public TsccvEmpresasView() {
        super("");
        
        //Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();
		
		try{
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
			siActivo = new ArrayList<SelectItem>();
			siEmpresa = new ArrayList<SelectItem>();
			
			//se crear los listados
			String valorParametroActivo[] = null;
			List<TsccvEmpresas> listadoEmpresas = null;
			listadoDetalleEmpresaVO = new ArrayList<DetalleGrupoEmpresaVO>();
			
			//se cargan los valores de las listas
			valorParametroActivo =  getValorParametroAct();
			listadoEmpresas = BusinessDelegatorView.getTsccvEmpresas();
            
			//lista de activo			
        	siActivo.add(siNulo);        	
            for(int i=0; i<valorParametroActivo.length; i++){
        		if(valorParametroActivo[i] == null || valorParametroActivo[i].equals("")){
        			continue;
        		}
            	SelectItem siAct = new SelectItem();        		
        		siAct.setLabel(valorParametroActivo[i]);
        		siAct.setValue(new Long(i));
        		
        		siActivo.add(siAct);
        	}
            
            //lista de empresas
        	/*siEmpresa.add(siNulo);
        	for(TsccvEmpresas objetoEmpresa: listadoEmpresas){
        		if(objetoEmpresa.getActivo().equals("1") || objetoEmpresa.getTsccvGrupoEmpresa() != null){
        			continue;
        		}
        		SelectItem siEmpr = new SelectItem();        		
        		siEmpr.setLabel(objetoEmpresa.getNombre());
        		siEmpr.setValue(objetoEmpresa.getIdEmpr());
        		
        		siEmpresa.add(siEmpr);
        	}*/
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
    }

    public String action_clear() {
        txtActivo.setValue(null);
        txtActivo.setDisabled(false);
        somActivo.setValue(new Long(-1));
        txtCodMas.setValue(null);
        txtCodMas.setDisabled(false);
        txtCodSap.setValue(null);
        txtCodSap.setDisabled(false);
        txtCodVpe.setValue(null);
        txtCodVpe.setDisabled(false);
        txtNombre.setValue(null);
        txtNombre.setDisabled(false);
        txtIdGrue_TsccvGrupoEmpresa.setValue(null);
        txtIdGrue_TsccvGrupoEmpresa.setDisabled(false);

        txtFechaCreacion.setValue(null);
        txtFechaCreacion.setDisabled(false);

        txtIdEmpr.setValue(null);
        txtIdEmpr.setDisabled(false);
        //somEmpresa.setValue(new Long(-1));
        
        txtLogErrores.setRendered(false);
        txtLogErrores.resetValue();

        btnSave.setDisabled(false);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnInactivar.setDisabled(true);
        btnClear.setDisabled(false);
        
        listadoDetalleEmpresaVO.clear();        
        onePageDataModel = null;
        flag = true;
        getData();

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TsccvEmpresas entity = null;

            try {
                Integer idEmpr = new Integer(txtIdEmpr.getValue().toString());

                entity = BusinessDelegatorView.getTsccvEmpresas(idEmpr);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtActivo.setDisabled(false);
                txtCodMas.setDisabled(false);
                txtCodSap.setDisabled(false);
                txtCodVpe.setDisabled(false);
                txtNombre.setDisabled(false);
                txtIdGrue_TsccvGrupoEmpresa.setDisabled(false);

                txtFechaCreacion.setDisabled(false);

                txtIdEmpr.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnInactivar.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtActivo.setValue(entity.getActivo());
                txtActivo.setDisabled(false);
                somActivo.setValue(entity.getActivo());
                txtCodMas.setValue(entity.getCodMas());
                txtCodMas.setDisabled(false);
                txtCodSap.setValue(entity.getCodSap());
                txtCodSap.setDisabled(false);
                txtCodVpe.setValue(entity.getCodVpe());
                txtCodVpe.setDisabled(false);
                txtFechaCreacion.setValue(entity.getFechaCreacion());
                txtFechaCreacion.setDisabled(false);
                txtNombre.setValue(entity.getNombre());
                txtNombre.setDisabled(false);
                txtIdGrue_TsccvGrupoEmpresa.setValue(entity.getTsccvGrupoEmpresa().getIdGrue());                
                txtIdGrue_TsccvGrupoEmpresa.setDisabled(false);                

                txtIdEmpr.setValue(entity.getIdEmpr());
                txtIdEmpr.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnInactivar.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {    	
        try {
        	//ANTES DE GUARDAR SE ASIGNA EL VALOR DE LA LISTA SELECCIONADO AL CAMPO DE TEXTO RESPECTIVO
    		txtActivo.setValue(new Long(somActivo.getValue().toString()) != -1 ? somActivo.getValue() : null);
        	txtFechaCreacion.setValue(new Date());
        	        	 
        	/*if(BusinessDelegatorView.empresaIsRegistrada(txtNombre.getValue().toString(), txtCodSap.getValue().toString(),
    													 txtCodMas.getValue().toString(), txtCodVpe.getValue().toString())){
    			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("La Empresa ya se encuentra registrada", MBMensaje.MENSAJE_ALERTA);
              	
              	return "";
    		}*/
        	
            BusinessDelegatorView.saveTsccvEmpresas((((txtActivo.getValue()) == null) ||
                (txtActivo.getValue()).equals("")) ? null
                                                   : new String(
                    txtActivo.getValue().toString()),
                (((txtCodMas.getValue()) == null) ||
                (txtCodMas.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodMas.getValue().toString()),
                (((txtCodSap.getValue()) == null) ||
                (txtCodSap.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodSap.getValue().toString()),
                (((txtCodVpe.getValue()) == null) ||
                (txtCodVpe.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodVpe.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdEmpr.getValue()) == null) ||
                (txtIdEmpr.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdEmpr.getValue().toString()),
                (((txtNombre.getValue()) == null) ||
                (txtNombre.getValue()).equals("")) ? null
                                                   : new String(
                    txtNombre.getValue().toString()).toUpperCase(),
                (((txtIdGrue_TsccvGrupoEmpresa.getValue()) == null) ||
                (txtIdGrue_TsccvGrupoEmpresa.getValue()).equals("")) ? null
                                                                     : new Integer(
                    txtIdGrue_TsccvGrupoEmpresa.getValue().toString()), listadoDetalleEmpresaVO);

            //se refresca la lista de seleccion empresas
            refrescarListaEmpresas();
            
            MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
          	mensaje.mostrarMensaje("Empresa adicionada de forma satisfactoria", MBMensaje.MENSAJE_OK);

        } catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTsccvEmpresas((((txtIdEmpr.getValue()) == null) ||
                (txtIdEmpr.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdEmpr.getValue().toString()));

            //se refresca la lista de seleccion empresas
            refrescarListaEmpresas();
            
            MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
          	mensaje.mostrarMensaje("Empresa eliminada de forma satisfactoria", MBMensaje.MENSAJE_OK);
        } catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
        	txtActivo.setValue(somActivo.getValue());
        	txtFechaCreacion.setValue(new Date());
        	
            BusinessDelegatorView.updateTsccvEmpresas((((txtActivo.getValue()) == null) ||
                (txtActivo.getValue()).equals("")) ? null
                                                   : new String(
                    txtActivo.getValue().toString()),
                (((txtCodMas.getValue()) == null) ||
                (txtCodMas.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodMas.getValue().toString()),
                (((txtCodSap.getValue()) == null) ||
                (txtCodSap.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodSap.getValue().toString()),
                (((txtCodVpe.getValue()) == null) ||
                (txtCodVpe.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodVpe.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdEmpr.getValue()) == null) ||
                (txtIdEmpr.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdEmpr.getValue().toString()),
                (((txtNombre.getValue()) == null) ||
                (txtNombre.getValue()).equals("")) ? null
                                                   : new String(
                    txtNombre.getValue().toString()).toUpperCase(),
                (((txtIdGrue_TsccvGrupoEmpresa.getValue()) == null) ||
                (txtIdGrue_TsccvGrupoEmpresa.getValue()).equals("")) ? null
                                                                     : new Integer(
                    txtIdGrue_TsccvGrupoEmpresa.getValue().toString()), listadoDetalleEmpresaVO);

            //se refresca la lista de seleccion empresas
            refrescarListaEmpresas();
            
            MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
          	mensaje.mostrarMensaje("Empresa modificada de forma satisfactoria", MBMensaje.MENSAJE_OK);

        } catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String activo, String codMas,
        String codSap, String codVpe, Date fechaCreacion, Integer idEmpr,
        String nombre, Integer idGrue_TsccvGrupoEmpresa, List<DetalleGrupoEmpresaVO> listadoDetalleEmpresaVO)
        throws Exception {
        try {
            BusinessDelegatorView.updateTsccvEmpresas(activo, codMas, codSap,
                codVpe, fechaCreacion, idEmpr, nombre, idGrue_TsccvGrupoEmpresa, listadoDetalleEmpresaVO);
            renderManager.getOnDemandRenderer("TsccvEmpresasView")
                         .requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TsccvEmpresasView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TsccvEmpresas> getTsccvEmpresas() {
        if (flag) {
            try {
                tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tsccvEmpresas;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTsccvEmpresas(List<TsccvEmpresas> tsccvEmpresas) {
        this.tsccvEmpresas = tsccvEmpresas;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTsccvEmpresas() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvEmpresas> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvEmpresas = 0;

        try {
            totalNumberTsccvEmpresas = BusinessDelegatorView.findTotalNumberTsccvEmpresas()
                                                            .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvEmpresas) {
            endIndex = totalNumberTsccvEmpresas;
        }

        try {
            tsccvEmpresas = BusinessDelegatorView.findPageTsccvEmpresas(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvEmpresas>(totalNumberTsccvEmpresas, startRow,
            tsccvEmpresas);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvEmpresasDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvEmpresas = 0;

        try {
            totalNumberTsccvEmpresas = BusinessDelegatorView.findTotalNumberTsccvEmpresas()
                                                            .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvEmpresas) {
            endIndex = totalNumberTsccvEmpresas;
        }

        try {
            tsccvEmpresas = BusinessDelegatorView.findPageTsccvEmpresas(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tsccvEmpresas)) {
                tsccvEmpresasDTO = new ArrayList<TsccvEmpresasDTO>();

                for (TsccvEmpresas tsccvEmpresasTmp : tsccvEmpresas) {
                    TsccvEmpresasDTO tsccvEmpresasDTO2 = new TsccvEmpresasDTO();
                    tsccvEmpresasDTO2.setIdEmpr(tsccvEmpresasTmp.getIdEmpr()
                                                                .toString());

                    tsccvEmpresasDTO2.setActivo((tsccvEmpresasTmp.getActivo() != null)
                        ? tsccvEmpresasTmp.getActivo().toString() : null);
                    tsccvEmpresasDTO2.setCodMas((tsccvEmpresasTmp.getCodMas() != null)
                        ? tsccvEmpresasTmp.getCodMas().toString() : null);
                    tsccvEmpresasDTO2.setCodSap((tsccvEmpresasTmp.getCodSap() != null)
                        ? tsccvEmpresasTmp.getCodSap().toString() : null);
                    tsccvEmpresasDTO2.setCodVpe((tsccvEmpresasTmp.getCodVpe() != null)
                        ? tsccvEmpresasTmp.getCodVpe().toString() : null);
                    tsccvEmpresasDTO2.setFechaCreacion(tsccvEmpresasTmp.getFechaCreacion());
                    tsccvEmpresasDTO2.setNombre((tsccvEmpresasTmp.getNombre() != null)
                        ? tsccvEmpresasTmp.getNombre().toString() : null);
                    tsccvEmpresasDTO2.setIdGrue_TsccvGrupoEmpresa((tsccvEmpresasTmp.getTsccvGrupoEmpresa()
                                                                                   .getIdGrue() != null)
                        ? tsccvEmpresasTmp.getTsccvGrupoEmpresa().getIdGrue()
                                          .toString() : null);
                    tsccvEmpresasDTO2.setTsccvEmpresas(tsccvEmpresasTmp);
                    tsccvEmpresasDTO2.setTsccvEmpresasView(this);
                    tsccvEmpresasDTO.add(tsccvEmpresasDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvEmpresasDTO>(totalNumberTsccvEmpresas,
            startRow, tsccvEmpresasDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tsccvEmpresasDTO)) {
            for (TsccvEmpresasDTO tsccvEmpresasTmp : tsccvEmpresasDTO) {
                renderManager.getOnDemandRenderer("TsccvEmpresasView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tsccvEmpresasDTO)) {
            for (TsccvEmpresasDTO tsccvEmpresasTmp : tsccvEmpresasDTO) {
                renderManager.getOnDemandRenderer("TsccvEmpresasView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtActivo() {
        return txtActivo;
    }

    public void setTxtActivo(HtmlInputText txtActivo) {
        this.txtActivo = txtActivo;
    }

    public HtmlInputText getTxtCodMas() {
        return txtCodMas;
    }

    public void setTxtCodMas(HtmlInputText txtCodMas) {
        this.txtCodMas = txtCodMas;
    }

    public HtmlInputText getTxtCodSap() {
        return txtCodSap;
    }

    public void setTxtCodSap(HtmlInputText txtCodSap) {
        this.txtCodSap = txtCodSap;
    }

    public HtmlInputText getTxtCodVpe() {
        return txtCodVpe;
    }

    public void setTxtCodVpe(HtmlInputText txtCodVpe) {
        this.txtCodVpe = txtCodVpe;
    }

    public HtmlInputText getTxtNombre() {
        return txtNombre;
    }

    public void setTxtNombre(HtmlInputText txtNombre) {
        this.txtNombre = txtNombre;
    }

    public HtmlInputText getTxtIdGrue_TsccvGrupoEmpresa() {
        return txtIdGrue_TsccvGrupoEmpresa;
    }

    public void setTxtIdGrue_TsccvGrupoEmpresa(
        HtmlInputText txtIdGrue_TsccvGrupoEmpresa) {
        this.txtIdGrue_TsccvGrupoEmpresa = txtIdGrue_TsccvGrupoEmpresa;
    }

    public SelectInputDate getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(SelectInputDate txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public HtmlInputText getTxtIdEmpr() {
        return txtIdEmpr;
    }

    public void setTxtIdEmpr(HtmlInputText txtIdEmpr) {
        this.txtIdEmpr = txtIdEmpr;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnInactivar() {
		return btnInactivar;
	}

	public void setBtnInactivar(HtmlCommandButton btnInactivar) {
		this.btnInactivar = btnInactivar;
	}

	public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TsccvEmpresasDTO> getTsccvEmpresasDTO() {
        return tsccvEmpresasDTO;
    }

    public void setTsccvEmpresasDTO(List<TsccvEmpresasDTO> tsccvEmpresasDTO) {
        this.tsccvEmpresasDTO = tsccvEmpresasDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    
    public HtmlSelectOneMenu getSomActivo() {
		return somActivo;
	}

	public void setSomActivo(HtmlSelectOneMenu somActivo) {
		this.somActivo = somActivo;
	}

	public List<SelectItem> getSiActivo() {
		return siActivo;
	}

	public void setSiActivo(List<SelectItem> siActivo) {
		this.siActivo = siActivo;
	}

	public String[] getValorParametroActivo() {
		return valorParametroActivo;
	}

	public void setValorParametroActivo(String[] valorParametroActivo) {
		this.valorParametroActivo = valorParametroActivo;
	}
    
	public HtmlSelectOneMenu getSomEmpresa() {
		return somEmpresa;
	}

	public void setSomEmpresa(HtmlSelectOneMenu somEmpresa) {
		this.somEmpresa = somEmpresa;
	}

	public List<SelectItem> getSiEmpresa() {
		return siEmpresa;
	}

	public void setSiEmpresa(List<SelectItem> siEmpresa) {
		this.siEmpresa = siEmpresa;
	}
	
	public List<DetalleGrupoEmpresaVO> getlistadoDetalleEmpresaVO() {
		return listadoDetalleEmpresaVO;
	}

	public void setlistadoDetalleEmpresaVO(
			List<DetalleGrupoEmpresaVO> listadoDetalleEmpresaVO) {
		this.listadoDetalleEmpresaVO = listadoDetalleEmpresaVO;
	}
	
	public InputFile getArchivo() {
		return archivo;
	}

	public void setArchivo(InputFile archivo) {
		this.archivo = archivo;
	}
	
	public HtmlInputTextarea getTxtLogErrores() {
		return txtLogErrores;
	}
	
	public void setTxtLogErrores(HtmlInputTextarea txtLogErrores) {
		this.txtLogErrores = txtLogErrores;
	}
	
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvEmpresas> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvEmpresasDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
    
    //Metodo encargado de consultar el parametro para el campo activo
    public String[] getValorParametroAct(){    	
    	//ARCHIVO PORPIEDADES
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String temp[] = null;
        try {
        	temp = archivoPropiedades.getProperty("LISTA_ACTIVO").toString().split(",");
        	
        	if(temp != null){
        		valorParametroActivo = new String[temp.length+1];  
	        	for(int i=0; i<temp.length; i++){
	        		valorParametroActivo[i] = temp[i];
	        	}
        	}
			return valorParametroActivo;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
		return null;           
    }

  //SE AGREGA EL METODO ASOCIADO AL ENLACE EN LA TABLA
    public void find_txtId(ActionEvent event) {
    	Integer idEmpr = Integer.parseInt(FacesUtils.getRequestParameter("idEmpr").toString());
    	find(idEmpr);
    }
    
    
    public void find (Integer idEmpr){
    	TsccvEmpresas entity = null;
    	 
        try {
            entity = BusinessDelegatorView.getTsccvEmpresas(idEmpr);
        } catch (Exception e) {
            // TODO: handle exception
        }
        
        if (entity == null) {
            txtActivo.setDisabled(false);
            txtCodMas.setDisabled(false);
            txtCodSap.setDisabled(false);
            txtCodVpe.setDisabled(false);
            txtNombre.setDisabled(false);
            txtIdGrue_TsccvGrupoEmpresa.setDisabled(false);

            txtFechaCreacion.setDisabled(false);

            txtIdEmpr.setDisabled(false);

            btnSave.setDisabled(false);
            btnDelete.setDisabled(true);
            btnModify.setDisabled(true);
            btnInactivar.setDisabled(true);
            btnClear.setDisabled(false);
        } else {
            txtActivo.setValue(entity.getActivo());
            txtActivo.setDisabled(false);
            somActivo.setValue(entity.getActivo());
            txtCodMas.setValue(entity.getCodMas());
            txtCodMas.setDisabled(false);
            txtCodSap.setValue(entity.getCodSap());
            txtCodSap.setDisabled(false);
            txtCodVpe.setValue(entity.getCodVpe());
            txtCodVpe.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtNombre.setValue(entity.getNombre());
            txtNombre.setDisabled(false);
            txtIdGrue_TsccvGrupoEmpresa.setValue(entity.getTsccvGrupoEmpresa() != null ? entity.getTsccvGrupoEmpresa().getIdGrue() : null);                
            txtIdGrue_TsccvGrupoEmpresa.setDisabled(false);                

            txtIdEmpr.setValue(entity.getIdEmpr());
            txtIdEmpr.setDisabled(true);

            btnSave.setDisabled(true);
            btnDelete.setDisabled(false);
            btnModify.setDisabled(false);
            btnInactivar.setDisabled(false);
            btnClear.setDisabled(false);
            
            //Se invoca el metodo encargado de consultar los detalles si es un grupo
            consultarDetalleGrupoEmpr(entity.getTsccvGrupoEmpresa() != null ? entity.getTsccvGrupoEmpresa().getIdGrue() : null);
        }
    }
    
    //Metodo encargado de agregar las empresas hijas seleccionadas
    public void agregarEmpresa(ActionEvent event){
    	//Se valida si la empresa seleccionada es valida
    	try{
	    	if((somEmpresa.getValue() != null) && (new Integer(somEmpresa.getValue().toString()) != -1)){
	    		//Se valida si la empresa ya se agrego
	    		for(DetalleGrupoEmpresaVO empresaHija: listadoDetalleEmpresaVO){
	    			if(empresaHija.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr().equals(new Integer(somEmpresa.getValue().toString()))){
	    				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	    	          	mensaje.mostrarMensaje("Esta Empresa ya fue agregada", MBMensaje.MENSAJE_ALERTA);
	    	          	return;
	    			}
	    		}
	    		
	    		TsccvDetalleGrupoEmpresa detalleGrupEmpresa = new TsccvDetalleGrupoEmpresa();
	    		TsccvEmpresas empresaSeleccionada = new TsccvEmpresas();
	    		empresaSeleccionada = BusinessDelegatorView.getTsccvEmpresas(new Integer(somEmpresa.getValue().toString()));
	    		//se setea el valor "0" para diferenciar las empresas que estan en memoria de las que esta en la BD
	    		empresaSeleccionada.setActivo(new String("e"));
	    		detalleGrupEmpresa.setTsccvEmpresas(empresaSeleccionada);
	    		DetalleGrupoEmpresaVO detalleVO = new DetalleGrupoEmpresaVO();
	    		detalleVO.setTsccvDetalleGrupoEmpresa(detalleGrupEmpresa);
	    		
	    		listadoDetalleEmpresaVO.add(detalleVO);
	    	}else{
	    		//FacesContext.getCurrentInstance().addMessage("", new FacesMessage("Favor selecciona una empresa valida"));
	    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	          	mensaje.mostrarMensaje("Favor seleccione una Empresa valida", MBMensaje.MENSAJE_ALERTA);
	    	}
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
		
    	somEmpresa.setValue(new Long(-1));
    	somEmpresa.setDisabled(false);
    }
    
    //Metodo encargado de consultar las empresas pertenecientes al grupo seleccionado
    public void consultarDetalleGrupoEmpr(Integer idGruEpmr){
    	listadoDetalleEmpresaVO = new ArrayList<DetalleGrupoEmpresaVO>();
    	if(idGruEpmr != null){    		
    		try{
    			//Se consulta el detalle del grupo
    			List<TsccvDetalleGrupoEmpresa> listadoDetalleGrupoEmpresa = BusinessDelegatorView.findByPropertyTsccvDetalleGrupoEmpresa(TsccvDetalleGrupoEmpresaDAO.IDGRUE, idGruEpmr);
    			
    			//Se recorre el listado de detalles y se asigna cada una de las empresas a la tabla
    			for(TsccvDetalleGrupoEmpresa objetoDetalleGrupoEmpresa : listadoDetalleGrupoEmpresa){
    				DetalleGrupoEmpresaVO detalleVO = new DetalleGrupoEmpresaVO();
    	    		detalleVO.setSeleccionada(true);
    				detalleVO.setTsccvDetalleGrupoEmpresa(objetoDetalleGrupoEmpresa);
    	    		
    				listadoDetalleEmpresaVO.add(detalleVO);
    			}
    		}catch(Exception e){
    			//FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
    			//Mensaje mensaje = (Mensaje) FacesUtils.getManagedBean("mensaje");
              	//mensaje.mostrarMensaje(e.getMessage(), Mensaje.MENSAJE_ERROR);
    		}
    	}
    }
    
    
    //metodo que elimina las empresas de la lista en memoria o de la BD
    public void eliminarEmpresa(ActionEvent event){
    	try {    		
    		TsccvDetalleGrupoEmpresa detalleGrupEmp = (TsccvDetalleGrupoEmpresa) event.getComponent().getAttributes().get("detalleGrupEmp");
    		if(detalleGrupEmp.getTsccvEmpresas().getActivo().equals("e")){  
    			listadoDetalleEmpresaVO.remove(detalleGrupEmp);
    		}
    		else {
	    		if(txtIdEmpr.getValue() != null && !txtIdEmpr.getValue().equals("")) {
	    			BusinessDelegatorView.deleteTsccvDetalleGrupoEmpresa((((txtIdGrue_TsccvGrupoEmpresa.getValue()) == null) ||
	    	                (txtIdGrue_TsccvGrupoEmpresa.getValue()).equals("")) ? null
	    	                                                   : new Integer(
	    	                txtIdGrue_TsccvGrupoEmpresa.getValue().toString()),
	    	                detalleGrupEmp.getTsccvEmpresas().getIdEmpr());
	    			
		            consultarDetalleGrupoEmpr(new Integer(txtIdGrue_TsccvGrupoEmpresa.getValue().toString()));
				}
				else {
					MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
		          	mensaje.mostrarMensaje("Debe seleccionar un Usuario", MBMensaje.MENSAJE_ALERTA);
	    		}
    		}    		
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    }
    
    //consulta por todos los campos
    public String consularEmpresas(){
    	List<TsccvEmpresas> registros = null;    	
    	
    	try {
        	//ANTES DE GUARDAR SE ASIGNA EL VALOR DE LA LISTA SELECCIONADO AL CAMPO DE TEXTO RESPECTIVO
    		txtActivo.setValue(new Long(somActivo.getValue().toString()) != -1 ? somActivo.getValue() : null);
        	
    		registros = BusinessDelegatorView.consularEmpresas((((txtActivo.getValue()) == null) ||
                    (txtActivo.getValue()).equals("")) ? null
                                                       : new String(
                        txtActivo.getValue().toString()),
                    (((txtCodMas.getValue()) == null) ||
                    (txtCodMas.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodMas.getValue().toString()),
                    (((txtCodSap.getValue()) == null) ||
                    (txtCodSap.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodSap.getValue().toString()),
                    (((txtCodVpe.getValue()) == null) ||
                    (txtCodVpe.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodVpe.getValue().toString()),
                    (((txtFechaCreacion.getValue()) == null) ||
                    (txtFechaCreacion.getValue()).equals("")) ? null
                                                              : (Date) txtFechaCreacion.getValue(),
                    (((txtIdEmpr.getValue()) == null) ||
                    (txtIdEmpr.getValue()).equals("")) ? null
                                                       : new Integer(
                        txtIdEmpr.getValue().toString()),
                    (((txtNombre.getValue()) == null) ||
                    (txtNombre.getValue()).equals("")) ? null
                                                       : new String(
                        txtNombre.getValue().toString()).toUpperCase(),
                    (((txtIdGrue_TsccvGrupoEmpresa.getValue()) == null) ||
                    (txtIdGrue_TsccvGrupoEmpresa.getValue()).equals("")) ? null
                                                                         : new Integer(
                        txtIdGrue_TsccvGrupoEmpresa.getValue().toString()));
            
            if(registros == null || registros.size() == 0){            	
            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("No se encontraron registros", MBMensaje.MENSAJE_ALERTA);
            	
              	onePageDataModel.setDirtyData(false);
              	flag = false;
              	onePageDataModel.setPage(new DataPage<TsccvEmpresas>(0, 0, registros));
              	return"";
            }
            
            onePageDataModel.setDirtyData(false);
            flag = true;
            onePageDataModel.setPage(new DataPage<TsccvEmpresas>(registros.size(), 0, registros));
            
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}
    	
    	return "";
    }
    
    public String inactivarEmpresa(){
    	try {
			TsccvEmpresas tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(new Integer(txtIdEmpr.getValue().toString()));
			
			if((tsccvEmpresas != null)){
				if(!tsccvEmpresas.getActivo().equals("1")){
					BusinessDelegatorView.inactivarEmpresa(tsccvEmpresas.getIdEmpr());
				}
				else{
					MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	              	mensaje.mostrarMensaje("La Empresa ya esta inactiva", MBMensaje.MENSAJE_ALERTA);
	              	return "";
				}
			}
			
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje("La Empresa fue inactivada de forma satisfactoria", MBMensaje.MENSAJE_OK);
						
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
		
		action_clear();
		
    	return "";
    }
	
    //metodo encargado de refrescar las lista de empresas
    public void refrescarListaEmpresas(){
    	try{
	    	//Se crea un objeto del bundle		
	        ArchivoBundle archivoBundle = new ArchivoBundle();
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
	    	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
	    	siNulo.setValue(new Long(-1));
	        
	        List<TsccvEmpresas> listadoEmpresas = null;			
			listadoEmpresas = BusinessDelegatorView.getTsccvEmpresas();
			siEmpresa = new ArrayList<SelectItem>();
	
			//lista de empresas
	    	siEmpresa.add(siNulo);
	    	for(TsccvEmpresas objetoEmpresa: listadoEmpresas){
	    		if(objetoEmpresa.getActivo().equals("1") || objetoEmpresa.getTsccvGrupoEmpresa() != null){
	    			continue;
	    		}
	    		SelectItem siEmpr = new SelectItem();        		
	    		siEmpr.setLabel(objetoEmpresa.getNombre());
	    		siEmpr.setValue(objetoEmpresa.getIdEmpr());
	    		
	    		siEmpresa.add(siEmpr);
	    	}
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
    }
    
    
    
    //metodo encargado mostrar el panel para la busqueda y seleccion de la empresa
	public void armarGrupoEmpresas(){				
		//se obtiene el managebean
		ArmarGrupoEmpresasView armarGrupoEmpresasView = (ArmarGrupoEmpresasView) FacesUtils.getManagedBean("armarGrupoEmpresasView");
				
		armarGrupoEmpresasView.setListadoDetGrupEmpresasVOSeleccionadas(listadoDetalleEmpresaVO);
		armarGrupoEmpresasView.desmarcarTodas();
		armarGrupoEmpresasView.marcarSeleccionadas();
		//se setea el managebean que realiza el llamado this
		armarGrupoEmpresasView.setManageBeanLlmado(this);
		armarGrupoEmpresasView.mostrarPanel();
	}
	
	
	//metodo encargado de cargar las empresas
	public void uploadFile(ActionEvent event) {
		try{
			InputFile inputFile = (InputFile) event.getSource();
	        FileInfo fileInfo = inputFile.getFileInfo();
			if (fileInfo.getStatus() == FileInfo.SAVED) {
				//se valida el tipo de archivo enviado
				
				//application/vnd.openxmlformats-officedocument.spreadsheetml.sheet (office 2007)
				//application/vnd.ms-excel (office 2003)
	            if(!inputFile.getFileInfo().getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") &&
	            		!inputFile.getFileInfo().getContentType().equals("application/vnd.ms-excel")){
	            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
					mensaje.mostrarMensaje("Solo es posible cargar archivos formato xls", MBMensaje.MENSAJE_ALERTA);
					archivo.reset();
					
					return;
	            }
	            archivo = inputFile;
	        }
		}catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
	}
	
	public String cargarEmpresas(){
		try{
			if(archivo.getFileInfo().getFile() != null){
				CargarEmpresas cargarEmpresas = new CargarEmpresas();
	            String logErrores = cargarEmpresas.cargar(archivo.getFileInfo().getFile());
	            
	            consularEmpresas();//se llama el metodo consularEmpresas para refrescar la tabla
	            if(logErrores == null || logErrores.equals("")){
	            	txtLogErrores.setRendered(false);
		            txtLogErrores.setValue(null);
		            
		            MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
		          	mensaje.mostrarMensaje("Las Empresas han sido cargadas de forma exitosa", MBMensaje.MENSAJE_OK);
	            }
	            else{
	            	txtLogErrores.setRendered(true);
		            txtLogErrores.setValue("Inconsistencias producidas durante el cargue:\n\n"+logErrores);
		            
	            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
		          	mensaje.mostrarMensaje("Es posible que no se hayan cargado las Empresas correctamanete debido a inconsistencias en los datos", MBMensaje.MENSAJE_ALERTA);
	            }
	          	
	          	archivo.getFileInfo().setFile(null);
	        }
			else{
				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensaje("Debe seleccionar y enviar un archivo", MBMensaje.MENSAJE_ERROR);
			}
		}catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
		}
		
		return "";
	}

}
