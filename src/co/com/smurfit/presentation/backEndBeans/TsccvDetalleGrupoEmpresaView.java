package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TsccvDetalleGrupoEmpresaDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvDetalleGrupoEmpresaView extends DataSource
    implements Renderable, DisposableBean {
    private HtmlInputText txtIdEmpr_TsccvEmpresas;
    private HtmlInputText txtIdGrue_TsccvGrupoEmpresa;
    private HtmlInputText txtIdGrue;
    private HtmlInputText txtIdEmpr;
    private SelectInputDate txtFechaCreacion;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TsccvDetalleGrupoEmpresa> tsccvDetalleGrupoEmpresa;
    private List<TsccvDetalleGrupoEmpresaDTO> tsccvDetalleGrupoEmpresaDTO;

    public TsccvDetalleGrupoEmpresaView() {
        super("");
    }

    public String action_clear() {
        txtIdEmpr_TsccvEmpresas.setValue(null);
        txtIdEmpr_TsccvEmpresas.setDisabled(true);
        txtIdGrue_TsccvGrupoEmpresa.setValue(null);
        txtIdGrue_TsccvGrupoEmpresa.setDisabled(true);

        txtFechaCreacion.setValue(null);
        txtFechaCreacion.setDisabled(true);

        txtIdGrue.setValue(null);
        txtIdGrue.setDisabled(false);
        txtIdEmpr.setValue(null);
        txtIdEmpr.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TsccvDetalleGrupoEmpresa entity = null;

            try {
                TsccvDetalleGrupoEmpresaId id = new TsccvDetalleGrupoEmpresaId();
                id.setIdGrue((((txtIdGrue.getValue()) == null) ||
                    (txtIdGrue.getValue()).equals("")) ? null
                                                       : new Integer(
                        txtIdGrue.getValue().toString()));
                id.setIdEmpr((((txtIdEmpr.getValue()) == null) ||
                    (txtIdEmpr.getValue()).equals("")) ? null
                                                       : new Integer(
                        txtIdEmpr.getValue().toString()));

                entity = BusinessDelegatorView.getTsccvDetalleGrupoEmpresa(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtIdEmpr_TsccvEmpresas.setDisabled(false);
                txtIdGrue_TsccvGrupoEmpresa.setDisabled(false);

                txtFechaCreacion.setDisabled(false);

                txtIdGrue.setDisabled(false);
                txtIdEmpr.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtFechaCreacion.setValue(entity.getFechaCreacion());
                txtFechaCreacion.setDisabled(false);
                txtIdEmpr_TsccvEmpresas.setValue(entity.getTsccvEmpresas()
                                                       .getIdEmpr());
                txtIdEmpr_TsccvEmpresas.setDisabled(false);
                txtIdGrue_TsccvGrupoEmpresa.setValue(entity.getTsccvGrupoEmpresa()
                                                           .getIdGrue());
                txtIdGrue_TsccvGrupoEmpresa.setDisabled(false);

                txtIdGrue.setValue(entity.getId().getIdGrue());
                txtIdGrue.setDisabled(true);
                txtIdEmpr.setValue(entity.getId().getIdEmpr());
                txtIdEmpr.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveTsccvDetalleGrupoEmpresa((((txtIdGrue.getValue()) == null) ||
                (txtIdGrue.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdGrue.getValue().toString()),
                (((txtIdEmpr.getValue()) == null) ||
                (txtIdEmpr.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdEmpr.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdEmpr_TsccvEmpresas.getValue()) == null) ||
                (txtIdEmpr_TsccvEmpresas.getValue()).equals("")) ? null
                                                                 : new Integer(
                    txtIdEmpr_TsccvEmpresas.getValue().toString()),
                (((txtIdGrue_TsccvGrupoEmpresa.getValue()) == null) ||
                (txtIdGrue_TsccvGrupoEmpresa.getValue()).equals("")) ? null
                                                                     : new Integer(
                    txtIdGrue_TsccvGrupoEmpresa.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTsccvDetalleGrupoEmpresa((((txtIdGrue.getValue()) == null) ||
                (txtIdGrue.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdGrue.getValue().toString()),
                (((txtIdEmpr.getValue()) == null) ||
                (txtIdEmpr.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdEmpr.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateTsccvDetalleGrupoEmpresa((((txtIdGrue.getValue()) == null) ||
                (txtIdGrue.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdGrue.getValue().toString()),
                (((txtIdEmpr.getValue()) == null) ||
                (txtIdEmpr.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdEmpr.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtIdEmpr_TsccvEmpresas.getValue()) == null) ||
                (txtIdEmpr_TsccvEmpresas.getValue()).equals("")) ? null
                                                                 : new Integer(
                    txtIdEmpr_TsccvEmpresas.getValue().toString()),
                (((txtIdGrue_TsccvGrupoEmpresa.getValue()) == null) ||
                (txtIdGrue_TsccvGrupoEmpresa.getValue()).equals("")) ? null
                                                                     : new Integer(
                    txtIdGrue_TsccvGrupoEmpresa.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Integer idGrue, Integer idEmpr,
        Date fechaCreacion, Integer idEmpr_TsccvEmpresas,
        Integer idGrue_TsccvGrupoEmpresa) throws Exception {
        try {
            BusinessDelegatorView.updateTsccvDetalleGrupoEmpresa(idGrue,
                idEmpr, fechaCreacion, idEmpr_TsccvEmpresas,
                idGrue_TsccvGrupoEmpresa);
            renderManager.getOnDemandRenderer("TsccvDetalleGrupoEmpresaView")
                         .requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TsccvDetalleGrupoEmpresaView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TsccvDetalleGrupoEmpresa> getTsccvDetalleGrupoEmpresa() {
        if (flag) {
            try {
                tsccvDetalleGrupoEmpresa = BusinessDelegatorView.getTsccvDetalleGrupoEmpresa();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tsccvDetalleGrupoEmpresa;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTsccvDetalleGrupoEmpresa(
        List<TsccvDetalleGrupoEmpresa> tsccvDetalleGrupoEmpresa) {
        this.tsccvDetalleGrupoEmpresa = tsccvDetalleGrupoEmpresa;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTsccvDetalleGrupoEmpresa() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvDetalleGrupoEmpresa> getDataPage(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvDetalleGrupoEmpresa = 0;

        try {
            totalNumberTsccvDetalleGrupoEmpresa = BusinessDelegatorView.findTotalNumberTsccvDetalleGrupoEmpresa()
                                                                       .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvDetalleGrupoEmpresa) {
            endIndex = totalNumberTsccvDetalleGrupoEmpresa;
        }

        try {
            tsccvDetalleGrupoEmpresa = BusinessDelegatorView.findPageTsccvDetalleGrupoEmpresa(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvDetalleGrupoEmpresa>(totalNumberTsccvDetalleGrupoEmpresa,
            startRow, tsccvDetalleGrupoEmpresa);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvDetalleGrupoEmpresaDTO> getDataPageDTO(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvDetalleGrupoEmpresa = 0;

        try {
            totalNumberTsccvDetalleGrupoEmpresa = BusinessDelegatorView.findTotalNumberTsccvDetalleGrupoEmpresa()
                                                                       .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvDetalleGrupoEmpresa) {
            endIndex = totalNumberTsccvDetalleGrupoEmpresa;
        }

        try {
            tsccvDetalleGrupoEmpresa = BusinessDelegatorView.findPageTsccvDetalleGrupoEmpresa(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tsccvDetalleGrupoEmpresa)) {
                tsccvDetalleGrupoEmpresaDTO = new ArrayList<TsccvDetalleGrupoEmpresaDTO>();

                for (TsccvDetalleGrupoEmpresa tsccvDetalleGrupoEmpresaTmp : tsccvDetalleGrupoEmpresa) {
                    TsccvDetalleGrupoEmpresaDTO tsccvDetalleGrupoEmpresaDTO2 = new TsccvDetalleGrupoEmpresaDTO();
                    tsccvDetalleGrupoEmpresaDTO2.setIdGrue(tsccvDetalleGrupoEmpresaTmp.getId()
                                                                                      .getIdGrue()
                                                                                      .toString());
                    tsccvDetalleGrupoEmpresaDTO2.setIdEmpr(tsccvDetalleGrupoEmpresaTmp.getId()
                                                                                      .getIdEmpr()
                                                                                      .toString());

                    tsccvDetalleGrupoEmpresaDTO2.setFechaCreacion(tsccvDetalleGrupoEmpresaTmp.getFechaCreacion());
                    tsccvDetalleGrupoEmpresaDTO2.setIdEmpr_TsccvEmpresas((tsccvDetalleGrupoEmpresaTmp.getTsccvEmpresas()
                                                                                                     .getIdEmpr() != null)
                        ? tsccvDetalleGrupoEmpresaTmp.getTsccvEmpresas()
                                                     .getIdEmpr().toString()
                        : null);
                    tsccvDetalleGrupoEmpresaDTO2.setIdGrue_TsccvGrupoEmpresa((tsccvDetalleGrupoEmpresaTmp.getTsccvGrupoEmpresa()
                                                                                                         .getIdGrue() != null)
                        ? tsccvDetalleGrupoEmpresaTmp.getTsccvGrupoEmpresa()
                                                     .getIdGrue().toString()
                        : null);
                    tsccvDetalleGrupoEmpresaDTO2.setTsccvDetalleGrupoEmpresa(tsccvDetalleGrupoEmpresaTmp);
                    tsccvDetalleGrupoEmpresaDTO2.setTsccvDetalleGrupoEmpresaView(this);
                    tsccvDetalleGrupoEmpresaDTO.add(tsccvDetalleGrupoEmpresaDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvDetalleGrupoEmpresaDTO>(totalNumberTsccvDetalleGrupoEmpresa,
            startRow, tsccvDetalleGrupoEmpresaDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tsccvDetalleGrupoEmpresaDTO)) {
            for (TsccvDetalleGrupoEmpresaDTO tsccvDetalleGrupoEmpresaTmp : tsccvDetalleGrupoEmpresaDTO) {
                renderManager.getOnDemandRenderer(
                    "TsccvDetalleGrupoEmpresaView").remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tsccvDetalleGrupoEmpresaDTO)) {
            for (TsccvDetalleGrupoEmpresaDTO tsccvDetalleGrupoEmpresaTmp : tsccvDetalleGrupoEmpresaDTO) {
                renderManager.getOnDemandRenderer(
                    "TsccvDetalleGrupoEmpresaView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtIdEmpr_TsccvEmpresas() {
        return txtIdEmpr_TsccvEmpresas;
    }

    public void setTxtIdEmpr_TsccvEmpresas(
        HtmlInputText txtIdEmpr_TsccvEmpresas) {
        this.txtIdEmpr_TsccvEmpresas = txtIdEmpr_TsccvEmpresas;
    }

    public HtmlInputText getTxtIdGrue_TsccvGrupoEmpresa() {
        return txtIdGrue_TsccvGrupoEmpresa;
    }

    public void setTxtIdGrue_TsccvGrupoEmpresa(
        HtmlInputText txtIdGrue_TsccvGrupoEmpresa) {
        this.txtIdGrue_TsccvGrupoEmpresa = txtIdGrue_TsccvGrupoEmpresa;
    }

    public SelectInputDate getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(SelectInputDate txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public HtmlInputText getTxtIdGrue() {
        return txtIdGrue;
    }

    public void setTxtIdGrue(HtmlInputText txtIdGrue) {
        this.txtIdGrue = txtIdGrue;
    }

    public HtmlInputText getTxtIdEmpr() {
        return txtIdEmpr;
    }

    public void setTxtIdEmpr(HtmlInputText txtIdEmpr) {
        this.txtIdEmpr = txtIdEmpr;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TsccvDetalleGrupoEmpresaDTO> getTsccvDetalleGrupoEmpresaDTO() {
        return tsccvDetalleGrupoEmpresaDTO;
    }

    public void setTsccvDetalleGrupoEmpresaDTO(
        List<TsccvDetalleGrupoEmpresaDTO> tsccvDetalleGrupoEmpresaDTO) {
        this.tsccvDetalleGrupoEmpresaDTO = tsccvDetalleGrupoEmpresaDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvDetalleGrupoEmpresa> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvDetalleGrupoEmpresaDTO> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
}
