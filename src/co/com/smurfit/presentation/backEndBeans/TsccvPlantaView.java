package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TsccvPlantaDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvPlantaView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtDespla;
    private HtmlInputText txtDirele;
    private HtmlInputText txtPlanta;
    private HtmlInputText txtIdApli_TsccvAplicaciones;
    private HtmlInputText txtIdTipr_TsccvTipoProducto;
    private HtmlInputText txtCodpla;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TsccvPlanta> tsccvPlanta;
    private List<TsccvPlantaDTO> tsccvPlantaDTO;
    
    //listas desplegables
    private HtmlSelectOneMenu somAplicacion;
    private List<SelectItem> siAplicacion;
    private HtmlSelectOneMenu somTipoProducto;
    private List<SelectItem> siTipoProducto;

    public TsccvPlantaView() {
        super("");
        
        //Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();
		
		try{
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
        	siAplicacion = new ArrayList<SelectItem>();
			siTipoProducto = new ArrayList<SelectItem>();
			
			//se crear los listados
			List<TsccvAplicaciones> listadoAplicaciones = new ArrayList<TsccvAplicaciones>();
			List<TsccvTipoProducto> listadoTiposProducto = new ArrayList<TsccvTipoProducto>();
			
			//se cargan los valores de las listas
			listadoAplicaciones = BusinessDelegatorView.getTsccvAplicaciones();
			listadoTiposProducto = BusinessDelegatorView.getTsccvTipoProducto();
            
			
        	//lista de grupos
        	siAplicacion.add(siNulo);
        	for(TsccvAplicaciones objetoApli: listadoAplicaciones){
        		if(objetoApli.getActivo().equals("1")){
        			continue;
        		}
        		
        		SelectItem siApli = new SelectItem();        		
        		siApli.setLabel(objetoApli.getDescripcion());
        		siApli.setValue(objetoApli.getIdApli());
        		
        		siAplicacion.add(siApli);
        	}
        	
        	//lista de empresa
        	siTipoProducto.add(siNulo);
        	for(TsccvTipoProducto objetoTipPro: listadoTiposProducto){
        		if(objetoTipPro.getActivo().equals("1")){
        			continue;
        		}
        		
        		SelectItem siTipPro = new SelectItem();        		
        		siTipPro.setLabel(objetoTipPro.getDescripcion());
        		siTipPro.setValue(objetoTipPro.getIdTipr());
        		
        		siTipoProducto.add(siTipPro);
        	}
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
    }

    public String action_clear() {
        txtDespla.setValue(null);
        txtDespla.setDisabled(false);
        txtDirele.setValue(null);
        txtDirele.setDisabled(false);
        txtPlanta.setValue(null);
        txtPlanta.setDisabled(false);
        txtIdApli_TsccvAplicaciones.setValue(null);
        txtIdApli_TsccvAplicaciones.setDisabled(false);
        somAplicacion.setValue(new Long(-1));
        
        txtIdTipr_TsccvTipoProducto.setValue(null);
        txtIdTipr_TsccvTipoProducto.setDisabled(false);
        somTipoProducto.setValue(new Long(-1));

        txtCodpla.setValue(null);
        txtCodpla.setDisabled(false);

        btnSave.setDisabled(false);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);
        
        onePageDataModel = null;
        flag = true;
        getData();

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TsccvPlanta entity = null;

            try {
                String codpla = new String(txtCodpla.getValue().toString());

                entity = BusinessDelegatorView.getTsccvPlanta(codpla);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtDespla.setDisabled(false);
                txtDirele.setDisabled(false);
                txtPlanta.setDisabled(false);
                txtIdApli_TsccvAplicaciones.setDisabled(false);
                txtIdTipr_TsccvTipoProducto.setDisabled(false);

                txtCodpla.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtDespla.setValue(entity.getDespla());
                txtDespla.setDisabled(false);
                txtDirele.setValue(entity.getDirele());
                txtDirele.setDisabled(false);
                txtPlanta.setValue(entity.getPlanta());
                txtPlanta.setDisabled(false);
                txtIdApli_TsccvAplicaciones.setValue(entity.getTsccvAplicaciones() != null ? entity.getTsccvAplicaciones().getIdApli() : null);
                txtIdApli_TsccvAplicaciones.setDisabled(false);
                somAplicacion.setValue(entity.getTsccvAplicaciones() != null ? entity.getTsccvAplicaciones().getIdApli() : null);
                
                txtIdTipr_TsccvTipoProducto.setValue(entity.getTsccvTipoProducto() != null ? entity.getTsccvTipoProducto().getIdTipr() : null);
                txtIdTipr_TsccvTipoProducto.setDisabled(false);
                somTipoProducto.setValue(entity.getTsccvTipoProducto() != null ? entity.getTsccvTipoProducto().getIdTipr() : null);

                txtCodpla.setValue(entity.getCodpla());
                txtCodpla.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
    	txtIdApli_TsccvAplicaciones.setValue(new Long(somAplicacion.getValue().toString()) != -1 ? somAplicacion.getValue() : null);
    	txtIdTipr_TsccvTipoProducto.setValue(new Long(somTipoProducto.getValue().toString()) != -1 ? somTipoProducto.getValue() : null);
    	
        try {
        	if(txtDespla.getValue() != null && !txtDespla.getValue().equals("")){
        		if(BusinessDelegatorView.plantaIsRegistrada(txtDespla.getValue().toString().toUpperCase())){
        			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
                  	mensaje.mostrarMensaje("La Planta ya se encuentra registrada", MBMensaje.MENSAJE_ALERTA);
                  	
                  	return "";
        		}
        	}
        	
            BusinessDelegatorView.saveTsccvPlanta((((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtDespla.getValue()) == null) ||
                (txtDespla.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespla.getValue().toString()).toUpperCase(),
                (((txtDirele.getValue()) == null) ||
                (txtDirele.getValue()).equals("")) ? null
                                                   : new String(
                    txtDirele.getValue().toString()),
                (((txtPlanta.getValue()) == null) ||
                (txtPlanta.getValue()).equals("")) ? null
                                                   : new String(
                    txtPlanta.getValue().toString()),
                (((txtIdApli_TsccvAplicaciones.getValue()) == null) ||
                (txtIdApli_TsccvAplicaciones.getValue()).equals("")) ? null
                                                                     : new Long(
                    txtIdApli_TsccvAplicaciones.getValue().toString()),
                (((txtIdTipr_TsccvTipoProducto.getValue()) == null) ||
                (txtIdTipr_TsccvTipoProducto.getValue()).equals("")) ? null
                                                                     : new Long(
                    txtIdTipr_TsccvTipoProducto.getValue().toString()));

            /*FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));*/
            MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
            mensaje.mostrarMensaje("La Planta fue adicionada de forma satisfactoria", MBMensaje.MENSAJE_OK);
        } catch(BMBaseException e){
        	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
        	mensaje.mostrarMensaje(e);
        	return "";
        	/*FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));*/
        }
        catch(Exception e){
        	BMBaseException be= new BMBaseException(e);
        	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
        	mensaje.mostrarMensaje(be);
        	return "";
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTsccvPlanta((((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()));

            /*FacesContext.getCurrentInstance()
            .addMessage("",
		    new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));*/
            MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
            mensaje.mostrarMensaje("La Planta fue eliminada de forma satisfactoria", MBMensaje.MENSAJE_OK);
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
		            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			return "";
		}

        action_clear();

        return "";
    }

    public String action_modify() {
    	txtIdApli_TsccvAplicaciones.setValue(new Long(somAplicacion.getValue().toString()) != -1 ? somAplicacion.getValue() : null);
    	txtIdTipr_TsccvTipoProducto.setValue(new Long(somTipoProducto.getValue().toString()) != -1 ? somTipoProducto.getValue() : null);
    	
        try {
            BusinessDelegatorView.updateTsccvPlanta((((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtDespla.getValue()) == null) ||
                (txtDespla.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespla.getValue().toString()).toUpperCase(),
                (((txtDirele.getValue()) == null) ||
                (txtDirele.getValue()).equals("")) ? null
                                                   : new String(
                    txtDirele.getValue().toString()),
                (((txtPlanta.getValue()) == null) ||
                (txtPlanta.getValue()).equals("")) ? null
                                                   : new String(
                    txtPlanta.getValue().toString()),
                (((txtIdApli_TsccvAplicaciones.getValue()) == null) ||
                (txtIdApli_TsccvAplicaciones.getValue()).equals("")) ? null
                                                                     : new Long(
                    txtIdApli_TsccvAplicaciones.getValue().toString()),
                (((txtIdTipr_TsccvTipoProducto.getValue()) == null) ||
                (txtIdTipr_TsccvTipoProducto.getValue()).equals("")) ? null
                                                                     : new Long(
                    txtIdTipr_TsccvTipoProducto.getValue().toString()));

            /*FacesContext.getCurrentInstance()
            .addMessage("",
		    new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));*/
            MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
            mensaje.mostrarMensaje("La Planta fue modificada de forma satisfactoria", MBMensaje.MENSAJE_OK);
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
		            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			return "";
		}

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String codpla, String despla,
        String direle, String planta, Long idApli_TsccvAplicaciones,
        Long idTipr_TsccvTipoProducto) throws Exception {
        try {
            BusinessDelegatorView.updateTsccvPlanta(codpla, despla, direle,
                planta, idApli_TsccvAplicaciones, idTipr_TsccvTipoProducto);
            renderManager.getOnDemandRenderer("TsccvPlantaView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TsccvPlantaView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TsccvPlanta> getTsccvPlanta() {
        if (flag) {
            try {
                tsccvPlanta = BusinessDelegatorView.getTsccvPlanta();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tsccvPlanta;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTsccvPlanta(List<TsccvPlanta> tsccvPlanta) {
        this.tsccvPlanta = tsccvPlanta;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTsccvPlanta() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvPlanta> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvPlanta = 0;

        try {
            totalNumberTsccvPlanta = BusinessDelegatorView.findTotalNumberTsccvPlanta()
                                                          .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvPlanta) {
            endIndex = totalNumberTsccvPlanta;
        }

        try {
            tsccvPlanta = BusinessDelegatorView.findPageTsccvPlanta(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvPlanta>(totalNumberTsccvPlanta, startRow,
            tsccvPlanta);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvPlantaDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvPlanta = 0;

        try {
            totalNumberTsccvPlanta = BusinessDelegatorView.findTotalNumberTsccvPlanta()
                                                          .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvPlanta) {
            endIndex = totalNumberTsccvPlanta;
        }

        try {
            tsccvPlanta = BusinessDelegatorView.findPageTsccvPlanta(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tsccvPlanta)) {
                tsccvPlantaDTO = new ArrayList<TsccvPlantaDTO>();

                for (TsccvPlanta tsccvPlantaTmp : tsccvPlanta) {
                    TsccvPlantaDTO tsccvPlantaDTO2 = new TsccvPlantaDTO();
                    tsccvPlantaDTO2.setCodpla(tsccvPlantaTmp.getCodpla()
                                                            .toString());

                    tsccvPlantaDTO2.setDespla((tsccvPlantaTmp.getDespla() != null)
                        ? tsccvPlantaTmp.getDespla().toString() : null);
                    tsccvPlantaDTO2.setDirele((tsccvPlantaTmp.getDirele() != null)
                        ? tsccvPlantaTmp.getDirele().toString() : null);
                    tsccvPlantaDTO2.setPlanta((tsccvPlantaTmp.getPlanta() != null)
                        ? tsccvPlantaTmp.getPlanta().toString() : null);
                    tsccvPlantaDTO2.setIdApli_TsccvAplicaciones((tsccvPlantaTmp.getTsccvAplicaciones()
                                                                               .getIdApli() != null)
                        ? tsccvPlantaTmp.getTsccvAplicaciones().getIdApli()
                                        .toString() : null);
                    tsccvPlantaDTO2.setIdTipr_TsccvTipoProducto((tsccvPlantaTmp.getTsccvTipoProducto()
                                                                               .getIdTipr() != null)
                        ? tsccvPlantaTmp.getTsccvTipoProducto().getIdTipr()
                                        .toString() : null);
                    tsccvPlantaDTO2.setTsccvPlanta(tsccvPlantaTmp);
                    tsccvPlantaDTO2.setTsccvPlantaView(this);
                    tsccvPlantaDTO.add(tsccvPlantaDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvPlantaDTO>(totalNumberTsccvPlanta, startRow,
            tsccvPlantaDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tsccvPlantaDTO)) {
            for (TsccvPlantaDTO tsccvPlantaTmp : tsccvPlantaDTO) {
                renderManager.getOnDemandRenderer("TsccvPlantaView").remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tsccvPlantaDTO)) {
            for (TsccvPlantaDTO tsccvPlantaTmp : tsccvPlantaDTO) {
                renderManager.getOnDemandRenderer("TsccvPlantaView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtDespla() {
        return txtDespla;
    }

    public void setTxtDespla(HtmlInputText txtDespla) {
        this.txtDespla = txtDespla;
    }

    public HtmlInputText getTxtDirele() {
        return txtDirele;
    }

    public void setTxtDirele(HtmlInputText txtDirele) {
        this.txtDirele = txtDirele;
    }

    public HtmlInputText getTxtPlanta() {
        return txtPlanta;
    }

    public void setTxtPlanta(HtmlInputText txtPlanta) {
        this.txtPlanta = txtPlanta;
    }

    public HtmlInputText getTxtIdApli_TsccvAplicaciones() {
        return txtIdApli_TsccvAplicaciones;
    }

    public void setTxtIdApli_TsccvAplicaciones(
        HtmlInputText txtIdApli_TsccvAplicaciones) {
        this.txtIdApli_TsccvAplicaciones = txtIdApli_TsccvAplicaciones;
    }

    public HtmlInputText getTxtIdTipr_TsccvTipoProducto() {
        return txtIdTipr_TsccvTipoProducto;
    }

    public void setTxtIdTipr_TsccvTipoProducto(
        HtmlInputText txtIdTipr_TsccvTipoProducto) {
        this.txtIdTipr_TsccvTipoProducto = txtIdTipr_TsccvTipoProducto;
    }

    public HtmlInputText getTxtCodpla() {
        return txtCodpla;
    }

    public void setTxtCodpla(HtmlInputText txtCodpla) {
        this.txtCodpla = txtCodpla;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

	public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TsccvPlantaDTO> getTsccvPlantaDTO() {
        return tsccvPlantaDTO;
    }

    public void setTsccvPlantaDTO(List<TsccvPlantaDTO> tsccvPlantaDTO) {
        this.tsccvPlantaDTO = tsccvPlantaDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    
    public HtmlSelectOneMenu getSomAplicacion() {
		return somAplicacion;
	}

	public void setSomAplicacion(HtmlSelectOneMenu somAplicacion) {
		this.somAplicacion = somAplicacion;
	}

	public List<SelectItem> getSiAplicacion() {
		return siAplicacion;
	}

	public void setSiAplicacion(List<SelectItem> siAplicacion) {
		this.siAplicacion = siAplicacion;
	}

	public HtmlSelectOneMenu getSomTipoProducto() {
		return somTipoProducto;
	}

	public void setSomTipoProducto(HtmlSelectOneMenu somTipoProducto) {
		this.somTipoProducto = somTipoProducto;
	}

	public List<SelectItem> getSiTipoProducto() {
		return siTipoProducto;
	}

	public void setSiTipoProducto(List<SelectItem> siTipoProducto) {
		this.siTipoProducto = siTipoProducto;
	}
    
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvPlanta> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvPlantaDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }

    
    //SE AGREGA EL METODO ASOCIADO AL ENLACE EN LA TABLA 
    public void find_txtId(ActionEvent event) {
    	String codpla = FacesUtils.getRequestParameter("txtCodpla").toString();
    	find(codpla);
    }
    
    //METODO DE BUSQUEDA.
    public void find (String codpla){
    	TsccvPlanta entity = null;
    	
        try {
            entity = BusinessDelegatorView.getTsccvPlanta(codpla);
        } catch (Exception e) {
            // TODO: handle exception
        }
        
        if (entity == null) {
            txtDespla.setDisabled(false);
            txtDirele.setDisabled(false);
            txtPlanta.setDisabled(false);
            txtIdApli_TsccvAplicaciones.setDisabled(false);
            txtIdTipr_TsccvTipoProducto.setDisabled(false);

            txtCodpla.setDisabled(false);

            btnSave.setDisabled(false);
            btnDelete.setDisabled(true);
            btnModify.setDisabled(true);
            btnClear.setDisabled(false);
        } else {
            txtDespla.setValue(entity.getDespla());
            txtDespla.setDisabled(false);
            txtDirele.setValue(entity.getDirele());
            txtDirele.setDisabled(false);
            txtPlanta.setValue(entity.getPlanta());
            txtPlanta.setDisabled(false);
            txtIdApli_TsccvAplicaciones.setValue(entity.getTsccvAplicaciones() != null ? entity.getTsccvAplicaciones().getIdApli() : null);
            txtIdApli_TsccvAplicaciones.setDisabled(false);
            somAplicacion.setValue(entity.getTsccvAplicaciones() != null ? entity.getTsccvAplicaciones().getIdApli() : null);
            
            txtIdTipr_TsccvTipoProducto.setValue(entity.getTsccvTipoProducto() != null ? entity.getTsccvTipoProducto().getIdTipr() : null);
            txtIdTipr_TsccvTipoProducto.setDisabled(false);
            somTipoProducto.setValue(entity.getTsccvTipoProducto() != null ? entity.getTsccvTipoProducto().getIdTipr() : null);

            txtCodpla.setValue(entity.getCodpla());
            txtCodpla.setDisabled(true);

            btnSave.setDisabled(true);
            btnDelete.setDisabled(false);
            btnModify.setDisabled(false);
            btnClear.setDisabled(false);
        }
    }    
    
    //consulta los usuarios por tolos los campos
    public String consultarPlantas(){
    	List<TsccvPlanta> registros=null;    	
    	
    	try {
        	//ANTES DE GUARDAR SE ASIGNA EL VALOR DE LA LISTA SELECCIONADO AL CAMPO DE TEXTO RESPECTIVO
    		txtIdApli_TsccvAplicaciones.setValue(new Long(somAplicacion.getValue().toString()) != -1 ? somAplicacion.getValue() : null);
        	txtIdTipr_TsccvTipoProducto.setValue(new Long(somTipoProducto.getValue().toString()) != -1 ? somTipoProducto.getValue() : null);        	        
        	
            registros = BusinessDelegatorView.consultarPlantas((((txtCodpla.getValue()) == null) ||
                    (txtCodpla.getValue()).equals("")) ? null
					                            : new String(
					txtCodpla.getValue().toString()),
					(((txtDespla.getValue()) == null) ||
					(txtDespla.getValue()).equals("")) ? null
					                            : new String(
					txtDespla.getValue().toString()).toUpperCase(),
					(((txtDirele.getValue()) == null) ||
					(txtDirele.getValue()).equals("")) ? null
					                            : new String(
					txtDirele.getValue().toString()),
					(((txtPlanta.getValue()) == null) ||
					(txtPlanta.getValue()).equals("")) ? null
					                            : new String(
					txtPlanta.getValue().toString()),
					(((txtIdApli_TsccvAplicaciones.getValue()) == null) ||
					(txtIdApli_TsccvAplicaciones.getValue()).equals("")) ? null
					                                              : new Long(
					txtIdApli_TsccvAplicaciones.getValue().toString()),
					(((txtIdTipr_TsccvTipoProducto.getValue()) == null) ||
					(txtIdTipr_TsccvTipoProducto.getValue()).equals("")) ? null
					                                              : new Long(
					txtIdTipr_TsccvTipoProducto.getValue().toString()));
            
            if(registros == null || registros.size() == 0){            	
            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("No se encontraron registros", MBMensaje.MENSAJE_ALERTA);
            	
              	onePageDataModel.setDirtyData(false);
              	flag = false;
              	onePageDataModel.setPage(new DataPage<TsccvPlanta>(0, 0, registros));
              	return"";
            }
            
            onePageDataModel.setDirtyData(false);
            flag = true;
            onePageDataModel.setPage(new DataPage<TsccvPlanta>(registros.size(), 0, registros));
            
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}
    	
    	return "";
    }
	
}
