package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TsccvEntregasPedidoDAO;
import co.com.smurfit.dataaccess.dao.TsccvProductosPedidoDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.ConstruirEmailConfirmacionOrden;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.EnviarEmail;
import co.com.smurfit.utilities.FacesUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class ConfirmacionDeOrdenView {
	private TsccvPedidos pedido;
    private List<ProductosPedidoVO> listadoProductosPedido;
    private List<TsccvEntregasPedido> listadoEntregasTotal;//contiene todas las entregas de todos los productosPedido del pedido
    private Date fechaDocumento;
    private int cols = 80;
    private int rows = 3;
    private Double valorTotalSolicitud;
    private String moneda;
    
    private boolean renderDataTableVpe;
	private boolean renderDataTableMas;

    public ConfirmacionDeOrdenView() {        		
		try{
			
			//se crear los listados
			listadoProductosPedido = new ArrayList<ProductosPedidoVO>();	
			listadoEntregasTotal = new ArrayList<TsccvEntregasPedido>(0);
			
			//lista de productos
        	HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        	//se obtiene el pedido de la sesion
        	pedido = (TsccvPedidos)sesion.getAttribute("pedido");
        	//se obtiene el la lista de productos pedidos de la sesion
        	//listadoProductosPedido = (List<ProductosPedidoVO>)sesion.getAttribute("listaProductosRegistrados");
        	
        	//se consultan los productos del pedido
        	listadoProductosPedido = BusinessDelegatorView.consultarProductosPedido(pedido.getIdPedi());
        	
        	//se llama el metodo que calcula el valor de la solicitud
        	calcularValorSolicitud();
		} catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}    
    }

	public List<ProductosPedidoVO> getListadoProductosPedido() {
		return listadoProductosPedido;
	}

	public void setListadoProductosPedido(
			List<ProductosPedidoVO> listadoProductosPedido) {
		this.listadoProductosPedido = listadoProductosPedido;
	}

	public List<TsccvEntregasPedido> getListadoEntregasTotal() {
		return listadoEntregasTotal;
	}

	public void setListadoEntregasTotal(
			List<TsccvEntregasPedido> listadoEntregasTotal) {
		this.listadoEntregasTotal = listadoEntregasTotal;
	}

	public Date getFechaDocumento() {
		fechaDocumento = new Date();
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public TsccvPedidos getPedido() {
		return pedido;
	}

	public void setPedido(TsccvPedidos pedido) {
		this.pedido = pedido;
	}
	
	public int getCols() {
		return cols;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
    
    public Double getValorTotalSolicitud() {
		return valorTotalSolicitud;
	}

	public void setValorTotalSolicitud(Double valorTotalSolicitud) {
		this.valorTotalSolicitud = valorTotalSolicitud;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public boolean isRenderDataTableVpe() {
		return renderDataTableVpe;
	}

	public void setRenderDataTableVpe(boolean renderDataTableVpe) {
		this.renderDataTableVpe = renderDataTableVpe;
	}

	public boolean isRenderDataTableMas() {
		return renderDataTableMas;
	}

	public void setRenderDataTableMas(boolean renderDataTableMas) {
		this.renderDataTableMas = renderDataTableMas;
	}

	public TimeZone getTimeZone(){
    	return TimeZone.getDefault();
    }

	
	public void calcularValorSolicitud(){
		TsccvProductosPedido prodPed = null;
		valorTotalSolicitud = new Double(0);
		try{
			//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        
			//HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			//String codCliente = sesion.getAttribute("codCliente") != null ? sesion.getAttribute("codCliente").toString() : null;
			
			//si el tipo de producto de la planta del pedido es corrugado o sacos se consultan los precios con base en la cantidad total pedida para cada producto
			if(!pedido.getTsccvPlanta().getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
				//este metodo devuelve la lista de productos pedido con su respectivo precio de acuerdo a la cantidad total
				//SE COMENTA YA QUE SE PASO PARA LA PANTALLA ANTERIOR EN EL METODO ACEPTAR
				//listadoProductosPedido = BusinessDelegatorView.consultarPrecioPorCantidadTotal(listadoProductosPedido, codCliente);
				
				renderDataTableVpe = true;
				renderDataTableMas = false;
			}
			else{
				renderDataTableVpe = false;
				renderDataTableMas = true;
			}
			
			//se coloca la moneda del primer producto pedido
			moneda = listadoProductosPedido.get(0).getTsccvProductosPedido().getMoneda();
			
			String precio = null;
			for(ProductosPedidoVO prodPedVO : listadoProductosPedido){
				prodPed = prodPedVO.getTsccvProductosPedido();
				
				/*precio = prodPed.getPrecio().replace(",", "");//se eliminan las comas para evitar el error al comvertirlo a double
				
				valorTotalSolicitud += (Double)(prodPed.getCantidadTotal() * new Double(precio));
				*/
				//se adicionan las entregas de este producto pedido a la lista total de entregas 
				List<TsccvEntregasPedido> entregasTemp = BusinessDelegatorView.findByPropertyEntregasPedido(TsccvEntregasPedidoDAO.PRODPED, prodPedVO.getTsccvProductosPedido().getIdPrpe());
				
				for(TsccvEntregasPedido entrega : entregasTemp){
					precio = entrega.getPrecio().replace(",", "");//se eliminan las comas para evitar el error al comvertirlo a double
					
					valorTotalSolicitud += (Double)(entrega.getCantidad() * new Double(precio));
				}
				listadoEntregasTotal.addAll(entregasTemp);
			}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	//metodo encargado de modificar el pedido a registrado y guardar la observacion ingresada
	public String confirmarIngresoOrden(){
		try{
			//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        
			//NOTA: ahora se cambia el pedido a registrado (0)
			BusinessDelegatorView.updateTsccvPedidos(pedido.getActivo(), new Date(), pedido.getIdPedi(), 
												pedido.getTsccvEmpresas().getIdEmpr(), 
												pedido.getObservaciones().toUpperCase(), "0", 
												pedido.getTsccvPlanta().getCodpla(), pedido.getTsccvUsuario().getIdUsua(), null);	
	        
	        //se crea un registro en la tabla de estadisticas colocando como tipo accion pedido (2)
	        BusinessDelegatorView.saveTsccvEstadisticaAccPed(new Long(1), new Date(), null, new Long(1),
					pedido.getTsccvUsuario().getTbmBaseGruposUsuarios().getIdGrup(), pedido.getTsccvEmpresas().getIdEmpr(), 
					pedido.getTsccvUsuario().getTsccvPlanta().getCodpla(), new Long(2), pedido.getTsccvUsuario().getIdUsua());
	        
	        //se valida si la planta tiene direccion electronica
	        if(pedido.getTsccvPlanta().getDirele() != null && (pedido.getTsccvPlanta().getDirele() != null && !pedido.getTsccvPlanta().getDirele().equals(""))){
	        	ConstruirEmailConfirmacionOrden construitEmail = new ConstruirEmailConfirmacionOrden();
				construitEmail.setPedido(pedido);				
				
				//se genera el email de confirmacion segun la planta del productoPedido
				if(pedido.getTsccvPlanta().getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_MOLINOS")))){
					construitEmail.construirConfirmacionDeOrdenSap(listadoProductosPedido);
				}
				else{
					construitEmail.construirConfirmacionDeOrdenBvp(listadoProductosPedido);
				}
				
				EnviarEmail enviarEmail = new EnviarEmail();
				//Enviamos el email a los destinarios externos si los hay
				if(construitEmail.getDestinatarios().size() > 0){
					enviarEmail.enviar(pedido.getTsccvPlanta().getDirele(), construitEmail.getMensaje(), construitEmail.getDestinatarios());
				}
				//Enviamos el email a los destinarios internos si los hay
				if(construitEmail.getDestinatariosInternos().size() > 0){
					enviarEmail.enviar(pedido.getTsccvPlanta().getDirele(), construitEmail.getMensajeSinImagenes(), construitEmail.getDestinatariosInternos());
				}
	        }
	        else{//si el servicliente o planta no tiene email se muestra un mensaje de alerta indicando que no fue posible enviar el email
	        	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensajeAlerta("No se puede enviar el email de confirmación por que no se dispone del email de Serviclientes.");	
	        }
			
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
		}
    	catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			e.printStackTrace();
			return "";
		}
		
		return "goMensajeOrdenConfirmada";
	}
	
	
	public String corregirPedido(){
		try{
        	//se coloca la moneda del primer producto 
        	moneda = listadoProductosPedido.get(0).getTsccvProductosPedido().getMoneda();
			
			//NOTA: aun no se registra el pedido se deja el con registrado (1)
			BusinessDelegatorView.updateTsccvPedidos(pedido.getActivo(), new Date(), pedido.getIdPedi(), pedido.getTsccvEmpresas().getIdEmpr(),
												pedido.getObservaciones().toUpperCase(), "1", 
												pedido.getTsccvPlanta().getCodpla(), pedido.getTsccvUsuario().getIdUsua(), null);
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
		}
    	catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
    	
    	return "goPlanDeEntrega";
	}
}



