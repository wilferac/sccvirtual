package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.BvpDetDspDAO;
import co.com.smurfit.dataaccess.dao.CarteraResumenDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.dto.CarteraResumenDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlOutputText;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.servlet.http.HttpSession;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class CarteraResumenView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtCodcli;
    private HtmlInputText txtNumfac;
    private HtmlInputText txtClsdoc;
    private HtmlInputText txtFecfac;
    private HtmlInputText txtFecven;
    private HtmlInputText txtDias;
    private HtmlInputText txtMonto;
    private HtmlInputText txtPlanta;
    private HtmlInputText txtRango;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<CarteraResumen> carteraResumen;
    private List<CarteraResumenDTO> carteraResumenDTO;
    
    private List<CarteraResumen> listadoDetalleFactura;
    private HtmlOutputText msgDetalleFactura;

    public CarteraResumenView() {
        super("");
        
        //se instancia el objeto mediante el cual semostrara el titulo
        msgDetalleFactura = new HtmlOutputText();

        //se llama el etodo que realiza la consulta de detalle
        consultarDetalleFactura();
    }

    public String action_clear() {
        txtCodcli.setValue(null);
        txtCodcli.setDisabled(false);
        txtNumfac.setValue(null);
        txtNumfac.setDisabled(false);
        txtClsdoc.setValue(null);
        txtClsdoc.setDisabled(false);
        txtFecfac.setValue(null);
        txtFecfac.setDisabled(false);
        txtFecven.setValue(null);
        txtFecven.setDisabled(false);
        txtDias.setValue(null);
        txtDias.setDisabled(false);
        txtMonto.setValue(null);
        txtMonto.setDisabled(false);
        txtPlanta.setValue(null);
        txtPlanta.setDisabled(false);
        txtRango.setValue(null);
        txtRango.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            CarteraResumen entity = null;

            try {
                CarteraResumenId id = new CarteraResumenId();
                id.setCodcli((((txtCodcli.getValue()) == null) ||
                    (txtCodcli.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodcli.getValue().toString()));
                id.setNumfac((((txtNumfac.getValue()) == null) ||
                    (txtNumfac.getValue()).equals("")) ? null
                                                       : new String(
                        txtNumfac.getValue().toString()));
                id.setClsdoc((((txtClsdoc.getValue()) == null) ||
                    (txtClsdoc.getValue()).equals("")) ? null
                                                       : new String(
                        txtClsdoc.getValue().toString()));
                id.setFecfac((((txtFecfac.getValue()) == null) ||
                    (txtFecfac.getValue()).equals("")) ? null
                                                       : new String(
                        txtFecfac.getValue().toString()));
                id.setFecven((((txtFecven.getValue()) == null) ||
                    (txtFecven.getValue()).equals("")) ? null
                                                       : new String(
                        txtFecven.getValue().toString()));
                id.setDias((((txtDias.getValue()) == null) ||
                    (txtDias.getValue()).equals("")) ? null
                                                     : new String(
                        txtDias.getValue().toString()));
                id.setMonto((((txtMonto.getValue()) == null) ||
                    (txtMonto.getValue()).equals("")) ? null
                                                      : new String(
                        txtMonto.getValue().toString()));
                id.setPlanta((((txtPlanta.getValue()) == null) ||
                    (txtPlanta.getValue()).equals("")) ? null
                                                       : new String(
                        txtPlanta.getValue().toString()));
                id.setRango((((txtRango.getValue()) == null) ||
                    (txtRango.getValue()).equals("")) ? null
                                                      : new String(
                        txtRango.getValue().toString()));

                entity = BusinessDelegatorView.getCarteraResumen(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtCodcli.setDisabled(false);
                txtNumfac.setDisabled(false);
                txtClsdoc.setDisabled(false);
                txtFecfac.setDisabled(false);
                txtFecven.setDisabled(false);
                txtDias.setDisabled(false);
                txtMonto.setDisabled(false);
                txtPlanta.setDisabled(false);
                txtRango.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtCodcli.setValue(entity.getId().getCodcli());
                txtCodcli.setDisabled(true);
                txtNumfac.setValue(entity.getId().getNumfac());
                txtNumfac.setDisabled(true);
                txtClsdoc.setValue(entity.getId().getClsdoc());
                txtClsdoc.setDisabled(true);
                txtFecfac.setValue(entity.getId().getFecfac());
                txtFecfac.setDisabled(true);
                txtFecven.setValue(entity.getId().getFecven());
                txtFecven.setDisabled(true);
                txtDias.setValue(entity.getId().getDias());
                txtDias.setDisabled(true);
                txtMonto.setValue(entity.getId().getMonto());
                txtMonto.setDisabled(true);
                txtPlanta.setValue(entity.getId().getPlanta());
                txtPlanta.setDisabled(true);
                txtRango.setValue(entity.getId().getRango());
                txtRango.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveCarteraResumen((((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtNumfac.getValue()) == null) ||
                (txtNumfac.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumfac.getValue().toString()),
                (((txtClsdoc.getValue()) == null) ||
                (txtClsdoc.getValue()).equals("")) ? null
                                                   : new String(
                    txtClsdoc.getValue().toString()),
                (((txtFecfac.getValue()) == null) ||
                (txtFecfac.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecfac.getValue().toString()),
                (((txtFecven.getValue()) == null) ||
                (txtFecven.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecven.getValue().toString()),
                (((txtDias.getValue()) == null) ||
                (txtDias.getValue()).equals("")) ? null
                                                 : new String(
                    txtDias.getValue().toString()),
                (((txtMonto.getValue()) == null) ||
                (txtMonto.getValue()).equals("")) ? null
                                                  : new String(
                    txtMonto.getValue().toString()),
                (((txtPlanta.getValue()) == null) ||
                (txtPlanta.getValue()).equals("")) ? null
                                                   : new String(
                    txtPlanta.getValue().toString()),
                (((txtRango.getValue()) == null) ||
                (txtRango.getValue()).equals("")) ? null
                                                  : new String(
                    txtRango.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteCarteraResumen((((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtNumfac.getValue()) == null) ||
                (txtNumfac.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumfac.getValue().toString()),
                (((txtClsdoc.getValue()) == null) ||
                (txtClsdoc.getValue()).equals("")) ? null
                                                   : new String(
                    txtClsdoc.getValue().toString()),
                (((txtFecfac.getValue()) == null) ||
                (txtFecfac.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecfac.getValue().toString()),
                (((txtFecven.getValue()) == null) ||
                (txtFecven.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecven.getValue().toString()),
                (((txtDias.getValue()) == null) ||
                (txtDias.getValue()).equals("")) ? null
                                                 : new String(
                    txtDias.getValue().toString()),
                (((txtMonto.getValue()) == null) ||
                (txtMonto.getValue()).equals("")) ? null
                                                  : new String(
                    txtMonto.getValue().toString()),
                (((txtPlanta.getValue()) == null) ||
                (txtPlanta.getValue()).equals("")) ? null
                                                   : new String(
                    txtPlanta.getValue().toString()),
                (((txtRango.getValue()) == null) ||
                (txtRango.getValue()).equals("")) ? null
                                                  : new String(
                    txtRango.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateCarteraResumen((((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtNumfac.getValue()) == null) ||
                (txtNumfac.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumfac.getValue().toString()),
                (((txtClsdoc.getValue()) == null) ||
                (txtClsdoc.getValue()).equals("")) ? null
                                                   : new String(
                    txtClsdoc.getValue().toString()),
                (((txtFecfac.getValue()) == null) ||
                (txtFecfac.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecfac.getValue().toString()),
                (((txtFecven.getValue()) == null) ||
                (txtFecven.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecven.getValue().toString()),
                (((txtDias.getValue()) == null) ||
                (txtDias.getValue()).equals("")) ? null
                                                 : new String(
                    txtDias.getValue().toString()),
                (((txtMonto.getValue()) == null) ||
                (txtMonto.getValue()).equals("")) ? null
                                                  : new String(
                    txtMonto.getValue().toString()),
                (((txtPlanta.getValue()) == null) ||
                (txtPlanta.getValue()).equals("")) ? null
                                                   : new String(
                    txtPlanta.getValue().toString()),
                (((txtRango.getValue()) == null) ||
                (txtRango.getValue()).equals("")) ? null
                                                  : new String(
                    txtRango.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String codcli, String numfac,
        String clsdoc, String fecfac, String fecven, String dias, String monto,
        String planta, String rango) throws Exception {
        try {
            BusinessDelegatorView.updateCarteraResumen(codcli, numfac, clsdoc,
                fecfac, fecven, dias, monto, planta, rango);
            renderManager.getOnDemandRenderer("CarteraResumenView")
                         .requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("CarteraResumenView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<CarteraResumen> getCarteraResumen() {
        if (flag) {
            try {
                carteraResumen = BusinessDelegatorView.getCarteraResumen();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return carteraResumen;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setCarteraResumen(List<CarteraResumen> carteraResumen) {
        this.carteraResumen = carteraResumen;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberCarteraResumen() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<CarteraResumen> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberCarteraResumen = 0;

        try {
            totalNumberCarteraResumen = BusinessDelegatorView.findTotalNumberCarteraResumen()
                                                             .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberCarteraResumen) {
            endIndex = totalNumberCarteraResumen;
        }

        try {
            carteraResumen = BusinessDelegatorView.findPageCarteraResumen(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;
        
        return new DataPage<CarteraResumen>(totalNumberCarteraResumen,
            startRow, carteraResumen);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<CarteraResumenDTO> getDataPageDTO(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberCarteraResumen = 0;

        try {
            totalNumberCarteraResumen = BusinessDelegatorView.findTotalNumberCarteraResumen()
                                                             .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberCarteraResumen) {
            endIndex = totalNumberCarteraResumen;
        }

        try {
            carteraResumen = BusinessDelegatorView.findPageCarteraResumen(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(carteraResumen)) {
                carteraResumenDTO = new ArrayList<CarteraResumenDTO>();

                for (CarteraResumen carteraResumenTmp : carteraResumen) {
                    CarteraResumenDTO carteraResumenDTO2 = new CarteraResumenDTO();
                    carteraResumenDTO2.setCodcli(carteraResumenTmp.getId()
                                                                  .getCodcli()
                                                                  .toString());
                    carteraResumenDTO2.setNumfac(carteraResumenTmp.getId()
                                                                  .getNumfac()
                                                                  .toString());
                    carteraResumenDTO2.setClsdoc(carteraResumenTmp.getId()
                                                                  .getClsdoc()
                                                                  .toString());
                    carteraResumenDTO2.setFecfac(carteraResumenTmp.getId()
                                                                  .getFecfac()
                                                                  .toString());
                    carteraResumenDTO2.setFecven(carteraResumenTmp.getId()
                                                                  .getFecven()
                                                                  .toString());
                    carteraResumenDTO2.setDias(carteraResumenTmp.getId()
                                                                .getDias()
                                                                .toString());
                    carteraResumenDTO2.setMonto(carteraResumenTmp.getId()
                                                                 .getMonto()
                                                                 .toString());
                    carteraResumenDTO2.setPlanta(carteraResumenTmp.getId()
                                                                  .getPlanta()
                                                                  .toString());
                    carteraResumenDTO2.setRango(carteraResumenTmp.getId()
                                                                 .getRango()
                                                                 .toString());

                    carteraResumenDTO2.setCarteraResumen(carteraResumenTmp);
                    carteraResumenDTO2.setCarteraResumenView(this);
                    carteraResumenDTO.add(carteraResumenDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<CarteraResumenDTO>(totalNumberCarteraResumen,
            startRow, carteraResumenDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(carteraResumenDTO)) {
            for (CarteraResumenDTO carteraResumenTmp : carteraResumenDTO) {
                renderManager.getOnDemandRenderer("CarteraResumenView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(carteraResumenDTO)) {
            for (CarteraResumenDTO carteraResumenTmp : carteraResumenDTO) {
                renderManager.getOnDemandRenderer("CarteraResumenView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtCodcli() {
        return txtCodcli;
    }

    public void setTxtCodcli(HtmlInputText txtCodcli) {
        this.txtCodcli = txtCodcli;
    }

    public HtmlInputText getTxtNumfac() {
        return txtNumfac;
    }

    public void setTxtNumfac(HtmlInputText txtNumfac) {
        this.txtNumfac = txtNumfac;
    }

    public HtmlInputText getTxtClsdoc() {
        return txtClsdoc;
    }

    public void setTxtClsdoc(HtmlInputText txtClsdoc) {
        this.txtClsdoc = txtClsdoc;
    }

    public HtmlInputText getTxtFecfac() {
        return txtFecfac;
    }

    public void setTxtFecfac(HtmlInputText txtFecfac) {
        this.txtFecfac = txtFecfac;
    }

    public HtmlInputText getTxtFecven() {
        return txtFecven;
    }

    public void setTxtFecven(HtmlInputText txtFecven) {
        this.txtFecven = txtFecven;
    }

    public HtmlInputText getTxtDias() {
        return txtDias;
    }

    public void setTxtDias(HtmlInputText txtDias) {
        this.txtDias = txtDias;
    }

    public HtmlInputText getTxtMonto() {
        return txtMonto;
    }

    public void setTxtMonto(HtmlInputText txtMonto) {
        this.txtMonto = txtMonto;
    }

    public HtmlInputText getTxtPlanta() {
        return txtPlanta;
    }

    public void setTxtPlanta(HtmlInputText txtPlanta) {
        this.txtPlanta = txtPlanta;
    }

    public HtmlInputText getTxtRango() {
        return txtRango;
    }

    public void setTxtRango(HtmlInputText txtRango) {
        this.txtRango = txtRango;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<CarteraResumenDTO> getCarteraResumenDTO() {
        return carteraResumenDTO;
    }

    public void setCarteraResumenDTO(List<CarteraResumenDTO> carteraResumenDTO) {
        this.carteraResumenDTO = carteraResumenDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }
    
    
    

	public List<CarteraResumen> getListadoDetalleFactura() {
		return listadoDetalleFactura;
	}

	public void setListadoDetalleFactura(List<CarteraResumen> listadoDetalleFactura) {
		this.listadoDetalleFactura = listadoDetalleFactura;
	}

	public HtmlOutputText getMsgDetalleFactura() {
		return msgDetalleFactura;
	}

	public void setMsgDetalleFactura(HtmlOutputText msgDetalleFactura) {
		this.msgDetalleFactura = msgDetalleFactura;
	}

    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<CarteraResumen> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<CarteraResumenDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
    
    
    
    //metodo encrgado de consultar el detalle de la factura seleccionada
    public void consultarDetalleFactura(){
    	List<CarteraResumen> registros = null;    	
    	
    	try {
    		HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);    		
    		String rango = (String)session.getAttribute("rango");
    		Integer codCli = (Integer)session.getAttribute("codCli");
        	
        	if(rango != null){
        		//Se crea un objeto del archivo propiedades		
                ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();                
                //se consulta e listado con las descripciones de los rangos
            	String descripcionesRangos[] = archivoPropiedades.getProperty("LISTA_RANGO_FACTURAS").split(",");
            	msgDetalleFactura.setValue(descripcionesRangos[Integer.parseInt(rango)]);
            	
	    		registros = BusinessDelegatorView.consultarDetalleFactura(codCli, rango);
	    		
	    		//se eliminan los atributos de la sesion
	    		session.removeAttribute("rango");
	    		session.removeAttribute("codCli");
	            
	            if(registros == null || registros.size() == 0){            	
	            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	              	mensaje.mostrarMensaje("No se encontraron Facturas Relacionadas", MBMensaje.MENSAJE_ALERTA);
	            	
	              	listadoDetalleFactura = new ArrayList<CarteraResumen>();
	              	
	              	/*onePageDataModel.setDirtyData(false);
	              	flag = false;
	              	onePageDataModel.setPage(new DataPage<CarteraResumen>(0, 0, registros));*/
	              	return;
	            }
	            
	            listadoDetalleFactura = registros;
	            
	            /*onePageDataModel.setDirtyData(false);
	            flag = true;
	            onePageDataModel.setPage(new DataPage<CarteraResumen>(registros.size(), 0, registros));*/
            }        	
            
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    	
    	return;
    }

}
