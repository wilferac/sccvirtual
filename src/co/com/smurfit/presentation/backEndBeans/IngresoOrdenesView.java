package co.com.smurfit.presentation.backEndBeans;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import co.com.smurfit.dataaccess.dao.TsccvDetalleGrupoEmpresaDAO;
import co.com.smurfit.dataaccess.dao.TsccvProductosPedidoDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ConstruirEmailConfirmacionOrden;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.EnviarEmail;
import co.com.smurfit.utilities.FacesUtils;

import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlOutputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.ext.RowSelectorEvent;

public class IngresoOrdenesView {
	
	private HtmlInputText txtClaveBusqueda;
	private HtmlSelectOneMenu somClaveBusqueda;
	private List<SelectItem> siClaveBusqueda;
	private HtmlSelectOneMenu somPlanta;
	private List<SelectItem> siPlanta;
	private HtmlInputText txtNombreEmpresa;
	private HtmlInputText txtIdEmpresa;
	
	private List<ProductoVO> listaProductos;
	private List<ProductoVO> listaProductosSeleccionados;
	
	private boolean renderDataTableBvp;
	private boolean renderDataTableMas;
	private boolean renderLupa;
	
	public boolean ascendente;//utilizado para el orden por los campos en la tabla
	
	public IngresoOrdenesView() {		
		//Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();
        //Se crea un objeto del archivo propiedades		
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String codplaSacosCali=null;
		String codplaCorrugadoCali=null;
		MBUsuario usuario=null;
		try{			
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
        	siClaveBusqueda = new ArrayList<SelectItem>();
        	listaProductos = new ArrayList<ProductoVO>();
        	listaProductosSeleccionados = new ArrayList<ProductoVO>(); 
        	txtNombreEmpresa = new HtmlInputText();
        	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
        	txtIdEmpresa = new HtmlInputText();
        	somPlanta = new HtmlSelectOneMenu();
        	siPlanta = new ArrayList<SelectItem>();
			
			//se crear los listados
			String[] listadoClaBusq = null;
			List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			
			//se cargan los valores de las listas
			listadoClaBusq = getValorParametroClaBusq(); 
			//listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
			
        	//lista de clave busqueda
        	siClaveBusqueda.add(siNulo);
        	for(int i=0; i<listadoClaBusq.length; i++){
        		if(listadoClaBusq[i] == null || listadoClaBusq[i].equals("")){
        			continue;
        		}
        		
            	SelectItem siClaBusq = new SelectItem();        		
            	siClaBusq.setLabel(listadoClaBusq[i]);
            	siClaBusq.setValue(new Long(i));
        		
            	siClaveBusqueda.add(siClaBusq);
        	}
        	
        	//se obtiene el usuario en sesion
        	MBUsuario mbUsu = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
        	TsccvUsuario userLogin = mbUsu.getUsuario();
        	
        	//se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo se muestra la lupa para la busqueda de la empresa
        		renderLupa = true;
        	}
        	else{//si es una empresa normal se colocan el nombre y el id
        		txtIdEmpresa.setValue(userLogin.getTsccvEmpresas().getIdEmpr());
        		txtNombreEmpresa.setValue(userLogin.getTsccvEmpresas().getNombre());        		
        	}        	
        	        	
        	//lista de plantas
        	siPlanta.add(siNulo);
        	if(renderLupa == false){//si la empresa del usuario no es un grupo
        		//si la empresa del usuario tiene cod_mas se consultan las planta con tipo de producto corrugado o sacos
            	if(userLogin.getTsccvEmpresas().getCodVpe() != null || !userLogin.getTsccvEmpresas().getCodVpe().equals("")){
    				listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("vpe", new Long(archivoPropiedades.getProperty("APLICACION"))));
    			}
    			//si la empresa del usuario tiene cod_mas se consultan las planta con tipo de producto molinos
    			if((userLogin.getTsccvEmpresas().getCodMas() != null && !userLogin.getTsccvEmpresas().getCodMas().equals(""))){
    				listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("mas", new Long(archivoPropiedades.getProperty("APLICACION"))));
    			}
    			
	        	for(TsccvPlanta objetoPlanta: listadoPlantas){	        		
	        		SelectItem siPlan = new SelectItem();        		
	        		siPlan.setLabel(objetoPlanta.getDespla());
	        		siPlan.setValue(objetoPlanta.getCodpla());
	        		
	        		siPlanta.add(siPlan);
	        	}
			}
        	        	
            /** Este logica elimina de la lista total de productos los productos ya seleccionados,
             * una vez que vuelve de la pantalla planDeEntrega o ingresa en una nueva sesion y hay un pedido no registrado con productos **/
            
            //lista de productos
        	HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    		
        	somPlanta.setValue(sesion.getAttribute("planta") != null ? sesion.getAttribute("planta") : new Long(-1));
	    	String codCliente = sesion.getAttribute("codCliente") != null ? sesion.getAttribute("codCliente").toString() : null;
	    	String nombreCliente = sesion.getAttribute("nombreCliente") != null ? sesion.getAttribute("nombreCliente").toString() : null;
	    		
	    	if(codCliente != null && !codCliente.equals("")){
	    		txtIdEmpresa.setValue(codCliente);
	    		txtNombreEmpresa.setValue(nombreCliente);
	    		
	    		//se vuelve a cargar la lista de plantas de esa empresa
	    		refrescarListaPlantas();
			}
    		
	    	TsccvPlanta tsccvPlanta = null;
	    	TsccvPlanta plantaConsulta = null;
	    	List<ProductoVO> listaResuldatoConsulta = null;	  
	    	List<TsccvProductosPedido> listaProductosRegistrados = null;
	    	List<ProductosPedidoVO> listadoProductosPedido = null;
	    	
	    	if(new Long(somPlanta.getValue().toString()) != -1 && codCliente != null){
	    		tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(somPlanta.getValue().toString());
	    		
	    		codplaSacosCali= archivoPropiedades.getProperty("planta_sacos_cali");
	    		if(codplaSacosCali.equals(tsccvPlanta.getCodpla())){
		    		codplaCorrugadoCali= archivoPropiedades.getProperty("planta_sacos_cali_reemplaza");
		    		plantaConsulta= BusinessDelegatorView.getTsccvPlanta(codplaCorrugadoCali);
		    	}
		    	else{
		    		plantaConsulta= tsccvPlanta;
		    	}
	    		
	    		listaResuldatoConsulta = BusinessDelegatorView.consultarProductos(null, null, codCliente, tsccvPlanta, "", true);
	    		usuario= (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
	    		Integer idPedi = BusinessDelegatorView.consultarPedidoNoRegistrado(obtenerEmpresaUsuario(), somPlanta.getValue().toString(), usuario.getUsuario().getIdUsua());
	    		if(idPedi != null){
	    			listadoProductosPedido = new ArrayList<ProductosPedidoVO>();
		    		//si hay un pedido no registrado se consulta
			    	TsccvPedidos tsccvPedidos = BusinessDelegatorView.getTsccvPedidos(idPedi);
			    	//se consultan los productos pedido
			    	listaProductosRegistrados = BusinessDelegatorView.findByPropertyProductosPedido(TsccvProductosPedidoDAO.PEDIDO, tsccvPedidos.getIdPedi());			    	
			    	//listadoProductosPedido = new ArrayList<ProductosPedidoVO>();
					
			    	//se rrecorre la lista de productosPedido y se crea una de productosVO
					for(TsccvProductosPedido prodPed : listaProductosRegistrados){
						ProductosPedidoVO prodPedVO = new ProductosPedidoVO();
						prodPedVO.setTsccvProductosPedido(prodPed);
						
						listadoProductosPedido.add(prodPedVO);
					}
		    	}
	    		
	        	//se eliminan los productos que ya se encuentran en el pedido
				for(ProductoVO producto : listaResuldatoConsulta){
					listaProductos.add(producto);				
	
					if(listaProductosRegistrados != null && idPedi != null){
						for(ProductosPedidoVO objProdPedVO : listadoProductosPedido){
							//si el tipo de producto de la planata es corrugado
							if(producto.getBvpPrecio() != null){
								if(objProdPedVO.getTsccvProductosPedido().getCodCli().trim().equals(producto.getBvpPrecio().getId().getCodCli().trim())){
									listaProductos.remove(producto);
									break;
								}
							}
							//si el tipo de producto de la planata es molinos
							else if(producto.getSapPrecio() != null){
								if(objProdPedVO.getTsccvProductosPedido().getCodCli().trim().equals(producto.getSapPrecio().getId().getCodcli().trim())){
									listaProductos.remove(producto);
									break;
								}
							}
						}
					}
				}
				
	    		//se muestra la tabla dependiendo del tipo de producto de la planta
				if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_CORRUGADO"))) ||
		    	   tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_SACOS")))){
					renderDataTableBvp = true;
					renderDataTableMas = false;
	        	}
	        	else{//productos molinos
	        		renderDataTableBvp = false;
					renderDataTableMas = true;
	        	}
	    	}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	
	public boolean isRenderDataTableMas() {
		return renderDataTableMas;
	}

	public void setRenderDataTableMas(boolean renderDataTableMas) {
		this.renderDataTableMas = renderDataTableMas;
	}

	public boolean isRenderDataTableBvp() {
		return renderDataTableBvp;
	}

	public void setRenderDataTableBvp(boolean renderDataTableBvp) {
		this.renderDataTableBvp = renderDataTableBvp;
	}

	public boolean isRenderLupa() {
		return renderLupa;
	}

	public void setRenderLupa(boolean renderLupa) {
		this.renderLupa = renderLupa;
	}
	
	public HtmlInputText getTxtClaveBusqueda() {
		return txtClaveBusqueda;
	}

	public void setTxtClaveBusqueda(HtmlInputText txtClaveBusqueda) {
		this.txtClaveBusqueda = txtClaveBusqueda;
	}

	public HtmlSelectOneMenu getSomClaveBusqueda() {
		return somClaveBusqueda;
	}

	public void setSomClaveBusqueda(HtmlSelectOneMenu somClaveBusqueda) {
		this.somClaveBusqueda = somClaveBusqueda;
	}

	public List<SelectItem> getSiClaveBusqueda() {
		return siClaveBusqueda;
	}

	public void setSiClaveBusqueda(List<SelectItem> siClaveBusqueda) {
		this.siClaveBusqueda = siClaveBusqueda;
	}
    
	public HtmlSelectOneMenu getSomPlanta() {
		return somPlanta;
	}

	public void setSomPlanta(HtmlSelectOneMenu somPlanta) {
		this.somPlanta = somPlanta;
	}

	public List<SelectItem> getSiPlanta() {
		return siPlanta;
	}

	public void setSiPlanta(List<SelectItem> siPlanta) {
		this.siPlanta = siPlanta;
	}

	public HtmlInputText getTxtNombreEmpresa() {
		return txtNombreEmpresa;
	}

	public void setTxtNombreEmpresa(HtmlInputText txtNombreEmpresa) {
		this.txtNombreEmpresa = txtNombreEmpresa;
	}

	public HtmlInputText getTxtIdEmpresa() {
		return txtIdEmpresa;
	}

	public void setTxtIdEmpresa(HtmlInputText txtIdEmpresa) {
		this.txtIdEmpresa = txtIdEmpresa;
	}

	public List<ProductoVO> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<ProductoVO> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public void limpiar(ValueChangeEvent event){
		txtClaveBusqueda.setValue(null);
	}
	
	public boolean isAscendente() {
		return ascendente;
	}

	public void setAscendente(boolean ascendente) {
		this.ascendente = ascendente;
	}
	
	public String limpiar(){
		try{
	    	//se obtiene el usuario en sesion
        	MBUsuario mbUsu = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
        	TsccvUsuario userLogin = mbUsu.getUsuario();
        	
        	//se valida si la empresa del usuario es un grupo
        	if(userLogin.getTsccvEmpresas().getTsccvGrupoEmpresa() != null){
        		//si es un grupo de empresas se limpian el nombre y el id
    	        ArchivoBundle archivoBundle = new ArchivoBundle();
    	    	txtNombreEmpresa.setValue(archivoBundle.getProperty("seleccioneUnaEmpresa"));
    	    	txtIdEmpresa.setValue(null);
    	    	
    	    	refrescarListaPlantas();
        	}
	    	
	    	somPlanta.setValue(new Long(-1));
	    	somClaveBusqueda.setValue(new Long(-1));
	    	txtClaveBusqueda.setValue(null);
	    	listaProductos.clear();
	    	
	    	return "";
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
	}


	//Metodo encargado de consultar el parametro para el campo claveBusqueda
    public String[] getValorParametroClaBusq(){    	
    	//ARCHIVO PORPIEDADES
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String temp[] = null;
        try {
        	temp = archivoPropiedades.getProperty("LISTA_CLAVE_BUSQUEDA").toString().split(",");
        	
			return temp;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
		return null;           
    }
    
    //metodo encargado de adiconar los productos seleccionados a la una segunda lista
	public void seleccionar(ValueChangeEvent event){
		listaProductosSeleccionados.clear();
		
		for(ProductoVO objProducto : listaProductos){			
			if(objProducto.getSeleccionado()){
				listaProductosSeleccionados.add(objProducto);
			}			
		}		
	}
	
	public String generarPedido(){
		MBUsuario usuario=null;
		try{
			//se llama al metodo seleccionar para adicionar los productos con check en true en la lista listaProductosSeleccionados 
			seleccionar(null);
			
			PlanDeEntregaView planDeEntregaView = (PlanDeEntregaView) FacesUtils.getManagedBean("planDeEntregaView");
			TsccvPedidos tsccvPedidos = null;
			usuario= (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
			Integer idPedi = BusinessDelegatorView.consultarPedidoNoRegistrado(obtenerEmpresaUsuario(), somPlanta.getValue().toString(), usuario.getUsuario().getIdUsua());
			
			HttpSession sesion = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
			sesion.setAttribute("planta", somPlanta.getValue().toString());
			sesion.setAttribute("codCliente", obtenerEmpresaUsuario());
			sesion.setAttribute("nombreCliente", txtNombreEmpresa.getValue());
			
			if(idPedi != null){//si hay un pedido no registrado (pendiente)
				tsccvPedidos = BusinessDelegatorView.getTsccvPedidos(idPedi);
				List<TsccvProductosPedido> listadoProdPedido = BusinessDelegatorView.findByPropertyProductosPedido(TsccvProductosPedidoDAO.PEDIDO, tsccvPedidos.getIdPedi());
				
				//se despliega la tabla
				planDeEntregaView.setRenderDataTable(true);
				//se setea el pedido pendiente
				planDeEntregaView.setPedido(tsccvPedidos);
				
				if(listaProductosSeleccionados.size() > 0){
					planDeEntregaView.setListaProductosSeleccionados(listaProductosSeleccionados);
					planDeEntregaView.adicionarProductosPedido();
				}
				else{
					if(listadoProdPedido != null && listadoProdPedido.size() > 0){
						planDeEntregaView.consultarProductosPedido();
					}
					else{
						MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
						mensaje.mostrarMensaje("Hay un pedido pendiente para la Empresa y Planta seleccionadas, pero no tiene productos adicionados.", MBMensaje.MENSAJE_ALERTA);
						return "";
					}
				}
				
				//se configura la pantalla planDeEntrega en caso de que el pedido sea para una planta con tipo producto molinos
				planDeEntregaView.configurarParaMolinos();
			}
			else if(listaProductosSeleccionados.size() > 0){//si el pedido es nuevos
				//se setea el listado de productos seleccionados
				planDeEntregaView.setListaProductosSeleccionados(listaProductosSeleccionados);
				//se despliega la tabla
				planDeEntregaView.setRenderDataTable(true);
				planDeEntregaView.guardarPedido();

				//se configura la pantalla planDeEntrega en caso de que el pedido sea para una planta con tipo producto molinos
				planDeEntregaView.configurarParaMolinos();
			}
			else{
				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensaje("No hay productos seleccionados para generar el pedido, por favor consulte sus productos y seleccione al menos uno.", MBMensaje.MENSAJE_ALERTA);
				return "";
			}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return "";
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return "";
		}
		
		return "ogPlanDeEntrega";
	}
	
	public void consultarProductos(){
		ArchivoPropiedades arc=null;
		String codplaSacosCali=null;
		String codplaCorrugadoCali=null;
		MBUsuario usuario=null;
		try{			
			//se limpia la lista actual
			listaProductos.clear();
			
			String codigoCli = String.valueOf(obtenerEmpresaUsuario());
	    	TsccvPlanta tsccvPlanta = BusinessDelegatorView.getTsccvPlanta(somPlanta.getValue().toString());
	    	TsccvPlanta plantaConsulta=null;
	    	
	    	//Validamos si la planta no es sacos ya que deberia cambiarse
	    	arc= new ArchivoPropiedades();
	    	codplaSacosCali= arc.getProperty("planta_sacos_cali");
	    	
	    	if(codplaSacosCali.equals(tsccvPlanta.getCodpla())){
	    		codplaCorrugadoCali= arc.getProperty("planta_sacos_cali_reemplaza");
	    		plantaConsulta= BusinessDelegatorView.getTsccvPlanta(codplaCorrugadoCali);
	    	}
	    	else{
	    		plantaConsulta= tsccvPlanta;
	    	}
			
	    	//Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	    	
	        //se consultan todos los productos para la planta y empresa seleccionadas
			List<ProductoVO> listaResuldatoConsulta = BusinessDelegatorView.consultarProductos(
					(new Long(somClaveBusqueda.getValue().toString()) == 0 ? txtClaveBusqueda.getValue().toString().toUpperCase() : null),
					(new Long(somClaveBusqueda.getValue().toString()) == 1 ? txtClaveBusqueda.getValue().toString().toUpperCase() : null), codigoCli, plantaConsulta,
					FacesUtils.getRequestParameter("nombreColumna"), (ascendente = !ascendente));
			
			//se consultan los productos ya seleccionados para el pedido
        	List<ProductosPedidoVO> listadoProductosPedido = null;
        	usuario= (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
        	Integer idPedi = BusinessDelegatorView.consultarPedidoNoRegistrado(obtenerEmpresaUsuario(), somPlanta.getValue().toString(), usuario.getUsuario().getIdUsua());
	    	if(idPedi != null){
	    		listadoProductosPedido = new ArrayList<ProductosPedidoVO>();
	    		//si hay un pedido no registrado se consulta
		    	TsccvPedidos tsccvPedidos = BusinessDelegatorView.getTsccvPedidos(idPedi);
		    	//se consultan los productos pedido
		    	List<TsccvProductosPedido> listaProductosRegistrados = BusinessDelegatorView.findByPropertyProductosPedido(TsccvProductosPedidoDAO.PEDIDO, tsccvPedidos.getIdPedi());			    	
		    	//listadoProductosPedido = new ArrayList<ProductosPedidoVO>();
				
		    	//se rrecorre la lista de productosPedido y se crea una de productosVO 
				for(TsccvProductosPedido prodPed : listaProductosRegistrados){
					ProductosPedidoVO prodPedVO = new ProductosPedidoVO();
					prodPedVO.setTsccvProductosPedido(prodPed);
					
					listadoProductosPedido.add(prodPedVO);
				}
	    	}
        	
	    	//flag utilizado para saber cuando se eliminaron productos de la lista por que ya estaban registrados en un pedido
	    	boolean yaRegistrados = false;
	    	
        	//se eliminan los productos que ya se encuentran en el pedido
    		for(ProductoVO producto : listaResuldatoConsulta){
				listaProductos.add(producto);

				if((listadoProductosPedido != null && listadoProductosPedido.size() > 0) && idPedi != null){
					//se deja el falg en true ya que hay porductos que estan registrados en un pedido pendiente
					yaRegistrados = true;
					
					for(ProductosPedidoVO objProdPedVO : listadoProductosPedido){
						//si el tipo de producto de la planata es corrugado
						if(producto.getBvpPrecio() != null){
							if(objProdPedVO.getTsccvProductosPedido().getCodCli().trim().equals(producto.getBvpPrecio().getId().getCodCli().trim())){
								listaProductos.remove(producto);
								break;
							}
						}
						//si el tipo de producto de la planata es molinos
						else if(producto.getSapPrecio() != null){
							if(objProdPedVO.getTsccvProductosPedido().getCodCli().trim().equals(producto.getSapPrecio().getId().getCodcli().trim())){
								listaProductos.remove(producto);
								break;
							}
						}
					}
				}
			}			
    		
    		//se muestra la tabla dependiendo del tipo de producto de la planta
    		if(tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_CORRUGADO"))) ||
    	        tsccvPlanta.getTsccvTipoProducto().getIdTipr().equals(new Long(archivoPropiedades.getProperty("PRODUCTO_SACOS")))){
    			renderDataTableBvp = true;
				renderDataTableMas = false;
        	}
        	else{//productos molinos
        		renderDataTableBvp = false;
				renderDataTableMas = true;
        	}
			
			//se valida si se elimnaron productis de la lista
    		if(yaRegistrados){
    			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensaje("Existen productos que se eliminaron del listado ya que hacen parte de un pedido sin registrar, " +
									   "podra observarlos al presionar el boton Generar Pedido.", MBMensaje.MENSAJE_ALERTA);
    		}
    		else if(!yaRegistrados && (listaResuldatoConsulta == null || listaResuldatoConsulta.size() == 0)){
    			listaProductos.clear();
    			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
				mensaje.mostrarMensaje("No se encontraron productos para la Planta y Empresas seleccionadas.", MBMensaje.MENSAJE_ALERTA);
    		}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	//metodo encargado de limpiar las listas cuando cambie la planta o la empresa
	public void cambiaPlantaOEmpresa(ValueChangeEvent event){
		try{			
			if(obtenerEmpresaUsuario() != null && new Long(somPlanta.getValue().toString()) != -1){
				listaProductos.clear();
				listaProductosSeleccionados.clear();
				consultarProductos();
			}
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
	
	public Integer obtenerEmpresaUsuario(){
		String codigoCli = null;
		codigoCli = txtIdEmpresa.getValue().toString();
		return !codigoCli.equals("") ? new Integer(codigoCli) : null;
	}
	
	//metodo encargado mostrar el panel para la busqueda y seleccion de la empresa
	public void buscarEmpresa(){
		listaProductos.clear();			
		
		//se obtiene el managebean
		BusquedaEmpresaView busquedaEmpresaView = (BusquedaEmpresaView) FacesUtils.getManagedBean("busquedaEmpresaView");
		
		//se setea el managebean que realiza el llamado this
		busquedaEmpresaView.setManageBeanLlmado(this);
		busquedaEmpresaView.mostrarPanel();
	}
	
	//metodo encargado de setear a los componentes de la pantalla el nombre y el id de la empresa seleccionada 
	public void colocarSeleccion(Integer idEmpr, String nombreEmpr){
		txtIdEmpresa.setValue(idEmpr);
		txtNombreEmpresa.setValue(nombreEmpr);
		refrescarListaPlantas();
	}
	
	//metodo encargado de colocar las plantas de acuerdo a la empresa seleccionada considarando:
	//1. si la empresa tiene cod_vpe solo se cargan plantas con tipo de producto corrugado o sacos
	//2. si la empresa tiene cod_mas solo se cargan plantas con tipo de producto molinos
	public void refrescarListaPlantas(){
		try{
			//Se crea un objeto del bundle		
	        ArchivoBundle archivoBundle = new ArchivoBundle();
	        //Se crea un objeto del archivo propiedades		
	        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	        
			List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			//listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
			siPlanta = new ArrayList<SelectItem>();
			
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
	    	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
	    	siNulo.setValue(new Long(-1));
	    	siPlanta.add(siNulo);
	    	
	    	TsccvEmpresas tsccvEmpresas = null;
	    	if(txtIdEmpresa.getValue() != null && !txtIdEmpresa.getValue().equals("")){
	    		tsccvEmpresas = BusinessDelegatorView.getTsccvEmpresas(new Integer(txtIdEmpresa.getValue().toString()));	    	
			
	    		//se consultan las plantas del cliente
	        	if(tsccvEmpresas.getCodVpe() != null && !tsccvEmpresas.getCodVpe().equals("")){
					listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("vpe", new Long(archivoPropiedades.getProperty("APLICACION"))));
				}
				//si la la empresa del usuario tiene cod_mas elimina las planta con tipo de producto corrugado o sacos
				if((tsccvEmpresas.getCodMas() != null && !tsccvEmpresas.getCodMas().equals(""))){
					listadoPlantas.addAll(BusinessDelegatorView.findByTipProdAndAplicacion("mas", new Long(archivoPropiedades.getProperty("APLICACION"))));
				}
	        	
	        	for(TsccvPlanta objetoPlanta: listadoPlantas){	        		
	        		SelectItem siPlan = new SelectItem();        		
	        		siPlan.setLabel(objetoPlanta.getDespla());
	        		siPlan.setValue(objetoPlanta.getCodpla());
	        		
	        		siPlanta.add(siPlan);
				}
	    	}
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);			
			
			return;
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);			
			
			return;
		}
	}
}
