package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.dataaccess.dao.TsccvPlantasUsuarioDAO;
import co.com.smurfit.dataaccess.dao.TsccvUsuarioDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseErroresLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesGruposLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseOpcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseSubopcionesOpcLogic;
import co.com.smurfit.sccvirtual.control.TsccvAccesosUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvDetalleGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantasUsuarioLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvUsuarioLogic;
import co.com.smurfit.sccvirtual.dto.TsccvPlantasUsuarioDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.etiquetas.ArchivoBundle;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvPlantasUsuarioView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtCodpla_TsccvPlanta;
    private HtmlInputText txtIdUsua_TsccvUsuario;
    private HtmlInputText txtCodpla;
    private HtmlInputText txtIdUsua;
    private SelectInputDate txtFechaCreacion;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<TsccvPlantasUsuario> tsccvPlantasUsuario;
    private List<TsccvPlantasUsuarioDTO> tsccvPlantasUsuarioDTO;
    
    //listas desplegables
    private HtmlSelectOneMenu somUsuario;
    private List<SelectItem> siUsuario;
    private HtmlSelectOneMenu somPlanta;
    private List<SelectItem> siPlanta;
    
    private List<TsccvPlantasUsuario> listadoPlantasUsuaio;

    public TsccvPlantasUsuarioView() {
        super("");
        
        //Se crea un objeto del bundle		
        ArchivoBundle archivoBundle = new ArchivoBundle();
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		
		try{
			//se crea el primer item de las listas
			SelectItem siNulo= new SelectItem();
        	siNulo.setLabel(archivoBundle.getProperty("seleccionarRegistro"));
        	siNulo.setValue(new Long(-1));
			
			//se inicializan los componentes de las listas
			siUsuario = new ArrayList<SelectItem>();
			siPlanta = new ArrayList<SelectItem>();
			
			//se crear los listados
			List<TsccvUsuario> listadoUsuarios = new ArrayList<TsccvUsuario>();
			List<TsccvPlanta> listadoPlantas = new ArrayList<TsccvPlanta>();
			listadoPlantasUsuaio = new ArrayList<TsccvPlantasUsuario>();
			
			//se cargan los valores de las listas
			//se filtrar la lista de usuarios mostrando solo aquellos que pertenezcan al grupo ADMINISTRADORES_PLANTA
			listadoUsuarios = BusinessDelegatorView.consultarUsuariosPorGrupo(new Integer(archivoPropiedades.getProperty("ADMINISTRADOR_PLANTAS")));
			listadoPlantas = BusinessDelegatorView.findByActivoAplicacion("0", new Long(archivoPropiedades.getProperty("APLICACION")));
            
			//lista de usuarios
        	siUsuario.add(siNulo);
        	for(TsccvUsuario objetoUsua: listadoUsuarios){
        		if(objetoUsua.getActivo().equals("1")){
        			continue;
        		}
        		
        		SelectItem siUsua = new SelectItem();        		
        		siUsua.setLabel(objetoUsua.getApellido()+" "+objetoUsua.getNombre());
        		siUsua.setValue(objetoUsua.getIdUsua());
        		
        		siUsuario.add(siUsua);
        	}
        	
            //lista de plantas
        	siPlanta.add(siNulo);
        	for(TsccvPlanta objetoPlanta: listadoPlantas){        		
        		SelectItem siPlan = new SelectItem();        		
        		siPlan.setLabel(objetoPlanta.getDespla());
        		siPlan.setValue(objetoPlanta.getCodpla());
        		
        		siPlanta.add(siPlan);
        	}
		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
		}
    }

    public String action_clear() {
        txtCodpla_TsccvPlanta.setValue(null);
        txtCodpla_TsccvPlanta.setDisabled(false);
        somPlanta.setValue(new Long(-1));
        
        txtIdUsua_TsccvUsuario.setValue(null);
        txtIdUsua_TsccvUsuario.setDisabled(false);
        somUsuario.setValue(new Long(-1));

        txtFechaCreacion.setValue(null);
        txtFechaCreacion.setDisabled(false);

        txtCodpla.setValue(null);
        txtCodpla.setDisabled(false);
        txtIdUsua.setValue(null);
        txtIdUsua.setDisabled(false);

        btnSave.setDisabled(true);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);
        
        listadoPlantasUsuaio.clear();

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            TsccvPlantasUsuario entity = null;

            try {
                TsccvPlantasUsuarioId id = new TsccvPlantasUsuarioId();
                id.setCodpla((((txtCodpla.getValue()) == null) ||
                    (txtCodpla.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodpla.getValue().toString()));
                id.setIdUsua((((txtIdUsua.getValue()) == null) ||
                    (txtIdUsua.getValue()).equals("")) ? null
                                                       : new Integer(
                        txtIdUsua.getValue().toString()));

                entity = BusinessDelegatorView.getTsccvPlantasUsuario(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtCodpla_TsccvPlanta.setDisabled(false);
                txtIdUsua_TsccvUsuario.setDisabled(false);

                txtFechaCreacion.setDisabled(false);

                txtCodpla.setDisabled(false);
                txtIdUsua.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtFechaCreacion.setValue(entity.getFechaCreacion());
                txtFechaCreacion.setDisabled(false);
                txtCodpla_TsccvPlanta.setValue(entity.getTsccvPlanta()
                                                     .getCodpla());
                txtCodpla_TsccvPlanta.setDisabled(false);
                txtIdUsua_TsccvUsuario.setValue(entity.getTsccvUsuario()
                                                      .getIdUsua());
                txtIdUsua_TsccvUsuario.setDisabled(false);

                txtCodpla.setValue(entity.getId().getCodpla());
                txtCodpla.setDisabled(true);
                txtIdUsua.setValue(entity.getId().getIdUsua());
                txtIdUsua.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
        	txtIdUsua.setValue(somUsuario.getValue());
        	txtIdUsua_TsccvUsuario.setValue(somUsuario.getValue());
        	txtFechaCreacion.setValue(new Date());
        	
        	if(listadoPlantasUsuaio.size() > 0){
	        	int plantasNuevas = 0;
	        	for(TsccvPlantasUsuario plantaUsua : listadoPlantasUsuaio){
	        		if(plantaUsua.getTsccvPlanta().getPlanta() != null){
	        			plantasNuevas++;
	        		}
	        	}
	        	
	        	if(plantasNuevas == 0){
	        		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	              	mensaje.mostrarMensaje("No se agrego ninguna Planta nueva", MBMensaje.MENSAJE_ALERTA);
	              	
	              	return "";
	            }
        	}
        	else{
    			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("Por favor agregue al menos una Planta", MBMensaje.MENSAJE_ALERTA);
              	return "";
        	}
        	
            BusinessDelegatorView.saveTsccvPlantasUsuario((((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtIdUsua.getValue()) == null) ||
                (txtIdUsua.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdUsua.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtCodpla_TsccvPlanta.getValue()) == null) ||
                (txtCodpla_TsccvPlanta.getValue()).equals("")) ? null
                                                               : new String(
                    txtCodpla_TsccvPlanta.getValue().toString()),
                (((txtIdUsua_TsccvUsuario.getValue()) == null) ||
                (txtIdUsua_TsccvUsuario.getValue()).equals("")) ? null
                                                                : new Integer(
                    txtIdUsua_TsccvUsuario.getValue().toString()), listadoPlantasUsuaio);
            
            MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
          	mensaje.mostrarMensaje("Plantas adicionadas de forma satisfactoria", MBMensaje.MENSAJE_OK);

        } catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return "";
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return "";
		}

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteTsccvPlantasUsuario((((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtIdUsua.getValue()) == null) ||
                (txtIdUsua.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdUsua.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateTsccvPlantasUsuario((((txtCodpla.getValue()) == null) ||
                (txtCodpla.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpla.getValue().toString()),
                (((txtIdUsua.getValue()) == null) ||
                (txtIdUsua.getValue()).equals("")) ? null
                                                   : new Integer(
                    txtIdUsua.getValue().toString()),
                (((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : (Date) txtFechaCreacion.getValue(),
                (((txtCodpla_TsccvPlanta.getValue()) == null) ||
                (txtCodpla_TsccvPlanta.getValue()).equals("")) ? null
                                                               : new String(
                    txtCodpla_TsccvPlanta.getValue().toString()),
                (((txtIdUsua_TsccvUsuario.getValue()) == null) ||
                (txtIdUsua_TsccvUsuario.getValue()).equals("")) ? null
                                                                : new Integer(
                    txtIdUsua_TsccvUsuario.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String codpla, Integer idUsua,
        Date fechaCreacion, String codpla_TsccvPlanta,
        Integer idUsua_TsccvUsuario) throws Exception {
        try {
            BusinessDelegatorView.updateTsccvPlantasUsuario(codpla, idUsua,
                fechaCreacion, codpla_TsccvPlanta, idUsua_TsccvUsuario);
            renderManager.getOnDemandRenderer("TsccvPlantasUsuarioView")
                         .requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("TsccvPlantasUsuarioView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<TsccvPlantasUsuario> getTsccvPlantasUsuario() {
        if (flag) {
            try {
                tsccvPlantasUsuario = BusinessDelegatorView.getTsccvPlantasUsuario();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return tsccvPlantasUsuario;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setTsccvPlantasUsuario(
        List<TsccvPlantasUsuario> tsccvPlantasUsuario) {
        this.tsccvPlantasUsuario = tsccvPlantasUsuario;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberTsccvPlantasUsuario() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvPlantasUsuario> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvPlantasUsuario = 0;

        try {
            totalNumberTsccvPlantasUsuario = BusinessDelegatorView.findTotalNumberTsccvPlantasUsuario()
                                                                  .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvPlantasUsuario) {
            endIndex = totalNumberTsccvPlantasUsuario;
        }

        try {
            tsccvPlantasUsuario = BusinessDelegatorView.findPageTsccvPlantasUsuario(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvPlantasUsuario>(totalNumberTsccvPlantasUsuario,
            startRow, tsccvPlantasUsuario);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<TsccvPlantasUsuarioDTO> getDataPageDTO(int startRow,
        int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberTsccvPlantasUsuario = 0;

        try {
            totalNumberTsccvPlantasUsuario = BusinessDelegatorView.findTotalNumberTsccvPlantasUsuario()
                                                                  .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberTsccvPlantasUsuario) {
            endIndex = totalNumberTsccvPlantasUsuario;
        }

        try {
            tsccvPlantasUsuario = BusinessDelegatorView.findPageTsccvPlantasUsuario(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(tsccvPlantasUsuario)) {
                tsccvPlantasUsuarioDTO = new ArrayList<TsccvPlantasUsuarioDTO>();

                for (TsccvPlantasUsuario tsccvPlantasUsuarioTmp : tsccvPlantasUsuario) {
                    TsccvPlantasUsuarioDTO tsccvPlantasUsuarioDTO2 = new TsccvPlantasUsuarioDTO();
                    tsccvPlantasUsuarioDTO2.setCodpla(tsccvPlantasUsuarioTmp.getId()
                                                                            .getCodpla()
                                                                            .toString());
                    tsccvPlantasUsuarioDTO2.setIdUsua(tsccvPlantasUsuarioTmp.getId()
                                                                            .getIdUsua()
                                                                            .toString());

                    tsccvPlantasUsuarioDTO2.setFechaCreacion(tsccvPlantasUsuarioTmp.getFechaCreacion());
                    tsccvPlantasUsuarioDTO2.setCodpla_TsccvPlanta((tsccvPlantasUsuarioTmp.getTsccvPlanta()
                                                                                         .getCodpla() != null)
                        ? tsccvPlantasUsuarioTmp.getTsccvPlanta().getCodpla()
                                                .toString() : null);
                    tsccvPlantasUsuarioDTO2.setIdUsua_TsccvUsuario((tsccvPlantasUsuarioTmp.getTsccvUsuario()
                                                                                          .getIdUsua() != null)
                        ? tsccvPlantasUsuarioTmp.getTsccvUsuario().getIdUsua()
                                                .toString() : null);
                    tsccvPlantasUsuarioDTO2.setTsccvPlantasUsuario(tsccvPlantasUsuarioTmp);
                    tsccvPlantasUsuarioDTO2.setTsccvPlantasUsuarioView(this);
                    tsccvPlantasUsuarioDTO.add(tsccvPlantasUsuarioDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<TsccvPlantasUsuarioDTO>(totalNumberTsccvPlantasUsuario,
            startRow, tsccvPlantasUsuarioDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(tsccvPlantasUsuarioDTO)) {
            for (TsccvPlantasUsuarioDTO tsccvPlantasUsuarioTmp : tsccvPlantasUsuarioDTO) {
                renderManager.getOnDemandRenderer("TsccvPlantasUsuarioView")
                             .remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(tsccvPlantasUsuarioDTO)) {
            for (TsccvPlantasUsuarioDTO tsccvPlantasUsuarioTmp : tsccvPlantasUsuarioDTO) {
                renderManager.getOnDemandRenderer("TsccvPlantasUsuarioView")
                             .add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtCodpla_TsccvPlanta() {
        return txtCodpla_TsccvPlanta;
    }

    public void setTxtCodpla_TsccvPlanta(HtmlInputText txtCodpla_TsccvPlanta) {
        this.txtCodpla_TsccvPlanta = txtCodpla_TsccvPlanta;
    }

    public HtmlInputText getTxtIdUsua_TsccvUsuario() {
        return txtIdUsua_TsccvUsuario;
    }

    public void setTxtIdUsua_TsccvUsuario(HtmlInputText txtIdUsua_TsccvUsuario) {
        this.txtIdUsua_TsccvUsuario = txtIdUsua_TsccvUsuario;
    }

    public SelectInputDate getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(SelectInputDate txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public HtmlInputText getTxtCodpla() {
        return txtCodpla;
    }

    public void setTxtCodpla(HtmlInputText txtCodpla) {
        this.txtCodpla = txtCodpla;
    }

    public HtmlInputText getTxtIdUsua() {
        return txtIdUsua;
    }

    public void setTxtIdUsua(HtmlInputText txtIdUsua) {
        this.txtIdUsua = txtIdUsua;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TsccvPlantasUsuarioDTO> getTsccvPlantasUsuarioDTO() {
        return tsccvPlantasUsuarioDTO;
    }

    public void setTsccvPlantasUsuarioDTO(
        List<TsccvPlantasUsuarioDTO> tsccvPlantasUsuarioDTO) {
        this.tsccvPlantasUsuarioDTO = tsccvPlantasUsuarioDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    
    
    public HtmlSelectOneMenu getSomUsuario() {
		return somUsuario;
	}

	public void setSomUsuario(HtmlSelectOneMenu somUsuario) {
		this.somUsuario = somUsuario;
	}

	public List<SelectItem> getSiUsuario() {
		return siUsuario;
	}

	public void setSiUsuario(List<SelectItem> siUsuario) {
		this.siUsuario = siUsuario;
	}

	public HtmlSelectOneMenu getSomPlanta() {
		return somPlanta;
	}

	public void setSomPlanta(HtmlSelectOneMenu somPlanta) {
		this.somPlanta = somPlanta;
	}

	public List<SelectItem> getSiPlanta() {
		return siPlanta;
	}

	public void setSiPlanta(List<SelectItem> siPlanta) {
		this.siPlanta = siPlanta;
	}
    
	public List<TsccvPlantasUsuario> getListadoPlantasUsuaio() {
		return listadoPlantasUsuaio;
	}

	public void setListadoPlantasUsuaio(List<TsccvPlantasUsuario> listadoPlantasUsuaio) {
		this.listadoPlantasUsuaio = listadoPlantasUsuaio;
	}
	
	
    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvPlantasUsuario> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<TsccvPlantasUsuarioDTO> fetchPage(int startRow,
            int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }

    
    public void consultarPlantasPorUsuario(ValueChangeEvent event){
    	if (event == null || (event.getNewValue() != null && !event.getNewValue().equals(""))) {	    	
	    	Integer idUsua = new Integer(new Long(somUsuario.getValue().toString()) != -1 ? somUsuario.getValue().toString() : "0");
	    	
	    	listadoPlantasUsuaio = new ArrayList<TsccvPlantasUsuario>();
	    	
	    	idUsua = idUsua.equals(new Integer(0)) ? null : idUsua;
	    	
	    	if(idUsua != null){    		
	    		try{
	    			//Se consulta las plantas para el usuario seleccionado
	    			//List<TsccvPlanta> listadoPlantas = BusinessDelegatorView.consultarPlantasPorUsuario(idUsua);
	    			List<TsccvPlantasUsuario> listadoPlantas = BusinessDelegatorView.findByPropertyPlantasUsuario(TsccvPlantasUsuarioDAO.USUARIO, idUsua);	    			
	    			
	    			//Se recorre el listado de detalles y se asigna cada una de los usuaros a la tabla
	    			for(TsccvPlantasUsuario objetoPlanta : listadoPlantas){
	    				listadoPlantasUsuaio.add(objetoPlanta);
	    			}
	    			
	    			btnSave.setDisabled(false);
	    		}catch(Exception e){
	    			//FacesContext.getCurrentInstance().addMessage("", new FacesMessage(e.getMessage()));
	    			//Mensaje mensaje = (Mensaje) FacesUtils.getManagedBean("mensaje");
	              	//mensaje.mostrarMensaje(e.getMessage(), Mensaje.MENSAJE_ERROR);
	    		}
	    	}
	    	else{
	    		btnSave.setDisabled(true);
	    	}
    	}
    }

	//metodo que agrega las plantas a la lista
    public void agregarPlanta(ActionEvent event){
    	//Se valida si la planta seleccionada es valida
    	try{
	    	if((somPlanta.getValue() != null) && (new Long(somPlanta.getValue().toString()) != -1)){
	    		//se valida si la planta ya esta siendo administrada, de ser asi este metodo lanza una Excepcion
	    		BusinessDelegatorView.plantaIsAdministrada(somPlanta.getValue().toString());	    		
	    		
	    		//Se valida si la planta ya se agrego
	    		for(TsccvPlantasUsuario plantaUsua: listadoPlantasUsuaio){
	    			if(plantaUsua.getTsccvPlanta().getCodpla().equals(somPlanta.getValue().toString())){
	    				MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	    	          	mensaje.mostrarMensaje("Esta Planta ya fue agregada", MBMensaje.MENSAJE_ALERTA);
	    	          	
	    	          	return;
	    			}
	    		}
	    		
	    		TsccvPlantasUsuario plantaUsuario = new TsccvPlantasUsuario();
	    		TsccvPlanta plantaSeleccionada = new TsccvPlanta();
	    		plantaSeleccionada = BusinessDelegatorView.getTsccvPlanta(somPlanta.getValue().toString());
	    		//se setea el valor "0" para diferenciar las plantas que estan en memoria de las que esta en la BD 
	    		plantaSeleccionada.setPlanta("e");
	    		plantaUsuario.setTsccvPlanta(plantaSeleccionada);
	    		
	    		listadoPlantasUsuaio.add(plantaUsuario);
	    	}else{
	    		//FacesContext.getCurrentInstance().addMessage("", new FacesMessage("Favor selecciona una Planta valida"));
	    		MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
	          	mensaje.mostrarMensaje("Por favor seleccione una Planta valida", MBMensaje.MENSAJE_ALERTA);
	    	}
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    	
		somPlanta.setValue(new Long(-1));
		somPlanta.setDisabled(false);
    }
    
    //metodo que elimina las plantas de la lista en memoria o de la BD
    public void eliminarPlanta(ActionEvent event){
    	try {    		
    		TsccvPlantasUsuario plantaUsua = (TsccvPlantasUsuario) event.getComponent().getAttributes().get("plantaUsua");
    		if(plantaUsua.getTsccvPlanta().getPlanta() != null){  
    			listadoPlantasUsuaio.remove(plantaUsua);
    		}
    		else {
	    		if(new Long(somUsuario.getValue().toString()) != -1) {
	    			txtIdUsua.setValue(somUsuario.getValue());
	    			txtCodpla.setValue(plantaUsua.getTsccvPlanta().getCodpla());	    			
		        	
	    			BusinessDelegatorView.deleteTsccvPlantasUsuario((((txtCodpla.getValue()) == null) ||
	    	                (txtCodpla.getValue()).equals("")) ? null
	    	                                                   : new String(
	    	                    txtCodpla.getValue().toString()),
	    	                (((txtIdUsua.getValue()) == null) ||
	    	                (txtIdUsua.getValue()).equals("")) ? null
	    	                                                   : new Integer(
	    	                    txtIdUsua.getValue().toString()));
	    			
		            consultarPlantasPorUsuario(null);
				}
				else {
					MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
		          	mensaje.mostrarMensaje("Debe seleccionar un Usuario", MBMensaje.MENSAJE_ALERTA);
	    		}
    		}    		
    	} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    }
	
	
}
