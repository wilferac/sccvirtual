package co.com.smurfit.presentation.backEndBeans;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.BvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.BvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.BvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.CarteraDivLogic;
import co.com.smurfit.sccvirtual.control.CarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.ClasesDocLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpDetPedLogic;
import co.com.smurfit.sccvirtual.control.IBvpDirDspLogic;
import co.com.smurfit.sccvirtual.control.IBvpHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ICarteraDivLogic;
import co.com.smurfit.sccvirtual.control.ICarteraResumenLogic;
import co.com.smurfit.sccvirtual.control.IClasesDocLogic;
import co.com.smurfit.sccvirtual.control.ISapDetDspLogic;
import co.com.smurfit.sccvirtual.control.ISapDetPedLogic;
import co.com.smurfit.sccvirtual.control.ISapDirDspLogic;
import co.com.smurfit.sccvirtual.control.ISapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapUmedidaLogic;
import co.com.smurfit.sccvirtual.control.SapDetDspLogic;
import co.com.smurfit.sccvirtual.control.SapDetPedLogic;
import co.com.smurfit.sccvirtual.control.SapDirDspLogic;
import co.com.smurfit.sccvirtual.control.SapHistDespachosLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.SapUmedidaLogic;
import co.com.smurfit.sccvirtual.dto.BvpDetPedDTO;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.utilities.DataPage;
import co.com.smurfit.utilities.DataSource;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.PagedListDataModel;
import co.com.smurfit.utilities.Utilities;

import com.icesoft.faces.async.render.RenderManager;
import com.icesoft.faces.async.render.Renderable;
import com.icesoft.faces.component.ext.HtmlCommandButton;
import com.icesoft.faces.component.ext.HtmlInputText;
import com.icesoft.faces.component.ext.HtmlSelectOneMenu;
import com.icesoft.faces.component.selectinputdate.SelectInputDate;
import com.icesoft.faces.context.DisposableBean;
import com.icesoft.faces.webapp.xmlhttp.FatalRenderingException;
import com.icesoft.faces.webapp.xmlhttp.PersistentFacesState;
import com.icesoft.faces.webapp.xmlhttp.RenderingException;
import com.icesoft.faces.webapp.xmlhttp.TransientRenderingException;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.Vector;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */

//Pantalla consulta de pedidos
public class BvpDetPedView extends DataSource implements Renderable,
    DisposableBean {
    private HtmlInputText txtPlanta;
    private HtmlInputText txtNumped;
    private HtmlInputText txtItmped;
    private HtmlInputText txtCodcli;
    private HtmlInputText txtCodscc;
    private HtmlInputText txtCodpro;
    private HtmlInputText txtTippro;
    private HtmlInputText txtDespro;
    private HtmlInputText txtFecsol;
    private HtmlInputText txtFecdsp;
    private HtmlInputText txtEstado;
    private HtmlInputText txtUndmed;
    private HtmlInputText txtCntped;
    private HtmlInputText txtCntdsp;
    private HtmlInputText txtOrdcom;
    private SelectInputDate txtFechaInicial;
    private SelectInputDate txtFechaFinal;
    private HtmlCommandButton btnSave;
    private HtmlCommandButton btnModify;
    private HtmlCommandButton btnDelete;
    private HtmlCommandButton btnClear;
    private boolean renderDataTable;
    private boolean flag = true;
    private RenderManager renderManager;
    private PersistentFacesState state = PersistentFacesState.getInstance();
    private List<BvpDetPed> bvpDetPed;
    private List<BvpDetPedDTO> bvpDetPedDTO;
    
    private TsccvPlanta tsccvPlanta;
    private TsccvEmpresas tsccvEmpresas;
    
    private boolean ascendente;//utilizado para el orden por los campos en la tabla

    public BvpDetPedView() {
        super("");        
    }

    public String action_clear() {
        /*txtPlanta.setValue(null);
        txtPlanta.setDisabled(true);
        txtNumped.setValue(null);
        txtNumped.setDisabled(true);
        txtItmped.setValue(null);
        txtItmped.setDisabled(true);
        txtCodcli.setValue(null);
        txtCodcli.setDisabled(true);
        txtCodscc.setValue(null);
        txtCodscc.setDisabled(true);
        txtCodpro.setValue(null);
        txtCodpro.setDisabled(true);
        txtTippro.setValue(null);
        txtTippro.setDisabled(true);
        txtDespro.setValue(null);
        txtDespro.setDisabled(true);
        txtFecsol.setValue(null);
        txtFecsol.setDisabled(true);
        txtFecdsp.setValue(null);
        txtFecdsp.setDisabled(true);
        txtEstado.setValue(null);
        txtEstado.setDisabled(true);
        txtUndmed.setValue(null);
        txtUndmed.setDisabled(true);
        txtCntped.setValue(null);
        txtCntped.setDisabled(true);
        txtCntdsp.setValue(null);
        txtCntdsp.setDisabled(true);
        txtOrdcom.setValue(null);
        txtOrdcom.setDisabled(true);

        btnSave.setDisabled(false);
        btnDelete.setDisabled(true);
        btnModify.setDisabled(true);
        btnClear.setDisabled(false);*/
    	
    	txtNumped.setValue(null);
    	txtOrdcom.setValue(null);
    	txtCodcli.setValue(null);

        return "";
    }

    public void listener_txtId(ValueChangeEvent event) {
        if ((event.getNewValue() != null) && !event.getNewValue().equals("")) {
            BvpDetPed entity = null;

            try {
                BvpDetPedId id = new BvpDetPedId();
                id.setPlanta((((txtPlanta.getValue()) == null) ||
                    (txtPlanta.getValue()).equals("")) ? null
                                                       : new String(
                        txtPlanta.getValue().toString()));
                id.setNumped((((txtNumped.getValue()) == null) ||
                    (txtNumped.getValue()).equals("")) ? null
                                                       : new String(
                        txtNumped.getValue().toString()));
                id.setItmped((((txtItmped.getValue()) == null) ||
                    (txtItmped.getValue()).equals("")) ? null
                                                       : new String(
                        txtItmped.getValue().toString()));
                id.setCodcli((((txtCodcli.getValue()) == null) ||
                    (txtCodcli.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodcli.getValue().toString()));
                id.setCodscc((((txtCodscc.getValue()) == null) ||
                    (txtCodscc.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodscc.getValue().toString()));
                id.setCodpro((((txtCodpro.getValue()) == null) ||
                    (txtCodpro.getValue()).equals("")) ? null
                                                       : new String(
                        txtCodpro.getValue().toString()));
                id.setTippro((((txtTippro.getValue()) == null) ||
                    (txtTippro.getValue()).equals("")) ? null
                                                       : new String(
                        txtTippro.getValue().toString()));
                id.setDespro((((txtDespro.getValue()) == null) ||
                    (txtDespro.getValue()).equals("")) ? null
                                                       : new String(
                        txtDespro.getValue().toString()));
                id.setFecsol((((txtFecsol.getValue()) == null) ||
                    (txtFecsol.getValue()).equals("")) ? null
                                                       : new String(
                        txtFecsol.getValue().toString()));
                id.setFecdsp((((txtFecdsp.getValue()) == null) ||
                    (txtFecdsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtFecdsp.getValue().toString()));
                id.setEstado((((txtEstado.getValue()) == null) ||
                    (txtEstado.getValue()).equals("")) ? null
                                                       : new String(
                        txtEstado.getValue().toString()));
                id.setUndmed((((txtUndmed.getValue()) == null) ||
                    (txtUndmed.getValue()).equals("")) ? null
                                                       : new String(
                        txtUndmed.getValue().toString()));
                id.setCntped((((txtCntped.getValue()) == null) ||
                    (txtCntped.getValue()).equals("")) ? null
                                                       : new String(
                        txtCntped.getValue().toString()));
                id.setCntdsp((((txtCntdsp.getValue()) == null) ||
                    (txtCntdsp.getValue()).equals("")) ? null
                                                       : new String(
                        txtCntdsp.getValue().toString()));
                id.setOrdcom((((txtOrdcom.getValue()) == null) ||
                    (txtOrdcom.getValue()).equals("")) ? null
                                                       : new String(
                        txtOrdcom.getValue().toString()));

                entity = BusinessDelegatorView.getBvpDetPed(id);
            } catch (Exception e) {
                // TODO: handle exception
            }

            if (entity == null) {
                txtPlanta.setDisabled(false);
                txtNumped.setDisabled(false);
                txtItmped.setDisabled(false);
                txtCodcli.setDisabled(false);
                txtCodscc.setDisabled(false);
                txtCodpro.setDisabled(false);
                txtTippro.setDisabled(false);
                txtDespro.setDisabled(false);
                txtFecsol.setDisabled(false);
                txtFecdsp.setDisabled(false);
                txtEstado.setDisabled(false);
                txtUndmed.setDisabled(false);
                txtCntped.setDisabled(false);
                txtCntdsp.setDisabled(false);
                txtOrdcom.setDisabled(false);

                btnSave.setDisabled(false);
                btnDelete.setDisabled(true);
                btnModify.setDisabled(true);
                btnClear.setDisabled(false);
            } else {
                txtPlanta.setValue(entity.getId().getPlanta());
                txtPlanta.setDisabled(true);
                txtNumped.setValue(entity.getId().getNumped());
                txtNumped.setDisabled(true);
                txtItmped.setValue(entity.getId().getItmped());
                txtItmped.setDisabled(true);
                txtCodcli.setValue(entity.getId().getCodcli());
                txtCodcli.setDisabled(true);
                txtCodscc.setValue(entity.getId().getCodscc());
                txtCodscc.setDisabled(true);
                txtCodpro.setValue(entity.getId().getCodpro());
                txtCodpro.setDisabled(true);
                txtTippro.setValue(entity.getId().getTippro());
                txtTippro.setDisabled(true);
                txtDespro.setValue(entity.getId().getDespro());
                txtDespro.setDisabled(true);
                txtFecsol.setValue(entity.getId().getFecsol());
                txtFecsol.setDisabled(true);
                txtFecdsp.setValue(entity.getId().getFecdsp());
                txtFecdsp.setDisabled(true);
                txtEstado.setValue(entity.getId().getEstado());
                txtEstado.setDisabled(true);
                txtUndmed.setValue(entity.getId().getUndmed());
                txtUndmed.setDisabled(true);
                txtCntped.setValue(entity.getId().getCntped());
                txtCntped.setDisabled(true);
                txtCntdsp.setValue(entity.getId().getCntdsp());
                txtCntdsp.setDisabled(true);
                txtOrdcom.setValue(entity.getId().getOrdcom());
                txtOrdcom.setDisabled(true);

                btnSave.setDisabled(true);
                btnDelete.setDisabled(false);
                btnModify.setDisabled(false);
                btnClear.setDisabled(false);
            }
        }
    }

    public String action_save() {
        try {
            BusinessDelegatorView.saveBvpDetPed((((txtPlanta.getValue()) == null) ||
                (txtPlanta.getValue()).equals("")) ? null
                                                   : new String(
                    txtPlanta.getValue().toString()),
                (((txtNumped.getValue()) == null) ||
                (txtNumped.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumped.getValue().toString()),
                (((txtItmped.getValue()) == null) ||
                (txtItmped.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmped.getValue().toString()),
                (((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtCodscc.getValue()) == null) ||
                (txtCodscc.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodscc.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtTippro.getValue()) == null) ||
                (txtTippro.getValue()).equals("")) ? null
                                                   : new String(
                    txtTippro.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtFecsol.getValue()) == null) ||
                (txtFecsol.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecsol.getValue().toString()),
                (((txtFecdsp.getValue()) == null) ||
                (txtFecdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecdsp.getValue().toString()),
                (((txtEstado.getValue()) == null) ||
                (txtEstado.getValue()).equals("")) ? null
                                                   : new String(
                    txtEstado.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtCntped.getValue()) == null) ||
                (txtCntped.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntped.getValue().toString()),
                (((txtCntdsp.getValue()) == null) ||
                (txtCntdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntdsp.getValue().toString()),
                (((txtOrdcom.getValue()) == null) ||
                (txtOrdcom.getValue()).equals("")) ? null
                                                   : new String(
                    txtOrdcom.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYSAVED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_delete() {
        try {
            BusinessDelegatorView.deleteBvpDetPed((((txtPlanta.getValue()) == null) ||
                (txtPlanta.getValue()).equals("")) ? null
                                                   : new String(
                    txtPlanta.getValue().toString()),
                (((txtNumped.getValue()) == null) ||
                (txtNumped.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumped.getValue().toString()),
                (((txtItmped.getValue()) == null) ||
                (txtItmped.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmped.getValue().toString()),
                (((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtCodscc.getValue()) == null) ||
                (txtCodscc.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodscc.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtTippro.getValue()) == null) ||
                (txtTippro.getValue()).equals("")) ? null
                                                   : new String(
                    txtTippro.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtFecsol.getValue()) == null) ||
                (txtFecsol.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecsol.getValue().toString()),
                (((txtFecdsp.getValue()) == null) ||
                (txtFecdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecdsp.getValue().toString()),
                (((txtEstado.getValue()) == null) ||
                (txtEstado.getValue()).equals("")) ? null
                                                   : new String(
                    txtEstado.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtCntped.getValue()) == null) ||
                (txtCntped.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntped.getValue().toString()),
                (((txtCntdsp.getValue()) == null) ||
                (txtCntdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntdsp.getValue().toString()),
                (((txtOrdcom.getValue()) == null) ||
                (txtOrdcom.getValue()).equals("")) ? null
                                                   : new String(
                    txtOrdcom.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYDELETED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modify() {
        try {
            BusinessDelegatorView.updateBvpDetPed((((txtPlanta.getValue()) == null) ||
                (txtPlanta.getValue()).equals("")) ? null
                                                   : new String(
                    txtPlanta.getValue().toString()),
                (((txtNumped.getValue()) == null) ||
                (txtNumped.getValue()).equals("")) ? null
                                                   : new String(
                    txtNumped.getValue().toString()),
                (((txtItmped.getValue()) == null) ||
                (txtItmped.getValue()).equals("")) ? null
                                                   : new String(
                    txtItmped.getValue().toString()),
                (((txtCodcli.getValue()) == null) ||
                (txtCodcli.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodcli.getValue().toString()),
                (((txtCodscc.getValue()) == null) ||
                (txtCodscc.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodscc.getValue().toString()),
                (((txtCodpro.getValue()) == null) ||
                (txtCodpro.getValue()).equals("")) ? null
                                                   : new String(
                    txtCodpro.getValue().toString()),
                (((txtTippro.getValue()) == null) ||
                (txtTippro.getValue()).equals("")) ? null
                                                   : new String(
                    txtTippro.getValue().toString()),
                (((txtDespro.getValue()) == null) ||
                (txtDespro.getValue()).equals("")) ? null
                                                   : new String(
                    txtDespro.getValue().toString()),
                (((txtFecsol.getValue()) == null) ||
                (txtFecsol.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecsol.getValue().toString()),
                (((txtFecdsp.getValue()) == null) ||
                (txtFecdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtFecdsp.getValue().toString()),
                (((txtEstado.getValue()) == null) ||
                (txtEstado.getValue()).equals("")) ? null
                                                   : new String(
                    txtEstado.getValue().toString()),
                (((txtUndmed.getValue()) == null) ||
                (txtUndmed.getValue()).equals("")) ? null
                                                   : new String(
                    txtUndmed.getValue().toString()),
                (((txtCntped.getValue()) == null) ||
                (txtCntped.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntped.getValue().toString()),
                (((txtCntdsp.getValue()) == null) ||
                (txtCntdsp.getValue()).equals("")) ? null
                                                   : new String(
                    txtCntdsp.getValue().toString()),
                (((txtOrdcom.getValue()) == null) ||
                (txtOrdcom.getValue()).equals("")) ? null
                                                   : new String(
                    txtOrdcom.getValue().toString()));

            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String planta, String numped,
        String itmped, String codcli, String codscc, String codpro,
        String tippro, String despro, String fecsol, String fecdsp,
        String estado, String undmed, String cntped, String cntdsp,
        String ordcom) throws Exception {
        try {
            BusinessDelegatorView.updateBvpDetPed(planta, numped, itmped,
                codcli, codscc, codpro, tippro, despro, fecsol, fecdsp, estado,
                undmed, cntped, cntdsp, ordcom);
            renderManager.getOnDemandRenderer("BvpDetPedView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("",
                new FacesMessage(ExceptionMessages.ENTITY_SUCCESFULLYMODIFIED));
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("BvpDetPedView").requestRender();
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
            throw e;
        }

        return "";
    }

    public List<BvpDetPed> getBvpDetPed() {
        if (flag) {
            try {
                bvpDetPed = BusinessDelegatorView.getBvpDetPed();
                flag = false;
            } catch (Exception e) {
                flag = true;
                FacesContext.getCurrentInstance()
                            .addMessage("", new FacesMessage(e.getMessage()));
            }
        }

        return bvpDetPed;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setBvpDetPed(List<BvpDetPed> bvpDetPed) {
        this.bvpDetPed = bvpDetPed;
    }

    public boolean isRenderDataTable() {
        try {
            if (flag) {
                if (BusinessDelegatorView.findTotalNumberBvpDetPed() > 0) {
                    renderDataTable = true;
                } else {
                    renderDataTable = false;
                }
            }

            flag = false;
        } catch (Exception e) {
            renderDataTable = false;
            e.printStackTrace();
        }

        return renderDataTable;
    }

    public DataModel getData() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModel(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<BvpDetPed> getDataPage(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        
    	//se comenta para que cuando se utilice la funcion de oreden enla tabla no se muestren los registros no filtrados
    	/*int totalNumberBvpDetPed = 0;

        try {
            totalNumberBvpDetPed = BusinessDelegatorView.findTotalNumberBvpDetPed()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberBvpDetPed) {
            endIndex = totalNumberBvpDetPed;
        }*/

        try {
        	//tambien se comenta esta consulta
            /*bvpDetPed = BusinessDelegatorView.findPageBvpDetPed(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);*/
            
            
        	if(txtNumped == null){//si la pantalla no se ha cargado se detiene la ejecucion del metodo.
        		return new DataPage<BvpDetPed>(0, 0, bvpDetPed);
        	}
        	
        	bvpDetPed = BusinessDelegatorView.consultarPedidosBvp((((txtNumped.getValue()) == null) ||
					(txtNumped.getValue()).equals("")) ? null
					                            : new String(
					txtNumped.getValue().toString()),
					(((txtOrdcom.getValue()) == null) ||
					(txtOrdcom.getValue()).equals("")) ? null
					                            : new String(
            		txtOrdcom.getValue().toString()),
            		tsccvEmpresas,//codigo cliente
					(((txtFechaInicial.getValue()) == null) ||
					(txtFechaInicial.getValue()).equals("")) ? null
					                      		: (Date)(txtFechaInicial.getValue()),
					(((txtFechaFinal.getValue()) == null) ||
					(txtFechaFinal.getValue()).equals("")) ? null
					                            : (Date)(txtFechaFinal.getValue()),
					(((txtCodpro.getValue()) == null) ||//codigo producto cliente
					(txtCodpro.getValue()).equals("")) ? null
					                            : new String(
            		txtCodpro.getValue().toString()),
					tsccvPlanta, sortColumnName, sortAscending);            

            // Remove this Renderable from the existing render groups.
            //leaveRenderGroups();        			
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        //joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        
        //return new DataPage<BvpDetPed>(totalNumberBvpDetPed, startRow, bvpDetPed);
        return new DataPage<BvpDetPed>(bvpDetPed.size(), startRow, bvpDetPed);
    }

    public DataModel getDataDTO() {
        state = PersistentFacesState.getInstance();

        if (onePageDataModel == null) {
            onePageDataModel = new LocalDataModelDTO(pageSize);
        }

        return onePageDataModel;
    }

    /**
     * This is where the Object data is retrieved from the database and
     * returned as a list of objects for display in the UI.
     */
    private DataPage<BvpDetPedDTO> getDataPageDTO(int startRow, int pageSize) {
        // Retrieve the total number of objects from the database.  This
        // number is required by the DataPage object so the paginator will know
        // the relative location of the page data.
        int totalNumberBvpDetPed = 0;

        try {
            totalNumberBvpDetPed = BusinessDelegatorView.findTotalNumberBvpDetPed()
                                                        .intValue();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Calculate indices to be displayed in the ui.
        int endIndex = startRow + pageSize;

        if (endIndex > totalNumberBvpDetPed) {
            endIndex = totalNumberBvpDetPed;
        }

        try {
            bvpDetPed = BusinessDelegatorView.findPageBvpDetPed(sortColumnName,
                    sortAscending, startRow, endIndex - startRow);

            if (Utilities.validationsList(bvpDetPed)) {
                bvpDetPedDTO = new ArrayList<BvpDetPedDTO>();

                for (BvpDetPed bvpDetPedTmp : bvpDetPed) {
                    BvpDetPedDTO bvpDetPedDTO2 = new BvpDetPedDTO();
                    bvpDetPedDTO2.setPlanta(bvpDetPedTmp.getId().getPlanta()
                                                        .toString());
                    bvpDetPedDTO2.setNumped(bvpDetPedTmp.getId().getNumped()
                                                        .toString());
                    bvpDetPedDTO2.setItmped(bvpDetPedTmp.getId().getItmped()
                                                        .toString());
                    bvpDetPedDTO2.setCodcli(bvpDetPedTmp.getId().getCodcli()
                                                        .toString());
                    bvpDetPedDTO2.setCodscc(bvpDetPedTmp.getId().getCodscc()
                                                        .toString());
                    bvpDetPedDTO2.setCodpro(bvpDetPedTmp.getId().getCodpro()
                                                        .toString());
                    bvpDetPedDTO2.setTippro(bvpDetPedTmp.getId().getTippro()
                                                        .toString());
                    bvpDetPedDTO2.setDespro(bvpDetPedTmp.getId().getDespro()
                                                        .toString());
                    bvpDetPedDTO2.setFecsol(bvpDetPedTmp.getId().getFecsol()
                                                        .toString());
                    bvpDetPedDTO2.setFecdsp(bvpDetPedTmp.getId().getFecdsp()
                                                        .toString());
                    bvpDetPedDTO2.setEstado(bvpDetPedTmp.getId().getEstado()
                                                        .toString());
                    bvpDetPedDTO2.setUndmed(bvpDetPedTmp.getId().getUndmed()
                                                        .toString());
                    bvpDetPedDTO2.setCntped(bvpDetPedTmp.getId().getCntped()
                                                        .toString());
                    bvpDetPedDTO2.setCntdsp(bvpDetPedTmp.getId().getCntdsp()
                                                        .toString());
                    bvpDetPedDTO2.setOrdcom(bvpDetPedTmp.getId().getOrdcom()
                                                        .toString());

                    bvpDetPedDTO2.setBvpDetPed(bvpDetPedTmp);
                    bvpDetPedDTO2.setBvpDetPedView(this);
                    bvpDetPedDTO.add(bvpDetPedDTO2);
                }
            }

            // Remove this Renderable from the existing render groups.
            leaveRenderGroups();
        } catch (Exception e) {
            FacesContext.getCurrentInstance()
                        .addMessage("", new FacesMessage(e.getMessage()));
        }

        // Add this Renderable to the new render groups.
        joinRenderGroups();

        // Reset the dirtyData flag.
        onePageDataModel.setDirtyData(false);

        // This is required when using Hibernate JPA.  If the EntityManager is not
        // cleared or closed objects are cached and stale objects will show up
        // in the table.
        // This way, the detached objects are reread from the database.
        // This call is not required with TopLink JPA, which uses a Query Hint
        // to clear the l2 cache in the DAO.
        //EntityManagerHelper.getEntityManager().clear();
        flag = true;

        return new DataPage<BvpDetPedDTO>(totalNumberBvpDetPed, startRow,
            bvpDetPedDTO);
    }

    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    /**
     * This method is called when a render call is made from the server. Render
     * calls are only made to views containing an updated record. The data is
     * marked as dirty to trigger a fetch of the updated record from the
     * database before rendering takes place.
     */
    public PersistentFacesState getState() {
        onePageDataModel.setDirtyData();

        return state;
    }

    /**
     * This method is called from faces-config.xml with each new session.
     */
    public void setRenderManager(RenderManager renderManager) {
        this.renderManager = renderManager;
    }

    public void renderingException(RenderingException arg0) {
        if (arg0 instanceof TransientRenderingException) {
        } else if (arg0 instanceof FatalRenderingException) {
            // Remove from existing Customer render groups.
            leaveRenderGroups();
        }
    }

    /**
     * Remove this Renderable from existing uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void leaveRenderGroups() {
        if (Utilities.validationsList(bvpDetPedDTO)) {
            for (BvpDetPedDTO bvpDetPedTmp : bvpDetPedDTO) {
                renderManager.getOnDemandRenderer("BvpDetPedView").remove(this);
            }
        }
    }

    /**
     * Add this Renderable to the new uiCustomerBeans render groups.
     * OnDemandRenderers are named/created using the underlying Customer Number.
     */
    private void joinRenderGroups() {
        if (Utilities.validationsList(bvpDetPedDTO)) {
            for (BvpDetPedDTO bvpDetPedTmp : bvpDetPedDTO) {
                renderManager.getOnDemandRenderer("BvpDetPedView").add(this);
            }
        }
    }

    @Override
    public void dispose() throws Exception {
        // TODO Auto-generated method stub
    }

    public RenderManager getRenderManager() {
        return renderManager;
    }

    public void setState(PersistentFacesState state) {
        this.state = state;
    }

    public HtmlInputText getTxtPlanta() {
        return txtPlanta;
    }

    public void setTxtPlanta(HtmlInputText txtPlanta) {
        this.txtPlanta = txtPlanta;
    }

    public HtmlInputText getTxtNumped() {
        return txtNumped;
    }

    public void setTxtNumped(HtmlInputText txtNumped) {
        this.txtNumped = txtNumped;
    }

    public HtmlInputText getTxtItmped() {
        return txtItmped;
    }

    public void setTxtItmped(HtmlInputText txtItmped) {
        this.txtItmped = txtItmped;
    }

    public HtmlInputText getTxtCodcli() {
        return txtCodcli;
    }

    public void setTxtCodcli(HtmlInputText txtCodcli) {
        this.txtCodcli = txtCodcli;
    }

    public HtmlInputText getTxtCodscc() {
        return txtCodscc;
    }

    public void setTxtCodscc(HtmlInputText txtCodscc) {
        this.txtCodscc = txtCodscc;
    }

    public HtmlInputText getTxtCodpro() {
        return txtCodpro;
    }

    public void setTxtCodpro(HtmlInputText txtCodpro) {
        this.txtCodpro = txtCodpro;
    }

    public HtmlInputText getTxtTippro() {
        return txtTippro;
    }

    public void setTxtTippro(HtmlInputText txtTippro) {
        this.txtTippro = txtTippro;
    }

    public HtmlInputText getTxtDespro() {
        return txtDespro;
    }

    public void setTxtDespro(HtmlInputText txtDespro) {
        this.txtDespro = txtDespro;
    }

    public HtmlInputText getTxtFecsol() {
        return txtFecsol;
    }

    public void setTxtFecsol(HtmlInputText txtFecsol) {
        this.txtFecsol = txtFecsol;
    }

    public HtmlInputText getTxtFecdsp() {
        return txtFecdsp;
    }

    public void setTxtFecdsp(HtmlInputText txtFecdsp) {
        this.txtFecdsp = txtFecdsp;
    }

    public HtmlInputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(HtmlInputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public HtmlInputText getTxtUndmed() {
        return txtUndmed;
    }

    public void setTxtUndmed(HtmlInputText txtUndmed) {
        this.txtUndmed = txtUndmed;
    }

    public HtmlInputText getTxtCntped() {
        return txtCntped;
    }

    public void setTxtCntped(HtmlInputText txtCntped) {
        this.txtCntped = txtCntped;
    }

    public HtmlInputText getTxtCntdsp() {
        return txtCntdsp;
    }

    public void setTxtCntdsp(HtmlInputText txtCntdsp) {
        this.txtCntdsp = txtCntdsp;
    }

    public HtmlInputText getTxtOrdcom() {
        return txtOrdcom;
    }

    public void setTxtOrdcom(HtmlInputText txtOrdcom) {
        this.txtOrdcom = txtOrdcom;
    }

    public HtmlCommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(HtmlCommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public HtmlCommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(HtmlCommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public HtmlCommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(HtmlCommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public HtmlCommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(HtmlCommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public void setRenderDataTable(boolean renderDataTable) {
        this.renderDataTable = renderDataTable;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<BvpDetPedDTO> getBvpDetPedDTO() {
        return bvpDetPedDTO;
    }

    public void setBvpDetPedDTO(List<BvpDetPedDTO> bvpDetPedDTO) {
        this.bvpDetPedDTO = bvpDetPedDTO;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }
    
    
    

	public SelectInputDate getTxtFechaInicial() {
		return txtFechaInicial;
	}

	public void setTxtFechaInicial(SelectInputDate txtFechaInicial) {
		this.txtFechaInicial = txtFechaInicial;
	}

	public SelectInputDate getTxtFechaFinal() {
		return txtFechaFinal;
	}

	public void setTxtFechaFinal(SelectInputDate txtFechaFinal) {
		this.txtFechaFinal = txtFechaFinal;
	}

	public TsccvPlanta getTsccvPlanta() {
		return tsccvPlanta;
	}

	public void setTsccvPlanta(TsccvPlanta tsccvPlanta) {
		this.tsccvPlanta = tsccvPlanta;
	}

	public TsccvEmpresas getTsccvEmpresas() {
		return tsccvEmpresas;
	}

	public void setTsccvEmpresas(TsccvEmpresas tsccvEmpresas) {
		this.tsccvEmpresas = tsccvEmpresas;
	}

	public boolean isAscendente() {
		return ascendente;
	}

	public void setAscendente(boolean ascendente) {
		this.ascendente = ascendente;
	}
	

    /**
     * A special type of JSF DataModel to allow a datatable and datapaginator
     * to page through a large set of data without having to hold the entire
     * set of data in memory at once.
     * Any time a managed bean wants to avoid holding an entire dataset,
     * the managed bean declares this inner class which extends PagedListDataModel
     * and implements the fetchData method. fetchData is called
     * as needed when the table requires data that isn't available in the
     * current data page held by this object.
     * This requires the managed bean (and in general the business
     * method that the managed bean uses) to provide the data wrapped in
     * a DataPage object that provides info on the full size of the dataset.
     */
    private class LocalDataModel extends PagedListDataModel {
        public LocalDataModel(int pageSize) {
            super(pageSize);
        }

        public DataPage<BvpDetPed> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPage(startRow, pageSize);
        }
    }

    private class LocalDataModelDTO extends PagedListDataModel {
        public LocalDataModelDTO(int pageSize) {
            super(pageSize);
        }

        public DataPage<BvpDetPedDTO> fetchPage(int startRow, int pageSize) {
            // call enclosing managed bean method to fetch the data
            return getDataPageDTO(startRow, pageSize);
        }
    }
    
    //metodo agregado para consultar los pedidos de acuerdo a la informacion ingresada
    public void consultarPedidosBvp(){
    	List<BvpDetPed> registros=null;
    	try {    		
    		registros = BusinessDelegatorView.consultarPedidosBvp((((txtNumped.getValue()) == null) ||
					(txtNumped.getValue()).equals("")) ? null
                            : new String(
					txtNumped.getValue().toString()),
					(((txtOrdcom.getValue()) == null) ||
					(txtOrdcom.getValue()).equals("")) ? null
					                            : new String(
					txtOrdcom.getValue().toString()), 
					tsccvEmpresas,//cliente
					(((txtFechaInicial.getValue()) == null) ||
					(txtFechaInicial.getValue()).equals("")) ? null
					                      		: (Date)(txtFechaInicial.getValue()),
					(((txtFechaFinal.getValue()) == null) ||
					(txtFechaFinal.getValue()).equals("")) ? null
					                            : (Date)(txtFechaFinal.getValue()),
					(((txtCodpro.getValue()) == null) ||//codigo producto cliente
					(txtCodpro.getValue()).equals("")) ? null
					                            : new String(
					txtCodpro.getValue().toString()),
					tsccvPlanta, FacesUtils.getRequestParameter("nombreColumna"), (ascendente = !ascendente));
            
            if(registros == null || registros.size() == 0){            	
            	MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
              	mensaje.mostrarMensaje("Para esta Planta no hay pedidos relacionados.", MBMensaje.MENSAJE_ALERTA);
            	
              	/*onePageDataModel.setDirtyData(false);
              	flag = false;
              	onePageDataModel.setPage(new DataPage<BvpDetPed>(0, 0, registros));*/
              	bvpDetPed = new ArrayList<BvpDetPed>();
              	return;
            }
            
            /*onePageDataModel.setDirtyData(false);
            flag = true;
            onePageDataModel.setPage(new DataPage<BvpDetPed>(registros.size(), 0, registros));*/
            bvpDetPed = registros;
            
		} catch(BMBaseException e){
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(e);
			
			return;
			/*FacesContext.getCurrentInstance()
			            .addMessage("", new FacesMessage(e.getMessage()));*/
		}
		catch(Exception e){
			BMBaseException be= new BMBaseException(e);
			MBMensaje mensaje = (MBMensaje) FacesUtils.getManagedBean("MBMensaje");
			mensaje.mostrarMensaje(be);
			
			return;
		}
    	
    	return;
    }
    
    //metodo encargado de hacer el llamado a la pantalla que muestra el detalle despacho 
    public String detalleDespacho(){
    	//se obtiene la sesion y se setea el atributo con el numero del pedido
    	HttpSession session= (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    	session.setAttribute("txtNumped", FacesUtils.getRequestParameter("txtNumped").toString());
    	session.setAttribute("txtItmped", FacesUtils.getRequestParameter("txtItmped").toString());
    	session.setAttribute("codPla", tsccvPlanta.getCodpla());
    	session.setAttribute("codVpe", tsccvEmpresas.getCodVpe());
    	
    	return "goDetalleDespachoBvp";
    }

}
