package co.com.smurfit.sccvirtual.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ArchivoPropiedades {
	private String archivo;
	
	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public String getProperty(String name) throws Exception{
		Properties pro=null;
		InputStream input=null;
		String valor=null;
	    try {
	    	pro= new Properties();
	    	//pro.load(this.getClass().getResource("pruebas.properties").openStream());
	    	if (archivo == null){
	    		input=this.getClass().getResource("parametros.properties").openStream();
	    		pro.load(input);
	    	}
	    	else{
	    		input=this.getClass().getResource(archivo+".properties").openStream();
	    		pro.load(input);
	    	}
	    	input.close();
	    	valor= pro.getProperty(name);
	    	pro.clear();
	    	return valor;
	    } catch (IOException ex) {
	    	System.out.println("Error leyendo propiedad por: "+ex.getMessage());
	    	throw ex;
	    }
	}
	
	
}
