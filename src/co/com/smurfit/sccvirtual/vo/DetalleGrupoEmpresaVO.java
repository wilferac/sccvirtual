package co.com.smurfit.sccvirtual.vo;

import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;

public class DetalleGrupoEmpresaVO implements java.io.Serializable{
	private TsccvDetalleGrupoEmpresa tsccvDetalleGrupoEmpresa;
	private Boolean seleccionada;
	
	
	public DetalleGrupoEmpresaVO() {
		super();
		seleccionada = false;
	}

	public DetalleGrupoEmpresaVO(TsccvDetalleGrupoEmpresa tsccvDetalleGrupoEmpresa, Boolean seleccionada) {
		super();
		this.tsccvDetalleGrupoEmpresa = tsccvDetalleGrupoEmpresa;
		this.seleccionada = seleccionada;
	}


	public TsccvDetalleGrupoEmpresa getTsccvDetalleGrupoEmpresa() {
		return tsccvDetalleGrupoEmpresa;
	}

	public void setTsccvDetalleGrupoEmpresa(TsccvDetalleGrupoEmpresa tsccvDetalleGrupoEmpresa) {
		this.tsccvDetalleGrupoEmpresa = tsccvDetalleGrupoEmpresa;
	}

	public Boolean getSeleccionada() {
		return seleccionada;
	}

	public void setSeleccionada(Boolean seleccionada) {
		this.seleccionada = seleccionada;
	}
}
