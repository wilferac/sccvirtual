package co.com.smurfit.sccvirtual.vo;

import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;

public class ProductosPedidoVO implements java.io.Serializable{
	/**
	 * 
	 */	
	private TsccvProductosPedido tsccvProductosPedido;
	private Integer numeroEntregas;
	/*private String undmed;
	private String moneda;
	
	//propiedades adicionadas para los productos de molinos
	private String eje;
	private String diamtr;*/
	
	public ProductosPedidoVO() {
		super();
		this.numeroEntregas = new Integer(0);
	}

	public TsccvProductosPedido getTsccvProductosPedido() {
		return tsccvProductosPedido;
	}

	public void setTsccvProductosPedido(TsccvProductosPedido tsccvProductosPedido) {
		this.tsccvProductosPedido = tsccvProductosPedido;
	}

	public Integer getNumeroEntregas() {
		return numeroEntregas;
	}

	public void setNumeroEntregas(Integer numeroEntregas) {
		this.numeroEntregas = numeroEntregas;
	}

	/*
	public String getUndmed() {
		return undmed;
	}

	public void setUndmed(String undmed) {
		this.undmed = undmed;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getEje() {
		return eje;
	}

	public void setEje(String eje) {
		this.eje = eje;
	}

	public String getDiamtr() {
		return diamtr;
	}

	public void setDiamtr(String diamtr) {
		this.diamtr = diamtr;
	}
	*/
}
