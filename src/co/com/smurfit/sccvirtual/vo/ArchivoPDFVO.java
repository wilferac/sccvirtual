package co.com.smurfit.sccvirtual.vo;

import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;

public class ArchivoPDFVO {
	
	private TsccvPdfsProductos pdf;
	private String nombreArchivo;
	private String estado;
	private String url;
	
	public ArchivoPDFVO(){
		
	}

	public TsccvPdfsProductos getPdf() {
		return pdf;
	}

	public void setPdf(TsccvPdfsProductos pdf) {
		this.pdf = pdf;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}	

}
