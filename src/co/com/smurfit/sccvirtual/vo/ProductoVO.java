package co.com.smurfit.sccvirtual.vo;

import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;

/*
 * Se crea con el fin de adicionar la propiedad seleccionado,
 * la cual se utiliza en la tabla al momento de seleccionar los productos  
 * */
public class ProductoVO implements java.io.Serializable{
	
	private BvpPrecio bvpPrecio;
	private SapPrecio sapPrecio;
	private Boolean seleccionado;
	//se adiciona esta propiedad ya que la pantalla pdfsFaltantes muestra el nombre de la empresa como una combinacion (cod_sap-nombre_empresa)
	private String nombreEmpresa;
	//utilizado en la pantalla de pdfs faltantes
	private String codSap;//(codigo smurfit) 
	//se agrega para la numeracion de los registros en la tabla de resultados
	private Long consecutivo;
	//propiedad utilizada en la pantalla lista precios
	private String nombreArchivo;
	
	
	public ProductoVO(BvpPrecio bvpPrecio, Boolean seleccionado) {
		super();
		this.bvpPrecio = bvpPrecio;
		this.seleccionado = seleccionado;
	}
	
	public ProductoVO(SapPrecio sapPrecio, Boolean seleccionado) {
		super();
		this.sapPrecio = sapPrecio;
		this.seleccionado = seleccionado;
	}

	public ProductoVO() {
		super();
	}


	public BvpPrecio getBvpPrecio() {
		return bvpPrecio;
	}

	public void setBvpPrecio(BvpPrecio bvpPrecio) {
		this.bvpPrecio = bvpPrecio;
	}

	public SapPrecio getSapPrecio() {
		return sapPrecio;
	}

	public void setSapPrecio(SapPrecio sapPrecio) {
		this.sapPrecio = sapPrecio;
	}

	public Boolean getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(Boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getCodSap() {
		return codSap;
	}

	public void setCodSap(String codSap) {
		this.codSap = codSap;
	}

	public Long getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(Long consecutivo) {
		this.consecutivo = consecutivo;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}	
	
	
}
