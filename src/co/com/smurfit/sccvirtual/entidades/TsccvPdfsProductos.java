package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvPdfsProductos entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_PDFS_PRODUCTOS", schema = "dbo")
public class TsccvPdfsProductos implements java.io.Serializable {

	// Fields

	private TsccvPdfsProductosId id;
	private TsccvPlanta tsccvPlanta;
	private Date fechaCreacion;

	// Constructors

	/** default constructor */
	public TsccvPdfsProductos() {
	}

	/** full constructor */
	public TsccvPdfsProductos(TsccvPdfsProductosId id, TsccvPlanta tsccvPlanta,
			Date fechaCreacion) {
		this.id = id;
		this.tsccvPlanta = tsccvPlanta;
		this.fechaCreacion = fechaCreacion;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codScc", column = @Column(name = "COD_SCC", nullable = false, length = 8)),
			@AttributeOverride(name = "codpla", column = @Column(name = "CODPLA", nullable = false, length = 2)) })
	public TsccvPdfsProductosId getId() {
		return this.id;
	}

	public void setId(TsccvPdfsProductosId id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "CODPLA", nullable = false, insertable = false, updatable = false)
	public TsccvPlanta getTsccvPlanta() {
		return this.tsccvPlanta;
	}

	public void setTsccvPlanta(TsccvPlanta tsccvPlanta) {
		this.tsccvPlanta = tsccvPlanta;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}