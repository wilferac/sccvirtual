package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvUsuario entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_USUARIO", schema = "dbo")
public class TsccvUsuario implements java.io.Serializable {

	// Fields

	private Integer idUsua;
	private TsccvEmpresas tsccvEmpresas;
	private TsccvPlanta tsccvPlanta;
	private TbmBaseGruposUsuarios tbmBaseGruposUsuarios;
	private String login;
	private String password;
	private String nombre;
	private String apellido;
	private Integer cedula;
	private String email;
	private String activo;
	private Date fechaCreacion;
	private Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds = new HashSet<TsccvEstadisticaAccPed>(
			0);
	private Set<TsccvPedidos> tsccvPedidoses = new HashSet<TsccvPedidos>(0);
	private Set<TsccvPlantasUsuario> tsccvPlantasUsuarios = new HashSet<TsccvPlantasUsuario>(
			0);

	// Constructors

	/** default constructor */
	public TsccvUsuario() {
	}

	/** minimal constructor */
	public TsccvUsuario(Integer idUsua, TsccvEmpresas tsccvEmpresas,
			TsccvPlanta tsccvPlanta,
			TbmBaseGruposUsuarios tbmBaseGruposUsuarios, String login,
			String password, String nombre, String apellido, Integer cedula,
			String email, String activo, Date fechaCreacion) {
		this.idUsua = idUsua;
		this.tsccvEmpresas = tsccvEmpresas;
		this.tsccvPlanta = tsccvPlanta;
		this.tbmBaseGruposUsuarios = tbmBaseGruposUsuarios;
		this.login = login;
		this.password = password;
		this.nombre = nombre;
		this.apellido = apellido;
		this.cedula = cedula;
		this.email = email;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TsccvUsuario(Integer idUsua, TsccvEmpresas tsccvEmpresas,
			TsccvPlanta tsccvPlanta,
			TbmBaseGruposUsuarios tbmBaseGruposUsuarios, String login,
			String password, String nombre, String apellido, Integer cedula,
			String email, String activo, Date fechaCreacion,
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds,
			Set<TsccvPedidos> tsccvPedidoses,
			Set<TsccvPlantasUsuario> tsccvPlantasUsuarios) {
		this.idUsua = idUsua;
		this.tsccvEmpresas = tsccvEmpresas;
		this.tsccvPlanta = tsccvPlanta;
		this.tbmBaseGruposUsuarios = tbmBaseGruposUsuarios;
		this.login = login;
		this.password = password;
		this.nombre = nombre;
		this.apellido = apellido;
		this.cedula = cedula;
		this.email = email;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
		this.tsccvPedidoses = tsccvPedidoses;
		this.tsccvPlantasUsuarios = tsccvPlantasUsuarios;
	}

	// Property accessors
	@Id
	@Column(name = "ID_USUA", unique = true, nullable = false)
	public Integer getIdUsua() {
		return this.idUsua;
	}

	public void setIdUsua(Integer idUsua) {
		this.idUsua = idUsua;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EMPR", nullable = false)
	public TsccvEmpresas getTsccvEmpresas() {
		return this.tsccvEmpresas;
	}

	public void setTsccvEmpresas(TsccvEmpresas tsccvEmpresas) {
		this.tsccvEmpresas = tsccvEmpresas;
	}

	@ManyToOne
	@JoinColumn(name = "CODPLA", nullable = false)
	public TsccvPlanta getTsccvPlanta() {
		return this.tsccvPlanta;
	}

	public void setTsccvPlanta(TsccvPlanta tsccvPlanta) {
		this.tsccvPlanta = tsccvPlanta;
	}

	@ManyToOne
	@JoinColumn(name = "ID_GRUP", nullable = false)
	public TbmBaseGruposUsuarios getTbmBaseGruposUsuarios() {
		return this.tbmBaseGruposUsuarios;
	}

	public void setTbmBaseGruposUsuarios(
			TbmBaseGruposUsuarios tbmBaseGruposUsuarios) {
		this.tbmBaseGruposUsuarios = tbmBaseGruposUsuarios;
	}

	@Column(name = "LOGIN", nullable = false, length = 20)
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name = "PASSWORD", nullable = false, length = 20)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "NOMBRE", nullable = false, length = 30)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "APELLIDO", nullable = false, length = 30)
	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Column(name = "CEDULA", nullable = false)
	public Integer getCedula() {
		return this.cedula;
	}

	public void setCedula(Integer cedula) {
		this.cedula = cedula;
	}

	@Column(name = "EMAIL", nullable = false, length = 60)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "ACTIVO", nullable = false, length = 1)
	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvUsuario")
	public Set<TsccvEstadisticaAccPed> getTsccvEstadisticaAccPeds() {
		return this.tsccvEstadisticaAccPeds;
	}

	public void setTsccvEstadisticaAccPeds(
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds) {
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvUsuario")
	public Set<TsccvPedidos> getTsccvPedidoses() {
		return this.tsccvPedidoses;
	}

	public void setTsccvPedidoses(Set<TsccvPedidos> tsccvPedidoses) {
		this.tsccvPedidoses = tsccvPedidoses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvUsuario")
	public Set<TsccvPlantasUsuario> getTsccvPlantasUsuarios() {
		return this.tsccvPlantasUsuarios;
	}

	public void setTsccvPlantasUsuarios(
			Set<TsccvPlantasUsuario> tsccvPlantasUsuarios) {
		this.tsccvPlantasUsuarios = tsccvPlantasUsuarios;
	}
}