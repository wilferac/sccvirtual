package co.com.smurfit.sccvirtual.entidades;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * TsccvPlanta entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_PLANTA", schema = "dbo")
public class TsccvPlanta implements java.io.Serializable {

	// Fields

	private String codpla;
	private TsccvAplicaciones tsccvAplicaciones;
	private TsccvTipoProducto tsccvTipoProducto;
	private String despla;
	private String planta;
	private String direle;
	private Set<TsccvPdfsProductos> tsccvPdfsProductoses = new HashSet<TsccvPdfsProductos>(
			0);
	private Set<TsccvUsuario> tsccvUsuarios = new HashSet<TsccvUsuario>(0);
	private Set<TsccvPlantasUsuario> tsccvPlantasUsuarios = new HashSet<TsccvPlantasUsuario>(
			0);
	private Set<TsccvPedidos> tsccvPedidoses = new HashSet<TsccvPedidos>(0);
	private Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds = new HashSet<TsccvEstadisticaAccPed>(
			0);

	// Constructors

	/** default constructor */
	public TsccvPlanta() {
	}

	/** minimal constructor */
	public TsccvPlanta(String codpla, TsccvAplicaciones tsccvAplicaciones,
			TsccvTipoProducto tsccvTipoProducto, String despla) {
		this.codpla = codpla;
		this.tsccvAplicaciones = tsccvAplicaciones;
		this.tsccvTipoProducto = tsccvTipoProducto;
		this.despla = despla;
	}

	/** full constructor */
	public TsccvPlanta(String codpla, TsccvAplicaciones tsccvAplicaciones,
			TsccvTipoProducto tsccvTipoProducto, String despla, String planta,
			String direle, Set<TsccvPdfsProductos> tsccvPdfsProductoses,
			Set<TsccvUsuario> tsccvUsuarios,
			Set<TsccvPlantasUsuario> tsccvPlantasUsuarios,
			Set<TsccvPedidos> tsccvPedidoses,
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds) {
		this.codpla = codpla;
		this.tsccvAplicaciones = tsccvAplicaciones;
		this.tsccvTipoProducto = tsccvTipoProducto;
		this.despla = despla;
		this.planta = planta;
		this.direle = direle;
		this.tsccvPdfsProductoses = tsccvPdfsProductoses;
		this.tsccvUsuarios = tsccvUsuarios;
		this.tsccvPlantasUsuarios = tsccvPlantasUsuarios;
		this.tsccvPedidoses = tsccvPedidoses;
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
	}

	// Property accessors
	@Id
	@Column(name = "CODPLA", unique = true, nullable = false, length = 2)
	public String getCodpla() {
		return this.codpla;
	}

	public void setCodpla(String codpla) {
		this.codpla = codpla;
	}

	@ManyToOne
	@JoinColumn(name = "ID_APLI", nullable = false)
	public TsccvAplicaciones getTsccvAplicaciones() {
		return this.tsccvAplicaciones;
	}

	public void setTsccvAplicaciones(TsccvAplicaciones tsccvAplicaciones) {
		this.tsccvAplicaciones = tsccvAplicaciones;
	}

	@ManyToOne
	@JoinColumn(name = "ID_TIPR", nullable = false)
	public TsccvTipoProducto getTsccvTipoProducto() {
		return this.tsccvTipoProducto;
	}

	public void setTsccvTipoProducto(TsccvTipoProducto tsccvTipoProducto) {
		this.tsccvTipoProducto = tsccvTipoProducto;
	}

	@Column(name = "DESPLA", nullable = false, length = 30)
	public String getDespla() {
		return this.despla;
	}

	public void setDespla(String despla) {
		this.despla = despla;
	}

	@Column(name = "PLANTA", length = 1)
	public String getPlanta() {
		return this.planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	@Column(name = "DIRELE", length = 60)
	public String getDirele() {
		return this.direle;
	}

	public void setDirele(String direle) {
		this.direle = direle;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvPlanta")
	public Set<TsccvPdfsProductos> getTsccvPdfsProductoses() {
		return this.tsccvPdfsProductoses;
	}

	public void setTsccvPdfsProductoses(
			Set<TsccvPdfsProductos> tsccvPdfsProductoses) {
		this.tsccvPdfsProductoses = tsccvPdfsProductoses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvPlanta")
	public Set<TsccvUsuario> getTsccvUsuarios() {
		return this.tsccvUsuarios;
	}

	public void setTsccvUsuarios(Set<TsccvUsuario> tsccvUsuarios) {
		this.tsccvUsuarios = tsccvUsuarios;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvPlanta")
	public Set<TsccvPlantasUsuario> getTsccvPlantasUsuarios() {
		return this.tsccvPlantasUsuarios;
	}

	public void setTsccvPlantasUsuarios(
			Set<TsccvPlantasUsuario> tsccvPlantasUsuarios) {
		this.tsccvPlantasUsuarios = tsccvPlantasUsuarios;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvPlanta")
	public Set<TsccvPedidos> getTsccvPedidoses() {
		return this.tsccvPedidoses;
	}

	public void setTsccvPedidoses(Set<TsccvPedidos> tsccvPedidoses) {
		this.tsccvPedidoses = tsccvPedidoses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvPlanta")
	public Set<TsccvEstadisticaAccPed> getTsccvEstadisticaAccPeds() {
		return this.tsccvEstadisticaAccPeds;
	}

	public void setTsccvEstadisticaAccPeds(
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds) {
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
	}

}