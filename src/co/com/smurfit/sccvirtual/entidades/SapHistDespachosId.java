package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SapHistDespachosId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class SapHistDespachosId implements java.io.Serializable {

	// Fields

	private String codpro;
	private String nummes;
	private String codcli;
	private String cantid;
	private String despro;
	private String undmed;
	private String cankgs;

	// Constructors

	/** default constructor */
	public SapHistDespachosId() {
	}

	/** minimal constructor */
	public SapHistDespachosId(String codpro, String nummes, String codcli) {
		this.codpro = codpro;
		this.nummes = nummes;
		this.codcli = codcli;
	}

	/** full constructor */
	public SapHistDespachosId(String codpro, String nummes, String codcli,
			String cantid, String despro, String undmed, String cankgs) {
		this.codpro = codpro;
		this.nummes = nummes;
		this.codcli = codcli;
		this.cantid = cantid;
		this.despro = despro;
		this.undmed = undmed;
		this.cankgs = cankgs;
	}

	// Property accessors

	@Column(name = "CODPRO", nullable = false, length = 10)
	public String getCodpro() {
		return this.codpro;
	}

	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}

	@Column(name = "NUMMES", nullable = false, length = 6)
	public String getNummes() {
		return this.nummes;
	}

	public void setNummes(String nummes) {
		this.nummes = nummes;
	}

	@Column(name = "CODCLI", nullable = false, length = 8)
	public String getCodcli() {
		return this.codcli;
	}

	public void setCodcli(String codcli) {
		this.codcli = codcli;
	}

	@Column(name = "CANTID", length = 16)
	public String getCantid() {
		return this.cantid;
	}

	public void setCantid(String cantid) {
		this.cantid = cantid;
	}

	@Column(name = "DESPRO", length = 60)
	public String getDespro() {
		return this.despro;
	}

	public void setDespro(String despro) {
		this.despro = despro;
	}

	@Column(name = "UNDMED", length = 10)
	public String getUndmed() {
		return this.undmed;
	}

	public void setUndmed(String undmed) {
		this.undmed = undmed;
	}

	@Column(name = "CANKGS", length = 16)
	public String getCankgs() {
		return this.cankgs;
	}

	public void setCankgs(String cankgs) {
		this.cankgs = cankgs;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SapHistDespachosId))
			return false;
		SapHistDespachosId castOther = (SapHistDespachosId) other;

		return ((this.getCodpro() == castOther.getCodpro()) || (this
				.getCodpro() != null
				&& castOther.getCodpro() != null && this.getCodpro().equals(
				castOther.getCodpro())))
				&& ((this.getNummes() == castOther.getNummes()) || (this
						.getNummes() != null
						&& castOther.getNummes() != null && this.getNummes()
						.equals(castOther.getNummes())))
				&& ((this.getCodcli() == castOther.getCodcli()) || (this
						.getCodcli() != null
						&& castOther.getCodcli() != null && this.getCodcli()
						.equals(castOther.getCodcli())))
				&& ((this.getCantid() == castOther.getCantid()) || (this
						.getCantid() != null
						&& castOther.getCantid() != null && this.getCantid()
						.equals(castOther.getCantid())))
				&& ((this.getDespro() == castOther.getDespro()) || (this
						.getDespro() != null
						&& castOther.getDespro() != null && this.getDespro()
						.equals(castOther.getDespro())))
				&& ((this.getUndmed() == castOther.getUndmed()) || (this
						.getUndmed() != null
						&& castOther.getUndmed() != null && this.getUndmed()
						.equals(castOther.getUndmed())))
				&& ((this.getCankgs() == castOther.getCankgs()) || (this
						.getCankgs() != null
						&& castOther.getCankgs() != null && this.getCankgs()
						.equals(castOther.getCankgs())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCodpro() == null ? 0 : this.getCodpro().hashCode());
		result = 37 * result
				+ (getNummes() == null ? 0 : this.getNummes().hashCode());
		result = 37 * result
				+ (getCodcli() == null ? 0 : this.getCodcli().hashCode());
		result = 37 * result
				+ (getCantid() == null ? 0 : this.getCantid().hashCode());
		result = 37 * result
				+ (getDespro() == null ? 0 : this.getDespro().hashCode());
		result = 37 * result
				+ (getUndmed() == null ? 0 : this.getUndmed().hashCode());
		result = 37 * result
				+ (getCankgs() == null ? 0 : this.getCankgs().hashCode());
		return result;
	}

}