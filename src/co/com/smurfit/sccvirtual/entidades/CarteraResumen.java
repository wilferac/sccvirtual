package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * CarteraResumen entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CARTERA_RESUMEN", schema = "dbo")
public class CarteraResumen implements java.io.Serializable {

	// Fields

	private CarteraResumenId id;

	// Constructors

	/** default constructor */
	public CarteraResumen() {
	}

	/** full constructor */
	public CarteraResumen(CarteraResumenId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codcli", column = @Column(name = "CODCLI", nullable = false, length = 8)),
			@AttributeOverride(name = "numfac", column = @Column(name = "NUMFAC", nullable = false, length = 10)),
			@AttributeOverride(name = "clsdoc", column = @Column(name = "CLSDOC", length = 4)),
			@AttributeOverride(name = "fecfac", column = @Column(name = "FECFAC", length = 10)),
			@AttributeOverride(name = "fecven", column = @Column(name = "FECVEN", length = 10)),
			@AttributeOverride(name = "dias", column = @Column(name = "DIAS", length = 10)),
			@AttributeOverride(name = "monto", column = @Column(name = "MONTO", length = 14)),
			@AttributeOverride(name = "planta", column = @Column(name = "PLANTA", length = 4)),
			@AttributeOverride(name = "rango", column = @Column(name = "RANGO", length = 1)) })
	public CarteraResumenId getId() {
		return this.id;
	}

	public void setId(CarteraResumenId id) {
		this.id = id;
	}

}