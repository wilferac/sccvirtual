package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * BvpPrecioId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class BvpPrecioId implements java.io.Serializable {

	// Fields

	private String codCli;
	private String codpla;
	private String codScc;
	private String precio;

	// Constructors

	/** default constructor */
	public BvpPrecioId() {
	}

	/** full constructor */
	public BvpPrecioId(String codCli, String codpla, String codScc,
			String precio) {
		this.codCli = codCli;
		this.codpla = codpla;
		this.codScc = codScc;
		this.precio = precio;
	}

	// Property accessors

	@Column(name = "COD_CLI", nullable = false, length = 8)
	public String getCodCli() {
		return this.codCli;
	}

	public void setCodCli(String codCli) {
		this.codCli = codCli;
	}

	@Column(name = "CODPLA", nullable = false, length = 2)
	public String getCodpla() {
		return this.codpla;
	}

	public void setCodpla(String codpla) {
		this.codpla = codpla;
	}

	@Column(name = "COD_SCC", nullable = false, length = 8)
	public String getCodScc() {
		return this.codScc;
	}

	public void setCodScc(String codScc) {
		this.codScc = codScc;
	}

	@Column(name = "PRECIO", nullable = false, length = 16)
	public String getPrecio() {
		return this.precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BvpPrecioId))
			return false;
		BvpPrecioId castOther = (BvpPrecioId) other;

		return ((this.getCodCli() == castOther.getCodCli()) || (this
				.getCodCli() != null
				&& castOther.getCodCli() != null && this.getCodCli().equals(
				castOther.getCodCli())))
				&& ((this.getCodpla() == castOther.getCodpla()) || (this
						.getCodpla() != null
						&& castOther.getCodpla() != null && this.getCodpla()
						.equals(castOther.getCodpla())))
				&& ((this.getCodScc() == castOther.getCodScc()) || (this
						.getCodScc() != null
						&& castOther.getCodScc() != null && this.getCodScc()
						.equals(castOther.getCodScc())))
				&& ((this.getPrecio() == castOther.getPrecio()) || (this
						.getPrecio() != null
						&& castOther.getPrecio() != null && this.getPrecio()
						.equals(castOther.getPrecio())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCodCli() == null ? 0 : this.getCodCli().hashCode());
		result = 37 * result
				+ (getCodpla() == null ? 0 : this.getCodpla().hashCode());
		result = 37 * result
				+ (getCodScc() == null ? 0 : this.getCodScc().hashCode());
		result = 37 * result
				+ (getPrecio() == null ? 0 : this.getPrecio().hashCode());
		return result;
	}

}