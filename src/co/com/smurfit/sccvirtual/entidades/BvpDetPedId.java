package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * BvpDetPedId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class BvpDetPedId implements java.io.Serializable {

	// Fields

	private String planta;
	private String numped;
	private String itmped;
	private String codcli;
	private String codscc;
	private String codpro;
	private String tippro;
	private String despro;
	private String fecsol;
	private String fecdsp;
	private String estado;
	private String undmed;
	private String cntped;
	private String cntdsp;
	private String ordcom;

	// Constructors

	/** default constructor */
	public BvpDetPedId() {
	}

	/** minimal constructor */
	public BvpDetPedId(String planta, String numped, String itmped) {
		this.planta = planta;
		this.numped = numped;
		this.itmped = itmped;
	}

	/** full constructor */
	public BvpDetPedId(String planta, String numped, String itmped,
			String codcli, String codscc, String codpro, String tippro,
			String despro, String fecsol, String fecdsp, String estado,
			String undmed, String cntped, String cntdsp, String ordcom) {
		this.planta = planta;
		this.numped = numped;
		this.itmped = itmped;
		this.codcli = codcli;
		this.codscc = codscc;
		this.codpro = codpro;
		this.tippro = tippro;
		this.despro = despro;
		this.fecsol = fecsol;
		this.fecdsp = fecdsp;
		this.estado = estado;
		this.undmed = undmed;
		this.cntped = cntped;
		this.cntdsp = cntdsp;
		this.ordcom = ordcom;
	}

	// Property accessors

	@Column(name = "PLANTA", nullable = false, length = 1)
	public String getPlanta() {
		return this.planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	@Column(name = "NUMPED", nullable = false, length = 8)
	public String getNumped() {
		return this.numped;
	}

	public void setNumped(String numped) {
		this.numped = numped;
	}

	@Column(name = "ITMPED", nullable = false, length = 2)
	public String getItmped() {
		return this.itmped;
	}

	public void setItmped(String itmped) {
		this.itmped = itmped;
	}

	@Column(name = "CODCLI", length = 8)
	public String getCodcli() {
		return this.codcli;
	}

	public void setCodcli(String codcli) {
		this.codcli = codcli;
	}

	@Column(name = "CODSCC", length = 8)
	public String getCodscc() {
		return this.codscc;
	}

	public void setCodscc(String codscc) {
		this.codscc = codscc;
	}

	@Column(name = "CODPRO", length = 20)
	public String getCodpro() {
		return this.codpro;
	}

	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}

	@Column(name = "TIPPRO", length = 1)
	public String getTippro() {
		return this.tippro;
	}

	public void setTippro(String tippro) {
		this.tippro = tippro;
	}

	@Column(name = "DESPRO", length = 60)
	public String getDespro() {
		return this.despro;
	}

	public void setDespro(String despro) {
		this.despro = despro;
	}

	@Column(name = "FECSOL", length = 10)
	public String getFecsol() {
		return this.fecsol;
	}

	public void setFecsol(String fecsol) {
		this.fecsol = fecsol;
	}

	@Column(name = "FECDSP", length = 10)
	public String getFecdsp() {
		return this.fecdsp;
	}

	public void setFecdsp(String fecdsp) {
		this.fecdsp = fecdsp;
	}

	@Column(name = "ESTADO", length = 1)
	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Column(name = "UNDMED", length = 10)
	public String getUndmed() {
		return this.undmed;
	}

	public void setUndmed(String undmed) {
		this.undmed = undmed;
	}

	@Column(name = "CNTPED", length = 16)
	public String getCntped() {
		return this.cntped;
	}

	public void setCntped(String cntped) {
		this.cntped = cntped;
	}

	@Column(name = "CNTDSP", length = 16)
	public String getCntdsp() {
		return this.cntdsp;
	}

	public void setCntdsp(String cntdsp) {
		this.cntdsp = cntdsp;
	}

	@Column(name = "ORDCOM", length = 15)
	public String getOrdcom() {
		return this.ordcom;
	}

	public void setOrdcom(String ordcom) {
		this.ordcom = ordcom;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BvpDetPedId))
			return false;
		BvpDetPedId castOther = (BvpDetPedId) other;

		return ((this.getPlanta() == castOther.getPlanta()) || (this
				.getPlanta() != null
				&& castOther.getPlanta() != null && this.getPlanta().equals(
				castOther.getPlanta())))
				&& ((this.getNumped() == castOther.getNumped()) || (this
						.getNumped() != null
						&& castOther.getNumped() != null && this.getNumped()
						.equals(castOther.getNumped())))
				&& ((this.getItmped() == castOther.getItmped()) || (this
						.getItmped() != null
						&& castOther.getItmped() != null && this.getItmped()
						.equals(castOther.getItmped())))
				&& ((this.getCodcli() == castOther.getCodcli()) || (this
						.getCodcli() != null
						&& castOther.getCodcli() != null && this.getCodcli()
						.equals(castOther.getCodcli())))
				&& ((this.getCodscc() == castOther.getCodscc()) || (this
						.getCodscc() != null
						&& castOther.getCodscc() != null && this.getCodscc()
						.equals(castOther.getCodscc())))
				&& ((this.getCodpro() == castOther.getCodpro()) || (this
						.getCodpro() != null
						&& castOther.getCodpro() != null && this.getCodpro()
						.equals(castOther.getCodpro())))
				&& ((this.getTippro() == castOther.getTippro()) || (this
						.getTippro() != null
						&& castOther.getTippro() != null && this.getTippro()
						.equals(castOther.getTippro())))
				&& ((this.getDespro() == castOther.getDespro()) || (this
						.getDespro() != null
						&& castOther.getDespro() != null && this.getDespro()
						.equals(castOther.getDespro())))
				&& ((this.getFecsol() == castOther.getFecsol()) || (this
						.getFecsol() != null
						&& castOther.getFecsol() != null && this.getFecsol()
						.equals(castOther.getFecsol())))
				&& ((this.getFecdsp() == castOther.getFecdsp()) || (this
						.getFecdsp() != null
						&& castOther.getFecdsp() != null && this.getFecdsp()
						.equals(castOther.getFecdsp())))
				&& ((this.getEstado() == castOther.getEstado()) || (this
						.getEstado() != null
						&& castOther.getEstado() != null && this.getEstado()
						.equals(castOther.getEstado())))
				&& ((this.getUndmed() == castOther.getUndmed()) || (this
						.getUndmed() != null
						&& castOther.getUndmed() != null && this.getUndmed()
						.equals(castOther.getUndmed())))
				&& ((this.getCntped() == castOther.getCntped()) || (this
						.getCntped() != null
						&& castOther.getCntped() != null && this.getCntped()
						.equals(castOther.getCntped())))
				&& ((this.getCntdsp() == castOther.getCntdsp()) || (this
						.getCntdsp() != null
						&& castOther.getCntdsp() != null && this.getCntdsp()
						.equals(castOther.getCntdsp())))
				&& ((this.getOrdcom() == castOther.getOrdcom()) || (this
						.getOrdcom() != null
						&& castOther.getOrdcom() != null && this.getOrdcom()
						.equals(castOther.getOrdcom())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getPlanta() == null ? 0 : this.getPlanta().hashCode());
		result = 37 * result
				+ (getNumped() == null ? 0 : this.getNumped().hashCode());
		result = 37 * result
				+ (getItmped() == null ? 0 : this.getItmped().hashCode());
		result = 37 * result
				+ (getCodcli() == null ? 0 : this.getCodcli().hashCode());
		result = 37 * result
				+ (getCodscc() == null ? 0 : this.getCodscc().hashCode());
		result = 37 * result
				+ (getCodpro() == null ? 0 : this.getCodpro().hashCode());
		result = 37 * result
				+ (getTippro() == null ? 0 : this.getTippro().hashCode());
		result = 37 * result
				+ (getDespro() == null ? 0 : this.getDespro().hashCode());
		result = 37 * result
				+ (getFecsol() == null ? 0 : this.getFecsol().hashCode());
		result = 37 * result
				+ (getFecdsp() == null ? 0 : this.getFecdsp().hashCode());
		result = 37 * result
				+ (getEstado() == null ? 0 : this.getEstado().hashCode());
		result = 37 * result
				+ (getUndmed() == null ? 0 : this.getUndmed().hashCode());
		result = 37 * result
				+ (getCntped() == null ? 0 : this.getCntped().hashCode());
		result = 37 * result
				+ (getCntdsp() == null ? 0 : this.getCntdsp().hashCode());
		result = 37 * result
				+ (getOrdcom() == null ? 0 : this.getOrdcom().hashCode());
		return result;
	}

}