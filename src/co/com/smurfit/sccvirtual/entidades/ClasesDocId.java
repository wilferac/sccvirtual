package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ClasesDocId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class ClasesDocId implements java.io.Serializable {

	// Fields

	private String coddoc;
	private String desdoc;

	// Constructors

	/** default constructor */
	public ClasesDocId() {
	}

	/** minimal constructor */
	public ClasesDocId(String coddoc) {
		this.coddoc = coddoc;
	}

	/** full constructor */
	public ClasesDocId(String coddoc, String desdoc) {
		this.coddoc = coddoc;
		this.desdoc = desdoc;
	}

	// Property accessors

	@Column(name = "CODDOC", nullable = false, length = 2)
	public String getCoddoc() {
		return this.coddoc;
	}

	public void setCoddoc(String coddoc) {
		this.coddoc = coddoc;
	}

	@Column(name = "DESDOC", length = 22)
	public String getDesdoc() {
		return this.desdoc;
	}

	public void setDesdoc(String desdoc) {
		this.desdoc = desdoc;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ClasesDocId))
			return false;
		ClasesDocId castOther = (ClasesDocId) other;

		return ((this.getCoddoc() == castOther.getCoddoc()) || (this
				.getCoddoc() != null
				&& castOther.getCoddoc() != null && this.getCoddoc().equals(
				castOther.getCoddoc())))
				&& ((this.getDesdoc() == castOther.getDesdoc()) || (this
						.getDesdoc() != null
						&& castOther.getDesdoc() != null && this.getDesdoc()
						.equals(castOther.getDesdoc())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCoddoc() == null ? 0 : this.getCoddoc().hashCode());
		result = 37 * result
				+ (getDesdoc() == null ? 0 : this.getDesdoc().hashCode());
		return result;
	}

}