package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * BvpHistDespachos entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "BVP_HIST_DESPACHOS", schema = "dbo")
public class BvpHistDespachos implements java.io.Serializable {

	// Fields

	private BvpHistDespachosId id;

	// Constructors

	/** default constructor */
	public BvpHistDespachos() {
	}

	/** full constructor */
	public BvpHistDespachos(BvpHistDespachosId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "planta", column = @Column(name = "PLANTA", nullable = false, length = 2)),
			@AttributeOverride(name = "nummes", column = @Column(name = "NUMMES", nullable = false, length = 6)),
			@AttributeOverride(name = "codcli", column = @Column(name = "CODCLI", nullable = false, length = 8)),
			@AttributeOverride(name = "codscc", column = @Column(name = "CODSCC", nullable = false, length = 8)),
			@AttributeOverride(name = "codpro", column = @Column(name = "CODPRO", length = 20)),
			@AttributeOverride(name = "despro", column = @Column(name = "DESPRO", length = 60)),
			@AttributeOverride(name = "tippro", column = @Column(name = "TIPPRO", length = 1)),
			@AttributeOverride(name = "undmed", column = @Column(name = "UNDMED", length = 10)),
			@AttributeOverride(name = "cantid", column = @Column(name = "CANTID", length = 16)) })
	public BvpHistDespachosId getId() {
		return this.id;
	}

	public void setId(BvpHistDespachosId id) {
		this.id = id;
	}

}