package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvAplicaciones entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_APLICACIONES", schema = "dbo")
public class TsccvAplicaciones implements java.io.Serializable {

	// Fields

	private Long idApli;
	private String descripcion;
	private String activo;
	private Date fechaCreacion;
	private Set<TsccvPlanta> tsccvPlantas = new HashSet<TsccvPlanta>(0);

	// Constructors

	/** default constructor */
	public TsccvAplicaciones() {
	}

	/** minimal constructor */
	public TsccvAplicaciones(Long idApli, String descripcion, String activo,
			Date fechaCreacion) {
		this.idApli = idApli;
		this.descripcion = descripcion;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TsccvAplicaciones(Long idApli, String descripcion, String activo,
			Date fechaCreacion, Set<TsccvPlanta> tsccvPlantas) {
		this.idApli = idApli;
		this.descripcion = descripcion;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.tsccvPlantas = tsccvPlantas;
	}

	// Property accessors
	@Id
	@Column(name = "ID_APLI", unique = true, nullable = false, precision = 18, scale = 0)
	public Long getIdApli() {
		return this.idApli;
	}

	public void setIdApli(Long idApli) {
		this.idApli = idApli;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 20)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "ACTIVO", nullable = false, length = 1)
	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvAplicaciones")
	public Set<TsccvPlanta> getTsccvPlantas() {
		return this.tsccvPlantas;
	}

	public void setTsccvPlantas(Set<TsccvPlanta> tsccvPlantas) {
		this.tsccvPlantas = tsccvPlantas;
	}

}