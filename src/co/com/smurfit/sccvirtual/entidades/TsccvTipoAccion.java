package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvTipoAccion entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_TIPO_ACCION", schema = "dbo")
public class TsccvTipoAccion implements java.io.Serializable {

	// Fields

	private Long idTiac;
	private String descripcion;
	private Date fechaCreacion;
	private Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds = new HashSet<TsccvEstadisticaAccPed>(
			0);

	// Constructors

	/** default constructor */
	public TsccvTipoAccion() {
	}

	/** minimal constructor */
	public TsccvTipoAccion(Long idTiac, String descripcion, Date fechaCreacion) {
		this.idTiac = idTiac;
		this.descripcion = descripcion;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TsccvTipoAccion(Long idTiac, String descripcion, Date fechaCreacion,
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds) {
		this.idTiac = idTiac;
		this.descripcion = descripcion;
		this.fechaCreacion = fechaCreacion;
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
	}

	// Property accessors
	@Id
	@Column(name = "ID_TIAC", unique = true, nullable = false, precision = 1, scale = 0)
	public Long getIdTiac() {
		return this.idTiac;
	}

	public void setIdTiac(Long idTiac) {
		this.idTiac = idTiac;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 20)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvTipoAccion")
	public Set<TsccvEstadisticaAccPed> getTsccvEstadisticaAccPeds() {
		return this.tsccvEstadisticaAccPeds;
	}

	public void setTsccvEstadisticaAccPeds(
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds) {
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
	}

}