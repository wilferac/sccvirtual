package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * TsccvEntregasPedido entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_ENTREGAS_PEDIDO", schema = "dbo")
public class TsccvEntregasPedido implements java.io.Serializable {

	// Fields

	private Integer idEnpe;
	private TsccvProductosPedido tsccvProductosPedido;
	private String desdir;
	private Date fechaEntrega;
	private Integer cantidad;
	private String anchoRollo;
	private String anchoHojas;
	private String largoHojas;
	private String precio;

	// Constructors

		/** default constructor */
	public TsccvEntregasPedido() {
	}

	/** minimal constructor */
	public TsccvEntregasPedido(Integer idEnpe, String desdir,
			Date fechaEntrega, Integer cantidad) {
		this.idEnpe = idEnpe;
		this.desdir = desdir;
		this.fechaEntrega = fechaEntrega;
		this.cantidad = cantidad;
	}

	/** full constructor */
	public TsccvEntregasPedido(Integer idEnpe,
			TsccvProductosPedido tsccvProductosPedido, String desdir,
			Date fechaEntrega, Integer cantidad, String anchoRollo,
			String anchoHojas, String largoHojas) {
		this.idEnpe = idEnpe;
		this.tsccvProductosPedido = tsccvProductosPedido;
		this.desdir = desdir;
		this.fechaEntrega = fechaEntrega;
		this.cantidad = cantidad;
		this.anchoRollo = anchoRollo;
		this.anchoHojas = anchoHojas;
		this.largoHojas = largoHojas;
	}

	// Property accessors
	@Id
	@Column(name = "ID_ENPE", unique = true, nullable = false)
	public Integer getIdEnpe() {
		return this.idEnpe;
	}

	public void setIdEnpe(Integer idEnpe) {
		this.idEnpe = idEnpe;
	}

	@ManyToOne
	@JoinColumn(name = "ID_PRPE")
	public TsccvProductosPedido getTsccvProductosPedido() {
		return this.tsccvProductosPedido;
	}

	public void setTsccvProductosPedido(
			TsccvProductosPedido tsccvProductosPedido) {
		this.tsccvProductosPedido = tsccvProductosPedido;
	}

	@Column(name = "DESDIR", nullable = false, length = 80)
	public String getDesdir() {
		return this.desdir;
	}

	public void setDesdir(String desdir) {
		this.desdir = desdir;
	}

	@Column(name = "FECHA_ENTREGA", nullable = false, length = 23)
	public Date getFechaEntrega() {
		return this.fechaEntrega;
	}

	public void setFechaEntrega(Date fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	@Column(name = "CANTIDAD", nullable = false)
	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	@Column(name = "ANCHO_ROLLO", length = 50)
	public String getAnchoRollo() {
		return this.anchoRollo;
	}

	public void setAnchoRollo(String anchoRollo) {
		this.anchoRollo = anchoRollo;
	}

	@Column(name = "ANCHO_HOJAS", length = 50)
	public String getAnchoHojas() {
		return this.anchoHojas;
	}

	public void setAnchoHojas(String anchoHojas) {
		this.anchoHojas = anchoHojas;
	}

	@Column(name = "LARGO_HOJAS", length = 50)
	public String getLargoHojas() {
		return this.largoHojas;
	}

	public void setLargoHojas(String largoHojas) {
		this.largoHojas = largoHojas;
	}
	
	@Column(name = "PRECIO", length = 16)
	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

}