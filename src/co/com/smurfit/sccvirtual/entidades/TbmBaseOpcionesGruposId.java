package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * TbmBaseOpcionesGruposId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class TbmBaseOpcionesGruposId implements java.io.Serializable {

	// Fields

	private Long idOpci;
	private Long idGrup;

	// Constructors

	/** default constructor */
	public TbmBaseOpcionesGruposId() {
	}

	/** full constructor */
	public TbmBaseOpcionesGruposId(Long idOpci, Long idGrup) {
		this.idOpci = idOpci;
		this.idGrup = idGrup;
	}

	// Property accessors

	@Column(name = "ID_OPCI", nullable = false, precision = 2, scale = 0)
	public Long getIdOpci() {
		return this.idOpci;
	}

	public void setIdOpci(Long idOpci) {
		this.idOpci = idOpci;
	}

	@Column(name = "ID_GRUP", nullable = false, precision = 3, scale = 0)
	public Long getIdGrup() {
		return this.idGrup;
	}

	public void setIdGrup(Long idGrup) {
		this.idGrup = idGrup;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TbmBaseOpcionesGruposId))
			return false;
		TbmBaseOpcionesGruposId castOther = (TbmBaseOpcionesGruposId) other;

		return ((this.getIdOpci() == castOther.getIdOpci()) || (this
				.getIdOpci() != null
				&& castOther.getIdOpci() != null && this.getIdOpci().equals(
				castOther.getIdOpci())))
				&& ((this.getIdGrup() == castOther.getIdGrup()) || (this
						.getIdGrup() != null
						&& castOther.getIdGrup() != null && this.getIdGrup()
						.equals(castOther.getIdGrup())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getIdOpci() == null ? 0 : this.getIdOpci().hashCode());
		result = 37 * result
				+ (getIdGrup() == null ? 0 : this.getIdGrup().hashCode());
		return result;
	}

}