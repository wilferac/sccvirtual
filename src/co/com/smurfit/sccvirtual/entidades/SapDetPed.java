package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * SapDetPed entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "SAP_DET_PED", schema = "dbo")
public class SapDetPed implements java.io.Serializable {

	// Fields

	private SapDetPedId id;

	// Constructors

	/** default constructor */
	public SapDetPed() {
	}

	/** full constructor */
	public SapDetPed(SapDetPedId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "numped", column = @Column(name = "NUMPED", nullable = false, length = 8)),
			@AttributeOverride(name = "itmped", column = @Column(name = "ITMPED", nullable = false, length = 3)),
			@AttributeOverride(name = "subitm", column = @Column(name = "SUBITM", nullable = false, length = 2)),
			@AttributeOverride(name = "codcli", column = @Column(name = "CODCLI", length = 8)),
			@AttributeOverride(name = "despro", column = @Column(name = "DESPRO", length = 64)),
			@AttributeOverride(name = "cntped", column = @Column(name = "CNTPED", length = 16)),
			@AttributeOverride(name = "undmed", column = @Column(name = "UNDMED", length = 10)),
			@AttributeOverride(name = "fecsol", column = @Column(name = "FECSOL", length = 10)),
			@AttributeOverride(name = "fecdsp", column = @Column(name = "FECDSP", length = 10)),
			@AttributeOverride(name = "codpro", column = @Column(name = "CODPRO", length = 8)),
			@AttributeOverride(name = "estado", column = @Column(name = "ESTADO", length = 2)),
			@AttributeOverride(name = "ordcom", column = @Column(name = "ORDCOM", length = 15)),
			@AttributeOverride(name = "cntdsp", column = @Column(name = "CNTDSP", length = 16)),
			@AttributeOverride(name = "cntkgs", column = @Column(name = "CNTKGS", length = 16)),
			@AttributeOverride(name = "arollos", column = @Column(name = "AROLLOS", length = 10)),
			@AttributeOverride(name = "ahojas", column = @Column(name = "AHOJAS", length = 10)),
			@AttributeOverride(name = "lhojas", column = @Column(name = "LHOJAS", length = 10)),
			@AttributeOverride(name = "arollo", column = @Column(name = "AROLLO", length = 10)) })
	public SapDetPedId getId() {
		return this.id;
	}

	public void setId(SapDetPedId id) {
		this.id = id;
	}

}