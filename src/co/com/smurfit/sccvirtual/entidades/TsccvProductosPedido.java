package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvProductosPedido entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_PRODUCTOS_PEDIDO", schema = "dbo")
public class TsccvProductosPedido implements java.io.Serializable {

	// Fields

	private Integer idPrpe;
	private TsccvPedidos tsccvPedidos;
	private Integer cantidadTotal;
	private Date fechaPrimeraEntrega;
	private String ordenCompra;
	private String codCli;
	private String codpla;
	private String codScc;
	private String precio;
	private String activo;
	private Date fechaCreacion;
	private String despro;
	private String undmed;
	private String moneda;
	private String eje;
	private String diamtr;
	private Set<TsccvEntregasPedido> tsccvEntregasPedidos = new HashSet<TsccvEntregasPedido>(
			0);

	// Constructors

	/** default constructor */
	public TsccvProductosPedido() {
		
	}

	/** minimal constructor */
	public TsccvProductosPedido(Integer idPrpe, Integer cantidadTotal,
			String ordenCompra, String codCli, String codpla, String precio,
			String activo, Date fechaCreacion, String despro) {
		this.idPrpe = idPrpe;
		this.cantidadTotal = cantidadTotal;
		this.ordenCompra = ordenCompra;
		this.codCli = codCli;
		this.codpla = codpla;
		this.precio = precio;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.despro = despro;
	}

	/** full constructor */
	public TsccvProductosPedido(Integer idPrpe, TsccvPedidos tsccvPedidos,
			Integer cantidadTotal, Date fechaPrimeraEntrega,
			String ordenCompra, String codCli, String codpla, String codScc,
			String precio, String activo, Date fechaCreacion, String despro,
			String undmed, String moneda, String eje, String diamtr,
			Set<TsccvEntregasPedido> tsccvEntregasPedidos) {
		this.idPrpe = idPrpe;
		this.tsccvPedidos = tsccvPedidos;
		this.cantidadTotal = cantidadTotal;
		this.fechaPrimeraEntrega = fechaPrimeraEntrega;
		this.ordenCompra = ordenCompra;
		this.codCli = codCli;
		this.codpla = codpla;
		this.codScc = codScc;
		this.precio = precio;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.despro = despro;
		this.undmed = undmed;
		this.moneda = moneda;
		this.eje = eje;
		this.diamtr = diamtr;
		this.tsccvEntregasPedidos = tsccvEntregasPedidos;
	}

	// Property accessors
	@Id
	@Column(name = "ID_PRPE", unique = true, nullable = false)
	public Integer getIdPrpe() {
		return this.idPrpe;
	}

	public void setIdPrpe(Integer idPrpe) {
		this.idPrpe = idPrpe;
	}

	@ManyToOne
	@JoinColumn(name = "ID_PEDI")
	public TsccvPedidos getTsccvPedidos() {
		return this.tsccvPedidos;
	}

	public void setTsccvPedidos(TsccvPedidos tsccvPedidos) {
		this.tsccvPedidos = tsccvPedidos;
	}

	@Column(name = "CANTIDAD_TOTAL", nullable = false)
	public Integer getCantidadTotal() {
		return this.cantidadTotal;
	}

	public void setCantidadTotal(Integer cantidadTotal) {
		this.cantidadTotal = cantidadTotal;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_PRIMERA_ENTREGA", length = 23)
	public Date getFechaPrimeraEntrega() {
		return this.fechaPrimeraEntrega;
	}

	public void setFechaPrimeraEntrega(Date fechaPrimeraEntrega) {
		this.fechaPrimeraEntrega = fechaPrimeraEntrega;
	}

	@Column(name = "ORDEN_COMPRA", nullable = false, length = 15)
	public String getOrdenCompra() {
		return this.ordenCompra;
	}

	public void setOrdenCompra(String ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	@Column(name = "COD_CLI", nullable = false, length = 8)
	public String getCodCli() {
		return this.codCli;
	}

	public void setCodCli(String codCli) {
		this.codCli = codCli;
	}

	@Column(name = "CODPLA", nullable = false, length = 2)
	public String getCodpla() {
		return this.codpla;
	}

	public void setCodpla(String codpla) {
		this.codpla = codpla;
	}

	@Column(name = "COD_SCC", length = 8)
	public String getCodScc() {
		return this.codScc;
	}

	public void setCodScc(String codScc) {
		this.codScc = codScc;
	}

	@Column(name = "PRECIO", nullable = false, length = 16)
	public String getPrecio() {
		return this.precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	@Column(name = "ACTIVO", nullable = false, length = 1)
	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "DESPRO", nullable = false, length = 60)
	public String getDespro() {
		return this.despro;
	}

	public void setDespro(String despro) {
		this.despro = despro;
	}

	@Column(name = "UNDMED", length = 50)
	public String getUndmed() {
		return this.undmed;
	}

	public void setUndmed(String undmed) {
		this.undmed = undmed;
	}

	@Column(name = "MONEDA", length = 50)
	public String getMoneda() {
		return this.moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Column(name = "EJE", length = 16)
	public String getEje() {
		return this.eje;
	}

	public void setEje(String eje) {
		this.eje = eje;
	}

	@Column(name = "DIAMTR", length = 16)
	public String getDiamtr() {
		return this.diamtr;
	}

	public void setDiamtr(String diamtr) {
		this.diamtr = diamtr;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvProductosPedido")
	public Set<TsccvEntregasPedido> getTsccvEntregasPedidos() {
		return this.tsccvEntregasPedidos;
	}

	public void setTsccvEntregasPedidos(
			Set<TsccvEntregasPedido> tsccvEntregasPedidos) {
		this.tsccvEntregasPedidos = tsccvEntregasPedidos;
	}

}