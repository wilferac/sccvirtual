package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvEstadisticaAccPed entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_ESTADISTICA_ACC_PED", schema = "dbo")
public class TsccvEstadisticaAccPed implements java.io.Serializable {

	// Fields

	private Long idEsap;
	private TsccvEmpresas tsccvEmpresas;
	private TsccvTipoAccion tsccvTipoAccion;
	private TsccvPlanta tsccvPlanta;
	private TbmBaseGruposUsuarios tbmBaseGruposUsuarios;
	private TsccvUsuario tsccvUsuario;
	private Long acceso;
	private Long pedido;
	private Date fechaCreacion;

	// Constructors

	/** default constructor */
	public TsccvEstadisticaAccPed() {
	}

	/** minimal constructor */
	public TsccvEstadisticaAccPed(Long idEsap, TsccvEmpresas tsccvEmpresas,
			TsccvPlanta tsccvPlanta, TbmBaseGruposUsuarios tbmBaseGruposUsuarios) {
		this.idEsap = idEsap;
		this.tsccvEmpresas = tsccvEmpresas;
		this.tsccvPlanta = tsccvPlanta;
		this.tbmBaseGruposUsuarios = tbmBaseGruposUsuarios;
	}

	/** full constructor */
	public TsccvEstadisticaAccPed(Long idEsap, TsccvEmpresas tsccvEmpresas,
			TsccvTipoAccion tsccvTipoAccion, TsccvPlanta tsccvPlanta,
			TbmBaseGruposUsuarios tbmBaseGruposUsuarios,
			TsccvUsuario tsccvUsuario, Long acceso, Long pedido,
			Date fechaCreacion) {
		this.idEsap = idEsap;
		this.tsccvEmpresas = tsccvEmpresas;
		this.tsccvTipoAccion = tsccvTipoAccion;
		this.tsccvPlanta = tsccvPlanta;
		this.tbmBaseGruposUsuarios = tbmBaseGruposUsuarios;
		this.tsccvUsuario = tsccvUsuario;
		this.acceso = acceso;
		this.pedido = pedido;
		this.fechaCreacion = fechaCreacion;
	}

	// Property accessors
	@Id
	@Column(name = "ID_ESAP", unique = true, nullable = false, precision = 18, scale = 0)
	public Long getIdEsap() {
		return this.idEsap;
	}

	public void setIdEsap(Long idEsap) {
		this.idEsap = idEsap;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EMPR", nullable = false)
	public TsccvEmpresas getTsccvEmpresas() {
		return this.tsccvEmpresas;
	}

	public void setTsccvEmpresas(TsccvEmpresas tsccvEmpresas) {
		this.tsccvEmpresas = tsccvEmpresas;
	}

	@ManyToOne
	@JoinColumn(name = "ID_TIAC")
	public TsccvTipoAccion getTsccvTipoAccion() {
		return this.tsccvTipoAccion;
	}

	public void setTsccvTipoAccion(TsccvTipoAccion tsccvTipoAccion) {
		this.tsccvTipoAccion = tsccvTipoAccion;
	}

	@ManyToOne
	@JoinColumn(name = "CODPLA", nullable = false)
	public TsccvPlanta getTsccvPlanta() {
		return this.tsccvPlanta;
	}

	public void setTsccvPlanta(TsccvPlanta tsccvPlanta) {
		this.tsccvPlanta = tsccvPlanta;
	}

	@ManyToOne
	@JoinColumn(name = "ID_GRUP", nullable = false)
	public TbmBaseGruposUsuarios getTbmBaseGruposUsuarios() {
		return this.tbmBaseGruposUsuarios;
	}

	public void setTbmBaseGruposUsuarios(
			TbmBaseGruposUsuarios tbmBaseGruposUsuarios) {
		this.tbmBaseGruposUsuarios = tbmBaseGruposUsuarios;
	}

	@ManyToOne
	@JoinColumn(name = "ID_USUA")
	public TsccvUsuario getTsccvUsuario() {
		return this.tsccvUsuario;
	}

	public void setTsccvUsuario(TsccvUsuario tsccvUsuario) {
		this.tsccvUsuario = tsccvUsuario;
	}

	@Column(name = "ACCESO", precision = 1, scale = 0)
	public Long getAcceso() {
		return this.acceso;
	}

	public void setAcceso(Long acceso) {
		this.acceso = acceso;
	}

	@Column(name = "PEDIDO", precision = 1, scale = 0)
	public Long getPedido() {
		return this.pedido;
	}

	public void setPedido(Long pedido) {
		this.pedido = pedido;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}