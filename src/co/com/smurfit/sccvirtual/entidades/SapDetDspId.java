package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SapDetDspId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class SapDetDspId implements java.io.Serializable {

	// Fields

	private String subdsp;
	private String numdsp;
	private String itmdsp;
	private String codcli;
	private String numped;
	private String itmped;
	private String subitm;
	private String fecdsp;
	private String candsp;
	private String undmed;
	private String emptra;
	private String placa;
	private String nomcon;
	private String dirdsp;
	private String cankgs;

	// Constructors

	/** default constructor */
	public SapDetDspId() {
	}

	/** minimal constructor */
	public SapDetDspId(String subdsp, String numdsp, String itmdsp) {
		this.subdsp = subdsp;
		this.numdsp = numdsp;
		this.itmdsp = itmdsp;
	}

	/** full constructor */
	public SapDetDspId(String subdsp, String numdsp, String itmdsp,
			String codcli, String numped, String itmped, String subitm,
			String fecdsp, String candsp, String undmed, String emptra,
			String placa, String nomcon, String dirdsp, String cankgs) {
		this.subdsp = subdsp;
		this.numdsp = numdsp;
		this.itmdsp = itmdsp;
		this.codcli = codcli;
		this.numped = numped;
		this.itmped = itmped;
		this.subitm = subitm;
		this.fecdsp = fecdsp;
		this.candsp = candsp;
		this.undmed = undmed;
		this.emptra = emptra;
		this.placa = placa;
		this.nomcon = nomcon;
		this.dirdsp = dirdsp;
		this.cankgs = cankgs;
	}

	// Property accessors

	@Column(name = "SUBDSP", nullable = false, length = 2)
	public String getSubdsp() {
		return this.subdsp;
	}

	public void setSubdsp(String subdsp) {
		this.subdsp = subdsp;
	}

	@Column(name = "NUMDSP", nullable = false, length = 8)
	public String getNumdsp() {
		return this.numdsp;
	}

	public void setNumdsp(String numdsp) {
		this.numdsp = numdsp;
	}

	@Column(name = "ITMDSP", nullable = false, length = 2)
	public String getItmdsp() {
		return this.itmdsp;
	}

	public void setItmdsp(String itmdsp) {
		this.itmdsp = itmdsp;
	}

	@Column(name = "CODCLI", length = 8)
	public String getCodcli() {
		return this.codcli;
	}

	public void setCodcli(String codcli) {
		this.codcli = codcli;
	}

	@Column(name = "NUMPED", length = 8)
	public String getNumped() {
		return this.numped;
	}

	public void setNumped(String numped) {
		this.numped = numped;
	}

	@Column(name = "ITMPED", length = 3)
	public String getItmped() {
		return this.itmped;
	}

	public void setItmped(String itmped) {
		this.itmped = itmped;
	}

	@Column(name = "SUBITM", length = 2)
	public String getSubitm() {
		return this.subitm;
	}

	public void setSubitm(String subitm) {
		this.subitm = subitm;
	}

	@Column(name = "FECDSP", length = 10)
	public String getFecdsp() {
		return this.fecdsp;
	}

	public void setFecdsp(String fecdsp) {
		this.fecdsp = fecdsp;
	}

	@Column(name = "CANDSP", length = 16)
	public String getCandsp() {
		return this.candsp;
	}

	public void setCandsp(String candsp) {
		this.candsp = candsp;
	}

	@Column(name = "UNDMED", length = 10)
	public String getUndmed() {
		return this.undmed;
	}

	public void setUndmed(String undmed) {
		this.undmed = undmed;
	}

	@Column(name = "EMPTRA", length = 32)
	public String getEmptra() {
		return this.emptra;
	}

	public void setEmptra(String emptra) {
		this.emptra = emptra;
	}

	@Column(name = "PLACA", length = 15)
	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	@Column(name = "NOMCON", length = 40)
	public String getNomcon() {
		return this.nomcon;
	}

	public void setNomcon(String nomcon) {
		this.nomcon = nomcon;
	}

	@Column(name = "DIRDSP", length = 80)
	public String getDirdsp() {
		return this.dirdsp;
	}

	public void setDirdsp(String dirdsp) {
		this.dirdsp = dirdsp;
	}

	@Column(name = "CANKGS", length = 16)
	public String getCankgs() {
		return this.cankgs;
	}

	public void setCankgs(String cankgs) {
		this.cankgs = cankgs;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SapDetDspId))
			return false;
		SapDetDspId castOther = (SapDetDspId) other;

		return ((this.getSubdsp() == castOther.getSubdsp()) || (this
				.getSubdsp() != null
				&& castOther.getSubdsp() != null && this.getSubdsp().equals(
				castOther.getSubdsp())))
				&& ((this.getNumdsp() == castOther.getNumdsp()) || (this
						.getNumdsp() != null
						&& castOther.getNumdsp() != null && this.getNumdsp()
						.equals(castOther.getNumdsp())))
				&& ((this.getItmdsp() == castOther.getItmdsp()) || (this
						.getItmdsp() != null
						&& castOther.getItmdsp() != null && this.getItmdsp()
						.equals(castOther.getItmdsp())))
				&& ((this.getCodcli() == castOther.getCodcli()) || (this
						.getCodcli() != null
						&& castOther.getCodcli() != null && this.getCodcli()
						.equals(castOther.getCodcli())))
				&& ((this.getNumped() == castOther.getNumped()) || (this
						.getNumped() != null
						&& castOther.getNumped() != null && this.getNumped()
						.equals(castOther.getNumped())))
				&& ((this.getItmped() == castOther.getItmped()) || (this
						.getItmped() != null
						&& castOther.getItmped() != null && this.getItmped()
						.equals(castOther.getItmped())))
				&& ((this.getSubitm() == castOther.getSubitm()) || (this
						.getSubitm() != null
						&& castOther.getSubitm() != null && this.getSubitm()
						.equals(castOther.getSubitm())))
				&& ((this.getFecdsp() == castOther.getFecdsp()) || (this
						.getFecdsp() != null
						&& castOther.getFecdsp() != null && this.getFecdsp()
						.equals(castOther.getFecdsp())))
				&& ((this.getCandsp() == castOther.getCandsp()) || (this
						.getCandsp() != null
						&& castOther.getCandsp() != null && this.getCandsp()
						.equals(castOther.getCandsp())))
				&& ((this.getUndmed() == castOther.getUndmed()) || (this
						.getUndmed() != null
						&& castOther.getUndmed() != null && this.getUndmed()
						.equals(castOther.getUndmed())))
				&& ((this.getEmptra() == castOther.getEmptra()) || (this
						.getEmptra() != null
						&& castOther.getEmptra() != null && this.getEmptra()
						.equals(castOther.getEmptra())))
				&& ((this.getPlaca() == castOther.getPlaca()) || (this
						.getPlaca() != null
						&& castOther.getPlaca() != null && this.getPlaca()
						.equals(castOther.getPlaca())))
				&& ((this.getNomcon() == castOther.getNomcon()) || (this
						.getNomcon() != null
						&& castOther.getNomcon() != null && this.getNomcon()
						.equals(castOther.getNomcon())))
				&& ((this.getDirdsp() == castOther.getDirdsp()) || (this
						.getDirdsp() != null
						&& castOther.getDirdsp() != null && this.getDirdsp()
						.equals(castOther.getDirdsp())))
				&& ((this.getCankgs() == castOther.getCankgs()) || (this
						.getCankgs() != null
						&& castOther.getCankgs() != null && this.getCankgs()
						.equals(castOther.getCankgs())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getSubdsp() == null ? 0 : this.getSubdsp().hashCode());
		result = 37 * result
				+ (getNumdsp() == null ? 0 : this.getNumdsp().hashCode());
		result = 37 * result
				+ (getItmdsp() == null ? 0 : this.getItmdsp().hashCode());
		result = 37 * result
				+ (getCodcli() == null ? 0 : this.getCodcli().hashCode());
		result = 37 * result
				+ (getNumped() == null ? 0 : this.getNumped().hashCode());
		result = 37 * result
				+ (getItmped() == null ? 0 : this.getItmped().hashCode());
		result = 37 * result
				+ (getSubitm() == null ? 0 : this.getSubitm().hashCode());
		result = 37 * result
				+ (getFecdsp() == null ? 0 : this.getFecdsp().hashCode());
		result = 37 * result
				+ (getCandsp() == null ? 0 : this.getCandsp().hashCode());
		result = 37 * result
				+ (getUndmed() == null ? 0 : this.getUndmed().hashCode());
		result = 37 * result
				+ (getEmptra() == null ? 0 : this.getEmptra().hashCode());
		result = 37 * result
				+ (getPlaca() == null ? 0 : this.getPlaca().hashCode());
		result = 37 * result
				+ (getNomcon() == null ? 0 : this.getNomcon().hashCode());
		result = 37 * result
				+ (getDirdsp() == null ? 0 : this.getDirdsp().hashCode());
		result = 37 * result
				+ (getCankgs() == null ? 0 : this.getCankgs().hashCode());
		return result;
	}

}