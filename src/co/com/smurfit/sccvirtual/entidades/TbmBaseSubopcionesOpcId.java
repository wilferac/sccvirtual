package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TbmBaseSubopcionesOpcId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class TbmBaseSubopcionesOpcId implements java.io.Serializable {

	// Fields

	private Long idOpci;
	private Long idSuop;
	private String nombreVisible;
	private Date fechaCreacion;
	private Long posicion;

	// Constructors

	/** default constructor */
	public TbmBaseSubopcionesOpcId() {
	}

	/** minimal constructor */
	public TbmBaseSubopcionesOpcId(String nombreVisible, Date fechaCreacion,
			Long posicion) {
		this.nombreVisible = nombreVisible;
		this.fechaCreacion = fechaCreacion;
		this.posicion = posicion;
	}

	/** full constructor */
	public TbmBaseSubopcionesOpcId(Long idOpci, Long idSuop,
			String nombreVisible, Date fechaCreacion, Long posicion) {
		this.idOpci = idOpci;
		this.idSuop = idSuop;
		this.nombreVisible = nombreVisible;
		this.fechaCreacion = fechaCreacion;
		this.posicion = posicion;
	}

	// Property accessors

	@Column(name = "ID_OPCI", precision = 2, scale = 0)
	public Long getIdOpci() {
		return this.idOpci;
	}

	public void setIdOpci(Long idOpci) {
		this.idOpci = idOpci;
	}

	@Column(name = "ID_SUOP", precision = 2, scale = 0)
	public Long getIdSuop() {
		return this.idSuop;
	}

	public void setIdSuop(Long idSuop) {
		this.idSuop = idSuop;
	}

	@Column(name = "NOMBRE_VISIBLE", nullable = false, length = 40)
	public String getNombreVisible() {
		return this.nombreVisible;
	}

	public void setNombreVisible(String nombreVisible) {
		this.nombreVisible = nombreVisible;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@Column(name = "POSICION", nullable = false, precision = 2, scale = 0)
	public Long getPosicion() {
		return this.posicion;
	}

	public void setPosicion(Long posicion) {
		this.posicion = posicion;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TbmBaseSubopcionesOpcId))
			return false;
		TbmBaseSubopcionesOpcId castOther = (TbmBaseSubopcionesOpcId) other;

		return ((this.getIdOpci() == castOther.getIdOpci()) || (this
				.getIdOpci() != null
				&& castOther.getIdOpci() != null && this.getIdOpci().equals(
				castOther.getIdOpci())))
				&& ((this.getIdSuop() == castOther.getIdSuop()) || (this
						.getIdSuop() != null
						&& castOther.getIdSuop() != null && this.getIdSuop()
						.equals(castOther.getIdSuop())))
				&& ((this.getNombreVisible() == castOther.getNombreVisible()) || (this
						.getNombreVisible() != null
						&& castOther.getNombreVisible() != null && this
						.getNombreVisible()
						.equals(castOther.getNombreVisible())))
				&& ((this.getFechaCreacion() == castOther.getFechaCreacion()) || (this
						.getFechaCreacion() != null
						&& castOther.getFechaCreacion() != null && this
						.getFechaCreacion()
						.equals(castOther.getFechaCreacion())))
				&& ((this.getPosicion() == castOther.getPosicion()) || (this
						.getPosicion() != null
						&& castOther.getPosicion() != null && this
						.getPosicion().equals(castOther.getPosicion())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getIdOpci() == null ? 0 : this.getIdOpci().hashCode());
		result = 37 * result
				+ (getIdSuop() == null ? 0 : this.getIdSuop().hashCode());
		result = 37
				* result
				+ (getNombreVisible() == null ? 0 : this.getNombreVisible()
						.hashCode());
		result = 37
				* result
				+ (getFechaCreacion() == null ? 0 : this.getFechaCreacion()
						.hashCode());
		result = 37 * result
				+ (getPosicion() == null ? 0 : this.getPosicion().hashCode());
		return result;
	}

}