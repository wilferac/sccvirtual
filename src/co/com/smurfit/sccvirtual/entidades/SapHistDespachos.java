package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * SapHistDespachos entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "SAP_HIST_DESPACHOS", schema = "dbo")
public class SapHistDespachos implements java.io.Serializable {

	// Fields

	private SapHistDespachosId id;

	// Constructors

	/** default constructor */
	public SapHistDespachos() {
	}

	/** full constructor */
	public SapHistDespachos(SapHistDespachosId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codpro", column = @Column(name = "CODPRO", nullable = false, length = 10)),
			@AttributeOverride(name = "nummes", column = @Column(name = "NUMMES", nullable = false, length = 6)),
			@AttributeOverride(name = "codcli", column = @Column(name = "CODCLI", nullable = false, length = 8)),
			@AttributeOverride(name = "cantid", column = @Column(name = "CANTID", length = 16)),
			@AttributeOverride(name = "despro", column = @Column(name = "DESPRO", length = 60)),
			@AttributeOverride(name = "undmed", column = @Column(name = "UNDMED", length = 10)),
			@AttributeOverride(name = "cankgs", column = @Column(name = "CANKGS", length = 16)) })
	public SapHistDespachosId getId() {
		return this.id;
	}

	public void setId(SapHistDespachosId id) {
		this.id = id;
	}

}