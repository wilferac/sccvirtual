package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvGrupoEmpresa entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_GRUPO_EMPRESA", schema = "dbo")
public class TsccvGrupoEmpresa implements java.io.Serializable {

	// Fields

	private Integer idGrue;
	private Date fechaCreacion;
	private Set<TsccvEmpresas> tsccvEmpresases = new HashSet<TsccvEmpresas>(0);
	private Set<TsccvDetalleGrupoEmpresa> tsccvDetalleGrupoEmpresas = new HashSet<TsccvDetalleGrupoEmpresa>(
			0);

	// Constructors

	/** default constructor */
	public TsccvGrupoEmpresa() {
	}

	/** minimal constructor */
	public TsccvGrupoEmpresa(Integer idGrue, Date fechaCreacion) {
		this.idGrue = idGrue;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TsccvGrupoEmpresa(Integer idGrue, Date fechaCreacion,
			Set<TsccvEmpresas> tsccvEmpresases,
			Set<TsccvDetalleGrupoEmpresa> tsccvDetalleGrupoEmpresas) {
		this.idGrue = idGrue;
		this.fechaCreacion = fechaCreacion;
		this.tsccvEmpresases = tsccvEmpresases;
		this.tsccvDetalleGrupoEmpresas = tsccvDetalleGrupoEmpresas;
	}

	// Property accessors
	@Id
	@Column(name = "ID_GRUE", unique = true, nullable = false)
	public Integer getIdGrue() {
		return this.idGrue;
	}

	public void setIdGrue(Integer idGrue) {
		this.idGrue = idGrue;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvGrupoEmpresa")
	public Set<TsccvEmpresas> getTsccvEmpresases() {
		return this.tsccvEmpresases;
	}

	public void setTsccvEmpresases(Set<TsccvEmpresas> tsccvEmpresases) {
		this.tsccvEmpresases = tsccvEmpresases;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvGrupoEmpresa")
	public Set<TsccvDetalleGrupoEmpresa> getTsccvDetalleGrupoEmpresas() {
		return this.tsccvDetalleGrupoEmpresas;
	}

	public void setTsccvDetalleGrupoEmpresas(
			Set<TsccvDetalleGrupoEmpresa> tsccvDetalleGrupoEmpresas) {
		this.tsccvDetalleGrupoEmpresas = tsccvDetalleGrupoEmpresas;
	}

}