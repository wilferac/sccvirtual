package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * CarteraDiv entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CARTERA_DIV", schema = "dbo")
public class CarteraDiv implements java.io.Serializable {

	// Fields

	private CarteraDivId id;

	// Constructors

	/** default constructor */
	public CarteraDiv() {
	}

	/** full constructor */
	public CarteraDiv(CarteraDivId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "coddiv", column = @Column(name = "CODDIV", nullable = false, length = 4)),
			@AttributeOverride(name = "desdiv", column = @Column(name = "DESDIV", length = 30)) })
	public CarteraDivId getId() {
		return this.id;
	}

	public void setId(CarteraDivId id) {
		this.id = id;
	}

}