package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * SapDirDsp entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "SAP_DIR_DSP", schema = "dbo")
public class SapDirDsp implements java.io.Serializable {

	// Fields

	private SapDirDspId id;

	// Constructors

	/** default constructor */
	public SapDirDsp() {
	}

	/** full constructor */
	public SapDirDsp(SapDirDspId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codcli", column = @Column(name = "CODCLI", nullable = false, length = 2)),
			@AttributeOverride(name = "coddir", column = @Column(name = "CODDIR", nullable = false, length = 2)),
			@AttributeOverride(name = "desdir", column = @Column(name = "DESDIR", nullable = false, length = 80)),
			@AttributeOverride(name = "numfax", column = @Column(name = "NUMFAX", length = 20)),
			@AttributeOverride(name = "desprv", column = @Column(name = "DESPRV", length = 20)),
			@AttributeOverride(name = "desciu", column = @Column(name = "DESCIU", length = 30)),
			@AttributeOverride(name = "despai", column = @Column(name = "DESPAI", length = 20)) })
	public SapDirDspId getId() {
		return this.id;
	}

	public void setId(SapDirDspId id) {
		this.id = id;
	}

}