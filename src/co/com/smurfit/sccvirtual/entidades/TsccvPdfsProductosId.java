package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * TsccvPdfsProductosId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class TsccvPdfsProductosId implements java.io.Serializable {

	// Fields

	private String codScc;
	private String codpla;

	// Constructors

	/** default constructor */
	public TsccvPdfsProductosId() {
	}

	/** full constructor */
	public TsccvPdfsProductosId(String codScc, String codpla) {
		this.codScc = codScc;
		this.codpla = codpla;
	}

	// Property accessors

	@Column(name = "COD_SCC", nullable = false, length = 8)
	public String getCodScc() {
		return this.codScc;
	}

	public void setCodScc(String codScc) {
		this.codScc = codScc;
	}

	@Column(name = "CODPLA", nullable = false, length = 2)
	public String getCodpla() {
		return this.codpla;
	}

	public void setCodpla(String codpla) {
		this.codpla = codpla;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TsccvPdfsProductosId))
			return false;
		TsccvPdfsProductosId castOther = (TsccvPdfsProductosId) other;

		return ((this.getCodScc() == castOther.getCodScc()) || (this
				.getCodScc() != null
				&& castOther.getCodScc() != null && this.getCodScc().equals(
				castOther.getCodScc())))
				&& ((this.getCodpla() == castOther.getCodpla()) || (this
						.getCodpla() != null
						&& castOther.getCodpla() != null && this.getCodpla()
						.equals(castOther.getCodpla())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCodScc() == null ? 0 : this.getCodScc().hashCode());
		result = 37 * result
				+ (getCodpla() == null ? 0 : this.getCodpla().hashCode());
		return result;
	}

}