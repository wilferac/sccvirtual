package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvAccesosUsuario entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_ACCESOS_USUARIO", schema = "dbo")
public class TsccvAccesosUsuario implements java.io.Serializable {

	// Fields

	private Integer idAcus;
	private TsccvEmpresas tsccvEmpresas;
	private TsccvUsuario tsccvUsuario;
	private Date fechaAcceso;

	// Constructors

	/** default constructor */
	public TsccvAccesosUsuario() {
	}

	/** minimal constructor */
	public TsccvAccesosUsuario(Integer idAcus, Date fechaAcceso) {
		this.idAcus = idAcus;
		this.fechaAcceso = fechaAcceso;
	}

	/** full constructor */
	public TsccvAccesosUsuario(Integer idAcus, TsccvEmpresas tsccvEmpresas,
			TsccvUsuario tsccvUsuario, Date fechaAcceso) {
		this.idAcus = idAcus;
		this.tsccvEmpresas = tsccvEmpresas;
		this.tsccvUsuario = tsccvUsuario;
		this.fechaAcceso = fechaAcceso;
	}

	// Property accessors
	@Id
	@Column(name = "ID_ACUS", unique = true, nullable = false)
	public Integer getIdAcus() {
		return this.idAcus;
	}

	public void setIdAcus(Integer idAcus) {
		this.idAcus = idAcus;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EMPR")
	public TsccvEmpresas getTsccvEmpresas() {
		return this.tsccvEmpresas;
	}

	public void setTsccvEmpresas(TsccvEmpresas tsccvEmpresas) {
		this.tsccvEmpresas = tsccvEmpresas;
	}

	@ManyToOne
	@JoinColumn(name = "ID_USUA")
	public TsccvUsuario getTsccvUsuario() {
		return this.tsccvUsuario;
	}

	public void setTsccvUsuario(TsccvUsuario tsccvUsuario) {
		this.tsccvUsuario = tsccvUsuario;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ACCESO", nullable = false, length = 23)
	public Date getFechaAcceso() {
		return this.fechaAcceso;
	}

	public void setFechaAcceso(Date fechaAcceso) {
		this.fechaAcceso = fechaAcceso;
	}

}