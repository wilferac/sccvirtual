package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvDetalleGrupoEmpresa entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_DETALLE_GRUPO_EMPRESA", schema = "dbo")
public class TsccvDetalleGrupoEmpresa implements java.io.Serializable {

	// Fields

	private TsccvDetalleGrupoEmpresaId id;
	private TsccvGrupoEmpresa tsccvGrupoEmpresa;
	private TsccvEmpresas tsccvEmpresas;
	private Date fechaCreacion;

	// Constructors

	/** default constructor */
	public TsccvDetalleGrupoEmpresa() {
	}

	/** full constructor */
	public TsccvDetalleGrupoEmpresa(TsccvDetalleGrupoEmpresaId id,
			TsccvGrupoEmpresa tsccvGrupoEmpresa, TsccvEmpresas tsccvEmpresas,
			Date fechaCreacion) {
		this.id = id;
		this.tsccvGrupoEmpresa = tsccvGrupoEmpresa;
		this.tsccvEmpresas = tsccvEmpresas;
		this.fechaCreacion = fechaCreacion;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idGrue", column = @Column(name = "ID_GRUE", nullable = false)),
			@AttributeOverride(name = "idEmpr", column = @Column(name = "ID_EMPR", nullable = false)) })
	public TsccvDetalleGrupoEmpresaId getId() {
		return this.id;
	}

	public void setId(TsccvDetalleGrupoEmpresaId id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_GRUE", nullable = false, insertable = false, updatable = false)
	public TsccvGrupoEmpresa getTsccvGrupoEmpresa() {
		return this.tsccvGrupoEmpresa;
	}

	public void setTsccvGrupoEmpresa(TsccvGrupoEmpresa tsccvGrupoEmpresa) {
		this.tsccvGrupoEmpresa = tsccvGrupoEmpresa;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EMPR", nullable = false, insertable = false, updatable = false)
	public TsccvEmpresas getTsccvEmpresas() {
		return this.tsccvEmpresas;
	}

	public void setTsccvEmpresas(TsccvEmpresas tsccvEmpresas) {
		this.tsccvEmpresas = tsccvEmpresas;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}