package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * SapDetDsp entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "SAP_DET_DSP", schema = "dbo")
public class SapDetDsp implements java.io.Serializable {

	// Fields

	private SapDetDspId id;

	// Constructors

	/** default constructor */
	public SapDetDsp() {
	}

	/** full constructor */
	public SapDetDsp(SapDetDspId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "subdsp", column = @Column(name = "SUBDSP", nullable = false, length = 2)),
			@AttributeOverride(name = "numdsp", column = @Column(name = "NUMDSP", nullable = false, length = 8)),
			@AttributeOverride(name = "itmdsp", column = @Column(name = "ITMDSP", nullable = false, length = 2)),
			@AttributeOverride(name = "codcli", column = @Column(name = "CODCLI", length = 8)),
			@AttributeOverride(name = "numped", column = @Column(name = "NUMPED", length = 8)),
			@AttributeOverride(name = "itmped", column = @Column(name = "ITMPED", length = 3)),
			@AttributeOverride(name = "subitm", column = @Column(name = "SUBITM", length = 2)),
			@AttributeOverride(name = "fecdsp", column = @Column(name = "FECDSP", length = 10)),
			@AttributeOverride(name = "candsp", column = @Column(name = "CANDSP", length = 16)),
			@AttributeOverride(name = "undmed", column = @Column(name = "UNDMED", length = 10)),
			@AttributeOverride(name = "emptra", column = @Column(name = "EMPTRA", length = 32)),
			@AttributeOverride(name = "placa", column = @Column(name = "PLACA", length = 15)),
			@AttributeOverride(name = "nomcon", column = @Column(name = "NOMCON", length = 40)),
			@AttributeOverride(name = "dirdsp", column = @Column(name = "DIRDSP", length = 80)),
			@AttributeOverride(name = "cankgs", column = @Column(name = "CANKGS", length = 16)) })
	public SapDetDspId getId() {
		return this.id;
	}

	public void setId(SapDetDspId id) {
		this.id = id;
	}

}