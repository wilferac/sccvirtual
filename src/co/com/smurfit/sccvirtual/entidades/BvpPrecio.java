package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * BvpPrecio entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "BVP_PRECIO", schema = "dbo")
public class BvpPrecio implements java.io.Serializable {

	// Fields

	private BvpPrecioId id;
	private String tippro;
	private String codpro;
	private String despro;
	private String undmed;
	private String refere;
	private String numcol;
	private String largo;
	private String ancho;
	private String alto;
	private String clave;
	private String estilo;
	private String larsac;
	private String ancsac;
	private String fuelle;
	private String moneda;
	private String kitpro;
	private String hijpro;
	private String cntmin;
	private String cntmax;

	// Constructors

	/** default constructor */
	public BvpPrecio() {
	}

	/** minimal constructor */
	public BvpPrecio(BvpPrecioId id) {
		this.id = id;
	}

	/** full constructor */
	public BvpPrecio(BvpPrecioId id, String tippro, String codpro,
			String despro, String undmed, String refere, String numcol,
			String largo, String ancho, String alto, String clave,
			String estilo, String larsac, String ancsac, String fuelle,
			String moneda, String kitpro, String hijpro, String cntmin,
			String cntmax) {
		this.id = id;
		this.tippro = tippro;
		this.codpro = codpro;
		this.despro = despro;
		this.undmed = undmed;
		this.refere = refere;
		this.numcol = numcol;
		this.largo = largo;
		this.ancho = ancho;
		this.alto = alto;
		this.clave = clave;
		this.estilo = estilo;
		this.larsac = larsac;
		this.ancsac = ancsac;
		this.fuelle = fuelle;
		this.moneda = moneda;
		this.kitpro = kitpro;
		this.hijpro = hijpro;
		this.cntmin = cntmin;
		this.cntmax = cntmax;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codCli", column = @Column(name = "COD_CLI", nullable = false, length = 8)),
			@AttributeOverride(name = "codpla", column = @Column(name = "CODPLA", nullable = false, length = 2)),
			@AttributeOverride(name = "codScc", column = @Column(name = "COD_SCC", nullable = false, length = 8)),
			@AttributeOverride(name = "precio", column = @Column(name = "PRECIO", nullable = false, length = 16)) })
	public BvpPrecioId getId() {
		return this.id;
	}

	public void setId(BvpPrecioId id) {
		this.id = id;
	}

	@Column(name = "TIPPRO", length = 1)
	public String getTippro() {
		return this.tippro;
	}

	public void setTippro(String tippro) {
		this.tippro = tippro;
	}

	@Column(name = "CODPRO", length = 20)
	public String getCodpro() {
		return this.codpro;
	}

	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}

	@Column(name = "DESPRO", length = 60)
	public String getDespro() {
		return this.despro;
	}

	public void setDespro(String despro) {
		this.despro = despro;
	}

	@Column(name = "UNDMED", length = 20)
	public String getUndmed() {
		return this.undmed;
	}

	public void setUndmed(String undmed) {
		this.undmed = undmed;
	}

	@Column(name = "REFERE", length = 20)
	public String getRefere() {
		return this.refere;
	}

	public void setRefere(String refere) {
		this.refere = refere;
	}

	@Column(name = "NUMCOL", length = 20)
	public String getNumcol() {
		return this.numcol;
	}

	public void setNumcol(String numcol) {
		this.numcol = numcol;
	}

	@Column(name = "LARGO", length = 20)
	public String getLargo() {
		return this.largo;
	}

	public void setLargo(String largo) {
		this.largo = largo;
	}

	@Column(name = "ANCHO", length = 20)
	public String getAncho() {
		return this.ancho;
	}

	public void setAncho(String ancho) {
		this.ancho = ancho;
	}

	@Column(name = "ALTO", length = 20)
	public String getAlto() {
		return this.alto;
	}

	public void setAlto(String alto) {
		this.alto = alto;
	}

	@Column(name = "CLAVE", length = 20)
	public String getClave() {
		return this.clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	@Column(name = "ESTILO", length = 20)
	public String getEstilo() {
		return this.estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	@Column(name = "LARSAC", length = 20)
	public String getLarsac() {
		return this.larsac;
	}

	public void setLarsac(String larsac) {
		this.larsac = larsac;
	}

	@Column(name = "ANCSAC", length = 20)
	public String getAncsac() {
		return this.ancsac;
	}

	public void setAncsac(String ancsac) {
		this.ancsac = ancsac;
	}

	@Column(name = "FUELLE", length = 20)
	public String getFuelle() {
		return this.fuelle;
	}

	public void setFuelle(String fuelle) {
		this.fuelle = fuelle;
	}

	@Column(name = "MONEDA", length = 10)
	public String getMoneda() {
		return this.moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Column(name = "KITPRO", length = 20)
	public String getKitpro() {
		return this.kitpro;
	}

	public void setKitpro(String kitpro) {
		this.kitpro = kitpro;
	}

	@Column(name = "HIJPRO", length = 1)
	public String getHijpro() {
		return this.hijpro;
	}

	public void setHijpro(String hijpro) {
		this.hijpro = hijpro;
	}

	@Column(name = "CNTMIN", length = 17)
	public String getCntmin() {
		return this.cntmin;
	}

	public void setCntmin(String cntmin) {
		this.cntmin = cntmin;
	}

	@Column(name = "CNTMAX", length = 50)
	public String getCntmax() {
		return this.cntmax;
	}

	public void setCntmax(String cntmax) {
		this.cntmax = cntmax;
	}

}