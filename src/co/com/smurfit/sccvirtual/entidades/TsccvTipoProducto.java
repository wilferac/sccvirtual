package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvTipoProducto entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_TIPO_PRODUCTO", schema = "dbo")
public class TsccvTipoProducto implements java.io.Serializable {

	// Fields

	private Long idTipr;
	private String descripcion;
	private String activo;
	private Date fechaCreacion;
	private Set<TsccvPlanta> tsccvPlantas = new HashSet<TsccvPlanta>(0);

	// Constructors

	/** default constructor */
	public TsccvTipoProducto() {
	}

	/** minimal constructor */
	public TsccvTipoProducto(Long idTipr, String descripcion, String activo,
			Date fechaCreacion) {
		this.idTipr = idTipr;
		this.descripcion = descripcion;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TsccvTipoProducto(Long idTipr, String descripcion, String activo,
			Date fechaCreacion, Set<TsccvPlanta> tsccvPlantas) {
		this.idTipr = idTipr;
		this.descripcion = descripcion;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.tsccvPlantas = tsccvPlantas;
	}

	// Property accessors
	@Id
	@Column(name = "ID_TIPR", unique = true, nullable = false, precision = 18, scale = 0)
	public Long getIdTipr() {
		return this.idTipr;
	}

	public void setIdTipr(Long idTipr) {
		this.idTipr = idTipr;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 15)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "ACTIVO", nullable = false, length = 1)
	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvTipoProducto")
	public Set<TsccvPlanta> getTsccvPlantas() {
		return this.tsccvPlantas;
	}

	public void setTsccvPlantas(Set<TsccvPlanta> tsccvPlantas) {
		this.tsccvPlantas = tsccvPlantas;
	}

}