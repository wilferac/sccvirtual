package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * CarteraDivId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class CarteraDivId implements java.io.Serializable {

	// Fields

	private String coddiv;
	private String desdiv;

	// Constructors

	/** default constructor */
	public CarteraDivId() {
	}

	/** minimal constructor */
	public CarteraDivId(String coddiv) {
		this.coddiv = coddiv;
	}

	/** full constructor */
	public CarteraDivId(String coddiv, String desdiv) {
		this.coddiv = coddiv;
		this.desdiv = desdiv;
	}

	// Property accessors

	@Column(name = "CODDIV", nullable = false, length = 4)
	public String getCoddiv() {
		return this.coddiv;
	}

	public void setCoddiv(String coddiv) {
		this.coddiv = coddiv;
	}

	@Column(name = "DESDIV", length = 30)
	public String getDesdiv() {
		return this.desdiv;
	}

	public void setDesdiv(String desdiv) {
		this.desdiv = desdiv;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CarteraDivId))
			return false;
		CarteraDivId castOther = (CarteraDivId) other;

		return ((this.getCoddiv() == castOther.getCoddiv()) || (this
				.getCoddiv() != null
				&& castOther.getCoddiv() != null && this.getCoddiv().equals(
				castOther.getCoddiv())))
				&& ((this.getDesdiv() == castOther.getDesdiv()) || (this
						.getDesdiv() != null
						&& castOther.getDesdiv() != null && this.getDesdiv()
						.equals(castOther.getDesdiv())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCoddiv() == null ? 0 : this.getCoddiv().hashCode());
		result = 37 * result
				+ (getDesdiv() == null ? 0 : this.getDesdiv().hashCode());
		return result;
	}

}