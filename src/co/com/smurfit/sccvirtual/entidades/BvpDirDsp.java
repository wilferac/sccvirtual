package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * BvpDirDsp entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "BVP_DIR_DSP", schema = "dbo")
public class BvpDirDsp implements java.io.Serializable {

	// Fields

	private BvpDirDspId id;

	// Constructors

	/** default constructor */
	public BvpDirDsp() {
	}

	/** full constructor */
	public BvpDirDsp(BvpDirDspId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codcli", column = @Column(name = "CODCLI", nullable = false, length = 2)),
			@AttributeOverride(name = "coddir", column = @Column(name = "CODDIR", nullable = false, length = 2)),
			@AttributeOverride(name = "codpla", column = @Column(name = "CODPLA", nullable = false, length = 2)),
			@AttributeOverride(name = "desdir", column = @Column(name = "DESDIR", nullable = false, length = 80)),
			@AttributeOverride(name = "numfax", column = @Column(name = "NUMFAX", length = 20)),
			@AttributeOverride(name = "desprv", column = @Column(name = "DESPRV", length = 20)) })
	public BvpDirDspId getId() {
		return this.id;
	}

	public void setId(BvpDirDspId id) {
		this.id = id;
	}

}