package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvPlantasUsuario entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_PLANTAS_USUARIO", schema = "dbo")
public class TsccvPlantasUsuario implements java.io.Serializable {

	// Fields

	private TsccvPlantasUsuarioId id;
	private TsccvPlanta tsccvPlanta;
	private TsccvUsuario tsccvUsuario;
	private Date fechaCreacion;

	// Constructors

	/** default constructor */
	public TsccvPlantasUsuario() {
	}

	/** full constructor */
	public TsccvPlantasUsuario(TsccvPlantasUsuarioId id,
			TsccvPlanta tsccvPlanta, TsccvUsuario tsccvUsuario,
			Date fechaCreacion) {
		this.id = id;
		this.tsccvPlanta = tsccvPlanta;
		this.tsccvUsuario = tsccvUsuario;
		this.fechaCreacion = fechaCreacion;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codpla", column = @Column(name = "CODPLA", nullable = false, length = 2)),
			@AttributeOverride(name = "idUsua", column = @Column(name = "ID_USUA", nullable = false)) })
	public TsccvPlantasUsuarioId getId() {
		return this.id;
	}

	public void setId(TsccvPlantasUsuarioId id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "CODPLA", nullable = false, insertable = false, updatable = false)
	public TsccvPlanta getTsccvPlanta() {
		return this.tsccvPlanta;
	}

	public void setTsccvPlanta(TsccvPlanta tsccvPlanta) {
		this.tsccvPlanta = tsccvPlanta;
	}

	@ManyToOne
	@JoinColumn(name = "ID_USUA", nullable = false, insertable = false, updatable = false)
	public TsccvUsuario getTsccvUsuario() {
		return this.tsccvUsuario;
	}

	public void setTsccvUsuario(TsccvUsuario tsccvUsuario) {
		this.tsccvUsuario = tsccvUsuario;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

}