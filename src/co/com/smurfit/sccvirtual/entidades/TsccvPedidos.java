package co.com.smurfit.sccvirtual.entidades;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvPedidos entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_PEDIDOS", schema = "dbo")
public class TsccvPedidos implements java.io.Serializable {

	// Fields

	private Integer idPedi;
	private TsccvEmpresas tsccvEmpresas;
	private TsccvPlanta tsccvPlanta;
	private TsccvUsuario tsccvUsuario;
	private String registrado;
	private String observaciones;
	private String activo;
	private Date fechaCreacion;
	private Set<TsccvProductosPedido> tsccvProductosPedidos = new HashSet<TsccvProductosPedido>(
			0);

	// Constructors

	/** default constructor */
	public TsccvPedidos() {
	}

	/** minimal constructor */
	public TsccvPedidos(Integer idPedi, String registrado, String activo,
			Date fechaCreacion) {
		this.idPedi = idPedi;
		this.registrado = registrado;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TsccvPedidos(Integer idPedi, TsccvEmpresas tsccvEmpresas,
			TsccvPlanta tsccvPlanta, TsccvUsuario tsccvUsuario,
			String registrado, String observaciones, String activo,
			Date fechaCreacion, Set<TsccvProductosPedido> tsccvProductosPedidos) {
		this.idPedi = idPedi;
		this.tsccvEmpresas = tsccvEmpresas;
		this.tsccvPlanta = tsccvPlanta;
		this.tsccvUsuario = tsccvUsuario;
		this.registrado = registrado;
		this.observaciones = observaciones;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.tsccvProductosPedidos = tsccvProductosPedidos;
	}

	// Property accessors
	@Id
	@Column(name = "ID_PEDI", unique = true, nullable = false)
	public Integer getIdPedi() {
		return this.idPedi;
	}

	public void setIdPedi(Integer idPedi) {
		this.idPedi = idPedi;
	}

	@ManyToOne
	@JoinColumn(name = "ID_EMPR")
	public TsccvEmpresas getTsccvEmpresas() {
		return this.tsccvEmpresas;
	}

	public void setTsccvEmpresas(TsccvEmpresas tsccvEmpresas) {
		this.tsccvEmpresas = tsccvEmpresas;
	}

	@ManyToOne
	@JoinColumn(name = "CODPLA")
	public TsccvPlanta getTsccvPlanta() {
		return this.tsccvPlanta;
	}

	public void setTsccvPlanta(TsccvPlanta tsccvPlanta) {
		this.tsccvPlanta = tsccvPlanta;
	}

	@ManyToOne
	@JoinColumn(name = "ID_USUA")
	public TsccvUsuario getTsccvUsuario() {
		return this.tsccvUsuario;
	}

	public void setTsccvUsuario(TsccvUsuario tsccvUsuario) {
		this.tsccvUsuario = tsccvUsuario;
	}

	@Column(name = "REGISTRADO", nullable = false, length = 1)
	public String getRegistrado() {
		return this.registrado;
	}

	public void setRegistrado(String registrado) {
		this.registrado = registrado;
	}

	@Column(name = "OBSERVACIONES", length = 1000)
	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Column(name = "ACTIVO", nullable = false, length = 1)
	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvPedidos")
	public Set<TsccvProductosPedido> getTsccvProductosPedidos() {
		return this.tsccvProductosPedidos;
	}

	public void setTsccvProductosPedidos(
			Set<TsccvProductosPedido> tsccvProductosPedidos) {
		this.tsccvProductosPedidos = tsccvProductosPedidos;
	}

}