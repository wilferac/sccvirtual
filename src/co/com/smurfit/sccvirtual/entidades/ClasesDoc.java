package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * ClasesDoc entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CLASES_DOC", schema = "dbo")
public class ClasesDoc implements java.io.Serializable {

	// Fields

	private ClasesDocId id;

	// Constructors

	/** default constructor */
	public ClasesDoc() {
	}

	/** full constructor */
	public ClasesDoc(ClasesDocId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "coddoc", column = @Column(name = "CODDOC", nullable = false, length = 2)),
			@AttributeOverride(name = "desdoc", column = @Column(name = "DESDOC", length = 22)) })
	public ClasesDocId getId() {
		return this.id;
	}

	public void setId(ClasesDocId id) {
		this.id = id;
	}

}