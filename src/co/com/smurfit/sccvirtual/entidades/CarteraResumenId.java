package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * CarteraResumenId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class CarteraResumenId implements java.io.Serializable {

	// Fields

	private String codcli;
	private String numfac;
	private String clsdoc;
	private String fecfac;
	private String fecven;
	private String dias;
	private String monto;
	private String planta;
	private String rango;

	// Constructors

	/** default constructor */
	public CarteraResumenId() {
	}

	/** minimal constructor */
	public CarteraResumenId(String codcli, String numfac) {
		this.codcli = codcli;
		this.numfac = numfac;
	}

	/** full constructor */
	public CarteraResumenId(String codcli, String numfac, String clsdoc,
			String fecfac, String fecven, String dias, String monto,
			String planta, String rango) {
		this.codcli = codcli;
		this.numfac = numfac;
		this.clsdoc = clsdoc;
		this.fecfac = fecfac;
		this.fecven = fecven;
		this.dias = dias;
		this.monto = monto;
		this.planta = planta;
		this.rango = rango;
	}

	// Property accessors

	@Column(name = "CODCLI", nullable = false, length = 8)
	public String getCodcli() {
		return this.codcli;
	}

	public void setCodcli(String codcli) {
		this.codcli = codcli;
	}

	@Column(name = "NUMFAC", nullable = false, length = 10)
	public String getNumfac() {
		return this.numfac;
	}

	public void setNumfac(String numfac) {
		this.numfac = numfac;
	}

	@Column(name = "CLSDOC", length = 4)
	public String getClsdoc() {
		return this.clsdoc;
	}

	public void setClsdoc(String clsdoc) {
		this.clsdoc = clsdoc;
	}

	@Column(name = "FECFAC", length = 10)
	public String getFecfac() {
		return this.fecfac;
	}

	public void setFecfac(String fecfac) {
		this.fecfac = fecfac;
	}

	@Column(name = "FECVEN", length = 10)
	public String getFecven() {
		return this.fecven;
	}

	public void setFecven(String fecven) {
		this.fecven = fecven;
	}

	@Column(name = "DIAS", length = 10)
	public String getDias() {
		return this.dias;
	}

	public void setDias(String dias) {
		this.dias = dias;
	}

	@Column(name = "MONTO", length = 14)
	public String getMonto() {
		return this.monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	@Column(name = "PLANTA", length = 4)
	public String getPlanta() {
		return this.planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	@Column(name = "RANGO", length = 1)
	public String getRango() {
		return this.rango;
	}

	public void setRango(String rango) {
		this.rango = rango;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CarteraResumenId))
			return false;
		CarteraResumenId castOther = (CarteraResumenId) other;

		return ((this.getCodcli() == castOther.getCodcli()) || (this
				.getCodcli() != null
				&& castOther.getCodcli() != null && this.getCodcli().equals(
				castOther.getCodcli())))
				&& ((this.getNumfac() == castOther.getNumfac()) || (this
						.getNumfac() != null
						&& castOther.getNumfac() != null && this.getNumfac()
						.equals(castOther.getNumfac())))
				&& ((this.getClsdoc() == castOther.getClsdoc()) || (this
						.getClsdoc() != null
						&& castOther.getClsdoc() != null && this.getClsdoc()
						.equals(castOther.getClsdoc())))
				&& ((this.getFecfac() == castOther.getFecfac()) || (this
						.getFecfac() != null
						&& castOther.getFecfac() != null && this.getFecfac()
						.equals(castOther.getFecfac())))
				&& ((this.getFecven() == castOther.getFecven()) || (this
						.getFecven() != null
						&& castOther.getFecven() != null && this.getFecven()
						.equals(castOther.getFecven())))
				&& ((this.getDias() == castOther.getDias()) || (this.getDias() != null
						&& castOther.getDias() != null && this.getDias()
						.equals(castOther.getDias())))
				&& ((this.getMonto() == castOther.getMonto()) || (this
						.getMonto() != null
						&& castOther.getMonto() != null && this.getMonto()
						.equals(castOther.getMonto())))
				&& ((this.getPlanta() == castOther.getPlanta()) || (this
						.getPlanta() != null
						&& castOther.getPlanta() != null && this.getPlanta()
						.equals(castOther.getPlanta())))
				&& ((this.getRango() == castOther.getRango()) || (this
						.getRango() != null
						&& castOther.getRango() != null && this.getRango()
						.equals(castOther.getRango())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCodcli() == null ? 0 : this.getCodcli().hashCode());
		result = 37 * result
				+ (getNumfac() == null ? 0 : this.getNumfac().hashCode());
		result = 37 * result
				+ (getClsdoc() == null ? 0 : this.getClsdoc().hashCode());
		result = 37 * result
				+ (getFecfac() == null ? 0 : this.getFecfac().hashCode());
		result = 37 * result
				+ (getFecven() == null ? 0 : this.getFecven().hashCode());
		result = 37 * result
				+ (getDias() == null ? 0 : this.getDias().hashCode());
		result = 37 * result
				+ (getMonto() == null ? 0 : this.getMonto().hashCode());
		result = 37 * result
				+ (getPlanta() == null ? 0 : this.getPlanta().hashCode());
		result = 37 * result
				+ (getRango() == null ? 0 : this.getRango().hashCode());
		return result;
	}

}