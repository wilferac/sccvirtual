package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SapUmedidaId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class SapUmedidaId implements java.io.Serializable {

	// Fields

	private String codume;
	private String desume;

	// Constructors

	/** default constructor */
	public SapUmedidaId() {
	}

	/** minimal constructor */
	public SapUmedidaId(String codume) {
		this.codume = codume;
	}

	/** full constructor */
	public SapUmedidaId(String codume, String desume) {
		this.codume = codume;
		this.desume = desume;
	}

	// Property accessors

	@Column(name = "CODUME", nullable = false, length = 2)
	public String getCodume() {
		return this.codume;
	}

	public void setCodume(String codume) {
		this.codume = codume;
	}

	@Column(name = "DESUME", length = 20)
	public String getDesume() {
		return this.desume;
	}

	public void setDesume(String desume) {
		this.desume = desume;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SapUmedidaId))
			return false;
		SapUmedidaId castOther = (SapUmedidaId) other;

		return ((this.getCodume() == castOther.getCodume()) || (this
				.getCodume() != null
				&& castOther.getCodume() != null && this.getCodume().equals(
				castOther.getCodume())))
				&& ((this.getDesume() == castOther.getDesume()) || (this
						.getDesume() != null
						&& castOther.getDesume() != null && this.getDesume()
						.equals(castOther.getDesume())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCodume() == null ? 0 : this.getCodume().hashCode());
		result = 37 * result
				+ (getDesume() == null ? 0 : this.getDesume().hashCode());
		return result;
	}

}