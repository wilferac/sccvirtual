package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TsccvEmpresas entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TSCCV_EMPRESAS", schema = "dbo")
public class TsccvEmpresas implements java.io.Serializable {

	// Fields

	private Integer idEmpr;
	private TsccvGrupoEmpresa tsccvGrupoEmpresa;
	private String nombre;
	private String codSap;
	private String codVpe;
	private String codMas;
	private String activo;
	private Date fechaCreacion;
	private Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds = new HashSet<TsccvEstadisticaAccPed>(
			0);
	private Set<TsccvPedidos> tsccvPedidoses = new HashSet<TsccvPedidos>(0);
	private Set<TsccvUsuario> tsccvUsuarios = new HashSet<TsccvUsuario>(0);
	private Set<TsccvDetalleGrupoEmpresa> tsccvDetalleGrupoEmpresas = new HashSet<TsccvDetalleGrupoEmpresa>(0);

	// Constructors

	/** default constructor */
	public TsccvEmpresas() {
	}

	/** minimal constructor */
	public TsccvEmpresas(Integer idEmpr, String nombre, String activo,
			Date fechaCreacion) {
		this.idEmpr = idEmpr;
		this.nombre = nombre;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TsccvEmpresas(Integer idEmpr, TsccvGrupoEmpresa tsccvGrupoEmpresa,
			String nombre, String codSap, String codVpe, String codMas,
			String activo, Date fechaCreacion,
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds,
			Set<TsccvPedidos> tsccvPedidoses, Set<TsccvUsuario> tsccvUsuarios, 
			Set<TsccvDetalleGrupoEmpresa> tsccvDetalleGrupoEmpresas) {
		this.idEmpr = idEmpr;
		this.tsccvGrupoEmpresa = tsccvGrupoEmpresa;
		this.nombre = nombre;
		this.codSap = codSap;
		this.codVpe = codVpe;
		this.codMas = codMas;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
		this.tsccvPedidoses = tsccvPedidoses;
		this.tsccvUsuarios = tsccvUsuarios;
		this.tsccvDetalleGrupoEmpresas = tsccvDetalleGrupoEmpresas;
	}

	// Property accessors
	@Id
	@Column(name = "ID_EMPR", unique = true, nullable = false)
	public Integer getIdEmpr() {
		return this.idEmpr;
	}

	public void setIdEmpr(Integer idEmpr) {
		this.idEmpr = idEmpr;
	}

	@ManyToOne
	@JoinColumn(name = "ID_GRUE")
	public TsccvGrupoEmpresa getTsccvGrupoEmpresa() {
		return this.tsccvGrupoEmpresa;
	}

	public void setTsccvGrupoEmpresa(TsccvGrupoEmpresa tsccvGrupoEmpresa) {
		this.tsccvGrupoEmpresa = tsccvGrupoEmpresa;
	}

	@Column(name = "NOMBRE", nullable = false, length = 60)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "COD_SAP", length = 10)
	public String getCodSap() {
		return this.codSap;
	}

	public void setCodSap(String codSap) {
		this.codSap = codSap;
	}

	@Column(name = "COD_VPE", length = 10)
	public String getCodVpe() {
		return this.codVpe;
	}

	public void setCodVpe(String codVpe) {
		this.codVpe = codVpe;
	}

	@Column(name = "COD_MAS", length = 10)
	public String getCodMas() {
		return this.codMas;
	}

	public void setCodMas(String codMas) {
		this.codMas = codMas;
	}

	@Column(name = "ACTIVO", nullable = false, length = 1)
	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvEmpresas")
	public Set<TsccvEstadisticaAccPed> getTsccvEstadisticaAccPeds() {
		return this.tsccvEstadisticaAccPeds;
	}

	public void setTsccvEstadisticaAccPeds(
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds) {
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvEmpresas")
	public Set<TsccvPedidos> getTsccvPedidoses() {
		return this.tsccvPedidoses;
	}

	public void setTsccvPedidoses(Set<TsccvPedidos> tsccvPedidoses) {
		this.tsccvPedidoses = tsccvPedidoses;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvEmpresas")
	public Set<TsccvUsuario> getTsccvUsuarios() {
		return this.tsccvUsuarios;
	}

	public void setTsccvUsuarios(Set<TsccvUsuario> tsccvUsuarios) {
		this.tsccvUsuarios = tsccvUsuarios;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tsccvEmpresas")
	public Set<TsccvDetalleGrupoEmpresa> getTsccvDetalleGrupoEmpresas() {
		return this.tsccvDetalleGrupoEmpresas;
	}

	public void setTsccvDetalleGrupoEmpresas(
			Set<TsccvDetalleGrupoEmpresa> tsccvDetalleGrupoEmpresas) {
		this.tsccvDetalleGrupoEmpresas = tsccvDetalleGrupoEmpresas;
	}

}