package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * BvpDetPed entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "BVP_DET_PED", schema = "dbo")
public class BvpDetPed implements java.io.Serializable {

	// Fields

	private BvpDetPedId id;

	// Constructors

	/** default constructor */
	public BvpDetPed() {
	}

	/** full constructor */
	public BvpDetPed(BvpDetPedId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "planta", column = @Column(name = "PLANTA", nullable = false, length = 1)),
			@AttributeOverride(name = "numped", column = @Column(name = "NUMPED", nullable = false, length = 8)),
			@AttributeOverride(name = "itmped", column = @Column(name = "ITMPED", nullable = false, length = 2)),
			@AttributeOverride(name = "codcli", column = @Column(name = "CODCLI", length = 8)),
			@AttributeOverride(name = "codscc", column = @Column(name = "CODSCC", length = 8)),
			@AttributeOverride(name = "codpro", column = @Column(name = "CODPRO", length = 20)),
			@AttributeOverride(name = "tippro", column = @Column(name = "TIPPRO", length = 1)),
			@AttributeOverride(name = "despro", column = @Column(name = "DESPRO", length = 60)),
			@AttributeOverride(name = "fecsol", column = @Column(name = "FECSOL", length = 10)),
			@AttributeOverride(name = "fecdsp", column = @Column(name = "FECDSP", length = 10)),
			@AttributeOverride(name = "estado", column = @Column(name = "ESTADO", length = 1)),
			@AttributeOverride(name = "undmed", column = @Column(name = "UNDMED", length = 10)),
			@AttributeOverride(name = "cntped", column = @Column(name = "CNTPED", length = 16)),
			@AttributeOverride(name = "cntdsp", column = @Column(name = "CNTDSP", length = 16)),
			@AttributeOverride(name = "ordcom", column = @Column(name = "ORDCOM", length = 15)) })
	public BvpDetPedId getId() {
		return this.id;
	}

	public void setId(BvpDetPedId id) {
		this.id = id;
	}

}