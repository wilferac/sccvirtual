package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * TsccvDetalleGrupoEmpresaId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class TsccvDetalleGrupoEmpresaId implements java.io.Serializable {

	// Fields

	private Integer idGrue;
	private Integer idEmpr;

	// Constructors

	/** default constructor */
	public TsccvDetalleGrupoEmpresaId() {
	}

	/** full constructor */
	public TsccvDetalleGrupoEmpresaId(Integer idGrue, Integer idEmpr) {
		this.idGrue = idGrue;
		this.idEmpr = idEmpr;
	}

	// Property accessors

	@Column(name = "ID_GRUE", nullable = false)
	public Integer getIdGrue() {
		return this.idGrue;
	}

	public void setIdGrue(Integer idGrue) {
		this.idGrue = idGrue;
	}

	@Column(name = "ID_EMPR", nullable = false)
	public Integer getIdEmpr() {
		return this.idEmpr;
	}

	public void setIdEmpr(Integer idEmpr) {
		this.idEmpr = idEmpr;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof TsccvDetalleGrupoEmpresaId))
			return false;
		TsccvDetalleGrupoEmpresaId castOther = (TsccvDetalleGrupoEmpresaId) other;

		return ((this.getIdGrue() == castOther.getIdGrue()) || (this
				.getIdGrue() != null
				&& castOther.getIdGrue() != null && this.getIdGrue().equals(
				castOther.getIdGrue())))
				&& ((this.getIdEmpr() == castOther.getIdEmpr()) || (this
						.getIdEmpr() != null
						&& castOther.getIdEmpr() != null && this.getIdEmpr()
						.equals(castOther.getIdEmpr())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getIdGrue() == null ? 0 : this.getIdGrue().hashCode());
		result = 37 * result
				+ (getIdEmpr() == null ? 0 : this.getIdEmpr().hashCode());
		return result;
	}

}