package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SapPrecioId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class SapPrecioId implements java.io.Serializable {

	// Fields

	private String codcli;
	private String codpro;
	private String precio;

	// Constructors

	/** default constructor */
	public SapPrecioId() {
	}

	/** full constructor */
	public SapPrecioId(String codcli, String codpro, String precio) {
		this.codcli = codcli;
		this.codpro = codpro;
		this.precio = precio;
	}

	// Property accessors

	@Column(name = "CODCLI", nullable = false, length = 8)
	public String getCodcli() {
		return this.codcli;
	}

	public void setCodcli(String codcli) {
		this.codcli = codcli;
	}

	@Column(name = "CODPRO", nullable = false, length = 8)
	public String getCodpro() {
		return this.codpro;
	}

	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}

	@Column(name = "PRECIO", nullable = false, length = 16)
	public String getPrecio() {
		return this.precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SapPrecioId))
			return false;
		SapPrecioId castOther = (SapPrecioId) other;

		return ((this.getCodcli() == castOther.getCodcli()) || (this
				.getCodcli() != null
				&& castOther.getCodcli() != null && this.getCodcli().equals(
				castOther.getCodcli())))
				&& ((this.getCodpro() == castOther.getCodpro()) || (this
						.getCodpro() != null
						&& castOther.getCodpro() != null && this.getCodpro()
						.equals(castOther.getCodpro())))
				&& ((this.getPrecio() == castOther.getPrecio()) || (this
						.getPrecio() != null
						&& castOther.getPrecio() != null && this.getPrecio()
						.equals(castOther.getPrecio())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCodcli() == null ? 0 : this.getCodcli().hashCode());
		result = 37 * result
				+ (getCodpro() == null ? 0 : this.getCodpro().hashCode());
		result = 37 * result
				+ (getPrecio() == null ? 0 : this.getPrecio().hashCode());
		return result;
	}

}