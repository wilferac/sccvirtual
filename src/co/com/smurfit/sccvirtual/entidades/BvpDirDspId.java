package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * BvpDirDspId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class BvpDirDspId implements java.io.Serializable {

	// Fields

	private String codcli;
	private String coddir;
	private String codpla;
	private String desdir;
	private String numfax;
	private String desprv;

	// Constructors

	/** default constructor */
	public BvpDirDspId() {
	}

	/** minimal constructor */
	public BvpDirDspId(String codcli, String coddir, String codpla,
			String desdir) {
		this.codcli = codcli;
		this.coddir = coddir;
		this.codpla = codpla;
		this.desdir = desdir;
	}

	/** full constructor */
	public BvpDirDspId(String codcli, String coddir, String codpla,
			String desdir, String numfax, String desprv) {
		this.codcli = codcli;
		this.coddir = coddir;
		this.codpla = codpla;
		this.desdir = desdir;
		this.numfax = numfax;
		this.desprv = desprv;
	}

	// Property accessors

	@Column(name = "CODCLI", nullable = false, length = 2)
	public String getCodcli() {
		return this.codcli;
	}

	public void setCodcli(String codcli) {
		this.codcli = codcli;
	}

	@Column(name = "CODDIR", nullable = false, length = 2)
	public String getCoddir() {
		return this.coddir;
	}

	public void setCoddir(String coddir) {
		this.coddir = coddir;
	}

	@Column(name = "CODPLA", nullable = false, length = 2)
	public String getCodpla() {
		return this.codpla;
	}

	public void setCodpla(String codpla) {
		this.codpla = codpla;
	}

	@Column(name = "DESDIR", nullable = false, length = 80)
	public String getDesdir() {
		return this.desdir;
	}

	public void setDesdir(String desdir) {
		this.desdir = desdir;
	}

	@Column(name = "NUMFAX", length = 20)
	public String getNumfax() {
		return this.numfax;
	}

	public void setNumfax(String numfax) {
		this.numfax = numfax;
	}

	@Column(name = "DESPRV", length = 20)
	public String getDesprv() {
		return this.desprv;
	}

	public void setDesprv(String desprv) {
		this.desprv = desprv;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BvpDirDspId))
			return false;
		BvpDirDspId castOther = (BvpDirDspId) other;

		return ((this.getCodcli() == castOther.getCodcli()) || (this
				.getCodcli() != null
				&& castOther.getCodcli() != null && this.getCodcli().equals(
				castOther.getCodcli())))
				&& ((this.getCoddir() == castOther.getCoddir()) || (this
						.getCoddir() != null
						&& castOther.getCoddir() != null && this.getCoddir()
						.equals(castOther.getCoddir())))
				&& ((this.getCodpla() == castOther.getCodpla()) || (this
						.getCodpla() != null
						&& castOther.getCodpla() != null && this.getCodpla()
						.equals(castOther.getCodpla())))
				&& ((this.getDesdir() == castOther.getDesdir()) || (this
						.getDesdir() != null
						&& castOther.getDesdir() != null && this.getDesdir()
						.equals(castOther.getDesdir())))
				&& ((this.getNumfax() == castOther.getNumfax()) || (this
						.getNumfax() != null
						&& castOther.getNumfax() != null && this.getNumfax()
						.equals(castOther.getNumfax())))
				&& ((this.getDesprv() == castOther.getDesprv()) || (this
						.getDesprv() != null
						&& castOther.getDesprv() != null && this.getDesprv()
						.equals(castOther.getDesprv())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getCodcli() == null ? 0 : this.getCodcli().hashCode());
		result = 37 * result
				+ (getCoddir() == null ? 0 : this.getCoddir().hashCode());
		result = 37 * result
				+ (getCodpla() == null ? 0 : this.getCodpla().hashCode());
		result = 37 * result
				+ (getDesdir() == null ? 0 : this.getDesdir().hashCode());
		result = 37 * result
				+ (getNumfax() == null ? 0 : this.getNumfax().hashCode());
		result = 37 * result
				+ (getDesprv() == null ? 0 : this.getDesprv().hashCode());
		return result;
	}

}