package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TbmBaseGruposUsuarios entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TBM_BASE_GRUPOS_USUARIOS", schema = "dbo")
public class TbmBaseGruposUsuarios implements java.io.Serializable {

	// Fields

	private Long idGrup;
	private String descripcion;
	private String activo;
	private Date fechaCreacion;
	private Set<TbmBaseOpcionesGrupos> tbmBaseOpcionesGruposes = new HashSet<TbmBaseOpcionesGrupos>(
			0);
	private Set<TsccvUsuario> tsccvUsuarios = new HashSet<TsccvUsuario>(0);
	private Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds = new HashSet<TsccvEstadisticaAccPed>(
			0);

	// Constructors

	/** default constructor */
	public TbmBaseGruposUsuarios() {
	}

	/** minimal constructor */
	public TbmBaseGruposUsuarios(Long idGrup, String descripcion,
			String activo, Date fechaCreacion) {
		this.idGrup = idGrup;
		this.descripcion = descripcion;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TbmBaseGruposUsuarios(Long idGrup, String descripcion,
			String activo, Date fechaCreacion,
			Set<TbmBaseOpcionesGrupos> tbmBaseOpcionesGruposes,
			Set<TsccvUsuario> tsccvUsuarios,
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds) {
		this.idGrup = idGrup;
		this.descripcion = descripcion;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.tbmBaseOpcionesGruposes = tbmBaseOpcionesGruposes;
		this.tsccvUsuarios = tsccvUsuarios;
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
	}

	// Property accessors
	@Id
	@Column(name = "ID_GRUP", unique = true, nullable = false, precision = 3, scale = 0)
	public Long getIdGrup() {
		return this.idGrup;
	}

	public void setIdGrup(Long idGrup) {
		this.idGrup = idGrup;
	}

	@Column(name = "DESCRIPCION", nullable = false, length = 30)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "ACTIVO", nullable = false, length = 1)
	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tbmBaseGruposUsuarios")
	public Set<TbmBaseOpcionesGrupos> getTbmBaseOpcionesGruposes() {
		return this.tbmBaseOpcionesGruposes;
	}

	public void setTbmBaseOpcionesGruposes(
			Set<TbmBaseOpcionesGrupos> tbmBaseOpcionesGruposes) {
		this.tbmBaseOpcionesGruposes = tbmBaseOpcionesGruposes;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tbmBaseGruposUsuarios")
	public Set<TsccvUsuario> getTsccvUsuarios() {
		return this.tsccvUsuarios;
	}

	public void setTsccvUsuarios(Set<TsccvUsuario> tsccvUsuarios) {
		this.tsccvUsuarios = tsccvUsuarios;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tbmBaseGruposUsuarios")
	public Set<TsccvEstadisticaAccPed> getTsccvEstadisticaAccPeds() {
		return this.tsccvEstadisticaAccPeds;
	}

	public void setTsccvEstadisticaAccPeds(
			Set<TsccvEstadisticaAccPed> tsccvEstadisticaAccPeds) {
		this.tsccvEstadisticaAccPeds = tsccvEstadisticaAccPeds;
	}

}