package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * SapPrecio entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "SAP_PRECIO", schema = "dbo")
public class SapPrecio implements java.io.Serializable {

	// Fields

	private SapPrecioId id;
	private String moneda;
	private String eje;
	private String diamtr;
	private String despro;
	private String undmed;
	private String tiprec;

	// Constructors

	/** default constructor */
	public SapPrecio() {
	}

	/** minimal constructor */
	public SapPrecio(SapPrecioId id) {
		this.id = id;
	}

	/** full constructor */
	public SapPrecio(SapPrecioId id, String moneda, String eje, String diamtr,
			String despro, String undmed, String tiprec) {
		this.id = id;
		this.moneda = moneda;
		this.eje = eje;
		this.diamtr = diamtr;
		this.despro = despro;
		this.undmed = undmed;
		this.tiprec = tiprec;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codcli", column = @Column(name = "CODCLI", nullable = false, length = 8)),
			@AttributeOverride(name = "codpro", column = @Column(name = "CODPRO", nullable = false, length = 8)),
			@AttributeOverride(name = "precio", column = @Column(name = "PRECIO", nullable = false, length = 16)) })
	public SapPrecioId getId() {
		return this.id;
	}

	public void setId(SapPrecioId id) {
		this.id = id;
	}

	@Column(name = "MONEDA", length = 10)
	public String getMoneda() {
		return this.moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	@Column(name = "EJE", length = 16)
	public String getEje() {
		return this.eje;
	}

	public void setEje(String eje) {
		this.eje = eje;
	}

	@Column(name = "DIAMTR", length = 16)
	public String getDiamtr() {
		return this.diamtr;
	}

	public void setDiamtr(String diamtr) {
		this.diamtr = diamtr;
	}

	@Column(name = "DESPRO", length = 60)
	public String getDespro() {
		return this.despro;
	}

	public void setDespro(String despro) {
		this.despro = despro;
	}

	@Column(name = "UNDMED", length = 10)
	public String getUndmed() {
		return this.undmed;
	}

	public void setUndmed(String undmed) {
		this.undmed = undmed;
	}

	@Column(name = "TIPREC", length = 20)
	public String getTiprec() {
		return this.tiprec;
	}

	public void setTiprec(String tiprec) {
		this.tiprec = tiprec;
	}

}