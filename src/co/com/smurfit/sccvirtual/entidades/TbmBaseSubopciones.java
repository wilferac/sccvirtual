package co.com.smurfit.sccvirtual.entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TbmBaseSubopciones entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TBM_BASE_SUBOPCIONES", schema = "dbo")
public class TbmBaseSubopciones implements java.io.Serializable {

	// Fields

	private Long idSuop;
	private String nombre;
	private String url;
	private String descripcion;
	private String activo;
	private Date fechaCreacion;
	private Set<TbmBaseSubopcionesOpc> tbmBaseSubopcionesOpcs = new HashSet<TbmBaseSubopcionesOpc>(
			0);

	// Constructors

	/** default constructor */
	public TbmBaseSubopciones() {
	}

	/** minimal constructor */
	public TbmBaseSubopciones(Long idSuop, String nombre, String url,
			String activo, Date fechaCreacion) {
		this.idSuop = idSuop;
		this.nombre = nombre;
		this.url = url;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TbmBaseSubopciones(Long idSuop, String nombre, String url,
			String descripcion, String activo, Date fechaCreacion,
			Set<TbmBaseSubopcionesOpc> tbmBaseSubopcionesOpcs) {
		this.idSuop = idSuop;
		this.nombre = nombre;
		this.url = url;
		this.descripcion = descripcion;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.tbmBaseSubopcionesOpcs = tbmBaseSubopcionesOpcs;
	}

	// Property accessors
	@Id
	@Column(name = "ID_SUOP", unique = true, nullable = false, precision = 2, scale = 0)
	public Long getIdSuop() {
		return this.idSuop;
	}

	public void setIdSuop(Long idSuop) {
		this.idSuop = idSuop;
	}

	@Column(name = "NOMBRE", nullable = false, length = 40)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "URL", nullable = false, length = 60)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "DESCRIPCION", length = 80)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "ACTIVO", nullable = false, length = 1)
	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tbmBaseSubopciones")
	public Set<TbmBaseSubopcionesOpc> getTbmBaseSubopcionesOpcs() {
		return this.tbmBaseSubopcionesOpcs;
	}

	public void setTbmBaseSubopcionesOpcs(
			Set<TbmBaseSubopcionesOpc> tbmBaseSubopcionesOpcs) {
		this.tbmBaseSubopcionesOpcs = tbmBaseSubopcionesOpcs;
	}

}