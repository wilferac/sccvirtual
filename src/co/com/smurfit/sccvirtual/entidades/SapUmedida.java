package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * SapUmedida entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "SAP_UMEDIDA", schema = "dbo")
public class SapUmedida implements java.io.Serializable {

	// Fields

	private SapUmedidaId id;

	// Constructors

	/** default constructor */
	public SapUmedida() {
	}

	/** full constructor */
	public SapUmedida(SapUmedidaId id) {
		this.id = id;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "codume", column = @Column(name = "CODUME", nullable = false, length = 2)),
			@AttributeOverride(name = "desume", column = @Column(name = "DESUME", length = 20)) })
	public SapUmedidaId getId() {
		return this.id;
	}

	public void setId(SapUmedidaId id) {
		this.id = id;
	}

}