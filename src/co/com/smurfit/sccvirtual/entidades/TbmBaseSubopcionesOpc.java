package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * TbmBaseSubopcionesOpc entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TBM_BASE_SUBOPCIONES_OPC", schema = "dbo")
public class TbmBaseSubopcionesOpc implements java.io.Serializable {

	// Fields

	private TbmBaseSubopcionesOpcId id;
	private TbmBaseSubopciones tbmBaseSubopciones;
	private TbmBaseOpciones tbmBaseOpciones;

	// Constructors

	/** default constructor */
	public TbmBaseSubopcionesOpc() {
	}

	/** minimal constructor */
	public TbmBaseSubopcionesOpc(TbmBaseSubopcionesOpcId id) {
		this.id = id;
	}

	/** full constructor */
	public TbmBaseSubopcionesOpc(TbmBaseSubopcionesOpcId id,
			TbmBaseSubopciones tbmBaseSubopciones,
			TbmBaseOpciones tbmBaseOpciones) {
		this.id = id;
		this.tbmBaseSubopciones = tbmBaseSubopciones;
		this.tbmBaseOpciones = tbmBaseOpciones;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "idOpci", column = @Column(name = "ID_OPCI", precision = 2, scale = 0)),
			@AttributeOverride(name = "idSuop", column = @Column(name = "ID_SUOP", precision = 2, scale = 0)),
			@AttributeOverride(name = "nombreVisible", column = @Column(name = "NOMBRE_VISIBLE", nullable = false, length = 40)),
			@AttributeOverride(name = "fechaCreacion", column = @Column(name = "FECHA_CREACION", nullable = false, length = 23)),
			@AttributeOverride(name = "posicion", column = @Column(name = "POSICION", nullable = false, precision = 2, scale = 0)) })
	public TbmBaseSubopcionesOpcId getId() {
		return this.id;
	}

	public void setId(TbmBaseSubopcionesOpcId id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "ID_SUOP", insertable = false, updatable = false)
	public TbmBaseSubopciones getTbmBaseSubopciones() {
		return this.tbmBaseSubopciones;
	}

	public void setTbmBaseSubopciones(TbmBaseSubopciones tbmBaseSubopciones) {
		this.tbmBaseSubopciones = tbmBaseSubopciones;
	}

	@ManyToOne
	@JoinColumn(name = "ID_OPCI", insertable = false, updatable = false)
	public TbmBaseOpciones getTbmBaseOpciones() {
		return this.tbmBaseOpciones;
	}

	public void setTbmBaseOpciones(TbmBaseOpciones tbmBaseOpciones) {
		this.tbmBaseOpciones = tbmBaseOpciones;
	}

}