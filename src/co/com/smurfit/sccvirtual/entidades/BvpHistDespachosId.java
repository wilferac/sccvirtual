package co.com.smurfit.sccvirtual.entidades;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * BvpHistDespachosId entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Embeddable
public class BvpHistDespachosId implements java.io.Serializable {

	// Fields

	private String planta;
	private String nummes;
	private String codcli;
	private String codscc;
	private String codpro;
	private String despro;
	private String tippro;
	private String undmed;
	private String cantid;

	// Constructors

	/** default constructor */
	public BvpHistDespachosId() {
	}

	/** minimal constructor */
	public BvpHistDespachosId(String planta, String nummes, String codcli,
			String codscc) {
		this.planta = planta;
		this.nummes = nummes;
		this.codcli = codcli;
		this.codscc = codscc;
	}

	/** full constructor */
	public BvpHistDespachosId(String planta, String nummes, String codcli,
			String codscc, String codpro, String despro, String tippro,
			String undmed, String cantid) {
		this.planta = planta;
		this.nummes = nummes;
		this.codcli = codcli;
		this.codscc = codscc;
		this.codpro = codpro;
		this.despro = despro;
		this.tippro = tippro;
		this.undmed = undmed;
		this.cantid = cantid;
	}

	// Property accessors

	@Column(name = "PLANTA", nullable = false, length = 2)
	public String getPlanta() {
		return this.planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	@Column(name = "NUMMES", nullable = false, length = 6)
	public String getNummes() {
		return this.nummes;
	}

	public void setNummes(String nummes) {
		this.nummes = nummes;
	}

	@Column(name = "CODCLI", nullable = false, length = 8)
	public String getCodcli() {
		return this.codcli;
	}

	public void setCodcli(String codcli) {
		this.codcli = codcli;
	}

	@Column(name = "CODSCC", nullable = false, length = 8)
	public String getCodscc() {
		return this.codscc;
	}

	public void setCodscc(String codscc) {
		this.codscc = codscc;
	}

	@Column(name = "CODPRO", length = 20)
	public String getCodpro() {
		return this.codpro;
	}

	public void setCodpro(String codpro) {
		this.codpro = codpro;
	}

	@Column(name = "DESPRO", length = 60)
	public String getDespro() {
		return this.despro;
	}

	public void setDespro(String despro) {
		this.despro = despro;
	}

	@Column(name = "TIPPRO", length = 1)
	public String getTippro() {
		return this.tippro;
	}

	public void setTippro(String tippro) {
		this.tippro = tippro;
	}

	@Column(name = "UNDMED", length = 10)
	public String getUndmed() {
		return this.undmed;
	}

	public void setUndmed(String undmed) {
		this.undmed = undmed;
	}

	@Column(name = "CANTID", length = 16)
	public String getCantid() {
		return this.cantid;
	}

	public void setCantid(String cantid) {
		this.cantid = cantid;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BvpHistDespachosId))
			return false;
		BvpHistDespachosId castOther = (BvpHistDespachosId) other;

		return ((this.getPlanta() == castOther.getPlanta()) || (this
				.getPlanta() != null
				&& castOther.getPlanta() != null && this.getPlanta().equals(
				castOther.getPlanta())))
				&& ((this.getNummes() == castOther.getNummes()) || (this
						.getNummes() != null
						&& castOther.getNummes() != null && this.getNummes()
						.equals(castOther.getNummes())))
				&& ((this.getCodcli() == castOther.getCodcli()) || (this
						.getCodcli() != null
						&& castOther.getCodcli() != null && this.getCodcli()
						.equals(castOther.getCodcli())))
				&& ((this.getCodscc() == castOther.getCodscc()) || (this
						.getCodscc() != null
						&& castOther.getCodscc() != null && this.getCodscc()
						.equals(castOther.getCodscc())))
				&& ((this.getCodpro() == castOther.getCodpro()) || (this
						.getCodpro() != null
						&& castOther.getCodpro() != null && this.getCodpro()
						.equals(castOther.getCodpro())))
				&& ((this.getDespro() == castOther.getDespro()) || (this
						.getDespro() != null
						&& castOther.getDespro() != null && this.getDespro()
						.equals(castOther.getDespro())))
				&& ((this.getTippro() == castOther.getTippro()) || (this
						.getTippro() != null
						&& castOther.getTippro() != null && this.getTippro()
						.equals(castOther.getTippro())))
				&& ((this.getUndmed() == castOther.getUndmed()) || (this
						.getUndmed() != null
						&& castOther.getUndmed() != null && this.getUndmed()
						.equals(castOther.getUndmed())))
				&& ((this.getCantid() == castOther.getCantid()) || (this
						.getCantid() != null
						&& castOther.getCantid() != null && this.getCantid()
						.equals(castOther.getCantid())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getPlanta() == null ? 0 : this.getPlanta().hashCode());
		result = 37 * result
				+ (getNummes() == null ? 0 : this.getNummes().hashCode());
		result = 37 * result
				+ (getCodcli() == null ? 0 : this.getCodcli().hashCode());
		result = 37 * result
				+ (getCodscc() == null ? 0 : this.getCodscc().hashCode());
		result = 37 * result
				+ (getCodpro() == null ? 0 : this.getCodpro().hashCode());
		result = 37 * result
				+ (getDespro() == null ? 0 : this.getDespro().hashCode());
		result = 37 * result
				+ (getTippro() == null ? 0 : this.getTippro().hashCode());
		result = 37 * result
				+ (getUndmed() == null ? 0 : this.getUndmed().hashCode());
		result = 37 * result
				+ (getCantid() == null ? 0 : this.getCantid().hashCode());
		return result;
	}

}