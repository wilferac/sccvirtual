package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvEntregasPedidoView;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvEntregasPedidoDTO {
	private String anchoHojas;
    private String anchoRollo;
    private String cantidad;
    private String desdir;
    private String largoHojas;
    private String idPrpe_TsccvProductosPedido;
    private String idEnpe;
    private Date fechaEntrega;
    private boolean rowSelected = false;
    private TsccvEntregasPedidoView tsccvEntregasPedidoView;
    private TsccvEntregasPedido tsccvEntregasPedido;

    public TsccvEntregasPedido getTsccvEntregasPedido() {
        return tsccvEntregasPedido;
    }

    public String listener_update(ActionEvent e) {
        try {
        	tsccvEntregasPedidoView.action_modifyWitDTO(((anchoHojas == null) ||
	            anchoHojas.equals("")) ? null : new String(anchoHojas),
	            ((anchoRollo == null) || anchoRollo.equals("")) ? null
	                                                            : new String(
	                anchoRollo),
	            ((cantidad == null) || cantidad.equals("")) ? null
	                                                        : new Integer(
	                cantidad),
	            ((desdir == null) || desdir.equals("")) ? null
	                                                    : new String(desdir),
	            ((fechaEntrega == null) || fechaEntrega.equals("")) ? null
	                                                                : fechaEntrega,
	            ((idEnpe == null) || idEnpe.equals("")) ? null
	                                                    : new Integer(idEnpe),
	            ((largoHojas == null) || largoHojas.equals("")) ? null
	                                                            : new String(
	                largoHojas),
	            ((idPrpe_TsccvProductosPedido == null) ||
	            idPrpe_TsccvProductosPedido.equals("")) ? null
	                                                    : new Integer(
	                idPrpe_TsccvProductosPedido));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idEnpe = tsccvEntregasPedido.getIdEnpe().toString();
        anchoHojas = (tsccvEntregasPedido.getAnchoHojas() != null)
            ? tsccvEntregasPedido.getAnchoHojas().toString() : null;
        anchoRollo = (tsccvEntregasPedido.getAnchoRollo() != null)
            ? tsccvEntregasPedido.getAnchoRollo().toString() : null;
        cantidad = (tsccvEntregasPedido.getCantidad() != null)
            ? tsccvEntregasPedido.getCantidad().toString() : null;
        desdir = (tsccvEntregasPedido.getDesdir() != null)
            ? tsccvEntregasPedido.getDesdir().toString() : null;
        fechaEntrega = tsccvEntregasPedido.getFechaEntrega();
        largoHojas = (tsccvEntregasPedido.getLargoHojas() != null)
            ? tsccvEntregasPedido.getLargoHojas().toString() : null;
        idPrpe_TsccvProductosPedido = (tsccvEntregasPedido.getTsccvProductosPedido()
                                                          .getIdPrpe() != null)
            ? tsccvEntregasPedido.getTsccvProductosPedido().getIdPrpe()
                                 .toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idEnpe = tsccvEntregasPedido.getIdEnpe().toString();
        anchoHojas = (tsccvEntregasPedido.getAnchoHojas() != null)
            ? tsccvEntregasPedido.getAnchoHojas().toString() : null;
        anchoRollo = (tsccvEntregasPedido.getAnchoRollo() != null)
            ? tsccvEntregasPedido.getAnchoRollo().toString() : null;
        cantidad = (tsccvEntregasPedido.getCantidad() != null)
            ? tsccvEntregasPedido.getCantidad().toString() : null;
        desdir = (tsccvEntregasPedido.getDesdir() != null)
            ? tsccvEntregasPedido.getDesdir().toString() : null;
        fechaEntrega = tsccvEntregasPedido.getFechaEntrega();
        largoHojas = (tsccvEntregasPedido.getLargoHojas() != null)
            ? tsccvEntregasPedido.getLargoHojas().toString() : null;
        idPrpe_TsccvProductosPedido = (tsccvEntregasPedido.getTsccvProductosPedido()
                                                          .getIdPrpe() != null)
            ? tsccvEntregasPedido.getTsccvProductosPedido().getIdPrpe()
                                 .toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvEntregasPedido(TsccvEntregasPedido tsccvEntregasPedido) {
        this.tsccvEntregasPedido = tsccvEntregasPedido;
    }

    public TsccvEntregasPedidoView getTsccvEntregasPedidoView() {
        return tsccvEntregasPedidoView;
    }

    public void setTsccvEntregasPedidoView(
        TsccvEntregasPedidoView tsccvEntregasPedidoView) {
        this.tsccvEntregasPedidoView = tsccvEntregasPedidoView;
    }

    public String getAnchoHojas() {
        return anchoHojas;
    }

    public void setAnchoHojas(String anchoHojas) {
        this.anchoHojas = anchoHojas;
    }

    public String getAnchoRollo() {
        return anchoRollo;
    }

    public void setAnchoRollo(String anchoRollo) {
        this.anchoRollo = anchoRollo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getDesdir() {
        return desdir;
    }

    public void setDesdir(String desdir) {
        this.desdir = desdir;
    }

    public String getLargoHojas() {
        return largoHojas;
    }

    public void setLargoHojas(String largoHojas) {
        this.largoHojas = largoHojas;
    }

    public String getIdPrpe_TsccvProductosPedido() {
        return idPrpe_TsccvProductosPedido;
    }

    public void setIdPrpe_TsccvProductosPedido(
        String idPrpe_TsccvProductosPedido) {
        this.idPrpe_TsccvProductosPedido = idPrpe_TsccvProductosPedido;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getIdEnpe() {
        return idEnpe;
    }

    public void setIdEnpe(String idEnpe) {
        this.idEnpe = idEnpe;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
