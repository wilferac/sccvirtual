package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TbmBaseOpcionesView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TbmBaseOpcionesDTO {
    private String activo;
    private String descripcion;
    private String imagenIcono;
    private String nombre;
    private String url;
    private String idOpci;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TbmBaseOpcionesView tbmBaseOpcionesView;
    private TbmBaseOpciones tbmBaseOpciones;

    public TbmBaseOpciones getTbmBaseOpciones() {
        return tbmBaseOpciones;
    }

    public String listener_update(ActionEvent e) {
        try {
            tbmBaseOpcionesView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((descripcion == null) || descripcion.equals("")) ? null
                                                                  : new String(
                    descripcion),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idOpci == null) || idOpci.equals("")) ? null : new Long(
                    idOpci),
                ((imagenIcono == null) || imagenIcono.equals("")) ? null
                                                                  : new String(
                    imagenIcono),
                ((nombre == null) || nombre.equals("")) ? null
                                                        : new String(nombre),
                ((url == null) || url.equals("")) ? null : new String(url));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idOpci = tbmBaseOpciones.getIdOpci().toString();
        activo = (tbmBaseOpciones.getActivo() != null)
            ? tbmBaseOpciones.getActivo().toString() : null;
        descripcion = (tbmBaseOpciones.getDescripcion() != null)
            ? tbmBaseOpciones.getDescripcion().toString() : null;
        fechaCreacion = tbmBaseOpciones.getFechaCreacion();
        imagenIcono = (tbmBaseOpciones.getImagenIcono() != null)
            ? tbmBaseOpciones.getImagenIcono().toString() : null;
        nombre = (tbmBaseOpciones.getNombre() != null)
            ? tbmBaseOpciones.getNombre().toString() : null;
        url = (tbmBaseOpciones.getUrl() != null)
            ? tbmBaseOpciones.getUrl().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idOpci = tbmBaseOpciones.getIdOpci().toString();
        activo = (tbmBaseOpciones.getActivo() != null)
            ? tbmBaseOpciones.getActivo().toString() : null;
        descripcion = (tbmBaseOpciones.getDescripcion() != null)
            ? tbmBaseOpciones.getDescripcion().toString() : null;
        fechaCreacion = tbmBaseOpciones.getFechaCreacion();
        imagenIcono = (tbmBaseOpciones.getImagenIcono() != null)
            ? tbmBaseOpciones.getImagenIcono().toString() : null;
        nombre = (tbmBaseOpciones.getNombre() != null)
            ? tbmBaseOpciones.getNombre().toString() : null;
        url = (tbmBaseOpciones.getUrl() != null)
            ? tbmBaseOpciones.getUrl().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTbmBaseOpciones(TbmBaseOpciones tbmBaseOpciones) {
        this.tbmBaseOpciones = tbmBaseOpciones;
    }

    public TbmBaseOpcionesView getTbmBaseOpcionesView() {
        return tbmBaseOpcionesView;
    }

    public void setTbmBaseOpcionesView(TbmBaseOpcionesView tbmBaseOpcionesView) {
        this.tbmBaseOpcionesView = tbmBaseOpcionesView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagenIcono() {
        return imagenIcono;
    }

    public void setImagenIcono(String imagenIcono) {
        this.imagenIcono = imagenIcono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdOpci() {
        return idOpci;
    }

    public void setIdOpci(String idOpci) {
        this.idOpci = idOpci;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
