package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvAccesosUsuarioView;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvAccesosUsuarioDTO {
    private String idUsua_TsccvUsuario;
    private String idAcus;
    private Date fechaAcceso;
    private boolean rowSelected = false;
    private TsccvAccesosUsuarioView tsccvAccesosUsuarioView;
    private TsccvAccesosUsuario tsccvAccesosUsuario;

    public TsccvAccesosUsuario getTsccvAccesosUsuario() {
        return tsccvAccesosUsuario;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvAccesosUsuarioView.action_modifyWitDTO(((fechaAcceso == null) ||
                fechaAcceso.equals("")) ? null : fechaAcceso,
                ((idAcus == null) || idAcus.equals("")) ? null
                                                        : new Integer(idAcus),
                ((idUsua_TsccvUsuario == null) ||
                idUsua_TsccvUsuario.equals("")) ? null
                                                : new Integer(
                    idUsua_TsccvUsuario));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idAcus = tsccvAccesosUsuario.getIdAcus().toString();
        fechaAcceso = tsccvAccesosUsuario.getFechaAcceso();
        idUsua_TsccvUsuario = (tsccvAccesosUsuario.getTsccvUsuario().getIdUsua() != null)
            ? tsccvAccesosUsuario.getTsccvUsuario().getIdUsua().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idAcus = tsccvAccesosUsuario.getIdAcus().toString();
        fechaAcceso = tsccvAccesosUsuario.getFechaAcceso();
        idUsua_TsccvUsuario = (tsccvAccesosUsuario.getTsccvUsuario().getIdUsua() != null)
            ? tsccvAccesosUsuario.getTsccvUsuario().getIdUsua().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvAccesosUsuario(TsccvAccesosUsuario tsccvAccesosUsuario) {
        this.tsccvAccesosUsuario = tsccvAccesosUsuario;
    }

    public TsccvAccesosUsuarioView getTsccvAccesosUsuarioView() {
        return tsccvAccesosUsuarioView;
    }

    public void setTsccvAccesosUsuarioView(
        TsccvAccesosUsuarioView tsccvAccesosUsuarioView) {
        this.tsccvAccesosUsuarioView = tsccvAccesosUsuarioView;
    }

    public String getIdUsua_TsccvUsuario() {
        return idUsua_TsccvUsuario;
    }

    public void setIdUsua_TsccvUsuario(String idUsua_TsccvUsuario) {
        this.idUsua_TsccvUsuario = idUsua_TsccvUsuario;
    }

    public Date getFechaAcceso() {
        return fechaAcceso;
    }

    public void setFechaAcceso(Date fechaAcceso) {
        this.fechaAcceso = fechaAcceso;
    }

    public String getIdAcus() {
        return idAcus;
    }

    public void setIdAcus(String idAcus) {
        this.idAcus = idAcus;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
