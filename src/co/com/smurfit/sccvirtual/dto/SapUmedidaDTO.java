package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.SapUmedidaView;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class SapUmedidaDTO {
    private String codume;
    private String desume;
    private boolean rowSelected = false;
    private SapUmedidaView sapUmedidaView;
    private SapUmedida sapUmedida;

    public SapUmedida getSapUmedida() {
        return sapUmedida;
    }

    public String listener_update(ActionEvent e) {
        try {
            sapUmedidaView.action_modifyWitDTO(((codume == null) ||
                codume.equals("")) ? null : new String(codume),
                ((desume == null) || desume.equals("")) ? null
                                                        : new String(desume));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codume = sapUmedida.getId().getCodume().toString();
        desume = sapUmedida.getId().getDesume().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codume = sapUmedida.getId().getCodume().toString();
        desume = sapUmedida.getId().getDesume().toString();
        rowSelected = !rowSelected;
    }

    public void setSapUmedida(SapUmedida sapUmedida) {
        this.sapUmedida = sapUmedida;
    }

    public SapUmedidaView getSapUmedidaView() {
        return sapUmedidaView;
    }

    public void setSapUmedidaView(SapUmedidaView sapUmedidaView) {
        this.sapUmedidaView = sapUmedidaView;
    }

    public String getCodume() {
        return codume;
    }

    public void setCodume(String codume) {
        this.codume = codume;
    }

    public String getDesume() {
        return desume;
    }

    public void setDesume(String desume) {
        this.desume = desume;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
