package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.BvpHistDespachosView;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class BvpHistDespachosDTO {
    private String planta;
    private String nummes;
    private String codcli;
    private String codscc;
    private String codpro;
    private String despro;
    private String tippro;
    private String undmed;
    private String cantid;
    private boolean rowSelected = false;
    private BvpHistDespachosView bvpHistDespachosView;
    private BvpHistDespachos bvpHistDespachos;

    public BvpHistDespachos getBvpHistDespachos() {
        return bvpHistDespachos;
    }

    public String listener_update(ActionEvent e) {
        try {
            bvpHistDespachosView.action_modifyWitDTO(((planta == null) ||
                planta.equals("")) ? null : new String(planta),
                ((nummes == null) || nummes.equals("")) ? null
                                                        : new String(nummes),
                ((codcli == null) || codcli.equals("")) ? null
                                                        : new String(codcli),
                ((codscc == null) || codscc.equals("")) ? null
                                                        : new String(codscc),
                ((codpro == null) || codpro.equals("")) ? null
                                                        : new String(codpro),
                ((despro == null) || despro.equals("")) ? null
                                                        : new String(despro),
                ((tippro == null) || tippro.equals("")) ? null
                                                        : new String(tippro),
                ((undmed == null) || undmed.equals("")) ? null
                                                        : new String(undmed),
                ((cantid == null) || cantid.equals("")) ? null
                                                        : new String(cantid));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        planta = bvpHistDespachos.getId().getPlanta().toString();
        nummes = bvpHistDespachos.getId().getNummes().toString();
        codcli = bvpHistDespachos.getId().getCodcli().toString();
        codscc = bvpHistDespachos.getId().getCodscc().toString();
        codpro = bvpHistDespachos.getId().getCodpro().toString();
        despro = bvpHistDespachos.getId().getDespro().toString();
        tippro = bvpHistDespachos.getId().getTippro().toString();
        undmed = bvpHistDespachos.getId().getUndmed().toString();
        cantid = bvpHistDespachos.getId().getCantid().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        planta = bvpHistDespachos.getId().getPlanta().toString();
        nummes = bvpHistDespachos.getId().getNummes().toString();
        codcli = bvpHistDespachos.getId().getCodcli().toString();
        codscc = bvpHistDespachos.getId().getCodscc().toString();
        codpro = bvpHistDespachos.getId().getCodpro().toString();
        despro = bvpHistDespachos.getId().getDespro().toString();
        tippro = bvpHistDespachos.getId().getTippro().toString();
        undmed = bvpHistDespachos.getId().getUndmed().toString();
        cantid = bvpHistDespachos.getId().getCantid().toString();
        rowSelected = !rowSelected;
    }

    public void setBvpHistDespachos(BvpHistDespachos bvpHistDespachos) {
        this.bvpHistDespachos = bvpHistDespachos;
    }

    public BvpHistDespachosView getBvpHistDespachosView() {
        return bvpHistDespachosView;
    }

    public void setBvpHistDespachosView(
        BvpHistDespachosView bvpHistDespachosView) {
        this.bvpHistDespachosView = bvpHistDespachosView;
    }

    public String getPlanta() {
        return planta;
    }

    public void setPlanta(String planta) {
        this.planta = planta;
    }

    public String getNummes() {
        return nummes;
    }

    public void setNummes(String nummes) {
        this.nummes = nummes;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getCodscc() {
        return codscc;
    }

    public void setCodscc(String codscc) {
        this.codscc = codscc;
    }

    public String getCodpro() {
        return codpro;
    }

    public void setCodpro(String codpro) {
        this.codpro = codpro;
    }

    public String getDespro() {
        return despro;
    }

    public void setDespro(String despro) {
        this.despro = despro;
    }

    public String getTippro() {
        return tippro;
    }

    public void setTippro(String tippro) {
        this.tippro = tippro;
    }

    public String getUndmed() {
        return undmed;
    }

    public void setUndmed(String undmed) {
        this.undmed = undmed;
    }

    public String getCantid() {
        return cantid;
    }

    public void setCantid(String cantid) {
        this.cantid = cantid;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
