package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvEmpresasView;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.vo.DetalleGrupoEmpresaVO;

import java.util.ArrayList;
import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvEmpresasDTO {
    private String activo;
    private String codMas;
    private String codSap;
    private String codVpe;
    private String nombre;
    private String idGrue_TsccvGrupoEmpresa;
    private String idEmpr;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TsccvEmpresasView tsccvEmpresasView;
    private TsccvEmpresas tsccvEmpresas;

    public TsccvEmpresas getTsccvEmpresas() {
        return tsccvEmpresas;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvEmpresasView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((codMas == null) || codMas.equals("")) ? null
                                                        : new String(codMas),
                ((codSap == null) || codSap.equals("")) ? null
                                                        : new String(codSap),
                ((codVpe == null) || codVpe.equals("")) ? null
                                                        : new String(codVpe),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idEmpr == null) || idEmpr.equals("")) ? null
                                                        : new Integer(idEmpr),
                ((nombre == null) || nombre.equals("")) ? null
                                                        : new String(nombre),
                ((idGrue_TsccvGrupoEmpresa == null) ||
                idGrue_TsccvGrupoEmpresa.equals("")) ? null
                                                     : new Integer(
                    idGrue_TsccvGrupoEmpresa), new ArrayList<DetalleGrupoEmpresaVO>());
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idEmpr = tsccvEmpresas.getIdEmpr().toString();
        activo = (tsccvEmpresas.getActivo() != null)
            ? tsccvEmpresas.getActivo().toString() : null;
        codMas = (tsccvEmpresas.getCodMas() != null)
            ? tsccvEmpresas.getCodMas().toString() : null;
        codSap = (tsccvEmpresas.getCodSap() != null)
            ? tsccvEmpresas.getCodSap().toString() : null;
        codVpe = (tsccvEmpresas.getCodVpe() != null)
            ? tsccvEmpresas.getCodVpe().toString() : null;
        fechaCreacion = tsccvEmpresas.getFechaCreacion();
        nombre = (tsccvEmpresas.getNombre() != null)
            ? tsccvEmpresas.getNombre().toString() : null;
        idGrue_TsccvGrupoEmpresa = (tsccvEmpresas.getTsccvGrupoEmpresa()
                                                 .getIdGrue() != null)
            ? tsccvEmpresas.getTsccvGrupoEmpresa().getIdGrue().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idEmpr = tsccvEmpresas.getIdEmpr().toString();
        activo = (tsccvEmpresas.getActivo() != null)
            ? tsccvEmpresas.getActivo().toString() : null;
        codMas = (tsccvEmpresas.getCodMas() != null)
            ? tsccvEmpresas.getCodMas().toString() : null;
        codSap = (tsccvEmpresas.getCodSap() != null)
            ? tsccvEmpresas.getCodSap().toString() : null;
        codVpe = (tsccvEmpresas.getCodVpe() != null)
            ? tsccvEmpresas.getCodVpe().toString() : null;
        fechaCreacion = tsccvEmpresas.getFechaCreacion();
        nombre = (tsccvEmpresas.getNombre() != null)
            ? tsccvEmpresas.getNombre().toString() : null;
        idGrue_TsccvGrupoEmpresa = (tsccvEmpresas.getTsccvGrupoEmpresa()
                                                 .getIdGrue() != null)
            ? tsccvEmpresas.getTsccvGrupoEmpresa().getIdGrue().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvEmpresas(TsccvEmpresas tsccvEmpresas) {
        this.tsccvEmpresas = tsccvEmpresas;
    }

    public TsccvEmpresasView getTsccvEmpresasView() {
        return tsccvEmpresasView;
    }

    public void setTsccvEmpresasView(TsccvEmpresasView tsccvEmpresasView) {
        this.tsccvEmpresasView = tsccvEmpresasView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getCodMas() {
        return codMas;
    }

    public void setCodMas(String codMas) {
        this.codMas = codMas;
    }

    public String getCodSap() {
        return codSap;
    }

    public void setCodSap(String codSap) {
        this.codSap = codSap;
    }

    public String getCodVpe() {
        return codVpe;
    }

    public void setCodVpe(String codVpe) {
        this.codVpe = codVpe;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdGrue_TsccvGrupoEmpresa() {
        return idGrue_TsccvGrupoEmpresa;
    }

    public void setIdGrue_TsccvGrupoEmpresa(String idGrue_TsccvGrupoEmpresa) {
        this.idGrue_TsccvGrupoEmpresa = idGrue_TsccvGrupoEmpresa;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(String idEmpr) {
        this.idEmpr = idEmpr;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
