package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.SapDirDspView;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class SapDirDspDTO {
    private String codcli;
    private String coddir;
    private String desdir;
    private String numfax;
    private String desprv;
    private String desciu;
    private String despai;
    private boolean rowSelected = false;
    private SapDirDspView sapDirDspView;
    private SapDirDsp sapDirDsp;

    public SapDirDsp getSapDirDsp() {
        return sapDirDsp;
    }

    public String listener_update(ActionEvent e) {
        try {
            sapDirDspView.action_modifyWitDTO(((codcli == null) ||
                codcli.equals("")) ? null : new String(codcli),
                ((coddir == null) || coddir.equals("")) ? null
                                                        : new String(coddir),
                ((desdir == null) || desdir.equals("")) ? null
                                                        : new String(desdir),
                ((numfax == null) || numfax.equals("")) ? null
                                                        : new String(numfax),
                ((desprv == null) || desprv.equals("")) ? null
                                                        : new String(desprv),
                ((desciu == null) || desciu.equals("")) ? null
                                                        : new String(desciu),
                ((despai == null) || despai.equals("")) ? null
                                                        : new String(despai));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codcli = sapDirDsp.getId().getCodcli().toString();
        coddir = sapDirDsp.getId().getCoddir().toString();
        desdir = sapDirDsp.getId().getDesdir().toString();
        numfax = sapDirDsp.getId().getNumfax().toString();
        desprv = sapDirDsp.getId().getDesprv().toString();
        desciu = sapDirDsp.getId().getDesciu().toString();
        despai = sapDirDsp.getId().getDespai().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codcli = sapDirDsp.getId().getCodcli().toString();
        coddir = sapDirDsp.getId().getCoddir().toString();
        desdir = sapDirDsp.getId().getDesdir().toString();
        numfax = sapDirDsp.getId().getNumfax().toString();
        desprv = sapDirDsp.getId().getDesprv().toString();
        desciu = sapDirDsp.getId().getDesciu().toString();
        despai = sapDirDsp.getId().getDespai().toString();
        rowSelected = !rowSelected;
    }

    public void setSapDirDsp(SapDirDsp sapDirDsp) {
        this.sapDirDsp = sapDirDsp;
    }

    public SapDirDspView getSapDirDspView() {
        return sapDirDspView;
    }

    public void setSapDirDspView(SapDirDspView sapDirDspView) {
        this.sapDirDspView = sapDirDspView;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getCoddir() {
        return coddir;
    }

    public void setCoddir(String coddir) {
        this.coddir = coddir;
    }

    public String getDesdir() {
        return desdir;
    }

    public void setDesdir(String desdir) {
        this.desdir = desdir;
    }

    public String getNumfax() {
        return numfax;
    }

    public void setNumfax(String numfax) {
        this.numfax = numfax;
    }

    public String getDesprv() {
        return desprv;
    }

    public void setDesprv(String desprv) {
        this.desprv = desprv;
    }

    public String getDesciu() {
        return desciu;
    }

    public void setDesciu(String desciu) {
        this.desciu = desciu;
    }

    public String getDespai() {
        return despai;
    }

    public void setDespai(String despai) {
        this.despai = despai;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
