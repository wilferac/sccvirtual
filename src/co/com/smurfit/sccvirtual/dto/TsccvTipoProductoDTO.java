package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvTipoProductoView;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvTipoProductoDTO {
    private String activo;
    private String descripcion;
    private String idTipr;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TsccvTipoProductoView tsccvTipoProductoView;
    private TsccvTipoProducto tsccvTipoProducto;

    public TsccvTipoProducto getTsccvTipoProducto() {
        return tsccvTipoProducto;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvTipoProductoView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((descripcion == null) || descripcion.equals("")) ? null
                                                                  : new String(
                    descripcion),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idTipr == null) || idTipr.equals("")) ? null : new Long(
                    idTipr));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idTipr = tsccvTipoProducto.getIdTipr().toString();
        activo = (tsccvTipoProducto.getActivo() != null)
            ? tsccvTipoProducto.getActivo().toString() : null;
        descripcion = (tsccvTipoProducto.getDescripcion() != null)
            ? tsccvTipoProducto.getDescripcion().toString() : null;
        fechaCreacion = tsccvTipoProducto.getFechaCreacion();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idTipr = tsccvTipoProducto.getIdTipr().toString();
        activo = (tsccvTipoProducto.getActivo() != null)
            ? tsccvTipoProducto.getActivo().toString() : null;
        descripcion = (tsccvTipoProducto.getDescripcion() != null)
            ? tsccvTipoProducto.getDescripcion().toString() : null;
        fechaCreacion = tsccvTipoProducto.getFechaCreacion();
        rowSelected = !rowSelected;
    }

    public void setTsccvTipoProducto(TsccvTipoProducto tsccvTipoProducto) {
        this.tsccvTipoProducto = tsccvTipoProducto;
    }

    public TsccvTipoProductoView getTsccvTipoProductoView() {
        return tsccvTipoProductoView;
    }

    public void setTsccvTipoProductoView(
        TsccvTipoProductoView tsccvTipoProductoView) {
        this.tsccvTipoProductoView = tsccvTipoProductoView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdTipr() {
        return idTipr;
    }

    public void setIdTipr(String idTipr) {
        this.idTipr = idTipr;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
