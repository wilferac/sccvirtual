package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TbmBaseOpcionesGruposView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TbmBaseOpcionesGruposDTO {
    private String idGrup_TbmBaseGruposUsuarios;
    private String idOpci_TbmBaseOpciones;
    private String idOpci;
    private String idGrup;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TbmBaseOpcionesGruposView tbmBaseOpcionesGruposView;
    private TbmBaseOpcionesGrupos tbmBaseOpcionesGrupos;

    public TbmBaseOpcionesGrupos getTbmBaseOpcionesGrupos() {
        return tbmBaseOpcionesGrupos;
    }

    public String listener_update(ActionEvent e) {
        try {
            tbmBaseOpcionesGruposView.action_modifyWitDTO(((idOpci == null) ||
                idOpci.equals("")) ? null : new Long(idOpci),
                ((idGrup == null) || idGrup.equals("")) ? null : new Long(
                    idGrup),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idGrup_TbmBaseGruposUsuarios == null) ||
                idGrup_TbmBaseGruposUsuarios.equals("")) ? null
                                                         : new Long(
                    idGrup_TbmBaseGruposUsuarios),
                ((idOpci_TbmBaseOpciones == null) ||
                idOpci_TbmBaseOpciones.equals("")) ? null
                                                   : new Long(
                    idOpci_TbmBaseOpciones));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idOpci = tbmBaseOpcionesGrupos.getId().getIdOpci().toString();
        idGrup = tbmBaseOpcionesGrupos.getId().getIdGrup().toString();
        fechaCreacion = tbmBaseOpcionesGrupos.getFechaCreacion();
        idGrup_TbmBaseGruposUsuarios = (tbmBaseOpcionesGrupos.getTbmBaseGruposUsuarios()
                                                             .getIdGrup() != null)
            ? tbmBaseOpcionesGrupos.getTbmBaseGruposUsuarios().getIdGrup()
                                   .toString() : null;
        idOpci_TbmBaseOpciones = (tbmBaseOpcionesGrupos.getTbmBaseOpciones()
                                                       .getIdOpci() != null)
            ? tbmBaseOpcionesGrupos.getTbmBaseOpciones().getIdOpci().toString()
            : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idOpci = tbmBaseOpcionesGrupos.getId().getIdOpci().toString();
        idGrup = tbmBaseOpcionesGrupos.getId().getIdGrup().toString();
        fechaCreacion = tbmBaseOpcionesGrupos.getFechaCreacion();
        idGrup_TbmBaseGruposUsuarios = (tbmBaseOpcionesGrupos.getTbmBaseGruposUsuarios()
                                                             .getIdGrup() != null)
            ? tbmBaseOpcionesGrupos.getTbmBaseGruposUsuarios().getIdGrup()
                                   .toString() : null;
        idOpci_TbmBaseOpciones = (tbmBaseOpcionesGrupos.getTbmBaseOpciones()
                                                       .getIdOpci() != null)
            ? tbmBaseOpcionesGrupos.getTbmBaseOpciones().getIdOpci().toString()
            : null;
        rowSelected = !rowSelected;
    }

    public void setTbmBaseOpcionesGrupos(
        TbmBaseOpcionesGrupos tbmBaseOpcionesGrupos) {
        this.tbmBaseOpcionesGrupos = tbmBaseOpcionesGrupos;
    }

    public TbmBaseOpcionesGruposView getTbmBaseOpcionesGruposView() {
        return tbmBaseOpcionesGruposView;
    }

    public void setTbmBaseOpcionesGruposView(
        TbmBaseOpcionesGruposView tbmBaseOpcionesGruposView) {
        this.tbmBaseOpcionesGruposView = tbmBaseOpcionesGruposView;
    }

    public String getIdGrup_TbmBaseGruposUsuarios() {
        return idGrup_TbmBaseGruposUsuarios;
    }

    public void setIdGrup_TbmBaseGruposUsuarios(
        String idGrup_TbmBaseGruposUsuarios) {
        this.idGrup_TbmBaseGruposUsuarios = idGrup_TbmBaseGruposUsuarios;
    }

    public String getIdOpci_TbmBaseOpciones() {
        return idOpci_TbmBaseOpciones;
    }

    public void setIdOpci_TbmBaseOpciones(String idOpci_TbmBaseOpciones) {
        this.idOpci_TbmBaseOpciones = idOpci_TbmBaseOpciones;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdOpci() {
        return idOpci;
    }

    public void setIdOpci(String idOpci) {
        this.idOpci = idOpci;
    }

    public String getIdGrup() {
        return idGrup;
    }

    public void setIdGrup(String idGrup) {
        this.idGrup = idGrup;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
