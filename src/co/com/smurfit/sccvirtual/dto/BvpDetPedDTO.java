package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.BvpDetPedView;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class BvpDetPedDTO {
    private String planta;
    private String numped;
    private String itmped;
    private String codcli;
    private String codscc;
    private String codpro;
    private String tippro;
    private String despro;
    private String fecsol;
    private String fecdsp;
    private String estado;
    private String undmed;
    private String cntped;
    private String cntdsp;
    private String ordcom;
    private boolean rowSelected = false;
    private BvpDetPedView bvpDetPedView;
    private BvpDetPed bvpDetPed;

    public BvpDetPed getBvpDetPed() {
        return bvpDetPed;
    }

    public String listener_update(ActionEvent e) {
        try {
            bvpDetPedView.action_modifyWitDTO(((planta == null) ||
                planta.equals("")) ? null : new String(planta),
                ((numped == null) || numped.equals("")) ? null
                                                        : new String(numped),
                ((itmped == null) || itmped.equals("")) ? null
                                                        : new String(itmped),
                ((codcli == null) || codcli.equals("")) ? null
                                                        : new String(codcli),
                ((codscc == null) || codscc.equals("")) ? null
                                                        : new String(codscc),
                ((codpro == null) || codpro.equals("")) ? null
                                                        : new String(codpro),
                ((tippro == null) || tippro.equals("")) ? null
                                                        : new String(tippro),
                ((despro == null) || despro.equals("")) ? null
                                                        : new String(despro),
                ((fecsol == null) || fecsol.equals("")) ? null
                                                        : new String(fecsol),
                ((fecdsp == null) || fecdsp.equals("")) ? null
                                                        : new String(fecdsp),
                ((estado == null) || estado.equals("")) ? null
                                                        : new String(estado),
                ((undmed == null) || undmed.equals("")) ? null
                                                        : new String(undmed),
                ((cntped == null) || cntped.equals("")) ? null
                                                        : new String(cntped),
                ((cntdsp == null) || cntdsp.equals("")) ? null
                                                        : new String(cntdsp),
                ((ordcom == null) || ordcom.equals("")) ? null
                                                        : new String(ordcom));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        planta = bvpDetPed.getId().getPlanta().toString();
        numped = bvpDetPed.getId().getNumped().toString();
        itmped = bvpDetPed.getId().getItmped().toString();
        codcli = bvpDetPed.getId().getCodcli().toString();
        codscc = bvpDetPed.getId().getCodscc().toString();
        codpro = bvpDetPed.getId().getCodpro().toString();
        tippro = bvpDetPed.getId().getTippro().toString();
        despro = bvpDetPed.getId().getDespro().toString();
        fecsol = bvpDetPed.getId().getFecsol().toString();
        fecdsp = bvpDetPed.getId().getFecdsp().toString();
        estado = bvpDetPed.getId().getEstado().toString();
        undmed = bvpDetPed.getId().getUndmed().toString();
        cntped = bvpDetPed.getId().getCntped().toString();
        cntdsp = bvpDetPed.getId().getCntdsp().toString();
        ordcom = bvpDetPed.getId().getOrdcom().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        planta = bvpDetPed.getId().getPlanta().toString();
        numped = bvpDetPed.getId().getNumped().toString();
        itmped = bvpDetPed.getId().getItmped().toString();
        codcli = bvpDetPed.getId().getCodcli().toString();
        codscc = bvpDetPed.getId().getCodscc().toString();
        codpro = bvpDetPed.getId().getCodpro().toString();
        tippro = bvpDetPed.getId().getTippro().toString();
        despro = bvpDetPed.getId().getDespro().toString();
        fecsol = bvpDetPed.getId().getFecsol().toString();
        fecdsp = bvpDetPed.getId().getFecdsp().toString();
        estado = bvpDetPed.getId().getEstado().toString();
        undmed = bvpDetPed.getId().getUndmed().toString();
        cntped = bvpDetPed.getId().getCntped().toString();
        cntdsp = bvpDetPed.getId().getCntdsp().toString();
        ordcom = bvpDetPed.getId().getOrdcom().toString();
        rowSelected = !rowSelected;
    }

    public void setBvpDetPed(BvpDetPed bvpDetPed) {
        this.bvpDetPed = bvpDetPed;
    }

    public BvpDetPedView getBvpDetPedView() {
        return bvpDetPedView;
    }

    public void setBvpDetPedView(BvpDetPedView bvpDetPedView) {
        this.bvpDetPedView = bvpDetPedView;
    }

    public String getPlanta() {
        return planta;
    }

    public void setPlanta(String planta) {
        this.planta = planta;
    }

    public String getNumped() {
        return numped;
    }

    public void setNumped(String numped) {
        this.numped = numped;
    }

    public String getItmped() {
        return itmped;
    }

    public void setItmped(String itmped) {
        this.itmped = itmped;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getCodscc() {
        return codscc;
    }

    public void setCodscc(String codscc) {
        this.codscc = codscc;
    }

    public String getCodpro() {
        return codpro;
    }

    public void setCodpro(String codpro) {
        this.codpro = codpro;
    }

    public String getTippro() {
        return tippro;
    }

    public void setTippro(String tippro) {
        this.tippro = tippro;
    }

    public String getDespro() {
        return despro;
    }

    public void setDespro(String despro) {
        this.despro = despro;
    }

    public String getFecsol() {
        return fecsol;
    }

    public void setFecsol(String fecsol) {
        this.fecsol = fecsol;
    }

    public String getFecdsp() {
        return fecdsp;
    }

    public void setFecdsp(String fecdsp) {
        this.fecdsp = fecdsp;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUndmed() {
        return undmed;
    }

    public void setUndmed(String undmed) {
        this.undmed = undmed;
    }

    public String getCntped() {
        return cntped;
    }

    public void setCntped(String cntped) {
        this.cntped = cntped;
    }

    public String getCntdsp() {
        return cntdsp;
    }

    public void setCntdsp(String cntdsp) {
        this.cntdsp = cntdsp;
    }

    public String getOrdcom() {
        return ordcom;
    }

    public void setOrdcom(String ordcom) {
        this.ordcom = ordcom;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
