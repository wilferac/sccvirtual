package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvPlantasUsuarioView;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvPlantasUsuarioDTO {
    private String codpla_TsccvPlanta;
    private String idUsua_TsccvUsuario;
    private String codpla;
    private String idUsua;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TsccvPlantasUsuarioView tsccvPlantasUsuarioView;
    private TsccvPlantasUsuario tsccvPlantasUsuario;

    public TsccvPlantasUsuario getTsccvPlantasUsuario() {
        return tsccvPlantasUsuario;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvPlantasUsuarioView.action_modifyWitDTO(((codpla == null) ||
                codpla.equals("")) ? null : new String(codpla),
                ((idUsua == null) || idUsua.equals("")) ? null
                                                        : new Integer(idUsua),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((codpla_TsccvPlanta == null) || codpla_TsccvPlanta.equals(""))
                ? null : new String(codpla_TsccvPlanta),
                ((idUsua_TsccvUsuario == null) ||
                idUsua_TsccvUsuario.equals("")) ? null
                                                : new Integer(
                    idUsua_TsccvUsuario));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codpla = tsccvPlantasUsuario.getId().getCodpla().toString();
        idUsua = tsccvPlantasUsuario.getId().getIdUsua().toString();
        fechaCreacion = tsccvPlantasUsuario.getFechaCreacion();
        codpla_TsccvPlanta = (tsccvPlantasUsuario.getTsccvPlanta().getCodpla() != null)
            ? tsccvPlantasUsuario.getTsccvPlanta().getCodpla().toString() : null;
        idUsua_TsccvUsuario = (tsccvPlantasUsuario.getTsccvUsuario().getIdUsua() != null)
            ? tsccvPlantasUsuario.getTsccvUsuario().getIdUsua().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codpla = tsccvPlantasUsuario.getId().getCodpla().toString();
        idUsua = tsccvPlantasUsuario.getId().getIdUsua().toString();
        fechaCreacion = tsccvPlantasUsuario.getFechaCreacion();
        codpla_TsccvPlanta = (tsccvPlantasUsuario.getTsccvPlanta().getCodpla() != null)
            ? tsccvPlantasUsuario.getTsccvPlanta().getCodpla().toString() : null;
        idUsua_TsccvUsuario = (tsccvPlantasUsuario.getTsccvUsuario().getIdUsua() != null)
            ? tsccvPlantasUsuario.getTsccvUsuario().getIdUsua().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvPlantasUsuario(TsccvPlantasUsuario tsccvPlantasUsuario) {
        this.tsccvPlantasUsuario = tsccvPlantasUsuario;
    }

    public TsccvPlantasUsuarioView getTsccvPlantasUsuarioView() {
        return tsccvPlantasUsuarioView;
    }

    public void setTsccvPlantasUsuarioView(
        TsccvPlantasUsuarioView tsccvPlantasUsuarioView) {
        this.tsccvPlantasUsuarioView = tsccvPlantasUsuarioView;
    }

    public String getCodpla_TsccvPlanta() {
        return codpla_TsccvPlanta;
    }

    public void setCodpla_TsccvPlanta(String codpla_TsccvPlanta) {
        this.codpla_TsccvPlanta = codpla_TsccvPlanta;
    }

    public String getIdUsua_TsccvUsuario() {
        return idUsua_TsccvUsuario;
    }

    public void setIdUsua_TsccvUsuario(String idUsua_TsccvUsuario) {
        this.idUsua_TsccvUsuario = idUsua_TsccvUsuario;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getCodpla() {
        return codpla;
    }

    public void setCodpla(String codpla) {
        this.codpla = codpla;
    }

    public String getIdUsua() {
        return idUsua;
    }

    public void setIdUsua(String idUsua) {
        this.idUsua = idUsua;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
