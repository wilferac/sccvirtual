package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvDetalleGrupoEmpresaView;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvDetalleGrupoEmpresaDTO {
    private String idEmpr_TsccvEmpresas;
    private String idGrue_TsccvGrupoEmpresa;
    private String idGrue;
    private String idEmpr;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TsccvDetalleGrupoEmpresaView tsccvDetalleGrupoEmpresaView;
    private TsccvDetalleGrupoEmpresa tsccvDetalleGrupoEmpresa;

    public TsccvDetalleGrupoEmpresa getTsccvDetalleGrupoEmpresa() {
        return tsccvDetalleGrupoEmpresa;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvDetalleGrupoEmpresaView.action_modifyWitDTO(((idGrue == null) ||
                idGrue.equals("")) ? null : new Integer(idGrue),
                ((idEmpr == null) || idEmpr.equals("")) ? null
                                                        : new Integer(idEmpr),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idEmpr_TsccvEmpresas == null) ||
                idEmpr_TsccvEmpresas.equals("")) ? null
                                                 : new Integer(
                    idEmpr_TsccvEmpresas),
                ((idGrue_TsccvGrupoEmpresa == null) ||
                idGrue_TsccvGrupoEmpresa.equals("")) ? null
                                                     : new Integer(
                    idGrue_TsccvGrupoEmpresa));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idGrue = tsccvDetalleGrupoEmpresa.getId().getIdGrue().toString();
        idEmpr = tsccvDetalleGrupoEmpresa.getId().getIdEmpr().toString();
        fechaCreacion = tsccvDetalleGrupoEmpresa.getFechaCreacion();
        idEmpr_TsccvEmpresas = (tsccvDetalleGrupoEmpresa.getTsccvEmpresas()
                                                        .getIdEmpr() != null)
            ? tsccvDetalleGrupoEmpresa.getTsccvEmpresas().getIdEmpr().toString()
            : null;
        idGrue_TsccvGrupoEmpresa = (tsccvDetalleGrupoEmpresa.getTsccvGrupoEmpresa()
                                                            .getIdGrue() != null)
            ? tsccvDetalleGrupoEmpresa.getTsccvGrupoEmpresa().getIdGrue()
                                      .toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idGrue = tsccvDetalleGrupoEmpresa.getId().getIdGrue().toString();
        idEmpr = tsccvDetalleGrupoEmpresa.getId().getIdEmpr().toString();
        fechaCreacion = tsccvDetalleGrupoEmpresa.getFechaCreacion();
        idEmpr_TsccvEmpresas = (tsccvDetalleGrupoEmpresa.getTsccvEmpresas()
                                                        .getIdEmpr() != null)
            ? tsccvDetalleGrupoEmpresa.getTsccvEmpresas().getIdEmpr().toString()
            : null;
        idGrue_TsccvGrupoEmpresa = (tsccvDetalleGrupoEmpresa.getTsccvGrupoEmpresa()
                                                            .getIdGrue() != null)
            ? tsccvDetalleGrupoEmpresa.getTsccvGrupoEmpresa().getIdGrue()
                                      .toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvDetalleGrupoEmpresa(
        TsccvDetalleGrupoEmpresa tsccvDetalleGrupoEmpresa) {
        this.tsccvDetalleGrupoEmpresa = tsccvDetalleGrupoEmpresa;
    }

    public TsccvDetalleGrupoEmpresaView getTsccvDetalleGrupoEmpresaView() {
        return tsccvDetalleGrupoEmpresaView;
    }

    public void setTsccvDetalleGrupoEmpresaView(
        TsccvDetalleGrupoEmpresaView tsccvDetalleGrupoEmpresaView) {
        this.tsccvDetalleGrupoEmpresaView = tsccvDetalleGrupoEmpresaView;
    }

    public String getIdEmpr_TsccvEmpresas() {
        return idEmpr_TsccvEmpresas;
    }

    public void setIdEmpr_TsccvEmpresas(String idEmpr_TsccvEmpresas) {
        this.idEmpr_TsccvEmpresas = idEmpr_TsccvEmpresas;
    }

    public String getIdGrue_TsccvGrupoEmpresa() {
        return idGrue_TsccvGrupoEmpresa;
    }

    public void setIdGrue_TsccvGrupoEmpresa(String idGrue_TsccvGrupoEmpresa) {
        this.idGrue_TsccvGrupoEmpresa = idGrue_TsccvGrupoEmpresa;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdGrue() {
        return idGrue;
    }

    public void setIdGrue(String idGrue) {
        this.idGrue = idGrue;
    }

    public String getIdEmpr() {
        return idEmpr;
    }

    public void setIdEmpr(String idEmpr) {
        this.idEmpr = idEmpr;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
