package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.SapPrecioView;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class SapPrecioDTO {
    private String despro;
    private String diamtr;
    private String eje;
    private String moneda;
    private String tiprec;
    private String undmed;
    private String codcli;
    private String codpro;
    private String precio;
    private boolean rowSelected = false;
    private SapPrecioView sapPrecioView;
    private SapPrecio sapPrecio;

    public SapPrecio getSapPrecio() {
        return sapPrecio;
    }

    public String listener_update(ActionEvent e) {
        try {
            sapPrecioView.action_modifyWitDTO(((codcli == null) ||
                codcli.equals("")) ? null : new String(codcli),
                ((codpro == null) || codpro.equals("")) ? null
                                                        : new String(codpro),
                ((precio == null) || precio.equals("")) ? null
                                                        : new String(precio),
                ((despro == null) || despro.equals("")) ? null
                                                        : new String(despro),
                ((diamtr == null) || diamtr.equals("")) ? null
                                                        : new String(diamtr),
                ((eje == null) || eje.equals("")) ? null : new String(eje),
                ((moneda == null) || moneda.equals("")) ? null
                                                        : new String(moneda),
                ((tiprec == null) || tiprec.equals("")) ? null
                                                        : new String(tiprec),
                ((undmed == null) || undmed.equals("")) ? null
                                                        : new String(undmed));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codcli = sapPrecio.getId().getCodcli().toString();
        codpro = sapPrecio.getId().getCodpro().toString();
        precio = sapPrecio.getId().getPrecio().toString();
        despro = (sapPrecio.getDespro() != null)
            ? sapPrecio.getDespro().toString() : null;
        diamtr = (sapPrecio.getDiamtr() != null)
            ? sapPrecio.getDiamtr().toString() : null;
        eje = (sapPrecio.getEje() != null) ? sapPrecio.getEje().toString() : null;
        moneda = (sapPrecio.getMoneda() != null)
            ? sapPrecio.getMoneda().toString() : null;
        tiprec = (sapPrecio.getTiprec() != null)
            ? sapPrecio.getTiprec().toString() : null;
        undmed = (sapPrecio.getUndmed() != null)
            ? sapPrecio.getUndmed().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codcli = sapPrecio.getId().getCodcli().toString();
        codpro = sapPrecio.getId().getCodpro().toString();
        precio = sapPrecio.getId().getPrecio().toString();
        despro = (sapPrecio.getDespro() != null)
            ? sapPrecio.getDespro().toString() : null;
        diamtr = (sapPrecio.getDiamtr() != null)
            ? sapPrecio.getDiamtr().toString() : null;
        eje = (sapPrecio.getEje() != null) ? sapPrecio.getEje().toString() : null;
        moneda = (sapPrecio.getMoneda() != null)
            ? sapPrecio.getMoneda().toString() : null;
        tiprec = (sapPrecio.getTiprec() != null)
            ? sapPrecio.getTiprec().toString() : null;
        undmed = (sapPrecio.getUndmed() != null)
            ? sapPrecio.getUndmed().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setSapPrecio(SapPrecio sapPrecio) {
        this.sapPrecio = sapPrecio;
    }

    public SapPrecioView getSapPrecioView() {
        return sapPrecioView;
    }

    public void setSapPrecioView(SapPrecioView sapPrecioView) {
        this.sapPrecioView = sapPrecioView;
    }

    public String getDespro() {
        return despro;
    }

    public void setDespro(String despro) {
        this.despro = despro;
    }

    public String getDiamtr() {
        return diamtr;
    }

    public void setDiamtr(String diamtr) {
        this.diamtr = diamtr;
    }

    public String getEje() {
        return eje;
    }

    public void setEje(String eje) {
        this.eje = eje;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getTiprec() {
        return tiprec;
    }

    public void setTiprec(String tiprec) {
        this.tiprec = tiprec;
    }

    public String getUndmed() {
        return undmed;
    }

    public void setUndmed(String undmed) {
        this.undmed = undmed;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getCodpro() {
        return codpro;
    }

    public void setCodpro(String codpro) {
        this.codpro = codpro;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
