package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TbmBaseErroresView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TbmBaseErroresDTO {
    private String activo;
    private String codigo;
    private String descripcion;
    private String solucion;
    private String idErr;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TbmBaseErroresView tbmBaseErroresView;
    private TbmBaseErrores tbmBaseErrores;

    public TbmBaseErrores getTbmBaseErrores() {
        return tbmBaseErrores;
    }

    public String listener_update(ActionEvent e) {
        try {
            tbmBaseErroresView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((codigo == null) || codigo.equals("")) ? null : new Long(
                    codigo),
                ((descripcion == null) || descripcion.equals("")) ? null
                                                                  : new String(
                    descripcion),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idErr == null) || idErr.equals("")) ? null : new Long(idErr),
                ((solucion == null) || solucion.equals("")) ? null
                                                            : new String(
                    solucion));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idErr = tbmBaseErrores.getIdErr().toString();
        activo = (tbmBaseErrores.getActivo() != null)
            ? tbmBaseErrores.getActivo().toString() : null;
        codigo = (tbmBaseErrores.getCodigo() != null)
            ? tbmBaseErrores.getCodigo().toString() : null;
        descripcion = (tbmBaseErrores.getDescripcion() != null)
            ? tbmBaseErrores.getDescripcion().toString() : null;
        fechaCreacion = tbmBaseErrores.getFechaCreacion();
        solucion = (tbmBaseErrores.getSolucion() != null)
            ? tbmBaseErrores.getSolucion().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idErr = tbmBaseErrores.getIdErr().toString();
        activo = (tbmBaseErrores.getActivo() != null)
            ? tbmBaseErrores.getActivo().toString() : null;
        codigo = (tbmBaseErrores.getCodigo() != null)
            ? tbmBaseErrores.getCodigo().toString() : null;
        descripcion = (tbmBaseErrores.getDescripcion() != null)
            ? tbmBaseErrores.getDescripcion().toString() : null;
        fechaCreacion = tbmBaseErrores.getFechaCreacion();
        solucion = (tbmBaseErrores.getSolucion() != null)
            ? tbmBaseErrores.getSolucion().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTbmBaseErrores(TbmBaseErrores tbmBaseErrores) {
        this.tbmBaseErrores = tbmBaseErrores;
    }

    public TbmBaseErroresView getTbmBaseErroresView() {
        return tbmBaseErroresView;
    }

    public void setTbmBaseErroresView(TbmBaseErroresView tbmBaseErroresView) {
        this.tbmBaseErroresView = tbmBaseErroresView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSolucion() {
        return solucion;
    }

    public void setSolucion(String solucion) {
        this.solucion = solucion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdErr() {
        return idErr;
    }

    public void setIdErr(String idErr) {
        this.idErr = idErr;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
