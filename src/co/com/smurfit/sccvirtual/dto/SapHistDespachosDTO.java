package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.SapHistDespachosView;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class SapHistDespachosDTO {
    private String codpro;
    private String nummes;
    private String codcli;
    private String cantid;
    private String despro;
    private String undmed;
    private String cankgs;
    private boolean rowSelected = false;
    private SapHistDespachosView sapHistDespachosView;
    private SapHistDespachos sapHistDespachos;

    public SapHistDespachos getSapHistDespachos() {
        return sapHistDespachos;
    }

    public String listener_update(ActionEvent e) {
        try {
            sapHistDespachosView.action_modifyWitDTO(((codpro == null) ||
                codpro.equals("")) ? null : new String(codpro),
                ((nummes == null) || nummes.equals("")) ? null
                                                        : new String(nummes),
                ((codcli == null) || codcli.equals("")) ? null
                                                        : new String(codcli),
                ((cantid == null) || cantid.equals("")) ? null
                                                        : new String(cantid),
                ((despro == null) || despro.equals("")) ? null
                                                        : new String(despro),
                ((undmed == null) || undmed.equals("")) ? null
                                                        : new String(undmed),
                ((cankgs == null) || cankgs.equals("")) ? null
                                                        : new String(cankgs));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codpro = sapHistDespachos.getId().getCodpro().toString();
        nummes = sapHistDespachos.getId().getNummes().toString();
        codcli = sapHistDespachos.getId().getCodcli().toString();
        cantid = sapHistDespachos.getId().getCantid().toString();
        despro = sapHistDespachos.getId().getDespro().toString();
        undmed = sapHistDespachos.getId().getUndmed().toString();
        cankgs = sapHistDespachos.getId().getCankgs().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codpro = sapHistDespachos.getId().getCodpro().toString();
        nummes = sapHistDespachos.getId().getNummes().toString();
        codcli = sapHistDespachos.getId().getCodcli().toString();
        cantid = sapHistDespachos.getId().getCantid().toString();
        despro = sapHistDespachos.getId().getDespro().toString();
        undmed = sapHistDespachos.getId().getUndmed().toString();
        cankgs = sapHistDespachos.getId().getCankgs().toString();
        rowSelected = !rowSelected;
    }

    public void setSapHistDespachos(SapHistDespachos sapHistDespachos) {
        this.sapHistDespachos = sapHistDespachos;
    }

    public SapHistDespachosView getSapHistDespachosView() {
        return sapHistDespachosView;
    }

    public void setSapHistDespachosView(
        SapHistDespachosView sapHistDespachosView) {
        this.sapHistDespachosView = sapHistDespachosView;
    }

    public String getCodpro() {
        return codpro;
    }

    public void setCodpro(String codpro) {
        this.codpro = codpro;
    }

    public String getNummes() {
        return nummes;
    }

    public void setNummes(String nummes) {
        this.nummes = nummes;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getCantid() {
        return cantid;
    }

    public void setCantid(String cantid) {
        this.cantid = cantid;
    }

    public String getDespro() {
        return despro;
    }

    public void setDespro(String despro) {
        this.despro = despro;
    }

    public String getUndmed() {
        return undmed;
    }

    public void setUndmed(String undmed) {
        this.undmed = undmed;
    }

    public String getCankgs() {
        return cankgs;
    }

    public void setCankgs(String cankgs) {
        this.cankgs = cankgs;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
