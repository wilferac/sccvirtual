package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.BvpPrecioView;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class BvpPrecioDTO {
    private String alto;
    private String ancho;
    private String ancsac;
    private String clave;
    private String cntmax;
    private String cntmin;
    private String codpro;
    private String despro;
    private String estilo;
    private String fuelle;
    private String hijpro;
    private String kitpro;
    private String largo;
    private String larsac;
    private String moneda;
    private String numcol;
    private String refere;
    private String tippro;
    private String undmed;
    private String codCli;
    private String codpla;
    private String codScc;
    private String precio;
    private boolean rowSelected = false;
    private BvpPrecioView bvpPrecioView;
    private BvpPrecio bvpPrecio;

    public BvpPrecio getBvpPrecio() {
        return bvpPrecio;
    }

    public String listener_update(ActionEvent e) {
        try {
            bvpPrecioView.action_modifyWitDTO(((codCli == null) ||
                codCli.equals("")) ? null : new String(codCli),
                ((codpla == null) || codpla.equals("")) ? null
                                                        : new String(codpla),
                ((codScc == null) || codScc.equals("")) ? null
                                                        : new String(codScc),
                ((precio == null) || precio.equals("")) ? null
                                                        : new String(precio),
                ((alto == null) || alto.equals("")) ? null : new String(alto),
                ((ancho == null) || ancho.equals("")) ? null : new String(ancho),
                ((ancsac == null) || ancsac.equals("")) ? null
                                                        : new String(ancsac),
                ((clave == null) || clave.equals("")) ? null : new String(clave),
                ((cntmax == null) || cntmax.equals("")) ? null
                                                        : new String(cntmax),
                ((cntmin == null) || cntmin.equals("")) ? null
                                                        : new String(cntmin),
                ((codpro == null) || codpro.equals("")) ? null
                                                        : new String(codpro),
                ((despro == null) || despro.equals("")) ? null
                                                        : new String(despro),
                ((estilo == null) || estilo.equals("")) ? null
                                                        : new String(estilo),
                ((fuelle == null) || fuelle.equals("")) ? null
                                                        : new String(fuelle),
                ((hijpro == null) || hijpro.equals("")) ? null
                                                        : new String(hijpro),
                ((kitpro == null) || kitpro.equals("")) ? null
                                                        : new String(kitpro),
                ((largo == null) || largo.equals("")) ? null : new String(largo),
                ((larsac == null) || larsac.equals("")) ? null
                                                        : new String(larsac),
                ((moneda == null) || moneda.equals("")) ? null
                                                        : new String(moneda),
                ((numcol == null) || numcol.equals("")) ? null
                                                        : new String(numcol),
                ((refere == null) || refere.equals("")) ? null
                                                        : new String(refere),
                ((tippro == null) || tippro.equals("")) ? null
                                                        : new String(tippro),
                ((undmed == null) || undmed.equals("")) ? null
                                                        : new String(undmed));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codCli = bvpPrecio.getId().getCodCli().toString();
        codpla = bvpPrecio.getId().getCodpla().toString();
        codScc = bvpPrecio.getId().getCodScc().toString();
        precio = bvpPrecio.getId().getPrecio().toString();
        alto = (bvpPrecio.getAlto() != null) ? bvpPrecio.getAlto().toString()
                                             : null;
        ancho = (bvpPrecio.getAncho() != null)
            ? bvpPrecio.getAncho().toString() : null;
        ancsac = (bvpPrecio.getAncsac() != null)
            ? bvpPrecio.getAncsac().toString() : null;
        clave = (bvpPrecio.getClave() != null)
            ? bvpPrecio.getClave().toString() : null;
        cntmax = (bvpPrecio.getCntmax() != null)
            ? bvpPrecio.getCntmax().toString() : null;
        cntmin = (bvpPrecio.getCntmin() != null)
            ? bvpPrecio.getCntmin().toString() : null;
        codpro = (bvpPrecio.getCodpro() != null)
            ? bvpPrecio.getCodpro().toString() : null;
        despro = (bvpPrecio.getDespro() != null)
            ? bvpPrecio.getDespro().toString() : null;
        estilo = (bvpPrecio.getEstilo() != null)
            ? bvpPrecio.getEstilo().toString() : null;
        fuelle = (bvpPrecio.getFuelle() != null)
            ? bvpPrecio.getFuelle().toString() : null;
        hijpro = (bvpPrecio.getHijpro() != null)
            ? bvpPrecio.getHijpro().toString() : null;
        kitpro = (bvpPrecio.getKitpro() != null)
            ? bvpPrecio.getKitpro().toString() : null;
        largo = (bvpPrecio.getLargo() != null)
            ? bvpPrecio.getLargo().toString() : null;
        larsac = (bvpPrecio.getLarsac() != null)
            ? bvpPrecio.getLarsac().toString() : null;
        moneda = (bvpPrecio.getMoneda() != null)
            ? bvpPrecio.getMoneda().toString() : null;
        numcol = (bvpPrecio.getNumcol() != null)
            ? bvpPrecio.getNumcol().toString() : null;
        refere = (bvpPrecio.getRefere() != null)
            ? bvpPrecio.getRefere().toString() : null;
        tippro = (bvpPrecio.getTippro() != null)
            ? bvpPrecio.getTippro().toString() : null;
        undmed = (bvpPrecio.getUndmed() != null)
            ? bvpPrecio.getUndmed().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codCli = bvpPrecio.getId().getCodCli().toString();
        codpla = bvpPrecio.getId().getCodpla().toString();
        codScc = bvpPrecio.getId().getCodScc().toString();
        precio = bvpPrecio.getId().getPrecio().toString();
        alto = (bvpPrecio.getAlto() != null) ? bvpPrecio.getAlto().toString()
                                             : null;
        ancho = (bvpPrecio.getAncho() != null)
            ? bvpPrecio.getAncho().toString() : null;
        ancsac = (bvpPrecio.getAncsac() != null)
            ? bvpPrecio.getAncsac().toString() : null;
        clave = (bvpPrecio.getClave() != null)
            ? bvpPrecio.getClave().toString() : null;
        cntmax = (bvpPrecio.getCntmax() != null)
            ? bvpPrecio.getCntmax().toString() : null;
        cntmin = (bvpPrecio.getCntmin() != null)
            ? bvpPrecio.getCntmin().toString() : null;
        codpro = (bvpPrecio.getCodpro() != null)
            ? bvpPrecio.getCodpro().toString() : null;
        despro = (bvpPrecio.getDespro() != null)
            ? bvpPrecio.getDespro().toString() : null;
        estilo = (bvpPrecio.getEstilo() != null)
            ? bvpPrecio.getEstilo().toString() : null;
        fuelle = (bvpPrecio.getFuelle() != null)
            ? bvpPrecio.getFuelle().toString() : null;
        hijpro = (bvpPrecio.getHijpro() != null)
            ? bvpPrecio.getHijpro().toString() : null;
        kitpro = (bvpPrecio.getKitpro() != null)
            ? bvpPrecio.getKitpro().toString() : null;
        largo = (bvpPrecio.getLargo() != null)
            ? bvpPrecio.getLargo().toString() : null;
        larsac = (bvpPrecio.getLarsac() != null)
            ? bvpPrecio.getLarsac().toString() : null;
        moneda = (bvpPrecio.getMoneda() != null)
            ? bvpPrecio.getMoneda().toString() : null;
        numcol = (bvpPrecio.getNumcol() != null)
            ? bvpPrecio.getNumcol().toString() : null;
        refere = (bvpPrecio.getRefere() != null)
            ? bvpPrecio.getRefere().toString() : null;
        tippro = (bvpPrecio.getTippro() != null)
            ? bvpPrecio.getTippro().toString() : null;
        undmed = (bvpPrecio.getUndmed() != null)
            ? bvpPrecio.getUndmed().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setBvpPrecio(BvpPrecio bvpPrecio) {
        this.bvpPrecio = bvpPrecio;
    }

    public BvpPrecioView getBvpPrecioView() {
        return bvpPrecioView;
    }

    public void setBvpPrecioView(BvpPrecioView bvpPrecioView) {
        this.bvpPrecioView = bvpPrecioView;
    }

    public String getAlto() {
        return alto;
    }

    public void setAlto(String alto) {
        this.alto = alto;
    }

    public String getAncho() {
        return ancho;
    }

    public void setAncho(String ancho) {
        this.ancho = ancho;
    }

    public String getAncsac() {
        return ancsac;
    }

    public void setAncsac(String ancsac) {
        this.ancsac = ancsac;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getCntmax() {
        return cntmax;
    }

    public void setCntmax(String cntmax) {
        this.cntmax = cntmax;
    }

    public String getCntmin() {
        return cntmin;
    }

    public void setCntmin(String cntmin) {
        this.cntmin = cntmin;
    }

    public String getCodpro() {
        return codpro;
    }

    public void setCodpro(String codpro) {
        this.codpro = codpro;
    }

    public String getDespro() {
        return despro;
    }

    public void setDespro(String despro) {
        this.despro = despro;
    }

    public String getEstilo() {
        return estilo;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public String getFuelle() {
        return fuelle;
    }

    public void setFuelle(String fuelle) {
        this.fuelle = fuelle;
    }

    public String getHijpro() {
        return hijpro;
    }

    public void setHijpro(String hijpro) {
        this.hijpro = hijpro;
    }

    public String getKitpro() {
        return kitpro;
    }

    public void setKitpro(String kitpro) {
        this.kitpro = kitpro;
    }

    public String getLargo() {
        return largo;
    }

    public void setLargo(String largo) {
        this.largo = largo;
    }

    public String getLarsac() {
        return larsac;
    }

    public void setLarsac(String larsac) {
        this.larsac = larsac;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getNumcol() {
        return numcol;
    }

    public void setNumcol(String numcol) {
        this.numcol = numcol;
    }

    public String getRefere() {
        return refere;
    }

    public void setRefere(String refere) {
        this.refere = refere;
    }

    public String getTippro() {
        return tippro;
    }

    public void setTippro(String tippro) {
        this.tippro = tippro;
    }

    public String getUndmed() {
        return undmed;
    }

    public void setUndmed(String undmed) {
        this.undmed = undmed;
    }

    public String getCodCli() {
        return codCli;
    }

    public void setCodCli(String codCli) {
        this.codCli = codCli;
    }

    public String getCodpla() {
        return codpla;
    }

    public void setCodpla(String codpla) {
        this.codpla = codpla;
    }

    public String getCodScc() {
        return codScc;
    }

    public void setCodScc(String codScc) {
        this.codScc = codScc;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
