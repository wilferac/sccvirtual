package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.BvpDirDspView;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class BvpDirDspDTO {
    private String codcli;
    private String coddir;
    private String codpla;
    private String desdir;
    private String numfax;
    private String desprv;
    private boolean rowSelected = false;
    private BvpDirDspView bvpDirDspView;
    private BvpDirDsp bvpDirDsp;

    public BvpDirDsp getBvpDirDsp() {
        return bvpDirDsp;
    }

    public String listener_update(ActionEvent e) {
        try {
            bvpDirDspView.action_modifyWitDTO(((codcli == null) ||
                codcli.equals("")) ? null : new String(codcli),
                ((coddir == null) || coddir.equals("")) ? null
                                                        : new String(coddir),
                ((codpla == null) || codpla.equals("")) ? null
                                                        : new String(codpla),
                ((desdir == null) || desdir.equals("")) ? null
                                                        : new String(desdir),
                ((numfax == null) || numfax.equals("")) ? null
                                                        : new String(numfax),
                ((desprv == null) || desprv.equals("")) ? null
                                                        : new String(desprv));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codcli = bvpDirDsp.getId().getCodcli().toString();
        coddir = bvpDirDsp.getId().getCoddir().toString();
        codpla = bvpDirDsp.getId().getCodpla().toString();
        desdir = bvpDirDsp.getId().getDesdir().toString();
        numfax = bvpDirDsp.getId().getNumfax().toString();
        desprv = bvpDirDsp.getId().getDesprv().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codcli = bvpDirDsp.getId().getCodcli().toString();
        coddir = bvpDirDsp.getId().getCoddir().toString();
        codpla = bvpDirDsp.getId().getCodpla().toString();
        desdir = bvpDirDsp.getId().getDesdir().toString();
        numfax = bvpDirDsp.getId().getNumfax().toString();
        desprv = bvpDirDsp.getId().getDesprv().toString();
        rowSelected = !rowSelected;
    }

    public void setBvpDirDsp(BvpDirDsp bvpDirDsp) {
        this.bvpDirDsp = bvpDirDsp;
    }

    public BvpDirDspView getBvpDirDspView() {
        return bvpDirDspView;
    }

    public void setBvpDirDspView(BvpDirDspView bvpDirDspView) {
        this.bvpDirDspView = bvpDirDspView;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getCoddir() {
        return coddir;
    }

    public void setCoddir(String coddir) {
        this.coddir = coddir;
    }

    public String getCodpla() {
        return codpla;
    }

    public void setCodpla(String codpla) {
        this.codpla = codpla;
    }

    public String getDesdir() {
        return desdir;
    }

    public void setDesdir(String desdir) {
        this.desdir = desdir;
    }

    public String getNumfax() {
        return numfax;
    }

    public void setNumfax(String numfax) {
        this.numfax = numfax;
    }

    public String getDesprv() {
        return desprv;
    }

    public void setDesprv(String desprv) {
        this.desprv = desprv;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
