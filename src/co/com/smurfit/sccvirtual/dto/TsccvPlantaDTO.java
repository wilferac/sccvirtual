package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvPlantaView;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;


import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvPlantaDTO {
    private String despla;
    private String direle;
    private String planta;
    private String idApli_TsccvAplicaciones;
    private String idTipr_TsccvTipoProducto;
    private String codpla;
    private boolean rowSelected = false;
    private TsccvPlantaView tsccvPlantaView;
    private TsccvPlanta tsccvPlanta;

    public TsccvPlanta getTsccvPlanta() {
        return tsccvPlanta;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvPlantaView.action_modifyWitDTO(((codpla == null) ||
                codpla.equals("")) ? null : new String(codpla),
                ((despla == null) || despla.equals("")) ? null
                                                        : new String(despla),
                ((direle == null) || direle.equals("")) ? null
                                                        : new String(direle),
                ((planta == null) || planta.equals("")) ? null
                                                        : new String(planta),
                ((idApli_TsccvAplicaciones == null) ||
                idApli_TsccvAplicaciones.equals("")) ? null
                                                     : new Long(
                    idApli_TsccvAplicaciones),
                ((idTipr_TsccvTipoProducto == null) ||
                idTipr_TsccvTipoProducto.equals("")) ? null
                                                     : new Long(
                    idTipr_TsccvTipoProducto));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codpla = tsccvPlanta.getCodpla().toString();
        despla = (tsccvPlanta.getDespla() != null)
            ? tsccvPlanta.getDespla().toString() : null;
        direle = (tsccvPlanta.getDirele() != null)
            ? tsccvPlanta.getDirele().toString() : null;
        planta = (tsccvPlanta.getPlanta() != null)
            ? tsccvPlanta.getPlanta().toString() : null;
        idApli_TsccvAplicaciones = (tsccvPlanta.getTsccvAplicaciones()
                                               .getIdApli() != null)
            ? tsccvPlanta.getTsccvAplicaciones().getIdApli().toString() : null;
        idTipr_TsccvTipoProducto = (tsccvPlanta.getTsccvTipoProducto()
                                               .getIdTipr() != null)
            ? tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codpla = tsccvPlanta.getCodpla().toString();
        despla = (tsccvPlanta.getDespla() != null)
            ? tsccvPlanta.getDespla().toString() : null;
        direle = (tsccvPlanta.getDirele() != null)
            ? tsccvPlanta.getDirele().toString() : null;
        planta = (tsccvPlanta.getPlanta() != null)
            ? tsccvPlanta.getPlanta().toString() : null;
        idApli_TsccvAplicaciones = (tsccvPlanta.getTsccvAplicaciones()
                                               .getIdApli() != null)
            ? tsccvPlanta.getTsccvAplicaciones().getIdApli().toString() : null;
        idTipr_TsccvTipoProducto = (tsccvPlanta.getTsccvTipoProducto()
                                               .getIdTipr() != null)
            ? tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvPlanta(TsccvPlanta tsccvPlanta) {
        this.tsccvPlanta = tsccvPlanta;
    }

    public TsccvPlantaView getTsccvPlantaView() {
        return tsccvPlantaView;
    }

    public void setTsccvPlantaView(TsccvPlantaView tsccvPlantaView) {
        this.tsccvPlantaView = tsccvPlantaView;
    }

    public String getDespla() {
        return despla;
    }

    public void setDespla(String despla) {
        this.despla = despla;
    }

    public String getDirele() {
        return direle;
    }

    public void setDirele(String direle) {
        this.direle = direle;
    }

    public String getPlanta() {
        return planta;
    }

    public void setPlanta(String planta) {
        this.planta = planta;
    }

    public String getIdApli_TsccvAplicaciones() {
        return idApli_TsccvAplicaciones;
    }

    public void setIdApli_TsccvAplicaciones(String idApli_TsccvAplicaciones) {
        this.idApli_TsccvAplicaciones = idApli_TsccvAplicaciones;
    }

    public String getIdTipr_TsccvTipoProducto() {
        return idTipr_TsccvTipoProducto;
    }

    public void setIdTipr_TsccvTipoProducto(String idTipr_TsccvTipoProducto) {
        this.idTipr_TsccvTipoProducto = idTipr_TsccvTipoProducto;
    }

    public String getCodpla() {
        return codpla;
    }

    public void setCodpla(String codpla) {
        this.codpla = codpla;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
