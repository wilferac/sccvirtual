package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TbmBaseGruposUsuariosView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TbmBaseGruposUsuariosDTO {
    private String activo;
    private String descripcion;
    private String idGrup;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TbmBaseGruposUsuariosView tbmBaseGruposUsuariosView;
    private TbmBaseGruposUsuarios tbmBaseGruposUsuarios;

    public TbmBaseGruposUsuarios getTbmBaseGruposUsuarios() {
        return tbmBaseGruposUsuarios;
    }

    public String listener_update(ActionEvent e) {
        try {
            tbmBaseGruposUsuariosView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((descripcion == null) || descripcion.equals("")) ? null
                                                                  : new String(
                    descripcion),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idGrup == null) || idGrup.equals("")) ? null : new Long(
                    idGrup));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idGrup = tbmBaseGruposUsuarios.getIdGrup().toString();
        activo = (tbmBaseGruposUsuarios.getActivo() != null)
            ? tbmBaseGruposUsuarios.getActivo().toString() : null;
        descripcion = (tbmBaseGruposUsuarios.getDescripcion() != null)
            ? tbmBaseGruposUsuarios.getDescripcion().toString() : null;
        fechaCreacion = tbmBaseGruposUsuarios.getFechaCreacion();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idGrup = tbmBaseGruposUsuarios.getIdGrup().toString();
        activo = (tbmBaseGruposUsuarios.getActivo() != null)
            ? tbmBaseGruposUsuarios.getActivo().toString() : null;
        descripcion = (tbmBaseGruposUsuarios.getDescripcion() != null)
            ? tbmBaseGruposUsuarios.getDescripcion().toString() : null;
        fechaCreacion = tbmBaseGruposUsuarios.getFechaCreacion();
        rowSelected = !rowSelected;
    }

    public void setTbmBaseGruposUsuarios(
        TbmBaseGruposUsuarios tbmBaseGruposUsuarios) {
        this.tbmBaseGruposUsuarios = tbmBaseGruposUsuarios;
    }

    public TbmBaseGruposUsuariosView getTbmBaseGruposUsuariosView() {
        return tbmBaseGruposUsuariosView;
    }

    public void setTbmBaseGruposUsuariosView(
        TbmBaseGruposUsuariosView tbmBaseGruposUsuariosView) {
        this.tbmBaseGruposUsuariosView = tbmBaseGruposUsuariosView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdGrup() {
        return idGrup;
    }

    public void setIdGrup(String idGrup) {
        this.idGrup = idGrup;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
