package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TbmBaseSubopcionesView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TbmBaseSubopcionesDTO {
    private String activo;
    private String descripcion;
    private String nombre;
    private String url;
    private String idSuop;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TbmBaseSubopcionesView tbmBaseSubopcionesView;
    private TbmBaseSubopciones tbmBaseSubopciones;

    public TbmBaseSubopciones getTbmBaseSubopciones() {
        return tbmBaseSubopciones;
    }

    public String listener_update(ActionEvent e) {
        try {
            tbmBaseSubopcionesView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((descripcion == null) || descripcion.equals("")) ? null
                                                                  : new String(
                    descripcion),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idSuop == null) || idSuop.equals("")) ? null : new Long(
                    idSuop),
                ((nombre == null) || nombre.equals("")) ? null
                                                        : new String(nombre),
                ((url == null) || url.equals("")) ? null : new String(url));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idSuop = tbmBaseSubopciones.getIdSuop().toString();
        activo = (tbmBaseSubopciones.getActivo() != null)
            ? tbmBaseSubopciones.getActivo().toString() : null;
        descripcion = (tbmBaseSubopciones.getDescripcion() != null)
            ? tbmBaseSubopciones.getDescripcion().toString() : null;
        fechaCreacion = tbmBaseSubopciones.getFechaCreacion();
        nombre = (tbmBaseSubopciones.getNombre() != null)
            ? tbmBaseSubopciones.getNombre().toString() : null;
        url = (tbmBaseSubopciones.getUrl() != null)
            ? tbmBaseSubopciones.getUrl().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idSuop = tbmBaseSubopciones.getIdSuop().toString();
        activo = (tbmBaseSubopciones.getActivo() != null)
            ? tbmBaseSubopciones.getActivo().toString() : null;
        descripcion = (tbmBaseSubopciones.getDescripcion() != null)
            ? tbmBaseSubopciones.getDescripcion().toString() : null;
        fechaCreacion = tbmBaseSubopciones.getFechaCreacion();
        nombre = (tbmBaseSubopciones.getNombre() != null)
            ? tbmBaseSubopciones.getNombre().toString() : null;
        url = (tbmBaseSubopciones.getUrl() != null)
            ? tbmBaseSubopciones.getUrl().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTbmBaseSubopciones(TbmBaseSubopciones tbmBaseSubopciones) {
        this.tbmBaseSubopciones = tbmBaseSubopciones;
    }

    public TbmBaseSubopcionesView getTbmBaseSubopcionesView() {
        return tbmBaseSubopcionesView;
    }

    public void setTbmBaseSubopcionesView(
        TbmBaseSubopcionesView tbmBaseSubopcionesView) {
        this.tbmBaseSubopcionesView = tbmBaseSubopcionesView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdSuop() {
        return idSuop;
    }

    public void setIdSuop(String idSuop) {
        this.idSuop = idSuop;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
