package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.SapDetPedView;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class SapDetPedDTO {
    private String numped;
    private String itmped;
    private String subitm;
    private String codcli;
    private String despro;
    private String cntped;
    private String undmed;
    private String fecsol;
    private String fecdsp;
    private String codpro;
    private String estado;
    private String ordcom;
    private String cntdsp;
    private String cntkgs;
    private String arollos;
    private String ahojas;
    private String lhojas;
    private String arollo;
    private boolean rowSelected = false;
    private SapDetPedView sapDetPedView;
    private SapDetPed sapDetPed;

    public SapDetPed getSapDetPed() {
        return sapDetPed;
    }

    public String listener_update(ActionEvent e) {
        try {
            sapDetPedView.action_modifyWitDTO(((numped == null) ||
                numped.equals("")) ? null : new String(numped),
                ((itmped == null) || itmped.equals("")) ? null
                                                        : new String(itmped),
                ((subitm == null) || subitm.equals("")) ? null
                                                        : new String(subitm),
                ((codcli == null) || codcli.equals("")) ? null
                                                        : new String(codcli),
                ((despro == null) || despro.equals("")) ? null
                                                        : new String(despro),
                ((cntped == null) || cntped.equals("")) ? null
                                                        : new String(cntped),
                ((undmed == null) || undmed.equals("")) ? null
                                                        : new String(undmed),
                ((fecsol == null) || fecsol.equals("")) ? null
                                                        : new String(fecsol),
                ((fecdsp == null) || fecdsp.equals("")) ? null
                                                        : new String(fecdsp),
                ((codpro == null) || codpro.equals("")) ? null
                                                        : new String(codpro),
                ((estado == null) || estado.equals("")) ? null
                                                        : new String(estado),
                ((ordcom == null) || ordcom.equals("")) ? null
                                                        : new String(ordcom),
                ((cntdsp == null) || cntdsp.equals("")) ? null
                                                        : new String(cntdsp),
                ((cntkgs == null) || cntkgs.equals("")) ? null
                                                        : new String(cntkgs),
                ((arollos == null) || arollos.equals("")) ? null
                                                          : new String(arollos),
                ((ahojas == null) || ahojas.equals("")) ? null
                                                        : new String(ahojas),
                ((lhojas == null) || lhojas.equals("")) ? null
                                                        : new String(lhojas),
                ((arollo == null) || arollo.equals("")) ? null
                                                        : new String(arollo));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        numped = sapDetPed.getId().getNumped().toString();
        itmped = sapDetPed.getId().getItmped().toString();
        subitm = sapDetPed.getId().getSubitm().toString();
        codcli = sapDetPed.getId().getCodcli().toString();
        despro = sapDetPed.getId().getDespro().toString();
        cntped = sapDetPed.getId().getCntped().toString();
        undmed = sapDetPed.getId().getUndmed().toString();
        fecsol = sapDetPed.getId().getFecsol().toString();
        fecdsp = sapDetPed.getId().getFecdsp().toString();
        codpro = sapDetPed.getId().getCodpro().toString();
        estado = sapDetPed.getId().getEstado().toString();
        ordcom = sapDetPed.getId().getOrdcom().toString();
        cntdsp = sapDetPed.getId().getCntdsp().toString();
        cntkgs = sapDetPed.getId().getCntkgs().toString();
        arollos = sapDetPed.getId().getArollos().toString();
        ahojas = sapDetPed.getId().getAhojas().toString();
        lhojas = sapDetPed.getId().getLhojas().toString();
        arollo = sapDetPed.getId().getArollo().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        numped = sapDetPed.getId().getNumped().toString();
        itmped = sapDetPed.getId().getItmped().toString();
        subitm = sapDetPed.getId().getSubitm().toString();
        codcli = sapDetPed.getId().getCodcli().toString();
        despro = sapDetPed.getId().getDespro().toString();
        cntped = sapDetPed.getId().getCntped().toString();
        undmed = sapDetPed.getId().getUndmed().toString();
        fecsol = sapDetPed.getId().getFecsol().toString();
        fecdsp = sapDetPed.getId().getFecdsp().toString();
        codpro = sapDetPed.getId().getCodpro().toString();
        estado = sapDetPed.getId().getEstado().toString();
        ordcom = sapDetPed.getId().getOrdcom().toString();
        cntdsp = sapDetPed.getId().getCntdsp().toString();
        cntkgs = sapDetPed.getId().getCntkgs().toString();
        arollos = sapDetPed.getId().getArollos().toString();
        ahojas = sapDetPed.getId().getAhojas().toString();
        lhojas = sapDetPed.getId().getLhojas().toString();
        arollo = sapDetPed.getId().getArollo().toString();
        rowSelected = !rowSelected;
    }

    public void setSapDetPed(SapDetPed sapDetPed) {
        this.sapDetPed = sapDetPed;
    }

    public SapDetPedView getSapDetPedView() {
        return sapDetPedView;
    }

    public void setSapDetPedView(SapDetPedView sapDetPedView) {
        this.sapDetPedView = sapDetPedView;
    }

    public String getNumped() {
        return numped;
    }

    public void setNumped(String numped) {
        this.numped = numped;
    }

    public String getItmped() {
        return itmped;
    }

    public void setItmped(String itmped) {
        this.itmped = itmped;
    }

    public String getSubitm() {
        return subitm;
    }

    public void setSubitm(String subitm) {
        this.subitm = subitm;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getDespro() {
        return despro;
    }

    public void setDespro(String despro) {
        this.despro = despro;
    }

    public String getCntped() {
        return cntped;
    }

    public void setCntped(String cntped) {
        this.cntped = cntped;
    }

    public String getUndmed() {
        return undmed;
    }

    public void setUndmed(String undmed) {
        this.undmed = undmed;
    }

    public String getFecsol() {
        return fecsol;
    }

    public void setFecsol(String fecsol) {
        this.fecsol = fecsol;
    }

    public String getFecdsp() {
        return fecdsp;
    }

    public void setFecdsp(String fecdsp) {
        this.fecdsp = fecdsp;
    }

    public String getCodpro() {
        return codpro;
    }

    public void setCodpro(String codpro) {
        this.codpro = codpro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getOrdcom() {
        return ordcom;
    }

    public void setOrdcom(String ordcom) {
        this.ordcom = ordcom;
    }

    public String getCntdsp() {
        return cntdsp;
    }

    public void setCntdsp(String cntdsp) {
        this.cntdsp = cntdsp;
    }

    public String getCntkgs() {
        return cntkgs;
    }

    public void setCntkgs(String cntkgs) {
        this.cntkgs = cntkgs;
    }

    public String getArollos() {
        return arollos;
    }

    public void setArollos(String arollos) {
        this.arollos = arollos;
    }

    public String getAhojas() {
        return ahojas;
    }

    public void setAhojas(String ahojas) {
        this.ahojas = ahojas;
    }

    public String getLhojas() {
        return lhojas;
    }

    public void setLhojas(String lhojas) {
        this.lhojas = lhojas;
    }

    public String getArollo() {
        return arollo;
    }

    public void setArollo(String arollo) {
        this.arollo = arollo;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
