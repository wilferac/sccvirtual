package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvPdfsProductosView;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvPdfsProductosDTO {
    private String codpla_TsccvPlanta;
    private String codScc;
    private String codpla;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TsccvPdfsProductosView tsccvPdfsProductosView;
    private TsccvPdfsProductos tsccvPdfsProductos;

    public TsccvPdfsProductos getTsccvPdfsProductos() {
        return tsccvPdfsProductos;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvPdfsProductosView.action_modifyWitDTO(((codScc == null) ||
                codScc.equals("")) ? null : new String(codScc),
                ((codpla == null) || codpla.equals("")) ? null
                                                        : new String(codpla),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((codpla_TsccvPlanta == null) || codpla_TsccvPlanta.equals(""))
                ? null : new String(codpla_TsccvPlanta));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codScc = tsccvPdfsProductos.getId().getCodScc().toString();
        codpla = tsccvPdfsProductos.getId().getCodpla().toString();
        fechaCreacion = tsccvPdfsProductos.getFechaCreacion();
        codpla_TsccvPlanta = (tsccvPdfsProductos.getTsccvPlanta().getCodpla() != null)
            ? tsccvPdfsProductos.getTsccvPlanta().getCodpla().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codScc = tsccvPdfsProductos.getId().getCodScc().toString();
        codpla = tsccvPdfsProductos.getId().getCodpla().toString();
        fechaCreacion = tsccvPdfsProductos.getFechaCreacion();
        codpla_TsccvPlanta = (tsccvPdfsProductos.getTsccvPlanta().getCodpla() != null)
            ? tsccvPdfsProductos.getTsccvPlanta().getCodpla().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvPdfsProductos(TsccvPdfsProductos tsccvPdfsProductos) {
        this.tsccvPdfsProductos = tsccvPdfsProductos;
    }

    public TsccvPdfsProductosView getTsccvPdfsProductosView() {
        return tsccvPdfsProductosView;
    }

    public void setTsccvPdfsProductosView(
        TsccvPdfsProductosView tsccvPdfsProductosView) {
        this.tsccvPdfsProductosView = tsccvPdfsProductosView;
    }

    public String getCodpla_TsccvPlanta() {
        return codpla_TsccvPlanta;
    }

    public void setCodpla_TsccvPlanta(String codpla_TsccvPlanta) {
        this.codpla_TsccvPlanta = codpla_TsccvPlanta;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getCodScc() {
        return codScc;
    }

    public void setCodScc(String codScc) {
        this.codScc = codScc;
    }

    public String getCodpla() {
        return codpla;
    }

    public void setCodpla(String codpla) {
        this.codpla = codpla;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
