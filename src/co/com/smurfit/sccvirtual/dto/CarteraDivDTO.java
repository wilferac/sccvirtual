package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.CarteraDivView;
import co.com.smurfit.sccvirtual.entidades.CarteraDiv;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class CarteraDivDTO {
    private String coddiv;
    private String desdiv;
    private boolean rowSelected = false;
    private CarteraDivView carteraDivView;
    private CarteraDiv carteraDiv;

    public CarteraDiv getCarteraDiv() {
        return carteraDiv;
    }

    public String listener_update(ActionEvent e) {
        try {
            carteraDivView.action_modifyWitDTO(((coddiv == null) ||
                coddiv.equals("")) ? null : new String(coddiv),
                ((desdiv == null) || desdiv.equals("")) ? null
                                                        : new String(desdiv));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        coddiv = carteraDiv.getId().getCoddiv().toString();
        desdiv = carteraDiv.getId().getDesdiv().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        coddiv = carteraDiv.getId().getCoddiv().toString();
        desdiv = carteraDiv.getId().getDesdiv().toString();
        rowSelected = !rowSelected;
    }

    public void setCarteraDiv(CarteraDiv carteraDiv) {
        this.carteraDiv = carteraDiv;
    }

    public CarteraDivView getCarteraDivView() {
        return carteraDivView;
    }

    public void setCarteraDivView(CarteraDivView carteraDivView) {
        this.carteraDivView = carteraDivView;
    }

    public String getCoddiv() {
        return coddiv;
    }

    public void setCoddiv(String coddiv) {
        this.coddiv = coddiv;
    }

    public String getDesdiv() {
        return desdiv;
    }

    public void setDesdiv(String desdiv) {
        this.desdiv = desdiv;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
