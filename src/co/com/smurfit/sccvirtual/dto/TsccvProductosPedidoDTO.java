package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvProductosPedidoView;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvProductosPedidoDTO {
    private String activo;
    private String cantidadTotal;
    private String codCli;
    private String codScc;
    private String codpla;
    private String despro;
    private String diamtr;
    private String eje;
    private String moneda;
    private String ordenCompra;
    private String precio;
    private String undmed;
    private String idPedi_TsccvPedidos;
    private String idPrpe;
    private Date fechaCreacion;
    private Date fechaPrimeraEntrega;
    private boolean rowSelected = false;
    private TsccvProductosPedidoView tsccvProductosPedidoView;
    private TsccvProductosPedido tsccvProductosPedido;

    public TsccvProductosPedido getTsccvProductosPedido() {
        return tsccvProductosPedido;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvProductosPedidoView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((cantidadTotal == null) || cantidadTotal.equals("")) ? null
                                                                      : new Integer(
                    cantidadTotal),
                ((codCli == null) || codCli.equals("")) ? null
                                                        : new String(codCli),
                ((codScc == null) || codScc.equals("")) ? null
                                                        : new String(codScc),
                ((codpla == null) || codpla.equals("")) ? null
                                                        : new String(codpla),
                ((despro == null) || despro.equals("")) ? null
                                                        : new String(despro),
                ((diamtr == null) || diamtr.equals("")) ? null
                                                        : new String(diamtr),
                ((eje == null) || eje.equals("")) ? null : new String(eje),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((fechaPrimeraEntrega == null) ||
                fechaPrimeraEntrega.equals("")) ? null : fechaPrimeraEntrega,
                ((idPrpe == null) || idPrpe.equals("")) ? null
                                                        : new Integer(idPrpe),
                ((moneda == null) || moneda.equals("")) ? null
                                                        : new String(moneda),
                ((ordenCompra == null) || ordenCompra.equals("")) ? null
                                                                  : new String(
                    ordenCompra),
                ((precio == null) || precio.equals("")) ? null
                                                        : new String(precio),
                ((undmed == null) || undmed.equals("")) ? null
                                                        : new String(undmed),
                ((idPedi_TsccvPedidos == null) ||
                idPedi_TsccvPedidos.equals("")) ? null
                                                : new Integer(
                    idPedi_TsccvPedidos));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idPrpe = tsccvProductosPedido.getIdPrpe().toString();
        activo = (tsccvProductosPedido.getActivo() != null)
            ? tsccvProductosPedido.getActivo().toString() : null;
        cantidadTotal = (tsccvProductosPedido.getCantidadTotal() != null)
            ? tsccvProductosPedido.getCantidadTotal().toString() : null;
        codCli = (tsccvProductosPedido.getCodCli() != null)
            ? tsccvProductosPedido.getCodCli().toString() : null;
        codScc = (tsccvProductosPedido.getCodScc() != null)
            ? tsccvProductosPedido.getCodScc().toString() : null;
        codpla = (tsccvProductosPedido.getCodpla() != null)
            ? tsccvProductosPedido.getCodpla().toString() : null;
        despro = (tsccvProductosPedido.getDespro() != null)
            ? tsccvProductosPedido.getDespro().toString() : null;
        diamtr = (tsccvProductosPedido.getDiamtr() != null)
            ? tsccvProductosPedido.getDiamtr().toString() : null;
        eje = (tsccvProductosPedido.getEje() != null)
            ? tsccvProductosPedido.getEje().toString() : null;
        fechaCreacion = tsccvProductosPedido.getFechaCreacion();
        fechaPrimeraEntrega = tsccvProductosPedido.getFechaPrimeraEntrega();
        moneda = (tsccvProductosPedido.getMoneda() != null)
            ? tsccvProductosPedido.getMoneda().toString() : null;
        ordenCompra = (tsccvProductosPedido.getOrdenCompra() != null)
            ? tsccvProductosPedido.getOrdenCompra().toString() : null;
        precio = (tsccvProductosPedido.getPrecio() != null)
            ? tsccvProductosPedido.getPrecio().toString() : null;
        undmed = (tsccvProductosPedido.getUndmed() != null)
            ? tsccvProductosPedido.getUndmed().toString() : null;
        idPedi_TsccvPedidos = (tsccvProductosPedido.getTsccvPedidos().getIdPedi() != null)
            ? tsccvProductosPedido.getTsccvPedidos().getIdPedi().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idPrpe = tsccvProductosPedido.getIdPrpe().toString();
        activo = (tsccvProductosPedido.getActivo() != null)
            ? tsccvProductosPedido.getActivo().toString() : null;
        cantidadTotal = (tsccvProductosPedido.getCantidadTotal() != null)
            ? tsccvProductosPedido.getCantidadTotal().toString() : null;
        codCli = (tsccvProductosPedido.getCodCli() != null)
            ? tsccvProductosPedido.getCodCli().toString() : null;
        codScc = (tsccvProductosPedido.getCodScc() != null)
            ? tsccvProductosPedido.getCodScc().toString() : null;
        codpla = (tsccvProductosPedido.getCodpla() != null)
            ? tsccvProductosPedido.getCodpla().toString() : null;
        despro = (tsccvProductosPedido.getDespro() != null)
            ? tsccvProductosPedido.getDespro().toString() : null;
        diamtr = (tsccvProductosPedido.getDiamtr() != null)
            ? tsccvProductosPedido.getDiamtr().toString() : null;
        eje = (tsccvProductosPedido.getEje() != null)
            ? tsccvProductosPedido.getEje().toString() : null;
        fechaCreacion = tsccvProductosPedido.getFechaCreacion();
        fechaPrimeraEntrega = tsccvProductosPedido.getFechaPrimeraEntrega();
        moneda = (tsccvProductosPedido.getMoneda() != null)
            ? tsccvProductosPedido.getMoneda().toString() : null;
        ordenCompra = (tsccvProductosPedido.getOrdenCompra() != null)
            ? tsccvProductosPedido.getOrdenCompra().toString() : null;
        precio = (tsccvProductosPedido.getPrecio() != null)
            ? tsccvProductosPedido.getPrecio().toString() : null;
        undmed = (tsccvProductosPedido.getUndmed() != null)
            ? tsccvProductosPedido.getUndmed().toString() : null;
        idPedi_TsccvPedidos = (tsccvProductosPedido.getTsccvPedidos().getIdPedi() != null)
            ? tsccvProductosPedido.getTsccvPedidos().getIdPedi().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvProductosPedido(
        TsccvProductosPedido tsccvProductosPedido) {
        this.tsccvProductosPedido = tsccvProductosPedido;
    }

    public TsccvProductosPedidoView getTsccvProductosPedidoView() {
        return tsccvProductosPedidoView;
    }

    public void setTsccvProductosPedidoView(
        TsccvProductosPedidoView tsccvProductosPedidoView) {
        this.tsccvProductosPedidoView = tsccvProductosPedidoView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getCantidadTotal() {
        return cantidadTotal;
    }

    public void setCantidadTotal(String cantidadTotal) {
        this.cantidadTotal = cantidadTotal;
    }

    public String getCodCli() {
        return codCli;
    }

    public void setCodCli(String codCli) {
        this.codCli = codCli;
    }

    public String getCodScc() {
        return codScc;
    }

    public void setCodScc(String codScc) {
        this.codScc = codScc;
    }

    public String getCodpla() {
        return codpla;
    }

    public void setCodpla(String codpla) {
        this.codpla = codpla;
    }

    public String getDespro() {
        return despro;
    }

    public void setDespro(String despro) {
        this.despro = despro;
    }

    public String getDiamtr() {
        return diamtr;
    }

    public void setDiamtr(String diamtr) {
        this.diamtr = diamtr;
    }

    public String getEje() {
        return eje;
    }

    public void setEje(String eje) {
        this.eje = eje;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(String ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getUndmed() {
        return undmed;
    }

    public void setUndmed(String undmed) {
        this.undmed = undmed;
    }

    public String getIdPedi_TsccvPedidos() {
        return idPedi_TsccvPedidos;
    }

    public void setIdPedi_TsccvPedidos(String idPedi_TsccvPedidos) {
        this.idPedi_TsccvPedidos = idPedi_TsccvPedidos;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaPrimeraEntrega() {
        return fechaPrimeraEntrega;
    }

    public void setFechaPrimeraEntrega(Date fechaPrimeraEntrega) {
        this.fechaPrimeraEntrega = fechaPrimeraEntrega;
    }

    public String getIdPrpe() {
        return idPrpe;
    }

    public void setIdPrpe(String idPrpe) {
        this.idPrpe = idPrpe;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
