package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.SapDetDspView;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class SapDetDspDTO {
    private String subdsp;
    private String numdsp;
    private String itmdsp;
    private String codcli;
    private String numped;
    private String itmped;
    private String subitm;
    private String fecdsp;
    private String candsp;
    private String undmed;
    private String emptra;
    private String placa;
    private String nomcon;
    private String dirdsp;
    private String cankgs;
    private boolean rowSelected = false;
    private SapDetDspView sapDetDspView;
    private SapDetDsp sapDetDsp;

    public SapDetDsp getSapDetDsp() {
        return sapDetDsp;
    }

    public String listener_update(ActionEvent e) {
        try {
            sapDetDspView.action_modifyWitDTO(((subdsp == null) ||
                subdsp.equals("")) ? null : new String(subdsp),
                ((numdsp == null) || numdsp.equals("")) ? null
                                                        : new String(numdsp),
                ((itmdsp == null) || itmdsp.equals("")) ? null
                                                        : new String(itmdsp),
                ((codcli == null) || codcli.equals("")) ? null
                                                        : new String(codcli),
                ((numped == null) || numped.equals("")) ? null
                                                        : new String(numped),
                ((itmped == null) || itmped.equals("")) ? null
                                                        : new String(itmped),
                ((subitm == null) || subitm.equals("")) ? null
                                                        : new String(subitm),
                ((fecdsp == null) || fecdsp.equals("")) ? null
                                                        : new String(fecdsp),
                ((candsp == null) || candsp.equals("")) ? null
                                                        : new String(candsp),
                ((undmed == null) || undmed.equals("")) ? null
                                                        : new String(undmed),
                ((emptra == null) || emptra.equals("")) ? null
                                                        : new String(emptra),
                ((placa == null) || placa.equals("")) ? null : new String(placa),
                ((nomcon == null) || nomcon.equals("")) ? null
                                                        : new String(nomcon),
                ((dirdsp == null) || dirdsp.equals("")) ? null
                                                        : new String(dirdsp),
                ((cankgs == null) || cankgs.equals("")) ? null
                                                        : new String(cankgs));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        subdsp = sapDetDsp.getId().getSubdsp().toString();
        numdsp = sapDetDsp.getId().getNumdsp().toString();
        itmdsp = sapDetDsp.getId().getItmdsp().toString();
        codcli = sapDetDsp.getId().getCodcli().toString();
        numped = sapDetDsp.getId().getNumped().toString();
        itmped = sapDetDsp.getId().getItmped().toString();
        subitm = sapDetDsp.getId().getSubitm().toString();
        fecdsp = sapDetDsp.getId().getFecdsp().toString();
        candsp = sapDetDsp.getId().getCandsp().toString();
        undmed = sapDetDsp.getId().getUndmed().toString();
        emptra = sapDetDsp.getId().getEmptra().toString();
        placa = sapDetDsp.getId().getPlaca().toString();
        nomcon = sapDetDsp.getId().getNomcon().toString();
        dirdsp = sapDetDsp.getId().getDirdsp().toString();
        cankgs = sapDetDsp.getId().getCankgs().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        subdsp = sapDetDsp.getId().getSubdsp().toString();
        numdsp = sapDetDsp.getId().getNumdsp().toString();
        itmdsp = sapDetDsp.getId().getItmdsp().toString();
        codcli = sapDetDsp.getId().getCodcli().toString();
        numped = sapDetDsp.getId().getNumped().toString();
        itmped = sapDetDsp.getId().getItmped().toString();
        subitm = sapDetDsp.getId().getSubitm().toString();
        fecdsp = sapDetDsp.getId().getFecdsp().toString();
        candsp = sapDetDsp.getId().getCandsp().toString();
        undmed = sapDetDsp.getId().getUndmed().toString();
        emptra = sapDetDsp.getId().getEmptra().toString();
        placa = sapDetDsp.getId().getPlaca().toString();
        nomcon = sapDetDsp.getId().getNomcon().toString();
        dirdsp = sapDetDsp.getId().getDirdsp().toString();
        cankgs = sapDetDsp.getId().getCankgs().toString();
        rowSelected = !rowSelected;
    }

    public void setSapDetDsp(SapDetDsp sapDetDsp) {
        this.sapDetDsp = sapDetDsp;
    }

    public SapDetDspView getSapDetDspView() {
        return sapDetDspView;
    }

    public void setSapDetDspView(SapDetDspView sapDetDspView) {
        this.sapDetDspView = sapDetDspView;
    }

    public String getSubdsp() {
        return subdsp;
    }

    public void setSubdsp(String subdsp) {
        this.subdsp = subdsp;
    }

    public String getNumdsp() {
        return numdsp;
    }

    public void setNumdsp(String numdsp) {
        this.numdsp = numdsp;
    }

    public String getItmdsp() {
        return itmdsp;
    }

    public void setItmdsp(String itmdsp) {
        this.itmdsp = itmdsp;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getNumped() {
        return numped;
    }

    public void setNumped(String numped) {
        this.numped = numped;
    }

    public String getItmped() {
        return itmped;
    }

    public void setItmped(String itmped) {
        this.itmped = itmped;
    }

    public String getSubitm() {
        return subitm;
    }

    public void setSubitm(String subitm) {
        this.subitm = subitm;
    }

    public String getFecdsp() {
        return fecdsp;
    }

    public void setFecdsp(String fecdsp) {
        this.fecdsp = fecdsp;
    }

    public String getCandsp() {
        return candsp;
    }

    public void setCandsp(String candsp) {
        this.candsp = candsp;
    }

    public String getUndmed() {
        return undmed;
    }

    public void setUndmed(String undmed) {
        this.undmed = undmed;
    }

    public String getEmptra() {
        return emptra;
    }

    public void setEmptra(String emptra) {
        this.emptra = emptra;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getNomcon() {
        return nomcon;
    }

    public void setNomcon(String nomcon) {
        this.nomcon = nomcon;
    }

    public String getDirdsp() {
        return dirdsp;
    }

    public void setDirdsp(String dirdsp) {
        this.dirdsp = dirdsp;
    }

    public String getCankgs() {
        return cankgs;
    }

    public void setCankgs(String cankgs) {
        this.cankgs = cankgs;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
