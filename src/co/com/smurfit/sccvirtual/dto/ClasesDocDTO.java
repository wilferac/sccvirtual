package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.ClasesDocView;
import co.com.smurfit.sccvirtual.entidades.ClasesDoc;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class ClasesDocDTO {
    private String coddoc;
    private String desdoc;
    private boolean rowSelected = false;
    private ClasesDocView clasesDocView;
    private ClasesDoc clasesDoc;

    public ClasesDoc getClasesDoc() {
        return clasesDoc;
    }

    public String listener_update(ActionEvent e) {
        try {
            clasesDocView.action_modifyWitDTO(((coddoc == null) ||
                coddoc.equals("")) ? null : new String(coddoc),
                ((desdoc == null) || desdoc.equals("")) ? null
                                                        : new String(desdoc));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        coddoc = clasesDoc.getId().getCoddoc().toString();
        desdoc = clasesDoc.getId().getDesdoc().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        coddoc = clasesDoc.getId().getCoddoc().toString();
        desdoc = clasesDoc.getId().getDesdoc().toString();
        rowSelected = !rowSelected;
    }

    public void setClasesDoc(ClasesDoc clasesDoc) {
        this.clasesDoc = clasesDoc;
    }

    public ClasesDocView getClasesDocView() {
        return clasesDocView;
    }

    public void setClasesDocView(ClasesDocView clasesDocView) {
        this.clasesDocView = clasesDocView;
    }

    public String getCoddoc() {
        return coddoc;
    }

    public void setCoddoc(String coddoc) {
        this.coddoc = coddoc;
    }

    public String getDesdoc() {
        return desdoc;
    }

    public void setDesdoc(String desdoc) {
        this.desdoc = desdoc;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
