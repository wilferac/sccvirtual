package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvAplicacionesView;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvAplicacionesDTO {
    private String activo;
    private String descripcion;
    private String idApli;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TsccvAplicacionesView tsccvAplicacionesView;
    private TsccvAplicaciones tsccvAplicaciones;

    public TsccvAplicaciones getTsccvAplicaciones() {
        return tsccvAplicaciones;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvAplicacionesView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((descripcion == null) || descripcion.equals("")) ? null
                                                                  : new String(
                    descripcion),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idApli == null) || idApli.equals("")) ? null : new Long(
                    idApli));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idApli = tsccvAplicaciones.getIdApli().toString();
        activo = (tsccvAplicaciones.getActivo() != null)
            ? tsccvAplicaciones.getActivo().toString() : null;
        descripcion = (tsccvAplicaciones.getDescripcion() != null)
            ? tsccvAplicaciones.getDescripcion().toString() : null;
        fechaCreacion = tsccvAplicaciones.getFechaCreacion();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idApli = tsccvAplicaciones.getIdApli().toString();
        activo = (tsccvAplicaciones.getActivo() != null)
            ? tsccvAplicaciones.getActivo().toString() : null;
        descripcion = (tsccvAplicaciones.getDescripcion() != null)
            ? tsccvAplicaciones.getDescripcion().toString() : null;
        fechaCreacion = tsccvAplicaciones.getFechaCreacion();
        rowSelected = !rowSelected;
    }

    public void setTsccvAplicaciones(TsccvAplicaciones tsccvAplicaciones) {
        this.tsccvAplicaciones = tsccvAplicaciones;
    }

    public TsccvAplicacionesView getTsccvAplicacionesView() {
        return tsccvAplicacionesView;
    }

    public void setTsccvAplicacionesView(
        TsccvAplicacionesView tsccvAplicacionesView) {
        this.tsccvAplicacionesView = tsccvAplicacionesView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdApli() {
        return idApli;
    }

    public void setIdApli(String idApli) {
        this.idApli = idApli;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
