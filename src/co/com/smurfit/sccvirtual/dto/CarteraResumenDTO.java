package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.CarteraResumenView;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class CarteraResumenDTO {
    private String codcli;
    private String numfac;
    private String clsdoc;
    private String fecfac;
    private String fecven;
    private String dias;
    private String monto;
    private String planta;
    private String rango;
    private boolean rowSelected = false;
    private CarteraResumenView carteraResumenView;
    private CarteraResumen carteraResumen;

    public CarteraResumen getCarteraResumen() {
        return carteraResumen;
    }

    public String listener_update(ActionEvent e) {
        try {
            carteraResumenView.action_modifyWitDTO(((codcli == null) ||
                codcli.equals("")) ? null : new String(codcli),
                ((numfac == null) || numfac.equals("")) ? null
                                                        : new String(numfac),
                ((clsdoc == null) || clsdoc.equals("")) ? null
                                                        : new String(clsdoc),
                ((fecfac == null) || fecfac.equals("")) ? null
                                                        : new String(fecfac),
                ((fecven == null) || fecven.equals("")) ? null
                                                        : new String(fecven),
                ((dias == null) || dias.equals("")) ? null : new String(dias),
                ((monto == null) || monto.equals("")) ? null : new String(monto),
                ((planta == null) || planta.equals("")) ? null
                                                        : new String(planta),
                ((rango == null) || rango.equals("")) ? null : new String(rango));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        codcli = carteraResumen.getId().getCodcli().toString();
        numfac = carteraResumen.getId().getNumfac().toString();
        clsdoc = carteraResumen.getId().getClsdoc().toString();
        fecfac = carteraResumen.getId().getFecfac().toString();
        fecven = carteraResumen.getId().getFecven().toString();
        dias = carteraResumen.getId().getDias().toString();
        monto = carteraResumen.getId().getMonto().toString();
        planta = carteraResumen.getId().getPlanta().toString();
        rango = carteraResumen.getId().getRango().toString();
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        codcli = carteraResumen.getId().getCodcli().toString();
        numfac = carteraResumen.getId().getNumfac().toString();
        clsdoc = carteraResumen.getId().getClsdoc().toString();
        fecfac = carteraResumen.getId().getFecfac().toString();
        fecven = carteraResumen.getId().getFecven().toString();
        dias = carteraResumen.getId().getDias().toString();
        monto = carteraResumen.getId().getMonto().toString();
        planta = carteraResumen.getId().getPlanta().toString();
        rango = carteraResumen.getId().getRango().toString();
        rowSelected = !rowSelected;
    }

    public void setCarteraResumen(CarteraResumen carteraResumen) {
        this.carteraResumen = carteraResumen;
    }

    public CarteraResumenView getCarteraResumenView() {
        return carteraResumenView;
    }

    public void setCarteraResumenView(CarteraResumenView carteraResumenView) {
        this.carteraResumenView = carteraResumenView;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getNumfac() {
        return numfac;
    }

    public void setNumfac(String numfac) {
        this.numfac = numfac;
    }

    public String getClsdoc() {
        return clsdoc;
    }

    public void setClsdoc(String clsdoc) {
        this.clsdoc = clsdoc;
    }

    public String getFecfac() {
        return fecfac;
    }

    public void setFecfac(String fecfac) {
        this.fecfac = fecfac;
    }

    public String getFecven() {
        return fecven;
    }

    public void setFecven(String fecven) {
        this.fecven = fecven;
    }

    public String getDias() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias = dias;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getPlanta() {
        return planta;
    }

    public void setPlanta(String planta) {
        this.planta = planta;
    }

    public String getRango() {
        return rango;
    }

    public void setRango(String rango) {
        this.rango = rango;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
