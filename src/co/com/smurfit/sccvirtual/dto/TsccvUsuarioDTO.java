package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvUsuarioView;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvUsuarioDTO {
    private String activo;
    private String apellido;
    private String cedula;
    private String email;
    private String login;
    private String nombre;
    private String password;
    private String idGrup_TbmBaseGruposUsuarios;
    private String idEmpr_TsccvEmpresas;
    private String codpla_TsccvPlanta;
    private String idUsua;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TsccvUsuarioView tsccvUsuarioView;
    private TsccvUsuario tsccvUsuario;

    public TsccvUsuario getTsccvUsuario() {
        return tsccvUsuario;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvUsuarioView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((apellido == null) || apellido.equals("")) ? null
                                                            : new String(
                    apellido),
                ((cedula == null) || cedula.equals("")) ? null
                                                        : new Integer(cedula),
                ((email == null) || email.equals("")) ? null : new String(email),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idUsua == null) || idUsua.equals("")) ? null
                                                        : new Integer(idUsua),
                ((login == null) || login.equals("")) ? null : new String(login),
                ((nombre == null) || nombre.equals("")) ? null
                                                        : new String(nombre),
                ((password == null) || password.equals("")) ? null
                                                            : new String(
                    password),
                ((idGrup_TbmBaseGruposUsuarios == null) ||
                idGrup_TbmBaseGruposUsuarios.equals("")) ? null
                                                         : new Long(
                    idGrup_TbmBaseGruposUsuarios),
                ((idEmpr_TsccvEmpresas == null) ||
                idEmpr_TsccvEmpresas.equals("")) ? null
                                                 : new Integer(
                    idEmpr_TsccvEmpresas),
                ((codpla_TsccvPlanta == null) || codpla_TsccvPlanta.equals(""))
                ? null : new String(codpla_TsccvPlanta));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idUsua = tsccvUsuario.getIdUsua().toString();
        activo = (tsccvUsuario.getActivo() != null)
            ? tsccvUsuario.getActivo().toString() : null;
        apellido = (tsccvUsuario.getApellido() != null)
            ? tsccvUsuario.getApellido().toString() : null;
        cedula = (tsccvUsuario.getCedula() != null)
            ? tsccvUsuario.getCedula().toString() : null;
        email = (tsccvUsuario.getEmail() != null)
            ? tsccvUsuario.getEmail().toString() : null;
        fechaCreacion = tsccvUsuario.getFechaCreacion();
        login = (tsccvUsuario.getLogin() != null)
            ? tsccvUsuario.getLogin().toString() : null;
        nombre = (tsccvUsuario.getNombre() != null)
            ? tsccvUsuario.getNombre().toString() : null;
        password = (tsccvUsuario.getPassword() != null)
            ? tsccvUsuario.getPassword().toString() : null;
        idGrup_TbmBaseGruposUsuarios = (tsccvUsuario.getTbmBaseGruposUsuarios()
                                                    .getIdGrup() != null)
            ? tsccvUsuario.getTbmBaseGruposUsuarios().getIdGrup().toString()
            : null;
        idEmpr_TsccvEmpresas = (tsccvUsuario.getTsccvEmpresas().getIdEmpr() != null)
            ? tsccvUsuario.getTsccvEmpresas().getIdEmpr().toString() : null;
        codpla_TsccvPlanta = (tsccvUsuario.getTsccvPlanta().getCodpla() != null)
            ? tsccvUsuario.getTsccvPlanta().getCodpla().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idUsua = tsccvUsuario.getIdUsua().toString();
        activo = (tsccvUsuario.getActivo() != null)
            ? tsccvUsuario.getActivo().toString() : null;
        apellido = (tsccvUsuario.getApellido() != null)
            ? tsccvUsuario.getApellido().toString() : null;
        cedula = (tsccvUsuario.getCedula() != null)
            ? tsccvUsuario.getCedula().toString() : null;
        email = (tsccvUsuario.getEmail() != null)
            ? tsccvUsuario.getEmail().toString() : null;
        fechaCreacion = tsccvUsuario.getFechaCreacion();
        login = (tsccvUsuario.getLogin() != null)
            ? tsccvUsuario.getLogin().toString() : null;
        nombre = (tsccvUsuario.getNombre() != null)
            ? tsccvUsuario.getNombre().toString() : null;
        password = (tsccvUsuario.getPassword() != null)
            ? tsccvUsuario.getPassword().toString() : null;
        idGrup_TbmBaseGruposUsuarios = (tsccvUsuario.getTbmBaseGruposUsuarios()
                                                    .getIdGrup() != null)
            ? tsccvUsuario.getTbmBaseGruposUsuarios().getIdGrup().toString()
            : null;
        idEmpr_TsccvEmpresas = (tsccvUsuario.getTsccvEmpresas().getIdEmpr() != null)
            ? tsccvUsuario.getTsccvEmpresas().getIdEmpr().toString() : null;
        codpla_TsccvPlanta = (tsccvUsuario.getTsccvPlanta().getCodpla() != null)
            ? tsccvUsuario.getTsccvPlanta().getCodpla().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvUsuario(TsccvUsuario tsccvUsuario) {
        this.tsccvUsuario = tsccvUsuario;
    }

    public TsccvUsuarioView getTsccvUsuarioView() {
        return tsccvUsuarioView;
    }

    public void setTsccvUsuarioView(TsccvUsuarioView tsccvUsuarioView) {
        this.tsccvUsuarioView = tsccvUsuarioView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdGrup_TbmBaseGruposUsuarios() {
        return idGrup_TbmBaseGruposUsuarios;
    }

    public void setIdGrup_TbmBaseGruposUsuarios(
        String idGrup_TbmBaseGruposUsuarios) {
        this.idGrup_TbmBaseGruposUsuarios = idGrup_TbmBaseGruposUsuarios;
    }

    public String getIdEmpr_TsccvEmpresas() {
        return idEmpr_TsccvEmpresas;
    }

    public void setIdEmpr_TsccvEmpresas(String idEmpr_TsccvEmpresas) {
        this.idEmpr_TsccvEmpresas = idEmpr_TsccvEmpresas;
    }

    public String getCodpla_TsccvPlanta() {
        return codpla_TsccvPlanta;
    }

    public void setCodpla_TsccvPlanta(String codpla_TsccvPlanta) {
        this.codpla_TsccvPlanta = codpla_TsccvPlanta;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdUsua() {
        return idUsua;
    }

    public void setIdUsua(String idUsua) {
        this.idUsua = idUsua;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
