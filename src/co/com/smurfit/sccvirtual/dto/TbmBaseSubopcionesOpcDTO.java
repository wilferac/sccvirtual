package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TbmBaseSubopcionesOpcView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TbmBaseSubopcionesOpcDTO {
    private String idOpci_TbmBaseOpciones;
    private String idSuop_TbmBaseSubopciones;
    private String idOpci;
    private String idSuop;
    private String nombreVisible;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TbmBaseSubopcionesOpcView tbmBaseSubopcionesOpcView;
    private TbmBaseSubopcionesOpc tbmBaseSubopcionesOpc;

    public TbmBaseSubopcionesOpc getTbmBaseSubopcionesOpc() {
        return tbmBaseSubopcionesOpc;
    }

    public String listener_update(ActionEvent e) {
        try {
            tbmBaseSubopcionesOpcView.action_modifyWitDTO(((idOpci == null) ||
                idOpci.equals("")) ? null : new Long(idOpci),
                ((idSuop == null) || idSuop.equals("")) ? null : new Long(
                    idSuop),
                ((nombreVisible == null) || nombreVisible.equals("")) ? null
                                                                      : new String(
                    nombreVisible),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idOpci_TbmBaseOpciones == null) ||
                idOpci_TbmBaseOpciones.equals("")) ? null
                                                   : new Long(
                    idOpci_TbmBaseOpciones),
                ((idSuop_TbmBaseSubopciones == null) ||
                idSuop_TbmBaseSubopciones.equals("")) ? null
                                                      : new Long(
                    idSuop_TbmBaseSubopciones));
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idOpci = tbmBaseSubopcionesOpc.getId().getIdOpci().toString();
        idSuop = tbmBaseSubopcionesOpc.getId().getIdSuop().toString();
        nombreVisible = tbmBaseSubopcionesOpc.getId().getNombreVisible()
                                             .toString();
        fechaCreacion = tbmBaseSubopcionesOpc.getId().getFechaCreacion();
        idOpci_TbmBaseOpciones = (tbmBaseSubopcionesOpc.getTbmBaseOpciones()
                                                       .getIdOpci() != null)
            ? tbmBaseSubopcionesOpc.getTbmBaseOpciones().getIdOpci().toString()
            : null;
        idSuop_TbmBaseSubopciones = (tbmBaseSubopcionesOpc.getTbmBaseSubopciones()
                                                          .getIdSuop() != null)
            ? tbmBaseSubopcionesOpc.getTbmBaseSubopciones().getIdSuop()
                                   .toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idOpci = tbmBaseSubopcionesOpc.getId().getIdOpci().toString();
        idSuop = tbmBaseSubopcionesOpc.getId().getIdSuop().toString();
        nombreVisible = tbmBaseSubopcionesOpc.getId().getNombreVisible()
                                             .toString();
        fechaCreacion = tbmBaseSubopcionesOpc.getId().getFechaCreacion();
        idOpci_TbmBaseOpciones = (tbmBaseSubopcionesOpc.getTbmBaseOpciones()
                                                       .getIdOpci() != null)
            ? tbmBaseSubopcionesOpc.getTbmBaseOpciones().getIdOpci().toString()
            : null;
        idSuop_TbmBaseSubopciones = (tbmBaseSubopcionesOpc.getTbmBaseSubopciones()
                                                          .getIdSuop() != null)
            ? tbmBaseSubopcionesOpc.getTbmBaseSubopciones().getIdSuop()
                                   .toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTbmBaseSubopcionesOpc(
        TbmBaseSubopcionesOpc tbmBaseSubopcionesOpc) {
        this.tbmBaseSubopcionesOpc = tbmBaseSubopcionesOpc;
    }

    public TbmBaseSubopcionesOpcView getTbmBaseSubopcionesOpcView() {
        return tbmBaseSubopcionesOpcView;
    }

    public void setTbmBaseSubopcionesOpcView(
        TbmBaseSubopcionesOpcView tbmBaseSubopcionesOpcView) {
        this.tbmBaseSubopcionesOpcView = tbmBaseSubopcionesOpcView;
    }

    public String getIdOpci_TbmBaseOpciones() {
        return idOpci_TbmBaseOpciones;
    }

    public void setIdOpci_TbmBaseOpciones(String idOpci_TbmBaseOpciones) {
        this.idOpci_TbmBaseOpciones = idOpci_TbmBaseOpciones;
    }

    public String getIdSuop_TbmBaseSubopciones() {
        return idSuop_TbmBaseSubopciones;
    }

    public void setIdSuop_TbmBaseSubopciones(String idSuop_TbmBaseSubopciones) {
        this.idSuop_TbmBaseSubopciones = idSuop_TbmBaseSubopciones;
    }

    public String getIdOpci() {
        return idOpci;
    }

    public void setIdOpci(String idOpci) {
        this.idOpci = idOpci;
    }

    public String getIdSuop() {
        return idSuop;
    }

    public void setIdSuop(String idSuop) {
        this.idSuop = idSuop;
    }

    public String getNombreVisible() {
        return nombreVisible;
    }

    public void setNombreVisible(String nombreVisible) {
        this.nombreVisible = nombreVisible;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
