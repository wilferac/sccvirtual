package co.com.smurfit.sccvirtual.dto;

import co.com.smurfit.presentation.backEndBeans.TsccvPedidosView;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;

import java.util.Date;

import javax.faces.event.ActionEvent;


/**
 *
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 *
 */
public class TsccvPedidosDTO {
    private String activo;
    private String observaciones;
    private String registrado;
    private String codpla_TsccvPlanta;
    private String idUsua_TsccvUsuario;
    private String idPedi;
    private Date fechaCreacion;
    private boolean rowSelected = false;
    private TsccvPedidosView tsccvPedidosView;
    private TsccvPedidos tsccvPedidos;

    public TsccvPedidos getTsccvPedidos() {
        return tsccvPedidos;
    }

    public String listener_update(ActionEvent e) {
        try {
            tsccvPedidosView.action_modifyWitDTO(((activo == null) ||
                activo.equals("")) ? null : new String(activo),
                ((fechaCreacion == null) || fechaCreacion.equals("")) ? null
                                                                      : fechaCreacion,
                ((idPedi == null) || idPedi.equals("")) ? null
                                                        : new Integer(idPedi),null,
                ((observaciones == null) || observaciones.equals("")) ? null
                                                                      : new String(
                    observaciones),
                ((registrado == null) || registrado.equals("")) ? null
                                                                : new String(
                    registrado),
                ((codpla_TsccvPlanta == null) || codpla_TsccvPlanta.equals(""))
                ? null : new String(codpla_TsccvPlanta),
                ((idUsua_TsccvUsuario == null) ||
                idUsua_TsccvUsuario.equals("")) ? null
                                                : new Integer(
                    idUsua_TsccvUsuario), null);
            rowSelected = !rowSelected;
        } catch (Exception ex) {
            return "";
        }

        return "";
    }

    public void listener_cancel(ActionEvent e) {
        idPedi = tsccvPedidos.getIdPedi().toString();
        activo = (tsccvPedidos.getActivo() != null)
            ? tsccvPedidos.getActivo().toString() : null;
        fechaCreacion = tsccvPedidos.getFechaCreacion();
        observaciones = (tsccvPedidos.getObservaciones() != null)
            ? tsccvPedidos.getObservaciones().toString() : null;
        registrado = (tsccvPedidos.getRegistrado() != null)
            ? tsccvPedidos.getRegistrado().toString() : null;
        codpla_TsccvPlanta = (tsccvPedidos.getTsccvPlanta().getCodpla() != null)
            ? tsccvPedidos.getTsccvPlanta().getCodpla().toString() : null;
        idUsua_TsccvUsuario = (tsccvPedidos.getTsccvUsuario().getIdUsua() != null)
            ? tsccvPedidos.getTsccvUsuario().getIdUsua().toString() : null;
        rowSelected = !rowSelected;
    }

    /**
     * <p>Bound to commandLink actionListener in the ui that renders/unrenders
     * the Customer details for editing.</p>
     */
    public void toggleSelected(ActionEvent e) {
        idPedi = tsccvPedidos.getIdPedi().toString();
        activo = (tsccvPedidos.getActivo() != null)
            ? tsccvPedidos.getActivo().toString() : null;
        fechaCreacion = tsccvPedidos.getFechaCreacion();
        observaciones = (tsccvPedidos.getObservaciones() != null)
            ? tsccvPedidos.getObservaciones().toString() : null;
        registrado = (tsccvPedidos.getRegistrado() != null)
            ? tsccvPedidos.getRegistrado().toString() : null;
        codpla_TsccvPlanta = (tsccvPedidos.getTsccvPlanta().getCodpla() != null)
            ? tsccvPedidos.getTsccvPlanta().getCodpla().toString() : null;
        idUsua_TsccvUsuario = (tsccvPedidos.getTsccvUsuario().getIdUsua() != null)
            ? tsccvPedidos.getTsccvUsuario().getIdUsua().toString() : null;
        rowSelected = !rowSelected;
    }

    public void setTsccvPedidos(TsccvPedidos tsccvPedidos) {
        this.tsccvPedidos = tsccvPedidos;
    }

    public TsccvPedidosView getTsccvPedidosView() {
        return tsccvPedidosView;
    }

    public void setTsccvPedidosView(TsccvPedidosView tsccvPedidosView) {
        this.tsccvPedidosView = tsccvPedidosView;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getRegistrado() {
        return registrado;
    }

    public void setRegistrado(String registrado) {
        this.registrado = registrado;
    }

    public String getCodpla_TsccvPlanta() {
        return codpla_TsccvPlanta;
    }

    public void setCodpla_TsccvPlanta(String codpla_TsccvPlanta) {
        this.codpla_TsccvPlanta = codpla_TsccvPlanta;
    }

    public String getIdUsua_TsccvUsuario() {
        return idUsua_TsccvUsuario;
    }

    public void setIdUsua_TsccvUsuario(String idUsua_TsccvUsuario) {
        this.idUsua_TsccvUsuario = idUsua_TsccvUsuario;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getIdPedi() {
        return idPedi;
    }

    public void setIdPedi(String idPedi) {
        this.idPedi = idPedi;
    }

    public boolean isRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(boolean rowSelected) {
        this.rowSelected = rowSelected;
    }
}
