package co.com.smurfit.sccvirtual.etiquetas;

import java.io.IOException;
import java.util.Properties;

public class ArchivoBundle {
	
	public String getProperty(String name) throws Exception{ 
		Properties pro=null;
	    try {
	    	pro= new Properties();
	    	//pro.load(this.getClass().getResource("pruebas.properties").openStream());
	    	pro.load(this.getClass().getResource("bundle.properties").openStream());
	    	return pro.getProperty(name);
	    } catch (IOException ex) {
	    	System.out.println("Error leyendo propiedad por: "+ex.getMessage());
	    	throw ex;
	    }
	}
	
	
}
