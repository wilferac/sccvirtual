package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.dao.TsccvDetalleGrupoEmpresaDAO;
import co.com.smurfit.dataaccess.dao.TsccvEstadisticaAccPedDAO;
import co.com.smurfit.dataaccess.dao.TsccvUsuarioDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEstadisticaAccPed;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoAccion;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class TsccvEstadisticaAccPedLogic implements ITsccvEstadisticaAccPedLogic {
    public List<TsccvEstadisticaAccPed> getTsccvEstadisticaAccPed()
        throws Exception {
        List<TsccvEstadisticaAccPed> list = new ArrayList<TsccvEstadisticaAccPed>();

        try {
            list = JPADaoFactory.getInstance().getTsccvEstadisticaAccPedDAO()
                                .findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public static synchronized void saveTsccvEstadisticaAccPed(Long acceso, Date fechaCreacion,
        Long idEsap, Long pedido, Long idGrup_TbmBaseGruposUsuarios,
        Integer idEmpr_TsccvEmpresas, String codpla_TsccvPlanta,
        Long idTiac_TsccvTipoAccion, Integer idUsua_TsccvUsuario)
        throws Exception {
        TsccvEstadisticaAccPed entity = null;

        try {
            if ((acceso != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        acceso, 1, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "acceso");
            }

            /*if (idEsap == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idEsap");
            }

            if ((idEsap != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idEsap, 18, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idEsap");
            }*/

            if ((pedido != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        pedido, 1, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "pedido");
            }

            if (idGrup_TbmBaseGruposUsuarios == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "idGrup_TbmBaseGruposUsuarios");
            }

            if ((idGrup_TbmBaseGruposUsuarios != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idGrup_TbmBaseGruposUsuarios, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idGrup_TbmBaseGruposUsuarios");
            }

            if (idEmpr_TsccvEmpresas == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "idEmpr_TsccvEmpresas");
            }

            if (codpla_TsccvPlanta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "codpla_TsccvPlanta");
            }

            if ((codpla_TsccvPlanta != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla_TsccvPlanta");
            }

            if ((idTiac_TsccvTipoAccion != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idTiac_TsccvTipoAccion, 1, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idTiac_TsccvTipoAccion");
            }

            ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios1 = new TbmBaseGruposUsuariosLogic();
            ITsccvEmpresasLogic logicTsccvEmpresas2 = new TsccvEmpresasLogic();
            ITsccvPlantaLogic logicTsccvPlanta3 = new TsccvPlantaLogic();
            ITsccvTipoAccionLogic logicTsccvTipoAccion4 = new TsccvTipoAccionLogic();
            ITsccvUsuarioLogic logicTsccvUsuario5 = new TsccvUsuarioLogic();

            
            System.out.println("Inicia consulta grupos usuarios");
            TbmBaseGruposUsuarios tbmBaseGruposUsuariosClass = logicTbmBaseGruposUsuarios1.getTbmBaseGruposUsuarios(idGrup_TbmBaseGruposUsuarios);
			System.out.println("Termina consulta grupos usuarios");
            
			System.out.println("Inicia consulta grupos empresas");
			TsccvEmpresas tsccvEmpresasClass = logicTsccvEmpresas2.getTsccvEmpresas(idEmpr_TsccvEmpresas);
			System.out.println("Termina consulta grupos empresas");
			
			System.out.println("Inicia consulta plantas");
			TsccvPlanta tsccvPlantaClass = logicTsccvPlanta3.getTsccvPlanta(codpla_TsccvPlanta);
			System.out.println("Termina consulta plantas");
			
			System.out.println("Inicia consulta tipo accion");
			TsccvTipoAccion tsccvTipoAccionClass = logicTsccvTipoAccion4.getTsccvTipoAccion(idTiac_TsccvTipoAccion);
			System.out.println("Termina consulta tipo accion");
            
			System.out.println("Inicia consulta usuario");
			TsccvUsuario tsccvUsuarioClass = logicTsccvUsuario5.getTsccvUsuario(idUsua_TsccvUsuario);
			System.out.println("Termina consulta usuario");
			

            if (tbmBaseGruposUsuariosClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvEmpresasClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }
            else{
            	if(tsccvEmpresasClass.getTsccvGrupoEmpresa() != null){//si la empresa es un grupo se selecciona de forma aleatoria de este para el registro
            		ITsccvDetalleGrupoEmpresaLogic tsccvDetalleGrupoEmpresaLogic = new TsccvDetalleGrupoEmpresaLogic();
            		System.out.println("Inicia consulta");
            		List<TsccvDetalleGrupoEmpresa> listaDetaleGrupoEmpr = tsccvDetalleGrupoEmpresaLogic.findByProperty(TsccvDetalleGrupoEmpresaDAO.IDGRUE,
            				tsccvEmpresasClass.getTsccvGrupoEmpresa().getIdGrue());
            		System.out.println("Termina consulta");
            		
            		
            		if(listaDetaleGrupoEmpr != null && listaDetaleGrupoEmpr.size() > 0){
            			tsccvEmpresasClass = listaDetaleGrupoEmpr.get(((int)(Math.random())*listaDetaleGrupoEmpr.size())).getTsccvEmpresas();
            		}
            	}
            }

            if (tsccvPlantaClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvTipoAccionClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvUsuarioClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            /*entity = getTsccvEstadisticaAccPed(idEsap);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }*/

            entity = new TsccvEstadisticaAccPed();

            entity.setAcceso(acceso);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdEsap(idEsap);
            entity.setPedido(pedido);

            entity.setTbmBaseGruposUsuarios(tbmBaseGruposUsuariosClass);
            entity.setTsccvEmpresas(tsccvEmpresasClass);
            entity.setTsccvPlanta(tsccvPlantaClass);
            entity.setTsccvTipoAccion(tsccvTipoAccionClass);
            entity.setTsccvUsuario(tsccvUsuarioClass);
            
            System.out.println("Inicia registro");
            EntityManagerHelper.beginTransaction();
            //Comentariamos el llamado inicial
            //JPADaoFactory.getInstance().getTsccvEstadisticaAccPedDAO().save(entity);
            //Llamamos el nuevo metodo que realiza el registro
            TsccvEstadisticaAccPedDAO.saveEstadisticaAccPed(entity);
            EntityManagerHelper.commit();
            System.out.println("Termina registro");
           
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTsccvEstadisticaAccPed(Long idEsap)
        throws Exception {
        TsccvEstadisticaAccPed entity = null;

        if (idEsap == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        entity = getTsccvEstadisticaAccPed(idEsap);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvEstadisticaAccPedDAO()
                         .delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTsccvEstadisticaAccPed(Long acceso, Date fechaCreacion,
        Long idEsap, Long pedido, Long idGrup_TbmBaseGruposUsuarios,
        Integer idEmpr_TsccvEmpresas, String codpla_TsccvPlanta,
        Long idTiac_TsccvTipoAccion, Integer idUsua_TsccvUsuario)
        throws Exception {
        TsccvEstadisticaAccPed entity = null;

        try {
            if ((acceso != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        acceso, 1, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "acceso");
            }

            if (idEsap == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idEsap");
            }

            if ((idEsap != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idEsap, 18, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idEsap");
            }

            if ((pedido != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        pedido, 1, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "pedido");
            }

            if (idGrup_TbmBaseGruposUsuarios == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "idGrup_TbmBaseGruposUsuarios");
            }

            if ((idGrup_TbmBaseGruposUsuarios != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idGrup_TbmBaseGruposUsuarios, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idGrup_TbmBaseGruposUsuarios");
            }

            if (idEmpr_TsccvEmpresas == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "idEmpr_TsccvEmpresas");
            }

            if (codpla_TsccvPlanta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "codpla_TsccvPlanta");
            }

            if ((codpla_TsccvPlanta != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla_TsccvPlanta");
            }

            if ((idTiac_TsccvTipoAccion != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idTiac_TsccvTipoAccion, 1, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idTiac_TsccvTipoAccion");
            }

            ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios1 = new TbmBaseGruposUsuariosLogic();
            ITsccvEmpresasLogic logicTsccvEmpresas2 = new TsccvEmpresasLogic();
            ITsccvPlantaLogic logicTsccvPlanta3 = new TsccvPlantaLogic();
            ITsccvTipoAccionLogic logicTsccvTipoAccion4 = new TsccvTipoAccionLogic();
            ITsccvUsuarioLogic logicTsccvUsuario5 = new TsccvUsuarioLogic();

            TbmBaseGruposUsuarios tbmBaseGruposUsuariosClass = logicTbmBaseGruposUsuarios1.getTbmBaseGruposUsuarios(idGrup_TbmBaseGruposUsuarios);
            TsccvEmpresas tsccvEmpresasClass = logicTsccvEmpresas2.getTsccvEmpresas(idEmpr_TsccvEmpresas);
            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta3.getTsccvPlanta(codpla_TsccvPlanta);
            TsccvTipoAccion tsccvTipoAccionClass = logicTsccvTipoAccion4.getTsccvTipoAccion(idTiac_TsccvTipoAccion);
            TsccvUsuario tsccvUsuarioClass = logicTsccvUsuario5.getTsccvUsuario(idUsua_TsccvUsuario);

            if (tbmBaseGruposUsuariosClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvEmpresasClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvPlantaClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvTipoAccionClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvUsuarioClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            entity = getTsccvEstadisticaAccPed(idEsap);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setAcceso(acceso);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdEsap(idEsap);
            entity.setPedido(pedido);

            entity.setTbmBaseGruposUsuarios(tbmBaseGruposUsuariosClass);
            entity.setTsccvEmpresas(tsccvEmpresasClass);
            entity.setTsccvPlanta(tsccvPlantaClass);
            entity.setTsccvTipoAccion(tsccvTipoAccionClass);
            entity.setTsccvUsuario(tsccvUsuarioClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvEstadisticaAccPedDAO()
                         .update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TsccvEstadisticaAccPed getTsccvEstadisticaAccPed(Long idEsap)
        throws Exception {
        TsccvEstadisticaAccPed entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvEstadisticaAccPedDAO()
                                  .findById(idEsap);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvEstadisticaAccPed"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TsccvEstadisticaAccPed> findPageTsccvEstadisticaAccPed(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        List<TsccvEstadisticaAccPed> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvEstadisticaAccPedDAO()
                                  .findPageTsccvEstadisticaAccPed(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvEstadisticaAccPed"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTsccvEstadisticaAccPed()
        throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvEstadisticaAccPedDAO()
                                  .findTotalNumberTsccvEstadisticaAccPed();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvEstadisticaAccPed Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TsccvEstadisticaAccPed> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TsccvEstadisticaAccPed> list = new ArrayList<TsccvEstadisticaAccPed>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTsccvEstadisticaAccPedDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
}
