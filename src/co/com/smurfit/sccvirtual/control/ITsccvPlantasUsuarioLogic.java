package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvPlantasUsuarioLogic {
    public List<TsccvPlantasUsuario> getTsccvPlantasUsuario()
        throws Exception;

    public void saveTsccvPlantasUsuario(String codpla, Integer idUsua,
        Date fechaCreacion, String codpla_TsccvPlanta,
        Integer idUsua_TsccvUsuario, List<TsccvPlantasUsuario> listadoPlantasUsuaio) throws Exception;

    public void deleteTsccvPlantasUsuario(String codpla, Integer idUsua)
        throws Exception;

    public void updateTsccvPlantasUsuario(String codpla, Integer idUsua,
        Date fechaCreacion, String codpla_TsccvPlanta,
        Integer idUsua_TsccvUsuario) throws Exception;

    public TsccvPlantasUsuario getTsccvPlantasUsuario(TsccvPlantasUsuarioId id)
        throws Exception;

    public List<TsccvPlantasUsuario> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvPlantasUsuario> findPageTsccvPlantasUsuario(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvPlantasUsuario() throws Exception;
    
    public List<TsccvPlantasUsuario> findByProperty(String propertyName,final Object value) throws Exception;
    
    public void plantaIsAdministrada(String codPla) throws Exception;
}
