package co.com.smurfit.sccvirtual.control;

import java.util.Vector;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.Utilities;

public class ConstruirEmailCreacionDeUsuario {	
	private StringBuffer cuerpoEmail;
	private String saltoDeLinea;
	private Vector<String> destinatarios;
	
	public ConstruirEmailCreacionDeUsuario() {
		saltoDeLinea = System.getProperty("line.separator");
		cuerpoEmail = new StringBuffer();
	}

	public Vector<String> getDestinatarios() {
		return destinatarios;
	}

	public void setDestinatarios(Vector<String> destinatarios) {
		this.destinatarios = destinatarios;
	}


	//metodo encargado de construir el cuerpo del mensaje de confirmacion de creacion de usuario
	public void construirConfirmacion(TsccvUsuario userNuevo, TsccvUsuario userLogin) throws BMBaseException, Exception{
		
		cuerpoEmail.append("<table class=\"tabla\" width=\"95%\">"+saltoDeLinea);
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\">Informaci�n B�sica</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);					
			
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"datoTabla\">"+saltoDeLinea);
					cuerpoEmail.append("Usuario Creado por: "+userLogin.getApellido()+" "+userLogin.getNombre()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Grupo: "+userLogin.getTbmBaseGruposUsuarios().getDescripcion()+"<br/><br/>"+saltoDeLinea);
				cuerpoEmail.append("</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr><br/>"+saltoDeLinea);									
			
			
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\">Informaci�n Nuevo Usuario</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
			
			//se adiciona el comentario del pedido
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"datoTabla\">"+saltoDeLinea);
					cuerpoEmail.append("Apellido y Nombre: "+userNuevo.getApellido()+" "+userNuevo.getNombre()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("C�dula: "+userNuevo.getCedula()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("E-mail: "+userNuevo.getEmail()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Usuario: "+userNuevo.getLogin()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Contrase�a: "+userNuevo.getPassword()+"<br/>"+saltoDeLinea);
					//cuerpoEmail.append("Activo: "+userNuevo.getActivo()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Planta: "+userNuevo.getTsccvPlanta().getDespla()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Grupo: "+userNuevo.getTbmBaseGruposUsuarios().getDescripcion()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Empresa: "+userNuevo.getTsccvEmpresas().getNombre()+"<br/>"+saltoDeLinea);
				cuerpoEmail.append("</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
		cuerpoEmail.append("</table>"+saltoDeLinea);
	}	
	
	
	//metodo encargado de construir el cuerpo del mensaje de cambio de contrase�a
	public void construirCambioContrasena(TsccvUsuario tsccvUsuario) throws BMBaseException, Exception{
		ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		
		cuerpoEmail.append("<table class=\"tabla\" width=\"95%\">"+saltoDeLinea);
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\">Informaci�n B�sica</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);					
			
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"datoTabla\">"+saltoDeLinea);
					cuerpoEmail.append(tsccvUsuario.getNombre()+" "+tsccvUsuario.getApellido()+". "+
									   "Usuario: "+tsccvUsuario.getLogin()+"<br/>"+
									   "Su contrase�a asociada a la cuenta en "+archivoPropiedades.getProperty("SUBJECT")+" ha sido modificada a lo siguiente:<br/> " +
									   "Contrase�a: "+tsccvUsuario.getPassword()+"<br/>"+
									   "Por favor, identif�quese con esta contrase�a para acceder a su cuenta y servicios. " +
									   "<br/><br/>"+saltoDeLinea);
				cuerpoEmail.append("</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
		cuerpoEmail.append("</table>"+saltoDeLinea);
	}
	
	
	//metodo encargado de retornar la union del cuerpo del email y la plantilla 
	public String getMensaje() throws BMBaseException, Exception{
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();		
		//String ruta = "http://74.208.147.70/"+Utilities.DIRIMG_PLANTILLAEMAIL;
		//String ruta = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/"+request.getContextPath()+Utilities.DIRIMG_PLANTILLAEMAIL;
		String ruta = request.getScheme()+"://"+request.getServerName()+"/"+request.getContextPath()+Utilities.DIRIMG_PLANTILLAEMAIL;
		
		ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		String plantilla = null;
		
		//se contruye la plantilla
		plantilla="<html>"+saltoDeLinea+
					"<head>"+saltoDeLinea+
						"<title></title>"+saltoDeLinea+//\""+ruta+"style-plantilla.css\"/
						//"<link rel=\"stylesheet\" type=\"text/css\" href=\""+ruta+archivoPropiedades.getProperty("NOMBRE_ESTILO_PLANTILLA")+"\"/>"+saltoDeLinea+
						
						//se diciona el estilo ya que por medio de una referncia no es posible
						"<style type=\"text/css\">"+
							".tituloPrimario{"+
								"width: 100%;"+
								"background: #99B3CC;"+
								"text-align: center;"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-weight: bold;"+
								"font-size: 14px;	"+
								"text-transform: uppercase;"+
								"height: 25px;"+
								"color: white;"+
							"}"+
							
							".tituloSecundario{"+
								"width: 100%;"+
								"background: #99B3CC;"+
								"text-align: left;"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-weight: bold;"+
								"font-size: 13px;	"+
								"text-transform: capitalize;"+
								"height: 25px;"+
								"color: white;"+
							"}"+
							
							".headerTabla{"+
								"background: #99B3CC;"+
								"color: white;"+
								"text-align: center;"+
								"text-transform: capitalize;"+
							"}"+
							
							".datoTabla{"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-size: 12px;"+
								"text-align: left;"+
							"}"+
							
							".tabla{"+
								"border: 1px"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-size: 12px;"+
								"text-align: left;"+
							"}"+
						"</style>"+
						
					"</head>"+saltoDeLinea+
					"<body>"+saltoDeLinea+
						"<table align=\"center\" border=\"0\" width=\"580\" cellspacing=\"0\" cellpadding=\"0\" >"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td>"+saltoDeLinea+
									"<img width=\"100%\" style=\"vertical-align:bottom\" src=\""+ruta+archivoPropiedades.getProperty("IMAGEN_SUPERIOR")+"\" alt=\""+archivoPropiedades.getProperty("IMAGEN_SUPERIOR")+"\"/>"+saltoDeLinea+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td align=\"center\" style=\"border-right-style:solid;border-left-style:solid;border-color:#B7BBBF;\">"+saltoDeLinea+
									"<table width=\"100%\"><tr><td>"+
										cuerpoEmail+saltoDeLinea+
									"</td></tr></table>"+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td>"+saltoDeLinea+
									"<img width=\"100%\" src=\""+ruta+archivoPropiedades.getProperty("IMAGEN_INFERIOR")+"\" alt=\""+archivoPropiedades.getProperty("IMAGEN_INFERIOR")+"\"/>"+saltoDeLinea+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
						"</table>"+saltoDeLinea+
					"</body>"+saltoDeLinea+
				  "</html>";
		
		return plantilla;
	}	
}
