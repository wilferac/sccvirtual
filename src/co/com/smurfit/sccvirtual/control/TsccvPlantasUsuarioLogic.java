package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.dao.TsccvPlantasUsuarioDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class TsccvPlantasUsuarioLogic implements ITsccvPlantasUsuarioLogic {
    public List<TsccvPlantasUsuario> getTsccvPlantasUsuario()
        throws Exception {
        List<TsccvPlantasUsuario> list = new ArrayList<TsccvPlantasUsuario>();

        try {
            list = JPADaoFactory.getInstance().getTsccvPlantasUsuarioDAO()
                                .findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveTsccvPlantasUsuario(String codpla, Integer idUsua,
        Date fechaCreacion, String codpla_TsccvPlanta,
        Integer idUsua_TsccvUsuario, List<TsccvPlantasUsuario> listadoPlantasUsuaio) throws Exception {
        TsccvPlantasUsuario entity = null;

        try {
        	//se crean las plantasUsuario
            if(listadoPlantasUsuaio != null){
            	for(TsccvPlantasUsuario plantaUsua : listadoPlantasUsuaio){
            		//se valida si la planta usuario es nueva
            		if(plantaUsua.getTsccvPlanta().getPlanta() == null){
            			continue;
            		}
            		
            		codpla = plantaUsua.getTsccvPlanta().getCodpla();
            		codpla_TsccvPlanta = plantaUsua.getTsccvPlanta().getCodpla();           		
            		
		            if (codpla == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "C�digo Planta");
		            }
		
		            if ((codpla != null) &&
		                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "C�digo Planta");
		            }
		
		            if (idUsua == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Id Usuario");
		            }
		
		            /*if ((idUsua != null) &&
		                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
		                        idUsua, 0, 0) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "idUsua");
		            }*/
		
		            if (fechaCreacion == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "Fecha Creaci�n");
		            }
		
		            if (codpla_TsccvPlanta == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "codpla_TsccvPlanta");
		            }
		
		            if ((codpla_TsccvPlanta != null) &&
		                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "C�digo Planta");
		            }
		
		            if (idUsua_TsccvUsuario == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "Id Usuario");
		            }
		
		            ITsccvPlantaLogic logicTsccvPlanta1 = new TsccvPlantaLogic();
		            ITsccvUsuarioLogic logicTsccvUsuario2 = new TsccvUsuarioLogic();
		
		            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta1.getTsccvPlanta(codpla_TsccvPlanta);
		            TsccvUsuario tsccvUsuarioClass = logicTsccvUsuario2.getTsccvUsuario(idUsua_TsccvUsuario);
		
		            if (tsccvPlantaClass == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL+"Planta");
		            }
		
		            if (tsccvUsuarioClass == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL+"Usuario");
		            }
		
		            TsccvPlantasUsuarioId idClass = new TsccvPlantasUsuarioId();
		            idClass.setCodpla(codpla);
		            idClass.setIdUsua(idUsua);
		
		            entity = getTsccvPlantasUsuario(idClass);
		
		            if (entity != null) {
		                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
		            }
		
		            entity = new TsccvPlantasUsuario();
		
		            entity.setFechaCreacion(fechaCreacion);
		            entity.setId(idClass);
		
		            entity.setTsccvPlanta(tsccvPlantaClass);
		            entity.setTsccvUsuario(tsccvUsuarioClass);
		
		            EntityManagerHelper.beginTransaction();
            		JPADaoFactory.getInstance().getTsccvPlantasUsuarioDAO().save(entity);   
            		EntityManagerHelper.commit();
            	}
            }
            
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTsccvPlantasUsuario(String codpla, Integer idUsua)
        throws Exception {
        TsccvPlantasUsuario entity = null;

        if (codpla == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL+"C�digo Planta");
        }

        if (idUsua == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL+"Id Usuario");
        }

        TsccvPlantasUsuarioId idClass = new TsccvPlantasUsuarioId();
        idClass.setCodpla(codpla);
        idClass.setIdUsua(idUsua);

        entity = getTsccvPlantasUsuario(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvPlantasUsuarioDAO()
                         .delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTsccvPlantasUsuario(String codpla, Integer idUsua,
        Date fechaCreacion, String codpla_TsccvPlanta,
        Integer idUsua_TsccvUsuario) throws Exception {
        TsccvPlantasUsuario entity = null;

        try {
            if (codpla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpla");
            }

            if ((codpla != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla");
            }

            if (idUsua == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idUsua");
            }

            if ((idUsua != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idUsua, 0, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idUsua");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "fechaCreacion");
            }

            if (codpla_TsccvPlanta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "codpla_TsccvPlanta");
            }

            if ((codpla_TsccvPlanta != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla_TsccvPlanta");
            }

            if (idUsua_TsccvUsuario == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "idUsua_TsccvUsuario");
            }

            ITsccvPlantaLogic logicTsccvPlanta1 = new TsccvPlantaLogic();
            ITsccvUsuarioLogic logicTsccvUsuario2 = new TsccvUsuarioLogic();

            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta1.getTsccvPlanta(codpla_TsccvPlanta);
            TsccvUsuario tsccvUsuarioClass = logicTsccvUsuario2.getTsccvUsuario(idUsua_TsccvUsuario);

            if (tsccvPlantaClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvUsuarioClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            TsccvPlantasUsuarioId idClass = new TsccvPlantasUsuarioId();
            idClass.setCodpla(codpla);
            idClass.setIdUsua(idUsua);

            entity = getTsccvPlantasUsuario(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setFechaCreacion(fechaCreacion);
            entity.setId(idClass);

            entity.setTsccvPlanta(tsccvPlantaClass);
            entity.setTsccvUsuario(tsccvUsuarioClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvPlantasUsuarioDAO()
                         .update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TsccvPlantasUsuario getTsccvPlantasUsuario(TsccvPlantasUsuarioId id)
        throws Exception {
        TsccvPlantasUsuario entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPlantasUsuarioDAO()
                                  .findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPlantasUsuario"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TsccvPlantasUsuario> findPageTsccvPlantasUsuario(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        List<TsccvPlantasUsuario> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPlantasUsuarioDAO()
                                  .findPageTsccvPlantasUsuario(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPlantasUsuario"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTsccvPlantasUsuario() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPlantasUsuarioDAO()
                                  .findTotalNumberTsccvPlantasUsuario();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPlantasUsuario Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TsccvPlantasUsuario> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TsccvPlantasUsuario> list = new ArrayList<TsccvPlantasUsuario>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTsccvPlantasUsuarioDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    //Metodo encargado de consultar plantasUsuario dependiendo de una propiedad
    public List<TsccvPlantasUsuario> findByProperty(String propertyName,final Object value) throws Exception{
    	try {
            return JPADaoFactory.getInstance().getTsccvPlantasUsuarioDAO().findByProperty(propertyName, value);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    //este metodo valida que la planta que se va a agregar a un usuario no este siendo administrada por otro
    public void plantaIsAdministrada(String codPla) throws Exception{
    	TsccvUsuario admin = null;
    	String nombreAdmin = "";
    	
    	try{
    		List<TsccvPlantasUsuario> resultado = BusinessDelegatorView.findByPropertyPlantasUsuario(TsccvPlantasUsuarioDAO.PLANTA, ((Object)codPla));
    		if(resultado != null && resultado.size() > 0){
        		admin = ((TsccvPlantasUsuario)resultado.get(0)).getTsccvUsuario();
        		nombreAdmin = admin.getApellido()+" "+admin.getNombre();
        		
        		throw new Exception("Esta planta ya est� siendo administrada por "+nombreAdmin);
    		}
	    } catch (Exception e) {
	        throw new Exception(e.getMessage());
	    } finally {
	        EntityManagerHelper.closeEntityManager();
	    }
    }
        
}
