package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvUsuarioLogic {
    public List<TsccvUsuario> getTsccvUsuario() throws Exception;

    /*public String saveTsccvUsuario(String activo, String apellido,
        Integer cedula, String email, Date fechaCreacion, Integer idUsua,
        String login, String nombre, String password,
        Long idGrup_TbmBaseGruposUsuarios, Integer idEmpr_TsccvEmpresas,
        String codpla_TsccvPlanta) throws Exception;
*/
    public void deleteTsccvUsuario(Integer idUsua) throws Exception;

    public String updateTsccvUsuario(String activo, String apellido,
        Integer cedula, String email, Date fechaCreacion, Integer idUsua,
        String login, String nombre, String password,
        Long idGrup_TbmBaseGruposUsuarios, Integer idEmpr_TsccvEmpresas,
        String codpla_TsccvPlanta) throws Exception;

    public TsccvUsuario getTsccvUsuario(Integer idUsua)
        throws Exception;

    public List<TsccvUsuario> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvUsuario> findPageTsccvUsuario(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTsccvUsuario() throws Exception;
    
    public List<TsccvUsuario> consultarUsuarios(String activo, String apellido,
            Integer cedula, String email, Date fechaCreacion, Integer idUsua,
            String login, String nombre, String password,
            Long idGrup_TbmBaseGruposUsuarios, Integer idEmpr_TsccvEmpresas,
            String codpla_TsccvPlanta) throws Exception;
    
    public Boolean usuarioIsRegistrado(String login, Integer cedula, Integer idEmpr) throws Exception;
    
    public void inactivarUsuario(Integer idUsua) throws Exception;
    
    public TsccvUsuario consultarUsuarioLogin(String propertyName, final Object login) throws Exception;
    
    public List<TsccvUsuario> consultarUsuariosPorGrupo(Integer idGrup) throws Exception;
    
    public void cambiarPassword(TsccvUsuario tsccvUsuario, String passwordNuevo) throws Exception, BMBaseException;
}
