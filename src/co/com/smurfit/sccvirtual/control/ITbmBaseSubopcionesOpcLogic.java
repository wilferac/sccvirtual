package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;

import java.math.BigDecimal;

import java.util.*;


public interface ITbmBaseSubopcionesOpcLogic {
    public List<TbmBaseSubopcionesOpc> getTbmBaseSubopcionesOpc()
        throws Exception;

    public void saveTbmBaseSubopcionesOpc(Long idOpci, Long idSuop,
        String nombreVisible, Date fechaCreacion, Long idOpci_TbmBaseOpciones,
        Long idSuop_TbmBaseSubopciones, List<TbmBaseSubopcionesOpc> listadoSubOpcionesOpc) throws Exception;

    public void deleteTbmBaseSubopcionesOpc(Long idOpci, Long idSuop,
        String nombreVisible, Date fechaCreacion, Long posicion) throws Exception;

    public void updateTbmBaseSubopcionesOpc(Long idOpci, Long idSuop,
        String nombreVisible, Date fechaCreacion, Long idOpci_TbmBaseOpciones,
        Long idSuop_TbmBaseSubopciones) throws Exception;

    public TbmBaseSubopcionesOpc getTbmBaseSubopcionesOpc(
        TbmBaseSubopcionesOpcId id) throws Exception;

    public List<TbmBaseSubopcionesOpc> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TbmBaseSubopcionesOpc> findPageTbmBaseSubopcionesOpc(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTbmBaseSubopcionesOpc()
        throws Exception;
    
    public List<TbmBaseSubopcionesOpc> findByProperty(String propertyName,final Object value) throws Exception;
    
    public Boolean subOpcionOpciIsRegistrada(String nombre, Long idOpci) throws Exception;
    
    public void cambiarPosicion(TbmBaseSubopcionesOpc tbmBaseSubopcionesOpc, String opcion) throws Exception;
}
