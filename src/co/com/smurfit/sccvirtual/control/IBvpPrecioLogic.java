package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;

import java.math.BigDecimal;

import java.util.*;


public interface IBvpPrecioLogic {
    public List<BvpPrecio> getBvpPrecio() throws Exception;

    public void saveBvpPrecio(String codCli, String codpla, String codScc,
        String precio, String alto, String ancho, String ancsac, String clave,
        String cntmax, String cntmin, String codpro, String despro,
        String estilo, String fuelle, String hijpro, String kitpro,
        String largo, String larsac, String moneda, String numcol,
        String refere, String tippro, String undmed) throws Exception;

    public void deleteBvpPrecio(String codCli, String codpla, String codScc,
        String precio) throws Exception;

    public void updateBvpPrecio(String codCli, String codpla, String codScc,
        String precio, String alto, String ancho, String ancsac, String clave,
        String cntmax, String cntmin, String codpro, String despro,
        String estilo, String fuelle, String hijpro, String kitpro,
        String largo, String larsac, String moneda, String numcol,
        String refere, String tippro, String undmed) throws Exception;

    public BvpPrecio getBvpPrecio(BvpPrecioId id) throws Exception;

    public List<BvpPrecio> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<BvpPrecio> findPageBvpPrecio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberBvpPrecio() throws Exception;
    
    public List<ProductoVO> consultarPdfsFaltantes(String codPla) throws Exception;
    
    public List<ProductoVO> consultarProductosBvp(String codigoProd, String descripcion, String coddigoCli, TsccvPlanta tsccvPlanta, String nombreColumna, Boolean ascendente) throws Exception;
    
    public List<ProductoVO> consultarPreciosBvp(String codigoProd, String descripcion, String coddigoCli, TsccvPlanta tsccvPlanta, String nombreColumna, Boolean ascendente) throws Exception;
    
    public Boolean precioIsRegistrado(String codScc, String codPla) throws Exception ;
    
    public Map consultarEscalasBvp(List<ProductosPedidoVO> listadoProductosPedido, String codCli) throws Exception;
    
    public List<ProductosPedidoVO> consultarPrecioPorCantidadTotal(List<ProductosPedidoVO> listadoProductosPedido, String coddigoCli)throws Exception;
}
