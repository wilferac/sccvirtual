package co.com.smurfit.sccvirtual.control;

import java.util.List;
import java.util.Vector;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import co.com.smurfit.dataaccess.dao.TsccvEntregasPedidoDAO;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.EnviarEmail;
import co.com.smurfit.utilities.Utilities;

public class ConstruirEmailConfirmacionOrden {	
	private StringBuffer cuerpoEmail;
	private String saltoDeLinea;
	private TsccvPedidos pedido;
	private Vector<String> destinatarios;
	private Vector<String> destinatariosInternos;
	private String dominioMailInterno="smurfitkappa.com.co";
	
	public ConstruirEmailConfirmacionOrden() {
		saltoDeLinea = System.getProperty("line.separator");
		cuerpoEmail = new StringBuffer();
	}
	
	
	public TsccvPedidos getPedido() {
		return pedido;
	}

	public void setPedido(TsccvPedidos pedido) {
		this.pedido = pedido;
	}
	
	public Vector<String> getDestinatarios() {
		return destinatarios;
	}

	public void setDestinatarios(Vector<String> destinatarios) {
		this.destinatarios = destinatarios;
	}


	//metodo encargado de construir el cuerpo del mensaje de confirmacion de orden para las plantas corrugado y sacos
	public void construirConfirmacionDeOrdenBvp(List<ProductosPedidoVO> listadoProductosPedido) throws BMBaseException, Exception{
		//NOTA: en cada elemento de la lista de productos pedido vienen sus respectivas entregas
		int numEntrega = 1;
		int numProductoPedido = 1;
		List<TsccvEntregasPedido> entregasProducto = null;
		
		cuerpoEmail.append("<table class=\"tabla\" width=\"95%\">"+saltoDeLinea);
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\">Informaci�n B�sica</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
						
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"datoTabla\">"+saltoDeLinea);
					cuerpoEmail.append("C�digo Cliente: "+pedido.getTsccvEmpresas().getCodVpe()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Nombre del Cliente: "+pedido.getTsccvEmpresas().getNombre()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Nombre Usuario: "+pedido.getTsccvUsuario().getNombre()+" "+pedido.getTsccvUsuario().getApellido()+"<br/><br/>"+saltoDeLinea);
				cuerpoEmail.append("</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
						
			//se recorre el listado de productos del pedido
			for(ProductosPedidoVO prodPedVO : listadoProductosPedido){
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\">Producto Pedido # "+numProductoPedido+"</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
			
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"datoTabla\">"+saltoDeLinea);
					cuerpoEmail.append("Orden de Compra: "+prodPedVO.getTsccvProductosPedido().getOrdenCompra()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("C�digo Producto - C�digo SCC: "+prodPedVO.getTsccvProductosPedido().getCodCli()+" - "+prodPedVO.getTsccvProductosPedido().getCodScc()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Descripci�n del Producto: "+prodPedVO.getTsccvProductosPedido().getDespro()+"<br/>"+saltoDeLinea);
				cuerpoEmail.append("</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>");
							
				
			//se construye la tabla de las entregas
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td>"+saltoDeLinea);
					cuerpoEmail.append("<table class=\"tabla\" width=\"100%\">"+saltoDeLinea);
						//titulo de la tabla
						cuerpoEmail.append("<tr>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"tituloSecundario\" colspan=\"4\">Entregas</td>"+saltoDeLinea);
						cuerpoEmail.append("</tr>"+saltoDeLinea);
						
						cuerpoEmail.append("<tr>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"5%\">No.</td>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"40%\">Direcci�n de Despacho</td>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"30%\">Fecha Entrega</td>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"25%\">Cantidad Solicitada</td>"+saltoDeLinea);
						cuerpoEmail.append("</tr>");
							
						entregasProducto = BusinessDelegatorView.findByPropertyEntregasPedido(TsccvEntregasPedidoDAO.PRODPED, prodPedVO.getTsccvProductosPedido().getIdPrpe());
						
				//se recorre el listado de entregas de cada productos pedido
				for(TsccvEntregasPedido entrePed : entregasProducto){
						cuerpoEmail.append("<tr>"+saltoDeLinea);
							cuerpoEmail.append("<td>"+numEntrega+"</td>");
							cuerpoEmail.append("<td>"+entrePed.getDesdir()+"</td>");
							cuerpoEmail.append("<td>"+entrePed.getFechaEntrega()+"</td>");
							cuerpoEmail.append("<td>"+entrePed.getCantidad()+"</td>");
						cuerpoEmail.append("</tr>"+saltoDeLinea);
					numEntrega++;
				}							
					cuerpoEmail.append("</table>");
				cuerpoEmail.append("</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
			
			numEntrega = 1;
			numProductoPedido++;
			}			
			
			//se adiciona el comentario del pedido
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\" colspan=\"4\">Observaciones</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td colspan=\"4\">"+(pedido.getObservaciones() != null && !pedido.getObservaciones().equals("") ? pedido.getObservaciones() : "Ninguna")+"</td>");
			cuerpoEmail.append("</tr>"+saltoDeLinea);
		cuerpoEmail.append("</table>"+saltoDeLinea);
		
		validarDestinatarios();
	}	
	
	
	//metodo encargado de construir el cuerpo del mensaje de confirmacion de orden para las plantas molinos
	public void construirConfirmacionDeOrdenSap(List<ProductosPedidoVO> listadoProductosPedido) throws BMBaseException, Exception{
		//NOTA: en cada elemento de la lista de productos pedido vienen sus respectivas entregas
		int numEntrega = 1;
		int numProductoPedido = 1;
		List<TsccvEntregasPedido> entregasProducto = null;
		
		cuerpoEmail.append("<table class=\"tabla\" width=\"95%\">"+saltoDeLinea);
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\">Informaci�n B�sica</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
						
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"datoTabla\">"+saltoDeLinea);
					cuerpoEmail.append("C�digo Cliente: "+pedido.getTsccvEmpresas().getCodMas()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Nombre del Cliente: "+pedido.getTsccvEmpresas().getNombre()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Nombre Usuario: "+pedido.getTsccvUsuario().getNombre()+" "+pedido.getTsccvUsuario().getApellido()+"<br/><br/>"+saltoDeLinea);
				cuerpoEmail.append("</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
						
			//se recorre el listado de productos del pedido
			for(ProductosPedidoVO prodPedVO : listadoProductosPedido){
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\">Producto Pedido # "+numProductoPedido+"</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
			
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"datoTabla\">"+saltoDeLinea);
					cuerpoEmail.append("Orden de Compra: "+prodPedVO.getTsccvProductosPedido().getOrdenCompra()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("C�digo Producto: "+prodPedVO.getTsccvProductosPedido().getCodCli()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Descripci�n del Producto: "+prodPedVO.getTsccvProductosPedido().getDespro()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Eje Solicitado: "+prodPedVO.getTsccvProductosPedido().getDiamtr()+"<br/>"+saltoDeLinea);
					cuerpoEmail.append("Di�metro Solicitado: "+prodPedVO.getTsccvProductosPedido().getEje()+"<br/>"+saltoDeLinea);
				cuerpoEmail.append("</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>");
							
				
			//se construye la tabla de las entregas
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td>"+saltoDeLinea);
					cuerpoEmail.append("<table class=\"tabla\" width=\"100%\">"+saltoDeLinea);
						//titulo de la tabla
						cuerpoEmail.append("<tr>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"tituloSecundario\" colspan=\"7\">Entregas</td>"+saltoDeLinea);
						cuerpoEmail.append("</tr>"+saltoDeLinea);
						
						cuerpoEmail.append("<tr>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"5%\">No.</td>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"30%\">Direcci�n de Despacho</td>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"16%\">Fecha Entrega</td>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"10%\">Cantidad</td>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"13%\">Ancho Rollo</td>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"13%\">Ancho Hoja</td>"+saltoDeLinea);
							cuerpoEmail.append("<td class=\"headerTabla\" width=\"13%\">Largo Hoja</td>"+saltoDeLinea);
						cuerpoEmail.append("</tr>");
							
						entregasProducto = BusinessDelegatorView.findByPropertyEntregasPedido(TsccvEntregasPedidoDAO.PRODPED, prodPedVO.getTsccvProductosPedido().getIdPrpe());
						
				//se recorre el listado de entregas de cada productos pedido
				for(TsccvEntregasPedido entrePed : entregasProducto){
						cuerpoEmail.append("<tr>"+saltoDeLinea);
							cuerpoEmail.append("<td>"+numEntrega+"</td>");
							cuerpoEmail.append("<td>"+entrePed.getDesdir()+"</td>");
							cuerpoEmail.append("<td>"+entrePed.getFechaEntrega()+"</td>");
							cuerpoEmail.append("<td>"+entrePed.getCantidad()+"</td>");
							cuerpoEmail.append("<td>"+((entrePed.getAnchoRollo() != null && !entrePed.getAnchoRollo().equals("")) ? entrePed.getAnchoRollo()+" cm" : "")+"</td>");
							cuerpoEmail.append("<td>"+((entrePed.getAnchoHojas() != null && !entrePed.getAnchoHojas().equals("")) ? entrePed.getAnchoHojas()+" cm" : "")+"</td>");
							cuerpoEmail.append("<td>"+((entrePed.getLargoHojas() != null && !entrePed.getLargoHojas().equals("")) ? entrePed.getLargoHojas()+" cm" : "")+"</td>");
						cuerpoEmail.append("</tr>"+saltoDeLinea);
					numEntrega++;
				}							
					cuerpoEmail.append("</table>");
				cuerpoEmail.append("</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
			
			numEntrega = 1;
			numProductoPedido++;
			}			
			
			//se adiciona el comentario del pedido
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\" colspan=\"4\">Observaciones</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td colspan=\"4\">"+(pedido.getObservaciones() != null && !pedido.getObservaciones().equals("") ? pedido.getObservaciones() : "Ninguna")+"</td>");
			cuerpoEmail.append("</tr>"+saltoDeLinea);
		cuerpoEmail.append("</table>"+saltoDeLinea);
		
		validarDestinatarios();
	}
	
	
	//metodo encargado de validar los emails de los destinatarios y en caso de no tenerlo agrega la anotacion respectiva 
	public void validarDestinatarios() throws BMBaseException, Exception{
		boolean faltaEmail = false;
		
		destinatarios = new Vector<String>(0);
		destinatariosInternos= new Vector<String>(0);
		
		if(!pedido.getTsccvPlanta().getDirele().endsWith(dominioMailInterno)){
			destinatarios.addElement(pedido.getTsccvPlanta().getDirele());//se adiciona email servicliente (planta)
		}
		else{
			destinatariosInternos.addElement(pedido.getTsccvPlanta().getDirele());//se adiciona email servicliente (planta)
		}
				
		cuerpoEmail.append("<table class=\"tabla\" width=\"95%\">"+saltoDeLinea);
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td class=\"tituloPrimario\">Anotaciones Adicionales</td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
			
			cuerpoEmail.append("<tr>"+saltoDeLinea);
				cuerpoEmail.append("<td>"+saltoDeLinea);
					cuerpoEmail.append("<ul>"+saltoDeLinea);
			if(pedido.getTsccvUsuario().getEmail() != null && !pedido.getTsccvUsuario().getEmail().equals("")){
				if(!pedido.getTsccvUsuario().getEmail().endsWith(dominioMailInterno)){
					destinatarios.addElement(pedido.getTsccvUsuario().getEmail());//se adiciona email usuario que realiza el pedido
				}
				else{
					destinatariosInternos.addElement(pedido.getTsccvUsuario().getEmail());//se adiciona email usuario que realiza el pedido
				}
			}
			else{				
						cuerpoEmail.append("<li>No se puede enviar el correo de confirmaci�n al Usuario por que no se dispone de email.</li>");				
				faltaEmail = true;
			}
			
			TsccvUsuarioLogic tsccvUsuarioLogic = new TsccvUsuarioLogic();
			List<TsccvUsuario> listadoVendedores = tsccvUsuarioLogic.consultarVendedores(pedido.getTsccvEmpresas().getIdEmpr(), pedido.getTsccvPlanta().getCodpla());
			if(listadoVendedores != null){
				for(TsccvUsuario vendedor : listadoVendedores){
					if(!vendedor.getEmail().endsWith(dominioMailInterno)){
						destinatarios.addElement(vendedor.getEmail());//se adiciona email vendedor
					}
					else{
						destinatariosInternos.addElement(vendedor.getEmail());//se adiciona email vendedor
					}
				}
			}
			else{
						cuerpoEmail.append("<li>No se puede enviar el correo de confirmaci�n a los Vendedores del Cliente por que no se dispone de email.</li>");
				faltaEmail = true;
			}
			
					cuerpoEmail.append("</ul>"+saltoDeLinea);
			if(!faltaEmail){
					cuerpoEmail.append("Ninguna");
				faltaEmail = true;
			}
				cuerpoEmail.append("<td>"+saltoDeLinea);
			cuerpoEmail.append("</tr>"+saltoDeLinea);
		cuerpoEmail.append("</table>");
	}
	
	//metodo encargado de integrar el cuerpo del email y la plantilla
	public String getMensaje() throws BMBaseException, Exception{
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();		
		//String ruta = "http://74.208.147.70/"+Utilities.DIRIMG_PLANTILLAEMAIL; 
		//String ruta = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/"+request.getContextPath()+Utilities.DIRIMG_PLANTILLAEMAIL;
		String ruta = request.getScheme()+"://"+request.getServerName()+"/"+request.getContextPath()+Utilities.DIRIMG_PLANTILLAEMAIL;
		
		ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		String plantilla = null;
		
		//se contruye la plantilla
		plantilla="<html>"+saltoDeLinea+
					"<head>"+saltoDeLinea+
						"<title></title>"+saltoDeLinea+
						//"<link rel=\"stylesheet\" type=\"text/css\" href=\""+ruta+archivoPropiedades.getProperty("NOMBRE_ESTILO_PLANTILLA")+"\"/>"+saltoDeLinea+
						
						//se diciona el estilo ya que por medio de una referncia no es posible
						"<style type=\"text/css\">"+
							".tituloPrimario{"+
								"width: 100%;"+
								"background: #99B3CC;"+
								"text-align: center;"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-weight: bold;"+
								"font-size: 14px;	"+
								"text-transform: uppercase;"+
								"height: 25px;"+
								"color: white;"+
							"}"+
							
							".tituloSecundario{"+
								"width: 100%;"+
								"background: #99B3CC;"+
								"text-align: left;"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-weight: bold;"+
								"font-size: 13px;	"+
								"text-transform: capitalize;"+
								"height: 25px;"+
								"color: white;"+
							"}"+
							
							".headerTabla{"+
								"background: #99B3CC;"+
								"color: white;"+
								"text-align: center;"+
								"text-transform: capitalize;"+
							"}"+
							
							".datoTabla{"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-size: 12px;"+
								"text-align: left;"+
							"}"+
							
							".tabla{"+
								"border: 1px"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-size: 12px;"+
								"text-align: left;"+
							"}"+
						"</style>"+
					"</head>"+saltoDeLinea+
					"<body>"+saltoDeLinea+
						"<table align=\"center\" border=\"0\" width=\"580\" cellspacing=\"0\" cellpadding=\"0\">"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td >"+saltoDeLinea+
									"<img width=\"100%\" style=\"vertical-align:bottom\" src=\""+ruta+archivoPropiedades.getProperty("IMAGEN_SUPERIOR")+"\" alt=\""+archivoPropiedades.getProperty("IMAGEN_SUPERIOR")+"\"/>"+saltoDeLinea+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td align=\"center\" style=\"border-right-style:solid;border-left-style:solid;border-color:#B7BBBF;\">"+saltoDeLinea+
									"<table width=\"100%\"><tr><td>"+
										cuerpoEmail+saltoDeLinea+//se adiciona el cuerpo del email
									"</td></tr></table>"+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td>"+saltoDeLinea+
									"<img width=\"100%\" src=\""+ruta+archivoPropiedades.getProperty("IMAGEN_INFERIOR")+"\" alt=\""+archivoPropiedades.getProperty("IMAGEN_INFERIOR")+"\"/>"+saltoDeLinea+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
						"</table>"+saltoDeLinea+
					"</body>"+saltoDeLinea+
				  "</html>";
		
		return plantilla;
	}	
	
	public String getMensajeSinImagenes() throws BMBaseException, Exception{
		//HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();		
		//String ruta = "http://74.208.147.70/"+Utilities.DIRIMG_PLANTILLAEMAIL; 
		//String ruta = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/"+request.getContextPath()+Utilities.DIRIMG_PLANTILLAEMAIL;
		//String ruta = request.getScheme()+"://"+request.getServerName()+"/"+request.getContextPath()+Utilities.DIRIMG_PLANTILLAEMAIL;
		
		ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		String plantilla = null;
		
		//se contruye la plantilla
		plantilla="<html>"+saltoDeLinea+
					"<head>"+saltoDeLinea+
						"<title></title>"+saltoDeLinea+
						//"<link rel=\"stylesheet\" type=\"text/css\" href=\""+ruta+archivoPropiedades.getProperty("NOMBRE_ESTILO_PLANTILLA")+"\"/>"+saltoDeLinea+
						
						//se diciona el estilo ya que por medio de una referncia no es posible
						"<style type=\"text/css\">"+
							".tituloPrimario{"+
								"width: 100%;"+
								"background: #99B3CC;"+
								"text-align: center;"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-weight: bold;"+
								"font-size: 14px;	"+
								"text-transform: uppercase;"+
								"height: 25px;"+
								"color: white;"+
							"}"+
							
							".tituloSecundario{"+
								"width: 100%;"+
								"background: #99B3CC;"+
								"text-align: left;"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-weight: bold;"+
								"font-size: 13px;	"+
								"text-transform: capitalize;"+
								"height: 25px;"+
								"color: white;"+
							"}"+
							
							".headerTabla{"+
								"background: #99B3CC;"+
								"color: white;"+
								"text-align: center;"+
								"text-transform: capitalize;"+
							"}"+
							
							".datoTabla{"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-size: 12px;"+
								"text-align: left;"+
							"}"+
							
							".tabla{"+
								"border: 1px"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-size: 12px;"+
								"text-align: left;"+
							"}"+
						"</style>"+
					"</head>"+saltoDeLinea+
					"<body>"+saltoDeLinea+
						"<table align=\"center\" border=\"0\" width=\"580\" cellspacing=\"0\" cellpadding=\"0\">"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td style=\"border:solid;border-left-style:solid;background-color:#007CC3;text-align:center;color:white;font-family:Arial;font-weight:bold;font-size:16px\" height=\"60\">"+saltoDeLinea+
									//"<img width=\"100%\" style=\"vertical-align:bottom\" src=\""+ruta+archivoPropiedades.getProperty("IMAGEN_SUPERIOR")+"\" alt=\""+archivoPropiedades.getProperty("IMAGEN_SUPERIOR")+"\"/>"+saltoDeLinea+
									"Smurfit Kappa Cart�n de Colombia - SKCCVirtual"+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td align=\"center\" style=\"border-right-style:solid;border-left-style:solid;border-color:#B7BBBF;\">"+saltoDeLinea+
									"<table width=\"100%\"><tr><td>"+
										cuerpoEmail+saltoDeLinea+//se adiciona el cuerpo del email
									"</td></tr></table>"+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td style=\"border:solid;background-color:#007CC3;text-align:center;color:white;font-family:Arial;font-weight:bold;font-size:11px\">"+saltoDeLinea+
									//"<img width=\"100%\" src=\""+ruta+archivoPropiedades.getProperty("IMAGEN_INFERIOR")+"\" alt=\""+archivoPropiedades.getProperty("IMAGEN_INFERIOR")+"\"/>"+saltoDeLinea+
									"www.smurfitkappa.com.co"+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
						"</table>"+saltoDeLinea+
					"</body>"+saltoDeLinea+
				  "</html>";
		
		return plantilla;
		
	}
	
	//metodo encargado de integrar el cuerpo del email y la plantilla
	public String getMensajeHV(HttpServletRequest request) throws BMBaseException, Exception{
		//HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();		
		//String ruta = "http://201.234.242.134/skccvirtual/plantilla-email/";
		//String ruta="http://localhost/";
		//String ruta = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/"+request.getContextPath()+Utilities.DIRIMG_PLANTILLAEMAIL;
		String ruta = request.getScheme()+"://"+request.getServerName()+"/"+request.getContextPath()+"/plantilla-email/";
		
		ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
		String plantilla = null;
		
		//se contruye la plantilla
		plantilla="<html>"+saltoDeLinea+
					"<head>"+saltoDeLinea+
						"<title></title>"+saltoDeLinea+
						//"<link rel=\"stylesheet\" type=\"text/css\" href=\""+ruta+archivoPropiedades.getProperty("NOMBRE_ESTILO_PLANTILLA")+"\"/>"+saltoDeLinea+
						
						//se diciona el estilo ya que por medio de una referncia no es posible
						"<style type=\"text/css\">"+
							".tituloPrimario{"+
								"width: 100%;"+
								"background: #99B3CC;"+
								"text-align: center;"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-weight: bold;"+
								"font-size: 14px;	"+
								"text-transform: uppercase;"+
								"height: 25px;"+
								"color: white;"+
							"}"+
							
							".tituloSecundario{"+
								"width: 100%;"+
								"background: #99B3CC;"+
								"text-align: left;"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-weight: bold;"+
								"font-size: 13px;	"+
								"text-transform: capitalize;"+
								"height: 25px;"+
								"color: white;"+
							"}"+
							
							".headerTabla{"+
								"background: #99B3CC;"+
								"color: white;"+
								"text-align: center;"+
								"text-transform: capitalize;"+
							"}"+
							
							".datoTabla{"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-size: 12px;"+
								"text-align: left;"+
							"}"+
							
							".tabla{"+
								"border: 1px"+
								"font-family: Verdana, Arial, Sans-Serif;"+
								"font-size: 12px;"+
								"text-align: left;"+
							"}"+
						"</style>"+
					"</head>"+saltoDeLinea+
					"<body>"+saltoDeLinea+
						"<table align=\"center\" border=\"0\" width=\"580\" cellspacing=\"0\" cellpadding=\"0\" >"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td>"+saltoDeLinea+
									"<img width=\"100%\" style=\"vertical-align:bottom\" src=\""+ruta+archivoPropiedades.getProperty("IMAGEN_SUPERIOR")+"\" alt=\""+archivoPropiedades.getProperty("IMAGEN_SUPERIOR")+"\"/>"+saltoDeLinea+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td style=\"border-right-style:solid;border-left-style:solid;border-color:#B7BBBF;\">"+saltoDeLinea+
									"<table width=\"100%\"><tr><td>"+
									cuerpoEmail+saltoDeLinea+//se adiciona el cuerpo del email
									"</td></tr></table>"+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
							"<tr>"+saltoDeLinea+
								"<td>"+saltoDeLinea+
									"<img width=\"100%\" src=\""+ruta+archivoPropiedades.getProperty("IMAGEN_INFERIOR")+"\" alt=\""+archivoPropiedades.getProperty("IMAGEN_INFERIOR")+"\"/>"+saltoDeLinea+
								"</td>"+saltoDeLinea+
							"</tr>"+saltoDeLinea+
						"</table>"+saltoDeLinea+
					"</body>"+saltoDeLinea+
				  "</html>";
		
		return plantilla;
	}	
	
	public void textoMensajeConfirmacionUsuarioHV(String nombre){
		/*cuerpoEmail.append("<br><br>Se�or<br>");
		cuerpoEmail.append("<b>"+nombre+"</b>"+"<br><br>");
		cuerpoEmail.append("La Ciudad<br><br>");
		cuerpoEmail.append("Asunto: Hoja de Vida<br><br>");
		cuerpoEmail.append("Cordial Saludo<br><br>");
		cuerpoEmail.append("<p style=\"text-align:justify;\"> Smurfit Kappa Cart&oacute;n de Colombia agradece su inter&eacute;s en presentar a nuestra consideraci&oacute;n ");
		cuerpoEmail.append("su hoja de vida. Tan pronto como sea requerido un perfil como el suyo, la empresa se pondr&aacute; en contacto ");
		cuerpoEmail.append("con usted.</p><br><br><br>");
		cuerpoEmail.append("Cordialmente <br><br><br>");
		cuerpoEmail.append("<b>Departamento de Personal</b><p>");
		*/
		try {
			ArchivoPropiedades archivo= new ArchivoPropiedades();
			cuerpoEmail.append(archivo.getProperty("texto_mail_hv_1"));
			cuerpoEmail.append("<b>"+nombre+"</b>"+"<br><br>");
			cuerpoEmail.append(archivo.getProperty("texto_mail_hv_2"));
			cuerpoEmail.append(archivo.getProperty("texto_mail_hv_3"));
			cuerpoEmail.append(archivo.getProperty("texto_mail_hv_4"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws Exception{
		
	}


	public Vector<String> getDestinatariosInternos() {
		return destinatariosInternos;
	}


	public void setDestinatariosInternos(Vector<String> destinatariosInternos) {
		this.destinatariosInternos = destinatariosInternos;
	}
}
