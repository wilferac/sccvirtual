package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.vo.DetalleGrupoEmpresaVO;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvEmpresasLogic {
    public List<TsccvEmpresas> getTsccvEmpresas() throws Exception;

    public void saveTsccvEmpresas(String activo, String codMas, String codSap,
        String codVpe, Date fechaCreacion, Integer idEmpr, String nombre,
        Integer idGrue_TsccvGrupoEmpresa, List<DetalleGrupoEmpresaVO> listadoDetalleEmpresaVO) throws Exception;

    public void deleteTsccvEmpresas(Integer idEmpr) throws Exception;

    public void updateTsccvEmpresas(String activo, String codMas,
        String codSap, String codVpe, Date fechaCreacion, Integer idEmpr,
        String nombre, Integer idGrue_TsccvGrupoEmpresa, List<DetalleGrupoEmpresaVO> listadoDetalleEmpresaVO)
        throws Exception;

    public TsccvEmpresas getTsccvEmpresas(Integer idEmpr)
        throws Exception;

    public List<TsccvEmpresas> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvEmpresas> findPageTsccvEmpresas(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTsccvEmpresas() throws Exception;
    
    public Boolean empresaIsRegistrada(String nombre, String codSap, String codMas, String codVpe) throws Exception;
    
    public List<TsccvEmpresas> consularEmpresas(String activo, String codMas, String codSap,
            String codVpe, Date fechaCreacion, Integer idEmpr, String nombre,
            Integer idGrue_TsccvGrupoEmpresa) throws Exception;
    
    public void inactivarEmpresa(Integer idEmpr) throws Exception;
    
    public List<TsccvEmpresas> buscarEmpresa(String codMas, String codSap,
            String codVpe, String nombre, Integer idGrue_TsccvGrupoEmpresa, Object manageBeanLlmado) throws Exception;
}
