package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;

import java.math.BigDecimal;

import java.util.*;


public interface IBvpDetPedLogic {
    public List<BvpDetPed> getBvpDetPed() throws Exception;

    public void saveBvpDetPed(String planta, String numped, String itmped,
        String codcli, String codscc, String codpro, String tippro,
        String despro, String fecsol, String fecdsp, String estado,
        String undmed, String cntped, String cntdsp, String ordcom)
        throws Exception;

    public void deleteBvpDetPed(String planta, String numped, String itmped,
        String codcli, String codscc, String codpro, String tippro,
        String despro, String fecsol, String fecdsp, String estado,
        String undmed, String cntped, String cntdsp, String ordcom)
        throws Exception;

    public void updateBvpDetPed(String planta, String numped, String itmped,
        String codcli, String codscc, String codpro, String tippro,
        String despro, String fecsol, String fecdsp, String estado,
        String undmed, String cntped, String cntdsp, String ordcom)
        throws Exception;

    public BvpDetPed getBvpDetPed(BvpDetPedId id) throws Exception;

    public List<BvpDetPed> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<BvpDetPed> findPageBvpDetPed(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberBvpDetPed() throws Exception;
    
    public List<BvpDetPed> consultarPedidos(String numped, String ordcom,
            TsccvEmpresas tsccvEmpresas, Date fechaInicial, Date fechaFinal, String codProdCli, TsccvPlanta tsccvPlanta, 
            String nombreColumna, boolean ascendente) throws Exception;
}
