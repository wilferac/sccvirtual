package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.utilities.Utilities;

import java.io.File;
import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class BvpDetPedLogic implements IBvpDetPedLogic {
    public List<BvpDetPed> getBvpDetPed() throws Exception {
        List<BvpDetPed> list = new ArrayList<BvpDetPed>();

        try {
            list = JPADaoFactory.getInstance().getBvpDetPedDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveBvpDetPed(String planta, String numped, String itmped,
        String codcli, String codscc, String codpro, String tippro,
        String despro, String fecsol, String fecdsp, String estado,
        String undmed, String cntped, String cntdsp, String ordcom)
        throws Exception {
        BvpDetPed entity = null;

        try {
            if (planta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "planta");
            }

            if ((planta != null) &&
                    (Utilities.checkWordAndCheckWithlength(planta, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "planta");
            }

            if (numped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numped");
            }

            if ((numped != null) &&
                    (Utilities.checkWordAndCheckWithlength(numped, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numped");
            }

            if (itmped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmped");
            }

            if ((itmped != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmped, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmped");
            }

            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (codscc == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codscc");
            }

            if ((codscc != null) &&
                    (Utilities.checkWordAndCheckWithlength(codscc, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codscc");
            }

            if (codpro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpro");
            }

            if ((codpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpro, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpro");
            }

            if (tippro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "tippro");
            }

            if ((tippro != null) &&
                    (Utilities.checkWordAndCheckWithlength(tippro, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "tippro");
            }

            if (despro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "despro");
            }

            if ((despro != null) &&
                    (Utilities.checkWordAndCheckWithlength(despro, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despro");
            }

            if (fecsol == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecsol");
            }

            if ((fecsol != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecsol, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecsol");
            }

            if (fecdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecdsp");
            }

            if ((fecdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecdsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecdsp");
            }

            if (estado == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "estado");
            }

            if ((estado != null) &&
                    (Utilities.checkWordAndCheckWithlength(estado, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "estado");
            }

            if (undmed == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "undmed");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            if (cntped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntped");
            }

            if ((cntped != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntped, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntped");
            }

            if (cntdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntdsp");
            }

            if ((cntdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntdsp, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntdsp");
            }

            if (ordcom == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "ordcom");
            }

            if ((ordcom != null) &&
                    (Utilities.checkWordAndCheckWithlength(ordcom, 15) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ordcom");
            }

            BvpDetPedId idClass = new BvpDetPedId();
            idClass.setPlanta(planta);
            idClass.setNumped(numped);
            idClass.setItmped(itmped);
            idClass.setCodcli(codcli);
            idClass.setCodscc(codscc);
            idClass.setCodpro(codpro);
            idClass.setTippro(tippro);
            idClass.setDespro(despro);
            idClass.setFecsol(fecsol);
            idClass.setFecdsp(fecdsp);
            idClass.setEstado(estado);
            idClass.setUndmed(undmed);
            idClass.setCntped(cntped);
            idClass.setCntdsp(cntdsp);
            idClass.setOrdcom(ordcom);

            entity = getBvpDetPed(idClass);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }

            entity = new BvpDetPed();

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getBvpDetPedDAO().save(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteBvpDetPed(String planta, String numped, String itmped,
        String codcli, String codscc, String codpro, String tippro,
        String despro, String fecsol, String fecdsp, String estado,
        String undmed, String cntped, String cntdsp, String ordcom)
        throws Exception {
        BvpDetPed entity = null;

        if (planta == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (numped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (itmped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (codcli == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (codscc == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (codpro == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (tippro == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (despro == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (fecsol == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (fecdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (estado == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (undmed == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (cntped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (cntdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (ordcom == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        BvpDetPedId idClass = new BvpDetPedId();
        idClass.setPlanta(planta);
        idClass.setNumped(numped);
        idClass.setItmped(itmped);
        idClass.setCodcli(codcli);
        idClass.setCodscc(codscc);
        idClass.setCodpro(codpro);
        idClass.setTippro(tippro);
        idClass.setDespro(despro);
        idClass.setFecsol(fecsol);
        idClass.setFecdsp(fecdsp);
        idClass.setEstado(estado);
        idClass.setUndmed(undmed);
        idClass.setCntped(cntped);
        idClass.setCntdsp(cntdsp);
        idClass.setOrdcom(ordcom);

        entity = getBvpDetPed(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getBvpDetPedDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateBvpDetPed(String planta, String numped, String itmped,
        String codcli, String codscc, String codpro, String tippro,
        String despro, String fecsol, String fecdsp, String estado,
        String undmed, String cntped, String cntdsp, String ordcom)
        throws Exception {
        BvpDetPed entity = null;

        try {
            if (planta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "planta");
            }

            if ((planta != null) &&
                    (Utilities.checkWordAndCheckWithlength(planta, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "planta");
            }

            if (numped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numped");
            }

            if ((numped != null) &&
                    (Utilities.checkWordAndCheckWithlength(numped, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numped");
            }

            if (itmped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmped");
            }

            if ((itmped != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmped, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmped");
            }

            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (codscc == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codscc");
            }

            if ((codscc != null) &&
                    (Utilities.checkWordAndCheckWithlength(codscc, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codscc");
            }

            if (codpro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpro");
            }

            if ((codpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpro, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpro");
            }

            if (tippro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "tippro");
            }

            if ((tippro != null) &&
                    (Utilities.checkWordAndCheckWithlength(tippro, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "tippro");
            }

            if (despro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "despro");
            }

            if ((despro != null) &&
                    (Utilities.checkWordAndCheckWithlength(despro, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despro");
            }

            if (fecsol == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecsol");
            }

            if ((fecsol != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecsol, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecsol");
            }

            if (fecdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecdsp");
            }

            if ((fecdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecdsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecdsp");
            }

            if (estado == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "estado");
            }

            if ((estado != null) &&
                    (Utilities.checkWordAndCheckWithlength(estado, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "estado");
            }

            if (undmed == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "undmed");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            if (cntped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntped");
            }

            if ((cntped != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntped, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntped");
            }

            if (cntdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntdsp");
            }

            if ((cntdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntdsp, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntdsp");
            }

            if (ordcom == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "ordcom");
            }

            if ((ordcom != null) &&
                    (Utilities.checkWordAndCheckWithlength(ordcom, 15) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ordcom");
            }

            BvpDetPedId idClass = new BvpDetPedId();
            idClass.setPlanta(planta);
            idClass.setNumped(numped);
            idClass.setItmped(itmped);
            idClass.setCodcli(codcli);
            idClass.setCodscc(codscc);
            idClass.setCodpro(codpro);
            idClass.setTippro(tippro);
            idClass.setDespro(despro);
            idClass.setFecsol(fecsol);
            idClass.setFecdsp(fecdsp);
            idClass.setEstado(estado);
            idClass.setUndmed(undmed);
            idClass.setCntped(cntped);
            idClass.setCntdsp(cntdsp);
            idClass.setOrdcom(ordcom);

            entity = getBvpDetPed(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getBvpDetPedDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public BvpDetPed getBvpDetPed(BvpDetPedId id) throws Exception {
        BvpDetPed entity = null;

        try {
            entity = JPADaoFactory.getInstance().getBvpDetPedDAO().findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("BvpDetPed"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<BvpDetPed> findPageBvpDetPed(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<BvpDetPed> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getBvpDetPedDAO()
                                  .findPageBvpDetPed(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("BvpDetPed"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberBvpDetPed() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getBvpDetPedDAO()
                                  .findTotalNumberBvpDetPed();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("BvpDetPed Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<BvpDetPed> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<BvpDetPed> list = new ArrayList<BvpDetPed>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getBvpDetPedDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    
    //metodo agregado para consultar los pedidos de acuerdo a la informacion ingresada
    public List<BvpDetPed> consultarPedidos(String numped, String ordcom,
        TsccvEmpresas tsccvEmpresas, Date fechaInicial, Date fechaFinal, String codProdCli, TsccvPlanta tsccvPlanta, 
        String nombreColumna, boolean ascendente) throws Exception{
    	
    	BvpDetPed entity = null;
        List<BvpDetPed> registros = null;
        String carpetaPlanta=null;
        
    	try {
    		entity = new BvpDetPed();
    		BvpDetPedId idClass = new BvpDetPedId();
            idClass.setNumped(numped);
            idClass.setCodcli(tsccvEmpresas.getCodVpe());//codigo cliente
            idClass.setOrdcom(ordcom);
            idClass.setCodpro(codProdCli);//codigo producto cliente
            idClass.setPlanta(tsccvPlanta.getCodpla());
            idClass.setTippro(tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString());

            entity.setId(idClass);
	    	
    		registros = JPADaoFactory.getInstance().getBvpDetPedDAO().consultarPedidos(entity, fechaInicial, fechaFinal, 
							    						(nombreColumna != null && !nombreColumna.equals("")) ? nombreColumna : "fecsol", 
														(nombreColumna != null && !nombreColumna.equals("")) ? ascendente : false);
    		
    		
    		if(registros != null && registros.size() > 0){
	    		ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
	    		String rutaRC = archivoPropiedades.getProperty("RUTA_REPORTE_CALIDAD");
	    		String nombreArchivo = null;
	    		
	    		carpetaPlanta= obtenerCarpetaPlanta(tsccvPlanta.getCodpla());	
	    		
	    		for(BvpDetPed detPed : registros){
	    			//se crea el archivo y se verifica si existe
	    			
	    			nombreArchivo = detPed.getId().getNumped()+detPed.getId().getItmped();
	    			//Eliminamos los ceros del nombre del archivo
	    			while (nombreArchivo.charAt(0) == '0'){
	    				nombreArchivo = nombreArchivo.substring(1);
	    			}
	    			
	    			File archivo = new File(rutaRC +carpetaPlanta+"/"+ nombreArchivo+".pdf");
	    			//TODO ELIMINAR SYSOU
	    			System.out.println("archivo que busca: "+rutaRC +carpetaPlanta+ "/"+nombreArchivo+".pdf");
	    			if(archivo.canRead()){
	    				//se le adiciona la clave RC al nombre del archivo para al momento de descargarlo identificarlo como un reporte de calidad
	    				//y colocar la ruta respectiva
	    				detPed.getId().setCodcli(Utilities.RUTA_SERVER_REPORTES_CALIDAD+carpetaPlanta+"/"+nombreArchivo+".pdf");
	    				//System.out.println("Y LO ENCUENTRA!!!");
	    			}
	    			else{
		    			//se utiliza la propiedad codpro de BvpDetPed para identificar en la jspx si el archivo existe o no
		    			//si el archivo no existe se deja la codpro como null si no se deja setea el nombre del archivo
	    				detPed.getId().setCodcli(null);
	    				//System.out.println("Y NO LO ENCUENTRA!!!");
	    			}
	    		}    		
    		}    		
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
            
        }
        
        return registros;
    }
    
    private String obtenerCarpetaPlanta(String codPla){
    	if(codPla.equals("1")){
    		return "Barranquilla";
    	}
    	else if(codPla.equals("2")){
    		return "Bogota";
    	}
    	else if(codPla.equals("3")){
    		return "Cali";
    	}
    	else if(codPla.equals("4")){
    		return "Medellin";
    	}
    	else if(codPla.equals("5")){
    		return "Cali1";
    	}
    	return "";
    }
}
