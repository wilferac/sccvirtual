package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;

import java.math.BigDecimal;

import java.util.*;


public interface ISapDetPedLogic {
    public List<SapDetPed> getSapDetPed() throws Exception;

    public void saveSapDetPed(String numped, String itmped, String subitm,
        String codcli, String despro, String cntped, String undmed,
        String fecsol, String fecdsp, String codpro, String estado,
        String ordcom, String cntdsp, String cntkgs, String arollos,
        String ahojas, String lhojas, String arollo) throws Exception;

    public void deleteSapDetPed(String numped, String itmped, String subitm,
        String codcli, String despro, String cntped, String undmed,
        String fecsol, String fecdsp, String codpro, String estado,
        String ordcom, String cntdsp, String cntkgs, String arollos,
        String ahojas, String lhojas, String arollo) throws Exception;

    public void updateSapDetPed(String numped, String itmped, String subitm,
        String codcli, String despro, String cntped, String undmed,
        String fecsol, String fecdsp, String codpro, String estado,
        String ordcom, String cntdsp, String cntkgs, String arollos,
        String ahojas, String lhojas, String arollo) throws Exception;

    public SapDetPed getSapDetPed(SapDetPedId id) throws Exception;

    public List<SapDetPed> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<SapDetPed> findPageSapDetPed(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberSapDetPed() throws Exception;

    public List<SapDetPed> consultarPedidos(String numped, String ordcom,
    		TsccvEmpresas tsccvEmpresas, Date fechaInicial, Date fechaFinal, String codProdCli, String filtroH, String nombreColumna, boolean ascendente) throws Exception;
}
