package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class TbmBaseErroresLogic implements ITbmBaseErroresLogic {
    public List<TbmBaseErrores> getTbmBaseErrores() throws Exception {
        List<TbmBaseErrores> list = new ArrayList<TbmBaseErrores>();

        try {
            list = JPADaoFactory.getInstance().getTbmBaseErroresDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveTbmBaseErrores(String activo, Long codigo,
        String descripcion, Date fechaCreacion, Long idErr, String solucion)
        throws Exception {
        TbmBaseErrores entity = null;

        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "activo");
            }

            if ((codigo != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        codigo, 18, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codigo");
            }

            if (descripcion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "descripcion");
            }

            if ((descripcion != null) &&
                    (Utilities.checkWordAndCheckWithlength(descripcion, 120) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "descripcion");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "fechaCreacion");
            }

            if (idErr == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idErr");
            }

            if ((idErr != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idErr, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idErr");
            }

            if (solucion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "solucion");
            }

            if ((solucion != null) &&
                    (Utilities.checkWordAndCheckWithlength(solucion, 120) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "solucion");
            }

            entity = getTbmBaseErrores(idErr);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }

            entity = new TbmBaseErrores();

            entity.setActivo(activo);
            entity.setCodigo(codigo);
            entity.setDescripcion(descripcion);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdErr(idErr);
            entity.setSolucion(solucion);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTbmBaseErroresDAO().save(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTbmBaseErrores(Long idErr) throws Exception {
        TbmBaseErrores entity = null;

        if (idErr == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        entity = getTbmBaseErrores(idErr);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTbmBaseErroresDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTbmBaseErrores(String activo, Long codigo,
        String descripcion, Date fechaCreacion, Long idErr, String solucion)
        throws Exception {
        TbmBaseErrores entity = null;

        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "activo");
            }

            if ((codigo != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        codigo, 18, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codigo");
            }

            if (descripcion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "descripcion");
            }

            if ((descripcion != null) &&
                    (Utilities.checkWordAndCheckWithlength(descripcion, 120) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "descripcion");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "fechaCreacion");
            }

            if (idErr == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idErr");
            }

            if ((idErr != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idErr, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idErr");
            }

            if (solucion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "solucion");
            }

            if ((solucion != null) &&
                    (Utilities.checkWordAndCheckWithlength(solucion, 120) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "solucion");
            }

            entity = getTbmBaseErrores(idErr);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setActivo(activo);
            entity.setCodigo(codigo);
            entity.setDescripcion(descripcion);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdErr(idErr);
            entity.setSolucion(solucion);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTbmBaseErroresDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TbmBaseErrores getTbmBaseErrores(Long idErr)
        throws Exception {
        TbmBaseErrores entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseErroresDAO()
                                  .findById(idErr);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseErrores"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TbmBaseErrores> findPageTbmBaseErrores(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<TbmBaseErrores> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseErroresDAO()
                                  .findPageTbmBaseErrores(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseErrores"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTbmBaseErrores() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseErroresDAO()
                                  .findTotalNumberTbmBaseErrores();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseErrores Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TbmBaseErrores> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TbmBaseErrores> list = new ArrayList<TbmBaseErrores>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTbmBaseErroresDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
}
