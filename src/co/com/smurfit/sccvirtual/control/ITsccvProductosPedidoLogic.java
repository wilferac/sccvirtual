package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvProductosPedidoLogic {
    public List<TsccvProductosPedido> getTsccvProductosPedido()
        throws Exception;

    /*public void saveTsccvProductosPedido(String activo, Integer cantidadTotal,
        String codCli, String codScc, String codpla, String despro,
        String diamtr, String eje, Date fechaCreacion,
        Date fechaPrimeraEntrega, Integer idPrpe, String moneda,
        String ordenCompra, String precio, String undmed,
        Integer idPedi_TsccvPedidos) throws Exception;
*/
    public void deleteTsccvProductosPedido(Integer idPrpe)
        throws Exception;

    public void updateTsccvProductosPedido(String activo,
        Integer cantidadTotal, String codCli, String codScc, String codpla,
        String despro, String diamtr, String eje, Date fechaCreacion,
        Date fechaPrimeraEntrega, Integer idPrpe, String moneda,
        String ordenCompra, String precio, String undmed,
        Integer idPedi_TsccvPedidos, List<ProductosPedidoVO> listadoProductosPedido) throws Exception;

    public TsccvProductosPedido getTsccvProductosPedido(Integer idPrpe)
        throws Exception;

    public List<TsccvProductosPedido> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvProductosPedido> findPageTsccvProductosPedido(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvProductosPedido() throws Exception;
    
    public List<TsccvProductosPedido> findByProperty(String propertyName,final Object value) throws Exception;
    
    public List<ProductosPedidoVO> consultarProductosPedido(Integer idPedi) throws Exception;
}
