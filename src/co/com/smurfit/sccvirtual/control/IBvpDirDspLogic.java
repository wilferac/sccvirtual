package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;

import java.math.BigDecimal;

import java.util.*;


public interface IBvpDirDspLogic {
    public List<BvpDirDsp> getBvpDirDsp() throws Exception;

    public void saveBvpDirDsp(String codcli, String coddir, String codpla,
        String desdir, String numfax, String desprv) throws Exception;

    public void deleteBvpDirDsp(String codcli, String coddir, String codpla,
        String desdir, String numfax, String desprv) throws Exception;

    public void updateBvpDirDsp(String codcli, String coddir, String codpla,
        String desdir, String numfax, String desprv) throws Exception;

    public BvpDirDsp getBvpDirDsp(BvpDirDspId id) throws Exception;

    public List<BvpDirDsp> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<BvpDirDsp> findPageBvpDirDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberBvpDirDsp() throws Exception;
    
    public List<BvpDirDsp> findByProperty(String propertyName,final Object value) throws Exception;
    
    public List<BvpDirDsp> consultarDireccionesClientePlanta(String codCli, String codPla) throws Exception;
}
