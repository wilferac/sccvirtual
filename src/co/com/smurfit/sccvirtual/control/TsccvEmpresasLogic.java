package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.dao.TsccvDetalleGrupoEmpresaDAO;
import co.com.smurfit.dataaccess.dao.TsccvEmpresasDAO;
import co.com.smurfit.dataaccess.dao.TsccvGrupoEmpresaDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.presentation.backEndBeans.TsccvUsuarioView;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.vo.DetalleGrupoEmpresaVO;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class TsccvEmpresasLogic implements ITsccvEmpresasLogic {
    public List<TsccvEmpresas> getTsccvEmpresas() throws Exception {
        List<TsccvEmpresas> list = new ArrayList<TsccvEmpresas>();

        try {
            list = JPADaoFactory.getInstance().getTsccvEmpresasDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveTsccvEmpresas(String activo, String codMas, String codSap,
        String codVpe, Date fechaCreacion, Integer idEmpr, String nombre,
        Integer idGrue_TsccvGrupoEmpresa, List<DetalleGrupoEmpresaVO> listadoDetalleEmpresaVO) throws BMBaseException, Exception {
        TsccvEmpresas entity = null;

        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Activo");
            }
            
            if ((codMas != null) &&
                    (Utilities.checkWordAndCheckWithlength(codMas, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "C�digo MAS");
            }

            if ((codSap != null) &&
                    (Utilities.checkWordAndCheckWithlength(codSap, 10) == false)) {                
            	throw new Exception(ExceptionMessages.VARIABLE_LENGTH +"C�digo SAP");
            }

            if ((codVpe != null) &&
                    (Utilities.checkWordAndCheckWithlength(codVpe, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "C�digo VPE");
            }
            
            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creaci�n");
            }

            /*if (idEmpr == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Id Empresa");
            }*/

            if (nombre == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Nombre");
            }

            if ((nombre != null) &&
                    (Utilities.checkWordAndCheckWithlength(nombre, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Nombre");
            }
            
            TsccvGrupoEmpresa tsccvGrupoEmpresaClass = null;
            if(idGrue_TsccvGrupoEmpresa != null){
            	ITsccvGrupoEmpresaLogic logicTsccvGrupoEmpresa1 = new TsccvGrupoEmpresaLogic();
            	tsccvGrupoEmpresaClass = logicTsccvGrupoEmpresa1.getTsccvGrupoEmpresa(idGrue_TsccvGrupoEmpresa);
	            if (tsccvGrupoEmpresaClass == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL+"Grupo Empresa");
	            }
            }

            /*entity = getTsccvEmpresas(idEmpr);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }*/
            
            
            //Antes de guardar se valida si es una empresa o un grupo de empresas
        	if(listadoDetalleEmpresaVO != null && listadoDetalleEmpresaVO.size() > 0){
        		ITsccvGrupoEmpresaLogic logicTsccvGrupoEmpresa = new TsccvGrupoEmpresaLogic();
        		ITsccvDetalleGrupoEmpresaLogic logicTsccvDetalleGrupoEmpresa = new TsccvDetalleGrupoEmpresaLogic();
        		
        		//Si tiene informacion en el listado es un grupo de empresas
        		//1.Se crea el grupo empresa
        		logicTsccvGrupoEmpresa.saveTsccvGrupoEmpresa(new Date(), null);
        		
        		//2.Despues de crear el grupo se obtiene el ultimo id del grupo
        		Integer idGrupoEmpresa = logicTsccvGrupoEmpresa.consultarUltimoIdGrue();
        		
        		//3.Se crea el detalle del grupo con las empresas selecionadas
        		//Se recorre el listado de las empresas hijas seleccionadas y se insertan en el detalle del grupo con id del grupo respectivo
        		for(DetalleGrupoEmpresaVO objetoDetalleGrupoEmpresaVO: listadoDetalleEmpresaVO){
        			logicTsccvDetalleGrupoEmpresa.saveTsccvDetalleGrupoEmpresa(idGrupoEmpresa, objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr(), 
        					fechaCreacion, objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr(), idGrupoEmpresa);
        		}
        		
        		//4.Una vez creado el grupo y el detalle del grupo se setea el id del grupo a la empresa
        		tsccvGrupoEmpresaClass = logicTsccvGrupoEmpresa.getTsccvGrupoEmpresa(idGrupoEmpresa);
        	}
            
            
            entity = new TsccvEmpresas();

            entity.setActivo(activo);
            entity.setCodMas(codMas);
            //entity.setCodSap(codSap);
            entity.setCodVpe(codVpe);
            
            if(listadoDetalleEmpresaVO == null || listadoDetalleEmpresaVO.size() == 0){//si la empresa no es un grupo se valida que ingrese el Codigo SAP && (Codigo MAS || Codigo VPE)
            	if (codSap == null) {
            		/*
	           		 CODIGO: 35
					 DSECRIPCION: Campo C�digo SAP sin valor
					 SOLUCION: Para las Empresas que no estan constituidas como un grupo es necesario ingresar este c�digo.
	           		 */
            		throw new BMBaseException(35, null);
            	}
            	else{
            		List resultado = JPADaoFactory.getInstance().getTsccvEmpresasDAO().findByCodSap(codSap);
            		if(resultado != null && resultado.size() > 0){
            			throw new Exception("El C�digo SAP ya est� registrado en el sistema");
            		}
            		entity.setCodSap(codSap);
            	}
            	
            	if(codMas == null && codVpe == null){
            		/*
	           		 CODIGO: 36
					 DSECRIPCION: Campo C�digo MAS O VPE sin valor
					 SOLUCION: para las Empresas que no estan constituidas como un grupo es necesario ingresar alguno de estos dos c�digos.
	           		 */
            		throw new BMBaseException(36, null);
            	}
            	/*else if(codMas != null && codVpe != null){
            		CODIGO: 38
					DSECRIPCION: No se puede ingresar el C�digo MAS y el C�digo VPE simultaneamente.
					SOLUCION: Ingrese el C�digo de acuerdo al tipo de producto que necesite la Empresa C�digo MAS para Molinos y C�digo VPE para Corrugado o Sacos.
	           		throw new BMBaseException(38, null);
            	}*/
            	else{
            		/*if(codMas != null){
            			entity.setCodMas(codMas);*/
            			if(empresaIsRegistrada(nombre, codSap, codMas, codVpe)){
            				throw new Exception("El Nombre o la combinaci�n C�digo SAP y C�digo MAS ya est� registrada en el sistema");
            			}
                	/*}
            		else{
            			entity.setCodVpe(codVpe);*/
            			if(empresaIsRegistrada(nombre, codSap, codMas, codVpe)){
            				throw new Exception("El Nombre o la combinaci�n C�digo SAP y C�digo VPE ya est� registrada en el sistema");
            			}
            		//}
            	}
            }
            
            entity.setFechaCreacion(fechaCreacion);
            //entity.setIdEmpr(idEmpr);
            entity.setNombre(nombre);

            entity.setTsccvGrupoEmpresa(tsccvGrupoEmpresaClass);

            EntityManagerHelper.beginTransaction();
            //Comentariamos el llamado inicial
            //JPADaoFactory.getInstance().getTsccvEmpresasDAO().save(entity);
            //Llamamos el nuevo metodo que realiza el registro
            TsccvEmpresasDAO.saveEmpresa(entity);
            EntityManagerHelper.commit();
        } catch (BMBaseException e) {
            EntityManagerHelper.rollback();
            throw e;
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        }finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTsccvEmpresas(Integer idEmpr) throws Exception {
        TsccvEmpresas entity = null;

        if (idEmpr == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL+"Id Empresa");
        }

        List<TsccvDetalleGrupoEmpresa> tsccvDetalleGrupoEmpresas = null;
        List<TsccvPedidos> tsccvPedidos = null;
        List<TsccvUsuario> tsccvUsuarios = null;

        entity = getTsccvEmpresas(idEmpr);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
        	//se adiciona validacion para saber si la empresa es un grupo 
            if(entity.getTsccvGrupoEmpresa() != null){
	        	tsccvDetalleGrupoEmpresas = JPADaoFactory.getInstance()
	                                                     .getTsccvDetalleGrupoEmpresaDAO()
	                                                     .findByProperty(TsccvDetalleGrupoEmpresaDAO.IDGRUE,
	                    entity.getTsccvGrupoEmpresa().getIdGrue(), 0);
	
	            if (Utilities.validationsList(tsccvDetalleGrupoEmpresas) == true) {
	                throw new Exception("No se puede eliminar ya que es un Grupo y tiene empresas asociadas");
	            }
	        }

            tsccvUsuarios = JPADaoFactory.getInstance().getTsccvUsuarioDAO()
                                         .findByProperty("tsccvEmpresas.idEmpr",
                    idEmpr, 0);

            if (Utilities.validationsList(tsccvUsuarios) == true) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild()+"Usuarios");
            }
            
            //se agrega validacion para saber si la empresa tiene pedidos asociados 
            tsccvPedidos = JPADaoFactory.getInstance().getTsccvPedidosDAO()
			            .findByProperty("tsccvEmpresas.idEmpr",
			idEmpr, 0);
			
			if(Utilities.validationsList(tsccvPedidos) == true) {
				throw new Exception(ExceptionManager.getInstance()
			                       					.exceptionDeletingEntityWithChild()+"Pedidos");
			}

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvEmpresasDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTsccvEmpresas(String activo, String codMas,
        String codSap, String codVpe, Date fechaCreacion, Integer idEmpr,
        String nombre, Integer idGrue_TsccvGrupoEmpresa, List<DetalleGrupoEmpresaVO> listadoDetalleEmpresaVO) throws BMBaseException, Exception {
        TsccvEmpresas entity = null;

        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Activo");
            }

            if ((codMas != null) &&
                    (Utilities.checkWordAndCheckWithlength(codMas, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "C�digo MAS");
            }

            /*if (codSap == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "C�digo SAP");
            }*/

            if ((codSap != null) &&
                    (Utilities.checkWordAndCheckWithlength(codSap, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "C�digo SAP");
            }

            if ((codVpe != null) &&
                    (Utilities.checkWordAndCheckWithlength(codVpe, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "C�digo VPE");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creaci�n");
            }

            if (idEmpr == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Id Empresa");
            }

            if (nombre == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Nombre");
            }

            if ((nombre != null) &&
                    (Utilities.checkWordAndCheckWithlength(nombre, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Nombre");
            }            

            TsccvGrupoEmpresa tsccvGrupoEmpresaClass = null;
            if(idGrue_TsccvGrupoEmpresa != null){
            	ITsccvGrupoEmpresaLogic logicTsccvGrupoEmpresa1 = new TsccvGrupoEmpresaLogic();
            	tsccvGrupoEmpresaClass = logicTsccvGrupoEmpresa1.getTsccvGrupoEmpresa(idGrue_TsccvGrupoEmpresa);
	            if (tsccvGrupoEmpresaClass == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL+"Grupo Empresa");
	            }
            }

            entity = getTsccvEmpresas(idEmpr);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }
            
            //Antes de guardar se valida si es una empresa o un grupo de empresas
            if(listadoDetalleEmpresaVO != null && listadoDetalleEmpresaVO.size() > 0){
        		ITsccvGrupoEmpresaLogic logicTsccvGrupoEmpresa = new TsccvGrupoEmpresaLogic();
        		ITsccvDetalleGrupoEmpresaLogic logicTsccvDetalleGrupoEmpresa = new TsccvDetalleGrupoEmpresaLogic();
        		
        		//se consultan el detalle empresa anterior
        		List<TsccvDetalleGrupoEmpresa> listadoDetalleAnterior = logicTsccvDetalleGrupoEmpresa.findByProperty(TsccvDetalleGrupoEmpresaDAO.IDGRUE, idGrue_TsccvGrupoEmpresa);
        		
        		Map idsDetalleNuevos = new HashMap(0);        		
        		//se crea un hashMap con las nuevas empresas del grupo
        		for(DetalleGrupoEmpresaVO objetoDetalleGrupoEmpresaVO : listadoDetalleEmpresaVO){
        			idsDetalleNuevos.put(objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr(), 
        					objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr());
        		}
        		
        		//se recorre el listado del detalle anterior para eliminar las empresas antarios que no estan en la lista de nuevos
        		for(TsccvDetalleGrupoEmpresa objetoDetalleGrupoEmpresa : listadoDetalleAnterior){
        			if(!idsDetalleNuevos.containsKey(objetoDetalleGrupoEmpresa.getTsccvEmpresas().getIdEmpr())){
        				logicTsccvDetalleGrupoEmpresa.deleteTsccvDetalleGrupoEmpresa(idGrue_TsccvGrupoEmpresa, objetoDetalleGrupoEmpresa.getTsccvEmpresas().getIdEmpr());
        			}
        		}
        		
        		//3.Se crea el detalle del grupo con las empresas selecionadas
        		//Se recorre el listado de las empresas hijas seleccionadas y se insertan en el detalle del grupo con id del grupo respectivo
        		for(DetalleGrupoEmpresaVO objetoDetalleGrupoEmpresaVO: listadoDetalleEmpresaVO){
    				logicTsccvDetalleGrupoEmpresa.saveTsccvDetalleGrupoEmpresa(idGrue_TsccvGrupoEmpresa, objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr(), 
        					fechaCreacion, objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr(), idGrue_TsccvGrupoEmpresa);
        		}
        		
        		//4.Una vez creado el grupo y el detalle del grupo se setea el id del grupo a la empresa
        		tsccvGrupoEmpresaClass = logicTsccvGrupoEmpresa.getTsccvGrupoEmpresa(idGrue_TsccvGrupoEmpresa);
        	}
            //si la empresa es un grupo pero no tiene ninguna empresa hija
            else if(entity.getTsccvGrupoEmpresa() != null && (listadoDetalleEmpresaVO == null || listadoDetalleEmpresaVO.size() == 0)){
            	ITsccvDetalleGrupoEmpresaLogic logicTsccvDetalleGrupoEmpresa = new TsccvDetalleGrupoEmpresaLogic();
        		
        		//se consultan el detalle empresa anterior
        		List<TsccvDetalleGrupoEmpresa> listadoDetalleAnterior = logicTsccvDetalleGrupoEmpresa.findByProperty(TsccvDetalleGrupoEmpresaDAO.IDGRUE, idGrue_TsccvGrupoEmpresa);
        		
        		Map idsDetalleNuevos = new HashMap(0);        		
        		//se crea un hashMap con las nuevas empresas del grupo
        		for(DetalleGrupoEmpresaVO objetoDetalleGrupoEmpresaVO : listadoDetalleEmpresaVO){
        			idsDetalleNuevos.put(objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr(), 
        					objetoDetalleGrupoEmpresaVO.getTsccvDetalleGrupoEmpresa().getTsccvEmpresas().getIdEmpr());
        		}
        		
        		//se recorre el listado del detalle anterior para eliminar las empresas antarios que no estan en la lista de nuevos
        		for(TsccvDetalleGrupoEmpresa objetoDetalleGrupoEmpresa : listadoDetalleAnterior){
        			if(!idsDetalleNuevos.containsKey(objetoDetalleGrupoEmpresa.getTsccvEmpresas().getIdEmpr())){
        				logicTsccvDetalleGrupoEmpresa.deleteTsccvDetalleGrupoEmpresa(idGrue_TsccvGrupoEmpresa, objetoDetalleGrupoEmpresa.getTsccvEmpresas().getIdEmpr());
        			}
        		}
            }

            entity.setActivo(activo);
            entity.setCodMas(codMas);
            entity.setCodSap(codSap);
            entity.setCodVpe(codVpe);
            
            if(entity.getTsccvGrupoEmpresa() == null){//si la empresa no es un grupo se valida que ingrese el Codigo SAP && (Codigo MAS || Codigo VPE)
            	if (codSap == null) {
            		/*
            		 CODIGO: 35
					 DSECRIPCION: Campo C�digo SAP sin valor
					 SOLUCION: Para las Empresas que no estan constituidas como un grupo es necesario ingresar este c�digo.
            		 */
            		throw new BMBaseException(35, null);
            	}
            	else{
            		entity.setCodSap(codSap);
            	}
            	
            	if(codMas == null && codVpe == null){
            		throw new BMBaseException(36, null);
            		/*
            		 CODIGO: 36
					 DSECRIPCION: Campo C�digo MAS O VPE sin valor
					 SOLUCION: para las Empresas que no estan constituidas como un grupo es necesario ingresar alguno de estos dos c�digos.
            		 */
            	}
            	/*else if(codMas != null && codVpe != null){
	           		CODIGO: 38
					DSECRIPCION: No se puede ingresar el C�digo MAS y el C�digo VPE simultaneamente.
					SOLUCION: Ingrese el C�digo de acuerdo al tipo de producto que necesite la Empresa C�digo MAS para Molinos y C�digo VPE para Corrugado o Sacos.
	           		 
            		throw new BMBaseException(38, null);
            	}*/
            	/*else{
            		if(codMas != null){
            			entity.setCodMas(codMas);
                	}
            		else{
            			entity.setCodVpe(codVpe);
            		}
            	}*/
            }
            
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdEmpr(idEmpr);
            entity.setNombre(nombre);

            entity.setTsccvGrupoEmpresa(tsccvGrupoEmpresaClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvEmpresasDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (BMBaseException e) {
            EntityManagerHelper.rollback();
            throw e;
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TsccvEmpresas getTsccvEmpresas(Integer idEmpr)
        throws Exception {
        TsccvEmpresas entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvEmpresasDAO()
                                  .findById(idEmpr);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvEmpresas"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TsccvEmpresas> findPageTsccvEmpresas(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<TsccvEmpresas> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvEmpresasDAO()
                                  .findPageTsccvEmpresas(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvEmpresas"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTsccvEmpresas() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvEmpresasDAO()
                                  .findTotalNumberTsccvEmpresas();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvEmpresas Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TsccvEmpresas> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TsccvEmpresas> list = new ArrayList<TsccvEmpresas>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTsccvEmpresasDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    //consulta las empresas con los datos para saber si esta registrada 
    public Boolean empresaIsRegistrada(String nombre, String codSap, String codMas, String codVpe) throws Exception{    	
    	try{
    		return JPADaoFactory.getInstance().getTsccvEmpresasDAO().empresaIsRegistrada(nombre, codSap, codMas, codVpe); 
	    } catch (Exception e) {
	        throw new Exception(e.getMessage());
	    } finally {
	        EntityManagerHelper.closeEntityManager();
	    }
    }
    
    //Metodo utilizado para la consulta por todos los campos
    public List<TsccvEmpresas> consularEmpresas(String activo, String codMas, String codSap,
        String codVpe, Date fechaCreacion, Integer idEmpr, String nombre,
        Integer idGrue_TsccvGrupoEmpresa) throws Exception {
        
    	TsccvEmpresas entity = null;
    	List<TsccvEmpresas> registros = null;
    	
        try {               
            entity = new TsccvEmpresas();

            entity.setActivo(activo);
            entity.setCodMas(codMas);
            entity.setCodSap(codSap);
            entity.setCodVpe(codVpe);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdEmpr(idEmpr);
            entity.setNombre(nombre);

            registros = JPADaoFactory.getInstance().getTsccvEmpresasDAO().consularEmpresas(entity);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return registros;
    }
    
    
    public void inactivarEmpresa(Integer idEmpr) throws Exception {
    	try{    
    		EntityManagerHelper.beginTransaction();
    		JPADaoFactory.getInstance().getTsccvEmpresasDAO().inactivarEmpresa(idEmpr);	
    		EntityManagerHelper.commit();
    	}catch(Exception e){
    		EntityManagerHelper.rollback();
    		throw new Exception(e.getMessage());
    	}
    	finally{    		
    		EntityManagerHelper.closeEntityManager();
    	}
    }
    
    
    //metodo encargado de consultar las empresas por el nombre
    public List<TsccvEmpresas> buscarEmpresa(String codMas, String codSap,
        String codVpe, String nombre, Integer idGrue_TsccvGrupoEmpresa, Object manageBeanLlmado) throws Exception {
    	
    	List<TsccvEmpresas> registros = null;    
    	
        try {    
        	TsccvEmpresas entity = new TsccvEmpresas();
        	
        	entity.setCodMas(codMas);
        	entity.setCodSap(codSap);
        	entity.setCodVpe(codVpe);
        	entity.setNombre(nombre);
        	
        	Boolean pantallaIsAdmin = false;
    		if(manageBeanLlmado instanceof TsccvUsuarioView){
            	pantallaIsAdmin = true;
            }
        	
        	if(idGrue_TsccvGrupoEmpresa != null && !pantallaIsAdmin){
        		TsccvGrupoEmpresa tsccvGrupoEmpresa = new TsccvGrupoEmpresa();
        		tsccvGrupoEmpresa.setIdGrue(idGrue_TsccvGrupoEmpresa);
        		entity.setTsccvGrupoEmpresa(tsccvGrupoEmpresa);
        	}
        	
            registros = JPADaoFactory.getInstance().getTsccvEmpresasDAO().buscarEmpresa(entity, pantallaIsAdmin);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return registros;
    }
}
