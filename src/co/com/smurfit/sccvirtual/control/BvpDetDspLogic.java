package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class BvpDetDspLogic implements IBvpDetDspLogic {
    public List<BvpDetDsp> getBvpDetDsp() throws Exception {
        List<BvpDetDsp> list = new ArrayList<BvpDetDsp>();

        try {
            list = JPADaoFactory.getInstance().getBvpDetDspDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveBvpDetDsp(String planta, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String fecdsp,
        String hordsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp) throws Exception {
        BvpDetDsp entity = null;

        try {
            if (planta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "planta");
            }

            if ((planta != null) &&
                    (Utilities.checkWordAndCheckWithlength(planta, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "planta");
            }

            if (numdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numdsp");
            }

            if ((numdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(numdsp, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numdsp");
            }

            if (itmdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmdsp");
            }

            if ((itmdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmdsp, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmdsp");
            }

            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (numped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numped");
            }

            if ((numped != null) &&
                    (Utilities.checkWordAndCheckWithlength(numped, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numped");
            }

            if (itmped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmped");
            }

            if ((itmped != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmped, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmped");
            }

            if (fecdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecdsp");
            }

            if ((fecdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecdsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecdsp");
            }

            if (hordsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "hordsp");
            }

            if ((hordsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(hordsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "hordsp");
            }

            if (candsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "candsp");
            }

            if ((candsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(candsp, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "candsp");
            }

            if (undmed == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "undmed");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            if (emptra == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "emptra");
            }

            if ((emptra != null) &&
                    (Utilities.checkWordAndCheckWithlength(emptra, 40) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "emptra");
            }

            if (placa == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "placa");
            }

            if ((placa != null) &&
                    (Utilities.checkWordAndCheckWithlength(placa, 15) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "placa");
            }

            if (nomcon == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "nomcon");
            }

            if ((nomcon != null) &&
                    (Utilities.checkWordAndCheckWithlength(nomcon, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "nomcon");
            }

            if (dirdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "dirdsp");
            }

            if ((dirdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(dirdsp, 80) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "dirdsp");
            }

            BvpDetDspId idClass = new BvpDetDspId();
            idClass.setPlanta(planta);
            idClass.setNumdsp(numdsp);
            idClass.setItmdsp(itmdsp);
            idClass.setCodcli(codcli);
            idClass.setNumped(numped);
            idClass.setItmped(itmped);
            idClass.setFecdsp(fecdsp);
            idClass.setHordsp(hordsp);
            idClass.setCandsp(candsp);
            idClass.setUndmed(undmed);
            idClass.setEmptra(emptra);
            idClass.setPlaca(placa);
            idClass.setNomcon(nomcon);
            idClass.setDirdsp(dirdsp);

            entity = getBvpDetDsp(idClass);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }

            entity = new BvpDetDsp();

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getBvpDetDspDAO().save(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteBvpDetDsp(String planta, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String fecdsp,
        String hordsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp) throws Exception {
        BvpDetDsp entity = null;

        if (planta == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (numdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (itmdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (codcli == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (numped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (itmped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (fecdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (hordsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (candsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (undmed == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (emptra == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (placa == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (nomcon == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (dirdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        BvpDetDspId idClass = new BvpDetDspId();
        idClass.setPlanta(planta);
        idClass.setNumdsp(numdsp);
        idClass.setItmdsp(itmdsp);
        idClass.setCodcli(codcli);
        idClass.setNumped(numped);
        idClass.setItmped(itmped);
        idClass.setFecdsp(fecdsp);
        idClass.setHordsp(hordsp);
        idClass.setCandsp(candsp);
        idClass.setUndmed(undmed);
        idClass.setEmptra(emptra);
        idClass.setPlaca(placa);
        idClass.setNomcon(nomcon);
        idClass.setDirdsp(dirdsp);

        entity = getBvpDetDsp(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getBvpDetDspDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateBvpDetDsp(String planta, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String fecdsp,
        String hordsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp) throws Exception {
        BvpDetDsp entity = null;

        try {
            if (planta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "planta");
            }

            if ((planta != null) &&
                    (Utilities.checkWordAndCheckWithlength(planta, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "planta");
            }

            if (numdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numdsp");
            }

            if ((numdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(numdsp, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numdsp");
            }

            if (itmdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmdsp");
            }

            if ((itmdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmdsp, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmdsp");
            }

            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (numped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numped");
            }

            if ((numped != null) &&
                    (Utilities.checkWordAndCheckWithlength(numped, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numped");
            }

            if (itmped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmped");
            }

            if ((itmped != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmped, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmped");
            }

            if (fecdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecdsp");
            }

            if ((fecdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecdsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecdsp");
            }

            if (hordsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "hordsp");
            }

            if ((hordsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(hordsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "hordsp");
            }

            if (candsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "candsp");
            }

            if ((candsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(candsp, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "candsp");
            }

            if (undmed == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "undmed");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            if (emptra == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "emptra");
            }

            if ((emptra != null) &&
                    (Utilities.checkWordAndCheckWithlength(emptra, 40) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "emptra");
            }

            if (placa == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "placa");
            }

            if ((placa != null) &&
                    (Utilities.checkWordAndCheckWithlength(placa, 15) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "placa");
            }

            if (nomcon == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "nomcon");
            }

            if ((nomcon != null) &&
                    (Utilities.checkWordAndCheckWithlength(nomcon, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "nomcon");
            }

            if (dirdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "dirdsp");
            }

            if ((dirdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(dirdsp, 80) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "dirdsp");
            }

            BvpDetDspId idClass = new BvpDetDspId();
            idClass.setPlanta(planta);
            idClass.setNumdsp(numdsp);
            idClass.setItmdsp(itmdsp);
            idClass.setCodcli(codcli);
            idClass.setNumped(numped);
            idClass.setItmped(itmped);
            idClass.setFecdsp(fecdsp);
            idClass.setHordsp(hordsp);
            idClass.setCandsp(candsp);
            idClass.setUndmed(undmed);
            idClass.setEmptra(emptra);
            idClass.setPlaca(placa);
            idClass.setNomcon(nomcon);
            idClass.setDirdsp(dirdsp);

            entity = getBvpDetDsp(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getBvpDetDspDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public BvpDetDsp getBvpDetDsp(BvpDetDspId id) throws Exception {
        BvpDetDsp entity = null;

        try {
            entity = JPADaoFactory.getInstance().getBvpDetDspDAO().findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("BvpDetDsp"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<BvpDetDsp> findPageBvpDetDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<BvpDetDsp> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getBvpDetDspDAO()
                                  .findPageBvpDetDsp(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("BvpDetDsp"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberBvpDetDsp() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getBvpDetDspDAO()
                                  .findTotalNumberBvpDetDsp();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("BvpDetDsp Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<BvpDetDsp> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<BvpDetDsp> list = new ArrayList<BvpDetDsp>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getBvpDetDspDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    //Metodo encargado de consultar los detalle despacho dependiendo de una propiedad
    public List<BvpDetDsp> findByProperty(String propertyName,final Object value) throws Exception{
    	try {
            return JPADaoFactory.getInstance().getBvpDetDspDAO().findByProperty(propertyName, value);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }    
    
    //metodo encrgado de consultar el detalle despacho
    public List<BvpDetDsp> consultarDetalleDespachoBvp(String numPed, String itemPed, String codPla, String codVpe) throws Exception{
    	try {        	
    		BvpDetPed entity = new BvpDetPed();
    		BvpDetPedId bvpDetPedId = new BvpDetPedId();
    		
    		bvpDetPedId.setNumped(numPed);
    		bvpDetPedId.setItmped(itemPed);
    		bvpDetPedId.setPlanta(codPla);
    		bvpDetPedId.setCodcli(codVpe);
    		
    		entity.setId(bvpDetPedId);
    		
    		return JPADaoFactory.getInstance().getBvpDetDspDAO().consultarDetalleDespachoBvp(entity);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
}
