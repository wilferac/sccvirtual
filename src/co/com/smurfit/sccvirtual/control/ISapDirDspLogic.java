package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;

import java.math.BigDecimal;

import java.util.*;


public interface ISapDirDspLogic {
    public List<SapDirDsp> getSapDirDsp() throws Exception;

    public void saveSapDirDsp(String codcli, String coddir, String desdir,
        String numfax, String desprv, String desciu, String despai)
        throws Exception;

    public void deleteSapDirDsp(String codcli, String coddir, String desdir,
        String numfax, String desprv, String desciu, String despai)
        throws Exception;

    public void updateSapDirDsp(String codcli, String coddir, String desdir,
        String numfax, String desprv, String desciu, String despai)
        throws Exception;

    public SapDirDsp getSapDirDsp(SapDirDspId id) throws Exception;

    public List<SapDirDsp> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<SapDirDsp> findPageSapDirDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberSapDirDsp() throws Exception;
    
    public List<SapDirDsp> consultarDireccionesCliente(String codCli) throws Exception;
}
