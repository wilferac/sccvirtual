package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


public interface ITsccvAplicacionesLogic {
    public List<TsccvAplicaciones> getTsccvAplicaciones()
        throws Exception;

    public void saveTsccvAplicaciones(String activo, String descripcion,
        Date fechaCreacion, Long idApli) throws Exception;

    public void deleteTsccvAplicaciones(Long idApli) throws Exception;

    public void updateTsccvAplicaciones(String activo, String descripcion,
        Date fechaCreacion, Long idApli) throws Exception;

    public TsccvAplicaciones getTsccvAplicaciones(Long idApli)
        throws Exception;

    public List<TsccvAplicaciones> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvAplicaciones> findPageTsccvAplicaciones(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvAplicaciones() throws Exception;
}
