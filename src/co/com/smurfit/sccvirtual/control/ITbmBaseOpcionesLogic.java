package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;

import java.math.BigDecimal;

import java.util.*;


public interface ITbmBaseOpcionesLogic {
    public List<TbmBaseOpciones> getTbmBaseOpciones() throws Exception;

    public void saveTbmBaseOpciones(String activo, String descripcion,
        Date fechaCreacion, Long idOpci, String imagenIcono, String nombre,
        String url) throws Exception;

    public void deleteTbmBaseOpciones(Long idOpci) throws Exception;

    public void updateTbmBaseOpciones(String activo, String descripcion,
        Date fechaCreacion, Long idOpci, String imagenIcono, String nombre,
        String url) throws Exception;

    public TbmBaseOpciones getTbmBaseOpciones(Long idOpci)
        throws Exception;

    public List<TbmBaseOpciones> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TbmBaseOpciones> findPageTbmBaseOpciones(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTbmBaseOpciones() throws Exception;
    
    public List<TbmBaseOpciones> consultarOpciones(String activo, String descripcion,
            Date fechaCreacion, Long idOpci, String imagenIcono, String nombre, String url) throws Exception;
    
    public Boolean opcionIsRegistrada(String nombre) throws Exception;
    
    public void inactivarOpcion(Long idOpci) throws Exception;
}
