package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;

import java.math.BigDecimal;

import java.util.*;


public interface ITbmBaseOpcionesGruposLogic {
    public List<TbmBaseOpcionesGrupos> getTbmBaseOpcionesGrupos()
        throws Exception;

    public void saveTbmBaseOpcionesGrupos(Long idOpci, Long idGrup,
        Date fechaCreacion, Long idGrup_TbmBaseGruposUsuarios,
        Long idOpci_TbmBaseOpciones, List<TbmBaseOpcionesGrupos> listadoOpcionesGrupo) throws Exception;

    public void deleteTbmBaseOpcionesGrupos(Long idOpci, Long idGrup)
        throws Exception;

    public void updateTbmBaseOpcionesGrupos(Long idOpci, Long idGrup,
        Date fechaCreacion, Long idGrup_TbmBaseGruposUsuarios,
        Long idOpci_TbmBaseOpciones) throws Exception;

    public TbmBaseOpcionesGrupos getTbmBaseOpcionesGrupos(
        TbmBaseOpcionesGruposId id) throws Exception;

    public List<TbmBaseOpcionesGrupos> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TbmBaseOpcionesGrupos> findPageTbmBaseOpcionesGrupos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTbmBaseOpcionesGrupos()
        throws Exception;
    
    public List<TbmBaseOpcionesGrupos> findByProperty(String propertyName,final Object value) throws Exception;
    
    public void cambiarOrden(TbmBaseOpcionesGrupos tbmBaseOpcionesGrupos, String orden) throws Exception;
}
