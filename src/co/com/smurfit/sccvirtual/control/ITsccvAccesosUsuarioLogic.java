package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvAccesosUsuarioLogic {
    public List<TsccvAccesosUsuario> getTsccvAccesosUsuario()
        throws Exception;

    /*public void saveTsccvAccesosUsuario(Date fechaAcceso, Integer idAcus,
        Integer idUsua_TsccvUsuario, Integer idEmpr_TsccvEmpresas) throws Exception;
*/
    public void deleteTsccvAccesosUsuario(Integer idAcus)
        throws Exception;

    public void updateTsccvAccesosUsuario(Date fechaAcceso, Integer idAcus,
        Integer idUsua_TsccvUsuario) throws Exception;

    public TsccvAccesosUsuario getTsccvAccesosUsuario(Integer idAcus)
        throws Exception;

    public List<TsccvAccesosUsuario> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvAccesosUsuario> findPageTsccvAccesosUsuario(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvAccesosUsuario() throws Exception;
}
