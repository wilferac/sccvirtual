package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.dao.TsccvPedidosDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class TsccvPedidosLogic implements ITsccvPedidosLogic {
    public List<TsccvPedidos> getTsccvPedidos() throws Exception {
        List<TsccvPedidos> list = new ArrayList<TsccvPedidos>();

        try {
            list = JPADaoFactory.getInstance().getTsccvPedidosDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public static synchronized void saveTsccvPedidos(String activo, Date fechaCreacion,
        Integer idPedi, Integer idEmpr_TsccvEmpresas, String observaciones, String registrado,
        String codpla_TsccvPlanta, Integer idUsua_TsccvUsuario, 
        List<ProductosPedidoVO> listadoProductosPedido) throws Exception {
        TsccvPedidos entity = null;

        try {        	
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "activo");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "fechaCreacion");
            }

            /*if (idPedi == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idPedi");
            }*/

            if ((observaciones != null) &&
                    (Utilities.checkWordAndCheckWithlength(observaciones, 1000) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "observaciones");
            }

            if (registrado == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "registrado");
            }

            if ((registrado != null) &&
                    (Utilities.checkWordAndCheckWithlength(registrado, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "registrado");
            }

            if ((codpla_TsccvPlanta != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla_TsccvPlanta");
            }

            ITsccvPlantaLogic logicTsccvPlanta1 = new TsccvPlantaLogic();
            ITsccvUsuarioLogic logicTsccvUsuario2 = new TsccvUsuarioLogic();
            ITsccvEmpresasLogic logicTsccvEmpresas3 = new TsccvEmpresasLogic();

            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta1.getTsccvPlanta(codpla_TsccvPlanta);
            TsccvUsuario tsccvUsuarioClass = logicTsccvUsuario2.getTsccvUsuario(idUsua_TsccvUsuario);
            TsccvEmpresas tsccvEmpresasClass = logicTsccvEmpresas3.getTsccvEmpresas(idEmpr_TsccvEmpresas);

            if (tsccvPlantaClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvUsuarioClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }
            
            if (tsccvEmpresasClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            /*entity = getTsccvPedidos(idPedi);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }*/

            entity = new TsccvPedidos();

            entity.setActivo(activo);
            entity.setFechaCreacion(fechaCreacion);
            //entity.setIdPedi(idPedi);
            entity.setObservaciones(observaciones);
            entity.setRegistrado(registrado);

            entity.setTsccvPlanta(tsccvPlantaClass);
            entity.setTsccvUsuario(tsccvUsuarioClass);
            entity.setTsccvEmpresas(tsccvEmpresasClass);

            //paso 1: se guarda el pedido
            EntityManagerHelper.beginTransaction();
            //Comentariamos el llamado inicial
            //JPADaoFactory.getInstance().getTsccvPedidosDAO().save(entity);
            //Llamamos el nuevo metodo que realiza el registro
            TsccvPedidosDAO.savePedido(entity);
            EntityManagerHelper.commit();
            
            if(listadoProductosPedido != null && listadoProductosPedido.size() > 0){
	            //se definen los logic necesarios para los siguientes pasos 
	            ITsccvProductosPedidoLogic tsccvProductosPedidoLogic = new TsccvProductosPedidoLogic();
	            
	            //se consulta el ultimo pedido registrado
	            Integer ultimoIdPed = JPADaoFactory.getInstance().getTsccvPedidosDAO().consultarUltimoIdPedido();
	            
	            //paso 2: se recorre y guarda cada uno de los elementos del listado de productos del pedido
	            for(ProductosPedidoVO objProdPedVO : listadoProductosPedido){
	            	TsccvProductosPedido objProdPed = objProdPedVO.getTsccvProductosPedido();
	            	TsccvProductosPedidoLogic.saveTsccvProductosPedido(objProdPed.getActivo(), objProdPed.getCantidadTotal(), 
            			objProdPed.getCodCli(), objProdPed.getCodScc(), objProdPed.getCodpla(), objProdPed.getDespro(),
            			objProdPed.getDiamtr(), objProdPed.getEje(), fechaCreacion, objProdPed.getFechaPrimeraEntrega(), 
            			null, objProdPed.getMoneda(), objProdPed.getOrdenCompra(), objProdPed.getPrecio(),
            			objProdPed.getUndmed(), ultimoIdPed);
	            }
    		}
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTsccvPedidos(Integer idPedi) throws Exception {
        TsccvPedidos entity = null;

        if (idPedi == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        List<TsccvProductosPedido> tsccvProductosPedidos = null;

        entity = getTsccvPedidos(idPedi);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            tsccvProductosPedidos = JPADaoFactory.getInstance()
                                                 .getTsccvProductosPedidoDAO()
                                                 .findByProperty("tsccvPedidos.idPedi",
                    idPedi, 0);

            if (Utilities.validationsList(tsccvProductosPedidos) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild());
            }

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvPedidosDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTsccvPedidos(String activo, Date fechaCreacion,
        Integer idPedi, Integer idEmpr_TsccvEmpresas, String observaciones, String registrado,
        String codpla_TsccvPlanta, Integer idUsua_TsccvUsuario, 
        List<ProductosPedidoVO> listadoProductosPedido)
        throws Exception {
        TsccvPedidos entity = null;

        try {
        	//if(registrado.equals("0")){//si el producto ya se va aregistrar se actualizan los datos asignados antes por defecto         		
        		if (activo == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL + "activo");
	            }
	
	            if ((activo != null) &&
	                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
	                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
	                    "activo");
	            }
	
	            if (fechaCreacion == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL +
	                    "fechaCreacion");
	            }
	
	            if (idPedi == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idPedi");
	            }
	
	            if ((observaciones != null) &&
	                    (Utilities.checkWordAndCheckWithlength(observaciones, 1000) == false)) {
	                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
	                    "observaciones");
	            }
	
	            if (registrado == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL +
	                    "registrado");
	            }
	
	            if ((registrado != null) &&
	                    (Utilities.checkWordAndCheckWithlength(registrado, 1) == false)) {
	                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
	                    "registrado");
	            }
	
	            if ((codpla_TsccvPlanta != null) &&
	                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
	                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
	                    "codpla_TsccvPlanta");
	            }
	
	            ITsccvPlantaLogic logicTsccvPlanta1 = new TsccvPlantaLogic();
	            ITsccvUsuarioLogic logicTsccvUsuario2 = new TsccvUsuarioLogic();
	            ITsccvEmpresasLogic logicTsccvEmpresas3 = new TsccvEmpresasLogic();
	
	            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta1.getTsccvPlanta(codpla_TsccvPlanta);
	            TsccvUsuario tsccvUsuarioClass = logicTsccvUsuario2.getTsccvUsuario(idUsua_TsccvUsuario);
	            TsccvEmpresas tsccvEmpresasClass = logicTsccvEmpresas3.getTsccvEmpresas(idEmpr_TsccvEmpresas);
	
	            if (tsccvPlantaClass == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL);
	            }
	
	            if (tsccvUsuarioClass == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL);
	            }
	            
	            if (tsccvEmpresasClass == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL);
	            }
	
	            entity = getTsccvPedidos(idPedi);
	
	            if (entity == null) {
	                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
	            }
	
	            entity.setActivo(activo);
	            entity.setFechaCreacion(fechaCreacion);
	            entity.setIdPedi(idPedi);
	            entity.setObservaciones(observaciones);
	            entity.setRegistrado(registrado);
	
	            entity.setTsccvPlanta(tsccvPlantaClass);
	            entity.setTsccvUsuario(tsccvUsuarioClass);
	
	            EntityManagerHelper.beginTransaction();
	            JPADaoFactory.getInstance().getTsccvPedidosDAO().update(entity);
	            EntityManagerHelper.commit();
	            EntityManagerHelper.closeEntityManager();
        	//}
        	
        	if(listadoProductosPedido != null && listadoProductosPedido.size() > 0){
	            //se definen los logic necesarios para los siguientes pasos 
	            ITsccvProductosPedidoLogic tsccvProductosPedidoLogic = new TsccvProductosPedidoLogic();	            
	            
	            //paso 1: se recorre y guarda cada uno de los elementos del listado de productos del pedido
	            for(ProductosPedidoVO objProdPedVO : listadoProductosPedido){
	            	TsccvProductosPedido objProdPed = objProdPedVO.getTsccvProductosPedido();
	            	TsccvProductosPedidoLogic.saveTsccvProductosPedido(objProdPed.getActivo(), objProdPed.getCantidadTotal(), 
            			objProdPed.getCodCli(), objProdPed.getCodScc(), objProdPed.getCodpla(), objProdPed.getDespro(),
            			objProdPed.getDiamtr(), objProdPed.getEje(), fechaCreacion, objProdPed.getFechaPrimeraEntrega(), 
            			null, objProdPed.getMoneda(), objProdPed.getOrdenCompra(), objProdPed.getPrecio(),
            			objProdPed.getUndmed(), idPedi);
	
	                /*se consulta el ultimo producto pedido registrado
	            	Integer ultimoIdPordPed = JPADaoFactory.getInstance().getTsccvProductosPedidoDAO().consultarUltimoProductoPedido();
	            	
	            	//paso 2: se recorre y guarda cada uno de los elementos del listado de entragas pedido            	            	
	            	for(TsccvEntregasPedido objEntregasPed : objProdPed.getTsccvEntregasPedidos()){
	            		tsccvEntregasPedidoLogic.saveTsccvEntregasPedido(objEntregasPed.getCantidad(), objEntregasPed.getDesdir(), 
	            				objEntregasPed.getFechaEntrega(), null, ultimoIdPordPed);
	        		}*/
	    		}
        	}
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TsccvPedidos getTsccvPedidos(Integer idPedi)
        throws Exception {
        TsccvPedidos entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPedidosDAO()
                                  .findById(idPedi);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPedidos"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TsccvPedidos> findPageTsccvPedidos(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<TsccvPedidos> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPedidosDAO()
                                  .findPageTsccvPedidos(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPedidos"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTsccvPedidos() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPedidosDAO()
                                  .findTotalNumberTsccvPedidos();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPedidos Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TsccvPedidos> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TsccvPedidos> list = new ArrayList<TsccvPedidos>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTsccvPedidosDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    
    //metodo encargado de consultar los pedidos no resgistrados para un cliente (empresa)
    public Integer consultarPedidoNoRegistrado(Integer idEmpr, String codPla, Integer idUsua) throws Exception{
    	Integer idPedi = null;

        try {
        	idPedi = JPADaoFactory.getInstance().getTsccvPedidosDAO().consultarPedidoNoRegistrado(idEmpr, codPla, idUsua);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance().exceptionFindingEntity("TsccvPedidos"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return idPedi;
    }
    
    public TsccvPedidos consultarUltimoPedido() throws Exception{
    	TsccvPedidos entity = null;
    	try{	    	
	    	//se consulta el ultimo pedido registrado
	        Integer ultimoIdPed = JPADaoFactory.getInstance().getTsccvPedidosDAO().consultarUltimoIdPedido();
	        if(ultimoIdPed == null){
	        	return null;
	        }
	        entity = getTsccvPedidos(ultimoIdPed);
    	} catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance().exceptionFindingEntity("TsccvPedidos"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        return entity;
    }
}
