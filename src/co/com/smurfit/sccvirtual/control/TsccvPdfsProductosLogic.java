package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ArchivoPDFVO;
import co.com.smurfit.utilities.CargarEmpresas;
import co.com.smurfit.utilities.EnviarEmail;
import co.com.smurfit.utilities.Utilities;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;


public class TsccvPdfsProductosLogic implements ITsccvPdfsProductosLogic {
    public List<TsccvPdfsProductos> getTsccvPdfsProductos()
        throws Exception {
        List<TsccvPdfsProductos> list = new ArrayList<TsccvPdfsProductos>();

        try {
            list = JPADaoFactory.getInstance().getTsccvPdfsProductosDAO()
                                .findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveTsccvPdfsProductos(String codScc, String codpla,
        Date fechaCreacion, String codpla_TsccvPlanta, List<ArchivoPDFVO> listadoPDFs)
        throws Exception {
        TsccvPdfsProductos entity = null;

        try {
        	ArchivoPropiedades propiedades = new ArchivoPropiedades();
        	/*
            if (codScc == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codScc");
            }

            if ((codScc != null) &&
                    (Utilities.checkWordAndCheckWithlength(codScc, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codScc");
            }

            if (codpla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpla");
            }

            if ((codpla != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "fechaCreacion");
            }

            if (codpla_TsccvPlanta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "codpla_TsccvPlanta");
            }

            if ((codpla_TsccvPlanta != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla_TsccvPlanta");
            }
			*/
        	
        	for(ArchivoPDFVO arcPdf : listadoPDFs){
	            //se envia el archivo al servidor
	            File fuente = new File(arcPdf.getUrl());
	            File dirBasePlanta = new File(propiedades.getProperty("RUTA_PDF")+arcPdf.getPdf().getTsccvPlanta().getDespla()+"\\");
	            //si no existe el directorio con el nombre de la planta se crea
	            dirBasePlanta.mkdir();
	            
	            //se arma el nombre del archivo	      
	            File destino = new File(dirBasePlanta.getPath()+"\\"+arcPdf.getPdf().getTsccvPlanta().getCodpla()+
	            						"-"+arcPdf.getPdf().getId().getCodScc()+".pdf");
	            
	            FileInputStream fis = new FileInputStream(fuente);
                FileOutputStream fos = new FileOutputStream(destino);
                FileChannel canalFuente = fis.getChannel();
                FileChannel canalDestino = fos.getChannel();
                canalFuente.transferTo(0, canalFuente.size(), canalDestino);
                fis.close();
                fos.close();
	            
	            
	            //si el archivo no es nuevo hace continue ya que solo se actualizacria el archivo mas no el resgistro
	            if(!arcPdf.getEstado().equals("Nuevo")){
	            	continue;
	            }
	            
	            ITsccvPlantaLogic logicTsccvPlanta1 = new TsccvPlantaLogic();
	        	
	            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta1.getTsccvPlanta(arcPdf.getPdf().getTsccvPlanta().getCodpla());
	
	            if (tsccvPlantaClass == null) {
	                throw new Exception(ExceptionMessages.VARIABLE_NULL+"Planta");
	            }
	
	            /*TsccvPdfsProductosId idClass = new TsccvPdfsProductosId();
	            idClass.setCodScc(codScc);
	            idClass.setCodpla(codpla);
	            */
	            
	            entity = getTsccvPdfsProductos(arcPdf.getPdf().getId());
	
	            if (entity != null) {
	                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
	            }
	
	            /*entity = new TsccvPdfsProductos();
	
	            entity.setFechaCreacion(fechaCreacion);
	            entity.setId(idClass);*/	            
	            arcPdf.getPdf().setTsccvPlanta(tsccvPlantaClass);
	
	            EntityManagerHelper.beginTransaction();
	            JPADaoFactory.getInstance().getTsccvPdfsProductosDAO().save(arcPdf.getPdf());
	            EntityManagerHelper.commit();
        	}            
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTsccvPdfsProductos(String codScc, String codpla)
        throws Exception {
        TsccvPdfsProductos entity = null;

        if (codScc == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL+"C�digo SCC Producto");
        }

        if (codpla == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL+"Planta");
        }

        TsccvPdfsProductosId idClass = new TsccvPdfsProductosId();
        idClass.setCodScc(codScc);
        idClass.setCodpla(codpla);

        entity = getTsccvPdfsProductos(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvPdfsProductosDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTsccvPdfsProductos(String codScc, String codpla,
        Date fechaCreacion, String codpla_TsccvPlanta)
        throws Exception {
        TsccvPdfsProductos entity = null;

        try {
            if (codScc == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codScc");
            }

            if ((codScc != null) &&
                    (Utilities.checkWordAndCheckWithlength(codScc, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codScc");
            }

            if (codpla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpla");
            }

            if ((codpla != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "fechaCreacion");
            }

            if (codpla_TsccvPlanta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "codpla_TsccvPlanta");
            }

            if ((codpla_TsccvPlanta != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla_TsccvPlanta");
            }

            ITsccvPlantaLogic logicTsccvPlanta1 = new TsccvPlantaLogic();

            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta1.getTsccvPlanta(codpla_TsccvPlanta);

            if (tsccvPlantaClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            TsccvPdfsProductosId idClass = new TsccvPdfsProductosId();
            idClass.setCodScc(codScc);
            idClass.setCodpla(codpla);

            entity = getTsccvPdfsProductos(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setFechaCreacion(fechaCreacion);
            entity.setId(idClass);

            entity.setTsccvPlanta(tsccvPlantaClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvPdfsProductosDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TsccvPdfsProductos getTsccvPdfsProductos(TsccvPdfsProductosId id)
        throws Exception {
        TsccvPdfsProductos entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPdfsProductosDAO()
                                  .findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPdfsProductos"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TsccvPdfsProductos> findPageTsccvPdfsProductos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        List<TsccvPdfsProductos> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPdfsProductosDAO()
                                  .findPageTsccvPdfsProductos(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPdfsProductos"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTsccvPdfsProductos() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPdfsProductosDAO()
                                  .findTotalNumberTsccvPdfsProductos();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPdfsProductos Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TsccvPdfsProductos> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TsccvPdfsProductos> list = new ArrayList<TsccvPdfsProductos>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTsccvPdfsProductosDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    } 
    
    
    
    
    
    //metodo utilizado para la migracion de los pdfs, encargado de guardar la bd solo los datos del PdfsProductos
    public void saveTsccvPdfsProductosSinArchivo(String codScc, String codpla, Date fechaCreacion, String codpla_TsccvPlanta)throws Exception {
        TsccvPdfsProductos entity = null;
        EnviarEmail enviarEmail=new EnviarEmail();
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        String emailError=archivoPropiedades.getProperty("EMAIL_DESTINO_ERROR");
        String remite=archivoPropiedades.getProperty("REMITE_ERROR");
        Vector<String> destinatarios=new Vector<String>();
        destinatarios.add(emailError);
        
        try {
        	ITsccvPlantaLogic logicTsccvPlanta1 = new TsccvPlantaLogic();
            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta1.getTsccvPlanta(codpla_TsccvPlanta);

            if (tsccvPlantaClass == null) {
                //throw new Exception(ExceptionMessages.VARIABLE_NULL);
            	//System.out.println("ERROR GRABANDO PDF: "+codScc+", NO EXISTE LA PLANTA");
            	
            	// JRUIZ: Se envia e-mail, indicando el error.
            	String mensaje="ERROR GRABANDO PDF: "+codScc+", NO EXISTE LA PLANTA";
            	enviarEmail.enviar(remite, mensaje, destinatarios);
            	// JRUIZ: fin.
            	return;
            }

            TsccvPdfsProductosId idClass = new TsccvPdfsProductosId();
            idClass.setCodScc(codScc);
            idClass.setCodpla(codpla);

            entity = getTsccvPdfsProductos(idClass);

            if (entity != null) {
                //throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            	//System.out.println("ERROR GRABANDO PDF: "+codScc+", YA EXISTE CON LA PLANTA: "+codpla);
            	
            	// JRUIZ: Se envia e-mail, indicando el error.
            	String mensaje="ERROR GRABANDO PDF: "+codScc+", YA EXISTE CON LA PLANTA: "+codpla;
            	enviarEmail.enviar(remite, mensaje, destinatarios);
            	// JRUIZ: fin.
            	return;
            }

            entity = new TsccvPdfsProductos();

            entity.setFechaCreacion(fechaCreacion);
            entity.setId(idClass);

            entity.setTsccvPlanta(tsccvPlantaClass);
            
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvPdfsProductosDAO().save(entity);
            EntityManagerHelper.commit();   
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
}
