package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.vo.ProductoVO;

import java.math.BigDecimal;

import java.util.*;


public interface ISapPrecioLogic {
    public List<SapPrecio> getSapPrecio() throws Exception;

    public void saveSapPrecio(String codcli, String codpro, String precio,
        String despro, String diamtr, String eje, String moneda, String tiprec,
        String undmed) throws Exception;

    public void deleteSapPrecio(String codcli, String codpro, String precio)
        throws Exception;

    public void updateSapPrecio(String codcli, String codpro, String precio,
        String despro, String diamtr, String eje, String moneda, String tiprec,
        String undmed) throws Exception;

    public SapPrecio getSapPrecio(SapPrecioId id) throws Exception;

    public List<SapPrecio> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<SapPrecio> findPageSapPrecio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberSapPrecio() throws Exception;
    
    public List<ProductoVO> consultarProductosSap(String codigoProd, String descripcion, String coddigoCli, String nombreColumna, Boolean ascendente) throws Exception;
    
    public List<ProductoVO> consultarPreciosSap(String codigoProd, String descripcion, String coddigoCli, String nombreColumna, Boolean ascendente) throws Exception;
}
