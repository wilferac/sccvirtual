package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvPlantaLogic {
    public List<TsccvPlanta> getTsccvPlanta() throws Exception;

    public void saveTsccvPlanta(String codpla, String despla, String direle,
        String planta, Long idApli_TsccvAplicaciones,
        Long idTipr_TsccvTipoProducto) throws Exception;

    public void deleteTsccvPlanta(String codpla) throws Exception;

    public void updateTsccvPlanta(String codpla, String despla, String direle,
        String planta, Long idApli_TsccvAplicaciones,
        Long idTipr_TsccvTipoProducto) throws Exception;

    public TsccvPlanta getTsccvPlanta(String codpla) throws Exception;

    public List<TsccvPlanta> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvPlanta> findPageTsccvPlanta(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTsccvPlanta() throws Exception;
    
    public List<TsccvPlanta> consultarPlantas(String codpla, String despla, String direle,
            String planta, Long idApli_TsccvAplicaciones, Long idTipr_TsccvTipoProducto) throws Exception;
    
    public Boolean plantaIsRegistrada(String desPla) throws Exception;
    
    public List<TsccvPlanta> findByActivoAplicacion(String activo, Long aplicacion)  throws Exception;
    
    public List<TsccvPlanta> findByUsuarioAndAplicacion(Integer usuario, Integer aplicacion, String activoApp) throws Exception;
    
    public List<TsccvPlanta> findByTipProdAndAplicacion(String tipoCodEmpr, Long aplicacion) throws Exception;
}
