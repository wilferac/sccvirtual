package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.dao.TbmBaseOpcionesDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class TbmBaseOpcionesLogic implements ITbmBaseOpcionesLogic {
    public List<TbmBaseOpciones> getTbmBaseOpciones() throws Exception {
        List<TbmBaseOpciones> list = new ArrayList<TbmBaseOpciones>();

        try {
            list = JPADaoFactory.getInstance().getTbmBaseOpcionesDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveTbmBaseOpciones(String activo, String descripcion,
        Date fechaCreacion, Long idOpci, String imagenIcono, String nombre,
        String url) throws Exception {
        TbmBaseOpciones entity = null;

        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Nombre");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Nombre");
            }

            if ((descripcion != null) &&
                    (Utilities.checkWordAndCheckWithlength(descripcion, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Descripci�n");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creaci�n");
            }

            /*if (idOpci == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idOpci");
            }

            if ((idOpci != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idOpci, 2, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idOpci");
            }*/

            if ((imagenIcono != null) &&
                    (Utilities.checkWordAndCheckWithlength(imagenIcono, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Imagen/Icono");
            }

            if (nombre == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "nombre");
            }

            if ((nombre != null) &&
                    (Utilities.checkWordAndCheckWithlength(nombre, 40) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Nombre");
            }

            if ((url != null) &&
                    (Utilities.checkWordAndCheckWithlength(url, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH + "URL");
            }

            /*entity = getTbmBaseOpciones(idOpci);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }*/

            entity = new TbmBaseOpciones();

            entity.setActivo(activo);
            entity.setDescripcion(descripcion);
            entity.setFechaCreacion(fechaCreacion);
            //entity.setIdOpci(idOpci);
            entity.setImagenIcono(imagenIcono);
            entity.setNombre(nombre);
            entity.setUrl(url);

            EntityManagerHelper.beginTransaction();
            //Comentariamos el llamado inicial
            //JPADaoFactory.getInstance().getTbmBaseOpcionesDAO().save(entity);
            //Llamamos el nuevo metodo que realiza el registro
            TbmBaseOpcionesDAO.saveOpcion(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTbmBaseOpciones(Long idOpci) throws Exception {
        TbmBaseOpciones entity = null;

        if (idOpci == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL+"Id Opci�n");
        }

        List<TbmBaseOpcionesGrupos> tbmBaseOpcionesGruposes = null;
        List<TbmBaseSubopcionesOpc> tbmBaseSubopcionesOpcs = null;

        entity = getTbmBaseOpciones(idOpci);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            tbmBaseOpcionesGruposes = JPADaoFactory.getInstance()
                                                   .getTbmBaseOpcionesGruposDAO()
                                                   .findByProperty("tbmBaseOpciones.idOpci",
                    idOpci, 0);

            if (Utilities.validationsList(tbmBaseOpcionesGruposes) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild()+"Opciones Grupos");
            }

            tbmBaseSubopcionesOpcs = JPADaoFactory.getInstance()
                                                  .getTbmBaseSubopcionesOpcDAO()
                                                  .findByProperty("tbmBaseOpciones.idOpci",
                    idOpci, 0);

            if (Utilities.validationsList(tbmBaseSubopcionesOpcs) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild()+"Sub. Opciones Opciones");
            }

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTbmBaseOpcionesDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTbmBaseOpciones(String activo, String descripcion,
        Date fechaCreacion, Long idOpci, String imagenIcono, String nombre,
        String url) throws Exception {
        TbmBaseOpciones entity = null;

        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Nombre");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Nombre");
            }

            if ((descripcion != null) &&
                    (Utilities.checkWordAndCheckWithlength(descripcion, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Descripci�n");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creaci�n");
            }

            if (idOpci == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Id Opci�n");
            }

            if ((idOpci != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idOpci, 2, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Id Opci�n");
            }

            if ((imagenIcono != null) &&
                    (Utilities.checkWordAndCheckWithlength(imagenIcono, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Imagen/Icono");
            }

            if (nombre == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Nombre");
            }

            if ((nombre != null) &&
                    (Utilities.checkWordAndCheckWithlength(nombre, 40) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Nombre");
            }

            if ((url != null) &&
                    (Utilities.checkWordAndCheckWithlength(url, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH + "URL");
            }

            entity = getTbmBaseOpciones(idOpci);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setActivo(activo);
            entity.setDescripcion(descripcion);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdOpci(idOpci);
            entity.setImagenIcono(imagenIcono);
            entity.setNombre(nombre);
            entity.setUrl(url);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTbmBaseOpcionesDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TbmBaseOpciones getTbmBaseOpciones(Long idOpci)
        throws Exception {
        TbmBaseOpciones entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseOpcionesDAO()
                                  .findById(idOpci);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseOpciones"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TbmBaseOpciones> findPageTbmBaseOpciones(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        List<TbmBaseOpciones> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseOpcionesDAO()
                                  .findPageTbmBaseOpciones(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseOpciones"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTbmBaseOpciones() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseOpcionesDAO()
                                  .findTotalNumberTbmBaseOpciones();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseOpciones Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TbmBaseOpciones> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TbmBaseOpciones> list = new ArrayList<TbmBaseOpciones>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTbmBaseOpcionesDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    //Metodo utilizado para la consulta por todos los campos
    public List<TbmBaseOpciones> consultarOpciones(String activo, String descripcion,
        Date fechaCreacion, Long idOpci, String imagenIcono, String nombre, String url) throws Exception {    	
        
    	TbmBaseOpciones entity = null;
        List<TbmBaseOpciones> registros = null;
        
    	try {
	    	entity = new TbmBaseOpciones();
	    	
	    	entity.setActivo(activo);
            entity.setDescripcion(descripcion);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdOpci(idOpci);
            entity.setImagenIcono(imagenIcono);
            entity.setNombre(nombre);
            entity.setUrl(url);	    	
	    	
    		registros = JPADaoFactory.getInstance().getTbmBaseOpcionesDAO().consultarOpciones(entity);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return registros;
    }
    
    //consulta las opciones con los datos para saber si esta registrado
    public Boolean opcionIsRegistrada(String nombre) throws Exception{
    	try{
	    	return JPADaoFactory.getInstance().getTbmBaseOpcionesDAO().opcionIsRegistrada(nombre);
	    } catch (Exception e) {
	        throw new Exception(e.getMessage());
	    } finally {
	        EntityManagerHelper.closeEntityManager();
	    }
    }
    
    public void inactivarOpcion(Long idOpci) throws Exception {
    	try{    
    		EntityManagerHelper.beginTransaction();
    		JPADaoFactory.getInstance().getTbmBaseOpcionesDAO().inactivarOpcion(idOpci);	
    		EntityManagerHelper.commit();
    	}catch(Exception e){
    		EntityManagerHelper.rollback();
    		throw new Exception(e.getMessage());
    	}
    	finally{    		
    		EntityManagerHelper.closeEntityManager();
    	}
    }
}
