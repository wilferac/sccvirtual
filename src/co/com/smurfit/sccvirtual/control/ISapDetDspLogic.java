package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;

import java.math.BigDecimal;

import java.util.*;


public interface ISapDetDspLogic {
    public List<SapDetDsp> getSapDetDsp() throws Exception;

    public void saveSapDetDsp(String subdsp, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String subitm,
        String fecdsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp, String cankgs)
        throws Exception;

    public void deleteSapDetDsp(String subdsp, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String subitm,
        String fecdsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp, String cankgs)
        throws Exception;

    public void updateSapDetDsp(String subdsp, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String subitm,
        String fecdsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp, String cankgs)
        throws Exception;

    public SapDetDsp getSapDetDsp(SapDetDspId id) throws Exception;

    public List<SapDetDsp> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<SapDetDsp> findPageSapDetDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberSapDetDsp() throws Exception;
    
    public List<SapDetDsp> consultarDetalleDespachoSap(String numPed, String itemPed, String codMas) throws Exception;
}
