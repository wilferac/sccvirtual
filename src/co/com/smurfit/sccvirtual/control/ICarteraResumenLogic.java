package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;

import java.math.BigDecimal;

import java.util.*;


public interface ICarteraResumenLogic {
    public List<CarteraResumen> getCarteraResumen() throws Exception;

    public void saveCarteraResumen(String codcli, String numfac, String clsdoc,
        String fecfac, String fecven, String dias, String monto, String planta,
        String rango) throws Exception;

    public void deleteCarteraResumen(String codcli, String numfac,
        String clsdoc, String fecfac, String fecven, String dias, String monto,
        String planta, String rango) throws Exception;

    public void updateCarteraResumen(String codcli, String numfac,
        String clsdoc, String fecfac, String fecven, String dias, String monto,
        String planta, String rango) throws Exception;

    public CarteraResumen getCarteraResumen(CarteraResumenId id)
        throws Exception;

    public List<CarteraResumen> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<CarteraResumen> findPageCarteraResumen(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberCarteraResumen() throws Exception;
    
    public List<Vector<Object>> consultarCartera(Integer codCli) throws Exception;
    
    public List<CarteraResumen> findByProperty(String propertyName, final Object value) throws Exception;
    
    public List<CarteraResumen> consultarDetalleFactura(Integer codCli, String rango) throws Exception;
}
