package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresaId;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvDetalleGrupoEmpresaLogic {
    public List<TsccvDetalleGrupoEmpresa> getTsccvDetalleGrupoEmpresa()
        throws Exception;

    public void saveTsccvDetalleGrupoEmpresa(Integer idGrue, Integer idEmpr,
        Date fechaCreacion, Integer idEmpr_TsccvEmpresas,
        Integer idGrue_TsccvGrupoEmpresa) throws Exception;

    public void deleteTsccvDetalleGrupoEmpresa(Integer idGrue, Integer idEmpr)
        throws Exception;

    public void updateTsccvDetalleGrupoEmpresa(Integer idGrue, Integer idEmpr,
        Date fechaCreacion, Integer idEmpr_TsccvEmpresas,
        Integer idGrue_TsccvGrupoEmpresa) throws Exception;

    public TsccvDetalleGrupoEmpresa getTsccvDetalleGrupoEmpresa(
        TsccvDetalleGrupoEmpresaId id) throws Exception;

    public List<TsccvDetalleGrupoEmpresa> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvDetalleGrupoEmpresa> findPageTsccvDetalleGrupoEmpresa(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvDetalleGrupoEmpresa()
        throws Exception;
    
    public List<TsccvDetalleGrupoEmpresa> findByProperty(String propertyName,final Object value) throws Exception;
}
