package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;

import java.math.BigDecimal;

import java.util.*;


public interface ITbmBaseGruposUsuariosLogic {
    public List<TbmBaseGruposUsuarios> getTbmBaseGruposUsuarios()
        throws Exception;

    public void saveTbmBaseGruposUsuarios(String activo, String descripcion,
        Date fechaCreacion, Long idGrup) throws Exception;

    public void deleteTbmBaseGruposUsuarios(Long idGrup)
        throws Exception;

    public void updateTbmBaseGruposUsuarios(String activo, String descripcion,
        Date fechaCreacion, Long idGrup) throws Exception;

    public TbmBaseGruposUsuarios getTbmBaseGruposUsuarios(Long idGrup)
        throws Exception;

    public List<TbmBaseGruposUsuarios> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TbmBaseGruposUsuarios> findPageTbmBaseGruposUsuarios(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTbmBaseGruposUsuarios()
        throws Exception;
    
    public Boolean grupoUsuarioIsRegistrado(String descripcion) throws Exception;
    
    public List<TbmBaseGruposUsuarios> consultarGruposUsuario(String activo, String descripcion,
            Date fechaCreacion, Long idGrup) throws Exception;
    
    public void inactivarGrupoUsuario(Long idGrup) throws Exception;
}
