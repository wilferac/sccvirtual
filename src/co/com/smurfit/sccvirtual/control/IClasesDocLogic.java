package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


public interface IClasesDocLogic {
    public List<ClasesDoc> getClasesDoc() throws Exception;

    public void saveClasesDoc(String coddoc, String desdoc)
        throws Exception;

    public void deleteClasesDoc(String coddoc, String desdoc)
        throws Exception;

    public void updateClasesDoc(String coddoc, String desdoc)
        throws Exception;

    public ClasesDoc getClasesDoc(ClasesDocId id) throws Exception;

    public List<ClasesDoc> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<ClasesDoc> findPageClasesDoc(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberClasesDoc() throws Exception;
}
