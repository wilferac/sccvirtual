package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class SapDetDspLogic implements ISapDetDspLogic {
    public List<SapDetDsp> getSapDetDsp() throws Exception {
        List<SapDetDsp> list = new ArrayList<SapDetDsp>();

        try {
            list = JPADaoFactory.getInstance().getSapDetDspDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveSapDetDsp(String subdsp, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String subitm,
        String fecdsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp, String cankgs)
        throws Exception {
        SapDetDsp entity = null;

        try {
            if (subdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "subdsp");
            }

            if ((subdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(subdsp, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "subdsp");
            }

            if (numdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numdsp");
            }

            if ((numdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(numdsp, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numdsp");
            }

            if (itmdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmdsp");
            }

            if ((itmdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmdsp, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmdsp");
            }

            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (numped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numped");
            }

            if ((numped != null) &&
                    (Utilities.checkWordAndCheckWithlength(numped, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numped");
            }

            if (itmped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmped");
            }

            if ((itmped != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmped, 3) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmped");
            }

            if (subitm == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "subitm");
            }

            if ((subitm != null) &&
                    (Utilities.checkWordAndCheckWithlength(subitm, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "subitm");
            }

            if (fecdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecdsp");
            }

            if ((fecdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecdsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecdsp");
            }

            if (candsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "candsp");
            }

            if ((candsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(candsp, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "candsp");
            }

            if (undmed == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "undmed");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            if (emptra == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "emptra");
            }

            if ((emptra != null) &&
                    (Utilities.checkWordAndCheckWithlength(emptra, 32) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "emptra");
            }

            if (placa == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "placa");
            }

            if ((placa != null) &&
                    (Utilities.checkWordAndCheckWithlength(placa, 15) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "placa");
            }

            if (nomcon == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "nomcon");
            }

            if ((nomcon != null) &&
                    (Utilities.checkWordAndCheckWithlength(nomcon, 40) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "nomcon");
            }

            if (dirdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "dirdsp");
            }

            if ((dirdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(dirdsp, 80) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "dirdsp");
            }

            if (cankgs == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cankgs");
            }

            if ((cankgs != null) &&
                    (Utilities.checkWordAndCheckWithlength(cankgs, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cankgs");
            }

            SapDetDspId idClass = new SapDetDspId();
            idClass.setSubdsp(subdsp);
            idClass.setNumdsp(numdsp);
            idClass.setItmdsp(itmdsp);
            idClass.setCodcli(codcli);
            idClass.setNumped(numped);
            idClass.setItmped(itmped);
            idClass.setSubitm(subitm);
            idClass.setFecdsp(fecdsp);
            idClass.setCandsp(candsp);
            idClass.setUndmed(undmed);
            idClass.setEmptra(emptra);
            idClass.setPlaca(placa);
            idClass.setNomcon(nomcon);
            idClass.setDirdsp(dirdsp);
            idClass.setCankgs(cankgs);

            entity = getSapDetDsp(idClass);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }

            entity = new SapDetDsp();

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getSapDetDspDAO().save(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteSapDetDsp(String subdsp, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String subitm,
        String fecdsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp, String cankgs)
        throws Exception {
        SapDetDsp entity = null;

        if (subdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (numdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (itmdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (codcli == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (numped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (itmped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (subitm == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (fecdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (candsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (undmed == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (emptra == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (placa == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (nomcon == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (dirdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (cankgs == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        SapDetDspId idClass = new SapDetDspId();
        idClass.setSubdsp(subdsp);
        idClass.setNumdsp(numdsp);
        idClass.setItmdsp(itmdsp);
        idClass.setCodcli(codcli);
        idClass.setNumped(numped);
        idClass.setItmped(itmped);
        idClass.setSubitm(subitm);
        idClass.setFecdsp(fecdsp);
        idClass.setCandsp(candsp);
        idClass.setUndmed(undmed);
        idClass.setEmptra(emptra);
        idClass.setPlaca(placa);
        idClass.setNomcon(nomcon);
        idClass.setDirdsp(dirdsp);
        idClass.setCankgs(cankgs);

        entity = getSapDetDsp(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getSapDetDspDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateSapDetDsp(String subdsp, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String subitm,
        String fecdsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp, String cankgs)
        throws Exception {
        SapDetDsp entity = null;

        try {
            if (subdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "subdsp");
            }

            if ((subdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(subdsp, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "subdsp");
            }

            if (numdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numdsp");
            }

            if ((numdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(numdsp, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numdsp");
            }

            if (itmdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmdsp");
            }

            if ((itmdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmdsp, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmdsp");
            }

            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (numped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numped");
            }

            if ((numped != null) &&
                    (Utilities.checkWordAndCheckWithlength(numped, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numped");
            }

            if (itmped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmped");
            }

            if ((itmped != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmped, 3) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmped");
            }

            if (subitm == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "subitm");
            }

            if ((subitm != null) &&
                    (Utilities.checkWordAndCheckWithlength(subitm, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "subitm");
            }

            if (fecdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecdsp");
            }

            if ((fecdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecdsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecdsp");
            }

            if (candsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "candsp");
            }

            if ((candsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(candsp, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "candsp");
            }

            if (undmed == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "undmed");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            if (emptra == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "emptra");
            }

            if ((emptra != null) &&
                    (Utilities.checkWordAndCheckWithlength(emptra, 32) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "emptra");
            }

            if (placa == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "placa");
            }

            if ((placa != null) &&
                    (Utilities.checkWordAndCheckWithlength(placa, 15) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "placa");
            }

            if (nomcon == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "nomcon");
            }

            if ((nomcon != null) &&
                    (Utilities.checkWordAndCheckWithlength(nomcon, 40) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "nomcon");
            }

            if (dirdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "dirdsp");
            }

            if ((dirdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(dirdsp, 80) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "dirdsp");
            }

            if (cankgs == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cankgs");
            }

            if ((cankgs != null) &&
                    (Utilities.checkWordAndCheckWithlength(cankgs, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cankgs");
            }

            SapDetDspId idClass = new SapDetDspId();
            idClass.setSubdsp(subdsp);
            idClass.setNumdsp(numdsp);
            idClass.setItmdsp(itmdsp);
            idClass.setCodcli(codcli);
            idClass.setNumped(numped);
            idClass.setItmped(itmped);
            idClass.setSubitm(subitm);
            idClass.setFecdsp(fecdsp);
            idClass.setCandsp(candsp);
            idClass.setUndmed(undmed);
            idClass.setEmptra(emptra);
            idClass.setPlaca(placa);
            idClass.setNomcon(nomcon);
            idClass.setDirdsp(dirdsp);
            idClass.setCankgs(cankgs);

            entity = getSapDetDsp(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getSapDetDspDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public SapDetDsp getSapDetDsp(SapDetDspId id) throws Exception {
        SapDetDsp entity = null;

        try {
            entity = JPADaoFactory.getInstance().getSapDetDspDAO().findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("SapDetDsp"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<SapDetDsp> findPageSapDetDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<SapDetDsp> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getSapDetDspDAO()
                                  .findPageSapDetDsp(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("SapDetDsp"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberSapDetDsp() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getSapDetDspDAO()
                                  .findTotalNumberSapDetDsp();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("SapDetDsp Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<SapDetDsp> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<SapDetDsp> list = new ArrayList<SapDetDsp>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getSapDetDspDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    
    //metodo encrgado de consultar el detalle despacho
    public List<SapDetDsp> consultarDetalleDespachoSap(String numPed, String itemPed, String codMas) throws Exception{
    	try {        	
    		SapDetPed entity = new SapDetPed();
    		SapDetPedId sapDetPedId = new SapDetPedId();
    		
    		sapDetPedId.setNumped(numPed);
    		sapDetPedId.setItmped(itemPed);
    		sapDetPedId.setCodcli(codMas);
    		
    		entity.setId(sapDetPedId);
    		
    		return JPADaoFactory.getInstance().getSapDetDspDAO().consultarDetalleDespachoSap(entity);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
}

