package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.dao.TsccvUsuarioDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.presentation.backEndBeans.MBUsuario;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.EnviarEmail;
import co.com.smurfit.utilities.FacesUtils;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Vector;


public class TsccvUsuarioLogic implements ITsccvUsuarioLogic {
    public List<TsccvUsuario> getTsccvUsuario() throws Exception {
        List<TsccvUsuario> list = new ArrayList<TsccvUsuario>();

        try {
            list = JPADaoFactory.getInstance().getTsccvUsuarioDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public static synchronized String saveTsccvUsuario(String activo, String apellido,
        Integer cedula, String email, Date fechaCreacion, Integer idUsua,
        String login, String nombre, String password,
        Long idGrup_TbmBaseGruposUsuarios, Integer idEmpr_TsccvEmpresas,
        String codpla_TsccvPlanta) throws Exception {
        TsccvUsuario entity = null;
        String msg = null;
        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Activo");
            }

            if (apellido == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Apellido");
            }

            if ((apellido != null) &&
                    (Utilities.checkWordAndCheckWithlength(apellido, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Apellido");
            }

            if (cedula == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "C�dula");
            }

            if (email == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "E-mail");
            }

            if ((email != null) &&
                    (Utilities.checkWordAndCheckWithlength(email, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "E-mail");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creaci�n");
            }

            /*if (idUsua == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idUsua");
            }*/

            if (login == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Login");
            }

            if ((login != null) &&
                    (Utilities.checkWordAndCheckWithlength(login, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Login");
            }

            if (nombre == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Nombre");
            }

            if ((nombre != null) &&
                    (Utilities.checkWordAndCheckWithlength(nombre, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Nombre");
            }

            if (password == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Password");
            }

            if ((password != null) &&
                    (Utilities.checkWordAndCheckWithlength(password, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Password");
            }

            if ((idGrup_TbmBaseGruposUsuarios != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idGrup_TbmBaseGruposUsuarios, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Id Grupo Usuario");
            }

            if (codpla_TsccvPlanta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "C�digo Planta");
            }

            if ((codpla_TsccvPlanta != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "C�digo Planta");
            }
            

            TbmBaseGruposUsuarios tbmBaseGruposUsuariosClass = null;
            if(idGrup_TbmBaseGruposUsuarios != null){
            	ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios1 = new TbmBaseGruposUsuariosLogic();
            	tbmBaseGruposUsuariosClass = logicTbmBaseGruposUsuarios1.getTbmBaseGruposUsuarios(idGrup_TbmBaseGruposUsuarios);
            	
            	if (tbmBaseGruposUsuariosClass == null) {
                    throw new Exception(ExceptionMessages.VARIABLE_NULL+"Grupo Usuario");
                }
            }
                        
            TsccvEmpresas tsccvEmpresasClass = null;
            if(idEmpr_TsccvEmpresas != null){
            	ITsccvEmpresasLogic logicTsccvEmpresas2 = new TsccvEmpresasLogic();
            	tsccvEmpresasClass = logicTsccvEmpresas2.getTsccvEmpresas(idEmpr_TsccvEmpresas);
            	
            	if (tsccvEmpresasClass == null) {
                    throw new Exception(ExceptionMessages.VARIABLE_NULL+"Empresa");
                }
            }
            
            ITsccvPlantaLogic logicTsccvPlanta3 = new TsccvPlantaLogic();
            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta3.getTsccvPlanta(codpla_TsccvPlanta);
            if (tsccvPlantaClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL+"Planta");
            }

            /*entity = getTsccvUsuario(idUsua);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }*/

            entity = new TsccvUsuario();

            entity.setActivo(activo);
            entity.setApellido(apellido);
            entity.setCedula(cedula);
            entity.setEmail(email);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdUsua(idUsua);
            entity.setLogin(login);
            entity.setNombre(nombre);
            entity.setPassword(password);

            entity.setTbmBaseGruposUsuarios(tbmBaseGruposUsuariosClass);
            entity.setTsccvEmpresas(tsccvEmpresasClass);
            entity.setTsccvPlanta(tsccvPlantaClass);

            EntityManagerHelper.beginTransaction();
            //Comentariamos el llamado inicial
            //JPADaoFactory.getInstance().getTsccvUsuarioDAO().save(entity);
            //Llamamos el nuevo metodo que realiza el registro
            TsccvUsuarioDAO.saveUsuario(entity);
            EntityManagerHelper.commit();
            
            
            //se envia el email de confirmacion de creacion de usuario 
            ConstruirEmailCreacionDeUsuario construirEmailCreacionDeUsuario = new ConstruirEmailCreacionDeUsuario(); 
            
            //se coge el usuario en sesion
            MBUsuario mbUser = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
            TsccvUsuario userLogin = mbUser.getUsuario();
            
            //se llama el metodo que construye el email de confimacion enviadole los el usuario en sesion y el creado 
            construirEmailCreacionDeUsuario.construirConfirmacion(entity, userLogin);
                        
            EnviarEmail enviarEmail = new EnviarEmail();
            //se crea un vector de destinatarios adicionando el email del usuario creado
            Vector<String> destinatarios = new Vector<String>();
            destinatarios.add(entity.getEmail());
            destinatarios.add(userLogin.getEmail());
            
            //se envia el email
            msg = enviarEmail.enviar(userLogin.getEmail(), construirEmailCreacionDeUsuario.getMensaje(), destinatarios);
            
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return msg != null ? (msg.indexOf("email") == -1 ? ", Pero fallo el envio del email por que "+msg : ", Pero "+msg.toLowerCase()) : null;
    }

    public void deleteTsccvUsuario(Integer idUsua) throws Exception {
        TsccvUsuario entity = null;

        if (idUsua == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL+"Id Usuario");
        }

        List<TsccvAccesosUsuario> tsccvAccesosUsuarios = null;
        List<TsccvPedidos> tsccvPedidoses = null;
        List<TsccvPlantasUsuario> tsccvPlantasUsuarios = null;

        entity = getTsccvUsuario(idUsua);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            tsccvPedidoses = JPADaoFactory.getInstance().getTsccvPedidosDAO()
                                          .findByProperty("tsccvUsuario.idUsua",
                    idUsua, 0);

            if (Utilities.validationsList(tsccvPedidoses) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild());
            }

            tsccvPlantasUsuarios = JPADaoFactory.getInstance()
                                                .getTsccvPlantasUsuarioDAO()
                                                .findByProperty("tsccvUsuario.idUsua",
                    idUsua, 0);

            if (Utilities.validationsList(tsccvPlantasUsuarios) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild()+"Pedidos");
            }

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvUsuarioDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public String updateTsccvUsuario(String activo, String apellido,
        Integer cedula, String email, Date fechaCreacion, Integer idUsua,
        String login, String nombre, String password,
        Long idGrup_TbmBaseGruposUsuarios, Integer idEmpr_TsccvEmpresas,
        String codpla_TsccvPlanta) throws Exception {
    	
        TsccvUsuario entity = null;
        boolean cambioPassword = false;
        String msg = null;
        
        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Activo");
            }

            if (apellido == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Apellido");
            }

            if ((apellido != null) &&
                    (Utilities.checkWordAndCheckWithlength(apellido, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Apellido");
            }

            if (cedula == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "C�dula");
            }

            if (email == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "E-mail");
            }

            if ((email != null) &&
                    (Utilities.checkWordAndCheckWithlength(email, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "E-mail");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creaci�n");
            }

            if (idUsua == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Id Usuario");
            }

            /*if (login == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Login");
            }

            if ((login != null) &&
                    (Utilities.checkWordAndCheckWithlength(login, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Login");
            }*/

            if (nombre == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Nombre");
            }

            if ((nombre != null) &&
                    (Utilities.checkWordAndCheckWithlength(nombre, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Nombre");
            }

            if (password == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Password");
            }

            if ((password != null) &&
                    (Utilities.checkWordAndCheckWithlength(password, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Password");
            }

            if ((idGrup_TbmBaseGruposUsuarios != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idGrup_TbmBaseGruposUsuarios, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Id Grupo Usuario");
            }

            if (codpla_TsccvPlanta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "C�digo Planta");
            }

            if ((codpla_TsccvPlanta != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla_TsccvPlanta, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "C�digo Planta");
            }                                   

            TbmBaseGruposUsuarios tbmBaseGruposUsuariosClass = null;
            if(idGrup_TbmBaseGruposUsuarios != null){
            	ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios1 = new TbmBaseGruposUsuariosLogic();
            	tbmBaseGruposUsuariosClass = logicTbmBaseGruposUsuarios1.getTbmBaseGruposUsuarios(idGrup_TbmBaseGruposUsuarios);
            	
            	if (tbmBaseGruposUsuariosClass == null) {
                    throw new Exception(ExceptionMessages.VARIABLE_NULL+"Grupo Usuario");
                }
            }
                        
            TsccvEmpresas tsccvEmpresasClass = null;
            if(idEmpr_TsccvEmpresas != null){
            	ITsccvEmpresasLogic logicTsccvEmpresas2 = new TsccvEmpresasLogic();
            	tsccvEmpresasClass = logicTsccvEmpresas2.getTsccvEmpresas(idEmpr_TsccvEmpresas);
            	
            	if (tsccvEmpresasClass == null) {
                    throw new Exception(ExceptionMessages.VARIABLE_NULL+"Empresa");
                }
            }
            
            ITsccvPlantaLogic logicTsccvPlanta3 = new TsccvPlantaLogic();
            TsccvPlanta tsccvPlantaClass = logicTsccvPlanta3.getTsccvPlanta(codpla_TsccvPlanta);
            if (tsccvPlantaClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL+"Planta");
            }

            entity = getTsccvUsuario(idUsua);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            //se valida si el password del usuario fue modificado
            if(!entity.getPassword().equals(password)){
            	cambioPassword = true;
            }
            
            entity.setActivo(activo);
            entity.setApellido(apellido);
            entity.setCedula(cedula);
            entity.setEmail(email);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdUsua(idUsua);
            entity.setLogin(login);
            entity.setNombre(nombre);
            entity.setPassword(password);

            entity.setTbmBaseGruposUsuarios(tbmBaseGruposUsuariosClass);
            entity.setTsccvEmpresas(tsccvEmpresasClass);
            entity.setTsccvPlanta(tsccvPlantaClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvUsuarioDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
            
            if(cambioPassword){
            	//se envia el email de confirmacion de creacion de usuario 
                ConstruirEmailCreacionDeUsuario construirEmailCreacionDeUsuario = new ConstruirEmailCreacionDeUsuario(); 
                
                //se coge el usuario en sesion
                MBUsuario mbUser = (MBUsuario) FacesUtils.getManagedBean("MBUsuario");
                TsccvUsuario userLogin = mbUser.getUsuario();
                
                //se llama el metodo que construye el email de confimacion enviadole los el usuario en sesion y el creado 
                construirEmailCreacionDeUsuario.construirCambioContrasena(entity);
                            
                EnviarEmail enviarEmail = new EnviarEmail();
                //se crea un vector de destinatarios adicionando el email del usuario creado
                Vector<String> destinatarios = new Vector<String>();
                destinatarios.add(entity.getEmail());
                destinatarios.add(userLogin.getEmail());
                
                ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
                
                //se envia el email
                msg = enviarEmail.enviar(archivoPropiedades.getProperty("USER"), construirEmailCreacionDeUsuario.getMensaje(), destinatarios);
                
                return msg != null ? (msg.indexOf("email") == -1 ? ", Pero fallo el envio del email por cambio de password por que "+msg : ", Pero "+msg.toLowerCase()) : null;
            }
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return null;
    }

    public TsccvUsuario getTsccvUsuario(Integer idUsua)
        throws Exception {
        TsccvUsuario entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvUsuarioDAO()
                                  .findById(idUsua);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvUsuario"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TsccvUsuario> findPageTsccvUsuario(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<TsccvUsuario> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvUsuarioDAO()
                                  .findPageTsccvUsuario(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvUsuario"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTsccvUsuario() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvUsuarioDAO()
                                  .findTotalNumberTsccvUsuario();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvUsuario Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TsccvUsuario> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TsccvUsuario> list = new ArrayList<TsccvUsuario>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTsccvUsuarioDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    
    public List<TsccvUsuario> consultarUsuarios(String activo, String apellido,
        Integer cedula, String email, Date fechaCreacion, Integer idUsua,
        String login, String nombre, String password,
        Long idGrup_TbmBaseGruposUsuarios, Integer idEmpr_TsccvEmpresas,
        String codpla_TsccvPlanta) throws Exception{
    	
    	TsccvUsuario entity = null;
    	List<TsccvUsuario> registros = null;
        try {
        	ITsccvPlantaLogic logicTsccvPlanta = new TsccvPlantaLogic();
	    	ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios = new TbmBaseGruposUsuariosLogic();
            ITsccvEmpresasLogic logicTsccvEmpresas = new TsccvEmpresasLogic();
            
            entity = new TsccvUsuario();
            
            //SE AGREGA VALIDACION AL CAMPO EMPRESA YA QUE NO ES OBLIGATORIO
            if(idEmpr_TsccvEmpresas != null){
    			TsccvEmpresas tsccvEmpresas = logicTsccvEmpresas.getTsccvEmpresas(idEmpr_TsccvEmpresas);
    			entity.setTsccvEmpresas(tsccvEmpresas);
    		}
    		
    		//SE AGREGA VALIDACION AL CAMPO GRUPO YA QUE NO ES OBLIGATORIO
    		if(idGrup_TbmBaseGruposUsuarios != null){
    			TbmBaseGruposUsuarios tbmBaseGruposUsuarios = logicTbmBaseGruposUsuarios.getTbmBaseGruposUsuarios(idGrup_TbmBaseGruposUsuarios);
    			entity.setTbmBaseGruposUsuarios(tbmBaseGruposUsuarios);
    		}
            
    		if(codpla_TsccvPlanta != null){
    			TsccvPlanta tsccvPlanta = logicTsccvPlanta.getTsccvPlanta(codpla_TsccvPlanta);
    			entity.setTsccvPlanta(tsccvPlanta);
    		}
    		
    		entity.setIdUsua(idUsua);
    		entity.setLogin(login);
    		entity.setPassword(password);
    		entity.setNombre(nombre);
    		entity.setApellido(apellido);
    		entity.setCedula(cedula);
    		entity.setEmail(email);
    		entity.setActivo(activo);
    		entity.setFechaCreacion(fechaCreacion);

            registros= JPADaoFactory.getInstance().getTsccvUsuarioDAO().consultarUsuarios(entity);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return registros;
    }
    
    //consulta un usuario con los datos para saber si esta registrado
    public Boolean usuarioIsRegistrado(String login, Integer cedula, Integer idEmpr) throws Exception{
	    try{
    		return JPADaoFactory.getInstance().getTsccvUsuarioDAO().usuarioIsRegistrado(login, cedula, idEmpr);
	    } catch (BMBaseException e) {
	        throw e;
	    } finally {
	        EntityManagerHelper.closeEntityManager();
	    }
    }
    
    public void inactivarUsuario(Integer idUsua) throws Exception {
    	try{    
    		EntityManagerHelper.beginTransaction();
    		JPADaoFactory.getInstance().getTsccvUsuarioDAO().inactivarUsuario(idUsua);    		
    		EntityManagerHelper.commit();
    	}catch(Exception e){
    		EntityManagerHelper.rollback();
    		throw new Exception(e.getMessage());
    	}
    	finally{    		
    		EntityManagerHelper.closeEntityManager();
    	}
    }
    
    
    //Metodo encargado de consultar Usuarios por la propiedad login
    public TsccvUsuario consultarUsuarioLogin(String propertyName, final Object login) throws Exception{
    	try {
    		List<TsccvUsuario> registros = null;
    		
    		registros = JPADaoFactory.getInstance().getTsccvUsuarioDAO().findByLogin(login);
    		
    		if(registros != null && registros.size() > 0){
    			//retorna el usuario
    			return registros.get(0);
    		}
    		
    		return null;    		
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    //Metodo encargado de consultar Usuario dependiendo de una propiedad
    public List<TsccvUsuario> consultarUsuariosPorGrupo(Integer idGrup) throws Exception{
    	try {
            return JPADaoFactory.getInstance().getTsccvUsuarioDAO().consultarUsuariosPorGrupo(idGrup);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    public void cambiarPassword(TsccvUsuario tsccvUsuario, String passwordNuevo) throws BMBaseException, Exception {
    	try{    
    		EntityManagerHelper.beginTransaction();
    		int numUpdate = JPADaoFactory.getInstance().getTsccvUsuarioDAO().cambiarPassword(tsccvUsuario, passwordNuevo);
    		
    		if(numUpdate == 0){
    			throw new BMBaseException(37, null);
    			/*
	       		 CODIGO: 37
				 DSECRIPCION: La contrase�a de Usuario es incorrecta.
				 SOLUCION: Por favor ingr�sela correctamente o contacte a servicio al cliente.
	       		 */
    		}
    		
    		EntityManagerHelper.commit();
    	}catch (BMBaseException e) {
	        EntityManagerHelper.rollback();
	        throw e;
	    } catch (Exception e) {
	        EntityManagerHelper.rollback();
	        throw e;
	    }
    	finally{    		
    		EntityManagerHelper.closeEntityManager();
    	}
    }
    
    
    //Metodo utilizado para la consultar los usuarios con grupo vendedor
    public List<TsccvUsuario> consultarVendedores(Integer codCli, String codPla) throws Exception{
    	try {
    		return JPADaoFactory.getInstance().getTsccvUsuarioDAO().consultarVendedores(codCli, codPla);   
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
}
