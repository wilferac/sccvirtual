package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvGrupoEmpresaLogic {
    public List<TsccvGrupoEmpresa> getTsccvGrupoEmpresa()
        throws Exception;

    public void saveTsccvGrupoEmpresa(Date fechaCreacion, Integer idGrue)
        throws Exception;

    public void deleteTsccvGrupoEmpresa(Integer idGrue)
        throws Exception;

    public void updateTsccvGrupoEmpresa(Date fechaCreacion, Integer idGrue)
        throws Exception;

    public TsccvGrupoEmpresa getTsccvGrupoEmpresa(Integer idGrue)
        throws Exception;

    public List<TsccvGrupoEmpresa> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvGrupoEmpresa> findPageTsccvGrupoEmpresa(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvGrupoEmpresa() throws Exception;
    
    public Integer consultarUltimoIdGrue() throws Exception;
}
