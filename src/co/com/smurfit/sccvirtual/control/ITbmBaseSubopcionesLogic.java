package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


public interface ITbmBaseSubopcionesLogic {
    public List<TbmBaseSubopciones> getTbmBaseSubopciones()
        throws Exception;

    public void saveTbmBaseSubopciones(String activo, String descripcion,
        Date fechaCreacion, Long idSuop, String nombre, String url)
        throws Exception;

    public void deleteTbmBaseSubopciones(Long idSuop) throws Exception;

    public void updateTbmBaseSubopciones(String activo, String descripcion,
        Date fechaCreacion, Long idSuop, String nombre, String url)
        throws Exception;

    public TbmBaseSubopciones getTbmBaseSubopciones(Long idSuop)
        throws Exception;

    public List<TbmBaseSubopciones> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TbmBaseSubopciones> findPageTbmBaseSubopciones(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTbmBaseSubopciones() throws Exception;
}
