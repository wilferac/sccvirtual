package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvTipoAccion;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


public interface ITsccvTipoAccionLogic {
    public List<TsccvTipoAccion> getTsccvTipoAccion() throws Exception;

    public void saveTsccvTipoAccion(String descripcion, Date fechaCreacion,
        Long idTiac) throws Exception;

    public void deleteTsccvTipoAccion(Long idTiac) throws Exception;

    public void updateTsccvTipoAccion(String descripcion, Date fechaCreacion,
        Long idTiac) throws Exception;

    public TsccvTipoAccion getTsccvTipoAccion(Long idTiac)
        throws Exception;

    public List<TsccvTipoAccion> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvTipoAccion> findPageTsccvTipoAccion(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvTipoAccion() throws Exception;
}
