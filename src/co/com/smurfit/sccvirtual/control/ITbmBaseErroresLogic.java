package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


public interface ITbmBaseErroresLogic {
    public List<TbmBaseErrores> getTbmBaseErrores() throws Exception;

    public void saveTbmBaseErrores(String activo, Long codigo,
        String descripcion, Date fechaCreacion, Long idErr, String solucion)
        throws Exception;

    public void deleteTbmBaseErrores(Long idErr) throws Exception;

    public void updateTbmBaseErrores(String activo, Long codigo,
        String descripcion, Date fechaCreacion, Long idErr, String solucion)
        throws Exception;

    public TbmBaseErrores getTbmBaseErrores(Long idErr)
        throws Exception;

    public List<TbmBaseErrores> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TbmBaseErrores> findPageTbmBaseErrores(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTbmBaseErrores() throws Exception;
}
