package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvPedidosLogic {
    public List<TsccvPedidos> getTsccvPedidos() throws Exception;

    /*public void saveTsccvPedidos(String activo, Date fechaCreacion,
        Integer idPedi, Integer idEmpr_TsccvEmpresas, String observaciones, String registrado,
        String codpla_TsccvPlanta, Integer idUsua_TsccvUsuario, List<ProductosPedidoVO> listadoProductosPedido)
        throws Exception;
*/
    public void deleteTsccvPedidos(Integer idPedi) throws Exception;

    public void updateTsccvPedidos(String activo, Date fechaCreacion,
        Integer idPedi, Integer idEmpr_TsccvEmpresas, String observaciones, String registrado,
        String codpla_TsccvPlanta, Integer idUsua_TsccvUsuario, List<ProductosPedidoVO> listadoProductosPedido)
        throws Exception;

    public TsccvPedidos getTsccvPedidos(Integer idPedi)
        throws Exception;

    public List<TsccvPedidos> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvPedidos> findPageTsccvPedidos(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTsccvPedidos() throws Exception;
    
    public Integer consultarPedidoNoRegistrado(Integer idEmpr, String codPla, Integer idUsua) throws Exception;
    
    public TsccvPedidos consultarUltimoPedido() throws Exception;
}
