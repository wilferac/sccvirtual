package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


public interface ICarteraDivLogic {
    public List<CarteraDiv> getCarteraDiv() throws Exception;

    public void saveCarteraDiv(String coddiv, String desdiv)
        throws Exception;

    public void deleteCarteraDiv(String coddiv, String desdiv)
        throws Exception;

    public void updateCarteraDiv(String coddiv, String desdiv)
        throws Exception;

    public CarteraDiv getCarteraDiv(CarteraDivId id) throws Exception;

    public List<CarteraDiv> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<CarteraDiv> findPageCarteraDiv(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberCarteraDiv() throws Exception;
}
