package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.vo.ArchivoPDFVO;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvPdfsProductosLogic {
    public List<TsccvPdfsProductos> getTsccvPdfsProductos()
        throws Exception;

    public void saveTsccvPdfsProductos(String codScc, String codpla,
        Date fechaCreacion, String codpla_TsccvPlanta, List<ArchivoPDFVO> listadoPDFs)
        throws Exception;

    public void deleteTsccvPdfsProductos(String codScc, String codpla)
        throws Exception;

    public void updateTsccvPdfsProductos(String codScc, String codpla,
        Date fechaCreacion, String codpla_TsccvPlanta)
        throws Exception;

    public TsccvPdfsProductos getTsccvPdfsProductos(TsccvPdfsProductosId id)
        throws Exception;

    public List<TsccvPdfsProductos> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvPdfsProductos> findPageTsccvPdfsProductos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvPdfsProductos() throws Exception;
    
    public void saveTsccvPdfsProductosSinArchivo(String codScc, String codpla, Date fechaCreacion, String codpla_TsccvPlanta)throws Exception;
}
