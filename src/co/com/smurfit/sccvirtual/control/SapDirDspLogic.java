package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class SapDirDspLogic implements ISapDirDspLogic {
    public List<SapDirDsp> getSapDirDsp() throws Exception {
        List<SapDirDsp> list = new ArrayList<SapDirDsp>();

        try {
            list = JPADaoFactory.getInstance().getSapDirDspDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveSapDirDsp(String codcli, String coddir, String desdir,
        String numfax, String desprv, String desciu, String despai)
        throws Exception {
        SapDirDsp entity = null;

        try {
            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (coddir == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "coddir");
            }

            if ((coddir != null) &&
                    (Utilities.checkWordAndCheckWithlength(coddir, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "coddir");
            }

            if (desdir == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "desdir");
            }

            if ((desdir != null) &&
                    (Utilities.checkWordAndCheckWithlength(desdir, 80) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "desdir");
            }

            if (numfax == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numfax");
            }

            if ((numfax != null) &&
                    (Utilities.checkWordAndCheckWithlength(numfax, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numfax");
            }

            if (desprv == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "desprv");
            }

            if ((desprv != null) &&
                    (Utilities.checkWordAndCheckWithlength(desprv, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "desprv");
            }

            if (desciu == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "desciu");
            }

            if ((desciu != null) &&
                    (Utilities.checkWordAndCheckWithlength(desciu, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "desciu");
            }

            if (despai == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "despai");
            }

            if ((despai != null) &&
                    (Utilities.checkWordAndCheckWithlength(despai, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despai");
            }

            SapDirDspId idClass = new SapDirDspId();
            idClass.setCodcli(codcli);
            idClass.setCoddir(coddir);
            idClass.setDesdir(desdir);
            idClass.setNumfax(numfax);
            idClass.setDesprv(desprv);
            idClass.setDesciu(desciu);
            idClass.setDespai(despai);

            entity = getSapDirDsp(idClass);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }

            entity = new SapDirDsp();

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getSapDirDspDAO().save(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteSapDirDsp(String codcli, String coddir, String desdir,
        String numfax, String desprv, String desciu, String despai)
        throws Exception {
        SapDirDsp entity = null;

        if (codcli == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (coddir == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (desdir == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (numfax == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (desprv == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (desciu == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (despai == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        SapDirDspId idClass = new SapDirDspId();
        idClass.setCodcli(codcli);
        idClass.setCoddir(coddir);
        idClass.setDesdir(desdir);
        idClass.setNumfax(numfax);
        idClass.setDesprv(desprv);
        idClass.setDesciu(desciu);
        idClass.setDespai(despai);

        entity = getSapDirDsp(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getSapDirDspDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateSapDirDsp(String codcli, String coddir, String desdir,
        String numfax, String desprv, String desciu, String despai)
        throws Exception {
        SapDirDsp entity = null;

        try {
            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (coddir == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "coddir");
            }

            if ((coddir != null) &&
                    (Utilities.checkWordAndCheckWithlength(coddir, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "coddir");
            }

            if (desdir == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "desdir");
            }

            if ((desdir != null) &&
                    (Utilities.checkWordAndCheckWithlength(desdir, 80) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "desdir");
            }

            if (numfax == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numfax");
            }

            if ((numfax != null) &&
                    (Utilities.checkWordAndCheckWithlength(numfax, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numfax");
            }

            if (desprv == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "desprv");
            }

            if ((desprv != null) &&
                    (Utilities.checkWordAndCheckWithlength(desprv, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "desprv");
            }

            if (desciu == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "desciu");
            }

            if ((desciu != null) &&
                    (Utilities.checkWordAndCheckWithlength(desciu, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "desciu");
            }

            if (despai == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "despai");
            }

            if ((despai != null) &&
                    (Utilities.checkWordAndCheckWithlength(despai, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despai");
            }

            SapDirDspId idClass = new SapDirDspId();
            idClass.setCodcli(codcli);
            idClass.setCoddir(coddir);
            idClass.setDesdir(desdir);
            idClass.setNumfax(numfax);
            idClass.setDesprv(desprv);
            idClass.setDesciu(desciu);
            idClass.setDespai(despai);

            entity = getSapDirDsp(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getSapDirDspDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public SapDirDsp getSapDirDsp(SapDirDspId id) throws Exception {
        SapDirDsp entity = null;

        try {
            entity = JPADaoFactory.getInstance().getSapDirDspDAO().findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("SapDirDsp"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<SapDirDsp> findPageSapDirDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<SapDirDsp> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getSapDirDspDAO()
                                  .findPageSapDirDsp(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("SapDirDsp"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberSapDirDsp() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getSapDirDspDAO()
                                  .findTotalNumberSapDirDsp();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("SapDirDsp Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<SapDirDsp> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<SapDirDsp> list = new ArrayList<SapDirDsp>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getSapDirDspDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    //metodo encargado de consultar las direcciones de despacho para un cliente
    public List<SapDirDsp> consultarDireccionesCliente(String codCli) throws Exception{
    	try {
            return JPADaoFactory.getInstance().getSapDirDspDAO().consultarDireccionesCliente(codCli);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
}
