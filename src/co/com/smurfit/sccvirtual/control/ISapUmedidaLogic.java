package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


public interface ISapUmedidaLogic {
    public List<SapUmedida> getSapUmedida() throws Exception;

    public void saveSapUmedida(String codume, String desume)
        throws Exception;

    public void deleteSapUmedida(String codume, String desume)
        throws Exception;

    public void updateSapUmedida(String codume, String desume)
        throws Exception;

    public SapUmedida getSapUmedida(SapUmedidaId id) throws Exception;

    public List<SapUmedida> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<SapUmedida> findPageSapUmedida(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberSapUmedida() throws Exception;
}
