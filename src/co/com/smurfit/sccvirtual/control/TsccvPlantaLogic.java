package co.com.smurfit.sccvirtual.control;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import co.com.smurfit.dataaccess.dao.TsccvPlantaDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.ExceptionManager;
import co.com.smurfit.exceptions.ExceptionMessages;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.Utilities;


public class TsccvPlantaLogic implements ITsccvPlantaLogic {
    public List<TsccvPlanta> getTsccvPlanta() throws Exception {
        List<TsccvPlanta> list = new ArrayList<TsccvPlanta>();

        try {
            list = JPADaoFactory.getInstance().getTsccvPlantaDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveTsccvPlanta(String codpla, String despla, String direle,
        String planta, Long idApli_TsccvAplicaciones,
        Long idTipr_TsccvTipoProducto) throws Exception {
        TsccvPlanta entity = null;

        try {
            /*if (codpla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpla");
            }

            if ((codpla != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla");
            }*/

            if (despla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "despla");
            }

            if ((despla != null) &&
                    (Utilities.checkWordAndCheckWithlength(despla, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despla");
            }

            if ((direle != null) &&
                    (Utilities.checkWordAndCheckWithlength(direle, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "direle");
            }

            if ((planta != null) &&
                    (Utilities.checkWordAndCheckWithlength(planta, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "planta");
            }

            if ((idApli_TsccvAplicaciones != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idApli_TsccvAplicaciones, 18, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idApli_TsccvAplicaciones");
            }

            if ((idTipr_TsccvTipoProducto != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idTipr_TsccvTipoProducto, 18, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idTipr_TsccvTipoProducto");
            }

            ITsccvAplicacionesLogic logicTsccvAplicaciones1 = new TsccvAplicacionesLogic();
            ITsccvTipoProductoLogic logicTsccvTipoProducto2 = new TsccvTipoProductoLogic();

            TsccvAplicaciones tsccvAplicacionesClass = logicTsccvAplicaciones1.getTsccvAplicaciones(idApli_TsccvAplicaciones);
            TsccvTipoProducto tsccvTipoProductoClass = logicTsccvTipoProducto2.getTsccvTipoProducto(idTipr_TsccvTipoProducto);

            if (tsccvAplicacionesClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvTipoProductoClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            /*entity = getTsccvPlanta(codpla);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }*/

            entity = new TsccvPlanta();

            entity.setCodpla(codpla);
            entity.setDespla(despla);
            entity.setDirele(direle);
            entity.setPlanta(planta);

            entity.setTsccvAplicaciones(tsccvAplicacionesClass);
            entity.setTsccvTipoProducto(tsccvTipoProductoClass);

            EntityManagerHelper.beginTransaction();
            //Comentariamos el llamado inicial
            //JPADaoFactory.getInstance().getTsccvPlantaDAO().save(entity);
            //Llamamos el nuevo metodo que realiza el registro
            TsccvPlantaDAO.savePlanta(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTsccvPlanta(String codpla) throws Exception {
        TsccvPlanta entity = null;

        if (codpla == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        List<TsccvPdfsProductos> tsccvPdfsProductoses = null;
        List<TsccvPedidos> tsccvPedidoses = null;
        List<TsccvPlantasUsuario> tsccvPlantasUsuarios = null;
        List<TsccvUsuario> tsccvUsuarios = null;

        entity = getTsccvPlanta(codpla);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            tsccvPdfsProductoses = JPADaoFactory.getInstance()
                                                .getTsccvPdfsProductosDAO()
                                                .findByProperty("tsccvPlanta.codpla",
                    codpla, 0);

            if (Utilities.validationsList(tsccvPdfsProductoses) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild());
            }

            tsccvPedidoses = JPADaoFactory.getInstance().getTsccvPedidosDAO()
                                          .findByProperty("tsccvPlanta.codpla",
                    codpla, 0);

            if (Utilities.validationsList(tsccvPedidoses) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild());
            }

            tsccvPlantasUsuarios = JPADaoFactory.getInstance()
                                                .getTsccvPlantasUsuarioDAO()
                                                .findByProperty("tsccvPlanta.codpla",
                    codpla, 0);

            if (Utilities.validationsList(tsccvPlantasUsuarios) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild());
            }

            tsccvUsuarios = JPADaoFactory.getInstance().getTsccvUsuarioDAO()
                                         .findByProperty("tsccvPlanta.codpla",
                    codpla, 0);

            if (Utilities.validationsList(tsccvUsuarios) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild());
            }

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvPlantaDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTsccvPlanta(String codpla, String despla, String direle,
        String planta, Long idApli_TsccvAplicaciones,
        Long idTipr_TsccvTipoProducto) throws Exception {
        TsccvPlanta entity = null;

        try {
            if (codpla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpla");
            }

            if ((codpla != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla");
            }

            if (despla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "despla");
            }

            if ((despla != null) &&
                    (Utilities.checkWordAndCheckWithlength(despla, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despla");
            }

            if ((direle != null) &&
                    (Utilities.checkWordAndCheckWithlength(direle, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "direle");
            }

            if ((planta != null) &&
                    (Utilities.checkWordAndCheckWithlength(planta, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "planta");
            }

            if ((idApli_TsccvAplicaciones != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idApli_TsccvAplicaciones, 18, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idApli_TsccvAplicaciones");
            }

            if ((idTipr_TsccvTipoProducto != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idTipr_TsccvTipoProducto, 18, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idTipr_TsccvTipoProducto");
            }

            ITsccvAplicacionesLogic logicTsccvAplicaciones1 = new TsccvAplicacionesLogic();
            ITsccvTipoProductoLogic logicTsccvTipoProducto2 = new TsccvTipoProductoLogic();

            TsccvAplicaciones tsccvAplicacionesClass = logicTsccvAplicaciones1.getTsccvAplicaciones(idApli_TsccvAplicaciones);
            TsccvTipoProducto tsccvTipoProductoClass = logicTsccvTipoProducto2.getTsccvTipoProducto(idTipr_TsccvTipoProducto);

            if (tsccvAplicacionesClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tsccvTipoProductoClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            entity = getTsccvPlanta(codpla);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setCodpla(codpla);
            entity.setDespla(despla);
            entity.setDirele(direle);
            entity.setPlanta(planta);

            entity.setTsccvAplicaciones(tsccvAplicacionesClass);
            entity.setTsccvTipoProducto(tsccvTipoProductoClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvPlantaDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TsccvPlanta getTsccvPlanta(String codpla) throws Exception {
        TsccvPlanta entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPlantaDAO()
                                  .findById(codpla);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPlanta"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TsccvPlanta> findPageTsccvPlanta(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<TsccvPlanta> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPlantaDAO()
                                  .findPageTsccvPlanta(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPlanta"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTsccvPlanta() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPlantaDAO()
                                  .findTotalNumberTsccvPlanta();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPlanta Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TsccvPlanta> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TsccvPlanta> list = new ArrayList<TsccvPlanta>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTsccvPlantaDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    //Metodo utilizado para la consulta por todos los campos
    public List<TsccvPlanta> consultarPlantas(String codpla, String despla, String direle,
        String planta, Long idApli_TsccvAplicaciones, Long idTipr_TsccvTipoProducto) throws Exception {    	
        TsccvPlanta entity = null;
        List<TsccvPlanta> registros = null;
        
    	try {
	    	ITsccvTipoProductoLogic logicTsccvTipoProducto = new TsccvTipoProductoLogic();
	    	ITsccvAplicacionesLogic logicTsccvAplicaciones = new TsccvAplicacionesLogic();            
	    	
	    	entity = new TsccvPlanta();
	    	entity.setCodpla(codpla);
    		
    		//SE AGREGA VALIDACION AL CAMPO TIPO PRODUCTO YA QUE NO ES OBLIGATORIO
    		if(idTipr_TsccvTipoProducto != null){
    			TsccvTipoProducto tsccvTipoProducto = logicTsccvTipoProducto.getTsccvTipoProducto(idTipr_TsccvTipoProducto);
    			entity.setTsccvTipoProducto(tsccvTipoProducto);
    		}
    		
    		//SE AGREGA VALIDACION AL CAMPO APLICACION YA QUE NO ES OBLIGATORIO
    		if(idApli_TsccvAplicaciones != null){
    			TsccvAplicaciones tsccvAplicaciones = logicTsccvAplicaciones.getTsccvAplicaciones(idApli_TsccvAplicaciones);
    			entity.setTsccvAplicaciones(tsccvAplicaciones);
    		}
    		
    		entity.setDespla(despla);
    		entity.setPlanta(planta);
    		entity.setDirele(direle);	    	
	    	
    		registros = JPADaoFactory.getInstance().getTsccvPlantaDAO().consultarPlantas(entity);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return registros;
    }
    
    //consulta las planta con los datos para saber si esta registrado
    public Boolean plantaIsRegistrada(String desPla) throws Exception{
    	try{
	    	return JPADaoFactory.getInstance().getTsccvPlantaDAO().plantaIsRegistrada(desPla);
	    } catch (Exception e) {
	        throw new Exception(e.getMessage());
	    } finally {
	        EntityManagerHelper.closeEntityManager();
	    }
    }
    
    /*
    //se agrega este metodo para consultar las plantas por usuario
    public List<TsccvPlanta> consultarPlantasPorUsuario(Integer idUser) throws Exception{
    	List<TsccvPlanta> registros = null;

        try {
        	registros = JPADaoFactory.getInstance().getTsccvPlantaDAO().consultarPlantasPorUsuario(idUser);
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return registros;
    }
    */   
    
    public List<TsccvPlanta> findByActivoAplicacion(String activo, Long aplicacion)  throws Exception {
        List<TsccvPlanta> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPlantaDAO()
                                  .findByActivoAplicacion(activo, aplicacion);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPlanta"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }
    
    public List<TsccvPlanta> findByUsuarioAndAplicacion(Integer usuario, Integer aplicacion, String activoApp) throws Exception {
        List<TsccvPlanta> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvPlantaDAO().findByUsuarioAndAplicacion(usuario, aplicacion, activoApp);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPlanta"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }
    
    //metodo ebcargado de consultar las plantas dependiendo de:
    //si la empresa (cliente) tiene cod_vpe entonces consulta las plantas con tipo de producto corrugado o sacos.
    //si la empresa (cliente) tiene cod_mas entonces consulta las plantas con tipo de producto molinos.
    public List<TsccvPlanta> findByTipProdAndAplicacion(String tipoCodEmpr, Long aplicacion) throws Exception {
        List<TsccvPlanta> entity = null;
        String condicion = "";
        //Se crea un objeto del archivo propiedades		
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        
        try {
        	if(tipoCodEmpr.equals("vpe")){
        		condicion = "(model.tsccvTipoProducto.idTipr = '"+archivoPropiedades.getProperty("PRODUCTO_CORRUGADO")+"' or " +
        					"model.tsccvTipoProducto.idTipr = '"+archivoPropiedades.getProperty("PRODUCTO_SACOS")+"')";
        	}
        	else{
        		condicion = "model.tsccvTipoProducto.idTipr = '"+archivoPropiedades.getProperty("PRODUCTO_MOLINOS")+"'";
        	}
            entity = JPADaoFactory.getInstance().getTsccvPlantaDAO().findByTipProdAndAplicacion(condicion, aplicacion);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvPlanta"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }
}
