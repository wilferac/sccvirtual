package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


public interface ITsccvTipoProductoLogic {
    public List<TsccvTipoProducto> getTsccvTipoProducto()
        throws Exception;

    public void saveTsccvTipoProducto(String activo, String descripcion,
        Date fechaCreacion, Long idTipr) throws Exception;

    public void deleteTsccvTipoProducto(Long idTipr) throws Exception;

    public void updateTsccvTipoProducto(String activo, String descripcion,
        Date fechaCreacion, Long idTipr) throws Exception;

    public TsccvTipoProducto getTsccvTipoProducto(Long idTipr)
        throws Exception;

    public List<TsccvTipoProducto> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvTipoProducto> findPageTsccvTipoProducto(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvTipoProducto() throws Exception;
}
