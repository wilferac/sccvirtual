package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class TbmBaseOpcionesGruposLogic implements ITbmBaseOpcionesGruposLogic {
    public List<TbmBaseOpcionesGrupos> getTbmBaseOpcionesGrupos()
        throws Exception {
        List<TbmBaseOpcionesGrupos> list = new ArrayList<TbmBaseOpcionesGrupos>();

        try {
            list = JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO()
                                .findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveTbmBaseOpcionesGrupos(Long idOpci, Long idGrup,
        Date fechaCreacion, Long idGrup_TbmBaseGruposUsuarios,
        Long idOpci_TbmBaseOpciones, List<TbmBaseOpcionesGrupos> listadoOpcionesGrupo) throws Exception {
        TbmBaseOpcionesGrupos entity = null;

        try {
        	//se crean las plantasUsuario
            if(listadoOpcionesGrupo != null){  
            	for(TbmBaseOpcionesGrupos opciGrup : listadoOpcionesGrupo){
            		if(!opciGrup.getTbmBaseOpciones().getActivo().equals("e")){
            			continue;
            		}
            		
            		idOpci = opciGrup.getTbmBaseOpciones().getIdOpci();
            		idOpci_TbmBaseOpciones = opciGrup.getTbmBaseOpciones().getIdOpci();
            		
		            if (idOpci == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Id Opci�n");
		            }
		
		            if ((idOpci != null) &&
		                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
		                        idOpci, 2, 0) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "Id Opci�n");
		            }
		
		            if (idGrup == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Id Grupo");
		            }
		
		            if ((idGrup != null) &&
		                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
		                        idGrup, 3, 0) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "Id Grupo");
		            }
		
		            if (fechaCreacion == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "Fecha Creaci�n");
		            }
		
		            if (idGrup_TbmBaseGruposUsuarios == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "Id Grupo Usuario");
		            }
		
		            if ((idGrup_TbmBaseGruposUsuarios != null) &&
		                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
		                        idGrup_TbmBaseGruposUsuarios, 3, 0) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "Id Grupo Usuario");
		            }
		
		            if (idOpci_TbmBaseOpciones == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "Id Opci�n");
		            }
		
		            if ((idOpci_TbmBaseOpciones != null) &&
		                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
		                        idOpci_TbmBaseOpciones, 2, 0) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "Id Opci�n");
		            }
		
		            ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios1 = new TbmBaseGruposUsuariosLogic();
		            ITbmBaseOpcionesLogic logicTbmBaseOpciones2 = new TbmBaseOpcionesLogic();
		
		            TbmBaseGruposUsuarios tbmBaseGruposUsuariosClass = logicTbmBaseGruposUsuarios1.getTbmBaseGruposUsuarios(idGrup_TbmBaseGruposUsuarios);
		            TbmBaseOpciones tbmBaseOpcionesClass = logicTbmBaseOpciones2.getTbmBaseOpciones(idOpci_TbmBaseOpciones);
		
		            if (tbmBaseGruposUsuariosClass == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL+"Grupo Usuario");
		            }
		
		            if (tbmBaseOpcionesClass == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL+"Opci�n");
		            }
		
		            TbmBaseOpcionesGruposId idClass = new TbmBaseOpcionesGruposId();
		            idClass.setIdOpci(idOpci);
		            idClass.setIdGrup(idGrup);		            
		            
		            entity = getTbmBaseOpcionesGrupos(idClass);
		
		            if (entity != null) {
		                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
		            }
		
		            entity = new TbmBaseOpcionesGrupos();
		
		            //se consulta el ultimo orden
		            Object ultimoOrden = JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO().consultarUltimoOrden(idGrup);
		            if(ultimoOrden != null){
		            	//si ya hay una opcion registradas de incrementa el valor en uno
		            	entity.setOrden((((BigDecimal)ultimoOrden).longValue())+1);
		            }
		            else{
		            	//si no se guardo como primera
		            	entity.setOrden(new Long(1));
		            }
		            
		            
		            entity.setFechaCreacion(fechaCreacion);
		            entity.setId(idClass);
		
		            entity.setTbmBaseGruposUsuarios(tbmBaseGruposUsuariosClass);
		            entity.setTbmBaseOpciones(tbmBaseOpcionesClass);
		
		            EntityManagerHelper.beginTransaction();
		            JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO().save(entity);
		            EntityManagerHelper.commit();
            	}
            }
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTbmBaseOpcionesGrupos(Long idOpci, Long idGrup)
        throws Exception {
        TbmBaseOpcionesGrupos entity = null;

        if (idOpci == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (idGrup == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        TbmBaseOpcionesGruposId idClass = new TbmBaseOpcionesGruposId();
        idClass.setIdOpci(idOpci);
        idClass.setIdGrup(idGrup);

        entity = getTbmBaseOpcionesGrupos(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO()
                         .delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
            
            JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO().restablecerPosiciones(entity.getOrden(), idGrup);
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTbmBaseOpcionesGrupos(Long idOpci, Long idGrup,
        Date fechaCreacion, Long idGrup_TbmBaseGruposUsuarios,
        Long idOpci_TbmBaseOpciones) throws Exception {
        TbmBaseOpcionesGrupos entity = null;

        try {
            if (idOpci == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Id Opci�n");
            }

            if ((idOpci != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idOpci, 2, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Id Opci�n");
            }

            if (idGrup == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Id Grupo");
            }

            if ((idGrup != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idGrup, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Id Grupo");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creaci�n");
            }

            if (idGrup_TbmBaseGruposUsuarios == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Id Grupo Usuario");
            }

            if ((idGrup_TbmBaseGruposUsuarios != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idGrup_TbmBaseGruposUsuarios, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Id Grupo Usuario");
            }

            if (idOpci_TbmBaseOpciones == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Id Opci�n");
            }

            if ((idOpci_TbmBaseOpciones != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idOpci_TbmBaseOpciones, 2, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Id Opci�n");
            }

            ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios1 = new TbmBaseGruposUsuariosLogic();
            ITbmBaseOpcionesLogic logicTbmBaseOpciones2 = new TbmBaseOpcionesLogic();

            TbmBaseGruposUsuarios tbmBaseGruposUsuariosClass = logicTbmBaseGruposUsuarios1.getTbmBaseGruposUsuarios(idGrup_TbmBaseGruposUsuarios);
            TbmBaseOpciones tbmBaseOpcionesClass = logicTbmBaseOpciones2.getTbmBaseOpciones(idOpci_TbmBaseOpciones);

            if (tbmBaseGruposUsuariosClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            if (tbmBaseOpcionesClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            TbmBaseOpcionesGruposId idClass = new TbmBaseOpcionesGruposId();
            idClass.setIdOpci(idOpci);
            idClass.setIdGrup(idGrup);

            entity = getTbmBaseOpcionesGrupos(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setFechaCreacion(fechaCreacion);
            entity.setId(idClass);

            entity.setTbmBaseGruposUsuarios(tbmBaseGruposUsuariosClass);
            entity.setTbmBaseOpciones(tbmBaseOpcionesClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO()
                         .update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TbmBaseOpcionesGrupos getTbmBaseOpcionesGrupos(
        TbmBaseOpcionesGruposId id) throws Exception {
        TbmBaseOpcionesGrupos entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO()
                                  .findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseOpcionesGrupos"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TbmBaseOpcionesGrupos> findPageTbmBaseOpcionesGrupos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        List<TbmBaseOpcionesGrupos> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO()
                                  .findPageTbmBaseOpcionesGrupos(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseOpcionesGrupos"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTbmBaseOpcionesGrupos()
        throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO()
                                  .findTotalNumberTbmBaseOpcionesGrupos();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseOpcionesGrupos Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TbmBaseOpcionesGrupos> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TbmBaseOpcionesGrupos> list = new ArrayList<TbmBaseOpcionesGrupos>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    //Metodo encargado de consultar opcionesgrupos dependiendo de una propiedad
    public List<TbmBaseOpcionesGrupos> findByProperty(String propertyName,final Object value) throws Exception{
    	try {
            return JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO().findByProperty(propertyName, value);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
	//metodo encargado de modificar el orden de la opcion
    public void cambiarOrden(TbmBaseOpcionesGrupos tbmBaseOpcionesGrupos, String orden) throws Exception{
    	try {
    		
            JPADaoFactory.getInstance().getTbmBaseOpcionesGruposDAO().cambiarOrden(tbmBaseOpcionesGrupos, orden);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
}
