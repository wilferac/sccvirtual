package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;

import java.math.BigDecimal;

import java.util.*;


public interface ISapHistDespachosLogic {
    public List<SapHistDespachos> getSapHistDespachos()
        throws Exception;

    public void saveSapHistDespachos(String codpro, String nummes,
        String codcli, String cantid, String despro, String undmed,
        String cankgs) throws Exception;

    public void deleteSapHistDespachos(String codpro, String nummes,
        String codcli, String cantid, String despro, String undmed,
        String cankgs) throws Exception;

    public void updateSapHistDespachos(String codpro, String nummes,
        String codcli, String cantid, String despro, String undmed,
        String cankgs) throws Exception;

    public SapHistDespachos getSapHistDespachos(SapHistDespachosId id)
        throws Exception;

    public List<SapHistDespachos> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<SapHistDespachos> findPageSapHistDespachos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberSapHistDespachos() throws Exception;
    
    public List<SapHistDespachos> consultarHistoricoDespachoSap(String codMas, String periodo) throws Exception;
    
    public String minPeriodo(String codMas) throws Exception;
}
