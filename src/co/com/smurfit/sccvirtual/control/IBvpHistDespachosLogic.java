package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;

import java.math.BigDecimal;

import java.util.*;


public interface IBvpHistDespachosLogic {
    public List<BvpHistDespachos> getBvpHistDespachos()
        throws Exception;

    public void saveBvpHistDespachos(String planta, String nummes,
        String codcli, String codscc, String codpro, String despro,
        String tippro, String undmed, String cantid) throws Exception;

    public void deleteBvpHistDespachos(String planta, String nummes,
        String codcli, String codscc, String codpro, String despro,
        String tippro, String undmed, String cantid) throws Exception;

    public void updateBvpHistDespachos(String planta, String nummes,
        String codcli, String codscc, String codpro, String despro,
        String tippro, String undmed, String cantid) throws Exception;

    public BvpHistDespachos getBvpHistDespachos(BvpHistDespachosId id)
        throws Exception;

    public List<BvpHistDespachos> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<BvpHistDespachos> findPageBvpHistDespachos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberBvpHistDespachos() throws Exception;
    
    public List<BvpHistDespachos> consultarHistoricoDespachoBvp(String codBvp, TsccvPlanta tsccvPlanta, String periodo) throws Exception;
    
    public String minPeriodo(String codVpe, TsccvPlanta tsccvPlanta) throws Exception;
}
