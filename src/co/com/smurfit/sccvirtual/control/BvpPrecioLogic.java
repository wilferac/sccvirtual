package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.Utilities;

import java.io.File;
import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;


public class BvpPrecioLogic implements IBvpPrecioLogic {
    public List<BvpPrecio> getBvpPrecio() throws Exception {
        List<BvpPrecio> list = new ArrayList<BvpPrecio>();

        try {
            list = JPADaoFactory.getInstance().getBvpPrecioDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveBvpPrecio(String codCli, String codpla, String codScc,
        String precio, String alto, String ancho, String ancsac, String clave,
        String cntmax, String cntmin, String codpro, String despro,
        String estilo, String fuelle, String hijpro, String kitpro,
        String largo, String larsac, String moneda, String numcol,
        String refere, String tippro, String undmed) throws Exception {
        BvpPrecio entity = null;

        try {
            if (codCli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codCli");
            }

            if ((codCli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codCli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codCli");
            }

            if (codpla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpla");
            }

            if ((codpla != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla");
            }

            if (codScc == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codScc");
            }

            if ((codScc != null) &&
                    (Utilities.checkWordAndCheckWithlength(codScc, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codScc");
            }

            if (precio == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "precio");
            }

            if ((precio != null) &&
                    (Utilities.checkWordAndCheckWithlength(precio, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "precio");
            }

            if ((alto != null) &&
                    (Utilities.checkWordAndCheckWithlength(alto, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH + "alto");
            }

            if ((ancho != null) &&
                    (Utilities.checkWordAndCheckWithlength(ancho, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ancho");
            }

            if ((ancsac != null) &&
                    (Utilities.checkWordAndCheckWithlength(ancsac, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ancsac");
            }

            if ((clave != null) &&
                    (Utilities.checkWordAndCheckWithlength(clave, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "clave");
            }

            if ((cntmax != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntmax, 17) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntmax");
            }

            if ((cntmin != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntmin, 17) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntmin");
            }

            if ((codpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpro, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpro");
            }

            if ((despro != null) &&
                    (Utilities.checkWordAndCheckWithlength(despro, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despro");
            }

            if ((estilo != null) &&
                    (Utilities.checkWordAndCheckWithlength(estilo, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "estilo");
            }

            if ((fuelle != null) &&
                    (Utilities.checkWordAndCheckWithlength(fuelle, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fuelle");
            }

            if ((hijpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(hijpro, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "hijpro");
            }

            if ((kitpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(kitpro, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "kitpro");
            }

            if ((largo != null) &&
                    (Utilities.checkWordAndCheckWithlength(largo, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "largo");
            }

            if ((larsac != null) &&
                    (Utilities.checkWordAndCheckWithlength(larsac, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "larsac");
            }

            if ((moneda != null) &&
                    (Utilities.checkWordAndCheckWithlength(moneda, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "moneda");
            }

            if ((numcol != null) &&
                    (Utilities.checkWordAndCheckWithlength(numcol, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numcol");
            }

            if ((refere != null) &&
                    (Utilities.checkWordAndCheckWithlength(refere, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "refere");
            }

            if ((tippro != null) &&
                    (Utilities.checkWordAndCheckWithlength(tippro, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "tippro");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            BvpPrecioId idClass = new BvpPrecioId();
            idClass.setCodCli(codCli);
            idClass.setCodpla(codpla);
            idClass.setCodScc(codScc);
            idClass.setPrecio(precio);

            entity = getBvpPrecio(idClass);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }

            entity = new BvpPrecio();

            entity.setAlto(alto);
            entity.setAncho(ancho);
            entity.setAncsac(ancsac);
            entity.setClave(clave);
            entity.setCntmax(cntmax);
            entity.setCntmin(cntmin);
            entity.setCodpro(codpro);
            entity.setDespro(despro);
            entity.setEstilo(estilo);
            entity.setFuelle(fuelle);
            entity.setHijpro(hijpro);
            entity.setId(idClass);
            entity.setKitpro(kitpro);
            entity.setLargo(largo);
            entity.setLarsac(larsac);
            entity.setMoneda(moneda);
            entity.setNumcol(numcol);
            entity.setRefere(refere);
            entity.setTippro(tippro);
            entity.setUndmed(undmed);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getBvpPrecioDAO().save(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteBvpPrecio(String codCli, String codpla, String codScc,
        String precio) throws Exception {
        BvpPrecio entity = null;

        if (codCli == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (codpla == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (codScc == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (precio == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        BvpPrecioId idClass = new BvpPrecioId();
        idClass.setCodCli(codCli);
        idClass.setCodpla(codpla);
        idClass.setCodScc(codScc);
        idClass.setPrecio(precio);

        entity = getBvpPrecio(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getBvpPrecioDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateBvpPrecio(String codCli, String codpla, String codScc,
        String precio, String alto, String ancho, String ancsac, String clave,
        String cntmax, String cntmin, String codpro, String despro,
        String estilo, String fuelle, String hijpro, String kitpro,
        String largo, String larsac, String moneda, String numcol,
        String refere, String tippro, String undmed) throws Exception {
        BvpPrecio entity = null;

        try {
            if (codCli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codCli");
            }

            if ((codCli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codCli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codCli");
            }

            if (codpla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpla");
            }

            if ((codpla != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpla");
            }

            if (codScc == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codScc");
            }

            if ((codScc != null) &&
                    (Utilities.checkWordAndCheckWithlength(codScc, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codScc");
            }

            if (precio == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "precio");
            }

            if ((precio != null) &&
                    (Utilities.checkWordAndCheckWithlength(precio, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "precio");
            }

            if ((alto != null) &&
                    (Utilities.checkWordAndCheckWithlength(alto, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH + "alto");
            }

            if ((ancho != null) &&
                    (Utilities.checkWordAndCheckWithlength(ancho, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ancho");
            }

            if ((ancsac != null) &&
                    (Utilities.checkWordAndCheckWithlength(ancsac, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ancsac");
            }

            if ((clave != null) &&
                    (Utilities.checkWordAndCheckWithlength(clave, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "clave");
            }

            if ((cntmax != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntmax, 17) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntmax");
            }

            if ((cntmin != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntmin, 17) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntmin");
            }

            if ((codpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpro, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpro");
            }

            if ((despro != null) &&
                    (Utilities.checkWordAndCheckWithlength(despro, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despro");
            }

            if ((estilo != null) &&
                    (Utilities.checkWordAndCheckWithlength(estilo, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "estilo");
            }

            if ((fuelle != null) &&
                    (Utilities.checkWordAndCheckWithlength(fuelle, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fuelle");
            }

            if ((hijpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(hijpro, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "hijpro");
            }

            if ((kitpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(kitpro, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "kitpro");
            }

            if ((largo != null) &&
                    (Utilities.checkWordAndCheckWithlength(largo, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "largo");
            }

            if ((larsac != null) &&
                    (Utilities.checkWordAndCheckWithlength(larsac, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "larsac");
            }

            if ((moneda != null) &&
                    (Utilities.checkWordAndCheckWithlength(moneda, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "moneda");
            }

            if ((numcol != null) &&
                    (Utilities.checkWordAndCheckWithlength(numcol, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numcol");
            }

            if ((refere != null) &&
                    (Utilities.checkWordAndCheckWithlength(refere, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "refere");
            }

            if ((tippro != null) &&
                    (Utilities.checkWordAndCheckWithlength(tippro, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "tippro");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            BvpPrecioId idClass = new BvpPrecioId();
            idClass.setCodCli(codCli);
            idClass.setCodpla(codpla);
            idClass.setCodScc(codScc);
            idClass.setPrecio(precio);

            entity = getBvpPrecio(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setAlto(alto);
            entity.setAncho(ancho);
            entity.setAncsac(ancsac);
            entity.setClave(clave);
            entity.setCntmax(cntmax);
            entity.setCntmin(cntmin);
            entity.setCodpro(codpro);
            entity.setDespro(despro);
            entity.setEstilo(estilo);
            entity.setFuelle(fuelle);
            entity.setHijpro(hijpro);
            entity.setId(idClass);
            entity.setKitpro(kitpro);
            entity.setLargo(largo);
            entity.setLarsac(larsac);
            entity.setMoneda(moneda);
            entity.setNumcol(numcol);
            entity.setRefere(refere);
            entity.setTippro(tippro);
            entity.setUndmed(undmed);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getBvpPrecioDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public BvpPrecio getBvpPrecio(BvpPrecioId id) throws Exception {
        BvpPrecio entity = null;

        try {
            entity = JPADaoFactory.getInstance().getBvpPrecioDAO().findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("BvpPrecio"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<BvpPrecio> findPageBvpPrecio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<BvpPrecio> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getBvpPrecioDAO()
                                  .findPageBvpPrecio(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("BvpPrecio"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberBvpPrecio() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getBvpPrecioDAO()
                                  .findTotalNumberBvpPrecio();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("BvpPrecio Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<BvpPrecio> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<BvpPrecio> list = new ArrayList<BvpPrecio>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getBvpPrecioDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    
    //metodo encargdo de consultar los pdf falatantes
    public List<ProductoVO> consultarPdfsFaltantes(String codPla) throws Exception {    	
        List<ProductoVO> registros = null;
        
    	try {	    	
    		if(codPla == null){
    			throw new Exception(ExceptionMessages.VARIABLE_NULL + "Planta");
    		}
    		
    		ITsccvPlantaLogic logicTsccvPlanta = new TsccvPlantaLogic();
	    	TsccvPlanta tsccvPlanta = logicTsccvPlanta.getTsccvPlanta(codPla);
    		
	    	registros = JPADaoFactory.getInstance().getBvpPrecioDAO().consultarPdfsFaltantes(tsccvPlanta);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return registros;
    }
    
    //metodo encargado de consultar los productos por codigoProd o descripcion y codigoCliente
    public List<ProductoVO> consultarProductosBvp(String codigoProd, String descripcion, String coddigoCli, TsccvPlanta tsccvPlanta, String nombreColumna, Boolean ascendente) throws Exception{
    	List<ProductoVO> registros = null;
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
    	try {	    	
    		ITsccvEmpresasLogic tsccvEmpresasLogic = new TsccvEmpresasLogic();
        	TsccvEmpresas tsccvEmpresas = tsccvEmpresasLogic.getTsccvEmpresas(new Integer(coddigoCli));
        	
    		registros = JPADaoFactory.getInstance().getBvpPrecioDAO().consultarProductosBvp(codigoProd, descripcion, tsccvEmpresas.getCodVpe(), tsccvPlanta, 
																    						(nombreColumna != null && !nombreColumna.equals("")) ? nombreColumna : "DESPRO", 
																							(nombreColumna != null && !nombreColumna.equals("")) ? ascendente : true);
    		    		
    		String basePath = archivoPropiedades.getProperty("RUTA_PDF")+tsccvPlanta.getDespla().trim()+"\\";
    		String nombreArchivo = null;
    		
    		//se recorre la lista de rsultados para validar que productos tienen pdf asociado con base en el archivo existente
    		for(ProductoVO producto : registros){
    			nombreArchivo = tsccvPlanta.getDespla()+"\\"+tsccvPlanta.getCodpla().trim()+"-"+producto.getBvpPrecio().getId().getCodScc().trim();
				
				//se crea el archivo y se verifica si existe
    			File archivo = new File(basePath + tsccvPlanta.getCodpla().trim()+"-"+producto.getBvpPrecio().getId().getCodScc().trim()+".pdf");	    			
    			System.out.println(archivo.getPath());
    			//si el archivo existe se setea el nombre del archivo
    			if(archivo.exists()){    				
    				//producto.setNombreArchivo(nombreArchivo);
    				producto.setNombreArchivo(Utilities.RUTA_SERVER_PDFS+tsccvPlanta.getDespla().trim()+"/"+tsccvPlanta.getCodpla().trim()+"-"+producto.getBvpPrecio().getId().getCodScc().trim()+".pdf");
    			}
    			/*else if(archivo.exists()){
    				System.out.println("El archivo ["+archivo.getAbsolutePath()+"] existe pero no puede ser leido.");
    			}*/
    		}
    		
    		return registros;
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    //metodo encargado de consultar los precios para armar la lista de precios (metodo utilizado en la pantalla lista precios)
    public List<ProductoVO> consultarPreciosBvp(String codigoProd, String descripcion, String coddigoCli, TsccvPlanta tsccvPlanta, String nombreColumna, Boolean ascendente) throws Exception{
    	List<ProductoVO> registros = null;
    	ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades(); 
    	try {
    		ITsccvEmpresasLogic tsccvEmpresasLogic = new TsccvEmpresasLogic();
        	TsccvEmpresas tsccvEmpresas = tsccvEmpresasLogic.getTsccvEmpresas(new Integer(coddigoCli));
        	
    		registros = JPADaoFactory.getInstance().getBvpPrecioDAO().consultarPreciosBvp(codigoProd, descripcion, tsccvEmpresas.getCodVpe(), tsccvPlanta, 
    														(nombreColumna != null && !nombreColumna.equals("")) ? nombreColumna : "DESPRO", 
															(nombreColumna != null && !nombreColumna.equals("")) ? ascendente : true);
    		
    		String basePath = archivoPropiedades.getProperty("RUTA_PDF")+tsccvPlanta.getDespla().trim()+"\\";
    		String nombreArchivo = null;
    		String escala = "";
    		
    		//se recorre la lista de rsultados para validar que productos tienen pdf asociado con base en el archivo existente
    		for(ProductoVO producto : registros){
    			//validacion copiada de la aplicacion actual
				if (!(Double.parseDouble(producto.getBvpPrecio().getCntmin()) <= 500 && Double.parseDouble(producto.getBvpPrecio().getCntmax()) == 9999999)){
					escala = " ("+producto.getBvpPrecio().getCntmin()+"-"+producto.getBvpPrecio().getCntmax()+" unds.)";
					producto.getBvpPrecio().setDespro(producto.getBvpPrecio().getDespro()+escala);
				}
				
				nombreArchivo = tsccvPlanta.getDespla().trim()+"\\"+tsccvPlanta.getCodpla().trim()+"-"+producto.getBvpPrecio().getId().getCodScc().trim();
				producto.setNombreArchivo(null);
				//se crea el archivo y se verifica si existe
    			File archivo = new File(basePath + tsccvPlanta.getCodpla().trim()+"-"+producto.getBvpPrecio().getId().getCodScc().trim()+".pdf");
    			System.out.println(archivo.getPath());
    			//si el archivo existe se setea el nombre del archivo
    			if(archivo.exists()){    				
    				//producto.setNombreArchivo(nombreArchivo);
    				producto.setNombreArchivo(Utilities.RUTA_SERVER_PDFS+tsccvPlanta.getDespla().trim()+"/"+tsccvPlanta.getCodpla().trim()+"-"+producto.getBvpPrecio().getId().getCodScc().trim()+".pdf");
    			}
    			/*else if(archivo.exists()){	    				
    				System.out.println("El archivo ["+archivo.getAbsolutePath()+"] existe pero no puede ser leido.");
    			}*/  
    		}
    		
    		return registros;
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    //metodo encargado de verificar si un producto esta registrado
    public Boolean precioIsRegistrado(String codScc, String codPla) throws Exception {        
        try {        	
            return JPADaoFactory.getInstance().getBvpPrecioDAO().precioIsRegistrado(codScc, codPla);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    
    //metodo encargado de consultar la cantidad minima y maxima de cada precio bvp para armar las escalas
    public Map consultarEscalasBvp(List<ProductosPedidoVO> listadoProductosPedido, String codCli) throws Exception {        
        try {
            return JPADaoFactory.getInstance().getBvpPrecioDAO().consultarEscalasBvp(listadoProductosPedido, codCli);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    //metodo encargado de consultar el precio de cada producto pedido con base en la cantidad total de este
    public List<ProductosPedidoVO> consultarPrecioPorCantidadTotal(List<ProductosPedidoVO> listadoProductosPedido, String coddigoCli)throws Exception {    
        try {
        	String precio = null;
        	
        	ITsccvEmpresasLogic tsccvEmpresasLogic = new TsccvEmpresasLogic();
        	TsccvEmpresas tsccvEmpresas = tsccvEmpresasLogic.getTsccvEmpresas(new Integer(coddigoCli));
        	
        	for(ProductosPedidoVO prodPedVO : listadoProductosPedido){
        		precio = JPADaoFactory.getInstance().getBvpPrecioDAO().consultarPrecioPorCantidadTotal(prodPedVO.getTsccvProductosPedido().getCodScc(),
        												tsccvEmpresas.getCodVpe(), prodPedVO.getTsccvProductosPedido().getCodpla(), prodPedVO.getTsccvProductosPedido().getCantidadTotal());
        		if(precio == null || precio.equals("")){
        			throw new Exception("No existe precio definido para la Cantidad Total del producto "+prodPedVO.getTsccvProductosPedido().getDespro());
        		}
        		
        		prodPedVO.getTsccvProductosPedido().setPrecio(precio);
        	}
            
            
            return listadoProductosPedido;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
}
