package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;

import java.math.BigDecimal;

import java.util.*;


public interface IBvpDetDspLogic {
    public List<BvpDetDsp> getBvpDetDsp() throws Exception;

    public void saveBvpDetDsp(String planta, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String fecdsp,
        String hordsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp) throws Exception;

    public void deleteBvpDetDsp(String planta, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String fecdsp,
        String hordsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp) throws Exception;

    public void updateBvpDetDsp(String planta, String numdsp, String itmdsp,
        String codcli, String numped, String itmped, String fecdsp,
        String hordsp, String candsp, String undmed, String emptra,
        String placa, String nomcon, String dirdsp) throws Exception;

    public BvpDetDsp getBvpDetDsp(BvpDetDspId id) throws Exception;

    public List<BvpDetDsp> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<BvpDetDsp> findPageBvpDetDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberBvpDetDsp() throws Exception;
    
    public List<BvpDetDsp> findByProperty(String propertyName,final Object value) throws Exception;
        
    public List<BvpDetDsp> consultarDetalleDespachoBvp(String numPed, String itemPed, String codPla, String codVpe) throws Exception;;
}
