package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;

import java.math.BigDecimal;

import java.util.*;


public interface ITsccvEntregasPedidoLogic {
    public List<TsccvEntregasPedido> getTsccvEntregasPedido()
        throws Exception;

    /*public void saveTsccvEntregasPedido(String anchoHojas, String anchoRollo,
            Integer cantidad, String desdir, Date fechaEntrega, Integer idEnpe,
            String largoHojas, Integer idPrpe_TsccvProductosPedido, List<TsccvEntregasPedido> listadoEntregasPedido)
        throws Exception;
*/
    public void deleteTsccvEntregasPedido(Integer idEnpe)
        throws Exception;

    public void updateTsccvEntregasPedido(String anchoHojas, String anchoRollo,
            Integer cantidad, String desdir, Date fechaEntrega, Integer idEnpe,
            String largoHojas, Integer idPrpe_TsccvProductosPedido, String precio)
        throws Exception;

    public TsccvEntregasPedido getTsccvEntregasPedido(Integer idEnpe)
        throws Exception;

    public List<TsccvEntregasPedido> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TsccvEntregasPedido> findPageTsccvEntregasPedido(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberTsccvEntregasPedido() throws Exception;
    
    public List<TsccvEntregasPedido> findByProperty(String propertyName,final Object value) throws Exception;
}
