package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.dao.TsccvEntregasPedidoDAO;
import co.com.smurfit.dataaccess.dao.TsccvProductosPedidoDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

 
public class TsccvProductosPedidoLogic implements ITsccvProductosPedidoLogic {
    public List<TsccvProductosPedido> getTsccvProductosPedido()
        throws Exception {
        List<TsccvProductosPedido> list = new ArrayList<TsccvProductosPedido>();
 
        try {
            list = JPADaoFactory.getInstance().getTsccvProductosPedidoDAO()
                                .findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public static synchronized void saveTsccvProductosPedido(String activo, Integer cantidadTotal,
        String codCli, String codScc, String codpla, String despro,
        String diamtr, String eje, Date fechaCreacion,
        Date fechaPrimeraEntrega, Integer idPrpe, String moneda,
        String ordenCompra, String precio, String undmed,
        Integer idPedi_TsccvPedidos) throws Exception {
        TsccvProductosPedido entity = null;

        try {	
        	if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Activo");
            }

            if (cantidadTotal == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Cantidad Total");
            }

            if (codCli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "C�dido Cliente");
            }

            if ((codCli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codCli, 20) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codCli");
            }

            if ((codScc != null) &&
                    (Utilities.checkWordAndCheckWithlength(codScc, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "C�digo Smurfit");
            }

            if (codpla == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "C�digo Planta");
            }

            if ((codpla != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "C�digo Planta");
            }

            if (despro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Descripci�n");
            }

            if ((despro != null) &&
                    (Utilities.checkWordAndCheckWithlength(despro, 60) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Descripci�n");
            }

            if ((diamtr != null) &&
                    (Utilities.checkWordAndCheckWithlength(diamtr, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Diametro");
            }

            if ((eje != null) &&
                    (Utilities.checkWordAndCheckWithlength(eje, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH + "Eje");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creaci�n");
            }

            /*if (idPrpe == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idPrpe");
            }*/

            if ((moneda != null) &&
                    (Utilities.checkWordAndCheckWithlength(moneda, 50) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Moneda");
            }

            if (ordenCompra == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Orden de Compra");
            }

            if ((ordenCompra != null) &&
                    (Utilities.checkWordAndCheckWithlength(ordenCompra, 15) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Orden de Compra");
            }

            if (precio == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Precio");
            }            

            //if ((precio != null) && (Utilities.checkWordAndCheckWithlength(precio, 16) == false)) {
            if ((precio != null) && (precio.length() > 16)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Precio");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 50) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Unidad de Medida");
            }

            ITsccvPedidosLogic logicTsccvPedidos1 = new TsccvPedidosLogic();

            TsccvPedidos tsccvPedidosClass = logicTsccvPedidos1.getTsccvPedidos(idPedi_TsccvPedidos);

            if (tsccvPedidosClass == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL);
            }

            /*entity = getTsccvProductosPedido(idPrpe);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }*/

            entity = new TsccvProductosPedido();

            entity.setActivo(activo);
            entity.setCantidadTotal(cantidadTotal);
            entity.setCodCli(codCli);
            entity.setCodScc(codScc);
            entity.setCodpla(codpla);
            entity.setDespro(despro);
            entity.setDiamtr(diamtr);
            entity.setEje(eje);
            entity.setFechaCreacion(fechaCreacion);
            entity.setFechaPrimeraEntrega(fechaPrimeraEntrega);
            //entity.setIdPrpe(idPrpe);
            entity.setMoneda(moneda);
            entity.setOrdenCompra(ordenCompra);
            entity.setPrecio(precio);
            entity.setUndmed(undmed);

            entity.setTsccvPedidos(tsccvPedidosClass);
            
            EntityManagerHelper.beginTransaction();
            //Comentariamos el llamado inicial
            //JPADaoFactory.getInstance().getTsccvProductosPedidoDAO().save(entity);
            //Llamamos el nuevo metodo que realiza el registro
            TsccvProductosPedidoDAO.saveProductosPedido(entity);
            EntityManagerHelper.commit();    	
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTsccvProductosPedido(Integer idPrpe)
        throws Exception {
        TsccvProductosPedido entity = null;

        if (idPrpe == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        List<TsccvEntregasPedido> tsccvEntregasPedidos = null;

        entity = getTsccvProductosPedido(idPrpe);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
        	//se comenta para que al eliminar el producto tambien se eliminen las entregas
            /*tsccvEntregasPedidos = JPADaoFactory.getInstance()
                                                .getTsccvEntregasPedidoDAO()
                                                .findByProperty("tsccvProductosPedido.idPrpe",
                    idPrpe, 0);

            if (Utilities.validationsList(tsccvEntregasPedidos) == true) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild());
            }*/

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTsccvProductosPedidoDAO()
                         .delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTsccvProductosPedido(String activo,
        Integer cantidadTotal, String codCli, String codScc, String codpla,
        String despro, String diamtr, String eje, Date fechaCreacion,
        Date fechaPrimeraEntrega, Integer idPrpe, String moneda,
        String ordenCompra, String precio, String undmed,
        Integer idPedi_TsccvPedidos, List<ProductosPedidoVO> listadoProductosPedido) throws Exception {
        TsccvProductosPedido entity = null;
        
        try {
        	ITsccvEntregasPedidoLogic tsccvEntregasPedidoLogic = new TsccvEntregasPedidoLogic();
        	
        	if(listadoProductosPedido != null && listadoProductosPedido.size() > 0){
	        	for(ProductosPedidoVO prodPedVO : listadoProductosPedido){
	        		TsccvProductosPedido prodPed = prodPedVO.getTsccvProductosPedido();
	        		
	        		//se consultan las entregas del producto pedido
	        		List<TsccvEntregasPedido> entregasTemp = tsccvEntregasPedidoLogic.findByProperty(TsccvEntregasPedidoDAO.PRODPED, prodPed.getIdPrpe());
	        		
		    		idPrpe = prodPed.getIdPrpe();
		    		idPedi_TsccvPedidos = prodPed.getTsccvPedidos().getIdPedi();
	        		activo = "0";
		    		//cantidadTotal = calcularCantTotal(idPrpe);
	        		cantidadTotal = calcularCantTotal(entregasTemp);
		    		codCli = prodPed.getCodCli();
		    		codScc = prodPed.getCodScc();
		    		codpla = prodPed.getCodpla();
		    		fechaCreacion = prodPed.getFechaCreacion();
		    		//fechaPrimeraEntrega = getFechaPrimeraEntrega(idPrpe);
		    		fechaPrimeraEntrega = getFechaPrimeraEntrega(entregasTemp);
		    		ordenCompra = prodPed.getOrdenCompra();		    		
		    		
		    		undmed = prodPedVO.getTsccvProductosPedido().getUndmed();
		    		moneda = prodPedVO.getTsccvProductosPedido().getMoneda();
		    		
		    		//propiedades para los producto de molinos
		    		eje = prodPedVO.getTsccvProductosPedido().getEje();
		    		diamtr = prodPedVO.getTsccvProductosPedido().getDiamtr();
		    		
		    		precio = prodPed.getPrecio();
		    		despro = prodPed.getDespro();
		        	
		            if (activo == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Activo");
		            }
		
		            if ((activo != null) &&
		                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "Activo");
		            }
		
		            if (cantidadTotal == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "Cantidad Total");
		            }
		
		            if (codCli == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "C�digo Cliente");
		            }
		
		            if ((codCli != null) &&
		                    (Utilities.checkWordAndCheckWithlength(codCli, 20) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "C�digo Cliente");
		            }
		
		            /*if (codScc == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codScc");
		            }*/
		
		            if ((codScc != null) &&
		                    (Utilities.checkWordAndCheckWithlength(codScc, 8) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "C�digo Smurfit");
		            }
		
		            if (codpla == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpla");
		            }
		
		            if ((codpla != null) &&
		                    (Utilities.checkWordAndCheckWithlength(codpla, 2) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "C�digo Planta");
		            }
		
		            if (fechaCreacion == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "Fecha Creaci�n");
		            }
		
		            /*if (fechaPrimeraEntrega == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "fechaPrimeraEntrega");
		            }*/
		
		            if (idPrpe == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idPrpe");
		            }
		
		            if (ordenCompra == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL +
		                    "Orden Compra");
		            }
		
		            if ((ordenCompra != null) &&
		                    (Utilities.checkWordAndCheckWithlength(ordenCompra, 15) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "Orden Compra");
		            }
		
		            if (precio == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Precio");
		            }
		
		            //if ((precio != null) && (Utilities.checkWordAndCheckWithlength(precio, 16) == false)) {
		            if ((precio != null) && (precio.length() > 16)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "Precio");
		            }
		
		            if (despro == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Descripci�n del Producto");
		            }
		
		            if ((despro != null) &&
		                    (Utilities.checkWordAndCheckWithlength(despro, 60) == false)) {
		                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
		                    "Descripci�n del Producto");
		            }
		
		            ITsccvPedidosLogic logicTsccvPedidos1 = new TsccvPedidosLogic();
		
		            TsccvPedidos tsccvPedidosClass = logicTsccvPedidos1.getTsccvPedidos(idPedi_TsccvPedidos);
		
		            if (tsccvPedidosClass == null) {
		                throw new Exception(ExceptionMessages.VARIABLE_NULL);
		            }
		
		            entity = getTsccvProductosPedido(idPrpe);
		
		            if (entity == null) {
		                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
		            }
		
		            entity.setActivo(activo);
		            entity.setCantidadTotal(cantidadTotal);
		            entity.setCodCli(codCli);
		            entity.setCodScc(codScc);
		            entity.setCodpla(codpla);
		            entity.setDespro(despro);
		            entity.setDiamtr(diamtr);
		            entity.setEje(eje);
		            entity.setFechaCreacion(fechaCreacion);
		            entity.setFechaPrimeraEntrega(fechaPrimeraEntrega);
		            entity.setIdPrpe(idPrpe);
		            entity.setMoneda(moneda);
		            entity.setOrdenCompra(ordenCompra);
		            entity.setPrecio(precio);
		            entity.setUndmed(undmed);
		
		            entity.setTsccvPedidos(tsccvPedidosClass);
		
		            EntityManagerHelper.beginTransaction();
		            JPADaoFactory.getInstance().getTsccvProductosPedidoDAO()
		                         .update(entity);
		            EntityManagerHelper.commit();
		
		            EntityManagerHelper.closeEntityManager();
	        	}
        	}
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TsccvProductosPedido getTsccvProductosPedido(Integer idPrpe)
        throws Exception {
        TsccvProductosPedido entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvProductosPedidoDAO()
                                  .findById(idPrpe);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvProductosPedido"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TsccvProductosPedido> findPageTsccvProductosPedido(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        List<TsccvProductosPedido> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvProductosPedidoDAO()
                                  .findPageTsccvProductosPedido(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvProductosPedido"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTsccvProductosPedido() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTsccvProductosPedidoDAO()
                                  .findTotalNumberTsccvProductosPedido();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TsccvProductosPedido Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TsccvProductosPedido> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TsccvProductosPedido> list = new ArrayList<TsccvProductosPedido>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTsccvProductosPedidoDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    //Metodo encargado de consultar productos pedido dependiendo de una propiedad
    public List<TsccvProductosPedido> findByProperty(String propertyName,final Object value) throws Exception{
    	try {
            return JPADaoFactory.getInstance().getTsccvProductosPedidoDAO().findByProperty(propertyName, value);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    //Metodo encargado de consultar productos pedido dependiendo de un pedido
    public List<ProductosPedidoVO> consultarProductosPedido(Integer idPedi) throws Exception{
    	try {
    		return  JPADaoFactory.getInstance().getTsccvProductosPedidoDAO().consultarProductosPedido(idPedi);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    
    //metodo encargado de sumar las cantidades para definiar la cantidad total
	//private Integer calcularCantTotal(Integer idPrpe) throws Exception{
    private Integer calcularCantTotal(List<TsccvEntregasPedido> listadoEntregasPed) throws Exception{
		try {
			int cantidadTotal = 0;
			
			if(listadoEntregasPed != null && listadoEntregasPed.size() > 0){
				for(TsccvEntregasPedido obj : listadoEntregasPed){
					cantidadTotal += obj.getCantidad();
				}
			}
			
			//return JPADaoFactory.getInstance().getTsccvProductosPedidoDAO().calcularCantTotal(idPrpe);
			return cantidadTotal;
		} catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
	}
	
	//metodo encargado de buscar la primera fecha de entrega
	private Date getFechaPrimeraEntrega(List<TsccvEntregasPedido> listadoEntregasPed)throws Exception{
	//private Date getFechaPrimeraEntrega(Integer idPrpe) throws Exception{
		try{
			Iterator<TsccvEntregasPedido> iterador = listadoEntregasPed.iterator();
			Date menor = null;
			if(iterador.hasNext()){
				menor = iterador.next().getFechaEntrega();
				while (iterador.hasNext()) {
					TsccvEntregasPedido actual = iterador.next();
					if(actual.getFechaEntrega().before(menor)){
						menor = actual.getFechaEntrega();
					}
				}
			}	
			//return JPADaoFactory.getInstance().getTsccvProductosPedidoDAO().getFechaPrimeraEntrega(idPrpe);
			return menor;
		} catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
	}

}
