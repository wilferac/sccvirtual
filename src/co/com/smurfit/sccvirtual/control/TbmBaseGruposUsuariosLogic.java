package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.dao.TbmBaseGruposUsuariosDAO;
import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class TbmBaseGruposUsuariosLogic implements ITbmBaseGruposUsuariosLogic {
    public List<TbmBaseGruposUsuarios> getTbmBaseGruposUsuarios()
        throws Exception {
        List<TbmBaseGruposUsuarios> list = new ArrayList<TbmBaseGruposUsuarios>();

        try {
            list = JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO()
                                .findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveTbmBaseGruposUsuarios(String activo, String descripcion,
        Date fechaCreacion, Long idGrup) throws Exception {
        TbmBaseGruposUsuarios entity = null;

        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Activo");
            }

            if (descripcion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Descripción");
            }

            if ((descripcion != null) &&
                    (Utilities.checkWordAndCheckWithlength(descripcion, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Descripción");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creación");
            }

            /*if (idGrup == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idGrup");
            }

            if ((idGrup != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idGrup, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "idGrup");
            }*/

            /*entity = getTbmBaseGruposUsuarios(idGrup);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }*/

            entity = new TbmBaseGruposUsuarios();

            entity.setActivo(activo);
            entity.setDescripcion(descripcion);
            entity.setFechaCreacion(fechaCreacion);
            //entity.setIdGrup(idGrup);

            EntityManagerHelper.beginTransaction();
            //Comentariamos el llamado inicial
            //JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO().save(entity);
            //Llamamos el nuevo metodo que realiza el registro
            TbmBaseGruposUsuariosDAO.saveGrupoUsuario(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteTbmBaseGruposUsuarios(Long idGrup)
        throws Exception {
        TbmBaseGruposUsuarios entity = null;

        if (idGrup == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL+"Id Grupo");
        }

        List<TbmBaseOpcionesGrupos> tbmBaseOpcionesGruposes = null;
        List<TsccvUsuario> tsccvUsuarios = null;

        entity = getTbmBaseGruposUsuarios(idGrup);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            tbmBaseOpcionesGruposes = JPADaoFactory.getInstance()
                                                   .getTbmBaseOpcionesGruposDAO()
                                                   .findByProperty("tbmBaseGruposUsuarios.idGrup",
                    idGrup, 0);

            if (Utilities.validationsList(tbmBaseOpcionesGruposes) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild()+"Opciones Grupo");
            }

            tsccvUsuarios = JPADaoFactory.getInstance().getTsccvUsuarioDAO()
                                         .findByProperty("tbmBaseGruposUsuarios.idGrup",
                    idGrup, 0);

            if (Utilities.validationsList(tsccvUsuarios) == false) {
                throw new Exception(ExceptionManager.getInstance()
                                                    .exceptionDeletingEntityWithChild()+"Usuarios");
            }

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO()
                         .delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateTbmBaseGruposUsuarios(String activo, String descripcion,
        Date fechaCreacion, Long idGrup) throws Exception {
        TbmBaseGruposUsuarios entity = null;

        try {
            if (activo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "Activo");
            }

            if ((activo != null) &&
                    (Utilities.checkWordAndCheckWithlength(activo, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Activo");
            }

            if (descripcion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Descripción");
            }

            if ((descripcion != null) &&
                    (Utilities.checkWordAndCheckWithlength(descripcion, 30) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Descripción");
            }

            if (fechaCreacion == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "Fecha Creación");
            }

            if (idGrup == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "idGrup");
            }

            if ((idGrup != null) &&
                    (Utilities.checkNumberAndCheckWithPrecisionAndScale("" +
                        idGrup, 3, 0) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "Id Grupo");
            }

            entity = getTbmBaseGruposUsuarios(idGrup);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setActivo(activo);
            entity.setDescripcion(descripcion);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdGrup(idGrup);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO()
                         .update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public TbmBaseGruposUsuarios getTbmBaseGruposUsuarios(Long idGrup)
        throws Exception {
        TbmBaseGruposUsuarios entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO()
                                  .findById(idGrup);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseGruposUsuarios"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<TbmBaseGruposUsuarios> findPageTbmBaseGruposUsuarios(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        List<TbmBaseGruposUsuarios> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO()
                                  .findPageTbmBaseGruposUsuarios(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseGruposUsuarios"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberTbmBaseGruposUsuarios()
        throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO()
                                  .findTotalNumberTbmBaseGruposUsuarios();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("TbmBaseGruposUsuarios Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<TbmBaseGruposUsuarios> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<TbmBaseGruposUsuarios> list = new ArrayList<TbmBaseGruposUsuarios>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    
    //consulta un grupoUsuario con los datos para saber si esta registrado
    public Boolean grupoUsuarioIsRegistrado(String descripcion) throws Exception{
	    try{
    		return JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO().grupoUsuarioIsRegistrado(descripcion);
	    } catch (Exception e) {
	        throw new Exception(e.getMessage());
	    } finally {
	        EntityManagerHelper.closeEntityManager();
	    }
    }
    
    
    public List<TbmBaseGruposUsuarios> consultarGruposUsuario(String activo, String descripcion,
        Date fechaCreacion, Long idGrup) throws Exception {
        
    	TbmBaseGruposUsuarios entity = null;
        List<TbmBaseGruposUsuarios> registros = null;

        try {            
            entity = new TbmBaseGruposUsuarios();

            entity.setActivo(activo);
            entity.setDescripcion(descripcion);
            entity.setFechaCreacion(fechaCreacion);
            entity.setIdGrup(idGrup);

            registros = JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO().consultarGruposUsuario(entity);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return registros;
    }
    
    
    public void inactivarGrupoUsuario(Long idGrup) throws Exception {
    	try{    
    		EntityManagerHelper.beginTransaction();
    		JPADaoFactory.getInstance().getTbmBaseGruposUsuariosDAO().inactivarGrupoUsuario(idGrup);    		
    		EntityManagerHelper.commit();
    	}catch(Exception e){
    		EntityManagerHelper.rollback();
    		throw new Exception(e.getMessage());
    	}
    	finally{    		
    		EntityManagerHelper.closeEntityManager();
    	}
    }
}
