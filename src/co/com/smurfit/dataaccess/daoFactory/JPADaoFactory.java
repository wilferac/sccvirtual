package co.com.smurfit.dataaccess.daoFactory;

import co.com.smurfit.dataaccess.dao.*;


/**
 * Factory for Data Access Objects Strategy The DAO pattern can be made highly flexible
 * by adopting the Abstract Factory [GoF] and the Factory Method [GoF] patterns.
 * When the underlying storage is not subject to change from one implementation to another,
 * this strategy can be implemented using the Factory Method pattern to produce a number of DAOs needed by the application.
 * This class is a Factory Method pattern
 *
 * @author Zathura Code Generator http://code.google.com/p/zathura
 */
public class JPADaoFactory {
    private static JPADaoFactory instance = null;

    private JPADaoFactory() {
    }

    /**
    *
    * @return JPADaoFactory
    */
    public static JPADaoFactory getInstance() {
        if (instance == null) {
            instance = new JPADaoFactory();
        }

        return instance;
    }

    /**
    * This method instantiates the IBvpDetDspDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return IBvpDetDspDAO implementation
    */
    public IBvpDetDspDAO getBvpDetDspDAO() {
        return new BvpDetDspDAO();
    }

    /**
    * This method instantiates the IBvpDetPedDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return IBvpDetPedDAO implementation
    */
    public IBvpDetPedDAO getBvpDetPedDAO() {
        return new BvpDetPedDAO();
    }

    /**
    * This method instantiates the IBvpDirDspDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return IBvpDirDspDAO implementation
    */
    public IBvpDirDspDAO getBvpDirDspDAO() {
        return new BvpDirDspDAO();
    }

    /**
    * This method instantiates the IBvpHistDespachosDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return IBvpHistDespachosDAO implementation
    */
    public IBvpHistDespachosDAO getBvpHistDespachosDAO() {
        return new BvpHistDespachosDAO();
    }

    /**
    * This method instantiates the IBvpPrecioDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return IBvpPrecioDAO implementation
    */
    public IBvpPrecioDAO getBvpPrecioDAO() {
        return new BvpPrecioDAO();
    }

    /**
    * This method instantiates the ICarteraDivDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ICarteraDivDAO implementation
    */
    public ICarteraDivDAO getCarteraDivDAO() {
        return new CarteraDivDAO();
    }

    /**
    * This method instantiates the ICarteraResumenDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ICarteraResumenDAO implementation
    */
    public ICarteraResumenDAO getCarteraResumenDAO() {
        return new CarteraResumenDAO();
    }

    /**
    * This method instantiates the IClasesDocDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return IClasesDocDAO implementation
    */
    public IClasesDocDAO getClasesDocDAO() {
        return new ClasesDocDAO();
    }

    /**
    * This method instantiates the ISapDetDspDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ISapDetDspDAO implementation
    */
    public ISapDetDspDAO getSapDetDspDAO() {
        return new SapDetDspDAO();
    }

    /**
    * This method instantiates the ISapDetPedDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ISapDetPedDAO implementation
    */
    public ISapDetPedDAO getSapDetPedDAO() {
        return new SapDetPedDAO();
    }

    /**
    * This method instantiates the ISapDirDspDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ISapDirDspDAO implementation
    */
    public ISapDirDspDAO getSapDirDspDAO() {
        return new SapDirDspDAO();
    }

    /**
    * This method instantiates the ISapHistDespachosDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ISapHistDespachosDAO implementation
    */
    public ISapHistDespachosDAO getSapHistDespachosDAO() {
        return new SapHistDespachosDAO();
    }

    /**
    * This method instantiates the ISapPrecioDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ISapPrecioDAO implementation
    */
    public ISapPrecioDAO getSapPrecioDAO() {
        return new SapPrecioDAO();
    }

    /**
    * This method instantiates the ISapUmedidaDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ISapUmedidaDAO implementation
    */
    public ISapUmedidaDAO getSapUmedidaDAO() {
        return new SapUmedidaDAO();
    }

    /**
    * This method instantiates the ITbmBaseErroresDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITbmBaseErroresDAO implementation
    */
    public ITbmBaseErroresDAO getTbmBaseErroresDAO() {
        return new TbmBaseErroresDAO();
    }

    /**
    * This method instantiates the ITbmBaseGruposUsuariosDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITbmBaseGruposUsuariosDAO implementation
    */
    public ITbmBaseGruposUsuariosDAO getTbmBaseGruposUsuariosDAO() {
        return new TbmBaseGruposUsuariosDAO();
    }

    /**
    * This method instantiates the ITbmBaseOpcionesDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITbmBaseOpcionesDAO implementation
    */
    public ITbmBaseOpcionesDAO getTbmBaseOpcionesDAO() {
        return new TbmBaseOpcionesDAO();
    }

    /**
    * This method instantiates the ITbmBaseOpcionesGruposDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITbmBaseOpcionesGruposDAO implementation
    */
    public ITbmBaseOpcionesGruposDAO getTbmBaseOpcionesGruposDAO() {
        return new TbmBaseOpcionesGruposDAO();
    }

    /**
    * This method instantiates the ITbmBaseSubopcionesDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITbmBaseSubopcionesDAO implementation
    */
    public ITbmBaseSubopcionesDAO getTbmBaseSubopcionesDAO() {
        return new TbmBaseSubopcionesDAO();
    }

    /**
    * This method instantiates the ITbmBaseSubopcionesOpcDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITbmBaseSubopcionesOpcDAO implementation
    */
    public ITbmBaseSubopcionesOpcDAO getTbmBaseSubopcionesOpcDAO() {
        return new TbmBaseSubopcionesOpcDAO();
    }

    /**
    * This method instantiates the ITsccvAccesosUsuarioDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvAccesosUsuarioDAO implementation
    */
    public ITsccvAccesosUsuarioDAO getTsccvAccesosUsuarioDAO() {
        return new TsccvAccesosUsuarioDAO();
    }

    /**
    * This method instantiates the ITsccvAplicacionesDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvAplicacionesDAO implementation
    */
    public ITsccvAplicacionesDAO getTsccvAplicacionesDAO() {
        return new TsccvAplicacionesDAO();
    }

    /**
    * This method instantiates the ITsccvDetalleGrupoEmpresaDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvDetalleGrupoEmpresaDAO implementation
    */
    public ITsccvDetalleGrupoEmpresaDAO getTsccvDetalleGrupoEmpresaDAO() {
        return new TsccvDetalleGrupoEmpresaDAO();
    }

    /**
    * This method instantiates the ITsccvEmpresasDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvEmpresasDAO implementation
    */
    public ITsccvEmpresasDAO getTsccvEmpresasDAO() {
        return new TsccvEmpresasDAO();
    }

    /**
    * This method instantiates the ITsccvEntregasPedidoDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvEntregasPedidoDAO implementation
    */
    public ITsccvEntregasPedidoDAO getTsccvEntregasPedidoDAO() {
        return new TsccvEntregasPedidoDAO();
    }

    /**
    * This method instantiates the ITsccvEstadisticaAccPedDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvEstadisticaAccPedDAO implementation
    */
    public ITsccvEstadisticaAccPedDAO getTsccvEstadisticaAccPedDAO() {
        return new TsccvEstadisticaAccPedDAO();
    }

    /**
    * This method instantiates the ITsccvGrupoEmpresaDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvGrupoEmpresaDAO implementation
    */
    public ITsccvGrupoEmpresaDAO getTsccvGrupoEmpresaDAO() {
        return new TsccvGrupoEmpresaDAO();
    }

    /**
    * This method instantiates the ITsccvPdfsProductosDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvPdfsProductosDAO implementation
    */
    public ITsccvPdfsProductosDAO getTsccvPdfsProductosDAO() {
        return new TsccvPdfsProductosDAO();
    }

    /**
    * This method instantiates the ITsccvPedidosDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvPedidosDAO implementation
    */
    public ITsccvPedidosDAO getTsccvPedidosDAO() {
        return new TsccvPedidosDAO();
    }

    /**
    * This method instantiates the ITsccvPlantaDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvPlantaDAO implementation
    */
    public ITsccvPlantaDAO getTsccvPlantaDAO() {
        return new TsccvPlantaDAO();
    }

    /**
    * This method instantiates the ITsccvPlantasUsuarioDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvPlantasUsuarioDAO implementation
    */
    public ITsccvPlantasUsuarioDAO getTsccvPlantasUsuarioDAO() {
        return new TsccvPlantasUsuarioDAO();
    }

    /**
    * This method instantiates the ITsccvProductosPedidoDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvProductosPedidoDAO implementation
    */
    public ITsccvProductosPedidoDAO getTsccvProductosPedidoDAO() {
        return new TsccvProductosPedidoDAO();
    }

    /**
    * This method instantiates the ITsccvTipoAccionDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvTipoAccionDAO implementation
    */
    public ITsccvTipoAccionDAO getTsccvTipoAccionDAO() {
        return new TsccvTipoAccionDAO();
    }

    /**
    * This method instantiates the ITsccvTipoProductoDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvTipoProductoDAO implementation
    */
    public ITsccvTipoProductoDAO getTsccvTipoProductoDAO() {
        return new TsccvTipoProductoDAO();
    }

    /**
    * This method instantiates the ITsccvUsuarioDAO class for JPA
    * that is used in this applications deployment environment to access the data.
    * @return ITsccvUsuarioDAO implementation
    */
    public ITsccvUsuarioDAO getTsccvUsuarioDAO() {
        return new TsccvUsuarioDAO();
    }
}
