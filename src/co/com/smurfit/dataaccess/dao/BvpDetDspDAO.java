package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;

import java.math.BigDecimal;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * BvpDetDsp entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.BvpDetDsp
 */
public class BvpDetDspDAO implements IBvpDetDspDAO {
    // property constants

    //public static final BvpDetDspId  ID = "id";
    public static final String ID = "id";

    //public static final BvpDetPedId  PEDIDO = "id.numped";
    public static final String PEDIDO = "id.numped";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved BvpDetDsp entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpDetDspDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDetDsp entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpDetDsp entity) {
        EntityManagerHelper.log("saving BvpDetDsp instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent BvpDetDsp entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpDetDspDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpDetDsp entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpDetDsp entity) {
        EntityManagerHelper.log("deleting BvpDetDsp instance", Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(BvpDetDsp.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved BvpDetDsp entity and return it or a copy of it
     * to the sender. A copy of the BvpDetDsp entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = BvpDetDspDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDetDsp entity to update
     * @return BvpDetDsp the persisted BvpDetDsp entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpDetDsp update(BvpDetDsp entity) {
        EntityManagerHelper.log("updating BvpDetDsp instance", Level.INFO, null);

        try {
            BvpDetDsp result = getEntityManager().merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public BvpDetDsp findById(BvpDetDspId id) {
        EntityManagerHelper.log("finding BvpDetDsp instance with id: " + id,
            Level.INFO, null);

        try {
            BvpDetDsp instance = getEntityManager().find(BvpDetDsp.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  BvpDetDsp entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  BvpDetDsp property to query
     * @param value
     *            the property value to match
     * @return List< BvpDetDsp> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpDetDsp> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  BvpDetDsp instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  BvpDetDsp model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all BvpDetDsp entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpDetDsp property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<BvpDetDsp> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpDetDsp> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding BvpDetDsp instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from BvpDetDsp model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpDetDsp> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<BvpDetDsp> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all BvpDetDsp entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpDetDsp> all BvpDetDsp entities
     */
    @SuppressWarnings("unchecked")
    public List<BvpDetDsp> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all BvpDetDsp instances", Level.INFO,
            null);

        try {
            final String queryString = "select model from BvpDetDsp model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpDetDsp> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from BvpDetDsp model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<BvpDetDsp> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in BvpDetDsp failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpDetDsp> findPageBvpDetDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from BvpDetDsp model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from BvpDetDsp model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberBvpDetDsp() {
        try {
            String queryString = "select count(*) from BvpDetDsp model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo encrgado de consultar el detalle despacho
    public List<BvpDetDsp> consultarDetalleDespachoBvp(BvpDetPed entity){
    	//NOTA: en el codcli viene seteado el cod_vpe de la empresa
    	StringBuffer query=new StringBuffer("SELECT NUMDSP, ITMDSP, CONVERT(DATETIME, FECDSP, 102) AS FECDSP, HORDSP, CANDSP, UNDMED, EMPTRA, PLACA, NOMCON, DIRDSP " +
    										"FROM BVP_DET_DSP WHERE CODCLI = '"+entity.getId().getCodcli()+ "' AND " +
    										"NUMPED = '"+entity.getId().getNumped()+"' AND " +
    										"ITMPED = '"+entity.getId().getItmped()+"' AND " +
    										"PLANTA = '"+entity.getId().getPlanta()+"' ");
    	List registros=null;
    	List<BvpDetDsp> resultado= null;
    	try {
    		System.out.println(query.toString());
	    	//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<BvpDetDsp>();           
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg = (Object[])registros.get(i);
	    		
	    		BvpDetDspId bvpDetDspId = new BvpDetDspId();
	    		BvpDetDsp detDsp = new BvpDetDsp();
	    			    		
	    		bvpDetDspId.setNumdsp((String)reg[0]);	    		
	    		bvpDetDspId.setItmdsp((String)reg[1]);
	    		
	    		//se valida el resto de los campos ya que no son obligatorios
	    		if(reg[2] != null){
	    			//TODO se comenta la conversion de la fecha de forma temporal 
	    			//bvpDetDspId.setFecdsp(sdf.format(new Date(((Timestamp)reg[2]).getTime())));
	    			try{
	    				bvpDetDspId.setFecdsp(sdf.format((Timestamp)reg[2]));
	    			}catch (Exception e) {
						// TODO: handle exception
					}
	    		}
	    		
	    		if(reg[3] != null){
	    			bvpDetDspId.setHordsp((String)reg[3]);
	    		}
	    		
	    		if(reg[4] != null){
	    			bvpDetDspId.setCandsp((String)reg[4]);
	    		}
	    		
	    		if(reg[5] != null){
	    			bvpDetDspId.setUndmed((String)reg[5]);
	    		}
	    		
	    		if(reg[6] != null){
	    			bvpDetDspId.setEmptra((String)reg[6]);
	    		}
	    		
	    		if(reg[7] != null){
	    			bvpDetDspId.setPlaca((String)reg[7]);
	    		}
	    		
	    		if(reg[8] != null){
		    		bvpDetDspId.setNomcon((String)reg[8]);
	            }
	    		
	    		if(reg[9] != null){
	    			bvpDetDspId.setDirdsp((String)reg[9]);
	    		}
	    		
	    		detDsp.setId(bvpDetDspId);
	    		
	            resultado.add(detDsp);
	    	}
	    	
	    	return resultado;
	    	
    	} catch (RuntimeException re) {
            throw re;
        }
    }
}
