package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * SapDirDsp entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.SapDirDsp
 */
public class SapDirDspDAO implements ISapDirDspDAO {
    // property constants

    //public static final SapDirDspId  ID = "id";
    public static final String ID = "id";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved SapDirDsp entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapDirDspDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDirDsp entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapDirDsp entity) {
        EntityManagerHelper.log("saving SapDirDsp instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent SapDirDsp entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapDirDspDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapDirDsp entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapDirDsp entity) {
        EntityManagerHelper.log("deleting SapDirDsp instance", Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(SapDirDsp.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved SapDirDsp entity and return it or a copy of it
     * to the sender. A copy of the SapDirDsp entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = SapDirDspDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDirDsp entity to update
     * @return SapDirDsp the persisted SapDirDsp entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapDirDsp update(SapDirDsp entity) {
        EntityManagerHelper.log("updating SapDirDsp instance", Level.INFO, null);

        try {
            SapDirDsp result = getEntityManager().merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public SapDirDsp findById(SapDirDspId id) {
        EntityManagerHelper.log("finding SapDirDsp instance with id: " + id,
            Level.INFO, null);

        try {
            SapDirDsp instance = getEntityManager().find(SapDirDsp.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  SapDirDsp entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  SapDirDsp property to query
     * @param value
     *            the property value to match
     * @return List< SapDirDsp> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapDirDsp> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  SapDirDsp instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  SapDirDsp model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all SapDirDsp entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapDirDsp property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<SapDirDsp> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapDirDsp> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding SapDirDsp instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from SapDirDsp model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapDirDsp> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<SapDirDsp> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all SapDirDsp entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapDirDsp> all SapDirDsp entities
     */
    @SuppressWarnings("unchecked")
    public List<SapDirDsp> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all SapDirDsp instances", Level.INFO,
            null);

        try {
            final String queryString = "select model from SapDirDsp model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapDirDsp> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from SapDirDsp model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<SapDirDsp> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in SapDirDsp failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapDirDsp> findPageSapDirDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from SapDirDsp model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from SapDirDsp model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberSapDirDsp() {
        try {
            String queryString = "select count(*) from SapDirDsp model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo encargado de consultar las direcciones de despacho para un cliente
    public List<SapDirDsp> consultarDireccionesCliente(String codCli){
    	try{
	    	String query = "SELECT CODDIR, DESDIR, DESPRV, NUMFAX, DESCIU, DESPAI " +
	    				   "FROM SAP_DIR_DSP WHERE LTRIM(RTRIM(CODCLI)) = '"+codCli.trim()+"' ";
	    	
	    	List registros=null;
			List<SapDirDsp> resultado= null;
			
			//Ejecutamos el query
			System.out.println(query.toString());
			registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos null
				return new ArrayList<SapDirDsp>();
			}
			
			resultado = new ArrayList<SapDirDsp>();
			for(int i=0; i<registros.size(); i++){
				Object[] reg = (Object[])registros.get(i);
				SapDirDsp sapDirDsp = new SapDirDsp();
				SapDirDspId sapDirDspId = new SapDirDspId();
				
				sapDirDspId.setCodcli(codCli);
				sapDirDspId.setCoddir((String)reg[0]);
				sapDirDspId.setDesdir((String)reg[1]);
				sapDirDspId.setDesprv((String)reg[2]);
				sapDirDspId.setNumfax((String)reg[3]);
				sapDirDspId.setDesciu((String)reg[4]);
				sapDirDspId.setDespai((String)reg[5]);
				sapDirDsp.setId(sapDirDspId);
				
				resultado.add(sapDirDsp);
			}
			
			return resultado;
    	}catch(Exception e){
			return null;
		}
    }
}
