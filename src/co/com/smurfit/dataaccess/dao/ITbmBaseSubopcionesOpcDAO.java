package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TbmBaseSubopcionesOpcDAO.
 *
*/
public interface ITbmBaseSubopcionesOpcDAO {
    /**
     * Perform an initial save of a previously unsaved TbmBaseSubopcionesOpc entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseSubopcionesOpcDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopcionesOpc entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseSubopcionesOpc entity);

    /**
     * Delete a persistent TbmBaseSubopcionesOpc entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseSubopcionesOpcDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopcionesOpc entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseSubopcionesOpc entity);

    /**
     * Persist a previously saved TbmBaseSubopcionesOpc entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseSubopcionesOpc entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITbmBaseSubopcionesOpcDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopcionesOpc entity to update
     * @return TbmBaseSubopcionesOpc the persisted TbmBaseSubopcionesOpc entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseSubopcionesOpc update(TbmBaseSubopcionesOpc entity);

    public TbmBaseSubopcionesOpc findById(TbmBaseSubopcionesOpcId id);

    /**
     * Find all TbmBaseSubopcionesOpc entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseSubopcionesOpc property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseSubopcionesOpc> found by query
     */
    public List<TbmBaseSubopcionesOpc> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TbmBaseSubopcionesOpc> findByCriteria(String whereCondition);

    public List<TbmBaseSubopcionesOpc> findById(Object id);

    public List<TbmBaseSubopcionesOpc> findById(Object id,
        int... rowStartIdxAndCount);

    /**
     * Find all TbmBaseSubopcionesOpc entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseSubopcionesOpc> all TbmBaseSubopcionesOpc entities
     */
    public List<TbmBaseSubopcionesOpc> findAll(int... rowStartIdxAndCount);

    public List<TbmBaseSubopcionesOpc> findPageTbmBaseSubopcionesOpc(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTbmBaseSubopcionesOpc();
    
    public Boolean subOpcionOpciIsRegistrada(String nombre, Long idOpci);
    
    public Object consultarUltimaPosicion(Long idOpcion);
    
    public void cambiarPosicion(TbmBaseSubopcionesOpc tbmBaseSubopcionesOpc, String opcion);
    
    public void restablecerPosiciones(Long posAcual, Long idOpci);
}
