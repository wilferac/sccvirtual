package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;


/**
 * Interface for BvpPrecioDAO.
 *
*/
public interface IBvpPrecioDAO {
    /**
     * Perform an initial save of a previously unsaved BvpPrecio entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpPrecioDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpPrecio entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpPrecio entity);

    /**
     * Delete a persistent BvpPrecio entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpPrecioDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpPrecio entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpPrecio entity);

    /**
     * Persist a previously saved BvpPrecio entity and return it or a copy of it
     * to the sender. A copy of the BvpPrecio entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = IBvpPrecioDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpPrecio entity to update
     * @return BvpPrecio the persisted BvpPrecio entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpPrecio update(BvpPrecio entity);

    public BvpPrecio findById(BvpPrecioId id);

    /**
     * Find all BvpPrecio entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpPrecio property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpPrecio> found by query
     */
    public List<BvpPrecio> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByCriteria(String whereCondition);

    public List<BvpPrecio> findByAlto(Object alto);

    public List<BvpPrecio> findByAlto(Object alto, int... rowStartIdxAndCount);

    public List<BvpPrecio> findByAncho(Object ancho);

    public List<BvpPrecio> findByAncho(Object ancho, int... rowStartIdxAndCount);

    public List<BvpPrecio> findByAncsac(Object ancsac);

    public List<BvpPrecio> findByAncsac(Object ancsac,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByClave(Object clave);

    public List<BvpPrecio> findByClave(Object clave, int... rowStartIdxAndCount);

    public List<BvpPrecio> findByCntmax(Object cntmax);

    public List<BvpPrecio> findByCntmax(Object cntmax,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByCntmin(Object cntmin);

    public List<BvpPrecio> findByCntmin(Object cntmin,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByCodpro(Object codpro);

    public List<BvpPrecio> findByCodpro(Object codpro,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByDespro(Object despro);

    public List<BvpPrecio> findByDespro(Object despro,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByEstilo(Object estilo);

    public List<BvpPrecio> findByEstilo(Object estilo,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByFuelle(Object fuelle);

    public List<BvpPrecio> findByFuelle(Object fuelle,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByHijpro(Object hijpro);

    public List<BvpPrecio> findByHijpro(Object hijpro,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findById(Object id);

    public List<BvpPrecio> findById(Object id, int... rowStartIdxAndCount);

    public List<BvpPrecio> findByKitpro(Object kitpro);

    public List<BvpPrecio> findByKitpro(Object kitpro,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByLargo(Object largo);

    public List<BvpPrecio> findByLargo(Object largo, int... rowStartIdxAndCount);

    public List<BvpPrecio> findByLarsac(Object larsac);

    public List<BvpPrecio> findByLarsac(Object larsac,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByMoneda(Object moneda);

    public List<BvpPrecio> findByMoneda(Object moneda,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByNumcol(Object numcol);

    public List<BvpPrecio> findByNumcol(Object numcol,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByRefere(Object refere);

    public List<BvpPrecio> findByRefere(Object refere,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByTippro(Object tippro);

    public List<BvpPrecio> findByTippro(Object tippro,
        int... rowStartIdxAndCount);

    public List<BvpPrecio> findByUndmed(Object undmed);

    public List<BvpPrecio> findByUndmed(Object undmed,
        int... rowStartIdxAndCount);

    /**
     * Find all BvpPrecio entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpPrecio> all BvpPrecio entities
     */
    public List<BvpPrecio> findAll(int... rowStartIdxAndCount);

    public List<BvpPrecio> findPageBvpPrecio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberBvpPrecio();
    
    public List<ProductoVO> consultarPdfsFaltantes(TsccvPlanta tsccvPlanta);
    
    public List<ProductoVO> consultarProductosBvp(String codigoProd, String descripcion, String coddigoVpe, TsccvPlanta tsccvPlanta, String nombreColumna, Boolean ascendente);
    
    public List<ProductoVO> consultarPreciosBvp(String codigoProd, String descripcion, String coddigoVpe, TsccvPlanta tsccvPlanta, String nombreColumna, Boolean ascendente);
    
    public Boolean precioIsRegistrado(String codScc, String codPla);
    
    public Map consultarEscalasBvp(List<ProductosPedidoVO> listadoProductosPedido, String codCli);
    
    public String consultarPrecioPorCantidadTotal(String codscc, String codVpe, String codPla, Integer cantidadTotal);
}
