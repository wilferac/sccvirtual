package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TbmBaseErroresDAO.
 *
*/
public interface ITbmBaseErroresDAO {
    /**
     * Perform an initial save of a previously unsaved TbmBaseErrores entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseErroresDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseErrores entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseErrores entity);

    /**
     * Delete a persistent TbmBaseErrores entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseErroresDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseErrores entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseErrores entity);

    /**
     * Persist a previously saved TbmBaseErrores entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseErrores entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITbmBaseErroresDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseErrores entity to update
     * @return TbmBaseErrores the persisted TbmBaseErrores entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseErrores update(TbmBaseErrores entity);

    public TbmBaseErrores findById(Long id);

    /**
     * Find all TbmBaseErrores entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseErrores property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseErrores> found by query
     */
    public List<TbmBaseErrores> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TbmBaseErrores> findByCriteria(String whereCondition);

    public List<TbmBaseErrores> findByActivo(Object activo);

    public List<TbmBaseErrores> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TbmBaseErrores> findByCodigo(Object codigo);

    public List<TbmBaseErrores> findByCodigo(Object codigo,
        int... rowStartIdxAndCount);

    public List<TbmBaseErrores> findByDescripcion(Object descripcion);

    public List<TbmBaseErrores> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount);

    public List<TbmBaseErrores> findByFechaCreacion(Object fechaCreacion);

    public List<TbmBaseErrores> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TbmBaseErrores> findByIdErr(Object idErr);

    public List<TbmBaseErrores> findByIdErr(Object idErr,
        int... rowStartIdxAndCount);

    public List<TbmBaseErrores> findBySolucion(Object solucion);

    public List<TbmBaseErrores> findBySolucion(Object solucion,
        int... rowStartIdxAndCount);

    /**
     * Find all TbmBaseErrores entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseErrores> all TbmBaseErrores entities
     */
    public List<TbmBaseErrores> findAll(int... rowStartIdxAndCount);

    public List<TbmBaseErrores> findPageTbmBaseErrores(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberTbmBaseErrores();
}
