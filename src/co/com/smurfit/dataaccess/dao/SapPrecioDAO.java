package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;

import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * SapPrecio entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.SapPrecio
 */
public class SapPrecioDAO implements ISapPrecioDAO {
    // property constants

    //public static final String  DESPRO = "despro";
    public static final String DESPRO = "despro";

    //public static final String  DIAMTR = "diamtr";
    public static final String DIAMTR = "diamtr";

    //public static final String  EJE = "eje";
    public static final String EJE = "eje";

    //public static final SapPrecioId  ID = "id";
    public static final String ID = "id";

    //public static final String  MONEDA = "moneda";
    public static final String MONEDA = "moneda";

    //public static final String  TIPREC = "tiprec";
    public static final String TIPREC = "tiprec";

    //public static final String  UNDMED = "undmed";
    public static final String UNDMED = "undmed";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved SapPrecio entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapPrecioDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapPrecio entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapPrecio entity) {
        EntityManagerHelper.log("saving SapPrecio instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent SapPrecio entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapPrecioDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapPrecio entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapPrecio entity) {
        EntityManagerHelper.log("deleting SapPrecio instance", Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(SapPrecio.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved SapPrecio entity and return it or a copy of it
     * to the sender. A copy of the SapPrecio entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = SapPrecioDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapPrecio entity to update
     * @return SapPrecio the persisted SapPrecio entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapPrecio update(SapPrecio entity) {
        EntityManagerHelper.log("updating SapPrecio instance", Level.INFO, null);

        try {
            SapPrecio result = getEntityManager().merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public SapPrecio findById(SapPrecioId id) {
        EntityManagerHelper.log("finding SapPrecio instance with id: " + id,
            Level.INFO, null);

        try {
            SapPrecio instance = getEntityManager().find(SapPrecio.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  SapPrecio entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  SapPrecio property to query
     * @param value
     *            the property value to match
     * @return List< SapPrecio> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapPrecio> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  SapPrecio instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  SapPrecio model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all SapPrecio entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapPrecio property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<SapPrecio> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapPrecio> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding SapPrecio instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from SapPrecio model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapPrecio> findByDespro(Object despro,
        int... rowStartIdxAndCount) {
        return findByProperty(DESPRO, despro, rowStartIdxAndCount);
    }

    public List<SapPrecio> findByDespro(Object despro) {
        return findByProperty(DESPRO, despro);
    }

    public List<SapPrecio> findByDiamtr(Object diamtr,
        int... rowStartIdxAndCount) {
        return findByProperty(DIAMTR, diamtr, rowStartIdxAndCount);
    }

    public List<SapPrecio> findByDiamtr(Object diamtr) {
        return findByProperty(DIAMTR, diamtr);
    }

    public List<SapPrecio> findByEje(Object eje, int... rowStartIdxAndCount) {
        return findByProperty(EJE, eje, rowStartIdxAndCount);
    }

    public List<SapPrecio> findByEje(Object eje) {
        return findByProperty(EJE, eje);
    }

    public List<SapPrecio> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<SapPrecio> findById(Object id) {
        return findByProperty(ID, id);
    }

    public List<SapPrecio> findByMoneda(Object moneda,
        int... rowStartIdxAndCount) {
        return findByProperty(MONEDA, moneda, rowStartIdxAndCount);
    }

    public List<SapPrecio> findByMoneda(Object moneda) {
        return findByProperty(MONEDA, moneda);
    }

    public List<SapPrecio> findByTiprec(Object tiprec,
        int... rowStartIdxAndCount) {
        return findByProperty(TIPREC, tiprec, rowStartIdxAndCount);
    }

    public List<SapPrecio> findByTiprec(Object tiprec) {
        return findByProperty(TIPREC, tiprec);
    }

    public List<SapPrecio> findByUndmed(Object undmed,
        int... rowStartIdxAndCount) {
        return findByProperty(UNDMED, undmed, rowStartIdxAndCount);
    }

    public List<SapPrecio> findByUndmed(Object undmed) {
        return findByProperty(UNDMED, undmed);
    }

    /**
     * Find all SapPrecio entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapPrecio> all SapPrecio entities
     */
    @SuppressWarnings("unchecked")
    public List<SapPrecio> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all SapPrecio instances", Level.INFO,
            null);

        try {
            final String queryString = "select model from SapPrecio model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapPrecio> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from SapPrecio model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<SapPrecio> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in SapPrecio failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapPrecio> findPageSapPrecio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from SapPrecio model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from SapPrecio model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberSapPrecio() {
        try {
            String queryString = "select count(*) from SapPrecio model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    
    //metodo encargado de consultar los productos por codigoProd o descripcion y codigoCliente 
    public List<ProductoVO> consultarProductosSap(String codigoProd, String descripcion, String coddigoMas, String nombreColumna, Boolean ascendente){
    	try{//consulta con base a la jsp actual SCCSearchProductoMAS.jsp
	    	
    		StringBuffer query = new StringBuffer("SELECT CODPRO, DESPRO, TIPREC, UNDMED, PRECIO, MONEDA, CODCLI " +
    											  "FROM SAP_PRECIO " +
    											  "WHERE LTRIM(RTRIM(CODCLI)) = '"+coddigoMas.trim()+"' ");
    		
    		if(codigoProd != null){
    			query.append("AND LTRIM(RTRIM(CODPRO)) LIKE '%"+codigoProd.trim()+"%' ");
    		}
    		else if(descripcion != null){
    			query.append("AND LTRIM(RTRIM(DESPRO)) LIKE '%"+descripcion.trim()+"%' ");
    		}
    		
    		//agregado para la funcion de ordenar por el campo
    		query.append("ORDER BY "+nombreColumna+" "+(ascendente ? "ASC" : "DESC"));
    		
			List registros=null;
			List<ProductoVO> resultado= null;
			
			//Ejecutamos el query
			System.out.println(query.toString());
			registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos null
				return new ArrayList<ProductoVO>();
			}
			
			resultado = new ArrayList<ProductoVO>();
			DecimalFormat df = new DecimalFormat("###,###,##0.00");
			
			for(int i=0; i<registros.size(); i++){
				Object[] reg= (Object[])registros.get(i);
				ProductoVO producto = new ProductoVO();
				SapPrecio sapPrecio = new SapPrecio();
				
				SapPrecioId sapPrecioId = new SapPrecioId();
				sapPrecioId.setCodpro((String)reg[0]);
				sapPrecioId.setCodcli((String)reg[0]);
				sapPrecioId.setPrecio(df.format(new Double((String)reg[4])));
				
				sapPrecio.setId(sapPrecioId);
				sapPrecio.setDespro((String)reg[1]);
				sapPrecio.setTiprec((String)reg[2]);
				sapPrecio.setUndmed((String)reg[3]);
				sapPrecio.setMoneda((String)reg[5]);
				producto.setSapPrecio(sapPrecio);
				
				resultado.add(producto);
			}
			
			return resultado;
		}catch(Exception e){
			return null;
		}
    }
    
    
    //metodo encargado de consultar los precios para armar la lista de precios (metodo utilizado en la pantalla lista precios) 
    public List<ProductoVO> consultarPreciosSap(String codigoProd, String descripcion, String coddigoMas, String nombreColumna, Boolean ascendente){
    	try{//consulta con base a la jsp actual SCCListaPreciosMAS.jsp 
    		StringBuffer query = new StringBuffer();
    		
    		query.append("SELECT CODPRO, DESPRO, MONEDA, PRECIO, UNDMED, TIPREC " +
						 "FROM SAP_PRECIO " +
						 "WHERE LTRIM(RTRIM(CODCLI)) = '"+coddigoMas.trim()+"' ");
    		
    		if(codigoProd != null){
    			query.append("AND LTRIM(RTRIM(CODPRO)) LIKE '%"+codigoProd.trim()+"%' "); 
    		}
    		else if(descripcion != null){
    			query.append("AND LTRIM(RTRIM(DESPRO)) LIKE '%"+descripcion.trim()+"%' ");
    		}
    		
    		//agregado para la funcion de ordenar por el campo
    		query.append("ORDER BY "+nombreColumna+" "+(ascendente ? "ASC" : "DESC"));
    		
			List registros=null;
			List<ProductoVO> resultado= null;
			
			//Ejecutamos el query
			System.out.println(query.toString());
			registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos un arry vacio
				return null;
			}
			
			resultado = new ArrayList<ProductoVO>();
			DecimalFormat df = new DecimalFormat("###,###,##0.00");
			for(int i=0; i<registros.size(); i++){
				Object[] reg= (Object[])registros.get(i);
				ProductoVO producto = new ProductoVO();
				producto.setSeleccionado(false);
				
				SapPrecio sapPrecio = new SapPrecio();				
				SapPrecioId sapPrecioId = new SapPrecioId();
				
				sapPrecioId.setCodpro((String)reg[0]);
				sapPrecioId.setPrecio(df.format(new Double((String)reg[3])));
				
				sapPrecio.setDespro((String)reg[1]);
				sapPrecio.setMoneda((String)reg[2]);
				sapPrecio.setUndmed((String)reg[4]);
				sapPrecio.setTiprec((String)reg[5]);
				
				sapPrecio.setId(sapPrecioId);
				producto.setSapPrecio(sapPrecio);
				resultado.add(producto);
			}
			
			return resultado;
		}catch(Exception e){
			return null;
		}
    }
}
