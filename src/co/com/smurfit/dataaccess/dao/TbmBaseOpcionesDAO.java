package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;


/**
 * A data access object (DAO) providing persistence and search support for
 * TbmBaseOpciones entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TbmBaseOpciones
 */
public class TbmBaseOpcionesDAO implements ITbmBaseOpcionesDAO {
    // property constants

    //public static final String  ACTIVO = "activo";
    public static final String ACTIVO = "activo";

    //public static final String  DESCRIPCION = "descripcion";
    public static final String DESCRIPCION = "descripcion";

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Long  IDOPCI = "idOpci";
    public static final String IDOPCI = "idOpci";

    //public static final String  IMAGENICONO = "imagenIcono";
    public static final String IMAGENICONO = "imagenIcono";

    //public static final String  NOMBRE = "nombre";
    public static final String NOMBRE = "nombre";

    //public static final String  URL = "url";
    public static final String URL = "url";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TbmBaseOpciones entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseOpcionesDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseOpciones entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseOpciones entity) {
        /*EntityManagerHelper.log("saving TbmBaseOpciones instance", Level.INFO,
            null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }*/
    }
    
    public static synchronized void saveOpcion(TbmBaseOpciones entity) {
        EntityManagerHelper.log("saving TbmBaseOpciones instance", Level.INFO, null);
        Object tempId = "";
        Long idOpci = null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	tempId = EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_Opci) from tbm_Base_Opciones").getSingleResult();         	
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro        	
        	idOpci = tempId != null ? ((((BigDecimal) tempId).longValue()) + 1) : 1;
        	
        	entity.setIdOpci(idOpci);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent TbmBaseOpciones entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseOpcionesDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseOpciones entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseOpciones entity) {
        EntityManagerHelper.log("deleting TbmBaseOpciones instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TbmBaseOpciones.class, entity.getIdOpci());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TbmBaseOpciones entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseOpciones entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TbmBaseOpcionesDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseOpciones entity to update
     * @return TbmBaseOpciones the persisted TbmBaseOpciones entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseOpciones update(TbmBaseOpciones entity) {
        EntityManagerHelper.log("updating TbmBaseOpciones instance",
            Level.INFO, null);

        try {
        	((Session)getEntityManager().getDelegate()).update(entity);
        	/*TbmBaseOpciones result = getEntityManager()
                                         .merge(entity);*/
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return entity;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TbmBaseOpciones findById(Long id) {
        EntityManagerHelper.log("finding TbmBaseOpciones instance with id: " +
            id, Level.INFO, null);

        try {
            TbmBaseOpciones instance = getEntityManager()
                                           .find(TbmBaseOpciones.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TbmBaseOpciones entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TbmBaseOpciones property to query
     * @param value
     *            the property value to match
     * @return List< TbmBaseOpciones> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseOpciones> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TbmBaseOpciones instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TbmBaseOpciones model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TbmBaseOpciones entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseOpciones property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TbmBaseOpciones> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseOpciones> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TbmBaseOpciones instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseOpciones model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseOpciones> findByActivo(Object activo,
        int... rowStartIdxAndCount) {
        return findByProperty(ACTIVO, activo, rowStartIdxAndCount);
    }

    public List<TbmBaseOpciones> findByActivo(Object activo) {
        return findByProperty(ACTIVO, activo);
    }

    public List<TbmBaseOpciones> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount) {
        return findByProperty(DESCRIPCION, descripcion, rowStartIdxAndCount);
    }

    public List<TbmBaseOpciones> findByDescripcion(Object descripcion) {
        return findByProperty(DESCRIPCION, descripcion);
    }

    public List<TbmBaseOpciones> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TbmBaseOpciones> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TbmBaseOpciones> findByIdOpci(Object idOpci,
        int... rowStartIdxAndCount) {
        return findByProperty(IDOPCI, idOpci, rowStartIdxAndCount);
    }

    public List<TbmBaseOpciones> findByIdOpci(Object idOpci) {
        return findByProperty(IDOPCI, idOpci);
    }

    public List<TbmBaseOpciones> findByImagenIcono(Object imagenIcono,
        int... rowStartIdxAndCount) {
        return findByProperty(IMAGENICONO, imagenIcono, rowStartIdxAndCount);
    }

    public List<TbmBaseOpciones> findByImagenIcono(Object imagenIcono) {
        return findByProperty(IMAGENICONO, imagenIcono);
    }

    public List<TbmBaseOpciones> findByNombre(Object nombre,
        int... rowStartIdxAndCount) {
        return findByProperty(NOMBRE, nombre, rowStartIdxAndCount);
    }

    public List<TbmBaseOpciones> findByNombre(Object nombre) {
        return findByProperty(NOMBRE, nombre);
    }

    public List<TbmBaseOpciones> findByUrl(Object url,
        int... rowStartIdxAndCount) {
        return findByProperty(URL, url, rowStartIdxAndCount);
    }

    public List<TbmBaseOpciones> findByUrl(Object url) {
        return findByProperty(URL, url);
    }

    /**
     * Find all TbmBaseOpciones entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseOpciones> all TbmBaseOpciones entities
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseOpciones> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TbmBaseOpciones instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseOpciones model order by model asc";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseOpciones> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TbmBaseOpciones model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TbmBaseOpciones> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TbmBaseOpciones failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseOpciones> findPageTbmBaseOpciones(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TbmBaseOpciones model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TbmBaseOpciones model order by model asc";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTbmBaseOpciones() {
        try {
            String queryString = "select count(*) from TbmBaseOpciones model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    
    //Metodo utilizado para la consulta por todos los campos
    public List<TbmBaseOpciones> consultarOpciones(TbmBaseOpciones entity){
    	StringBuffer query=new StringBuffer("SELECT ID_OPCI, NOMBRE, URL, IMAGEN_ICONO, DESCRIPCION, ACTIVO, FECHA_CREACION FROM TBM_BASE_OPCIONES  ");
    	List registros=null;
    	List<TbmBaseOpciones> resultado= null;
    	try {
	    	//Validamos si los campos vienen con valor o no para armar el query
	    	if(entity.getIdOpci() != null){
	    		query.append("WHERE ID_OPCI LIKE '%"+entity.getIdOpci()+"%' ");
	    	}
	    	else{
	    		query.append("WHERE ID_OPCI LIKE '%' ");
	    	}
	    	
	    	if(entity.getNombre() != null){
	    		query.append("AND NOMBRE LIKE '%"+entity.getNombre()+"%' ");
	    	}
	    	else{
	    		query.append("AND NOMBRE LIKE '%' ");
	    	}
	    	
	    	if(entity.getUrl() != null){
	    		query.append("AND URL LIKE '%"+entity.getUrl()+"%' ");
	    	}
	    	else{
	    		query.append("AND (URL LIKE '%' OR URL IS NULL) ");
	    	}	    		    
	    	
	    	if(entity.getImagenIcono() != null){
	    		query.append("AND IMAGEN_ICONO LIKE '%"+entity.getImagenIcono()+"%' ");
	    	}
	    	else{
	    		query.append("AND (IMAGEN_ICONO LIKE '%' OR IMAGEN_ICONO IS NULL) ");
	    	}
	    	
	    	if(entity.getDescripcion() != null){
	    		query.append("AND DESCRIPCION LIKE '%"+entity.getDescripcion()+"%' ");
	    	}
	    	else{
	    		query.append("AND (DESCRIPCION LIKE '%' OR DESCRIPCION IS NULL) ");
	    	}
	    	
	    	if(entity.getActivo() != null){
	    		query.append("AND ACTIVO LIKE '%"+entity.getActivo()+"%' ");
	    	}
	    	else{
	    		query.append("AND ACTIVO LIKE '%' ");
	    	}	    		    	
	    	
	    	//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<TbmBaseOpciones>();           
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TbmBaseOpciones opci = new TbmBaseOpciones();
	    		opci.setIdOpci(((BigDecimal)reg[0]).longValue());
	    		opci.setNombre((String)reg[1]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO URL YA QUE NO ES OBLIGATORIO
	    		if(reg[2] != null){
	    			opci.setUrl((String)reg[2]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO IMAGEN_ICONO YA QUE NO ES OBLIGATORIO
	    		if(reg[3] != null){
	    			opci.setImagenIcono((String)reg[3]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO IMAGEN_ICONO YA QUE NO ES OBLIGATORIO
	    		if(reg[4] != null){
	    			opci.setDescripcion((String)reg[4]);
	    		}
	    		
	    		opci.setActivo((String)reg[5]);
	    		opci.setFechaCreacion((Date)reg[6]);
	    		
	            resultado.add(opci);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
    
    
    //consulta las opciones con los datos para saber si esta registrado 
    public Boolean opcionIsRegistrada(String nombre){
    	String query = "SELECT * FROM TBM_BASE_OPCIONES " +
    				   "WHERE NOMBRE = '"+nombre+"' ";
    	List resultado = null;
    	try{
    		resultado = EntityManagerHelper.getEntityManager().createNativeQuery(query).getResultList();
	    	
	    	return (resultado != null && resultado.size() != 0) ? true : false;
    	}catch(Exception e){
    		return null;
    	}
    }
    
    public void inactivarOpcion(Long idOpci){    	
    	EntityManagerHelper.log("inactivating TbmBaseOpciones instance", Level.INFO, null);
    	String query = "update TbmBaseOpciones model set activo = '1' where model.idOpci = "+idOpci+" ";
    	
		try{
			getEntityManager().createQuery(query).executeUpdate();
			EntityManagerHelper.log("inactivation successful", Level.INFO, null);
		}catch(RuntimeException re){
			EntityManagerHelper.log("inactivation failed", Level.SEVERE, re);
			throw re;
		}
    }
}
