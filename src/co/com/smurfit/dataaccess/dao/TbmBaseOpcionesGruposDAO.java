package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;


/**
 * A data access object (DAO) providing persistence and search support for
 * TbmBaseOpcionesGrupos entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TbmBaseOpcionesGrupos
 */
public class TbmBaseOpcionesGruposDAO implements ITbmBaseOpcionesGruposDAO {
    // property constants

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final TbmBaseOpcionesGruposId  ID = "id";
    public static final String ID = "id";
    
    //public static final TbmBaseOpcionesGruposId  ID = "id.idGrup";
    public static final String GRUPO = "id.idGrup";
    
    //public static final TbmBaseOpcionesGruposId  ID = "id.idOpci";
    public static final String OPCION = "id.idOpci";
    
    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TbmBaseOpcionesGrupos entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseOpcionesGruposDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseOpcionesGrupos entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseOpcionesGrupos entity) {
        EntityManagerHelper.log("saving TbmBaseOpcionesGrupos instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent TbmBaseOpcionesGrupos entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseOpcionesGruposDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseOpcionesGrupos entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseOpcionesGrupos entity) {
        EntityManagerHelper.log("deleting TbmBaseOpcionesGrupos instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TbmBaseOpcionesGrupos.class,
                    entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TbmBaseOpcionesGrupos entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseOpcionesGrupos entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TbmBaseOpcionesGruposDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseOpcionesGrupos entity to update
     * @return TbmBaseOpcionesGrupos the persisted TbmBaseOpcionesGrupos entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseOpcionesGrupos update(TbmBaseOpcionesGrupos entity) {
        EntityManagerHelper.log("updating TbmBaseOpcionesGrupos instance",
            Level.INFO, null);

        try {
            /*TbmBaseOpcionesGrupos result = getEntityManager()
                                               .merge(entity);*/
        	((Session)getEntityManager().getDelegate()).update(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return entity;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TbmBaseOpcionesGrupos findById(TbmBaseOpcionesGruposId id) {
        EntityManagerHelper.log(
            "finding TbmBaseOpcionesGrupos instance with id: " + id,
            Level.INFO, null);

        try {
            TbmBaseOpcionesGrupos instance = getEntityManager()
                                                 .find(TbmBaseOpcionesGrupos.class,
                    id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TbmBaseOpcionesGrupos entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TbmBaseOpcionesGrupos property to query
     * @param value
     *            the property value to match
     * @return List< TbmBaseOpcionesGrupos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseOpcionesGrupos> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TbmBaseOpcionesGrupos instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TbmBaseOpcionesGrupos model where model." +
                propertyName + "= :propertyValue order by model.orden asc";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TbmBaseOpcionesGrupos entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseOpcionesGrupos property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TbmBaseOpcionesGrupos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseOpcionesGrupos> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TbmBaseOpcionesGrupos instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseOpcionesGrupos model where model." +
                propertyName + "= :propertyValue order by model.orden asc";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseOpcionesGrupos> findByFechaCreacion(
        Object fechaCreacion, int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TbmBaseOpcionesGrupos> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TbmBaseOpcionesGrupos> findById(Object id,
        int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<TbmBaseOpcionesGrupos> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all TbmBaseOpcionesGrupos entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseOpcionesGrupos> all TbmBaseOpcionesGrupos entities
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseOpcionesGrupos> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TbmBaseOpcionesGrupos instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseOpcionesGrupos model order by model.orden asc";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseOpcionesGrupos> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TbmBaseOpcionesGrupos model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TbmBaseOpcionesGrupos> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TbmBaseOpcionesGrupos failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseOpcionesGrupos> findPageTbmBaseOpcionesGrupos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TbmBaseOpcionesGrupos model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TbmBaseOpcionesGrupos model order by model.orden asc";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTbmBaseOpcionesGrupos() {
        try {
            String queryString = "select count(*) from TbmBaseOpcionesGrupos model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo encargado de consultar el ultimo orden de una opcion para un grupo
    public Object consultarUltimoOrden(Long idGrup){
		Object ultimoOrden = null;
		String query = "SELECT MAX(ORDEN) FROM TBM_BASE_OPCIONES_GRUPOS WHERE ID_GRUP = "+idGrup+" ";

		ultimoOrden = EntityManagerHelper.getEntityManager().createNativeQuery(query).getSingleResult();
		
		return ultimoOrden;
	}
	
	
	//metodo encargado de modificar el orden de la opcion
    public void cambiarOrden(TbmBaseOpcionesGrupos tbmBaseOpcionesGrupos, String orden){
    	Long ordenActual = tbmBaseOpcionesGrupos.getOrden();
    	String query = null;
    	int numUpdate = 0;
    	
    	try{
    		if(orden.equals("subir")){
	    		EntityManagerHelper.log("updating TbmBaseOpcionesGrupos instance", Level.INFO, null);
	    		
	    		EntityManagerHelper.beginTransaction();
	    		query = "update TbmBaseOpcionesGrupos model set model.orden = -1 " +
	    				"where model.orden = "+(ordenActual-1)+" and model.id.idGrup = "+tbmBaseOpcionesGrupos.getId().getIdGrup()+" ";	    		
	    		numUpdate = getEntityManager().createQuery(query).executeUpdate();
	    		EntityManagerHelper.commit();
	    		
	    		if(numUpdate == 1){
	    			EntityManagerHelper.beginTransaction();
	    			query = "update TbmBaseOpcionesGrupos model set model.orden = -2 " +
	    				"where model.orden = "+(ordenActual)+" and model.id.idGrup = "+tbmBaseOpcionesGrupos.getId().getIdGrup()+" ";	    		
		    		numUpdate = getEntityManager().createQuery(query).executeUpdate();
		    		EntityManagerHelper.commit();
		    		
		    		EntityManagerHelper.beginTransaction();
	    			query = "update TbmBaseOpcionesGrupos model set model.orden = "+(ordenActual)+" " +
	    				"where model.orden = -1 and model.id.idGrup = "+tbmBaseOpcionesGrupos.getId().getIdGrup()+" ";
		    		getEntityManager().createQuery(query).executeUpdate();
		    		EntityManagerHelper.commit();
		    		
		    		EntityManagerHelper.beginTransaction();
		    		query = "update TbmBaseOpcionesGrupos model set model.orden = "+(ordenActual-1)+" " +
	    				"where model.orden = -2 and model.id.idGrup = "+tbmBaseOpcionesGrupos.getId().getIdGrup()+" ";				
		    		getEntityManager().createQuery(query).executeUpdate();
		    		EntityManagerHelper.commit();
	    		}
	    	}
    		/*if(opcion.equals("bajar")){
	    		EntityManagerHelper.log("updating TbmBaseSubopcionesOpc instance", Level.INFO, null);
	    		
	    		EntityManagerHelper.beginTransaction();
	    		query = "update TbmBaseSubopcionesOpc model set model.posicion = -1 " +
	    				"where model.posicion = "+(posicionActual+1)+" and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";	    		
	    		numUpdate = getEntityManager().createQuery(query).executeUpdate();
	    		EntityManagerHelper.commit();
	    		
	    		if(numUpdate == 1){
	    			EntityManagerHelper.beginTransaction();
	    			query = "update TbmBaseSubopcionesOpc model set model.posicion = -2 " +
	    				"where model.posicion = "+(posicionActual)+" and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";	    		
		    		numUpdate = getEntityManager().createQuery(query).executeUpdate();
		    		EntityManagerHelper.commit();
		    		
		    		EntityManagerHelper.beginTransaction();
	    			query = "update TbmBaseSubopcionesOpc model set model.posicion = "+(posicionActual)+" " +
	    				"where model.posicion = -1 and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";				
		    		getEntityManager().createQuery(query).executeUpdate();
		    		EntityManagerHelper.commit();
		    		
		    		EntityManagerHelper.beginTransaction();
		    		query = "update TbmBaseSubopcionesOpc model set model.posicion = "+(posicionActual+1)+" " +
	    				"where model.posicion = -2 and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";				
		    		getEntityManager().createQuery(query).executeUpdate();	
		    		EntityManagerHelper.commit();
	    		}
	    	}*/
    	}catch(Exception e){
    		EntityManagerHelper.log(e.getMessage(), Level.INFO, null);
    	}
    }
    
    //metodo encargado de restablecer las posiciones de las opciones despues de eliminar una
    public void restablecerPosiciones(Long posAcual, Long idGrup){
    	//se consulta la posicion mas alta
    	Object ultimoOrden = consultarUltimoOrden(idGrup);
        Long ultPos = null;
    	if(ultimoOrden != null && ultimoOrden != posAcual){
    		ultPos = ((BigDecimal)ultimoOrden).longValue();
        
    		//se modifican las posiciones de las sub. opciones
	        for(Long i = posAcual; i<ultPos; i++){
		    	EntityManagerHelper.beginTransaction();
				String query = "update TbmBaseOpcionesGrupos model set model.orden = "+(i)+" " +
					"where model.orden = "+(i+1)+" and model.id.idGrup = "+idGrup+" ";
				getEntityManager().createQuery(query).executeUpdate();
				EntityManagerHelper.commit();
	    	}
    	}
    }
}
