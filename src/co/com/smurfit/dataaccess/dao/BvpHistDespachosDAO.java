package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * BvpHistDespachos entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.BvpHistDespachos
 */
public class BvpHistDespachosDAO implements IBvpHistDespachosDAO {
    // property constants

    //public static final BvpHistDespachosId  ID = "id";
    public static final String ID = "id";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved BvpHistDespachos entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpHistDespachosDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpHistDespachos entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpHistDespachos entity) {
        EntityManagerHelper.log("saving BvpHistDespachos instance", Level.INFO,
            null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent BvpHistDespachos entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpHistDespachosDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpHistDespachos entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpHistDespachos entity) {
        EntityManagerHelper.log("deleting BvpHistDespachos instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(BvpHistDespachos.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved BvpHistDespachos entity and return it or a copy of it
     * to the sender. A copy of the BvpHistDespachos entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = BvpHistDespachosDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpHistDespachos entity to update
     * @return BvpHistDespachos the persisted BvpHistDespachos entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpHistDespachos update(BvpHistDespachos entity) {
        EntityManagerHelper.log("updating BvpHistDespachos instance",
            Level.INFO, null);

        try {
            BvpHistDespachos result = getEntityManager()
                                          .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public BvpHistDespachos findById(BvpHistDespachosId id) {
        EntityManagerHelper.log("finding BvpHistDespachos instance with id: " +
            id, Level.INFO, null);

        try {
            BvpHistDespachos instance = getEntityManager()
                                            .find(BvpHistDespachos.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  BvpHistDespachos entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  BvpHistDespachos property to query
     * @param value
     *            the property value to match
     * @return List< BvpHistDespachos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpHistDespachos> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  BvpHistDespachos instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  BvpHistDespachos model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all BvpHistDespachos entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpHistDespachos property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<BvpHistDespachos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpHistDespachos> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding BvpHistDespachos instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from BvpHistDespachos model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpHistDespachos> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<BvpHistDespachos> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all BvpHistDespachos entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpHistDespachos> all BvpHistDespachos entities
     */
    @SuppressWarnings("unchecked")
    public List<BvpHistDespachos> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all BvpHistDespachos instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from BvpHistDespachos model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpHistDespachos> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from BvpHistDespachos model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<BvpHistDespachos> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in BvpHistDespachos failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpHistDespachos> findPageBvpHistDespachos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from BvpHistDespachos model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from BvpHistDespachos model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberBvpHistDespachos() {
        try {
            String queryString = "select count(*) from BvpHistDespachos model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
	//metodo agregado para consultar el historico de despacho de acuerdo a la informacion ingresada
    public List<BvpHistDespachos> consultarHistoricoDespachoBvp(String codVpe, TsccvPlanta tsccvPlanta, String periodo){
    	String query="SELECT CODPRO, CODSCC, DESPRO, CANTID, UNDMED " +
    				 "FROM BVP_HIST_DESPACHOS " +
    				 "WHERE LTRIM(RTRIM(CODCLI)) = '"+codVpe.trim()+"' " +
    				 "AND LTRIM(RTRIM(PLANTA)) = '"+tsccvPlanta.getCodpla().trim()+"' " +
    				 "AND LTRIM(RTRIM(TIPPRO)) = '"+Utilities.TIPOS_PRODUCTO[Integer.parseInt(tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString())-1]+"' AND NUMMES = '"+periodo+ "' " +
    				 "ORDER BY CODPRO ";
    	
		List registros=null;
		List<BvpHistDespachos> resultado = new ArrayList<BvpHistDespachos>();
		
		try {
			//Ejecutamos el query
			System.out.println(query.toString());
			registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos null
				return resultado;
			}
			
			DecimalFormat df = new DecimalFormat("###,###,###,##0");
			for(int i=0; i<registros.size() ; i++){
				Object[] reg= (Object[])registros.get(i);
				BvpHistDespachos histDesp = new BvpHistDespachos();
				BvpHistDespachosId histDespId = new BvpHistDespachosId();
				
				histDespId.setCodpro((String)reg[0]);
				histDespId.setCodcli((String)reg[0]);
				histDespId.setCodscc((String)reg[1]);
				histDespId.setDespro((String)reg[2]);
				histDespId.setCantid(df.format(new Double((String)reg[3])));
				histDespId.setUndmed((String)reg[4]);
				
				histDesp.setId(histDespId);
				resultado.add(histDesp);
			}
		
			return resultado;
		
		}catch (Exception re) {
			//throw re;
			return null;
		}
    }
    
    //metodo encargado de consultar el primer perido de despacho para un cliente, una planta y un tipo de producto
    public String minPeriodo(String codVpe, TsccvPlanta tsccvPlanta){
    	try{
	    	Object minPeriodo = null;
	    	
	    	String query = "SELECT MIN(NUMMES) " +
	    				   "FROM BVP_HIST_DESPACHOS " +
	    				   "WHERE LTRIM(RTRIM(CODCLI)) = '"+codVpe.trim()+"' AND " +
	    				   "LTRIM(RTRIM(PLANTA)) = '"+tsccvPlanta.getCodpla().trim()+"' AND " +
	    				   "LTRIM(RTRIM(TIPPRO)) = '"+Utilities.TIPOS_PRODUCTO[Integer.parseInt(tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString())-1]+"' ";
	    	
	    	minPeriodo = EntityManagerHelper.getEntityManager().createNativeQuery(query).getSingleResult();
	    	
	    	return minPeriodo != null ? minPeriodo.toString() : null;
    	}catch (Exception e) {
			return null;
		}
    }    
}




