package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for CarteraResumenDAO.
 *
*/
public interface ICarteraResumenDAO {
    /**
     * Perform an initial save of a previously unsaved CarteraResumen entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ICarteraResumenDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            CarteraResumen entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(CarteraResumen entity);

    /**
     * Delete a persistent CarteraResumen entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ICarteraResumenDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            CarteraResumen entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(CarteraResumen entity);

    /**
     * Persist a previously saved CarteraResumen entity and return it or a copy of it
     * to the sender. A copy of the CarteraResumen entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ICarteraResumenDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            CarteraResumen entity to update
     * @return CarteraResumen the persisted CarteraResumen entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public CarteraResumen update(CarteraResumen entity);

    public CarteraResumen findById(CarteraResumenId id);

    /**
     * Find all CarteraResumen entities with a specific property value.
     *
     * @param propertyName
     *            the name of the CarteraResumen property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<CarteraResumen> found by query
     */
    public List<CarteraResumen> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<CarteraResumen> findByCriteria(String whereCondition);

    public List<CarteraResumen> findById(Object id);

    public List<CarteraResumen> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all CarteraResumen entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<CarteraResumen> all CarteraResumen entities
     */
    public List<CarteraResumen> findAll(int... rowStartIdxAndCount);

    public List<CarteraResumen> findPageCarteraResumen(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberCarteraResumen();
    
    public List consultarCartera(String codSap);
    
    public List<CarteraResumen> consultarDetalleFactura(String codSap, String rango);
}
