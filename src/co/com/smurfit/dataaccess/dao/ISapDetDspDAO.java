package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for SapDetDspDAO.
 *
*/
public interface ISapDetDspDAO {
    /**
     * Perform an initial save of a previously unsaved SapDetDsp entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapDetDspDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDetDsp entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapDetDsp entity);

    /**
     * Delete a persistent SapDetDsp entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapDetDspDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapDetDsp entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapDetDsp entity);

    /**
     * Persist a previously saved SapDetDsp entity and return it or a copy of it
     * to the sender. A copy of the SapDetDsp entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ISapDetDspDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDetDsp entity to update
     * @return SapDetDsp the persisted SapDetDsp entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapDetDsp update(SapDetDsp entity);

    public SapDetDsp findById(SapDetDspId id);

    /**
     * Find all SapDetDsp entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapDetDsp property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapDetDsp> found by query
     */
    public List<SapDetDsp> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<SapDetDsp> findByCriteria(String whereCondition);

    public List<SapDetDsp> findById(Object id);

    public List<SapDetDsp> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all SapDetDsp entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapDetDsp> all SapDetDsp entities
     */
    public List<SapDetDsp> findAll(int... rowStartIdxAndCount);

    public List<SapDetDsp> findPageSapDetDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberSapDetDsp();
    
    public List<SapDetDsp> consultarDetalleDespachoSap(SapDetPed entity);
}
