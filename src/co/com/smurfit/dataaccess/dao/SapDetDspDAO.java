package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetDsp;
import co.com.smurfit.sccvirtual.entidades.SapDetDspId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;

import java.math.BigDecimal;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * SapDetDsp entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.SapDetDsp
 */
public class SapDetDspDAO implements ISapDetDspDAO {
    // property constants

    //public static final SapDetDspId  ID = "id";
    public static final String ID = "id";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved SapDetDsp entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapDetDspDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDetDsp entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapDetDsp entity) {
        EntityManagerHelper.log("saving SapDetDsp instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent SapDetDsp entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapDetDspDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapDetDsp entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapDetDsp entity) {
        EntityManagerHelper.log("deleting SapDetDsp instance", Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(SapDetDsp.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved SapDetDsp entity and return it or a copy of it
     * to the sender. A copy of the SapDetDsp entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = SapDetDspDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDetDsp entity to update
     * @return SapDetDsp the persisted SapDetDsp entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapDetDsp update(SapDetDsp entity) {
        EntityManagerHelper.log("updating SapDetDsp instance", Level.INFO, null);

        try {
            SapDetDsp result = getEntityManager().merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public SapDetDsp findById(SapDetDspId id) {
        EntityManagerHelper.log("finding SapDetDsp instance with id: " + id,
            Level.INFO, null);

        try {
            SapDetDsp instance = getEntityManager().find(SapDetDsp.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  SapDetDsp entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  SapDetDsp property to query
     * @param value
     *            the property value to match
     * @return List< SapDetDsp> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapDetDsp> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  SapDetDsp instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  SapDetDsp model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all SapDetDsp entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapDetDsp property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<SapDetDsp> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapDetDsp> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding SapDetDsp instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from SapDetDsp model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapDetDsp> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<SapDetDsp> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all SapDetDsp entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapDetDsp> all SapDetDsp entities
     */
    @SuppressWarnings("unchecked")
    public List<SapDetDsp> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all SapDetDsp instances", Level.INFO,
            null);

        try {
            final String queryString = "select model from SapDetDsp model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapDetDsp> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from SapDetDsp model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<SapDetDsp> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in SapDetDsp failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapDetDsp> findPageSapDetDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from SapDetDsp model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from SapDetDsp model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberSapDetDsp() {
        try {
            String queryString = "select count(*) from SapDetDsp model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    
    //metodo encrgado de consultar el detalle despacho
    public List<SapDetDsp> consultarDetalleDespachoSap(SapDetPed entity){
    	//NOTA: en el codcli viene seteado el cod_mas de la empresa
    	StringBuffer query=new StringBuffer("SELECT NUMDSP, FECDSP, CANDSP, UNDMED, EMPTRA, PLACA, NOMCON, DIRDSP, CANKGS " +
    										"FROM SAP_DET_DSP WHERE LTRIM(RTRIM(CODCLI)) = '"+entity.getId().getCodcli().trim()+ "' AND " +
    										"LTRIM(RTRIM(NUMPED)) = '"+entity.getId().getNumped().trim()+"' AND " +
    										"LTRIM(RTRIM(ITMPED)) = '"+entity.getId().getItmped().trim()+"' ");
    	List registros=null;
    	List<SapDetDsp> resultado= null;
    	try {
	    	
    		System.out.println(query.toString());
    		
    		//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<SapDetDsp>();           
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
	    	DecimalFormat df = new DecimalFormat("###,###,###,##0");
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg = (Object[])registros.get(i);
	    		
	    		SapDetDspId sapDetDspId = new SapDetDspId();
	    		SapDetDsp detDsp = new SapDetDsp();
	    			    		
	    		sapDetDspId.setNumdsp((String)reg[0]);
	    		sapDetDspId.setNumped(entity.getId().getNumped());
	    		
	    		//se valida el resto de los campos ya que no son obligatorios
	    		if(reg[1] != null){
	    			sapDetDspId.setFecdsp(sdf.format((Timestamp)reg[1]));
	    		}
	    		
	    		if(reg[2] != null){
	    			sapDetDspId.setCandsp(df.format(new Double((String)reg[2])));	    			
	    		}

	    		if(reg[3] != null){
	    			sapDetDspId.setUndmed((String)reg[3]);
	    		}
	    		
	    		if(reg[4] != null){
	    			sapDetDspId.setEmptra((String)reg[4]);
	    		}
	    		
	    		if(reg[5] != null){
	    			sapDetDspId.setPlaca((String)reg[5]);
	    		}
	    		
	    		if(reg[6] != null){
	    			sapDetDspId.setNomcon((String)reg[6]);
	            }
	    		
	    		if(reg[7] != null){
	    			sapDetDspId.setDirdsp((String)reg[7]);
	    		}
	    		
	    		if(reg[8] != null){
	    			sapDetDspId.setCankgs(df.format(new Double((String)reg[8])));
	    		}
	    		
	    		
	    		detDsp.setId(sapDetDspId);
	    		
	            resultado.add(detDsp);
	    	}
	    	
	    	return resultado;
	    	
    	} catch (RuntimeException re) {
            throw re;
        }
    }
}
