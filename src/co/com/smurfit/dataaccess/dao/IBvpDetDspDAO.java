package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDetDspId;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for BvpDetDspDAO.
 *
*/
public interface IBvpDetDspDAO {
    /**
     * Perform an initial save of a previously unsaved BvpDetDsp entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpDetDspDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDetDsp entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpDetDsp entity);

    /**
     * Delete a persistent BvpDetDsp entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpDetDspDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpDetDsp entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpDetDsp entity);

    /**
     * Persist a previously saved BvpDetDsp entity and return it or a copy of it
     * to the sender. A copy of the BvpDetDsp entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = IBvpDetDspDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDetDsp entity to update
     * @return BvpDetDsp the persisted BvpDetDsp entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpDetDsp update(BvpDetDsp entity);

    public BvpDetDsp findById(BvpDetDspId id);

    /**
     * Find all BvpDetDsp entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpDetDsp property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpDetDsp> found by query
     */
    public List<BvpDetDsp> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<BvpDetDsp> findByCriteria(String whereCondition);

    public List<BvpDetDsp> findById(Object id);

    public List<BvpDetDsp> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all BvpDetDsp entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpDetDsp> all BvpDetDsp entities
     */
    public List<BvpDetDsp> findAll(int... rowStartIdxAndCount);

    public List<BvpDetDsp> findPageBvpDetDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberBvpDetDsp();
    
    public List<BvpDetDsp> consultarDetalleDespachoBvp(BvpDetPed entity);
}
