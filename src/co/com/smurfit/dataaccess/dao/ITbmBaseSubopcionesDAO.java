package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TbmBaseSubopcionesDAO.
 *
*/
public interface ITbmBaseSubopcionesDAO {
    /**
     * Perform an initial save of a previously unsaved TbmBaseSubopciones entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseSubopcionesDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopciones entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseSubopciones entity);

    /**
     * Delete a persistent TbmBaseSubopciones entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseSubopcionesDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopciones entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseSubopciones entity);

    /**
     * Persist a previously saved TbmBaseSubopciones entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseSubopciones entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITbmBaseSubopcionesDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopciones entity to update
     * @return TbmBaseSubopciones the persisted TbmBaseSubopciones entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseSubopciones update(TbmBaseSubopciones entity);

    public TbmBaseSubopciones findById(Long id);

    /**
     * Find all TbmBaseSubopciones entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseSubopciones property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseSubopciones> found by query
     */
    public List<TbmBaseSubopciones> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TbmBaseSubopciones> findByCriteria(String whereCondition);

    public List<TbmBaseSubopciones> findByActivo(Object activo);

    public List<TbmBaseSubopciones> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TbmBaseSubopciones> findByDescripcion(Object descripcion);

    public List<TbmBaseSubopciones> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount);

    public List<TbmBaseSubopciones> findByFechaCreacion(Object fechaCreacion);

    public List<TbmBaseSubopciones> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TbmBaseSubopciones> findByIdSuop(Object idSuop);

    public List<TbmBaseSubopciones> findByIdSuop(Object idSuop,
        int... rowStartIdxAndCount);

    public List<TbmBaseSubopciones> findByNombre(Object nombre);

    public List<TbmBaseSubopciones> findByNombre(Object nombre,
        int... rowStartIdxAndCount);

    public List<TbmBaseSubopciones> findByUrl(Object url);

    public List<TbmBaseSubopciones> findByUrl(Object url,
        int... rowStartIdxAndCount);

    /**
     * Find all TbmBaseSubopciones entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseSubopciones> all TbmBaseSubopciones entities
     */
    public List<TbmBaseSubopciones> findAll(int... rowStartIdxAndCount);

    public List<TbmBaseSubopciones> findPageTbmBaseSubopciones(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTbmBaseSubopciones();
}
