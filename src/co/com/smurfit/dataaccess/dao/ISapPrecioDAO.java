package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.SapPrecio;
import co.com.smurfit.sccvirtual.entidades.SapPrecioId;
import co.com.smurfit.sccvirtual.vo.ProductoVO;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for SapPrecioDAO.
 *
*/
public interface ISapPrecioDAO {
    /**
     * Perform an initial save of a previously unsaved SapPrecio entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapPrecioDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapPrecio entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapPrecio entity);

    /**
     * Delete a persistent SapPrecio entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapPrecioDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapPrecio entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapPrecio entity);

    /**
     * Persist a previously saved SapPrecio entity and return it or a copy of it
     * to the sender. A copy of the SapPrecio entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ISapPrecioDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapPrecio entity to update
     * @return SapPrecio the persisted SapPrecio entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapPrecio update(SapPrecio entity);

    public SapPrecio findById(SapPrecioId id);

    /**
     * Find all SapPrecio entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapPrecio property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapPrecio> found by query
     */
    public List<SapPrecio> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<SapPrecio> findByCriteria(String whereCondition);

    public List<SapPrecio> findByDespro(Object despro);

    public List<SapPrecio> findByDespro(Object despro,
        int... rowStartIdxAndCount);

    public List<SapPrecio> findByDiamtr(Object diamtr);

    public List<SapPrecio> findByDiamtr(Object diamtr,
        int... rowStartIdxAndCount);

    public List<SapPrecio> findByEje(Object eje);

    public List<SapPrecio> findByEje(Object eje, int... rowStartIdxAndCount);

    public List<SapPrecio> findById(Object id);

    public List<SapPrecio> findById(Object id, int... rowStartIdxAndCount);

    public List<SapPrecio> findByMoneda(Object moneda);

    public List<SapPrecio> findByMoneda(Object moneda,
        int... rowStartIdxAndCount);

    public List<SapPrecio> findByTiprec(Object tiprec);

    public List<SapPrecio> findByTiprec(Object tiprec,
        int... rowStartIdxAndCount);

    public List<SapPrecio> findByUndmed(Object undmed);

    public List<SapPrecio> findByUndmed(Object undmed,
        int... rowStartIdxAndCount);

    /**
     * Find all SapPrecio entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapPrecio> all SapPrecio entities
     */
    public List<SapPrecio> findAll(int... rowStartIdxAndCount);

    public List<SapPrecio> findPageSapPrecio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberSapPrecio();
    
    public List<ProductoVO> consultarProductosSap(String codigoProd, String descripcion, String coddigoMas, String nombreColumna, Boolean ascendente);
    
    public List<ProductoVO> consultarPreciosSap(String codigoProd, String descripcion, String coddigoMas, String nombreColumna, Boolean ascendente);
}
