package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TbmBaseErrores;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * TbmBaseErrores entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TbmBaseErrores
 */
public class TbmBaseErroresDAO implements ITbmBaseErroresDAO {
    // property constants

    //public static final String  ACTIVO = "activo";
    public static final String ACTIVO = "activo";

    //public static final Long  CODIGO = "codigo";
    public static final String CODIGO = "codigo";

    //public static final String  DESCRIPCION = "descripcion";
    public static final String DESCRIPCION = "descripcion";

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Long  IDERR = "idErr";
    public static final String IDERR = "idErr";

    //public static final String  SOLUCION = "solucion";
    public static final String SOLUCION = "solucion";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TbmBaseErrores entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseErroresDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseErrores entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseErrores entity) {
        EntityManagerHelper.log("saving TbmBaseErrores instance", Level.INFO,
            null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent TbmBaseErrores entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseErroresDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseErrores entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseErrores entity) {
        EntityManagerHelper.log("deleting TbmBaseErrores instance", Level.INFO,
            null);

        try {
            entity = getEntityManager()
                         .getReference(TbmBaseErrores.class, entity.getIdErr());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TbmBaseErrores entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseErrores entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TbmBaseErroresDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseErrores entity to update
     * @return TbmBaseErrores the persisted TbmBaseErrores entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseErrores update(TbmBaseErrores entity) {
        EntityManagerHelper.log("updating TbmBaseErrores instance", Level.INFO,
            null);

        try {
            TbmBaseErrores result = getEntityManager()
                                        .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TbmBaseErrores findById(Long id) {
        EntityManagerHelper.log("finding TbmBaseErrores instance with id: " +
            id, Level.INFO, null);

        try {
            TbmBaseErrores instance = getEntityManager()
                                          .find(TbmBaseErrores.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TbmBaseErrores entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TbmBaseErrores property to query
     * @param value
     *            the property value to match
     * @return List< TbmBaseErrores> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseErrores> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TbmBaseErrores instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TbmBaseErrores model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TbmBaseErrores entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseErrores property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TbmBaseErrores> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseErrores> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TbmBaseErrores instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseErrores model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseErrores> findByActivo(Object activo,
        int... rowStartIdxAndCount) {
        return findByProperty(ACTIVO, activo, rowStartIdxAndCount);
    }

    public List<TbmBaseErrores> findByActivo(Object activo) {
        return findByProperty(ACTIVO, activo);
    }

    public List<TbmBaseErrores> findByCodigo(Object codigo,
        int... rowStartIdxAndCount) {
        return findByProperty(CODIGO, codigo, rowStartIdxAndCount);
    }

    public List<TbmBaseErrores> findByCodigo(Object codigo) {
        return findByProperty(CODIGO, codigo);
    }

    public List<TbmBaseErrores> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount) {
        return findByProperty(DESCRIPCION, descripcion, rowStartIdxAndCount);
    }

    public List<TbmBaseErrores> findByDescripcion(Object descripcion) {
        return findByProperty(DESCRIPCION, descripcion);
    }

    public List<TbmBaseErrores> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TbmBaseErrores> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TbmBaseErrores> findByIdErr(Object idErr,
        int... rowStartIdxAndCount) {
        return findByProperty(IDERR, idErr, rowStartIdxAndCount);
    }

    public List<TbmBaseErrores> findByIdErr(Object idErr) {
        return findByProperty(IDERR, idErr);
    }

    public List<TbmBaseErrores> findBySolucion(Object solucion,
        int... rowStartIdxAndCount) {
        return findByProperty(SOLUCION, solucion, rowStartIdxAndCount);
    }

    public List<TbmBaseErrores> findBySolucion(Object solucion) {
        return findByProperty(SOLUCION, solucion);
    }

    /**
     * Find all TbmBaseErrores entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseErrores> all TbmBaseErrores entities
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseErrores> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TbmBaseErrores instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseErrores model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseErrores> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TbmBaseErrores model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TbmBaseErrores> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TbmBaseErrores failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseErrores> findPageTbmBaseErrores(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TbmBaseErrores model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TbmBaseErrores model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTbmBaseErrores() {
        try {
            String queryString = "select count(*) from TbmBaseErrores model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
}
