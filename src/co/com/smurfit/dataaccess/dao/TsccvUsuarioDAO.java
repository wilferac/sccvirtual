package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TsccvDetalleGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvUsuario entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvUsuario
 */
public class TsccvUsuarioDAO implements ITsccvUsuarioDAO {
    // property constants

    //public static final String  ACTIVO = "activo";
    public static final String ACTIVO = "activo";

    //public static final String  APELLIDO = "apellido";
    public static final String APELLIDO = "apellido";

    //public static final Integer  CEDULA = "cedula";
    public static final String CEDULA = "cedula";

    //public static final String  EMAIL = "email";
    public static final String EMAIL = "email";

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Integer  IDUSUA = "idUsua";
    public static final String IDUSUA = "idUsua";

    //public static final String  LOGIN = "login";
    public static final String LOGIN = "login";

    //public static final String  NOMBRE = "nombre";
    public static final String NOMBRE = "nombre";

    //public static final String  PASSWORD = "password";
    public static final String PASSWORD = "password";
    
    //public static final String GRUPO_USUARIO = "tbmBaseGruposUsuarios.idGrup";
    public static final String GRUPO_USUARIO = "tbmBaseGruposUsuarios.idGrup";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TsccvUsuario entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvUsuarioDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvUsuario entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvUsuario entity) {
        /*EntityManagerHelper.log("saving TsccvUsuario instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }*/
    }

    /**
     * Delete a persistent TsccvUsuario entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvUsuarioDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvUsuario entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    
    
    public static void saveUsuario(TsccvUsuario entity) {
        EntityManagerHelper.log("saving TsccvUsuario instance", Level.INFO, null);
        Integer idUsua= null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	idUsua = (Integer) EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_usua) from tsccv_usuario").getSingleResult();
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro        	
        	idUsua = idUsua != null ? (idUsua + 1) : 1;
        	entity.setIdUsua(idUsua);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }
    
    
    public void delete(TsccvUsuario entity) {
        EntityManagerHelper.log("deleting TsccvUsuario instance", Level.INFO,
            null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvUsuario.class, entity.getIdUsua());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvUsuario entity and return it or a copy of it
     * to the sender. A copy of the TsccvUsuario entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvUsuarioDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvUsuario entity to update
     * @return TsccvUsuario the persisted TsccvUsuario entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvUsuario update(TsccvUsuario entity) {
        EntityManagerHelper.log("updating TsccvUsuario instance", Level.INFO,
            null);

        try {
            /*TsccvUsuario result = getEntityManager()
                                      .merge(entity);*/
        	((Session)getEntityManager().getDelegate()).update(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return entity;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvUsuario findById(Integer id) {
        EntityManagerHelper.log("finding TsccvUsuario instance with id: " + id,
            Level.INFO, null);

        try {
            TsccvUsuario instance = getEntityManager()
                                        .find(TsccvUsuario.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvUsuario entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvUsuario property to query
     * @param value
     *            the property value to match
     * @return List< TsccvUsuario> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvUsuario> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TsccvUsuario instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvUsuario model where model." +
                propertyName + "= :propertyValue order by model.idUsua asc";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvUsuario entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvUsuario property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvUsuario> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvUsuario> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding TsccvUsuario instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvUsuario model where model." +
                propertyName + "= :propertyValue order by model.idUsua asc";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvUsuario> findByActivo(Object activo,
        int... rowStartIdxAndCount) {
        return findByProperty(ACTIVO, activo, rowStartIdxAndCount);
    }

    public List<TsccvUsuario> findByActivo(Object activo) {
        return findByProperty(ACTIVO, activo);
    }

    public List<TsccvUsuario> findByApellido(Object apellido,
        int... rowStartIdxAndCount) {
        return findByProperty(APELLIDO, apellido, rowStartIdxAndCount);
    }

    public List<TsccvUsuario> findByApellido(Object apellido) {
        return findByProperty(APELLIDO, apellido);
    }

    public List<TsccvUsuario> findByCedula(Object cedula,
        int... rowStartIdxAndCount) {
        return findByProperty(CEDULA, cedula, rowStartIdxAndCount);
    }

    public List<TsccvUsuario> findByCedula(Object cedula) {
        return findByProperty(CEDULA, cedula);
    }

    public List<TsccvUsuario> findByEmail(Object email,
        int... rowStartIdxAndCount) {
        return findByProperty(EMAIL, email, rowStartIdxAndCount);
    }

    public List<TsccvUsuario> findByEmail(Object email) {
        return findByProperty(EMAIL, email);
    }

    public List<TsccvUsuario> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TsccvUsuario> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TsccvUsuario> findByIdUsua(Object idUsua,
        int... rowStartIdxAndCount) {
        return findByProperty(IDUSUA, idUsua, rowStartIdxAndCount);
    }

    public List<TsccvUsuario> findByIdUsua(Object idUsua) {
        return findByProperty(IDUSUA, idUsua);
    }

    public List<TsccvUsuario> findByLogin(Object login,
        int... rowStartIdxAndCount) {
        return findByProperty(LOGIN, login, rowStartIdxAndCount);
    }

    public List<TsccvUsuario> findByLogin(Object login) {
        return findByProperty(LOGIN, login);
    }

    public List<TsccvUsuario> findByNombre(Object nombre,
        int... rowStartIdxAndCount) {
        return findByProperty(NOMBRE, nombre, rowStartIdxAndCount);
    }

    public List<TsccvUsuario> findByNombre(Object nombre) {
        return findByProperty(NOMBRE, nombre);
    }

    public List<TsccvUsuario> findByPassword(Object password,
        int... rowStartIdxAndCount) {
        return findByProperty(PASSWORD, password, rowStartIdxAndCount);
    }

    public List<TsccvUsuario> findByPassword(Object password) {
        return findByProperty(PASSWORD, password);
    }

    /**
     * Find all TsccvUsuario entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvUsuario> all TsccvUsuario entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvUsuario> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvUsuario instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvUsuario model order by model.idUsua asc";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvUsuario> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvUsuario model order by model.idUsua asc" +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvUsuario> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvUsuario failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvUsuario> findPageTsccvUsuario(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvUsuario model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvUsuario model order by model.idUsua asc";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvUsuario() {
        try {
            String queryString = "select count(*) from TsccvUsuario model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    @SuppressWarnings("unchecked")
    public Long findTotalNumberBpmpCliente() {
        try {
            String queryString = "select count(*) from BpmpCliente model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //Metodo utilizado para la consulta por todos los campos
    public List<TsccvUsuario> consultarUsuarios(TsccvUsuario entity){
    	StringBuffer query=new StringBuffer("SELECT ID_USUA, ID_EMPR, ID_GRUP, CODPLA, LOGIN, PASSWORD, " +
    										"NOMBRE, APELLIDO, CEDULA, EMAIL, ACTIVO, FECHA_CREACION FROM TSCCV_USUARIO ");
    	List registros=null;
    	List<TsccvUsuario> resultado= null;
    	try {
	    	//Validamos si los campos vienen con valor o no para armar el query
	    	if(entity.getIdUsua() != null){
	    		query.append("WHERE ID_USUA LIKE '%"+entity.getIdUsua()+"%' ");
	    	}
	    	else{
	    		query.append("WHERE ID_USUA LIKE '%' ");
	    	}
	    	
	    	if(entity.getTsccvEmpresas() != null){
	    		query.append("AND ID_EMPR LIKE '%"+entity.getTsccvEmpresas().getIdEmpr()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ID_EMPR LIKE '%' OR ID_EMPR IS NULL) ");
	    	}
	    	
	    	if(entity.getTbmBaseGruposUsuarios() != null){
	    		query.append("AND ID_GRUP LIKE '%"+entity.getTbmBaseGruposUsuarios().getIdGrup()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ID_GRUP LIKE '%' OR ID_GRUP IS NULL) ");
	    	}
	    	
	    	if(entity.getTsccvPlanta() != null){
	    		query.append("AND CODPLA LIKE '%"+entity.getTsccvPlanta().getCodpla()+"%' ");
	    	}
	    	else{
	    		query.append("AND CODPLA LIKE '%' ");
	    	}
	    	
	    	if(entity.getLogin() != null){
	    		query.append("AND LOGIN LIKE '%"+entity.getLogin()+"%' ");
	    	}
	    	else{
	    		query.append("AND LOGIN LIKE '%' ");
	    	}
	    	
	    	if(entity.getPassword() != null){
	    		query.append("AND PASSWORD LIKE '%"+entity.getPassword()+"%' ");
	    	}
	    	else{
	    		query.append("AND PASSWORD LIKE '%' ");
	    	}
	    	
	    	if(entity.getNombre() != null){
	    		query.append("AND NOMBRE LIKE '%"+entity.getNombre()+"%' ");
	    	}
	    	else{
	    		query.append("AND NOMBRE LIKE '%' ");
	    	}
	    	
	    	if(entity.getApellido() != null){
	    		query.append("AND APELLIDO LIKE '%"+entity.getApellido()+"%' ");
	    	}
	    	else{
	    		query.append("AND APELLIDO LIKE '%' ");
	    	}
	    	
	    	if(entity.getCedula() != null){
	    		query.append("AND CEDULA LIKE '%"+entity.getCedula()+"%' ");
	    	}
	    	else{
	    		query.append("AND CEDULA LIKE '%' ");
	    	}
	    	
	    	if(entity.getEmail() != null){
	    		query.append("AND EMAIL LIKE '%"+entity.getEmail()+"%' ");
	    	}
	    	else{
	    		query.append("AND EMAIL LIKE '%' ");
	    	}
	    	
	    	if(entity.getActivo() != null){
	    		query.append("AND ACTIVO LIKE '%"+entity.getActivo()+"%' ");
	    	}
	    	else{
	    		query.append("AND ACTIVO LIKE '%' ");
	    	}
	    	
	    	query.append("ORDER BY ID_USUA ASC ");
	    	
	    	//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<TsccvUsuario>();
	    	ITsccvPlantaLogic logicTsccvPlanta = new TsccvPlantaLogic();
	    	ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios = new TbmBaseGruposUsuariosLogic();
            ITsccvEmpresasLogic logicTsccvEmpresas = new TsccvEmpresasLogic();
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TsccvUsuario usu= new TsccvUsuario();
	    		usu.setIdUsua((Integer)reg[0]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO EMPRESA YA QUE NO ES OBLIGATORIO
	    		if(reg[1] != null){
	    			TsccvEmpresas tsccvEmpresasClass = logicTsccvEmpresas.getTsccvEmpresas((Integer)reg[1]);
	    			usu.setTsccvEmpresas(tsccvEmpresasClass);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO GRUPO YA QUE NO ES OBLIGATORIO
	    		if(reg[2] != null){
	    			TbmBaseGruposUsuarios tbmBaseGruposUsuariosClass = logicTbmBaseGruposUsuarios.getTbmBaseGruposUsuarios((((BigDecimal)reg[2]).longValue()));
	    			usu.setTbmBaseGruposUsuarios(tbmBaseGruposUsuariosClass);
	    		}
	    		
	    		TsccvPlanta tsccvPlanta = logicTsccvPlanta.getTsccvPlanta((String)reg[3]);
	    		usu.setTsccvPlanta(tsccvPlanta);
	    		
	    		usu.setLogin((String)reg[4]);
	    		usu.setPassword((String)reg[5]);
	    		usu.setNombre((String)reg[6]);
	    		usu.setApellido((String)reg[7]);
	    		usu.setCedula((Integer)reg[8]);
	    		usu.setEmail((String)reg[9]);
	    		usu.setActivo((String)reg[10]);
	    		usu.setFechaCreacion((Date)reg[11]);
	    		
	            resultado.add(usu);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
    
    //consulta los usuario con los datos para saber si esta registrado
    public Boolean usuarioIsRegistrado(String login, Integer cedula, Integer idEmpr) throws Exception{
    	List resultado = null;    	
    	
    	try{
	    	String query = "SELECT ID_USUA FROM TSCCV_USUARIO USUA WHERE USUA.LOGIN = '"+login+"' ";	    	    	
	    	resultado = EntityManagerHelper.getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(resultado != null && resultado.size() != 0){
	    		throw new Exception("El campo Usuario ya esta registrado en el sistema.");
	    	}
	    	
	    	query = "SELECT ID_USUA FROM TSCCV_USUARIO USUA WHERE USUA.CEDULA = "+cedula+" AND USUA.ID_EMPR = "+idEmpr+" ";	    	    	
	    	resultado = EntityManagerHelper.getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(resultado != null && resultado.size() != 0){
	    		throw new Exception("La C�dula y Empresa ya se encuentran registradas en el sistema.");
	    	}
	    	
	    	return false;
    	}catch(Exception e){
    		throw e;
    	}
    }
    
    public void inactivarUsuario(Integer idUsua){    	
    	EntityManagerHelper.log("inactivating TsccvUsuario instance", Level.INFO, null);
    	String query = "update TsccvUsuario model set activo = '1' where model.idUsua = "+idUsua+" ";
    	
		try{
			getEntityManager().createQuery(query).executeUpdate();
			EntityManagerHelper.log("inactivation successful", Level.INFO, null);
		}catch(RuntimeException re){
			EntityManagerHelper.log("inactivation failed", Level.SEVERE, re);
			throw re;
		}
    }
    
    
    //Metodo utilizado para consultar los usuarios por grupo
    public List<TsccvUsuario> consultarUsuariosPorGrupo(Integer idGrup){
    	String query="SELECT ID_USUA, ID_EMPR, ID_GRUP, CODPLA, LOGIN, PASSWORD, " +
					 "NOMBRE, APELLIDO, CEDULA, EMAIL, ACTIVO, FECHA_CREACION " +
					 "FROM TSCCV_USUARIO " +
					 "WHERE ID_GRUP = "+idGrup+" ";
    	List registros=null;
    	List<TsccvUsuario> resultado = new ArrayList<TsccvUsuario>();
    	try {
	    	//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return resultado;
	    	}
	    		    	
	    	ITsccvPlantaLogic logicTsccvPlanta = new TsccvPlantaLogic();
	    	ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios = new TbmBaseGruposUsuariosLogic();
            ITsccvEmpresasLogic logicTsccvEmpresas = new TsccvEmpresasLogic();
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TsccvUsuario usu= new TsccvUsuario();
	    		usu.setIdUsua((Integer)reg[0]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO EMPRESA YA QUE NO ES OBLIGATORIO
	    		if(reg[1] != null){
	    			TsccvEmpresas tsccvEmpresasClass = logicTsccvEmpresas.getTsccvEmpresas((Integer)reg[1]);
	    			usu.setTsccvEmpresas(tsccvEmpresasClass);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO GRUPO YA QUE NO ES OBLIGATORIO
	    		if(reg[2] != null){
	    			TbmBaseGruposUsuarios tbmBaseGruposUsuariosClass = logicTbmBaseGruposUsuarios.getTbmBaseGruposUsuarios((((BigDecimal)reg[2]).longValue()));
	    			usu.setTbmBaseGruposUsuarios(tbmBaseGruposUsuariosClass);
	    		}
	    		
	    		TsccvPlanta tsccvPlanta = logicTsccvPlanta.getTsccvPlanta((String)reg[3]);
	    		usu.setTsccvPlanta(tsccvPlanta);
	    		
	    		usu.setLogin((String)reg[4]);
	    		usu.setPassword((String)reg[5]);
	    		usu.setNombre((String)reg[6]);
	    		usu.setApellido((String)reg[7]);
	    		usu.setCedula((Integer)reg[8]);
	    		usu.setEmail((String)reg[9]);
	    		usu.setActivo((String)reg[10]);
	    		usu.setFechaCreacion((Date)reg[11]);
	    		
	            resultado.add(usu);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
    
    public int cambiarPassword(TsccvUsuario tsccvUsuario, String passwordNuevo){    	
    	EntityManagerHelper.log("updating TsccvUsuario instance", Level.INFO, null);
    	int numUpdate = 0;
    	String query = "update TsccvUsuario model set model.password = '"+passwordNuevo+"' " +
    				   "where model.idUsua = "+tsccvUsuario.getIdUsua()+" and model.password = '"+tsccvUsuario.getPassword()+"' ";
    	
		try{
			numUpdate = getEntityManager().createQuery(query).executeUpdate();
			
			return numUpdate;
		}catch(RuntimeException re){
			EntityManagerHelper.log("update failed", Level.SEVERE, re);
			throw re;
		}
    }
    
    
    //Metodo utilizado para la consultar los usuarios con grupo vendedor 
    public List<TsccvUsuario> consultarVendedores(Integer codCli, String codPla){
    	List registros=null;
    	List<TsccvUsuario> resultado = null;
    	try {
    		//Se crea un objeto del archivo propiedades		
            ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
            
            ITsccvEmpresasLogic logicTsccvEmpresas = new TsccvEmpresasLogic();
            TsccvEmpresas tsccvEmpresasClass = logicTsccvEmpresas.getTsccvEmpresas(codCli);
            
        	StringBuffer query = new StringBuffer("SELECT ID_USUA, ID_EMPR, ID_GRUP, CODPLA, [LOGIN], PASSWORD, " +
    											  "NOMBRE, APELLIDO, CEDULA, EMAIL, ACTIVO, FECHA_CREACION FROM TSCCV_USUARIO " +
        										  "WHERE ID_GRUP = "+archivoPropiedades.getProperty("VENDEDOR")+" AND EMAIL IS NOT NULL AND CODPLA = '"+codPla+"' AND " +
        										  "ID_EMPR IN (SELECT ID_EMPR FROM TSCCV_EMPRESAS WHERE ID_GRUE IN (SELECT ID_GRUE "+ 
        										  				"FROM TSCCV_DETALLE_GRUPO_EMPRESA WHERE ID_EMPR="+codCli+"))");
        	
        	/*SELECT ID_USUA, ID_EMPR, ID_GRUP, CODPLA, [LOGIN], PASSWORD,
        	NOMBRE, APELLIDO, CEDULA, EMAIL, ACTIVO, FECHA_CREACION
        	 
        	FROM TSCCV_USUARIO

        	WHERE ID_GRUP = 3
        	AND EMAIL IS NOT NULL
        	 
        	AND CODPLA = 1
        	 
        	AND ID_EMPR IN 
        		(SELECT ID_EMPR 
        		FROM TSCCV_EMPRESAS 
        		WHERE ID_GRUE IN (SELECT ID_GRUE 
        							FROM TSCCV_DETALLE_GRUPO_EMPRESA 
        							WHERE ID_EMPR=2
        							)
        		)
        	*/
        	/*if(tsccvEmpresasClass.getTsccvGrupoEmpresa() != null){
	        	for(TsccvDetalleGrupoEmpresa detGrupo : tsccvEmpresasClass.getTsccvDetalleGrupoEmpresas()){
	        		query.append(" ,ID_EMPR = "+detGrupo.getTsccvEmpresas().getIdEmpr()+"");
	        	}	        	
        	}
        	query.append(") ");
        	*/
	    	//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<TsccvUsuario>();
	    	ITsccvPlantaLogic logicTsccvPlanta = new TsccvPlantaLogic();
	    	ITbmBaseGruposUsuariosLogic logicTbmBaseGruposUsuarios = new TbmBaseGruposUsuariosLogic();
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TsccvUsuario usu= new TsccvUsuario();
	    		usu.setIdUsua((Integer)reg[0]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO EMPRESA YA QUE NO ES OBLIGATORIO
	    		if(reg[1] != null){
	    			tsccvEmpresasClass = logicTsccvEmpresas.getTsccvEmpresas((Integer)reg[1]);
	    			usu.setTsccvEmpresas(tsccvEmpresasClass);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO GRUPO YA QUE NO ES OBLIGATORIO
	    		if(reg[2] != null){
	    			TbmBaseGruposUsuarios tbmBaseGruposUsuariosClass = logicTbmBaseGruposUsuarios.getTbmBaseGruposUsuarios((((BigDecimal)reg[2]).longValue()));
	    			usu.setTbmBaseGruposUsuarios(tbmBaseGruposUsuariosClass);
	    		}
	    		
	    		TsccvPlanta tsccvPlanta = logicTsccvPlanta.getTsccvPlanta((String)reg[3]);
	    		usu.setTsccvPlanta(tsccvPlanta);
	    		
	    		usu.setLogin((String)reg[4]);
	    		usu.setPassword((String)reg[5]);
	    		usu.setNombre((String)reg[6]);
	    		usu.setApellido((String)reg[7]);
	    		usu.setCedula((Integer)reg[8]);
	    		usu.setEmail((String)reg[9]);
	    		usu.setActivo((String)reg[10]);
	    		usu.setFechaCreacion((Date)reg[11]);
	    		
	            resultado.add(usu);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
    
    public static void consultar(String consulta) {
        EntityManagerHelper.log("consultando pantalla general", Level.INFO, null);
        List registros= null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	System.out.println("INICIA CONSULTA GENERAL: "+new Date());
        	registros = EntityManagerHelper.getEntityManager().createNativeQuery(consulta).getResultList();
        	System.out.println("TERMINA CONSULTA GENERAL: "+new Date());
        } catch (RuntimeException re) {
            EntityManagerHelper.log("fallo consulta general", Level.SEVERE, re);
            throw re;
        }
    }
}
