package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvGrupoEmpresaDAO.
 *
*/
public interface ITsccvGrupoEmpresaDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvGrupoEmpresa entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvGrupoEmpresaDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvGrupoEmpresa entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvGrupoEmpresa entity);

    /**
     * Delete a persistent TsccvGrupoEmpresa entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvGrupoEmpresaDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvGrupoEmpresa entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvGrupoEmpresa entity);

    /**
     * Persist a previously saved TsccvGrupoEmpresa entity and return it or a copy of it
     * to the sender. A copy of the TsccvGrupoEmpresa entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvGrupoEmpresaDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvGrupoEmpresa entity to update
     * @return TsccvGrupoEmpresa the persisted TsccvGrupoEmpresa entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvGrupoEmpresa update(TsccvGrupoEmpresa entity);

    public TsccvGrupoEmpresa findById(Integer id);

    /**
     * Find all TsccvGrupoEmpresa entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvGrupoEmpresa property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvGrupoEmpresa> found by query
     */
    public List<TsccvGrupoEmpresa> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvGrupoEmpresa> findByCriteria(String whereCondition);

    public List<TsccvGrupoEmpresa> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvGrupoEmpresa> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TsccvGrupoEmpresa> findByIdGrue(Object idGrue);

    public List<TsccvGrupoEmpresa> findByIdGrue(Object idGrue,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvGrupoEmpresa entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvGrupoEmpresa> all TsccvGrupoEmpresa entities
     */
    public List<TsccvGrupoEmpresa> findAll(int... rowStartIdxAndCount);

    public List<TsccvGrupoEmpresa> findPageTsccvGrupoEmpresa(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvGrupoEmpresa();
    
    public Integer consultarUltimoIdGrue();
}
