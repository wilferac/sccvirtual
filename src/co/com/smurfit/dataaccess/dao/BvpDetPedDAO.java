package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * BvpDetPed entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.BvpDetPed
 */
public class BvpDetPedDAO implements IBvpDetPedDAO {
    // property constants

    //public static final BvpDetPedId  ID = "id";
    public static final String ID = "id";    

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved BvpDetPed entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpDetPedDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDetPed entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpDetPed entity) {
        EntityManagerHelper.log("saving BvpDetPed instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent BvpDetPed entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpDetPedDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpDetPed entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpDetPed entity) {
        EntityManagerHelper.log("deleting BvpDetPed instance", Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(BvpDetPed.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved BvpDetPed entity and return it or a copy of it
     * to the sender. A copy of the BvpDetPed entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = BvpDetPedDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDetPed entity to update
     * @return BvpDetPed the persisted BvpDetPed entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpDetPed update(BvpDetPed entity) {
        EntityManagerHelper.log("updating BvpDetPed instance", Level.INFO, null);

        try {
            BvpDetPed result = getEntityManager().merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public BvpDetPed findById(BvpDetPedId id) {
        EntityManagerHelper.log("finding BvpDetPed instance with id: " + id,
            Level.INFO, null);

        try {
            BvpDetPed instance = getEntityManager().find(BvpDetPed.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  BvpDetPed entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  BvpDetPed property to query
     * @param value
     *            the property value to match
     * @return List< BvpDetPed> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpDetPed> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  BvpDetPed instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  BvpDetPed model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all BvpDetPed entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpDetPed property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<BvpDetPed> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpDetPed> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding BvpDetPed instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from BvpDetPed model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpDetPed> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<BvpDetPed> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all BvpDetPed entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpDetPed> all BvpDetPed entities
     */
    @SuppressWarnings("unchecked")
    public List<BvpDetPed> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all BvpDetPed instances", Level.INFO,
            null);

        try {
            final String queryString = "select model from BvpDetPed model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpDetPed> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from BvpDetPed model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<BvpDetPed> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in BvpDetPed failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpDetPed> findPageBvpDetPed(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from BvpDetPed model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from BvpDetPed model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberBvpDetPed() {
        try {
            String queryString = "select count(*) from BvpDetPed model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo agregado para consultar los pedidos de acuerdo a la informacion ingresada
    public List<BvpDetPed> consultarPedidos(BvpDetPed entity, Date fechaInicial, Date fechaFinal, String nombreColumna, boolean ascendente){
    	StringBuffer query=new StringBuffer("SELECT PLANTA, NUMPED, ITMPED, CODCLI, CODSCC, CODPRO, TIPPRO, DESPRO, " +
							    			"FECSOL, FECDSP, ESTADO, UNDMED, CNTPED, CNTDSP, ORDCOM " +
							    			"FROM BVP_DET_PED ");
    	List registros=null;
    	List<BvpDetPed> resultado= null;
    	//Se crea un objeto del archivo propiedades		
        ArchivoPropiedades archivoPropiedades = null;
    	try {
	    	archivoPropiedades= new ArchivoPropiedades();
    		
    		//Validamos si los campos vienen con valor o no para armar el query
	    	if(entity.getId() != null && entity.getId().getPlanta() != null){
	    		query.append("WHERE LTRIM(RTRIM(PLANTA)) LIKE '"+entity.getId().getPlanta().trim()+"' ");
	    	}
	    	else{
	    		query.append("WHERE (PLANTA LIKE '%' OR PLANTA IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getNumped() != null){
	    		query.append("AND LTRIM(RTRIM(NUMPED)) LIKE '%"+entity.getId().getNumped().trim()+"%' ");
	    	}
	    	else{
	    		query.append("AND (NUMPED LIKE '%' OR NUMPED IS NULL) ");
	    	}
	    	
	    	/*if(entity.getId() != null && entity.getId().getItmped() != null){
	    		query.append("AND ITMPED LIKE '%"+entity.getId().getItmped()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ITMPED LIKE '%' OR ITMPED IS NULL) ");
	    	}*/	    		    
	    	
	    	if(entity.getId() != null && entity.getId().getCodcli() != null){
	    		query.append("AND LTRIM(RTRIM(CODCLI)) LIKE '"+entity.getId().getCodcli().trim()+"' ");
	    	}
	    	/*else{
	    		query.append("AND (CODCLI LIKE '%' OR CODCLI IS NULL) ");
	    	}*/
	    	
	    	/*if(entity.getId() != null && entity.getId().getCodscc() != null){
	    		query.append("AND CODSCC LIKE '%"+entity.getId().getCodscc()+"%' ");
	    	}
	    	else{
	    		query.append("AND (CODSCC LIKE '%' OR CODSCC IS NULL) ");
	    	}
	    	*/
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND LTRIM(RTRIM(CODPRO)) LIKE '%"+entity.getId().getCodpro().trim()+"%' ");
	    	}
	    	else{
	    		query.append("AND (CODPRO LIKE '%' OR CODPRO IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getTippro() != null){
	    		query.append("AND LTRIM(RTRIM(TIPPRO)) LIKE '"+Utilities.TIPOS_PRODUCTO[Integer.parseInt(entity.getId().getTippro())-1]+"' ");
	    	}
	    	else{
	    		query.append("AND (TIPPRO LIKE '%' OR TIPPRO IS NULL) ");
	    	}
	    	/*
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND DESPRO LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (DESPRO LIKE '%' OR DESPRO IS NULL) ");
	    	}*/
	    	
	    	
	    	//se comenta validacion de fechas ya en sql server no funciona cuando son meses diferentes
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");	    	
	    	if(fechaInicial != null && !fechaInicial.equals("")){
	    		query.append("AND (FECSOL >= CONVERT(DATETIME, '"+sdf.format(fechaInicial)+"', 102)) ");
	    	}
	    	if(fechaFinal != null && !fechaFinal.equals("")){
	    		query.append("AND (FECSOL <= CONVERT(DATETIME, '"+sdf.format(fechaFinal)+"', 102)) ");
	    	}
	    	//if((fechaInicial == null || fechaInicial.equals("")) && (fechaFinal == null || !fechaFinal.equals(""))){
	    		query.append("AND FECSOL >= CONVERT(DATETIME, DATEADD(MONTH, (-"+archivoPropiedades.getProperty("meses_consulta_pedidos")+"), GETDATE()), 102) ");
	    	//}
	    	
	    	
	    	
	    	/*if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND FECDSP LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (FECDSP LIKE '%' OR FECDSP IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND ESTADO LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ESTADO LIKE '%' OR ESTADO IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND UNDMED LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (UNDMED LIKE '%' OR UNDMED IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND CNTPED LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (CNTPED LIKE '%' OR CNTPED IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND CNTDSP LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (CNTDSP LIKE '%' OR CNTDSP IS NULL) ");
	    	}*/
	    	
	    	if(entity.getId() != null && entity.getId().getOrdcom() != null){
	    		query.append("AND LTRIM(RTRIM(ORDCOM)) LIKE '%"+entity.getId().getOrdcom().trim()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ORDCOM LIKE '%' OR ORDCOM IS NULL) ");
	    	}
	    	
	    	//se si contiene el id. del nombre de la columna ya que es un NativeQuery
	    	nombreColumna = nombreColumna.indexOf("id.") != -1 ? nombreColumna.replace("id.", "") : nombreColumna;	    	
	    	query.append("ORDER BY "+nombreColumna+" " + (ascendente ? "asc" : "desc"));
	    	
	    	//Ejecutamos el query
	    	System.out.println(query.toString());
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<BvpDetPed>();           
	    	sdf = new SimpleDateFormat("yyyy.MM.dd");
	    	DecimalFormat df = new DecimalFormat("###,###,###,##0");
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg = (Object[])registros.get(i);
	    		
	    		BvpDetPedId bvpDetPedId = new BvpDetPedId();
	    		BvpDetPed ped = new BvpDetPed();
	    		
	    		
	    		/*se valida la fecha del registro con la ingresada
	    		if(reg[8] != null){	    			
	    			Date fehaReg = new Date(((Timestamp)reg[8]).getTime());
	    			
	    			Calendar tresMesesAtras = Calendar.getInstance();
	    			tresMesesAtras.add(Calendar.MONTH, -3);
	    			if(fehaReg.getTime() < tresMesesAtras.getTime().getTime()){//si la fecha del reggistro es menor a tres meses atras
	    				continue;
	    			}
	    			
	    			if(fechaInicial != null){
	    				if(fehaReg.getTime() < fechaInicial.getTime()){//si la fecha del registro es menor a la fecha inicial ingresada
		    				continue;
		    			}
	    			}
	    			
	    			if(fechaFinal != null){
	    				if(fehaReg.getTime() > fechaFinal.getTime()){//si la fecha del registro es menor a la fecha final ingresada
		    				continue;
		    			}
	    			}	    			
	    		}*/
	    		
	    		
	    		bvpDetPedId.setPlanta((String)reg[0]);	    		
	    		bvpDetPedId.setNumped((String)reg[1]);	    		
	    		bvpDetPedId.setItmped((String)reg[2]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO CODCLI YA QUE NO ES OBLIGATORIO
	    		if(reg[3] != null){
	    			bvpDetPedId.setCodcli((String)reg[3]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO CODSCC YA QUE NO ES OBLIGATORIO
	    		if(reg[4] != null){
	    			bvpDetPedId.setCodscc((String)reg[4]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO CODPRO YA QUE NO ES OBLIGATORIO
	    		if(reg[5] != null){
	    			bvpDetPedId.setCodpro((String)reg[5]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO TIPPRO YA QUE NO ES OBLIGATORIO
	    		if(reg[6] != null){
	    			bvpDetPedId.setTippro((String)reg[6]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO DESPRO YA QUE NO ES OBLIGATORIO
	    		if(reg[7] != null){
	    			bvpDetPedId.setDespro((String)reg[7]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO FECSOL YA QUE NO ES OBLIGATORIO
	    		if(reg[8] != null){	 	    			
	    			bvpDetPedId.setFecsol(sdf.format((Timestamp)reg[8]));
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO FECDSP YA QUE NO ES OBLIGATORIO
	    		if(reg[9] != null){
	    			bvpDetPedId.setFecdsp(sdf.format((Timestamp)reg[9]));
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO ESTADO YA QUE NO ES OBLIGATORIO
	    		if(reg[10] != null){
	    			bvpDetPedId.setEstado((String)reg[10]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO UNDMED YA QUE NO ES OBLIGATORIO
	    		if(reg[11] != null){
	    			bvpDetPedId.setUndmed((String)reg[11]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO CNTPED YA QUE NO ES OBLIGATORIO
	    		if(reg[12] != null){
	    			bvpDetPedId.setCntped(df.format(new Double((String)reg[12])));
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO CNTDSP YA QUE NO ES OBLIGATORIO
	    		if(reg[13] != null){
	    			bvpDetPedId.setCntdsp(df.format(new Double((String)reg[13])));
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO ORDCOM YA QUE NO ES OBLIGATORIO
	    		if(reg[14] != null){
	    			bvpDetPedId.setOrdcom((String)reg[14]);
	    		}
	    		
	    		ped.setId(bvpDetPedId);
	    		
	            resultado.add(ped);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
}
