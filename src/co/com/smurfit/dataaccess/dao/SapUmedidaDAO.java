package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * SapUmedida entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.SapUmedida
 */
public class SapUmedidaDAO implements ISapUmedidaDAO {
    // property constants

    //public static final SapUmedidaId  ID = "id";
    public static final String ID = "id";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved SapUmedida entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapUmedidaDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapUmedida entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapUmedida entity) {
        EntityManagerHelper.log("saving SapUmedida instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent SapUmedida entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapUmedidaDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapUmedida entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapUmedida entity) {
        EntityManagerHelper.log("deleting SapUmedida instance", Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(SapUmedida.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved SapUmedida entity and return it or a copy of it
     * to the sender. A copy of the SapUmedida entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = SapUmedidaDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapUmedida entity to update
     * @return SapUmedida the persisted SapUmedida entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapUmedida update(SapUmedida entity) {
        EntityManagerHelper.log("updating SapUmedida instance", Level.INFO, null);

        try {
            SapUmedida result = getEntityManager().merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public SapUmedida findById(SapUmedidaId id) {
        EntityManagerHelper.log("finding SapUmedida instance with id: " + id,
            Level.INFO, null);

        try {
            SapUmedida instance = getEntityManager().find(SapUmedida.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  SapUmedida entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  SapUmedida property to query
     * @param value
     *            the property value to match
     * @return List< SapUmedida> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapUmedida> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  SapUmedida instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  SapUmedida model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all SapUmedida entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapUmedida property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<SapUmedida> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapUmedida> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding SapUmedida instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from SapUmedida model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapUmedida> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<SapUmedida> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all SapUmedida entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapUmedida> all SapUmedida entities
     */
    @SuppressWarnings("unchecked")
    public List<SapUmedida> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all SapUmedida instances", Level.INFO,
            null);

        try {
            final String queryString = "select model from SapUmedida model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapUmedida> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from SapUmedida model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<SapUmedida> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in SapUmedida failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapUmedida> findPageSapUmedida(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from SapUmedida model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from SapUmedida model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberSapUmedida() {
        try {
            String queryString = "select count(*) from SapUmedida model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
}
