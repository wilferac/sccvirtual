package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvEntregasPedidoDAO.
 *
*/
public interface ITsccvEntregasPedidoDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvEntregasPedido entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvEntregasPedidoDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEntregasPedido entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvEntregasPedido entity);

    /**
     * Delete a persistent TsccvEntregasPedido entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvEntregasPedidoDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvEntregasPedido entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvEntregasPedido entity);

    /**
     * Persist a previously saved TsccvEntregasPedido entity and return it or a copy of it
     * to the sender. A copy of the TsccvEntregasPedido entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvEntregasPedidoDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEntregasPedido entity to update
     * @return TsccvEntregasPedido the persisted TsccvEntregasPedido entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvEntregasPedido update(TsccvEntregasPedido entity);

    public TsccvEntregasPedido findById(Integer id);

    /**
     * Find all TsccvEntregasPedido entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvEntregasPedido property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvEntregasPedido> found by query
     */
    public List<TsccvEntregasPedido> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvEntregasPedido> findByCriteria(String whereCondition);

    public List<TsccvEntregasPedido> findByCantidad(Object cantidad);

    public List<TsccvEntregasPedido> findByCantidad(Object cantidad,
        int... rowStartIdxAndCount);

    public List<TsccvEntregasPedido> findByDesdir(Object desdir);

    public List<TsccvEntregasPedido> findByDesdir(Object desdir,
        int... rowStartIdxAndCount);

    public List<TsccvEntregasPedido> findByFechaEntrega(Object fechaEntrega);

    public List<TsccvEntregasPedido> findByFechaEntrega(Object fechaEntrega,
        int... rowStartIdxAndCount);

    public List<TsccvEntregasPedido> findByIdEnpe(Object idEnpe);

    public List<TsccvEntregasPedido> findByIdEnpe(Object idEnpe,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvEntregasPedido entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvEntregasPedido> all TsccvEntregasPedido entities
     */
    public List<TsccvEntregasPedido> findAll(int... rowStartIdxAndCount);

    public List<TsccvEntregasPedido> findPageTsccvEntregasPedido(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvEntregasPedido();
}
