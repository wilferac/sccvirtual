package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for BvpHistDespachosDAO.
 *
*/
public interface IBvpHistDespachosDAO {
    /**
     * Perform an initial save of a previously unsaved BvpHistDespachos entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpHistDespachosDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpHistDespachos entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpHistDespachos entity);

    /**
     * Delete a persistent BvpHistDespachos entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpHistDespachosDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpHistDespachos entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpHistDespachos entity);

    /**
     * Persist a previously saved BvpHistDespachos entity and return it or a copy of it
     * to the sender. A copy of the BvpHistDespachos entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = IBvpHistDespachosDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpHistDespachos entity to update
     * @return BvpHistDespachos the persisted BvpHistDespachos entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpHistDespachos update(BvpHistDespachos entity);

    public BvpHistDespachos findById(BvpHistDespachosId id);

    /**
     * Find all BvpHistDespachos entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpHistDespachos property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpHistDespachos> found by query
     */
    public List<BvpHistDespachos> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<BvpHistDespachos> findByCriteria(String whereCondition);

    public List<BvpHistDespachos> findById(Object id);

    public List<BvpHistDespachos> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all BvpHistDespachos entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpHistDespachos> all BvpHistDespachos entities
     */
    public List<BvpHistDespachos> findAll(int... rowStartIdxAndCount);

    public List<BvpHistDespachos> findPageBvpHistDespachos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberBvpHistDespachos();
    
    public List<BvpHistDespachos> consultarHistoricoDespachoBvp(String codVpe, TsccvPlanta tsccvPlanta, String periodo);
    
    public String minPeriodo(String codVpe, TsccvPlanta tsccvPlanta);
}
