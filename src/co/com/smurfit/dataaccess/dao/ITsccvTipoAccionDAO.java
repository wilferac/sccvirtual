package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvTipoAccion;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvTipoAccionDAO.
 *
*/
public interface ITsccvTipoAccionDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvTipoAccion entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvTipoAccionDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvTipoAccion entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvTipoAccion entity);

    /**
     * Delete a persistent TsccvTipoAccion entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvTipoAccionDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvTipoAccion entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvTipoAccion entity);

    /**
     * Persist a previously saved TsccvTipoAccion entity and return it or a copy of it
     * to the sender. A copy of the TsccvTipoAccion entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvTipoAccionDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvTipoAccion entity to update
     * @return TsccvTipoAccion the persisted TsccvTipoAccion entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvTipoAccion update(TsccvTipoAccion entity);

    public TsccvTipoAccion findById(Long id);

    /**
     * Find all TsccvTipoAccion entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvTipoAccion property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvTipoAccion> found by query
     */
    public List<TsccvTipoAccion> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvTipoAccion> findByCriteria(String whereCondition);

    public List<TsccvTipoAccion> findByDescripcion(Object descripcion);

    public List<TsccvTipoAccion> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount);

    public List<TsccvTipoAccion> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvTipoAccion> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TsccvTipoAccion> findByIdTiac(Object idTiac);

    public List<TsccvTipoAccion> findByIdTiac(Object idTiac,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvTipoAccion entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvTipoAccion> all TsccvTipoAccion entities
     */
    public List<TsccvTipoAccion> findAll(int... rowStartIdxAndCount);

    public List<TsccvTipoAccion> findPageTsccvTipoAccion(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvTipoAccion();
}
