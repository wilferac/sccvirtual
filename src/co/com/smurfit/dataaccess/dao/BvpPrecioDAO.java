package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvPdfsProductosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.entidades.BvpPrecio;
import co.com.smurfit.sccvirtual.entidades.BvpPrecioId;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductoVO;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * BvpPrecio entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.BvpPrecio
 */
public class BvpPrecioDAO implements IBvpPrecioDAO {
    // property constants

    //public static final String  ALTO = "alto";
    public static final String ALTO = "alto";

    //public static final String  ANCHO = "ancho";
    public static final String ANCHO = "ancho";

    //public static final String  ANCSAC = "ancsac";
    public static final String ANCSAC = "ancsac";

    //public static final String  CLAVE = "clave";
    public static final String CLAVE = "clave";

    //public static final String  CNTMAX = "cntmax";
    public static final String CNTMAX = "cntmax";

    //public static final String  CNTMIN = "cntmin";
    public static final String CNTMIN = "cntmin";

    //public static final String  CODPRO = "codpro";
    public static final String CODPRO = "codpro";

    //public static final String  DESPRO = "despro";
    public static final String DESPRO = "despro";

    //public static final String  ESTILO = "estilo";
    public static final String ESTILO = "estilo";

    //public static final String  FUELLE = "fuelle";
    public static final String FUELLE = "fuelle";

    //public static final String  HIJPRO = "hijpro";
    public static final String HIJPRO = "hijpro";

    //public static final BvpPrecioId  ID = "id";
    public static final String ID = "id";
    
    //public static final BvpPrecioId  ID = "id";
    public static final String CODSCC = "id.codScc";

    //public static final String  KITPRO = "kitpro";
    public static final String KITPRO = "kitpro";

    //public static final String  LARGO = "largo";
    public static final String LARGO = "largo";

    //public static final String  LARSAC = "larsac";
    public static final String LARSAC = "larsac";

    //public static final String  MONEDA = "moneda";
    public static final String MONEDA = "moneda";

    //public static final String  NUMCOL = "numcol";
    public static final String NUMCOL = "numcol";

    //public static final String  REFERE = "refere";
    public static final String REFERE = "refere";

    //public static final String  TIPPRO = "tippro";
    public static final String TIPPRO = "tippro";

    //public static final String  UNDMED = "undmed";
    public static final String UNDMED = "undmed";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved BvpPrecio entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpPrecioDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpPrecio entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpPrecio entity) {
        EntityManagerHelper.log("saving BvpPrecio instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent BvpPrecio entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpPrecioDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpPrecio entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpPrecio entity) {
        EntityManagerHelper.log("deleting BvpPrecio instance", Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(BvpPrecio.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved BvpPrecio entity and return it or a copy of it
     * to the sender. A copy of the BvpPrecio entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = BvpPrecioDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpPrecio entity to update
     * @return BvpPrecio the persisted BvpPrecio entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpPrecio update(BvpPrecio entity) {
        EntityManagerHelper.log("updating BvpPrecio instance", Level.INFO, null);

        try {
            BvpPrecio result = getEntityManager().merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public BvpPrecio findById(BvpPrecioId id) {
        EntityManagerHelper.log("finding BvpPrecio instance with id: " + id,
            Level.INFO, null);

        try {
            BvpPrecio instance = getEntityManager().find(BvpPrecio.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  BvpPrecio entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  BvpPrecio property to query
     * @param value
     *            the property value to match
     * @return List< BvpPrecio> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpPrecio> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  BvpPrecio instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  BvpPrecio model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all BvpPrecio entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpPrecio property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<BvpPrecio> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpPrecio> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding BvpPrecio instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from BvpPrecio model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpPrecio> findByAlto(Object alto, int... rowStartIdxAndCount) {
        return findByProperty(ALTO, alto, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByAlto(Object alto) {
        return findByProperty(ALTO, alto);
    }

    public List<BvpPrecio> findByAncho(Object ancho, int... rowStartIdxAndCount) {
        return findByProperty(ANCHO, ancho, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByAncho(Object ancho) {
        return findByProperty(ANCHO, ancho);
    }

    public List<BvpPrecio> findByAncsac(Object ancsac,
        int... rowStartIdxAndCount) {
        return findByProperty(ANCSAC, ancsac, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByAncsac(Object ancsac) {
        return findByProperty(ANCSAC, ancsac);
    }

    public List<BvpPrecio> findByClave(Object clave, int... rowStartIdxAndCount) {
        return findByProperty(CLAVE, clave, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByClave(Object clave) {
        return findByProperty(CLAVE, clave);
    }

    public List<BvpPrecio> findByCntmax(Object cntmax,
        int... rowStartIdxAndCount) {
        return findByProperty(CNTMAX, cntmax, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByCntmax(Object cntmax) {
        return findByProperty(CNTMAX, cntmax);
    }

    public List<BvpPrecio> findByCntmin(Object cntmin,
        int... rowStartIdxAndCount) {
        return findByProperty(CNTMIN, cntmin, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByCntmin(Object cntmin) {
        return findByProperty(CNTMIN, cntmin);
    }

    public List<BvpPrecio> findByCodpro(Object codpro,
        int... rowStartIdxAndCount) {
        return findByProperty(CODPRO, codpro, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByCodpro(Object codpro) {
        return findByProperty(CODPRO, codpro);
    }

    public List<BvpPrecio> findByDespro(Object despro,
        int... rowStartIdxAndCount) {
        return findByProperty(DESPRO, despro, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByDespro(Object despro) {
        return findByProperty(DESPRO, despro);
    }

    public List<BvpPrecio> findByEstilo(Object estilo,
        int... rowStartIdxAndCount) {
        return findByProperty(ESTILO, estilo, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByEstilo(Object estilo) {
        return findByProperty(ESTILO, estilo);
    }

    public List<BvpPrecio> findByFuelle(Object fuelle,
        int... rowStartIdxAndCount) {
        return findByProperty(FUELLE, fuelle, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByFuelle(Object fuelle) {
        return findByProperty(FUELLE, fuelle);
    }

    public List<BvpPrecio> findByHijpro(Object hijpro,
        int... rowStartIdxAndCount) {
        return findByProperty(HIJPRO, hijpro, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByHijpro(Object hijpro) {
        return findByProperty(HIJPRO, hijpro);
    }

    public List<BvpPrecio> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findById(Object id) {
        return findByProperty(ID, id);
    }

    public List<BvpPrecio> findByKitpro(Object kitpro,
        int... rowStartIdxAndCount) {
        return findByProperty(KITPRO, kitpro, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByKitpro(Object kitpro) {
        return findByProperty(KITPRO, kitpro);
    }

    public List<BvpPrecio> findByLargo(Object largo, int... rowStartIdxAndCount) {
        return findByProperty(LARGO, largo, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByLargo(Object largo) {
        return findByProperty(LARGO, largo);
    }

    public List<BvpPrecio> findByLarsac(Object larsac,
        int... rowStartIdxAndCount) {
        return findByProperty(LARSAC, larsac, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByLarsac(Object larsac) {
        return findByProperty(LARSAC, larsac);
    }

    public List<BvpPrecio> findByMoneda(Object moneda,
        int... rowStartIdxAndCount) {
        return findByProperty(MONEDA, moneda, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByMoneda(Object moneda) {
        return findByProperty(MONEDA, moneda);
    }

    public List<BvpPrecio> findByNumcol(Object numcol,
        int... rowStartIdxAndCount) {
        return findByProperty(NUMCOL, numcol, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByNumcol(Object numcol) {
        return findByProperty(NUMCOL, numcol);
    }

    public List<BvpPrecio> findByRefere(Object refere,
        int... rowStartIdxAndCount) {
        return findByProperty(REFERE, refere, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByRefere(Object refere) {
        return findByProperty(REFERE, refere);
    }

    public List<BvpPrecio> findByTippro(Object tippro,
        int... rowStartIdxAndCount) {
        return findByProperty(TIPPRO, tippro, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByTippro(Object tippro) {
        return findByProperty(TIPPRO, tippro);
    }

    public List<BvpPrecio> findByUndmed(Object undmed,
        int... rowStartIdxAndCount) {
        return findByProperty(UNDMED, undmed, rowStartIdxAndCount);
    }

    public List<BvpPrecio> findByUndmed(Object undmed) {
        return findByProperty(UNDMED, undmed);
    }

    /**
     * Find all BvpPrecio entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpPrecio> all BvpPrecio entities
     */
    @SuppressWarnings("unchecked")
    public List<BvpPrecio> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all BvpPrecio instances", Level.INFO,
            null);

        try {
            final String queryString = "select model from BvpPrecio model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpPrecio> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from BvpPrecio model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<BvpPrecio> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in BvpPrecio failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpPrecio> findPageBvpPrecio(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from BvpPrecio model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from BvpPrecio model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberBvpPrecio() {
        try {
            String queryString = "select count(*) from BvpPrecio model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo encargdo de consultar los productos que no tienen archivo pdf asociado
    public List<ProductoVO> consultarPdfsFaltantes(TsccvPlanta tsccvPlanta){
    	try{	    	
    		StringBuffer query = new StringBuffer("SELECT DISTINCT EMPR.COD_SAP, EMPR.NOMBRE, PROD.COD_SCC, PROD.CODPRO, PROD.DESPRO "+ 
									    		  "FROM TSCCV_EMPRESAS EMPR, BVP_PRECIO PROD "+ 
									    		  "WHERE (EMPR.COD_VPE = PROD.COD_CLI OR EMPR.COD_VPE = PROD.CODPRO) AND " +
									    		  "LTRIM(RTRIM(PROD.CODPLA)) = '"+tsccvPlanta.getCodpla().trim()+"' AND " +
									    		  "LTRIM(RTRIM(PROD.TIPPRO)) = '"+Utilities.TIPOS_PRODUCTO[Integer.parseInt(tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString())-1]+"' AND "+
									    		  "RTRIM(PROD.COD_SCC) NOT LIKE '%0' AND "+
									    		  "PROD.COD_SCC NOT IN (SELECT PDF_PROD.COD_SCC FROM TSCCV_PDFS_PRODUCTOS PDF_PROD WHERE LTRIM(RTRIM(PDF_PROD.CODPLA)) = '"+tsccvPlanta.getCodpla().trim()+"') "+
									    		  "GROUP BY EMPR.COD_SAP, EMPR.NOMBRE, PROD.COD_SCC, PROD.CODPRO, PROD.DESPRO "+ 
									    		  "ORDER BY EMPR.NOMBRE ");
	    	
			List registros=null;
			List<ProductoVO> resultado= null;
			
			//Ejecutamos el query
			System.out.println(query.toString());
			registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos null
				return new ArrayList<ProductoVO>();
			}
			
			resultado = new ArrayList<ProductoVO>();
			Long consecutivo = new Long(1);
			
			for(int i=0; i<registros.size(); i++){
				Object[] reg= (Object[])registros.get(i);
				
				ProductoVO producto = new ProductoVO();
				BvpPrecio bvpPrecio = new BvpPrecio();
				
				BvpPrecioId bvpPrecioId = new BvpPrecioId();
				bvpPrecioId.setCodScc((String)reg[2]);
				
				bvpPrecio.setId(bvpPrecioId);
				bvpPrecio.setCodpro((String)reg[3]);
				bvpPrecio.setDespro((String)reg[4]);
				producto.setBvpPrecio(bvpPrecio);
				producto.setNombreEmpresa((reg[0] != null && !reg[0].equals("") ? (String)reg[0]+"-" : "")+(String)reg[1]);
				producto.setCodSap((String)reg[0]);
				producto.setConsecutivo(consecutivo++);
				
				resultado.add(producto);
			}
			
			return resultado;
		}catch(Exception e){
			return null;
		}
    }
    
    //metodo encargado de consultar los productos por codigoProd o descripcion y codigoCliente 
    public List<ProductoVO> consultarProductosBvp(String codigoProd, String descripcion, String coddigoVpe, TsccvPlanta tsccvPlanta, String nombreColumna, Boolean ascendente){
    	try{//consulta con base a en la jsp actual SCCSearchProductoVPE.jsp
	    	
    		StringBuffer query = new StringBuffer("SELECT DISTINCT COD_CLI, CODPLA, COD_SCC, CODPRO, PRECIO, DESPRO, UNDMED, MONEDA " +
    		//Se elimina del select el precio ya que lo traeria repetido por cada uno que se repite el 
    		//StringBuffer query = new StringBuffer("SELECT DISTINCT COD_CLI, CODPLA, COD_SCC, CODPRO, DESPRO, UNDMED, MONEDA " +
    											  "FROM BVP_PRECIO " +
    											  "WHERE LTRIM(RTRIM(COD_CLI)) = '"+coddigoVpe.trim()+"' AND LTRIM(RTRIM(CODPLA)) = '"+tsccvPlanta.getCodpla().trim()+"' AND " +
    											  		"LTRIM(RTRIM(TIPPRO)) = '"+Utilities.TIPOS_PRODUCTO[Integer.parseInt(tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString())-1]+"' " +
    											  		"AND (HIJPRO IS NULL OR HIJPRO <> 'N') ");
    		
    		if(codigoProd != null){ 
    			query.append("AND (LTRIM(RTRIM(CODPRO)) LIKE '%"+codigoProd+"%' OR LTRIM(RTRIM(COD_SCC)) LIKE '%"+codigoProd.trim()+"%') ");
    		}
    		else if(descripcion != null){
    			query.append("AND LTRIM(RTRIM(DESPRO)) LIKE '%"+descripcion.trim()+"%' ");
    		}
    		
    		//agregado para la funcion de ordenar por el campo
    		query.append("ORDER BY "+nombreColumna+",COD_SCC "+(ascendente ? "ASC" : "DESC"));
    		
			List registros=null;
			List<ProductoVO> resultado= null;
			
			//Ejecutamos el query
			System.out.println(query.toString());
			registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos null
				return new ArrayList<ProductoVO>();
			}
			
			resultado = new ArrayList<ProductoVO>();
			DecimalFormat df = new DecimalFormat("###,###,##0.00");
			String codSccActual=null;
			
			for(int i=0; i<registros.size(); i++){
				Object[] reg= (Object[])registros.get(i);
				ProductoVO producto = new ProductoVO();
				producto.setSeleccionado(false);
				BvpPrecio bvpPrecio = new BvpPrecio();
				
				BvpPrecioId bvpPrecioId = new BvpPrecioId();
				bvpPrecioId.setCodCli((String)reg[3]);
				bvpPrecioId.setCodpla((String)reg[1]);
				bvpPrecioId.setCodScc((String)reg[2]);
				if(i != 0 && bvpPrecioId.getCodScc().equals(codSccActual)){
					continue;
				}
				codSccActual= bvpPrecioId.getCodScc();
				bvpPrecioId.setPrecio(df.format(new Double((String)reg[4])));
				bvpPrecio.setId(bvpPrecioId);			
				
				bvpPrecio.setCodpro((String)reg[3]);
				bvpPrecio.setDespro((String)reg[5]);
				bvpPrecio.setUndmed((String)reg[6]);
				bvpPrecio.setMoneda((String)reg[7]);
				producto.setBvpPrecio(bvpPrecio);
				
				resultado.add(producto);
			}
			
			return resultado;
		}catch(Exception e){
			return null;
		}
    }
    
    
    //metodo encargado de consultar los precios para armar la lista de precios (utilizado en la pantalla lista precios) 
    public List<ProductoVO> consultarPreciosBvp(String codigoProd, String descripcion, String coddigoVpe, TsccvPlanta tsccvPlanta, String nombreColumna, Boolean ascendente){
    	try{//consulta con base en la jsp actual SCCListaPreciosVPE.jsp
	    	StringBuffer query = new StringBuffer();
    		
        	query.append("SELECT COD_CLI, COD_SCC, CODPRO, DESPRO, REFERE, NUMCOL, LARGO, ANCHO, ALTO, CLAVE, ESTILO, CNTMIN, CNTMAX, PRECIO, MONEDA, LARSAC, ANCSAC, FUELLE " +
    					 "FROM BVP_PRECIO ");
    		
    		query.append("WHERE LTRIM(RTRIM(COD_CLI)) = '"+coddigoVpe.trim()+ "' AND " +
    					 "LTRIM(RTRIM(CODPLA)) = '"+tsccvPlanta.getCodpla().trim()+ "' AND " +
    					 "LTRIM(RTRIM(TIPPRO)) = '"+Utilities.TIPOS_PRODUCTO[Integer.parseInt(tsccvPlanta.getTsccvTipoProducto().getIdTipr().toString())-1]+"' ");
    		
    		if(codigoProd != null){
    			query.append("AND (LTRIM(RTRIM(CODPRO)) LIKE '%"+codigoProd.trim()+"%' OR LTRIM(RTRIM(COD_SCC)) LIKE '%"+codigoProd+"%') "); 
    		}
    		else if(descripcion != null){
    			query.append("AND LTRIM(RTRIM(DESPRO)) LIKE '%"+descripcion.trim()+"%' ");
    		}
    		
    		//agregado para la funcion de ordenar por el campo
    		query.append("ORDER BY "+nombreColumna+" "+(ascendente ? "ASC" : "DESC"));
    		
			List registros=null;
			List<ProductoVO> resultado= null;
			
			//Ejecutamos el query
			System.out.println(query.toString());
			registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos un arry vacio
				return new ArrayList<ProductoVO>();
			}
			
			resultado = new ArrayList<ProductoVO>();
			DecimalFormat df = new DecimalFormat("###,###,##0.00");
			
			for(int i=0; i<registros.size(); i++){
				Object[] reg= (Object[])registros.get(i);
				ProductoVO producto = new ProductoVO();
				producto.setSeleccionado(false);
				
				BvpPrecio bvpPrecio = new BvpPrecio();				
				BvpPrecioId bvpPrecioId = new BvpPrecioId();
				bvpPrecioId.setCodCli((String)reg[2]);
				bvpPrecioId.setCodScc((String)reg[1]);
				
				bvpPrecio.setCodpro((String)reg[2]);
				bvpPrecio.setDespro((String)reg[3]);
				
				bvpPrecio.setRefere((String)reg[4]);
				bvpPrecio.setNumcol((String)reg[5]);
				bvpPrecio.setLargo((String)reg[6]);
				bvpPrecio.setAncho((String)reg[7]);
				bvpPrecio.setAlto((String)reg[8]);
				bvpPrecio.setClave((String)reg[9]);
				bvpPrecio.setEstilo((String)reg[10]);
				bvpPrecio.setCntmin((String)reg[11]);
				bvpPrecio.setCntmax((String)reg[12]);
				bvpPrecioId.setPrecio(df.format(new Double((String)reg[13])));
				bvpPrecio.setMoneda((String)reg[14]);

				bvpPrecio.setLarsac((String)reg[15]);
				bvpPrecio.setAncsac((String)reg[16]);
				bvpPrecio.setFuelle((String)reg[17]);
				
				bvpPrecio.setId(bvpPrecioId);
				producto.setBvpPrecio(bvpPrecio);
				resultado.add(producto);
			}
			
			return resultado;
		}catch(Exception e){
			return null;
		}
    }
        
    //metodo encargado de verificar si un producto esta registrado
    public Boolean precioIsRegistrado(String codScc, String codPla){
		try{//consulta con base en la jsp actual SCCEnviarProductoSPVE.jspx
			
			List resultado = null;
			String query = "SELECT COD_SCC FROM BVP_PRECIO WHERE LTRIM(RTRIM(COD_SCC)) = '"+codScc.trim()+"' AND LTRIM(RTRIM(CODPLA)) = '"+codPla.trim()+"' ";
			System.out.println(query);
			resultado = EntityManagerHelper.getEntityManager().createNativeQuery(query).getResultList();
			
			return (resultado != null && resultado.size() != 0) ? true : false;
	    }catch(Exception e){
			return null;
		}
	}
    
    //metodo encargado de consultar la cantidad minima y maxima de cada precio bvp para armar las escalas
    public Map consultarEscalasBvp(List<ProductosPedidoVO> listadoProductosPedido, String codVpe){
    	List registros = null;
    	Map listadoEscalasProductos = null;//La lista donde se guardaran las escalas
    	
    	try{//consulta con base en la jsp actual SCCEnviarProductoSPVE.jspx
			StringBuffer query = new StringBuffer("SELECT COD_SCC, CNTMIN, CNTMAX, PRECIO FROM BVP_PRECIO " +
												  "WHERE LTRIM(RTRIM(COD_CLI)) = '"+codVpe.trim()+"' AND LTRIM(RTRIM(COD_SCC)) IN (");			
			
			int conteo = 0;
			//se recorre el listado de productos seleccionados para terminar de armar el query
			for(ProductosPedidoVO proPedVO : listadoProductosPedido){				
				query.append("'"+proPedVO.getTsccvProductosPedido().getCodScc().trim()+"'");
				conteo++;
				
				if(conteo < listadoProductosPedido.size()){
					query.append(" ,");
				}
			}			
			query.append(") ORDER BY COD_SCC, CNTMIN");
			
    		//se consulta la cantidad minima y maxima de cada precio
			System.out.println(query);
			registros = EntityManagerHelper.getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos un arry vacio
				return new HashMap();
			}
			
			listadoEscalasProductos = new HashMap();
			for(int i=0; i<registros.size(); i++){
				Object[] reg= (Object[])registros.get(i);
				//Creamos un Vector en que se guarda cantidad minima y maxima por cada escala de precio
				Vector<String> escala = new Vector<String>(3);
				
				escala.add((String)reg[1]);//cantidad minima
				escala.add((String)reg[2]);//cantidad maxima
				escala.add((String)reg[3]);//precio
				
				//Validamos que ya no exista una escala para el mismo producto
				if(listadoEscalasProductos.get((String)reg[0]) != null){
					//Creamos un nuevo vector con dos posiciones mas
					Vector newEscala= new Vector<String>(((Vector)listadoEscalasProductos.get((String)reg[0])).size() + 3);
					newEscala.addAll((Vector)listadoEscalasProductos.get((String)reg[0]));
					newEscala.add((String)reg[1]);//cantidad minima
					newEscala.add((String)reg[2]);//cantidad maxima
					newEscala.add((String)reg[3]);//precio
					listadoEscalasProductos.put((String)reg[0], newEscala);
					continue;
				}
				
				//Adicionamos el nuevo vector al hashMap que contendra todas las escalas
				listadoEscalasProductos.put((String)reg[0], escala);
			}
			
			return listadoEscalasProductos;
			
	    }catch(Exception e){
			return null;
		}
    }
    
    //metodo encargado de consultar el precio de cada producto pedido con base en la cantidad total de este
    public String consultarPrecioPorCantidadTotal(String codscc, String codVpe, String codPla, Integer cantidadTotal){
    	List registros = null;
    	try{
    		String query = "SELECT PRECIO FROM BVP_PRECIO WHERE LTRIM(RTRIM(COD_CLI)) = '"+codVpe.trim()+"' AND LTRIM(RTRIM(COD_SCC)) = '"+codscc.trim()+"' " +
    				"AND CAST(LTRIM(RTRIM(CNTMIN)) AS FLOAT) <= "+cantidadTotal+" AND CAST(LTRIM(RTRIM(CNTMAX)) AS FLOAT) >= "+cantidadTotal+" ";

			//se consultan los precios
    		System.out.println(query);
			registros = EntityManagerHelper.getEntityManager().createNativeQuery(query).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos null
				return null;
			}
						
	    	return registros.get(0).toString();
	    }catch(Exception e){
			return null;
		}
    }
}
