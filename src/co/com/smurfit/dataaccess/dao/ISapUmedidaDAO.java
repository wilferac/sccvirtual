package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.SapUmedida;
import co.com.smurfit.sccvirtual.entidades.SapUmedidaId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for SapUmedidaDAO.
 *
*/
public interface ISapUmedidaDAO {
    /**
     * Perform an initial save of a previously unsaved SapUmedida entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapUmedidaDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapUmedida entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapUmedida entity);

    /**
     * Delete a persistent SapUmedida entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapUmedidaDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapUmedida entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapUmedida entity);

    /**
     * Persist a previously saved SapUmedida entity and return it or a copy of it
     * to the sender. A copy of the SapUmedida entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ISapUmedidaDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapUmedida entity to update
     * @return SapUmedida the persisted SapUmedida entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapUmedida update(SapUmedida entity);

    public SapUmedida findById(SapUmedidaId id);

    /**
     * Find all SapUmedida entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapUmedida property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapUmedida> found by query
     */
    public List<SapUmedida> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<SapUmedida> findByCriteria(String whereCondition);

    public List<SapUmedida> findById(Object id);

    public List<SapUmedida> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all SapUmedida entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapUmedida> all SapUmedida entities
     */
    public List<SapUmedida> findAll(int... rowStartIdxAndCount);

    public List<SapUmedida> findPageSapUmedida(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberSapUmedida();
}
