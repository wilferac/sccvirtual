package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvEntregasPedido entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvEntregasPedido
 */
public class TsccvEntregasPedidoDAO implements ITsccvEntregasPedidoDAO {
    // property constants

    //public static final Integer  CANTIDAD = "cantidad";
    public static final String CANTIDAD = "cantidad";

    //public static final String  DESDIR = "desdir";
    public static final String DESDIR = "desdir";

    //public static final Date  FECHAENTREGA = "fechaEntrega";
    public static final String FECHAENTREGA = "fechaEntrega";

    //public static final Integer  IDENPE = "idEnpe";
    public static final String IDENPE = "idEnpe";
    
    //public static final Integer  PRODPED = "tsccvProductosPedido.idPrpe";
    public static final String PRODPED = "tsccvProductosPedido.idPrpe"; 

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TsccvEntregasPedido entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvEntregasPedidoDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEntregasPedido entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvEntregasPedido entity) {
        EntityManagerHelper.log("saving TsccvEntregasPedido instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    
    public static void saveEntregasPedido(TsccvEntregasPedido entity) {
        EntityManagerHelper.log("saving TsccvEntregasPedido instance", Level.INFO, null);
        Integer idEnpe = null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	idEnpe = (Integer) EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_Enpe) from Tsccv_Entregas_Pedido").getSingleResult();
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro
        	idEnpe = idEnpe != null ? (idEnpe + 1) : 1;
        	entity.setIdEnpe(idEnpe);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }
    
    /**
     * Delete a persistent TsccvEntregasPedido entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvEntregasPedidoDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvEntregasPedido entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvEntregasPedido entity) {
        EntityManagerHelper.log("deleting TsccvEntregasPedido instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvEntregasPedido.class,
                    entity.getIdEnpe());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvEntregasPedido entity and return it or a copy of it
     * to the sender. A copy of the TsccvEntregasPedido entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvEntregasPedidoDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEntregasPedido entity to update
     * @return TsccvEntregasPedido the persisted TsccvEntregasPedido entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvEntregasPedido update(TsccvEntregasPedido entity) {
        EntityManagerHelper.log("updating TsccvEntregasPedido instance",
            Level.INFO, null);

        try {
        	((Session)getEntityManager().getDelegate()).update(entity);

        	//TsccvEntregasPedido result = getEntityManager()
            //                                 .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return entity;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvEntregasPedido findById(Integer id) {
        EntityManagerHelper.log(
            "finding TsccvEntregasPedido instance with id: " + id, Level.INFO,
            null);

        try {
            TsccvEntregasPedido instance = getEntityManager()
                                               .find(TsccvEntregasPedido.class,
                    id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvEntregasPedido entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvEntregasPedido property to query
     * @param value
     *            the property value to match
     * @return List< TsccvEntregasPedido> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvEntregasPedido> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TsccvEntregasPedido instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvEntregasPedido model where model." +
                propertyName + "= :propertyValue order by model.idEnpe asc";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvEntregasPedido entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvEntregasPedido property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvEntregasPedido> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvEntregasPedido> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TsccvEntregasPedido instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvEntregasPedido model where model." +
                propertyName + "= :propertyValue order by model.idEnpe asc";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvEntregasPedido> findByCantidad(Object cantidad,
        int... rowStartIdxAndCount) {
        return findByProperty(CANTIDAD, cantidad, rowStartIdxAndCount);
    }

    public List<TsccvEntregasPedido> findByCantidad(Object cantidad) {
        return findByProperty(CANTIDAD, cantidad);
    }

    public List<TsccvEntregasPedido> findByDesdir(Object desdir,
        int... rowStartIdxAndCount) {
        return findByProperty(DESDIR, desdir, rowStartIdxAndCount);
    }

    public List<TsccvEntregasPedido> findByDesdir(Object desdir) {
        return findByProperty(DESDIR, desdir);
    }

    public List<TsccvEntregasPedido> findByFechaEntrega(Object fechaEntrega,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHAENTREGA, fechaEntrega, rowStartIdxAndCount);
    }

    public List<TsccvEntregasPedido> findByFechaEntrega(Object fechaEntrega) {
        return findByProperty(FECHAENTREGA, fechaEntrega);
    }

    public List<TsccvEntregasPedido> findByIdEnpe(Object idEnpe,
        int... rowStartIdxAndCount) {
        return findByProperty(IDENPE, idEnpe, rowStartIdxAndCount);
    }

    public List<TsccvEntregasPedido> findByIdEnpe(Object idEnpe) {
        return findByProperty(IDENPE, idEnpe);
    }

    /**
     * Find all TsccvEntregasPedido entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvEntregasPedido> all TsccvEntregasPedido entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvEntregasPedido> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvEntregasPedido instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvEntregasPedido model order by model.idEnpe asc";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvEntregasPedido> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvEntregasPedido model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvEntregasPedido> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvEntregasPedido failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvEntregasPedido> findPageTsccvEntregasPedido(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvEntregasPedido model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvEntregasPedido model order by model.idEnpe asc";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvEntregasPedido() {
        try {
            String queryString = "select count(*) from TsccvEntregasPedido model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
}
