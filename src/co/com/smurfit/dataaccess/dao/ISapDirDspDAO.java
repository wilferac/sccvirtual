package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.SapDirDsp;
import co.com.smurfit.sccvirtual.entidades.SapDirDspId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for SapDirDspDAO.
 *
*/
public interface ISapDirDspDAO {
    /**
     * Perform an initial save of a previously unsaved SapDirDsp entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapDirDspDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDirDsp entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapDirDsp entity);

    /**
     * Delete a persistent SapDirDsp entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapDirDspDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapDirDsp entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapDirDsp entity);

    /**
     * Persist a previously saved SapDirDsp entity and return it or a copy of it
     * to the sender. A copy of the SapDirDsp entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ISapDirDspDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDirDsp entity to update
     * @return SapDirDsp the persisted SapDirDsp entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapDirDsp update(SapDirDsp entity);

    public SapDirDsp findById(SapDirDspId id);

    /**
     * Find all SapDirDsp entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapDirDsp property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapDirDsp> found by query
     */
    public List<SapDirDsp> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<SapDirDsp> findByCriteria(String whereCondition);

    public List<SapDirDsp> findById(Object id);

    public List<SapDirDsp> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all SapDirDsp entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapDirDsp> all SapDirDsp entities
     */
    public List<SapDirDsp> findAll(int... rowStartIdxAndCount);

    public List<SapDirDsp> findPageSapDirDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberSapDirDsp();
    
    public List<SapDirDsp> consultarDireccionesCliente(String codCli);
}
