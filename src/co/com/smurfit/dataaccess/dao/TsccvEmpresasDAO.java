package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvGrupoEmpresaLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvEmpresas entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvEmpresas
 */
public class TsccvEmpresasDAO implements ITsccvEmpresasDAO {
    // property constants

    //public static final String  ACTIVO = "activo";
    public static final String ACTIVO = "activo";

    //public static final String  CODMAS = "codMas";
    public static final String CODMAS = "codMas";

    //public static final String  CODSAP = "codSap";
    public static final String CODSAP = "codSap";

    //public static final String  CODVPE = "codVpe";
    public static final String CODVPE = "codVpe";

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Integer  IDEMPR = "idEmpr";
    public static final String IDEMPR = "idEmpr";

    //public static final String  NOMBRE = "nombre";
    public static final String NOMBRE = "nombre";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TsccvEmpresas entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvEmpresasDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEmpresas entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    
    public void save(TsccvEmpresas entity) {
    	/*EntityManagerHelper.log("saving TsccvEmpresas instance", Level.INFO,
            null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
        */
    }
    
    
    public static synchronized void saveEmpresa(TsccvEmpresas entity) {
        EntityManagerHelper.log("saving TsccvEmpresas instance", Level.INFO, null);
        Integer idEmpr = null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	idEmpr =(Integer) EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_Empr) from tsccv_Empresas").getSingleResult();
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro
        	
        	idEmpr = idEmpr != null ? (idEmpr + 1) : 1;
        	entity.setIdEmpr(idEmpr);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent TsccvEmpresas entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvEmpresasDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvEmpresas entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvEmpresas entity) {
        EntityManagerHelper.log("deleting TsccvEmpresas instance", Level.INFO,
            null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvEmpresas.class, entity.getIdEmpr());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvEmpresas entity and return it or a copy of it
     * to the sender. A copy of the TsccvEmpresas entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvEmpresasDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEmpresas entity to update
     * @return TsccvEmpresas the persisted TsccvEmpresas entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvEmpresas update(TsccvEmpresas entity) {
        EntityManagerHelper.log("updating TsccvEmpresas instance", Level.INFO, null);

        try {
        	((Session)getEntityManager().getDelegate()).update(entity);
        	/*TsccvEmpresas result = getEntityManager()
                                       .merge(entity);*/
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return entity;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvEmpresas findById(Integer id) {
        EntityManagerHelper.log("finding TsccvEmpresas instance with id: " +
            id, Level.INFO, null);

        try {
            TsccvEmpresas instance = getEntityManager()
                                         .find(TsccvEmpresas.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvEmpresas entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvEmpresas property to query
     * @param value
     *            the property value to match
     * @return List< TsccvEmpresas> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvEmpresas> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TsccvEmpresas instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvEmpresas model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvEmpresas entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvEmpresas property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvEmpresas> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvEmpresas> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TsccvEmpresas instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvEmpresas model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvEmpresas> findByActivo(Object activo,
        int... rowStartIdxAndCount) {
        return findByProperty(ACTIVO, activo, rowStartIdxAndCount);
    }

    public List<TsccvEmpresas> findByActivo(Object activo) {
        return findByProperty(ACTIVO, activo);
    }

    public List<TsccvEmpresas> findByCodMas(Object codMas,
        int... rowStartIdxAndCount) {
        return findByProperty(CODMAS, codMas, rowStartIdxAndCount);
    }

    public List<TsccvEmpresas> findByCodMas(Object codMas) {
        return findByProperty(CODMAS, codMas);
    }

    public List<TsccvEmpresas> findByCodSap(Object codSap,
        int... rowStartIdxAndCount) {
        return findByProperty(CODSAP, codSap, rowStartIdxAndCount);
    }

    public List<TsccvEmpresas> findByCodSap(Object codSap) {
        return findByProperty(CODSAP, codSap);
    }

    public List<TsccvEmpresas> findByCodVpe(Object codVpe,
        int... rowStartIdxAndCount) {
        return findByProperty(CODVPE, codVpe, rowStartIdxAndCount);
    }

    public List<TsccvEmpresas> findByCodVpe(Object codVpe) {
        return findByProperty(CODVPE, codVpe);
    }

    public List<TsccvEmpresas> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TsccvEmpresas> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TsccvEmpresas> findByIdEmpr(Object idEmpr,
        int... rowStartIdxAndCount) {
        return findByProperty(IDEMPR, idEmpr, rowStartIdxAndCount);
    }

    public List<TsccvEmpresas> findByIdEmpr(Object idEmpr) {
        return findByProperty(IDEMPR, idEmpr);
    }

    public List<TsccvEmpresas> findByNombre(Object nombre,
        int... rowStartIdxAndCount) {
        return findByProperty(NOMBRE, nombre, rowStartIdxAndCount);
    }

    public List<TsccvEmpresas> findByNombre(Object nombre) {
        return findByProperty(NOMBRE, nombre);
    }

    /**
     * Find all TsccvEmpresas entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvEmpresas> all TsccvEmpresas entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvEmpresas> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvEmpresas instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvEmpresas model order by model.nombre asc";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvEmpresas> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvEmpresas model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvEmpresas> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvEmpresas failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvEmpresas> findPageTsccvEmpresas(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvEmpresas model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvEmpresas model order by model asc";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvEmpresas() {
        try {
            String queryString = "select count(*) from TsccvEmpresas model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    
    //consulta las empresas con los datos para saber si esta registrada 
    public Boolean empresaIsRegistrada(String nombre, String codSap, String codMas, String codVpe){
    	List resultado = null;
    	try{
	    	StringBuffer query = new StringBuffer("SELECT ID_EMPR FROM TSCCV_EMPRESAS WHERE NOMBRE = '"+nombre+"' ");
	    	
	    	if((codMas != null && !codMas.equals(""))){
	    		query.append(" OR (COD_SAP = '"+codSap+"' AND COD_MAS = '"+codMas+"') ");
	    	}
	    	else if(codMas != null && !codMas.equals("")){
	    		query.append(" OR (COD_SAP = '"+codSap+"' AND COD_VPE = '"+codVpe+"') ");
	    	}    	
    	
    		resultado = EntityManagerHelper.getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	return (resultado != null && resultado.size() != 0) ? true : false;
    	}catch(Exception e){
    		return null;
    	}
    }
    
    
    //Metodo utilizado para la consulta por todos los campos
    public List<TsccvEmpresas> consularEmpresas(TsccvEmpresas entity){
     	StringBuffer query=new StringBuffer("SELECT ID_EMPR, ID_GRUE, NOMBRE, COD_SAP, COD_VPE, COD_MAS, ACTIVO, FECHA_CREACION FROM TSCCV_EMPRESAS ");
    	List registros=null;
    	List<TsccvEmpresas> resultado= null;
    	try {
	    	//Validamos si los campos vienen con valor o no para armar el query
	    	if(entity.getIdEmpr() != null){
	    		query.append("WHERE ID_EMPR LIKE '%"+entity.getIdEmpr()+"%' ");
	    	}
	    	else{
	    		query.append("WHERE ID_EMPR LIKE '%' ");
	    	}
	    	
	    	if(entity.getTsccvGrupoEmpresa() != null){
	    		query.append("AND ID_GRUE LIKE '%"+entity.getTsccvGrupoEmpresa().getIdGrue()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ID_GRUE LIKE '%' OR ID_GRUE IS NULL) ");
	    	}
	    	
	    	if(entity.getNombre() != null){
	    		query.append("AND NOMBRE LIKE '%"+entity.getNombre()+"%' ");
	    	}
	    	else{
	    		query.append("AND NOMBRE LIKE '%' ");
	    	}
	    	
	    	if(entity.getCodSap() != null){
	    		query.append("AND COD_SAP LIKE '%"+entity.getCodSap()+"%' ");
	    	}
	    	else{
	    		query.append("AND (COD_SAP LIKE '%' OR COD_SAP IS NULL)");
	    	}
	    	
	    	if(entity.getCodVpe() != null){
	    		query.append("AND COD_VPE LIKE '%"+entity.getCodVpe()+"%' ");
	    	}
	    	else{
	    		query.append("AND (COD_VPE LIKE '%' OR COD_VPE IS NULL) ");
	    	}
	    	
	    	if(entity.getCodMas() != null){
	    		query.append("AND COD_MAS LIKE '%"+entity.getCodMas()+"%' ");
	    	}
	    	else{
	    		query.append("AND (COD_MAS LIKE '%' OR COD_MAS IS NULL) ");
	    	}
	    	
	    	if(entity.getActivo() != null){
	    		query.append("AND ACTIVO LIKE '%"+entity.getActivo()+"%' ");
	    	}
	    	else{
	    		query.append("AND ACTIVO LIKE '%' ");
	    	}	    		    	
	    	
	    	/*if(entity.getFechaCreacion() != null){
	    		query.append("AND FECHA_CREACION LIKE '%"+entity.getFechaCreacion()+"%' ");
	    	}
	    	else{
	    		query.append("AND FECHA_CREACION LIKE '%' ");
	    	}*/
	    	
	    	//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<TsccvEmpresas>();
	    	ITsccvGrupoEmpresaLogic tsccvGrupoEmpresaLogic = new TsccvGrupoEmpresaLogic(); 
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TsccvEmpresas empr = new TsccvEmpresas();
	    		empr.setIdEmpr((Integer)reg[0]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO GRUPO_EMPRESA YA QUE NO ES OBLIGATORIO
	    		if(reg[1] != null){
	    			TsccvGrupoEmpresa tsccvGrupoEmpresaClass = tsccvGrupoEmpresaLogic.getTsccvGrupoEmpresa((Integer) reg[1]);
	    			empr.setTsccvGrupoEmpresa(tsccvGrupoEmpresaClass);
	    		}
	    		
	    		empr.setNombre((String)reg[2]);
	    		empr.setCodSap((String)reg[3]);
	    		empr.setCodVpe((String)reg[4]);
	    		empr.setCodMas((String)reg[5]);
	    		empr.setActivo((String)reg[6]);
	    		empr.setFechaCreacion((Date)reg[7]);
	    		
	            resultado.add(empr);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
    
    public void inactivarEmpresa(Integer idEmpr){    	
    	EntityManagerHelper.log("inactivating TsccvEmpresas instance", Level.INFO, null);
    	String query = "update TsccvEmpresas model set activo = '1' where model.idEmpr = "+idEmpr+" ";
    	
		try{			
			getEntityManager().createQuery(query).executeUpdate();
			EntityManagerHelper.log("inactivation successful", Level.INFO, null);
		}catch(RuntimeException re){
			EntityManagerHelper.log("inactivation failed", Level.SEVERE, re);
			throw re;
		}
    }
    
    //metodo encargado de consultar las empresas (utilizado en la pantalla busquedaEmpresa)
    public List<TsccvEmpresas> buscarEmpresa(TsccvEmpresas entity, Boolean pantallaIsAdmin){
    	StringBuffer query=new StringBuffer("SELECT ID_EMPR, ID_GRUE, NOMBRE, COD_SAP, COD_VPE, COD_MAS, ACTIVO, FECHA_CREACION "+
    										"FROM TSCCV_EMPRESAS EMPR ");
    	List registros=null;
    	List<TsccvEmpresas> resultado= null;
    	
    	try {	    
    		
    		if(entity.getNombre() != null){
	    		query.append("WHERE NOMBRE LIKE '%"+entity.getNombre()+"%' ");
	    	}
	    	else{
	    		query.append("WHERE NOMBRE LIKE '%' ");
	    	}
	    	
	    	if(entity.getCodSap() != null){
	    		query.append("AND COD_SAP LIKE '%"+entity.getCodSap()+"%' ");
	    	}
	    	else{
	    		query.append("AND (COD_SAP LIKE '%' OR COD_SAP IS NULL) ");
	    	}
	    	
	    	if(entity.getCodVpe() != null){
	    		query.append("AND COD_VPE LIKE '%"+entity.getCodVpe()+"%' ");
	    	}
	    	else{
	    		query.append("AND (COD_VPE LIKE '%' OR COD_VPE IS NULL) ");
	    	}
	    	
	    	if(entity.getCodMas() != null){
	    		query.append("AND COD_MAS LIKE '%"+entity.getCodMas()+"%' ");
	    	}
	    	else{
	    		query.append("AND (COD_MAS LIKE '%' OR COD_MAS IS NULL) ");
	    	}
	    	
	    	//se valida si es una pantalla administrativa ya que para estas es necesario mostrar tambien los grupos
	    	query.append("AND ACTIVO LIKE '0' "+(pantallaIsAdmin ? "" : "AND ID_GRUE IS NULL "));
	    	
	    	//si es un grupo asegura que las empresas resultantes de la consulta pertenezcan a este 
	    	if(entity.getTsccvGrupoEmpresa() != null && !pantallaIsAdmin){
	    		query.append("AND EMPR.ID_EMPR IN (SELECT ID_EMPR " +
												   "FROM TSCCV_DETALLE_GRUPO_EMPRESA " +
												   "WHERE ID_GRUE = "+entity.getTsccvGrupoEmpresa().getIdGrue()+") ");
	    	}
    		
	    	//Ejecutamos el query
	    	registros = getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<TsccvEmpresas>();
	    	ITsccvGrupoEmpresaLogic tsccvGrupoEmpresaLogic = new TsccvGrupoEmpresaLogic(); 
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TsccvEmpresas empr = new TsccvEmpresas();
	    		empr.setIdEmpr((Integer)reg[0]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO GRUPO_EMPRESA YA QUE NO ES OBLIGATORIO
	    		if(reg[1] != null){
	    			TsccvGrupoEmpresa tsccvGrupoEmpresaClass = tsccvGrupoEmpresaLogic.getTsccvGrupoEmpresa((Integer) reg[1]);
	    			empr.setTsccvGrupoEmpresa(tsccvGrupoEmpresaClass);
	    		}
	    		
	    		empr.setNombre((String)reg[2]);
	    		empr.setCodSap((String)reg[3]);
	    		empr.setCodVpe((String)reg[4]);
	    		empr.setCodMas((String)reg[5]);
	    		empr.setActivo((String)reg[6]);
	    		empr.setFechaCreacion((Date)reg[7]);
	    		
	            resultado.add(empr);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
}
