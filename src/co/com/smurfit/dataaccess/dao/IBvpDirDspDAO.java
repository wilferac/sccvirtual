package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for BvpDirDspDAO.
 *
*/
public interface IBvpDirDspDAO {
    /**
     * Perform an initial save of a previously unsaved BvpDirDsp entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpDirDspDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDirDsp entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpDirDsp entity);

    /**
     * Delete a persistent BvpDirDsp entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpDirDspDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpDirDsp entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpDirDsp entity);

    /**
     * Persist a previously saved BvpDirDsp entity and return it or a copy of it
     * to the sender. A copy of the BvpDirDsp entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = IBvpDirDspDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDirDsp entity to update
     * @return BvpDirDsp the persisted BvpDirDsp entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpDirDsp update(BvpDirDsp entity);

    public BvpDirDsp findById(BvpDirDspId id);

    /**
     * Find all BvpDirDsp entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpDirDsp property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpDirDsp> found by query
     */
    public List<BvpDirDsp> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<BvpDirDsp> findByCriteria(String whereCondition);

    public List<BvpDirDsp> findById(Object id);

    public List<BvpDirDsp> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all BvpDirDsp entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpDirDsp> all BvpDirDsp entities
     */
    public List<BvpDirDsp> findAll(int... rowStartIdxAndCount);

    public List<BvpDirDsp> findPageBvpDirDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberBvpDirDsp();
    
    public List<BvpDirDsp> consultarDireccionesClientePlanta(String codCli, String codPla);
}
