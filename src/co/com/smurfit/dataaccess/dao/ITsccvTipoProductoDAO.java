package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvTipoProductoDAO.
 *
*/
public interface ITsccvTipoProductoDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvTipoProducto entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvTipoProductoDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvTipoProducto entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvTipoProducto entity);

    /**
     * Delete a persistent TsccvTipoProducto entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvTipoProductoDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvTipoProducto entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvTipoProducto entity);

    /**
     * Persist a previously saved TsccvTipoProducto entity and return it or a copy of it
     * to the sender. A copy of the TsccvTipoProducto entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvTipoProductoDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvTipoProducto entity to update
     * @return TsccvTipoProducto the persisted TsccvTipoProducto entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvTipoProducto update(TsccvTipoProducto entity);

    public TsccvTipoProducto findById(Long id);

    /**
     * Find all TsccvTipoProducto entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvTipoProducto property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvTipoProducto> found by query
     */
    public List<TsccvTipoProducto> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvTipoProducto> findByCriteria(String whereCondition);

    public List<TsccvTipoProducto> findByActivo(Object activo);

    public List<TsccvTipoProducto> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TsccvTipoProducto> findByDescripcion(Object descripcion);

    public List<TsccvTipoProducto> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount);

    public List<TsccvTipoProducto> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvTipoProducto> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TsccvTipoProducto> findByIdTipr(Object idTipr);

    public List<TsccvTipoProducto> findByIdTipr(Object idTipr,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvTipoProducto entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvTipoProducto> all TsccvTipoProducto entities
     */
    public List<TsccvTipoProducto> findAll(int... rowStartIdxAndCount);

    public List<TsccvTipoProducto> findPageTsccvTipoProducto(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvTipoProducto();
}
