package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvPedidosDAO.
 *
*/
public interface ITsccvPedidosDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvPedidos entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvPedidosDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPedidos entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvPedidos entity);

    /**
     * Delete a persistent TsccvPedidos entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvPedidosDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvPedidos entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvPedidos entity);

    /**
     * Persist a previously saved TsccvPedidos entity and return it or a copy of it
     * to the sender. A copy of the TsccvPedidos entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvPedidosDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPedidos entity to update
     * @return TsccvPedidos the persisted TsccvPedidos entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvPedidos update(TsccvPedidos entity);

    public TsccvPedidos findById(Integer id);

    /**
     * Find all TsccvPedidos entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvPedidos property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPedidos> found by query
     */
    public List<TsccvPedidos> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<TsccvPedidos> findByCriteria(String whereCondition);

    public List<TsccvPedidos> findByActivo(Object activo);

    public List<TsccvPedidos> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TsccvPedidos> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvPedidos> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TsccvPedidos> findByIdPedi(Object idPedi);

    public List<TsccvPedidos> findByIdPedi(Object idPedi,
        int... rowStartIdxAndCount);

    public List<TsccvPedidos> findByObservaciones(Object observaciones);

    public List<TsccvPedidos> findByObservaciones(Object observaciones,
        int... rowStartIdxAndCount);

    public List<TsccvPedidos> findByRegistrado(Object registrado);

    public List<TsccvPedidos> findByRegistrado(Object registrado,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvPedidos entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPedidos> all TsccvPedidos entities
     */
    public List<TsccvPedidos> findAll(int... rowStartIdxAndCount);

    public List<TsccvPedidos> findPageTsccvPedidos(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberTsccvPedidos();
    
    public Integer consultarUltimoIdPedido();
    
    public Integer consultarPedidoNoRegistrado(Integer idEmpr, String codPla, Integer idUsua);
}
