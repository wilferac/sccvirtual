package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TsccvGrupoEmpresa;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvGrupoEmpresa entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvGrupoEmpresa
 */
public class TsccvGrupoEmpresaDAO implements ITsccvGrupoEmpresaDAO {
    // property constants

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Integer  IDGRUE = "idGrue";
    public static final String IDGRUE = "idGrue";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TsccvGrupoEmpresa entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvGrupoEmpresaDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvGrupoEmpresa entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    
    //se comenta ya que se utilizara el nuevo metodo (saveGrupoEmpresa) que genera el consecutivo
    public void save(TsccvGrupoEmpresa entity) {        
    	/*EntityManagerHelper.log("saving TsccvGrupoEmpresa instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }*/
    }

    
    public static synchronized void saveGrupoEmpresa(TsccvGrupoEmpresa entity) {
        EntityManagerHelper.log("saving TsccvGrupoEmpresa instance", Level.INFO, null);
        Integer idGrue = null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	idGrue =(Integer) EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_Grue) from tsccv_Grupo_Empresa").getSingleResult();
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro
        	
        	idGrue = idGrue != null ? (idGrue + 1) : 1;
        	entity.setIdGrue(idGrue);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }
    /**
     * Delete a persistent TsccvGrupoEmpresa entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvGrupoEmpresaDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvGrupoEmpresa entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvGrupoEmpresa entity) {
        EntityManagerHelper.log("deleting TsccvGrupoEmpresa instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvGrupoEmpresa.class,
                    entity.getIdGrue());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvGrupoEmpresa entity and return it or a copy of it
     * to the sender. A copy of the TsccvGrupoEmpresa entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvGrupoEmpresaDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvGrupoEmpresa entity to update
     * @return TsccvGrupoEmpresa the persisted TsccvGrupoEmpresa entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvGrupoEmpresa update(TsccvGrupoEmpresa entity) {
        EntityManagerHelper.log("updating TsccvGrupoEmpresa instance",
            Level.INFO, null);

        try {
            TsccvGrupoEmpresa result = getEntityManager()
                                           .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvGrupoEmpresa findById(Integer id) {
        EntityManagerHelper.log("finding TsccvGrupoEmpresa instance with id: " +
            id, Level.INFO, null);

        try {
            TsccvGrupoEmpresa instance = getEntityManager()
                                             .find(TsccvGrupoEmpresa.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvGrupoEmpresa entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvGrupoEmpresa property to query
     * @param value
     *            the property value to match
     * @return List< TsccvGrupoEmpresa> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvGrupoEmpresa> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TsccvGrupoEmpresa instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvGrupoEmpresa model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvGrupoEmpresa entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvGrupoEmpresa property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvGrupoEmpresa> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvGrupoEmpresa> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TsccvGrupoEmpresa instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvGrupoEmpresa model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvGrupoEmpresa> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TsccvGrupoEmpresa> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TsccvGrupoEmpresa> findByIdGrue(Object idGrue,
        int... rowStartIdxAndCount) {
        return findByProperty(IDGRUE, idGrue, rowStartIdxAndCount);
    }

    public List<TsccvGrupoEmpresa> findByIdGrue(Object idGrue) {
        return findByProperty(IDGRUE, idGrue);
    }

    /**
     * Find all TsccvGrupoEmpresa entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvGrupoEmpresa> all TsccvGrupoEmpresa entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvGrupoEmpresa> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvGrupoEmpresa instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvGrupoEmpresa model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvGrupoEmpresa> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvGrupoEmpresa model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvGrupoEmpresa> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvGrupoEmpresa failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvGrupoEmpresa> findPageTsccvGrupoEmpresa(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvGrupoEmpresa model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvGrupoEmpresa model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvGrupoEmpresa() {
        try {
            String queryString = "select count(*) from TsccvGrupoEmpresa model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    public Integer consultarUltimoIdGrue(){
    	try {
            String queryString = "select max(idGrue) from TsccvGrupoEmpresa model";

            return (Integer) getEntityManager().createQuery(queryString).getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
}
