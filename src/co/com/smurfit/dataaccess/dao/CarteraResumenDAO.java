package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;

import java.math.BigDecimal;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * CarteraResumen entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.CarteraResumen
 */
public class CarteraResumenDAO implements ICarteraResumenDAO {
    // property constants

    //public static final CarteraResumenId  ID = "id";
    public static final String ID = "id";
    
    //public static final CarteraResumenId  RANGO = "id.rango";
    public static final String RANGO = "id.rango";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved CarteraResumen entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * CarteraResumenDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            CarteraResumen entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(CarteraResumen entity) {
        EntityManagerHelper.log("saving CarteraResumen instance", Level.INFO,
            null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent CarteraResumen entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * CarteraResumenDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            CarteraResumen entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(CarteraResumen entity) {
        EntityManagerHelper.log("deleting CarteraResumen instance", Level.INFO,
            null);

        try {
            entity = getEntityManager()
                         .getReference(CarteraResumen.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved CarteraResumen entity and return it or a copy of it
     * to the sender. A copy of the CarteraResumen entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = CarteraResumenDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            CarteraResumen entity to update
     * @return CarteraResumen the persisted CarteraResumen entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public CarteraResumen update(CarteraResumen entity) {
        EntityManagerHelper.log("updating CarteraResumen instance", Level.INFO,
            null);

        try {
            CarteraResumen result = getEntityManager()
                                        .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public CarteraResumen findById(CarteraResumenId id) {
        EntityManagerHelper.log("finding CarteraResumen instance with id: " +
            id, Level.INFO, null);

        try {
            CarteraResumen instance = getEntityManager()
                                          .find(CarteraResumen.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  CarteraResumen entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  CarteraResumen property to query
     * @param value
     *            the property value to match
     * @return List< CarteraResumen> found by query
     */
    @SuppressWarnings("unchecked")
    public List<CarteraResumen> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  CarteraResumen instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  CarteraResumen model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all CarteraResumen entities with a specific property value.
     *
     * @param propertyName
     *            the name of the CarteraResumen property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<CarteraResumen> found by query
     */
    @SuppressWarnings("unchecked")
    public List<CarteraResumen> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding CarteraResumen instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from CarteraResumen model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<CarteraResumen> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<CarteraResumen> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all CarteraResumen entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<CarteraResumen> all CarteraResumen entities
     */
    @SuppressWarnings("unchecked")
    public List<CarteraResumen> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all CarteraResumen instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from CarteraResumen model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<CarteraResumen> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from CarteraResumen model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<CarteraResumen> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in CarteraResumen failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<CarteraResumen> findPageCarteraResumen(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from CarteraResumen model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from CarteraResumen model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberCarteraResumen() {
        try {
            String queryString = "select count(*) from CarteraResumen model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo encargado de generar el resumen de la cartera agrupando los montos por rango
    public List consultarCartera(String codSap){    	
    	String query = "SELECT RANGO, SUM(CONVERT(NUMERIC, MONTO)) FROM CARTERA_RESUMEN WHERE CODCLI='"+codSap+"' GROUP BY RANGO ORDER BY 1";
    	
    	List registros = null;
    	try{    		
    		registros = getEntityManager().createNativeQuery(query).getResultList();
    		
    		if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}   		
    	}catch (Exception e) {
			return null;
		}
    	
    	return registros;
    }
    
    
    //metodo encrgado de consultar el detalle de la factura seleccionada
    public List<CarteraResumen> consultarDetalleFactura(String codSap, String rango){    	
    	String query = "SELECT NUMFAC, DESDOC, FECFAC, FECVEN, DIAS, MONTO, DESDIV " +
    				   "FROM CARTERA_RESUMEN, CLASES_DOC, CARTERA_DIV " +
    				   "WHERE CODCLI = '"+codSap.trim()+ "' " +
    				   "AND RANGO = '"+rango.trim()+"' AND CLSDOC = CODDOC AND PLANTA = CODDIV ";
    	
    	List registros = null;
    	List<CarteraResumen> resultado = null;
    	try{    		
    		registros = getEntityManager().createNativeQuery(query).getResultList();
    		
    		if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	} 
    		DecimalFormat df = new DecimalFormat("###,###,###,##0.00");
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
    		
    		resultado = new ArrayList<CarteraResumen>();
    		for(int j=0; j<registros.size() ;j++){
    			Object[] reg = (Object[])registros.get(j);
    			CarteraResumen cartRes = new CarteraResumen();
    			CarteraResumenId cartResId = new CarteraResumenId();
    			
    			cartResId.setNumfac((String)reg[0]);
    			cartResId.setClsdoc((String)reg[1]);
    			cartResId.setFecfac(sdf.format((Timestamp)reg[2]));
    			cartResId.setFecven(sdf.format((Timestamp)reg[3]));
    			cartResId.setDias((String)reg[4]);
    			cartResId.setMonto(df.format(new Double((String)reg[5])));
    			cartResId.setPlanta((String)reg[6]);
    			
    			cartRes.setId(cartResId);   
    			
    			resultado.add(cartRes);
    		}
    	}catch (Exception e) {
			return null;
		}
    	
    	return resultado;
    }
}
