package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvPdfsProductos entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvPdfsProductos
 */
public class TsccvPdfsProductosDAO implements ITsccvPdfsProductosDAO {
    // property constants

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final TsccvPdfsProductosId  ID = "id";
    public static final String ID = "id";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TsccvPdfsProductos entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvPdfsProductosDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPdfsProductos entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvPdfsProductos entity) {
        EntityManagerHelper.log("saving TsccvPdfsProductos instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent TsccvPdfsProductos entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvPdfsProductosDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvPdfsProductos entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvPdfsProductos entity) {
        EntityManagerHelper.log("deleting TsccvPdfsProductos instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvPdfsProductos.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvPdfsProductos entity and return it or a copy of it
     * to the sender. A copy of the TsccvPdfsProductos entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvPdfsProductosDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPdfsProductos entity to update
     * @return TsccvPdfsProductos the persisted TsccvPdfsProductos entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvPdfsProductos update(TsccvPdfsProductos entity) {
        EntityManagerHelper.log("updating TsccvPdfsProductos instance",
            Level.INFO, null);

        try {
            TsccvPdfsProductos result = getEntityManager()
                                            .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvPdfsProductos findById(TsccvPdfsProductosId id) {
        EntityManagerHelper.log("finding TsccvPdfsProductos instance with id: " +
            id, Level.INFO, null);

        try {
            TsccvPdfsProductos instance = getEntityManager()
                                              .find(TsccvPdfsProductos.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvPdfsProductos entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvPdfsProductos property to query
     * @param value
     *            the property value to match
     * @return List< TsccvPdfsProductos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvPdfsProductos> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TsccvPdfsProductos instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvPdfsProductos model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvPdfsProductos entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvPdfsProductos property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvPdfsProductos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvPdfsProductos> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TsccvPdfsProductos instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvPdfsProductos model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvPdfsProductos> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TsccvPdfsProductos> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TsccvPdfsProductos> findById(Object id,
        int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<TsccvPdfsProductos> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all TsccvPdfsProductos entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPdfsProductos> all TsccvPdfsProductos entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvPdfsProductos> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvPdfsProductos instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvPdfsProductos model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvPdfsProductos> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvPdfsProductos model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvPdfsProductos> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvPdfsProductos failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvPdfsProductos> findPageTsccvPdfsProductos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvPdfsProductos model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvPdfsProductos model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvPdfsProductos() {
        try {
            String queryString = "select count(*) from TsccvPdfsProductos model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
}
