package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvPlantaDAO.
 *
*/
public interface ITsccvPlantaDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvPlanta entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvPlantaDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPlanta entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvPlanta entity);

    /**
     * Delete a persistent TsccvPlanta entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvPlantaDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvPlanta entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvPlanta entity);

    /**
     * Persist a previously saved TsccvPlanta entity and return it or a copy of it
     * to the sender. A copy of the TsccvPlanta entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvPlantaDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPlanta entity to update
     * @return TsccvPlanta the persisted TsccvPlanta entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvPlanta update(TsccvPlanta entity);

    public TsccvPlanta findById(String id);

    /**
     * Find all TsccvPlanta entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvPlanta property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPlanta> found by query
     */
    public List<TsccvPlanta> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<TsccvPlanta> findByCriteria(String whereCondition);

    public List<TsccvPlanta> findByCodpla(Object codpla);

    public List<TsccvPlanta> findByCodpla(Object codpla,
        int... rowStartIdxAndCount);

    public List<TsccvPlanta> findByDespla(Object despla);

    public List<TsccvPlanta> findByDespla(Object despla,
        int... rowStartIdxAndCount);

    public List<TsccvPlanta> findByDirele(Object direle);

    public List<TsccvPlanta> findByDirele(Object direle,
        int... rowStartIdxAndCount);

    public List<TsccvPlanta> findByPlanta(Object planta);

    public List<TsccvPlanta> findByPlanta(Object planta,
        int... rowStartIdxAndCount); 

    /**
     * Find all TsccvPlanta entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPlanta> all TsccvPlanta entities
     */
    public List<TsccvPlanta> findAll(int... rowStartIdxAndCount);

    public List<TsccvPlanta> findPageTsccvPlanta(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberTsccvPlanta();
    
    public List<TsccvPlanta> consultarPlantas(TsccvPlanta entity);
    
    public Boolean plantaIsRegistrada(String desPla);
    
    public List<TsccvPlanta> findByActivoAplicacion(String activo, Long aplicacion);
    
    public List<TsccvPlanta> findByUsuarioAndAplicacion(Integer usuario, Integer aplicacion, String activoApp);
    
    public List<TsccvPlanta> findByTipProdAndAplicacion(String condicion, Long aplicacion);
}
