package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;


/**
 * A data access object (DAO) providing persistence and search support for
 * TbmBaseGruposUsuarios entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TbmBaseGruposUsuarios
 */
public class TbmBaseGruposUsuariosDAO implements ITbmBaseGruposUsuariosDAO {
    // property constants

    //public static final String  ACTIVO = "activo";
    public static final String ACTIVO = "activo";

    //public static final String  DESCRIPCION = "descripcion";
    public static final String DESCRIPCION = "descripcion";

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Long  IDGRUP = "idGrup";
    public static final String IDGRUP = "idGrup";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TbmBaseGruposUsuarios entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseGruposUsuariosDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseGruposUsuarios entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseGruposUsuarios entity) {
        /*EntityManagerHelper.log("saving TbmBaseGruposUsuarios instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }*/
    }

    public static synchronized void saveGrupoUsuario(TbmBaseGruposUsuarios entity) {
        EntityManagerHelper.log("saving TbmBaseGruposUsuarios instance", Level.INFO, null);
        Object tempId = "";
        Long idGrup= null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	tempId = EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_Grup) from tbm_base_grupos_usuarios").getSingleResult();
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro
        	idGrup = tempId != null ? ((((BigDecimal) tempId).longValue()) + 1) : 1;
        	
        	entity.setIdGrup(idGrup);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }
    
    /**
     * Delete a persistent TbmBaseGruposUsuarios entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseGruposUsuariosDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseGruposUsuarios entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseGruposUsuarios entity) {
        EntityManagerHelper.log("deleting TbmBaseGruposUsuarios instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TbmBaseGruposUsuarios.class,
                    entity.getIdGrup());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TbmBaseGruposUsuarios entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseGruposUsuarios entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TbmBaseGruposUsuariosDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseGruposUsuarios entity to update
     * @return TbmBaseGruposUsuarios the persisted TbmBaseGruposUsuarios entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseGruposUsuarios update(TbmBaseGruposUsuarios entity) {
        EntityManagerHelper.log("updating TbmBaseGruposUsuarios instance",
            Level.INFO, null);

        try {
        	((Session)getEntityManager().getDelegate()).update(entity);
        	/*TbmBaseGruposUsuarios result = getEntityManager()
                                               .merge(entity);*/
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return entity;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TbmBaseGruposUsuarios findById(Long id) {
        EntityManagerHelper.log(
            "finding TbmBaseGruposUsuarios instance with id: " + id,
            Level.INFO, null);

        try {
            TbmBaseGruposUsuarios instance = getEntityManager()
                                                 .find(TbmBaseGruposUsuarios.class,
                    id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TbmBaseGruposUsuarios entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TbmBaseGruposUsuarios property to query
     * @param value
     *            the property value to match
     * @return List< TbmBaseGruposUsuarios> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseGruposUsuarios> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TbmBaseGruposUsuarios instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TbmBaseGruposUsuarios model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TbmBaseGruposUsuarios entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseGruposUsuarios property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TbmBaseGruposUsuarios> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseGruposUsuarios> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TbmBaseGruposUsuarios instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseGruposUsuarios model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseGruposUsuarios> findByActivo(Object activo,
        int... rowStartIdxAndCount) {
        return findByProperty(ACTIVO, activo, rowStartIdxAndCount);
    }

    public List<TbmBaseGruposUsuarios> findByActivo(Object activo) {
        return findByProperty(ACTIVO, activo);
    }

    public List<TbmBaseGruposUsuarios> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount) {
        return findByProperty(DESCRIPCION, descripcion, rowStartIdxAndCount);
    }

    public List<TbmBaseGruposUsuarios> findByDescripcion(Object descripcion) {
        return findByProperty(DESCRIPCION, descripcion);
    }

    public List<TbmBaseGruposUsuarios> findByFechaCreacion(
        Object fechaCreacion, int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TbmBaseGruposUsuarios> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TbmBaseGruposUsuarios> findByIdGrup(Object idGrup,
        int... rowStartIdxAndCount) {
        return findByProperty(IDGRUP, idGrup, rowStartIdxAndCount);
    }

    public List<TbmBaseGruposUsuarios> findByIdGrup(Object idGrup) {
        return findByProperty(IDGRUP, idGrup);
    }

    /**
     * Find all TbmBaseGruposUsuarios entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseGruposUsuarios> all TbmBaseGruposUsuarios entities
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseGruposUsuarios> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TbmBaseGruposUsuarios instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseGruposUsuarios model order by model asc";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseGruposUsuarios> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TbmBaseGruposUsuarios model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TbmBaseGruposUsuarios> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TbmBaseGruposUsuarios failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseGruposUsuarios> findPageTbmBaseGruposUsuarios(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TbmBaseGruposUsuarios model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TbmBaseGruposUsuarios model order by model asc";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTbmBaseGruposUsuarios() {
        try {
            String queryString = "select count(*) from TbmBaseGruposUsuarios model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    
    //consulta los grupos usuario con los datos para saber si esta registrado 
    public Boolean grupoUsuarioIsRegistrado(String descripcion){
    	String query = "SELECT * FROM TBM_BASE_GRUPOS_USUARIOS " +
    				   "WHERE DESCRIPCION = '"+descripcion+"' ";
    	List resultado = null;
    	try{
    		resultado = EntityManagerHelper.getEntityManager().createNativeQuery(query).getResultList();
	    	
	    	return (resultado != null && resultado.size() != 0) ? true : false;
    	}catch(Exception e){
    		return null;
    	}
    }
    
    
    //Metodo utilizado para la consulta por todos los campos
    public List<TbmBaseGruposUsuarios> consultarGruposUsuario(TbmBaseGruposUsuarios entity){
    	StringBuffer query = new StringBuffer("SELECT ID_GRUP, DESCRIPCION, ACTIVO, FECHA_CREACION FROM TBM_BASE_GRUPOS_USUARIOS ");
    	List registros=null;
    	List<TbmBaseGruposUsuarios> resultado= null;
    	try {
	    	//Validamos si los campos vienen con valor o no para armar el query
	    	if(entity.getIdGrup() != null){
	    		query.append("WHERE ID_GRUP LIKE '%"+entity.getIdGrup()+"%' ");
	    	}
	    	else{
	    		query.append("WHERE ID_GRUP LIKE '%' ");
	    	}    		    
	    	
	    	if(entity.getDescripcion() != null){
	    		query.append("AND DESCRIPCION LIKE '%"+entity.getDescripcion()+"%' ");
	    	}
	    	else{
	    		query.append("AND DESCRIPCION LIKE '%' ");
	    	}
	    	
	    	if(entity.getActivo() != null){
	    		query.append("AND ACTIVO LIKE '%"+entity.getActivo()+"%' ");
	    	}
	    	else{
	    		query.append("AND ACTIVO LIKE '%' ");
	    	}	    		    	
	    	
	    	//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<TbmBaseGruposUsuarios>();         
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TbmBaseGruposUsuarios grupUsua = new TbmBaseGruposUsuarios();
	    		
	    		grupUsua.setIdGrup(((BigDecimal)reg[0]).longValue());	    		
	    		grupUsua.setDescripcion((String)reg[1]);
	    		grupUsua.setActivo((String)reg[2]);
	    		grupUsua.setFechaCreacion((Date)reg[3]);
	    		
	            resultado.add(grupUsua);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
    
    
    public void inactivarGrupoUsuario(Long idGrup){    	
    	EntityManagerHelper.log("inactivating TbmBaseGruposUsuarios instance", Level.INFO, null);
    	String query = "update TbmBaseGruposUsuarios model set activo = '1' where model.idGrup = "+idGrup+" ";
    	
		try{
			getEntityManager().createQuery(query).executeUpdate();
			EntityManagerHelper.log("inactivation successful", Level.INFO, null);
		}catch(RuntimeException re){
			EntityManagerHelper.log("inactivation failed", Level.SEVERE, re);
			throw re;
		}
    }
}
