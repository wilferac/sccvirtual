package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TbmBaseGruposUsuariosDAO.
 *
*/
public interface ITbmBaseGruposUsuariosDAO {
    /**
     * Perform an initial save of a previously unsaved TbmBaseGruposUsuarios entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseGruposUsuariosDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseGruposUsuarios entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseGruposUsuarios entity);

    /**
     * Delete a persistent TbmBaseGruposUsuarios entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseGruposUsuariosDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseGruposUsuarios entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseGruposUsuarios entity);

    /**
     * Persist a previously saved TbmBaseGruposUsuarios entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseGruposUsuarios entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITbmBaseGruposUsuariosDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseGruposUsuarios entity to update
     * @return TbmBaseGruposUsuarios the persisted TbmBaseGruposUsuarios entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseGruposUsuarios update(TbmBaseGruposUsuarios entity);

    public TbmBaseGruposUsuarios findById(Long id);

    /**
     * Find all TbmBaseGruposUsuarios entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseGruposUsuarios property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseGruposUsuarios> found by query
     */
    public List<TbmBaseGruposUsuarios> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TbmBaseGruposUsuarios> findByCriteria(String whereCondition);

    public List<TbmBaseGruposUsuarios> findByActivo(Object activo);

    public List<TbmBaseGruposUsuarios> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TbmBaseGruposUsuarios> findByDescripcion(Object descripcion);

    public List<TbmBaseGruposUsuarios> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount);

    public List<TbmBaseGruposUsuarios> findByFechaCreacion(Object fechaCreacion);

    public List<TbmBaseGruposUsuarios> findByFechaCreacion(
        Object fechaCreacion, int... rowStartIdxAndCount);

    public List<TbmBaseGruposUsuarios> findByIdGrup(Object idGrup);

    public List<TbmBaseGruposUsuarios> findByIdGrup(Object idGrup,
        int... rowStartIdxAndCount);

    /**
     * Find all TbmBaseGruposUsuarios entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseGruposUsuarios> all TbmBaseGruposUsuarios entities
     */
    public List<TbmBaseGruposUsuarios> findAll(int... rowStartIdxAndCount);

    public List<TbmBaseGruposUsuarios> findPageTbmBaseGruposUsuarios(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTbmBaseGruposUsuarios();
    
    public Boolean grupoUsuarioIsRegistrado(String descripcion);
    
    public List<TbmBaseGruposUsuarios> consultarGruposUsuario(TbmBaseGruposUsuarios entity);
    
    public void inactivarGrupoUsuario(Long idGrup);
}
