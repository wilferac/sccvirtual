package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.exceptions.BMBaseException;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvUsuarioDAO.
 *
*/
public interface ITsccvUsuarioDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvUsuario entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvUsuarioDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvUsuario entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvUsuario entity);

    /**
     * Delete a persistent TsccvUsuario entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvUsuarioDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvUsuario entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvUsuario entity);

    /**
     * Persist a previously saved TsccvUsuario entity and return it or a copy of it
     * to the sender. A copy of the TsccvUsuario entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvUsuarioDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvUsuario entity to update
     * @return TsccvUsuario the persisted TsccvUsuario entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvUsuario update(TsccvUsuario entity);

    public TsccvUsuario findById(Integer id);

    /**
     * Find all TsccvUsuario entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvUsuario property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvUsuario> found by query
     */
    public List<TsccvUsuario> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<TsccvUsuario> findByCriteria(String whereCondition);

    public List<TsccvUsuario> findByActivo(Object activo);

    public List<TsccvUsuario> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TsccvUsuario> findByApellido(Object apellido);

    public List<TsccvUsuario> findByApellido(Object apellido,
        int... rowStartIdxAndCount);

    public List<TsccvUsuario> findByCedula(Object cedula);

    public List<TsccvUsuario> findByCedula(Object cedula,
        int... rowStartIdxAndCount);

    public List<TsccvUsuario> findByEmail(Object email);

    public List<TsccvUsuario> findByEmail(Object email,
        int... rowStartIdxAndCount);

    public List<TsccvUsuario> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvUsuario> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TsccvUsuario> findByIdUsua(Object idUsua);

    public List<TsccvUsuario> findByIdUsua(Object idUsua,
        int... rowStartIdxAndCount);

    public List<TsccvUsuario> findByLogin(Object login);

    public List<TsccvUsuario> findByLogin(Object login,
        int... rowStartIdxAndCount);

    public List<TsccvUsuario> findByNombre(Object nombre);

    public List<TsccvUsuario> findByNombre(Object nombre,
        int... rowStartIdxAndCount);

    public List<TsccvUsuario> findByPassword(Object password);

    public List<TsccvUsuario> findByPassword(Object password,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvUsuario entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvUsuario> all TsccvUsuario entities
     */
    public List<TsccvUsuario> findAll(int... rowStartIdxAndCount);

    public List<TsccvUsuario> findPageTsccvUsuario(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberTsccvUsuario();
    
    public List<TsccvUsuario> consultarUsuarios(TsccvUsuario entity);
    
    public Boolean usuarioIsRegistrado(String login, Integer cedula, Integer idEmpr) throws Exception;
    
    public void inactivarUsuario(Integer idUsua);
    
    public List<TsccvUsuario> consultarUsuariosPorGrupo(Integer idGrup);
    
    public int cambiarPassword(TsccvUsuario tsccvUsuario, String passwordNuevo);
    
    public List<TsccvUsuario> consultarVendedores(Integer codCli, String codPla);
}
