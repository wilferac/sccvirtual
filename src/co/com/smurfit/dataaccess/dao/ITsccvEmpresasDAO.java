package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvEmpresasDAO.
 *
*/
public interface ITsccvEmpresasDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvEmpresas entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvEmpresasDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEmpresas entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvEmpresas entity);

    /**
     * Delete a persistent TsccvEmpresas entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvEmpresasDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvEmpresas entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvEmpresas entity);

    /**
     * Persist a previously saved TsccvEmpresas entity and return it or a copy of it
     * to the sender. A copy of the TsccvEmpresas entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvEmpresasDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEmpresas entity to update
     * @return TsccvEmpresas the persisted TsccvEmpresas entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvEmpresas update(TsccvEmpresas entity);

    public TsccvEmpresas findById(Integer id);

    /**
     * Find all TsccvEmpresas entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvEmpresas property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvEmpresas> found by query
     */
    public List<TsccvEmpresas> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvEmpresas> findByCriteria(String whereCondition);

    public List<TsccvEmpresas> findByActivo(Object activo);

    public List<TsccvEmpresas> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TsccvEmpresas> findByCodMas(Object codMas);

    public List<TsccvEmpresas> findByCodMas(Object codMas,
        int... rowStartIdxAndCount);

    public List<TsccvEmpresas> findByCodSap(Object codSap);

    public List<TsccvEmpresas> findByCodSap(Object codSap,
        int... rowStartIdxAndCount);

    public List<TsccvEmpresas> findByCodVpe(Object codVpe);

    public List<TsccvEmpresas> findByCodVpe(Object codVpe,
        int... rowStartIdxAndCount);

    public List<TsccvEmpresas> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvEmpresas> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TsccvEmpresas> findByIdEmpr(Object idEmpr);

    public List<TsccvEmpresas> findByIdEmpr(Object idEmpr,
        int... rowStartIdxAndCount);

    public List<TsccvEmpresas> findByNombre(Object nombre);

    public List<TsccvEmpresas> findByNombre(Object nombre,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvEmpresas entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvEmpresas> all TsccvEmpresas entities
     */
    public List<TsccvEmpresas> findAll(int... rowStartIdxAndCount);

    public List<TsccvEmpresas> findPageTsccvEmpresas(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberTsccvEmpresas();
    
    public Boolean empresaIsRegistrada(String nombre, String codSap, String codMas, String codVpe);
    
    public List<TsccvEmpresas> consularEmpresas(TsccvEmpresas entity);
    
    public void inactivarEmpresa(Integer idEmpr);
    
    public List<TsccvEmpresas> buscarEmpresa(TsccvEmpresas entity, Boolean pantallaIsAdmin);
}
