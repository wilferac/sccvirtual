package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuarioId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvPlantasUsuarioDAO.
 *
*/
public interface ITsccvPlantasUsuarioDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvPlantasUsuario entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvPlantasUsuarioDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPlantasUsuario entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvPlantasUsuario entity);

    /**
     * Delete a persistent TsccvPlantasUsuario entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvPlantasUsuarioDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvPlantasUsuario entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvPlantasUsuario entity);

    /**
     * Persist a previously saved TsccvPlantasUsuario entity and return it or a copy of it
     * to the sender. A copy of the TsccvPlantasUsuario entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvPlantasUsuarioDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPlantasUsuario entity to update
     * @return TsccvPlantasUsuario the persisted TsccvPlantasUsuario entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvPlantasUsuario update(TsccvPlantasUsuario entity);

    public TsccvPlantasUsuario findById(TsccvPlantasUsuarioId id);

    /**
     * Find all TsccvPlantasUsuario entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvPlantasUsuario property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPlantasUsuario> found by query
     */
    public List<TsccvPlantasUsuario> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvPlantasUsuario> findByCriteria(String whereCondition);

    public List<TsccvPlantasUsuario> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvPlantasUsuario> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TsccvPlantasUsuario> findById(Object id);

    public List<TsccvPlantasUsuario> findById(Object id,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvPlantasUsuario entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPlantasUsuario> all TsccvPlantasUsuario entities
     */
    public List<TsccvPlantasUsuario> findAll(int... rowStartIdxAndCount);

    public List<TsccvPlantasUsuario> findPageTsccvPlantasUsuario(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvPlantasUsuario();
}
