package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;
import co.com.smurfit.sccvirtual.entidades.TsccvEstadisticaAccPed;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvEstadisticaAccPed entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvEstadisticaAccPed
 */
public class TsccvEstadisticaAccPedDAO implements ITsccvEstadisticaAccPedDAO {
    // property constants

    //public static final Long  ACCESO = "acceso";
    public static final String ACCESO = "acceso";

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Long  IDESAP = "idEsap";
    public static final String IDESAP = "idEsap";

    //public static final Long  PEDIDO = "pedido";
    public static final String PEDIDO = "pedido";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TsccvEstadisticaAccPed entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvEstadisticaAccPedDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEstadisticaAccPed entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvEstadisticaAccPed entity) {
        /*EntityManagerHelper.log("saving TsccvEstadisticaAccPed instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }*/
    }

    
    public static void saveEstadisticaAccPed(TsccvEstadisticaAccPed entity) {
        EntityManagerHelper.log("saving TsccvEstadisticaAccPed instance", Level.INFO, null);
        Object tempId = "";
        Long idEsap = null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	tempId = EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_esap) from tsccv_estadistica_acc_ped").getSingleResult();         	
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro
        	idEsap = tempId != null ? ((((BigDecimal) tempId).longValue()) + 1) : 1;
        	
        	entity.setIdEsap(idEsap);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }
    
    /**
     * Delete a persistent TsccvEstadisticaAccPed entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvEstadisticaAccPedDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvEstadisticaAccPed entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvEstadisticaAccPed entity) {
        EntityManagerHelper.log("deleting TsccvEstadisticaAccPed instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvEstadisticaAccPed.class,
                    entity.getIdEsap());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvEstadisticaAccPed entity and return it or a copy of it
     * to the sender. A copy of the TsccvEstadisticaAccPed entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvEstadisticaAccPedDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEstadisticaAccPed entity to update
     * @return TsccvEstadisticaAccPed the persisted TsccvEstadisticaAccPed entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvEstadisticaAccPed update(TsccvEstadisticaAccPed entity) {
        EntityManagerHelper.log("updating TsccvEstadisticaAccPed instance",
            Level.INFO, null);

        try {
            TsccvEstadisticaAccPed result = getEntityManager()
                                                .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvEstadisticaAccPed findById(Long id) {
        EntityManagerHelper.log(
            "finding TsccvEstadisticaAccPed instance with id: " + id,
            Level.INFO, null);

        try {
            TsccvEstadisticaAccPed instance = getEntityManager()
                                                  .find(TsccvEstadisticaAccPed.class,
                    id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvEstadisticaAccPed entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvEstadisticaAccPed property to query
     * @param value
     *            the property value to match
     * @return List< TsccvEstadisticaAccPed> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvEstadisticaAccPed> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TsccvEstadisticaAccPed instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvEstadisticaAccPed model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvEstadisticaAccPed entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvEstadisticaAccPed property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvEstadisticaAccPed> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvEstadisticaAccPed> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TsccvEstadisticaAccPed instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvEstadisticaAccPed model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvEstadisticaAccPed> findByAcceso(Object acceso,
        int... rowStartIdxAndCount) {
        return findByProperty(ACCESO, acceso, rowStartIdxAndCount);
    }

    public List<TsccvEstadisticaAccPed> findByAcceso(Object acceso) {
        return findByProperty(ACCESO, acceso);
    }

    public List<TsccvEstadisticaAccPed> findByFechaCreacion(
        Object fechaCreacion, int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TsccvEstadisticaAccPed> findByFechaCreacion(
        Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TsccvEstadisticaAccPed> findByIdEsap(Object idEsap,
        int... rowStartIdxAndCount) {
        return findByProperty(IDESAP, idEsap, rowStartIdxAndCount);
    }

    public List<TsccvEstadisticaAccPed> findByIdEsap(Object idEsap) {
        return findByProperty(IDESAP, idEsap);
    }

    public List<TsccvEstadisticaAccPed> findByPedido(Object pedido,
        int... rowStartIdxAndCount) {
        return findByProperty(PEDIDO, pedido, rowStartIdxAndCount);
    }

    public List<TsccvEstadisticaAccPed> findByPedido(Object pedido) {
        return findByProperty(PEDIDO, pedido);
    }

    /**
     * Find all TsccvEstadisticaAccPed entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvEstadisticaAccPed> all TsccvEstadisticaAccPed entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvEstadisticaAccPed> findAll(
        final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvEstadisticaAccPed instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvEstadisticaAccPed model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvEstadisticaAccPed> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvEstadisticaAccPed model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvEstadisticaAccPed> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvEstadisticaAccPed failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvEstadisticaAccPed> findPageTsccvEstadisticaAccPed(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvEstadisticaAccPed model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvEstadisticaAccPed model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvEstadisticaAccPed() {
        try {
            String queryString = "select count(*) from TsccvEstadisticaAccPed model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
}
