package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvPedidos entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvPedidos
 */
public class TsccvPedidosDAO implements ITsccvPedidosDAO {
    // property constants

    //public static final String  ACTIVO = "activo";
    public static final String ACTIVO = "activo";

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Integer  IDPEDI = "idPedi";
    public static final String IDPEDI = "idPedi";

    //public static final String  OBSERVACIONES = "observaciones";
    public static final String OBSERVACIONES = "observaciones";

    //public static final String  REGISTRADO = "registrado";
    public static final String REGISTRADO = "registrado";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TsccvPedidos entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvPedidosDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPedidos entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvPedidos entity) {
        /*EntityManagerHelper.log("saving TsccvPedidos instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }*/
    }

    
    public static void savePedido(TsccvPedidos entity) {
        EntityManagerHelper.log("saving TsccvPedidos instance", Level.INFO, null);
        Integer idPedi = null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	idPedi = (Integer) EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_pedi) from Tsccv_Pedidos").getSingleResult();
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro
        	idPedi = idPedi != null ? (idPedi + 1) : 1;
        	entity.setIdPedi(idPedi);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }
    /**
     * Delete a persistent TsccvPedidos entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvPedidosDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvPedidos entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvPedidos entity) {
        EntityManagerHelper.log("deleting TsccvPedidos instance", Level.INFO,
            null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvPedidos.class, entity.getIdPedi());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvPedidos entity and return it or a copy of it
     * to the sender. A copy of the TsccvPedidos entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvPedidosDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPedidos entity to update
     * @return TsccvPedidos the persisted TsccvPedidos entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvPedidos update(TsccvPedidos entity) {
        EntityManagerHelper.log("updating TsccvPedidos instance", Level.INFO,
            null);

        try {
        	((Session)getEntityManager().getDelegate()).update(entity);
        	/*TsccvPedidos result = getEntityManager()
                                      .merge(entity);*/
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return entity;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvPedidos findById(Integer id) {
        EntityManagerHelper.log("finding TsccvPedidos instance with id: " + id,
            Level.INFO, null);

        try {
            TsccvPedidos instance = getEntityManager()
                                        .find(TsccvPedidos.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvPedidos entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvPedidos property to query
     * @param value
     *            the property value to match
     * @return List< TsccvPedidos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvPedidos> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TsccvPedidos instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvPedidos model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvPedidos entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvPedidos property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvPedidos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvPedidos> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding TsccvPedidos instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvPedidos model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvPedidos> findByActivo(Object activo,
        int... rowStartIdxAndCount) {
        return findByProperty(ACTIVO, activo, rowStartIdxAndCount);
    }

    public List<TsccvPedidos> findByActivo(Object activo) {
        return findByProperty(ACTIVO, activo);
    }

    public List<TsccvPedidos> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TsccvPedidos> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TsccvPedidos> findByIdPedi(Object idPedi,
        int... rowStartIdxAndCount) {
        return findByProperty(IDPEDI, idPedi, rowStartIdxAndCount);
    }

    public List<TsccvPedidos> findByIdPedi(Object idPedi) {
        return findByProperty(IDPEDI, idPedi);
    }

    public List<TsccvPedidos> findByObservaciones(Object observaciones,
        int... rowStartIdxAndCount) {
        return findByProperty(OBSERVACIONES, observaciones, rowStartIdxAndCount);
    }

    public List<TsccvPedidos> findByObservaciones(Object observaciones) {
        return findByProperty(OBSERVACIONES, observaciones);
    }

    public List<TsccvPedidos> findByRegistrado(Object registrado,
        int... rowStartIdxAndCount) {
        return findByProperty(REGISTRADO, registrado, rowStartIdxAndCount);
    }

    public List<TsccvPedidos> findByRegistrado(Object registrado) {
        return findByProperty(REGISTRADO, registrado);
    }

    /**
     * Find all TsccvPedidos entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPedidos> all TsccvPedidos entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvPedidos> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvPedidos instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvPedidos model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvPedidos> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvPedidos model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvPedidos> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvPedidos failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvPedidos> findPageTsccvPedidos(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvPedidos model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvPedidos model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvPedidos() {
        try {
            String queryString = "select count(*) from TsccvPedidos model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo encargado de consultar el ultimo pedido registrado
    public Integer consultarUltimoIdPedido(){
    	Integer ultimoIdPed = null;
		String query = "select max(idPedi) from TsccvPedidos model ";

		ultimoIdPed = (Integer)getEntityManager().createQuery(query).getSingleResult();
		
		return ultimoIdPed;    	
    }
    
    //metodo encargado de consultar los pedidos no resgistrados para un cliente (empresa)
    public Integer consultarPedidoNoRegistrado(Integer idEmpr, String codPla, Integer idUsua){
    	String query = "select id_Pedi from Tsccv_Pedidos where id_Empr = "+idEmpr+" and registrado = '1' and codpla = '"+codPla+"' and id_usua="+idUsua;
    	
    	List resultado = getEntityManager().createNativeQuery(query).getResultList();
    	Object idPedi = (resultado != null && resultado.size() > 0) ? resultado.get(0) : null;
    	
    	return idPedi != null ? (Integer)idPedi : null;
    }
}
