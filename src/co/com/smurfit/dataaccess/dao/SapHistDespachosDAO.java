package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachos;
import co.com.smurfit.sccvirtual.entidades.BvpHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachos;
import co.com.smurfit.sccvirtual.entidades.SapHistDespachosId;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;

import java.math.BigDecimal;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * SapHistDespachos entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.SapHistDespachos
 */
public class SapHistDespachosDAO implements ISapHistDespachosDAO {
    // property constants

    //public static final SapHistDespachosId  ID = "id";
    public static final String ID = "id";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved SapHistDespachos entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapHistDespachosDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapHistDespachos entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapHistDespachos entity) {
        EntityManagerHelper.log("saving SapHistDespachos instance", Level.INFO,
            null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent SapHistDespachos entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapHistDespachosDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapHistDespachos entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapHistDespachos entity) {
        EntityManagerHelper.log("deleting SapHistDespachos instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(SapHistDespachos.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved SapHistDespachos entity and return it or a copy of it
     * to the sender. A copy of the SapHistDespachos entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = SapHistDespachosDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapHistDespachos entity to update
     * @return SapHistDespachos the persisted SapHistDespachos entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapHistDespachos update(SapHistDespachos entity) {
        EntityManagerHelper.log("updating SapHistDespachos instance",
            Level.INFO, null);

        try {
            SapHistDespachos result = getEntityManager()
                                          .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public SapHistDespachos findById(SapHistDespachosId id) {
        EntityManagerHelper.log("finding SapHistDespachos instance with id: " +
            id, Level.INFO, null);

        try {
            SapHistDespachos instance = getEntityManager()
                                            .find(SapHistDespachos.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  SapHistDespachos entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  SapHistDespachos property to query
     * @param value
     *            the property value to match
     * @return List< SapHistDespachos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapHistDespachos> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  SapHistDespachos instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  SapHistDespachos model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all SapHistDespachos entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapHistDespachos property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<SapHistDespachos> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapHistDespachos> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding SapHistDespachos instance with property: " + propertyName +
            ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from SapHistDespachos model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapHistDespachos> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<SapHistDespachos> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all SapHistDespachos entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapHistDespachos> all SapHistDespachos entities
     */
    @SuppressWarnings("unchecked")
    public List<SapHistDespachos> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all SapHistDespachos instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from SapHistDespachos model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapHistDespachos> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from SapHistDespachos model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<SapHistDespachos> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in SapHistDespachos failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapHistDespachos> findPageSapHistDespachos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from SapHistDespachos model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from SapHistDespachos model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberSapHistDespachos() {
        try {
            String queryString = "select count(*) from SapHistDespachos model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    
    //metodo agregado para consultar el historico de despacho de acuerdo a la informacion ingresada
    public List<SapHistDespachos> consultarHistoricoDespachoSap(String codMas, String periodo){
    	String query="SELECT CODPRO, DESPRO, CANTID, DESUME, CANKGS " +
    				 "FROM SAP_HIST_DESPACHOS, SAP_UMEDIDA " +
    				 "WHERE LTRIM(RTRIM(CODCLI)) = '"+codMas.trim()+"' AND " +
    				 "LTRIM(RTRIM(NUMMES)) = '"+periodo.trim()+"' AND " +
    				 "LTRIM(RTRIM(UNDMED)) = LTRIM(RTRIM(CODUME)) "+
    				 "ORDER BY CODPRO ";
    	
		List registros=null;
		List<SapHistDespachos> resultado = new ArrayList<SapHistDespachos>();
		
		try {
			//Ejecutamos el query
			System.out.println(query.toString());
			registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos null
				return resultado;
			}
			
			DecimalFormat df = new DecimalFormat("###,###,###,##0");
			for(int i=0; i<registros.size() ; i++){
				Object[] reg= (Object[])registros.get(i);
				SapHistDespachos histDesp = new SapHistDespachos();
				SapHistDespachosId histDespId = new SapHistDespachosId();
				
				histDespId.setCodpro((String)reg[0]);
				histDespId.setDespro((String)reg[1]);
				histDespId.setCantid(df.format(new Double((String)reg[2])));
				histDespId.setUndmed((String)reg[3]);
				histDespId.setCankgs(df.format(new Double((String)reg[4])));
				
				histDesp.setId(histDespId);
				resultado.add(histDesp);
			}
		
			return resultado;
		
		}catch (Exception re) {
			//throw re;
			return null;
		}
    }
    
    //metodo encargado de consultar el primer perido de despacho para un cliente
    public String minPeriodo(String codMas){
    	try{
	    	Object minPeriodo = null;
	    	
	    	String query = "SELECT MIN(NUMMES) " +
	    				   "FROM SAP_HIST_DESPACHOS " +
	    				   "WHERE CODCLI = '"+codMas.trim()+"' ";
	    	
	    	minPeriodo = EntityManagerHelper.getEntityManager().createNativeQuery(query).getSingleResult();
	    	
	    	return minPeriodo != null ? minPeriodo.toString() : null;
    	}catch (Exception e) {
			return null;
		}
    }
}
