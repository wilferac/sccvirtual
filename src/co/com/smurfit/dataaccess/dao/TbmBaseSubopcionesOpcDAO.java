package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpc;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopcionesOpcId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * TbmBaseSubopcionesOpc entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TbmBaseSubopcionesOpc
 */
public class TbmBaseSubopcionesOpcDAO implements ITbmBaseSubopcionesOpcDAO {
    // property constants

    //public static final TbmBaseSubopcionesOpcId  ID = "id";
    public static final String ID = "id";
    
    //public static final TbmBaseSubopcionesOpcId  ID = "id.idOpci";
    public static final String OPCION = "id.idOpci";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TbmBaseSubopcionesOpc entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseSubopcionesOpcDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopcionesOpc entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseSubopcionesOpc entity) {
        EntityManagerHelper.log("saving TbmBaseSubopcionesOpc instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent TbmBaseSubopcionesOpc entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseSubopcionesOpcDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopcionesOpc entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseSubopcionesOpc entity) {
        EntityManagerHelper.log("deleting TbmBaseSubopcionesOpc instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TbmBaseSubopcionesOpc.class,
                    entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TbmBaseSubopcionesOpc entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseSubopcionesOpc entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TbmBaseSubopcionesOpcDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopcionesOpc entity to update
     * @return TbmBaseSubopcionesOpc the persisted TbmBaseSubopcionesOpc entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseSubopcionesOpc update(TbmBaseSubopcionesOpc entity) {
        EntityManagerHelper.log("updating TbmBaseSubopcionesOpc instance",
            Level.INFO, null);

        try {
            TbmBaseSubopcionesOpc result = getEntityManager()
                                               .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TbmBaseSubopcionesOpc findById(TbmBaseSubopcionesOpcId id) {
        EntityManagerHelper.log(
            "finding TbmBaseSubopcionesOpc instance with id: " + id,
            Level.INFO, null);

        try {
            TbmBaseSubopcionesOpc instance = getEntityManager()
                                                 .find(TbmBaseSubopcionesOpc.class,
                    id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TbmBaseSubopcionesOpc entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TbmBaseSubopcionesOpc property to query
     * @param value
     *            the property value to match
     * @return List< TbmBaseSubopcionesOpc> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseSubopcionesOpc> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TbmBaseSubopcionesOpc instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TbmBaseSubopcionesOpc model where model." +
                propertyName + "= :propertyValue order by model.id.posicion asc";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TbmBaseSubopcionesOpc entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseSubopcionesOpc property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TbmBaseSubopcionesOpc> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseSubopcionesOpc> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TbmBaseSubopcionesOpc instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseSubopcionesOpc model where model." +
                propertyName + "= :propertyValue order by model.id.posicion asc";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseSubopcionesOpc> findById(Object id,
        int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<TbmBaseSubopcionesOpc> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all TbmBaseSubopcionesOpc entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseSubopcionesOpc> all TbmBaseSubopcionesOpc entities
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseSubopcionesOpc> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TbmBaseSubopcionesOpc instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseSubopcionesOpc model order by model.id.posicion asc";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseSubopcionesOpc> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TbmBaseSubopcionesOpc model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TbmBaseSubopcionesOpc> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TbmBaseSubopcionesOpc failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseSubopcionesOpc> findPageTbmBaseSubopcionesOpc(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TbmBaseSubopcionesOpc model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TbmBaseSubopcionesOpc model order by model.id.posicion asc";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTbmBaseSubopcionesOpc() {
        try {
            String queryString = "select count(*) from TbmBaseSubopcionesOpc model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    
    //consulta las sub. opociones opcion con los datos para saber si esta registrada 
    public Boolean subOpcionOpciIsRegistrada(String nombre, Long idOpci){
    	String query = "SELECT * FROM TBM_BASE_SUBOPCIONES_OPC WHERE NOMBRE_VISIBLE = '"+nombre+"' AND ID_OPCI = "+idOpci+" ";
    	List resultado = null;
    	try{
    		resultado = EntityManagerHelper.getEntityManager().createNativeQuery(query).getResultList();
	    	
	    	return (resultado != null && resultado.size() != 0) ? true : false;
    	}catch(Exception e){
    		return null;
    	}
    }
    
    //metodo encargado de consultar la ultima posicion de una sub. opcion para una opcion 
    public Object consultarUltimaPosicion(Long idOpcion){
		Object ultimaPos = null;
		String query = "SELECT MAX(POSICION) FROM TBM_BASE_SUBOPCIONES_OPC WHERE ID_OPCI = "+idOpcion+" ";

		ultimaPos = EntityManagerHelper.getEntityManager().createNativeQuery(query).getSingleResult();
		
		return ultimaPos;
	}
	
	
	//metodo encargado de modificar la posicion de la sub. opcion
    public void cambiarPosicion(TbmBaseSubopcionesOpc tbmBaseSubopcionesOpc, String opcion){
    	Long posicionActual = tbmBaseSubopcionesOpc.getId().getPosicion();
    	String query = null;
    	int numUpdate = 0;
    	
    	try{
    		if(opcion.equals("subir")){
	    		EntityManagerHelper.log("updating TbmBaseSubopcionesOpc instance", Level.INFO, null);
	    		
	    		EntityManagerHelper.beginTransaction();
	    		query = "update TbmBaseSubopcionesOpc model set model.id.posicion = -1 " +
	    				"where model.id.posicion = "+(posicionActual-1)+" and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";	    		
	    		numUpdate = getEntityManager().createQuery(query).executeUpdate();
	    		EntityManagerHelper.commit();
	    		
	    		if(numUpdate == 1){
	    			EntityManagerHelper.beginTransaction();
	    			query = "update TbmBaseSubopcionesOpc model set model.id.posicion = -2 " +
	    				"where model.id.posicion = "+(posicionActual)+" and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";	    		
		    		numUpdate = getEntityManager().createQuery(query).executeUpdate();
		    		EntityManagerHelper.commit();
		    		
		    		EntityManagerHelper.beginTransaction();
	    			query = "update TbmBaseSubopcionesOpc model set model.id.posicion = "+(posicionActual)+" " +
	    				"where model.id.posicion = -1 and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";				
		    		getEntityManager().createQuery(query).executeUpdate();
		    		EntityManagerHelper.commit();
		    		
		    		EntityManagerHelper.beginTransaction();
		    		query = "update TbmBaseSubopcionesOpc model set model.id.posicion = "+(posicionActual-1)+" " +
	    				"where model.id.posicion = -2 and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";				
		    		getEntityManager().createQuery(query).executeUpdate();	
		    		EntityManagerHelper.commit();
	    		}
	    	}
    		/*if(opcion.equals("bajar")){
	    		EntityManagerHelper.log("updating TbmBaseSubopcionesOpc instance", Level.INFO, null);
	    		
	    		EntityManagerHelper.beginTransaction();
	    		query = "update TbmBaseSubopcionesOpc model set model.posicion = -1 " +
	    				"where model.posicion = "+(posicionActual+1)+" and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";	    		
	    		numUpdate = getEntityManager().createQuery(query).executeUpdate();
	    		EntityManagerHelper.commit();
	    		
	    		if(numUpdate == 1){
	    			EntityManagerHelper.beginTransaction();
	    			query = "update TbmBaseSubopcionesOpc model set model.posicion = -2 " +
	    				"where model.posicion = "+(posicionActual)+" and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";	    		
		    		numUpdate = getEntityManager().createQuery(query).executeUpdate();
		    		EntityManagerHelper.commit();
		    		
		    		EntityManagerHelper.beginTransaction();
	    			query = "update TbmBaseSubopcionesOpc model set model.posicion = "+(posicionActual)+" " +
	    				"where model.posicion = -1 and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";				
		    		getEntityManager().createQuery(query).executeUpdate();
		    		EntityManagerHelper.commit();
		    		
		    		EntityManagerHelper.beginTransaction();
		    		query = "update TbmBaseSubopcionesOpc model set model.posicion = "+(posicionActual+1)+" " +
	    				"where model.posicion = -2 and model.id.idOpci = "+tbmBaseSubopcionesOpc.getId().getIdOpci()+" ";				
		    		getEntityManager().createQuery(query).executeUpdate();	
		    		EntityManagerHelper.commit();
	    		}
	    	}*/
    	}catch(Exception e){
    		EntityManagerHelper.log(e.getMessage(), Level.INFO, null);
    	}
    }
    
    //metodo encargado de restablecer las posiciones de las sub. opciones despues de eliminar una
    public void restablecerPosiciones(Long posAcual, Long idOpci){
    	//se consulta la posicion mas alta
    	Object ultimaPos = consultarUltimaPosicion(idOpci);
        Long ultPos = null;
        if(ultimaPos != null && ultimaPos != posAcual){
    		ultPos = ((BigDecimal)ultimaPos).longValue();
        
    		//se modifican las posiciones de las sub. opciones
	        for(Long i = posAcual; i<ultPos; i++){
		    	EntityManagerHelper.beginTransaction();
				String query = "update TbmBaseSubopcionesOpc model set model.id.posicion = "+(i)+" " +
					"where model.id.posicion = "+(i+1)+" and model.id.idOpci = "+idOpci+" ";
				getEntityManager().createQuery(query).executeUpdate();
				EntityManagerHelper.commit();
	    	}
    	}
    }
    
}
