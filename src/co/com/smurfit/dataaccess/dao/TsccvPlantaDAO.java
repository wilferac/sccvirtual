package co.com.smurfit.dataaccess.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.control.ITsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.ITsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.control.TsccvAplicacionesLogic;
import co.com.smurfit.sccvirtual.control.TsccvTipoProductoLogic;
import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvPlantasUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvTipoProducto;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.Utilities;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvPlanta entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvPlanta
 */
public class TsccvPlantaDAO implements ITsccvPlantaDAO {
    // property constants

    //public static final String  CODPLA = "codpla";
    public static final String CODPLA = "codpla";

    //public static final String  DESPLA = "despla";
    public static final String DESPLA = "despla";

    //public static final String  DIRELE = "direle";
    public static final String DIRELE = "direle";

    //public static final String  PLANTA = "planta";
    public static final String PLANTA = "planta";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    public void save(TsccvPlanta entity) {
    	
    }
    
    /**
     * Perform an initial save of a previously unsaved TsccvPlanta entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvPlantaDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPlanta entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public static synchronized void savePlanta(TsccvPlanta entity) {
    	EntityManagerHelper.log("saving TsccvPlanta instance", Level.INFO, null);
        String codpla= null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	codpla=(String) EntityManagerHelper.getEntityManager().createNativeQuery("select max(codpla) from tsccv_planta").getSingleResult();
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro
        	
        	codpla = codpla != null ? (Integer.parseInt(codpla) + 1)+"" : "1";
        	entity.setCodpla(codpla.toString());
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent TsccvPlanta entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvPlantaDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvPlanta entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvPlanta entity) {
        EntityManagerHelper.log("deleting TsccvPlanta instance", Level.INFO,
            null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvPlanta.class, entity.getCodpla());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvPlanta entity and return it or a copy of it
     * to the sender. A copy of the TsccvPlanta entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvPlantaDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPlanta entity to update
     * @return TsccvPlanta the persisted TsccvPlanta entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvPlanta update(TsccvPlanta entity) {
        EntityManagerHelper.log("updating TsccvPlanta instance", Level.INFO,
            null);

        try {
            //TsccvPlanta result = getEntityManager().merge(entity);
        	((Session)getEntityManager().getDelegate()).update(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return entity;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvPlanta findById(String id) {
        EntityManagerHelper.log("finding TsccvPlanta instance with id: " + id,
            Level.INFO, null);

        try {
            TsccvPlanta instance = getEntityManager().find(TsccvPlanta.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvPlanta entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvPlanta property to query
     * @param value
     *            the property value to match
     * @return List< TsccvPlanta> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvPlanta> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  TsccvPlanta instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvPlanta model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvPlanta entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvPlanta property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvPlanta> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvPlanta> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding TsccvPlanta instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvPlanta model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvPlanta> findByCodpla(Object codpla,
        int... rowStartIdxAndCount) {
        return findByProperty(CODPLA, codpla, rowStartIdxAndCount);
    }

    public List<TsccvPlanta> findByCodpla(Object codpla) {
        return findByProperty(CODPLA, codpla);
    }

    public List<TsccvPlanta> findByDespla(Object despla,
        int... rowStartIdxAndCount) {
        return findByProperty(DESPLA, despla, rowStartIdxAndCount);
    }

    public List<TsccvPlanta> findByDespla(Object despla) {
        return findByProperty(DESPLA, despla);
    }

    public List<TsccvPlanta> findByDirele(Object direle,
        int... rowStartIdxAndCount) {
        return findByProperty(DIRELE, direle, rowStartIdxAndCount);
    }

    public List<TsccvPlanta> findByDirele(Object direle) {
        return findByProperty(DIRELE, direle);
    }

    public List<TsccvPlanta> findByPlanta(Object planta,
        int... rowStartIdxAndCount) {
        return findByProperty(PLANTA, planta, rowStartIdxAndCount);
    }

    public List<TsccvPlanta> findByPlanta(Object planta) {
        return findByProperty(PLANTA, planta);
    }

    /**
     * Find all TsccvPlanta entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPlanta> all TsccvPlanta entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvPlanta> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvPlanta instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvPlanta model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvPlanta> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvPlanta model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvPlanta> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvPlanta failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvPlanta> findPageTsccvPlanta(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvPlanta model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvPlanta model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvPlanta() {
        try {
            String queryString = "select count(*) from TsccvPlanta model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
  //Metodo utilizado para la consulta por todos los campos
    public List<TsccvPlanta> consultarPlantas(TsccvPlanta entity){
    	StringBuffer query=new StringBuffer("SELECT CODPLA, ID_TIPR, ID_APLI, DESPLA, PLANTA, DIRELE FROM TSCCV_PLANTA ");
    	List registros=null;
    	List<TsccvPlanta> resultado= null;
    	try {
	    	//Validamos si los campos vienen con valor o no para armar el query
	    	if(entity.getCodpla() != null){
	    		query.append("WHERE CODPLA LIKE '%"+entity.getCodpla()+"%' ");
	    	}
	    	else{
	    		query.append("WHERE CODPLA LIKE '%' ");
	    	}
	    	
	    	if(entity.getTsccvTipoProducto() != null){
	    		query.append("AND ID_TIPR LIKE '%"+Utilities.TIPOS_PRODUCTO[Integer.parseInt(entity.getTsccvTipoProducto().getIdTipr().toString())-1]+"%' ");
	    	}
	    	else{
	    		query.append("AND (ID_TIPR LIKE '%' OR ID_TIPR IS NULL) ");
	    	}
	    	
	    	if(entity.getTsccvAplicaciones() != null){
	    		query.append("AND ID_APLI LIKE '%"+entity.getTsccvAplicaciones().getIdApli()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ID_APLI LIKE '%' OR ID_APLI IS NULL) ");
	    	}	    		    
	    	
	    	if(entity.getDespla() != null){
	    		query.append("AND DESPLA LIKE '%"+entity.getDespla()+"%' ");
	    	}
	    	else{
	    		query.append("AND DESPLA LIKE '%' ");
	    	}
	    	
	    	if(entity.getPlanta() != null){
	    		query.append("AND PLANTA LIKE '%"+entity.getPlanta()+"%' ");
	    	}
	    	else{
	    		query.append("AND (PLANTA LIKE '%' OR PLANTA IS NULL) ");
	    	}
	    	
	    	if(entity.getDirele() != null){
	    		query.append("AND DIRELE LIKE '%"+entity.getDirele()+"%' ");
	    	}
	    	else{
	    		query.append("AND (DIRELE LIKE '%' OR DIRELE IS NULL)");
	    	}	    		    	
	    	
	    	//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<TsccvPlanta>();
	    	ITsccvTipoProductoLogic logicTsccvTipoProducto = new TsccvTipoProductoLogic();
	    	ITsccvAplicacionesLogic logicTsccvAplicaciones = new TsccvAplicacionesLogic();            
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TsccvPlanta plan= new TsccvPlanta();
	    		plan.setCodpla((String)reg[0]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO TIPO PRODUCTO YA QUE NO ES OBLIGATORIO
	    		if(reg[1] != null){
	    			TsccvTipoProducto tsccvTipoProducto = logicTsccvTipoProducto.getTsccvTipoProducto(((BigDecimal)reg[1]).longValue());
	    			plan.setTsccvTipoProducto(tsccvTipoProducto);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO APLICACION YA QUE NO ES OBLIGATORIO
	    		if(reg[2] != null){
	    			TsccvAplicaciones tsccvAplicaciones = logicTsccvAplicaciones.getTsccvAplicaciones(((BigDecimal)reg[2]).longValue());
	    			plan.setTsccvAplicaciones(tsccvAplicaciones);
	    		}
	    		
	    		plan.setDespla((String)reg[3]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO DESPLA YA QUE NO ES OBLIGATORIO
	    		if(reg[4] != null){
	    			plan.setPlanta((String)reg[4]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO DIRELE YA QUE NO ES OBLIGATORIO
	    		if(reg[5] != null){
	    			plan.setDirele((String)reg[5]);
	    		}
	    		
	            resultado.add(plan);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
    
    //consulta las planta con los datos para saber si esta registrado 
    public Boolean plantaIsRegistrada(String desPla){
    	String query = "SELECT * FROM TSCCV_PLANTA " +
    				   "WHERE DESPLA = '"+desPla+"' ";
    	List resultado = null;
    	try{
    		resultado = EntityManagerHelper.getEntityManager().createNativeQuery(query).getResultList();
	    	
	    	return (resultado != null && resultado.size() != 0) ? true : false;
    	}catch(Exception e){
    		return null;
    	}
    }
    
    /*
    //se agrega este metodo para consultar las plantas por usuario
	@SuppressWarnings("unchecked")
    public List<TsccvPlanta> consultarPlantasPorUsuario(Integer idUser) {
		final String query="SELECT TSCCV_PLANTA.CODPLA, TSCCV_PLANTA.DESPLA, TSCCV_PLANTA.ID_TIPR, TSCCV_PLANTA.ID_APLI, "+ 
								  "TSCCV_PLANTAS_USUARIO.FECHA_CREACION "+ 
						   "FROM TSCCV_PLANTA, TSCCV_PLANTAS_USUARIO "+
						   "WHERE TSCCV_PLANTA.CODPLA = TSCCV_PLANTAS_USUARIO.CODPLA "+ 
								  "AND TSCCV_PLANTAS_USUARIO.ID_USUA = "+idUser+" "+
								  "ORDER BY TSCCV_PLANTAS_USUARIO.CODPLA ASC "; //el final significa que una variable constante.
		
		List registros=null;
    	List<TsccvPlanta> resultado= null;
		
		try{
			//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<TsccvPlanta>();
	    	ITsccvTipoProductoLogic logicTsccvTipoProducto = new TsccvTipoProductoLogic();
	    	ITsccvAplicacionesLogic logicTsccvAplicaciones = new TsccvAplicacionesLogic();            
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TsccvPlanta plan= new TsccvPlanta();
	    		plan.setCodpla((String)reg[0]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO TIPO PRODUCTO YA QUE NO ES OBLIGATORIO
	    		if(reg[1] != null){
	    			TsccvTipoProducto tsccvTipoProducto = logicTsccvTipoProducto.getTsccvTipoProducto(((BigDecimal)reg[1]).longValue());
	    			plan.setTsccvTipoProducto(tsccvTipoProducto);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO APLICACION YA QUE NO ES OBLIGATORIO
	    		if(reg[2] != null){
	    			TsccvAplicaciones tsccvAplicaciones = logicTsccvAplicaciones.getTsccvAplicaciones(((BigDecimal)reg[2]).longValue());
	    			plan.setTsccvAplicaciones(tsccvAplicaciones);
	    		}
	    		
	    		plan.setDespla((String)reg[3]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO DESPLA YA QUE NO ES OBLIGATORIO
	    		if(reg[4] != null){
	    			plan.setPlanta((String)reg[4]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO DIRELE YA QUE NO ES OBLIGATORIO
	    		if(reg[5] != null){
	    			plan.setDirele((String)reg[5]);
	    		}
	    		
	            resultado.add(plan);
	    	}
	    	
	    	return resultado;
		}catch(Exception e){
			return null;
		}
    }*/
    
    @SuppressWarnings("unchecked")
    public List<TsccvPlanta> findByActivoAplicacion(String activo, Long aplicacion) {        
        try {
            final String queryString = "select model from  TsccvPlanta model where model.tsccvAplicaciones.idApli = :idApli";
            Query query = getEntityManager().createQuery(queryString);
            //query.setParameter("activo", activo);
            query.setParameter("idApli", aplicacion);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }
    
    
    public List<TsccvPlanta> findByUsuarioAndAplicacion(Integer usuario, Integer aplicacion, String activoApp){
    	final String query="SELECT TSCCV_PLANTA.CODPLA, TSCCV_PLANTA.DESPLA, TSCCV_PLANTA.ID_TIPR, TSCCV_PLANTA.ID_APLI "+ 
						   "FROM TSCCV_PLANTA, TSCCV_PLANTAS_USUARIO, TSCCV_APLICACIONES "+
						   "WHERE TSCCV_PLANTA.CODPLA = TSCCV_PLANTAS_USUARIO.CODPLA AND TSCCV_APLICACIONES.ID_APLI = TSCCV_PLANTA.ID_APLI "+ 
								"AND TSCCV_PLANTAS_USUARIO.ID_USUA = "+usuario+" AND TSCCV_PLANTA.ID_APLI = "+aplicacion+" " +
								"AND TSCCV_APLICACIONES.ACTIVO = '"+activoApp+"' "+
								" ORDER BY TSCCV_PLANTA.CODPLA ASC ";
    	
    	List registros=null;
    	List<TsccvPlanta> resultado= null;
		
		try{
			//Ejecutamos el query
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return new ArrayList<TsccvPlanta>();
	    	}
	    	
	    	resultado = new ArrayList<TsccvPlanta>();
	    	ITsccvTipoProductoLogic logicTsccvTipoProducto = new TsccvTipoProductoLogic();
	    	ITsccvAplicacionesLogic logicTsccvAplicaciones = new TsccvAplicacionesLogic();            
	    	
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg= (Object[])registros.get(i);
	    		TsccvPlanta plan= new TsccvPlanta();
	    		plan.setCodpla((String)reg[0]);
	    		
	    		plan.setDespla((String)reg[1]);
	    		
	    		//SE AGREGA VALIDACION AL CAMPO TIPO PRODUCTO YA QUE NO ES OBLIGATORIO
	    		if(reg[2] != null){
	    			TsccvTipoProducto tsccvTipoProducto = logicTsccvTipoProducto.getTsccvTipoProducto(((BigDecimal)reg[2]).longValue());
	    			plan.setTsccvTipoProducto(tsccvTipoProducto);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO APLICACION YA QUE NO ES OBLIGATORIO
	    		if(reg[3] != null){
	    			TsccvAplicaciones tsccvAplicaciones = logicTsccvAplicaciones.getTsccvAplicaciones(((BigDecimal)reg[3]).longValue());
	    			plan.setTsccvAplicaciones(tsccvAplicaciones);
	    		}	    		
	    		
	            resultado.add(plan);
	    	}
	    	
	    	return resultado;
		}catch(Exception e){
			return null;
		}
    }
    
    
    //metodo ebcargado de consultar las plantas dependiendo de:
    //si la empresa (cliente) tiene cod_vpe entonces consulta las plantas con tipo de producto corrugado o sacos
    //si la empresa (cliente) tiene cod_mas entonces consulta las plantas con tipo de producto molinos
    @SuppressWarnings("unchecked")
    public List<TsccvPlanta> findByTipProdAndAplicacion(String condicion, Long aplicacion){
        try {            
            final String queryString = "select model from  TsccvPlanta model where model.tsccvAplicaciones.idApli = :idApli and "+condicion; 
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("idApli", aplicacion);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }
}
