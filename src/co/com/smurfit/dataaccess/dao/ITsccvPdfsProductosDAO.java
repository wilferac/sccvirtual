package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductos;
import co.com.smurfit.sccvirtual.entidades.TsccvPdfsProductosId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvPdfsProductosDAO.
 *
*/
public interface ITsccvPdfsProductosDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvPdfsProductos entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvPdfsProductosDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPdfsProductos entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvPdfsProductos entity);

    /**
     * Delete a persistent TsccvPdfsProductos entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvPdfsProductosDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvPdfsProductos entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvPdfsProductos entity);

    /**
     * Persist a previously saved TsccvPdfsProductos entity and return it or a copy of it
     * to the sender. A copy of the TsccvPdfsProductos entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvPdfsProductosDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvPdfsProductos entity to update
     * @return TsccvPdfsProductos the persisted TsccvPdfsProductos entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvPdfsProductos update(TsccvPdfsProductos entity);

    public TsccvPdfsProductos findById(TsccvPdfsProductosId id);

    /**
     * Find all TsccvPdfsProductos entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvPdfsProductos property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPdfsProductos> found by query
     */
    public List<TsccvPdfsProductos> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvPdfsProductos> findByCriteria(String whereCondition);

    public List<TsccvPdfsProductos> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvPdfsProductos> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TsccvPdfsProductos> findById(Object id);

    public List<TsccvPdfsProductos> findById(Object id,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvPdfsProductos entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvPdfsProductos> all TsccvPdfsProductos entities
     */
    public List<TsccvPdfsProductos> findAll(int... rowStartIdxAndCount);

    public List<TsccvPdfsProductos> findPageTsccvPdfsProductos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvPdfsProductos();
}
