package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for SapDetPedDAO.
 *
*/
public interface ISapDetPedDAO {
    /**
     * Perform an initial save of a previously unsaved SapDetPed entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapDetPedDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDetPed entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapDetPed entity);

    /**
     * Delete a persistent SapDetPed entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ISapDetPedDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapDetPed entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapDetPed entity);

    /**
     * Persist a previously saved SapDetPed entity and return it or a copy of it
     * to the sender. A copy of the SapDetPed entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ISapDetPedDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDetPed entity to update
     * @return SapDetPed the persisted SapDetPed entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapDetPed update(SapDetPed entity);

    public SapDetPed findById(SapDetPedId id);

    /**
     * Find all SapDetPed entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapDetPed property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapDetPed> found by query
     */
    public List<SapDetPed> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<SapDetPed> findByCriteria(String whereCondition);

    public List<SapDetPed> findById(Object id);

    public List<SapDetPed> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all SapDetPed entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapDetPed> all SapDetPed entities
     */
    public List<SapDetPed> findAll(int... rowStartIdxAndCount);

    public List<SapDetPed> findPageSapDetPed(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberSapDetPed();
    
    public List<SapDetPed> consultarPedidos(SapDetPed entity, Date fechaInicial, Date fechaFinal, String filtroH, String nombreColumna, boolean ascendente);
}
