package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvEstadisticaAccPed;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvEstadisticaAccPedDAO.
 *
*/
public interface ITsccvEstadisticaAccPedDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvEstadisticaAccPed entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvEstadisticaAccPedDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEstadisticaAccPed entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvEstadisticaAccPed entity);

    /**
     * Delete a persistent TsccvEstadisticaAccPed entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvEstadisticaAccPedDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvEstadisticaAccPed entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvEstadisticaAccPed entity);

    /**
     * Persist a previously saved TsccvEstadisticaAccPed entity and return it or a copy of it
     * to the sender. A copy of the TsccvEstadisticaAccPed entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvEstadisticaAccPedDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvEstadisticaAccPed entity to update
     * @return TsccvEstadisticaAccPed the persisted TsccvEstadisticaAccPed entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvEstadisticaAccPed update(TsccvEstadisticaAccPed entity);

    public TsccvEstadisticaAccPed findById(Long id);

    /**
     * Find all TsccvEstadisticaAccPed entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvEstadisticaAccPed property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvEstadisticaAccPed> found by query
     */
    public List<TsccvEstadisticaAccPed> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvEstadisticaAccPed> findByCriteria(String whereCondition);

    public List<TsccvEstadisticaAccPed> findByAcceso(Object acceso);

    public List<TsccvEstadisticaAccPed> findByAcceso(Object acceso,
        int... rowStartIdxAndCount);

    public List<TsccvEstadisticaAccPed> findByFechaCreacion(
        Object fechaCreacion);

    public List<TsccvEstadisticaAccPed> findByFechaCreacion(
        Object fechaCreacion, int... rowStartIdxAndCount);

    public List<TsccvEstadisticaAccPed> findByIdEsap(Object idEsap);

    public List<TsccvEstadisticaAccPed> findByIdEsap(Object idEsap,
        int... rowStartIdxAndCount);

    public List<TsccvEstadisticaAccPed> findByPedido(Object pedido);

    public List<TsccvEstadisticaAccPed> findByPedido(Object pedido,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvEstadisticaAccPed entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvEstadisticaAccPed> all TsccvEstadisticaAccPed entities
     */
    public List<TsccvEstadisticaAccPed> findAll(int... rowStartIdxAndCount);

    public List<TsccvEstadisticaAccPed> findPageTsccvEstadisticaAccPed(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvEstadisticaAccPed();
}
