package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvAccesosUsuario entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvAccesosUsuario
 */
public class TsccvAccesosUsuarioDAO implements ITsccvAccesosUsuarioDAO {
    // property constants

    //public static final Date  FECHAACCESO = "fechaAcceso";
    public static final String FECHAACCESO = "fechaAcceso";

    //public static final Integer  IDACUS = "idAcus";
    public static final String IDACUS = "idAcus";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TsccvAccesosUsuario entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvAccesosUsuarioDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvAccesosUsuario entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvAccesosUsuario entity) {
        /*EntityManagerHelper.log("saving TsccvAccesosUsuario instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }*/
    }
    
    
    public static void saveAccesoUsuario(TsccvAccesosUsuario entity) {
        EntityManagerHelper.log("saving TsccvAccesosUsuario instance", Level.INFO, null);
        Integer idAcus= null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	idAcus = (Integer) EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_acus) from tsccv_accesos_usuario").getSingleResult();
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro        	
        	idAcus = idAcus != null ? (idAcus + 1) : 1;
        	entity.setIdAcus(idAcus);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }
    

    /**
     * Delete a persistent TsccvAccesosUsuario entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvAccesosUsuarioDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvAccesosUsuario entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvAccesosUsuario entity) {
        EntityManagerHelper.log("deleting TsccvAccesosUsuario instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvAccesosUsuario.class,
                    entity.getIdAcus());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvAccesosUsuario entity and return it or a copy of it
     * to the sender. A copy of the TsccvAccesosUsuario entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvAccesosUsuarioDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvAccesosUsuario entity to update
     * @return TsccvAccesosUsuario the persisted TsccvAccesosUsuario entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvAccesosUsuario update(TsccvAccesosUsuario entity) {
        EntityManagerHelper.log("updating TsccvAccesosUsuario instance",
            Level.INFO, null);

        try {
            TsccvAccesosUsuario result = getEntityManager()
                                             .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvAccesosUsuario findById(Integer id) {
        EntityManagerHelper.log(
            "finding TsccvAccesosUsuario instance with id: " + id, Level.INFO,
            null);

        try {
            TsccvAccesosUsuario instance = getEntityManager()
                                               .find(TsccvAccesosUsuario.class,
                    id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvAccesosUsuario entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvAccesosUsuario property to query
     * @param value
     *            the property value to match
     * @return List< TsccvAccesosUsuario> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvAccesosUsuario> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TsccvAccesosUsuario instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvAccesosUsuario model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvAccesosUsuario entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvAccesosUsuario property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvAccesosUsuario> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvAccesosUsuario> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TsccvAccesosUsuario instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvAccesosUsuario model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvAccesosUsuario> findByFechaAcceso(Object fechaAcceso,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHAACCESO, fechaAcceso, rowStartIdxAndCount);
    }

    public List<TsccvAccesosUsuario> findByFechaAcceso(Object fechaAcceso) {
        return findByProperty(FECHAACCESO, fechaAcceso);
    }

    public List<TsccvAccesosUsuario> findByIdAcus(Object idAcus,
        int... rowStartIdxAndCount) {
        return findByProperty(IDACUS, idAcus, rowStartIdxAndCount);
    }

    public List<TsccvAccesosUsuario> findByIdAcus(Object idAcus) {
        return findByProperty(IDACUS, idAcus);
    }

    /**
     * Find all TsccvAccesosUsuario entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvAccesosUsuario> all TsccvAccesosUsuario entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvAccesosUsuario> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvAccesosUsuario instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvAccesosUsuario model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvAccesosUsuario> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvAccesosUsuario model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvAccesosUsuario> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvAccesosUsuario failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvAccesosUsuario> findPageTsccvAccesosUsuario(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvAccesosUsuario model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvAccesosUsuario model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvAccesosUsuario() {
        try {
            String queryString = "select count(*) from TsccvAccesosUsuario model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
}
