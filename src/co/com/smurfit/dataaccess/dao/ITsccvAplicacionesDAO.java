package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvAplicaciones;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvAplicacionesDAO.
 *
*/
public interface ITsccvAplicacionesDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvAplicaciones entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvAplicacionesDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvAplicaciones entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvAplicaciones entity);

    /**
     * Delete a persistent TsccvAplicaciones entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvAplicacionesDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvAplicaciones entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvAplicaciones entity);

    /**
     * Persist a previously saved TsccvAplicaciones entity and return it or a copy of it
     * to the sender. A copy of the TsccvAplicaciones entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvAplicacionesDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvAplicaciones entity to update
     * @return TsccvAplicaciones the persisted TsccvAplicaciones entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvAplicaciones update(TsccvAplicaciones entity);

    public TsccvAplicaciones findById(Long id);

    /**
     * Find all TsccvAplicaciones entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvAplicaciones property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvAplicaciones> found by query
     */
    public List<TsccvAplicaciones> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvAplicaciones> findByCriteria(String whereCondition);

    public List<TsccvAplicaciones> findByActivo(Object activo);

    public List<TsccvAplicaciones> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TsccvAplicaciones> findByDescripcion(Object descripcion);

    public List<TsccvAplicaciones> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount);

    public List<TsccvAplicaciones> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvAplicaciones> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TsccvAplicaciones> findByIdApli(Object idApli);

    public List<TsccvAplicaciones> findByIdApli(Object idApli,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvAplicaciones entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvAplicaciones> all TsccvAplicaciones entities
     */
    public List<TsccvAplicaciones> findAll(int... rowStartIdxAndCount);

    public List<TsccvAplicaciones> findPageTsccvAplicaciones(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvAplicaciones();
}
