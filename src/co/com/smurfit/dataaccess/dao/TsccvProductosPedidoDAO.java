package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.presentation.businessDelegate.BusinessDelegatorView;
import co.com.smurfit.sccvirtual.control.BvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.IBvpPrecioLogic;
import co.com.smurfit.sccvirtual.control.ISapPrecioLogic;
import co.com.smurfit.sccvirtual.control.ITbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.ITsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.ITsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.ITsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.control.SapPrecioLogic;
import co.com.smurfit.sccvirtual.control.TbmBaseGruposUsuariosLogic;
import co.com.smurfit.sccvirtual.control.TsccvEmpresasLogic;
import co.com.smurfit.sccvirtual.control.TsccvEntregasPedidoLogic;
import co.com.smurfit.sccvirtual.control.TsccvPedidosLogic;
import co.com.smurfit.sccvirtual.control.TsccvPlantaLogic;
import co.com.smurfit.sccvirtual.control.TsccvProductosPedidoLogic;
import co.com.smurfit.sccvirtual.entidades.TbmBaseGruposUsuarios;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvEntregasPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvPedidos;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;


/**
 * A data access object (DAO) providing persistence and search support for
 * TsccvProductosPedido entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TsccvProductosPedido
 */
public class TsccvProductosPedidoDAO implements ITsccvProductosPedidoDAO {
    // property constants

    //public static final String  ACTIVO = "activo";
    public static final String ACTIVO = "activo";

    //public static final Integer  CANTIDADTOTAL = "cantidadTotal";
    public static final String CANTIDADTOTAL = "cantidadTotal";

    //public static final String  CODCLI = "codCli";
    public static final String CODCLI = "codCli";

    //public static final String  CODSCC = "codScc";
    public static final String CODSCC = "codScc";

    //public static final String  CODPLA = "codpla";
    public static final String CODPLA = "codpla";

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Date  FECHAPRIMERAENTREGA = "fechaPrimeraEntrega";
    public static final String FECHAPRIMERAENTREGA = "fechaPrimeraEntrega";

    //public static final Integer  IDPRPE = "idPrpe";
    public static final String IDPRPE = "idPrpe";

    //public static final String  ORDENCOMPRA = "ordenCompra";
    public static final String ORDENCOMPRA = "ordenCompra";

    //public static final String  PRECIO = "precio";
    public static final String PRECIO = "precio";

    //public static final String  PRECIO = "precio";
    public static final String PEDIDO = "tsccvPedidos.idPedi";    

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TsccvProductosPedido entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvProductosPedidoDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvProductosPedido entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvProductosPedido entity) {
        /*EntityManagerHelper.log("saving TsccvProductosPedido instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }*/
    }

    public static void saveProductosPedido(TsccvProductosPedido entity) {
        EntityManagerHelper.log("saving TsccvProductosPedido instance", Level.INFO, null);
        Integer idPrpe = null;
        try {
        	//Consultamos el id del ultimo registro insertado
        	idPrpe = (Integer) EntityManagerHelper.getEntityManager().createNativeQuery("select max(id_Prpe) from Tsccv_Productos_Pedido").getSingleResult();
        	
        	//Incrementamos el consecutivo en 1 y se los asignamos al nuevo registro
        	idPrpe = idPrpe != null ? (idPrpe + 1) : 1;
        	entity.setIdPrpe(idPrpe);
        	EntityManagerHelper.getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }
    
    
    /**
     * Delete a persistent TsccvProductosPedido entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TsccvProductosPedidoDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvProductosPedido entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvProductosPedido entity) {
        EntityManagerHelper.log("deleting TsccvProductosPedido instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TsccvProductosPedido.class,
                    entity.getIdPrpe());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TsccvProductosPedido entity and return it or a copy of it
     * to the sender. A copy of the TsccvProductosPedido entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TsccvProductosPedidoDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvProductosPedido entity to update
     * @return TsccvProductosPedido the persisted TsccvProductosPedido entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvProductosPedido update(TsccvProductosPedido entity) {
        EntityManagerHelper.log("updating TsccvProductosPedido instance",
            Level.INFO, null);

        try {
        	((Session)getEntityManager().getDelegate()).update(entity);

        	//TsccvProductosPedido result = getEntityManager()
            //                                  .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return entity;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TsccvProductosPedido findById(Integer id) {
        EntityManagerHelper.log(
            "finding TsccvProductosPedido instance with id: " + id, Level.INFO,
            null);

        try {
            TsccvProductosPedido instance = getEntityManager()
                                                .find(TsccvProductosPedido.class,
                    id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TsccvProductosPedido entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TsccvProductosPedido property to query
     * @param value
     *            the property value to match
     * @return List< TsccvProductosPedido> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvProductosPedido> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TsccvProductosPedido instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TsccvProductosPedido model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TsccvProductosPedido entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvProductosPedido property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TsccvProductosPedido> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TsccvProductosPedido> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TsccvProductosPedido instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TsccvProductosPedido model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvProductosPedido> findByActivo(Object activo,
        int... rowStartIdxAndCount) {
        return findByProperty(ACTIVO, activo, rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByActivo(Object activo) {
        return findByProperty(ACTIVO, activo);
    }

    public List<TsccvProductosPedido> findByCantidadTotal(
        Object cantidadTotal, int... rowStartIdxAndCount) {
        return findByProperty(CANTIDADTOTAL, cantidadTotal, rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByCantidadTotal(Object cantidadTotal) {
        return findByProperty(CANTIDADTOTAL, cantidadTotal);
    }

    public List<TsccvProductosPedido> findByCodCli(Object codCli,
        int... rowStartIdxAndCount) {
        return findByProperty(CODCLI, codCli, rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByCodCli(Object codCli) {
        return findByProperty(CODCLI, codCli);
    }

    public List<TsccvProductosPedido> findByCodScc(Object codScc,
        int... rowStartIdxAndCount) {
        return findByProperty(CODSCC, codScc, rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByCodScc(Object codScc) {
        return findByProperty(CODSCC, codScc);
    }

    public List<TsccvProductosPedido> findByCodpla(Object codpla,
        int... rowStartIdxAndCount) {
        return findByProperty(CODPLA, codpla, rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByCodpla(Object codpla) {
        return findByProperty(CODPLA, codpla);
    }

    public List<TsccvProductosPedido> findByFechaCreacion(
        Object fechaCreacion, int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TsccvProductosPedido> findByFechaPrimeraEntrega(
        Object fechaPrimeraEntrega, int... rowStartIdxAndCount) {
        return findByProperty(FECHAPRIMERAENTREGA, fechaPrimeraEntrega,
            rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByFechaPrimeraEntrega(
        Object fechaPrimeraEntrega) {
        return findByProperty(FECHAPRIMERAENTREGA, fechaPrimeraEntrega);
    }

    public List<TsccvProductosPedido> findByIdPrpe(Object idPrpe,
        int... rowStartIdxAndCount) {
        return findByProperty(IDPRPE, idPrpe, rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByIdPrpe(Object idPrpe) {
        return findByProperty(IDPRPE, idPrpe);
    }

    public List<TsccvProductosPedido> findByOrdenCompra(Object ordenCompra,
        int... rowStartIdxAndCount) {
        return findByProperty(ORDENCOMPRA, ordenCompra, rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByOrdenCompra(Object ordenCompra) {
        return findByProperty(ORDENCOMPRA, ordenCompra);
    }

    public List<TsccvProductosPedido> findByPrecio(Object precio,
        int... rowStartIdxAndCount) {
        return findByProperty(PRECIO, precio, rowStartIdxAndCount);
    }

    public List<TsccvProductosPedido> findByPrecio(Object precio) {
        return findByProperty(PRECIO, precio);
    }

    /**
     * Find all TsccvProductosPedido entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvProductosPedido> all TsccvProductosPedido entities
     */
    @SuppressWarnings("unchecked")
    public List<TsccvProductosPedido> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TsccvProductosPedido instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TsccvProductosPedido model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvProductosPedido> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TsccvProductosPedido model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TsccvProductosPedido> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TsccvProductosPedido failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TsccvProductosPedido> findPageTsccvProductosPedido(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TsccvProductosPedido model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TsccvProductosPedido model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTsccvProductosPedido() {
        try {
            String queryString = "select count(*) from TsccvProductosPedido model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo encargado de consultar el ultimo producto pedido registrado
    public Integer consultarUltimoProductoPedido(){
    	Integer ultimoIdPed = null;
		String query = "select max(idPrpe) from TsccvProductosPedido model ";

		ultimoIdPed = (Integer)getEntityManager().createQuery(query).getSingleResult();
		
		return ultimoIdPed;
    }       
    
    //Metodo encargado de consultar productos pedido dependiendo de un pedido
    public List<ProductosPedidoVO> consultarProductosPedido(Integer idPedi){
		List<ProductosPedidoVO> resultado = new ArrayList<ProductosPedidoVO>();
		try {
			ITsccvEntregasPedidoLogic tsccvEntregasPedidoLogic = new TsccvEntregasPedidoLogic();
			ITsccvProductosPedidoLogic tsccvProductosPedidoLogic = new TsccvProductosPedidoLogic();
			
			//se consultan los productos ya seleccionados para el pedido
			List<TsccvProductosPedido> listadoProdPedido = tsccvProductosPedidoLogic.findByProperty(TsccvProductosPedidoDAO.PEDIDO, idPedi);
			
			ProductosPedidoVO prodPedVO = null;
			for(TsccvProductosPedido prodPed : listadoProdPedido){
				prodPedVO = new ProductosPedidoVO();
				
				prodPedVO.setTsccvProductosPedido(prodPed);
				
        		//se consultan las entregas del producto pedido para setear la cantidad
        		List<TsccvEntregasPedido> entregasTemp = tsccvEntregasPedidoLogic.findByProperty(TsccvEntregasPedidoDAO.PRODPED, prodPed.getIdPrpe());
				prodPedVO.setNumeroEntregas(entregasTemp.size());
				
				resultado.add(prodPedVO);
			}
			
			return resultado;
			
		}catch (Exception re) {
			//throw re;
			return null;
		}
    }
    
    public Integer calcularCantTotal(Integer idPrpe){
    	Integer cantidadTotal;
    	String query = "SELECT SUM(CANTIDAD) FROM TSCCV_ENTREGAS_PEDIDO WHERE TSCCV_ENTREGAS_PEDIDO.ID_PRPE = "+idPrpe;
    	
    	cantidadTotal = (Integer) EntityManagerHelper.getEntityManager().createNativeQuery(query).getSingleResult();
    	return cantidadTotal != null ? cantidadTotal : 0;
    }
    
    
    public Date getFechaPrimeraEntrega(Integer idPrpe){
    	Date menor;
    	String query = "SELECT MIN(FECHA_ENTREGA) FROM TSCCV_ENTREGAS_PEDIDO WHERE TSCCV_ENTREGAS_PEDIDO.ID_PRPE = "+idPrpe;
    	
    	menor = (Date) EntityManagerHelper.getEntityManager().createNativeQuery(query).getSingleResult();
    	return menor;
    }
}
