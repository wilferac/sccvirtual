package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for BvpDetPedDAO.
 *
*/
public interface IBvpDetPedDAO {
    /**
     * Perform an initial save of a previously unsaved BvpDetPed entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpDetPedDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDetPed entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpDetPed entity);

    /**
     * Delete a persistent BvpDetPed entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IBvpDetPedDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpDetPed entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpDetPed entity);

    /**
     * Persist a previously saved BvpDetPed entity and return it or a copy of it
     * to the sender. A copy of the BvpDetPed entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = IBvpDetPedDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDetPed entity to update
     * @return BvpDetPed the persisted BvpDetPed entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpDetPed update(BvpDetPed entity);

    public BvpDetPed findById(BvpDetPedId id);

    /**
     * Find all BvpDetPed entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpDetPed property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpDetPed> found by query
     */
    public List<BvpDetPed> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<BvpDetPed> findByCriteria(String whereCondition);

    public List<BvpDetPed> findById(Object id);

    public List<BvpDetPed> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all BvpDetPed entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpDetPed> all BvpDetPed entities
     */
    public List<BvpDetPed> findAll(int... rowStartIdxAndCount);

    public List<BvpDetPed> findPageBvpDetPed(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberBvpDetPed();
    
    public List<BvpDetPed> consultarPedidos(BvpDetPed entity, Date fechaInicial, Date fechaFinal, String nombreColumna, boolean ascendente);
}
