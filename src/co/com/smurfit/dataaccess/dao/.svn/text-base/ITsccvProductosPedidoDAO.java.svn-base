package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvProductosPedido;
import co.com.smurfit.sccvirtual.vo.ProductosPedidoVO;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvProductosPedidoDAO.
 *
*/
public interface ITsccvProductosPedidoDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvProductosPedido entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvProductosPedidoDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvProductosPedido entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvProductosPedido entity);

    /**
     * Delete a persistent TsccvProductosPedido entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvProductosPedidoDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvProductosPedido entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvProductosPedido entity);

    /**
     * Persist a previously saved TsccvProductosPedido entity and return it or a copy of it
     * to the sender. A copy of the TsccvProductosPedido entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvProductosPedidoDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvProductosPedido entity to update
     * @return TsccvProductosPedido the persisted TsccvProductosPedido entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvProductosPedido update(TsccvProductosPedido entity);

    public TsccvProductosPedido findById(Integer id);

    /**
     * Find all TsccvProductosPedido entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvProductosPedido property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvProductosPedido> found by query
     */
    public List<TsccvProductosPedido> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByCriteria(String whereCondition);

    public List<TsccvProductosPedido> findByActivo(Object activo);

    public List<TsccvProductosPedido> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByCantidadTotal(Object cantidadTotal);

    public List<TsccvProductosPedido> findByCantidadTotal(
        Object cantidadTotal, int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByCodCli(Object codCli);

    public List<TsccvProductosPedido> findByCodCli(Object codCli,
        int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByCodScc(Object codScc);

    public List<TsccvProductosPedido> findByCodScc(Object codScc,
        int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByCodpla(Object codpla);

    public List<TsccvProductosPedido> findByCodpla(Object codpla,
        int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByFechaCreacion(Object fechaCreacion);

    public List<TsccvProductosPedido> findByFechaCreacion(
        Object fechaCreacion, int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByFechaPrimeraEntrega(
        Object fechaPrimeraEntrega);

    public List<TsccvProductosPedido> findByFechaPrimeraEntrega(
        Object fechaPrimeraEntrega, int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByIdPrpe(Object idPrpe);

    public List<TsccvProductosPedido> findByIdPrpe(Object idPrpe,
        int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByOrdenCompra(Object ordenCompra);

    public List<TsccvProductosPedido> findByOrdenCompra(Object ordenCompra,
        int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findByPrecio(Object precio);

    public List<TsccvProductosPedido> findByPrecio(Object precio,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvProductosPedido entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvProductosPedido> all TsccvProductosPedido entities
     */
    public List<TsccvProductosPedido> findAll(int... rowStartIdxAndCount);

    public List<TsccvProductosPedido> findPageTsccvProductosPedido(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvProductosPedido();
    
    public Integer consultarUltimoProductoPedido();
    
    public List<ProductosPedidoVO> consultarProductosPedido(Integer idPedi);
    
    public Integer calcularCantTotal(Integer idPrpe);
    
    public Date getFechaPrimeraEntrega(Integer idPrpe);
}
