package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;

import java.math.BigDecimal;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * SapDetPed entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.SapDetPed
 */
public class SapDetPedDAO implements ISapDetPedDAO {
    // property constants

    //public static final SapDetPedId  ID = "id";
    public static final String ID = "id";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved SapDetPed entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapDetPedDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDetPed entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(SapDetPed entity) {
        EntityManagerHelper.log("saving SapDetPed instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent SapDetPed entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * SapDetPedDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            SapDetPed entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(SapDetPed entity) {
        EntityManagerHelper.log("deleting SapDetPed instance", Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(SapDetPed.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved SapDetPed entity and return it or a copy of it
     * to the sender. A copy of the SapDetPed entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = SapDetPedDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            SapDetPed entity to update
     * @return SapDetPed the persisted SapDetPed entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public SapDetPed update(SapDetPed entity) {
        EntityManagerHelper.log("updating SapDetPed instance", Level.INFO, null);

        try {
            SapDetPed result = getEntityManager().merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public SapDetPed findById(SapDetPedId id) {
        EntityManagerHelper.log("finding SapDetPed instance with id: " + id,
            Level.INFO, null);

        try {
            SapDetPed instance = getEntityManager().find(SapDetPed.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  SapDetPed entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  SapDetPed property to query
     * @param value
     *            the property value to match
     * @return List< SapDetPed> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapDetPed> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  SapDetPed instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  SapDetPed model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all SapDetPed entities with a specific property value.
     *
     * @param propertyName
     *            the name of the SapDetPed property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<SapDetPed> found by query
     */
    @SuppressWarnings("unchecked")
    public List<SapDetPed> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding SapDetPed instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from SapDetPed model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapDetPed> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<SapDetPed> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all SapDetPed entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<SapDetPed> all SapDetPed entities
     */
    @SuppressWarnings("unchecked")
    public List<SapDetPed> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all SapDetPed instances", Level.INFO,
            null);

        try {
            final String queryString = "select model from SapDetPed model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapDetPed> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from SapDetPed model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<SapDetPed> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in SapDetPed failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<SapDetPed> findPageSapDetPed(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from SapDetPed model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from SapDetPed model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberSapDetPed() {
        try {
            String queryString = "select count(*) from SapDetPed model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo agregado para consultar los pedidos de acuerdo a la informacion ingresada
    public List<SapDetPed> consultarPedidos(SapDetPed entity, Date fechaInicial, Date fechaFinal, String filtroH, String nombreColumna, boolean ascendente){
    	StringBuffer query=new StringBuffer("SELECT DISTINCT ORDCOM, NUMPED, ITMPED, SUBITM, CODPRO, DESPRO, FECSOL, " +
    										"FECDSP, ESTADO, CNTPED, CNTDSP, DESUME, CNTKGS, AROLLO, AHOJAS, LHOJAS "+ 
    										"FROM SAP_DET_PED, SAP_UMEDIDA "+
											"WHERE SAP_UMEDIDA.CODUME = SAP_DET_PED.UNDMED ");
    	List registros=null;
    	List<SapDetPed> resultado= null;
    	try {
	    	//Validamos si los campos vienen con valor o no para armar el query
	    	/*if(entity.getId() != null && entity.getId().getPlanta() != null){
	    		query.append("WHERE PLANTA LIKE '%"+entity.getId().getPlanta()+"%' ");
	    	}
	    	else{
	    		query.append("WHERE (PLANTA LIKE '%' OR PLANTA IS NULL) ");
	    	}*/
	    	
	    	if(entity.getId() != null && entity.getId().getNumped() != null){
	    		query.append("AND LTRIM(RTRIM(NUMPED)) LIKE '%"+entity.getId().getNumped().trim()+"%' ");
	    	}
	    	else{
	    		query.append("AND (NUMPED LIKE '%' OR NUMPED IS NULL) ");
	    	}
	    	
	    	/*if(entity.getId() != null && entity.getId().getItmped() != null){
	    		query.append("AND ITMPED LIKE '%"+entity.getId().getItmped()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ITMPED LIKE '%' OR ITMPED IS NULL) ");
	    	}*/	    		    
	    	
	    	if(entity.getId() != null && entity.getId().getCodcli() != null){
	    		query.append("AND LTRIM(RTRIM(CODCLI)) LIKE '"+entity.getId().getCodcli().trim()+"' ");
	    	}
	    	/*else{
	    		query.append("AND (CODCLI LIKE '%' OR CODCLI IS NULL) ");
	    	}*/
	    	
	    	/*if(entity.getId() != null && entity.getId().getCodscc() != null){
	    		query.append("AND CODSCC LIKE '%"+entity.getId().getCodscc()+"%' ");
	    	}
	    	else{
	    		query.append("AND (CODSCC LIKE '%' OR CODSCC IS NULL) ");
	    	}
	    	*/
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND LTRIM(RTRIM(CODPRO)) LIKE '%"+entity.getId().getCodpro().trim()+"%' ");
	    	}
	    	else{
	    		query.append("AND (CODPRO LIKE '%' OR CODPRO IS NULL) ");
	    	}
	    	
	    	/*
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND DESPRO LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (DESPRO LIKE '%' OR DESPRO IS NULL) ");
	    	}*/
	    	
	    	
	    	
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");	    	
	    	if(fechaInicial != null && !fechaInicial.equals("")){
	    		query.append("AND (FECSOL >= CONVERT(DATETIME, '"+sdf.format(fechaInicial)+"', 104)) ");
	    	}
	    	if(fechaFinal != null && !fechaFinal.equals("")){
	    		query.append("AND (FECSOL <= CONVERT(DATETIME, '"+sdf.format(fechaFinal)+"', 104)) ");
	    	}
	    	//if((fechaInicial == null || fechaInicial.equals("")) && (fechaFinal == null || !fechaFinal.equals(""))){
	    		query.append("AND FECSOL >= CONVERT(DATETIME, DATEADD(MONTH, (-3), GETDATE()), 104) ");
	    	//}
	    	
	    	
	    	
	    	
	    	/*if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND FECDSP LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (FECDSP LIKE '%' OR FECDSP IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND ESTADO LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ESTADO LIKE '%' OR ESTADO IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND UNDMED LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (UNDMED LIKE '%' OR UNDMED IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND CNTPED LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (CNTPED LIKE '%' OR CNTPED IS NULL) ");
	    	}
	    	
	    	if(entity.getId() != null && entity.getId().getCodpro() != null){
	    		query.append("AND CNTDSP LIKE '%"+entity.getId().getCodpro()+"%' ");
	    	}
	    	else{
	    		query.append("AND (CNTDSP LIKE '%' OR CNTDSP IS NULL) ");
	    	}*/
	    	
	    	if(entity.getId() != null && entity.getId().getOrdcom() != null){
	    		query.append("AND LTRIM(RTRIM(ORDCOM)) LIKE '%"+entity.getId().getOrdcom().trim()+"%' ");
	    	}
	    	else{
	    		query.append("AND (ORDCOM LIKE '%' OR ORDCOM IS NULL) ");
	    	}
	    	
	    	if(filtroH != null){
		    	if(filtroH.equals("0")){
		    		query.append("AND ESTADO LIKE '0' ");
		    	}
		    	else if(filtroH.equals("1")){//abierto
		    		query.append("AND ESTADO LIKE '90' ");
		    	}
	    	}
	    	
	    	//se si contiene el id. del nombre de la columna ya que es un NativeQuery
	    	nombreColumna = nombreColumna.indexOf("id.") != -1 ? nombreColumna.replace("id.", "") : nombreColumna;	    	
	    	query.append("ORDER BY "+nombreColumna+" "+(ascendente ? "ASC" : "DESC"));
	    	
	    	//Ejecutamos el query
	    	System.out.println(query.toString());
	    	registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
	    	
	    	if(registros ==  null || registros.size() == 0){
	    		//Si la consulta no trae registros retornamos null
	    		return null;
	    	}
	    	
	    	resultado = new ArrayList<SapDetPed>();           
	    	sdf = new SimpleDateFormat("yyyy.MM.dd");
	    	DecimalFormat df = new DecimalFormat("###,###,###,##0");
            for(int i=0; i<registros.size() ; i++){
	    		Object[] reg = (Object[])registros.get(i);
	    		
	    		SapDetPedId sapDetPedId = new SapDetPedId();
	    		SapDetPed ped = new SapDetPed();
	    		
	    		
	    		/*se valida la fecha del registro con la ingresada
	    		if(reg[6] != null){	    			
	    			Date fehaReg = new Date(((Timestamp)reg[6]).getTime());
	    			
	    			Calendar tresMesesAtras = Calendar.getInstance();
	    			tresMesesAtras.add(Calendar.MONTH, -3);
	    			if(fehaReg.getTime() < tresMesesAtras.getTime().getTime()){//si la fecha del reggistro es menor a tres meses atras
	    				continue;
	    			}
	    			
	    			if(fechaInicial != null){
	    				if(fehaReg.getTime() < fechaInicial.getTime()){//si la fecha del registro es menor a la fecha inicial ingresada
		    				continue;
		    			}
	    			}
	    			
	    			if(fechaFinal != null){//si la fecha del registro es menor a la fecha final ingresada
	    				if(fehaReg.getTime() > fechaFinal.getTime()){
		    				continue;
		    			}
	    			}	    			
	    		}*/
	    		
	    		
	    		
	    		sapDetPedId.setOrdcom((String)reg[0]);
	    		sapDetPedId.setNumped((String)reg[1]);
	    		sapDetPedId.setItmped((String)reg[2]);
	    		sapDetPedId.setSubitm((String)reg[3]);	    		
	    		
	    		//SE AGREGA VALIDACION AL CAMPO CODPRO YA QUE NO ES OBLIGATORIO
	    		if(reg[4] != null){
	    			sapDetPedId.setCodpro((String)reg[4]);
	    			sapDetPedId.setCodcli((String)reg[4]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO DESPRO YA QUE NO ES OBLIGATORIO
	    		if(reg[5] != null){
	    			sapDetPedId.setDespro((String)reg[5]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO FECSOL YA QUE NO ES OBLIGATORIO
	    		if(reg[6] != null){
	    			sapDetPedId.setFecsol(sdf.format((Timestamp)reg[6]));
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO FECDSP YA QUE NO ES OBLIGATORIO
	    		if(reg[7] != null){
	    			sapDetPedId.setFecdsp(sdf.format((Timestamp)reg[7]));
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO ESTADO YA QUE NO ES OBLIGATORIO
	    		if(reg[8] != null){
	    			sapDetPedId.setEstado((String)reg[8]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO CNTPED YA QUE NO ES OBLIGATORIO
	    		if(reg[9] != null){
	    			sapDetPedId.setCntped(df.format(new Double((String)reg[9])));
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO CNTDSP YA QUE NO ES OBLIGATORIO
	    		if(reg[10] != null){
	    			sapDetPedId.setCntdsp(df.format(new Double((String)reg[10])));
	    		}

	    		//SE AGREGA VALIDACION AL CAMPO UNDMED YA QUE NO ES OBLIGATORIO
	    		if(reg[11] != null){
	    			sapDetPedId.setUndmed((String)reg[11]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO CNTKGS YA QUE NO ES OBLIGATORIO
	    		if(reg[12] != null){
	    			sapDetPedId.setCntkgs((String)reg[12]);
	    		}
	    		
	    		//SE AGREGA VALIDACION AL CAMPO ARROLLO YA QUE NO ES OBLIGATORIO
	    		if(reg[13] != null){
	    			sapDetPedId.setArollo(df.format(new Double((String)reg[13])));
	    		}	    			    		
	    		
	    		//SE AGREGA VALIDACION AL CAMPO AHOJAS YA QUE NO ES OBLIGATORIO
	    		if(reg[14] != null){
	    			sapDetPedId.setAhojas((String)reg[14]);
	    		}

	    		//SE AGREGA VALIDACION AL CAMPO AHOJAS YA QUE NO ES OBLIGATORIO
	    		if(reg[15] != null){
	    			sapDetPedId.setLhojas((String)reg[15]);
	    		}
	    			    		
	    		ped.setId(sapDetPedId);
	    		
	            resultado.add(ped);
	    	}
	    	
	    	return resultado;
	    	
    	}catch (Exception re) {
    		//throw re;
    		return null;
        }
    }
}
