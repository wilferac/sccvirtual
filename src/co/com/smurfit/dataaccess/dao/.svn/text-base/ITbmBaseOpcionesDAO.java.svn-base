package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TbmBaseOpciones;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TbmBaseOpcionesDAO.
 *
*/
public interface ITbmBaseOpcionesDAO {
    /**
     * Perform an initial save of a previously unsaved TbmBaseOpciones entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseOpcionesDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseOpciones entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseOpciones entity);

    /**
     * Delete a persistent TbmBaseOpciones entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseOpcionesDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseOpciones entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseOpciones entity);

    /**
     * Persist a previously saved TbmBaseOpciones entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseOpciones entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITbmBaseOpcionesDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseOpciones entity to update
     * @return TbmBaseOpciones the persisted TbmBaseOpciones entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseOpciones update(TbmBaseOpciones entity);

    public TbmBaseOpciones findById(Long id);

    /**
     * Find all TbmBaseOpciones entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseOpciones property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseOpciones> found by query
     */
    public List<TbmBaseOpciones> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TbmBaseOpciones> findByCriteria(String whereCondition);

    public List<TbmBaseOpciones> findByActivo(Object activo);

    public List<TbmBaseOpciones> findByActivo(Object activo,
        int... rowStartIdxAndCount);

    public List<TbmBaseOpciones> findByDescripcion(Object descripcion);

    public List<TbmBaseOpciones> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount);

    public List<TbmBaseOpciones> findByFechaCreacion(Object fechaCreacion);

    public List<TbmBaseOpciones> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount);

    public List<TbmBaseOpciones> findByIdOpci(Object idOpci);

    public List<TbmBaseOpciones> findByIdOpci(Object idOpci,
        int... rowStartIdxAndCount);

    public List<TbmBaseOpciones> findByImagenIcono(Object imagenIcono);

    public List<TbmBaseOpciones> findByImagenIcono(Object imagenIcono,
        int... rowStartIdxAndCount);

    public List<TbmBaseOpciones> findByNombre(Object nombre);

    public List<TbmBaseOpciones> findByNombre(Object nombre,
        int... rowStartIdxAndCount);

    public List<TbmBaseOpciones> findByUrl(Object url);

    public List<TbmBaseOpciones> findByUrl(Object url,
        int... rowStartIdxAndCount);

    /**
     * Find all TbmBaseOpciones entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseOpciones> all TbmBaseOpciones entities
     */
    public List<TbmBaseOpciones> findAll(int... rowStartIdxAndCount);

    public List<TbmBaseOpciones> findPageTbmBaseOpciones(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTbmBaseOpciones();
    
    public List<TbmBaseOpciones> consultarOpciones(TbmBaseOpciones entity);
    
    public Boolean opcionIsRegistrada(String nombre);
    
    public void inactivarOpcion(Long idOpci);
}
