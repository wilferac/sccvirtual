package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TsccvAccesosUsuario;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TsccvAccesosUsuarioDAO.
 *
*/
public interface ITsccvAccesosUsuarioDAO {
    /**
     * Perform an initial save of a previously unsaved TsccvAccesosUsuario entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvAccesosUsuarioDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvAccesosUsuario entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TsccvAccesosUsuario entity);

    /**
     * Delete a persistent TsccvAccesosUsuario entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITsccvAccesosUsuarioDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TsccvAccesosUsuario entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TsccvAccesosUsuario entity);

    /**
     * Persist a previously saved TsccvAccesosUsuario entity and return it or a copy of it
     * to the sender. A copy of the TsccvAccesosUsuario entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITsccvAccesosUsuarioDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TsccvAccesosUsuario entity to update
     * @return TsccvAccesosUsuario the persisted TsccvAccesosUsuario entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TsccvAccesosUsuario update(TsccvAccesosUsuario entity);

    public TsccvAccesosUsuario findById(Integer id);

    /**
     * Find all TsccvAccesosUsuario entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TsccvAccesosUsuario property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvAccesosUsuario> found by query
     */
    public List<TsccvAccesosUsuario> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TsccvAccesosUsuario> findByCriteria(String whereCondition);

    public List<TsccvAccesosUsuario> findByFechaAcceso(Object fechaAcceso);

    public List<TsccvAccesosUsuario> findByFechaAcceso(Object fechaAcceso,
        int... rowStartIdxAndCount);

    public List<TsccvAccesosUsuario> findByIdAcus(Object idAcus);

    public List<TsccvAccesosUsuario> findByIdAcus(Object idAcus,
        int... rowStartIdxAndCount);

    /**
     * Find all TsccvAccesosUsuario entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TsccvAccesosUsuario> all TsccvAccesosUsuario entities
     */
    public List<TsccvAccesosUsuario> findAll(int... rowStartIdxAndCount);

    public List<TsccvAccesosUsuario> findPageTsccvAccesosUsuario(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTsccvAccesosUsuario();
}
