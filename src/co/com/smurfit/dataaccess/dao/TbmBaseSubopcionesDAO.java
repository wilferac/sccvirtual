package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.TbmBaseSubopciones;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * TbmBaseSubopciones entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.TbmBaseSubopciones
 */
public class TbmBaseSubopcionesDAO implements ITbmBaseSubopcionesDAO {
    // property constants

    //public static final String  ACTIVO = "activo";
    public static final String ACTIVO = "activo";

    //public static final String  DESCRIPCION = "descripcion";
    public static final String DESCRIPCION = "descripcion";

    //public static final Date  FECHACREACION = "fechaCreacion";
    public static final String FECHACREACION = "fechaCreacion";

    //public static final Long  IDSUOP = "idSuop";
    public static final String IDSUOP = "idSuop";

    //public static final String  NOMBRE = "nombre";
    public static final String NOMBRE = "nombre";

    //public static final String  URL = "url";
    public static final String URL = "url";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved TbmBaseSubopciones entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseSubopcionesDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopciones entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseSubopciones entity) {
        EntityManagerHelper.log("saving TbmBaseSubopciones instance",
            Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent TbmBaseSubopciones entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * TbmBaseSubopcionesDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopciones entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseSubopciones entity) {
        EntityManagerHelper.log("deleting TbmBaseSubopciones instance",
            Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(TbmBaseSubopciones.class,
                    entity.getIdSuop());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved TbmBaseSubopciones entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseSubopciones entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = TbmBaseSubopcionesDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseSubopciones entity to update
     * @return TbmBaseSubopciones the persisted TbmBaseSubopciones entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseSubopciones update(TbmBaseSubopciones entity) {
        EntityManagerHelper.log("updating TbmBaseSubopciones instance",
            Level.INFO, null);

        try {
            TbmBaseSubopciones result = getEntityManager()
                                            .merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public TbmBaseSubopciones findById(Long id) {
        EntityManagerHelper.log("finding TbmBaseSubopciones instance with id: " +
            id, Level.INFO, null);

        try {
            TbmBaseSubopciones instance = getEntityManager()
                                              .find(TbmBaseSubopciones.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  TbmBaseSubopciones entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  TbmBaseSubopciones property to query
     * @param value
     *            the property value to match
     * @return List< TbmBaseSubopciones> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseSubopciones> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log(
            "finding  TbmBaseSubopciones instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  TbmBaseSubopciones model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all TbmBaseSubopciones entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseSubopciones property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<TbmBaseSubopciones> found by query
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseSubopciones> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log(
            "finding TbmBaseSubopciones instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseSubopciones model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseSubopciones> findByActivo(Object activo,
        int... rowStartIdxAndCount) {
        return findByProperty(ACTIVO, activo, rowStartIdxAndCount);
    }

    public List<TbmBaseSubopciones> findByActivo(Object activo) {
        return findByProperty(ACTIVO, activo);
    }

    public List<TbmBaseSubopciones> findByDescripcion(Object descripcion,
        int... rowStartIdxAndCount) {
        return findByProperty(DESCRIPCION, descripcion, rowStartIdxAndCount);
    }

    public List<TbmBaseSubopciones> findByDescripcion(Object descripcion) {
        return findByProperty(DESCRIPCION, descripcion);
    }

    public List<TbmBaseSubopciones> findByFechaCreacion(Object fechaCreacion,
        int... rowStartIdxAndCount) {
        return findByProperty(FECHACREACION, fechaCreacion, rowStartIdxAndCount);
    }

    public List<TbmBaseSubopciones> findByFechaCreacion(Object fechaCreacion) {
        return findByProperty(FECHACREACION, fechaCreacion);
    }

    public List<TbmBaseSubopciones> findByIdSuop(Object idSuop,
        int... rowStartIdxAndCount) {
        return findByProperty(IDSUOP, idSuop, rowStartIdxAndCount);
    }

    public List<TbmBaseSubopciones> findByIdSuop(Object idSuop) {
        return findByProperty(IDSUOP, idSuop);
    }

    public List<TbmBaseSubopciones> findByNombre(Object nombre,
        int... rowStartIdxAndCount) {
        return findByProperty(NOMBRE, nombre, rowStartIdxAndCount);
    }

    public List<TbmBaseSubopciones> findByNombre(Object nombre) {
        return findByProperty(NOMBRE, nombre);
    }

    public List<TbmBaseSubopciones> findByUrl(Object url,
        int... rowStartIdxAndCount) {
        return findByProperty(URL, url, rowStartIdxAndCount);
    }

    public List<TbmBaseSubopciones> findByUrl(Object url) {
        return findByProperty(URL, url);
    }

    /**
     * Find all TbmBaseSubopciones entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseSubopciones> all TbmBaseSubopciones entities
     */
    @SuppressWarnings("unchecked")
    public List<TbmBaseSubopciones> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all TbmBaseSubopciones instances",
            Level.INFO, null);

        try {
            final String queryString = "select model from TbmBaseSubopciones model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseSubopciones> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from TbmBaseSubopciones model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<TbmBaseSubopciones> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in TbmBaseSubopciones failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<TbmBaseSubopciones> findPageTbmBaseSubopciones(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from TbmBaseSubopciones model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from TbmBaseSubopciones model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberTbmBaseSubopciones() {
        try {
            String queryString = "select count(*) from TbmBaseSubopciones model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
}
