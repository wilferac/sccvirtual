package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.ClasesDoc;
import co.com.smurfit.sccvirtual.entidades.ClasesDocId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for ClasesDocDAO.
 *
*/
public interface IClasesDocDAO {
    /**
     * Perform an initial save of a previously unsaved ClasesDoc entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IClasesDocDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            ClasesDoc entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(ClasesDoc entity);

    /**
     * Delete a persistent ClasesDoc entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * IClasesDocDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            ClasesDoc entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(ClasesDoc entity);

    /**
     * Persist a previously saved ClasesDoc entity and return it or a copy of it
     * to the sender. A copy of the ClasesDoc entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = IClasesDocDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            ClasesDoc entity to update
     * @return ClasesDoc the persisted ClasesDoc entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public ClasesDoc update(ClasesDoc entity);

    public ClasesDoc findById(ClasesDocId id);

    /**
     * Find all ClasesDoc entities with a specific property value.
     *
     * @param propertyName
     *            the name of the ClasesDoc property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<ClasesDoc> found by query
     */
    public List<ClasesDoc> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<ClasesDoc> findByCriteria(String whereCondition);

    public List<ClasesDoc> findById(Object id);

    public List<ClasesDoc> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all ClasesDoc entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<ClasesDoc> all ClasesDoc entities
     */
    public List<ClasesDoc> findAll(int... rowStartIdxAndCount);

    public List<ClasesDoc> findPageClasesDoc(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberClasesDoc();
}
