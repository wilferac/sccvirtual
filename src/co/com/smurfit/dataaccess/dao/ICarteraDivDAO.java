package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.CarteraDiv;
import co.com.smurfit.sccvirtual.entidades.CarteraDivId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for CarteraDivDAO.
 *
*/
public interface ICarteraDivDAO {
    /**
     * Perform an initial save of a previously unsaved CarteraDiv entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ICarteraDivDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            CarteraDiv entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(CarteraDiv entity);

    /**
     * Delete a persistent CarteraDiv entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ICarteraDivDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            CarteraDiv entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(CarteraDiv entity);

    /**
     * Persist a previously saved CarteraDiv entity and return it or a copy of it
     * to the sender. A copy of the CarteraDiv entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ICarteraDivDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            CarteraDiv entity to update
     * @return CarteraDiv the persisted CarteraDiv entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public CarteraDiv update(CarteraDiv entity);

    public CarteraDiv findById(CarteraDivId id);

    /**
     * Find all CarteraDiv entities with a specific property value.
     *
     * @param propertyName
     *            the name of the CarteraDiv property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<CarteraDiv> found by query
     */
    public List<CarteraDiv> findByProperty(String propertyName, Object value,
        int... rowStartIdxAndCount);

    public List<CarteraDiv> findByCriteria(String whereCondition);

    public List<CarteraDiv> findById(Object id);

    public List<CarteraDiv> findById(Object id, int... rowStartIdxAndCount);

    /**
     * Find all CarteraDiv entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<CarteraDiv> all CarteraDiv entities
     */
    public List<CarteraDiv> findAll(int... rowStartIdxAndCount);

    public List<CarteraDiv> findPageCarteraDiv(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults);

    public Long findTotalNumberCarteraDiv();
}
