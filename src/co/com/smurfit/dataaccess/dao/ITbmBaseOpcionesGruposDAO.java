package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGrupos;
import co.com.smurfit.sccvirtual.entidades.TbmBaseOpcionesGruposId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Interface for TbmBaseOpcionesGruposDAO.
 *
*/
public interface ITbmBaseOpcionesGruposDAO {
    /**
     * Perform an initial save of a previously unsaved TbmBaseOpcionesGrupos entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseOpcionesGruposDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseOpcionesGrupos entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(TbmBaseOpcionesGrupos entity);

    /**
     * Delete a persistent TbmBaseOpcionesGrupos entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * ITbmBaseOpcionesGruposDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            TbmBaseOpcionesGrupos entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(TbmBaseOpcionesGrupos entity);

    /**
     * Persist a previously saved TbmBaseOpcionesGrupos entity and return it or a copy of it
     * to the sender. A copy of the TbmBaseOpcionesGrupos entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = ITbmBaseOpcionesGruposDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            TbmBaseOpcionesGrupos entity to update
     * @return TbmBaseOpcionesGrupos the persisted TbmBaseOpcionesGrupos entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public TbmBaseOpcionesGrupos update(TbmBaseOpcionesGrupos entity);

    public TbmBaseOpcionesGrupos findById(TbmBaseOpcionesGruposId id);

    /**
     * Find all TbmBaseOpcionesGrupos entities with a specific property value.
     *
     * @param propertyName
     *            the name of the TbmBaseOpcionesGrupos property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseOpcionesGrupos> found by query
     */
    public List<TbmBaseOpcionesGrupos> findByProperty(String propertyName,
        Object value, int... rowStartIdxAndCount);

    public List<TbmBaseOpcionesGrupos> findByCriteria(String whereCondition);

    public List<TbmBaseOpcionesGrupos> findByFechaCreacion(Object fechaCreacion);

    public List<TbmBaseOpcionesGrupos> findByFechaCreacion(
        Object fechaCreacion, int... rowStartIdxAndCount);

    public List<TbmBaseOpcionesGrupos> findById(Object id);

    public List<TbmBaseOpcionesGrupos> findById(Object id,
        int... rowStartIdxAndCount);

    /**
     * Find all TbmBaseOpcionesGrupos entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<TbmBaseOpcionesGrupos> all TbmBaseOpcionesGrupos entities
     */
    public List<TbmBaseOpcionesGrupos> findAll(int... rowStartIdxAndCount);

    public List<TbmBaseOpcionesGrupos> findPageTbmBaseOpcionesGrupos(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults);

    public Long findTotalNumberTbmBaseOpcionesGrupos();
    
    public Object consultarUltimoOrden(Long idGrup);
    
    public void cambiarOrden(TbmBaseOpcionesGrupos tbmBaseOpcionesGrupos, String orden);
    
    public void restablecerPosiciones(Long posAcual, Long idGrup);
}
