var formato="yyyy-MM-dd";
var periodo="yyyy-MM";
function validarEntero(obj,valor){ 
      //intento convertir a entero. 
     //si era un entero no le afecta, si no lo era lo intenta convertir 
     valor = parseInt(valor);
	
      //Compruebo si es un valor num�rico 
      if (isNaN(valor)) { 
            //entonces (no es numero) devuelvo el valor cadena vacia 
            
            obj.value=""; 
      }else{ 
            //En caso contrario (Si era un n�mero) devuelvo el valor 
            obj.value=valor;
      } 
} 

function validarDouble(obj,valor,enteros,decimales){ 
      //intento convertir a entero. 
     //si era un entero no le afecta, si no lo era lo intenta convertir 
     valor= stripNonNumeric(valor);
     //valor = parseFloat(valor);
     
     //Compruebo si es un valor num�rico 
      if (isNaN(valor)) { 
            //entonces (no es numero) devuelvo el valor cadena vacia 
            
            obj.value=""; 
      }else{ 
            //En caso contrario (Si era un n�mero) devuelvo el valor 
            obj.value=valor;
      } 
} 

// This function removes non-numeric characters
function stripNonNumeric( str ){
  str += '';
  var rgx = /^\d|\.|-$/;
  var out = '';
  for( var i = 0; i < str.length; i++ ){
    if( rgx.test( str.charAt(i) ) ){
      if( !( ( str.charAt(i) == '.' && out.indexOf( '.' ) != -1 ) ||
             ( str.charAt(i) == '-' && out.length != 0 ) ) ){
        out += str.charAt(i);
      }
    }
  }
  return out;
}


function ValidaForma() { //v4.0
	//diegomez@jvsoftware.com
  clearFormSpace();
  transformToUpperCase();//transforma los campos a may�sculas,jmduran@jvsoftware.com
  //isNumericIntPositivo(str);
        var i, p,r, q, nm, test, num, min, max, dateFormat, errors = '', args = ValidaForma.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = BuscarObjeto(args[i]);                        
            if (val) {
				var valTemp = val;
                nm=args[i+1];//nm = fncNombresCampos(val.name);
                if ((val = val.value) != "") {
					val = Trim(val);
					if (val.length == 0)
						errors += '- ' + nm + ' es requerido.\n';
					

          if (test.indexOf('isEmail') != -1) {
                var patron=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
     			      var bandera=patron.test(val);
		    	      if (bandera==false){
                        	errors += '- ' + nm + ' debe contener una direcci�n de email v�lida.\n';
			     }
          } 
          else if (test.indexOf('isDate') != -1) 
          {
          	var bandera=isDate(val);
		    	  if (bandera==false){
                        	errors += '- ' + nm + ' debe ser una fecha v\u00E1lida con formato: yyyy-mm-dd \n';
		     	   }
          }else if(test.indexOf('isComboLleno') != -1)
          {
             if(!isComboFull(val))
                 errors += '- ' + nm + ' Error.\n';
                    	}
            
            
          else if (test.indexOf('isDFormat') != -1) 
          {            
            p = test.indexOf(':');
            dateFormat = test.substring(p + 1);
          	var bandera = isDateFormat(val,dateFormat);
		    	  if (bandera == false){
                 errors += '- ' + nm + ' no tiene un formato v�lido.\n';               
		     	   }
          }
          else if (test.indexOf('isPeriod') != -1) 
          {            
            p = test.indexOf(':');
            if (p<0)//si no se envio el formato del periodo
              dateFormat=periodo;
            else
             dateFormat = test.substring(p + 1);            
           	var bandera = isDateFormat(val,dateFormat);
		    	  if (bandera == false){
                 errors += '- ' + nm + ' no tiene el formato ' + dateFormat.toUpperCase() + ' .\n';                 
		     	   }
          }
          else if (test.indexOf('isMoney') != -1) //valida si el formato es moneda
          {
          	var bandera=validarMoneda(val);
		    	  if (bandera==false){
                        	errors += '- ' + nm + ' debe ser un formato de moneda v�lido.\n';
		     	   }
          }          
          
          else if (test.indexOf('isLetter') != -1) //JD
          {
          	var bandera = validarSoloLetras(val);
		    	  if (bandera==false){
                        	errors += '- ' + nm + ' debe contener solo letras.\n';
		     	   }
          }
          
					else if (test.indexOf('isCheck') != -1)
					{
  					    var icheck,swcheck = 0;
						for (icheck=0;icheck<valTemp.length;icheck++)
						{
							if(valTemp[icheck].checked == true){
  						       swcheck = 1;break;
							}
						}
						if (swcheck == 0)
						{
							errors += '- ' + nm + ' requiere seleccionar una opci�n.\n';   
						}
					}else if(test.indexOf('isCombo') != -1){
							if(val=="-1")
								errors += '- ' + nm + ' no se seleccion�.\n';
					
					}else if (test.indexOf('isEspecial') != -1 || test.indexOf('isNotNum') != -1)
					{
						if(test.indexOf('isEspecial') != -1)
						{
						 if(!validarCaracteres(val))
					       errors += '- ' + nm + ' contiene caracteres con tildes o caracteres no v�lidos .\n';
						}
            
						if (test.indexOf('isNotNum') != -1)
						{
						  num = parseFloat(val);
                          if (!isNaN(num)) errors += '- ' + nm + ' NO debe contener solo n�meros.\n';
						}
            
							
					}else if (test.indexOf('isPassword') != -1)
						{
							if(!validarClave(val))
					      		errors += '- ' + nm + ' contiene caracteres no v�lidos.\n';	
					}
          else if (test.indexOf('isPeMayorActual') != -1) {
            val = val + "-01";
            var bien=fechaMayorHoy(val);
            if(bien==false) {
              errors += '- ' + nm + ' debe ser menor que la fecha actual.\n';
            }
          }
          else if (test.indexOf('isPeMenorActual') != -1) {
            val = val + "-01";
            var bien=fechaMayorHoy(val);
            if(bien==true) {
              errors += '- ' + nm + ' debe ser mayor que la fecha actual.\n';
            }
          }
          else if (test.indexOf('isFechaMayorHoy') != -1) {
            var bien=fechaMayorHoy(val);
            if(bien==false) {
              errors += '- ' + nm + ' debe ser menor que la fecha actual.\n';
            }
          }
          else if (test.indexOf('isFechaMenorHoy') != -1) {
            var bien=fechaMayorHoy(val);
            if(bien==true) {
              errors += '- ' + nm + ' debe ser mayor que la fecha actual.\n';
            }
          }         
                    else if (test.indexOf('isPositivo') != -1)
                      {
                    if(!isNumericIntPositivo(val))
                          errors += '- ' + nm + ' debe ser n�mero entero positivo.\n';
                    	}
                      else if (test.indexOf('isInteger') != -1)
                      {
                    if(!validarEnteros(val))
                          errors += '- ' + nm + ' debe contener n�meros enteros.\n';
                    	}
                      else if (test.indexOf('isPositive') != -1)
                      {
                    if(!isNumericPositivo(val))
                          errors += '- ' + nm + ' debe contener n�meros positivos.\n';
                    	}
                      else 
                          if (test.indexOf('validarDecimales') != -1) // Adicionado por Alejandro para validar consistencia de campos decimales con la base de datos.
                          {
                              p = test.indexOf(':');
                              enteros = test.substring(16, p); // guardamos la cantidad de enteros que permite almacenar la base de datos
                              decimales = test.substring(p + 1); // guardamos la cantidad de decimales que permite almacenar la base de datos
                              if(isNumeric(val))
                              {
                                  punto = val.indexOf('.');
                                  var coma = val.indexOf(',');
                                  if(coma != -1 && punto<0)
                                      errors += '- ' + nm + ' debe separar su parte decimal con punto no con coma.\n';
                                  else
                                      if(punto != -1) // si vienen decimales validamos que traiga la cantidad correcta
                                      {
                                          var parteEntera = val.substring(0, punto); // guardamos la parte entera
                                          var parteDecimal = val.substring(punto + 1); // guardamos la parte decimal
                                          if(parteEntera.length > enteros)
                                          {
                                             errors += '- ' + nm + ' debe contener un m�ximo de ' + enteros + ' digitos enteros y ' + decimales + ' decimales.\n'; 
                                          }
                                      }
                                      else
                                        if (val.length > enteros)
                                          errors += '- ' + nm + ' debe contener un m�ximo de ' + enteros + ' digitos enteros y ' + decimales + ' decimales.\n';
                              }
                              else
                                  errors += '- ' + nm + ' debe ser un n�mero con un m�ximo de ' + enteros + ' digitos enteros y ' + decimales + ' decimales.\n';
                          }
                    else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) errors += '- ' + nm + ' debe contener solo n�meros.\n';
						if (test.indexOf('isMayor') != -1) { 
                        	num = parseFloat(val);
							p = test.indexOf(':');
                        	min = test.substring(p + 1);
                        	if (num <= min)
                           	 errors += '- ' + nm + ' debe contener n�meros mayores a ' + min + '\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num)
                                errors += '- ' + nm + ' debe contener solo n�meros entre ' + min + ' y ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') errors += '- ' + nm + ' es requerido.\n';
            }
        }
        if (errors)
        {
        alert('Se presentaron los siguientes errores:\n' + errors);
        return false;
        }
        else
        {
          return true;
        }
    }
    
    //M�todo que permite Quitar los espacios en blanco a los lados en las formas que tiene campos de tipo texto
//Hecho Por Diego Gomez diegomez@jvsoftware.com
  function clearFormSpace()
  {
	var forma = window.document.forms[0]
	for (var i = 0; i < forma.elements.length; i++) {
	    if (forma.elements[i].type == "text" || forma.elements[i].type == "textArea") {
	        forma.elements[i].value =Trim(forma.elements[i].value);
            }
        }
 }
 
 
 //Funci�n que transforma los datos en May�sculas a aquellos campos que tengan la clase SnowInput, al resto no les hace nada
//Hecho por Jeffer Miller Duran 17-Nov-2004
function transformToUpperCase()
{
 i=0;
 while(document.forms[i]) {
  if (document.forms[i]!=null)
  {
    var forma = window.document.forms[i]
    for (var j = 0; j < forma.elements.length; j++) {
	    if (forma.elements[j].type !=null && (forma.elements[j].type == "text" || forma.elements[j].type.toLowerCase() == "textarea")) {
       if(forma.elements[j].className == 'SnowInput') {
	        forma.elements[j].value = forma.elements[j].value.toUpperCase();          
       }   
      }
    }
  }
  i++;
 }//fin while
} 



/*quita los espacios al lado de una cadena */
function LTrim(str)
{
   var whitespace = new String(" \t\n\r");

   var s = new String(str);

   if (whitespace.indexOf(s.charAt(0)) != -1) {

      var j=0, i = s.length;

      while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
         j++;

      s = s.substring(j, i);
   }
   return s;
}


function RTrim(str)
{
   var whitespace = new String(" \t\n\r");

   var s = new String(str);

   if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
      var i = s.length - 1;      
      while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
         i--;

      s = s.substring(0, i+1);
   }

   return s;
}
/*quita esocios de la cadena a la derecha y a la izquierda*/
function Trim(str)
{
  return RTrim(LTrim(str));
}



/*busca un objeto dentro del documento*/
function BuscarObjeto(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=BuscarObjeto(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

/////////////////////funciones para validar fechas
// ===================================================================
// Author: Matt Kruse <matt@mattkruse.com>
// WWW: http://www.mattkruse.com/
//
// NOTICE: You may use this code for any purpose, commercial or
// private, without any further permission from the author. You may
// remove this notice from your final code if you wish, however it is
// appreciated by the author if at least my web site address is kept.
//
// You may *NOT* re-distribute this code in any way except through its
// use. That means, you can include it in your product, or your web
// site, or any other form where the code is actually being used. You
// may not put the plain javascript up on your site for download or
// include it in your javascript libraries for download. 
// If you wish to share this code with others, please just point them
// to the URL instead.
// Please DO NOT link directly to my .js files from your site. Copy
// the files to your server and use them there. Thank you.
// ===================================================================

// HISTORY
// ------------------------------------------------------------------
// May 17, 2003: Fixed bug in parseDate() for dates <1970
// March 11, 2003: Added parseDate() function
// March 11, 2003: Added "NNN" formatting option. Doesn't match up
//                 perfectly with SimpleDateFormat formats, but 
//                 backwards-compatability was required.

// ------------------------------------------------------------------
// These functions use the same 'format' strings as the 
// java.text.SimpleDateFormat class, with minor exceptions.
// The format string consists of the following abbreviations:
// 
// Field        | Full Form          | Short Form
// -------------+--------------------+-----------------------
// Year         | yyyy (4 digits)    | yy (2 digits), y (2 or 4 digits)
// Month        | MMM (name or abbr.)| MM (2 digits), M (1 or 2 digits)
//              | NNN (abbr.)        |
// Day of Month | dd (2 digits)      | d (1 or 2 digits)
// Day of Week  | EE (name)          | E (abbr)
// Hour (1-12)  | hh (2 digits)      | h (1 or 2 digits)
// Hour (0-23)  | HH (2 digits)      | H (1 or 2 digits)
// Hour (0-11)  | KK (2 digits)      | K (1 or 2 digits)
// Hour (1-24)  | kk (2 digits)      | k (1 or 2 digits)
// Minute       | mm (2 digits)      | m (1 or 2 digits)
// Second       | ss (2 digits)      | s (1 or 2 digits)
// AM/PM        | a                  |
//
// NOTE THE DIFFERENCE BETWEEN MM and mm! Month=MM, not mm!
// Examples:
//  "MMM d, y" matches: January 01, 2000
//                      Dec 1, 1900
//                      Nov 20, 00
//  "M/d/yy"   matches: 01/20/00
//                      9/2/00
//  "MMM dd, yyyy hh:mm:ssa" matches: "January 01, 2000 12:30:45AM"
// ------------------------------------------------------------------

var MONTH_NAMES=new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
var DAY_NAMES=new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat');
function LZ(x) {return(x<0||x>9?"":"0")+x}

// ------------------------------------------------------------------
// isDate ( date_string, format_string )
// Returns true if date string matches format of format string and
// is a valid date. Else returns false.
// It is recommended that you trim whitespace around the value before
// passing it to this function, as whitespace is NOT ignored!
// ------------------------------------------------------------------
function isDate(val) { //si no recibe el formato lo toma de la variable global formato.Nota: Debe inicializarse esta variabl�e a trav�s del metodo setFormat
	
  // Modificado por Alejandro Alvarez para validar que las fechas validas esten unicamente entre el a�o 1900 y 2200.
  var stringDate=val.split("-");
	if(parseInt(stringDate[0])<1700 || parseInt(stringDate[0])>2300)
    return false;
  var date=getDateFromFormat(val,formato);
	if (date==0) { return false; }
	return true;
	}

// -------------------------------------------------------------------
// compareDates(date1,date1format,date2,date2format)
//   Compare two date strings to see which is greater.
//   Returns:
//   1 if date1 is greater than date2
//   0 if date2 is greater than date1 of if they are the same
//  -1 if either of the dates is in an invalid format
// -------------------------------------------------------------------
function compareDates(date1,dateformat1,date2,dateformat2) {
	var d1=getDateFromFormat(date1,dateformat1);
	var d2=getDateFromFormat(date2,dateformat2);
	if (d1==0 || d2==0) {
		return -1;
		}
	else if (d1 > d2) {
		return 1;
		}
	return 0;
	}

// ------------------------------------------------------------------
// formatDate (date_object, format)
// Returns a date in the output format specified.
// The format string uses the same abbreviations as in getDateFromFormat()
// ------------------------------------------------------------------
function formatDate(date,format) {
	format=format+"";
	var result="";
	var i_format=0;
	var c="";
	var token="";
	var y=date.getYear()+"";
	var M=date.getMonth()+1;
	var d=date.getDate();
	var E=date.getDay();
	var H=date.getHours();
	var m=date.getMinutes();
	var s=date.getSeconds();
	var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
	// Convert real date parts into formatted versions
	var value=new Object();
	if (y.length < 4) {y=""+(y-0+1900);}
	value["y"]=""+y;
	value["yyyy"]=y;
	value["yy"]=y.substring(2,4);
	value["M"]=M;
	value["MM"]=LZ(M);
	value["MMM"]=MONTH_NAMES[M-1];
	value["NNN"]=MONTH_NAMES[M+11];
	value["d"]=d;
	value["dd"]=LZ(d);
	value["E"]=DAY_NAMES[E+7];
	value["EE"]=DAY_NAMES[E];
	value["H"]=H;
	value["HH"]=LZ(H);
	if (H==0){value["h"]=12;}
	else if (H>12){value["h"]=H-12;}
	else {value["h"]=H;}
	value["hh"]=LZ(value["h"]);
	if (H>11){value["K"]=H-12;} else {value["K"]=H;}
	value["k"]=H+1;
	value["KK"]=LZ(value["K"]);
	value["kk"]=LZ(value["k"]);
	if (H > 11) { value["a"]="PM"; }
	else { value["a"]="AM"; }
	value["m"]=m;
	value["mm"]=LZ(m);
	value["s"]=s;
	value["ss"]=LZ(s);
	while (i_format < format.length) {
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
			}
		if (value[token] != null) { result=result + value[token]; }
		else { result=result + token; }
		}
	return result;
	}
// ------------------------------------------------------------------
// isDateFormat (date_object, format)
// Retorna true si una fecha corresponde al formato dado
// ------------------------------------------------------------------
function isDateFormat(val,format) {
    var date=getDateFromFormat(val,format);
      if (date==0) { return false; }
    return true;
  }
	
// ------------------------------------------------------------------
// Utility functions for parsing in getDateFromFormat()
// ------------------------------------------------------------------
function _isInteger(val) {
	var digits="1234567890";
	for (var i=0; i < val.length; i++) {
		if (digits.indexOf(val.charAt(i))==-1) { return false; }
		}
	return true;
	}
function _getInt(str,i,minlength,maxlength) {
	for (var x=maxlength; x>=minlength; x--) {
		var token=str.substring(i,i+x);
		if (token.length < minlength) { return null; }
		if (_isInteger(token)) { return token; }
		}
	return null;
	}
	
// ------------------------------------------------------------------
// getDateFromFormat( date_string , format_string )
//
// This function takes a date string and a format string. It matches
// If the date string matches the format string, it returns the 
// getTime() of the date. If it does not match, it returns 0.
// ------------------------------------------------------------------
function getDateFromFormat(val,format) {
	val=val+"";
	format=format+"";
	var i_val=0;
	var i_format=0;
	var c="";
	var token="";
	var token2="";
	var x,y;
	var now=new Date();
	var year=now.getYear();
	var month=now.getMonth()+1;
	var date=1;
	var hh=now.getHours();
	var mm=now.getMinutes();
	var ss=now.getSeconds();
	var ampm="";
	
	while (i_format < format.length) {
		// Get next token from format string
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
			}
		// Extract contents of value based on format token
		if (token=="yyyy" || token=="yy" || token=="y") {
			if (token=="yyyy") { x=4;y=4; }
			if (token=="yy")   { x=2;y=2; }
			if (token=="y")    { x=2;y=4; }
			year=_getInt(val,i_val,x,y);
			if (year==null) { return 0; }
			i_val += year.length;
			if (year.length==2) {
				if (year > 70) { year=1900+(year-0); }
				else { year=2000+(year-0); }
				}
			}
		else if (token=="MMM"||token=="NNN"){
			month=0;
			for (var i=0; i<MONTH_NAMES.length; i++) {
				var month_name=MONTH_NAMES[i];
				if (val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()) {
					if (token=="MMM"||(token=="NNN"&&i>11)) {
						month=i+1;
						if (month>12) { month -= 12; }
						i_val += month_name.length;
						break;
						}
					}
				}
			if ((month < 1)||(month>12)){return 0;}
			}
		else if (token=="EE"||token=="E"){
			for (var i=0; i<DAY_NAMES.length; i++) {
				var day_name=DAY_NAMES[i];
				if (val.substring(i_val,i_val+day_name.length).toLowerCase()==day_name.toLowerCase()) {
					i_val += day_name.length;
					break;
					}
				}
			}
		else if (token=="MM"||token=="M") {
			month=_getInt(val,i_val,token.length,2);
			if(month==null||(month<1)||(month>12)){return 0;}
			i_val+=month.length;}
		else if (token=="dd"||token=="d") {
			date=_getInt(val,i_val,token.length,2);
			if(date==null||(date<1)||(date>31)){return 0;}
			i_val+=date.length;}
		else if (token=="hh"||token=="h") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>12)){return 0;}
			i_val+=hh.length;}
		else if (token=="HH"||token=="H") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>23)){return 0;}
			i_val+=hh.length;}
		else if (token=="KK"||token=="K") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>11)){return 0;}
			i_val+=hh.length;}
		else if (token=="kk"||token=="k") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>24)){return 0;}
			i_val+=hh.length;hh--;}
		else if (token=="mm"||token=="m") {
			mm=_getInt(val,i_val,token.length,2);
			if(mm==null||(mm<0)||(mm>59)){return 0;}
			i_val+=mm.length;}
		else if (token=="ss"||token=="s") {
			ss=_getInt(val,i_val,token.length,2);
			if(ss==null||(ss<0)||(ss>59)){return 0;}
			i_val+=ss.length;}
		else if (token=="a") {
			if (val.substring(i_val,i_val+2).toLowerCase()=="am") {ampm="AM";}
			else if (val.substring(i_val,i_val+2).toLowerCase()=="pm") {ampm="PM";}
			else {return 0;}
			i_val+=2;}
		else {
			if (val.substring(i_val,i_val+token.length)!=token) {return 0;}
			else {i_val+=token.length;}
			}
		}
	// If there are any trailing characters left in the value, it doesn't match
	if (i_val != val.length) { return 0; }
	// Is date valid for month?
	if (month==2) {
		// Check for leap year
		if ( ( (year%4==0)&&(year%100 != 0) ) || (year%400==0) ) { // leap year
			if (date > 29){ return 0; }
			}
		else { if (date > 28) { return 0; } }
		}
	if ((month==4)||(month==6)||(month==9)||(month==11)) {
		if (date > 30) { return 0; }
		}
	// Correct hours value
	if (hh<12 && ampm=="PM") { hh=hh-0+12; }
	else if (hh>11 && ampm=="AM") { hh-=12; }
	var newdate=new Date(year,month-1,date,hh,mm,ss);
	return newdate.getTime();
	}

// ------------------------------------------------------------------
// parseDate( date_string [, prefer_euro_format] )
//
// This function takes a date string and tries to match it to a
// number of possible date formats to get the value. It will try to
// match against the following international formats, in this order:
// y-M-d   MMM d, y   MMM d,y   y-MMM-d   d-MMM-y  MMM d
// M/d/y   M-d-y      M.d.y     MMM-d     M/d      M-d
// d/M/y   d-M-y      d.M.y     d-MMM     d/M      d-M
// A second argument may be passed to instruct the method to search
// for formats like d/M/y (european format) before M/d/y (American).
// Returns a Date object or null if no patterns match.
// ------------------------------------------------------------------
function parseDate(val) {
	var preferEuro=(arguments.length==2)?arguments[1]:false;
	generalFormats=new Array('y-M-d','MMM d, y','MMM d,y','y-MMM-d','d-MMM-y','MMM d');
	monthFirst=new Array('M/d/y','M-d-y','M.d.y','MMM-d','M/d','M-d');
	dateFirst =new Array('d/M/y','d-M-y','d.M.y','d-MMM','d/M','d-M');
	var checkList=new Array('generalFormats',preferEuro?'dateFirst':'monthFirst',preferEuro?'monthFirst':'dateFirst');
	var d=null;
	for (var i=0; i<checkList.length; i++) {
		var l=window[checkList[i]];
		for (var j=0; j<l.length; j++) {
			d=getDateFromFormat(val,l[j]);
			if (d!=0) { return new Date(d); }
			}
		}
	return null;
	}
  
function validaHorMin(hor,min)
{
 if(hor>23 || hor<0 || min>59 || min<0) 
    return false;
 else
     return true;
}

function validarProductoPedidoBvp(){
	var indice = 0;
    
    while(document.getElementById('fmrPrincipal:tablePlanDeEntrega:'+indice+':imgEliminar') != null){		                    	
    	error = ValidaForma('fmrPrincipal:tablePlanDeEntrega:'+indice+':txtOrdenCompra', 'La Orden de Compra', 'R');
    	
    	if(!error){                    		
    		return false;
    	}    	
    	
    	if(document.getElementById('fmrPrincipal:tablePlanDeEntrega:'+indice+':txtCantidadTotal').value == 0){
    		alert('Hay productos sin Cantidad, por favor ingresela o eliminelo para continuar.');
    		return false;
    	}
    	
    	indice++;
    }
    
    return true;
}

function validarProductoPedidoSap(){
	var indice = 0;
    
    while(document.getElementById('fmrPrincipal:tablePlanDeEntrega:'+indice+':imgEliminar') != null){
    	error = ValidaForma('fmrPrincipal:tablePlanDeEntrega:'+indice+':txtOrdenCompra', 'La Orden de Compra', 'R',
    						'fmrPrincipal:tablePlanDeEntrega:'+indice+':txtDiametro', 'El Diametro', 'R',
    						'fmrPrincipal:tablePlanDeEntrega:'+indice+':txtEje', 'El Eje', 'R');
    	
    	if(!error){
    		document.getElementById('fmrPrincipal:tablePlanDeEntrega:'+indice+':txtDiametro').select();
    		return false;
    	}    	
    	
    	if(document.getElementById('fmrPrincipal:tablePlanDeEntrega:'+indice+':txtCantidadTotal').value == 0){
    		alert('Hay productos sin Cantidad, por favor o eliminelo para continuar.');
    		return false;
    	}
    	
    	if(document.getElementById('fmrPrincipal:tablePlanDeEntrega:'+indice+':somUnidadMedida').value == '-'){
    		alert('La Unidad de Medida es requerida.');
    		return false;
    	}
    	
    	indice++;
    }
    
    return true;
}

//esta funcion se encarga de validar los datos de las entregas de las plantas corrugado y sacos
function validarEntregasBvp(){		                    		                    
    var indice = 0;
    var fechaActual = new Date();      
    var fechaEntrega = null;
    var fechaEntregaDiv = null;
    
    while(document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtFechaEntrega') != null){		                    	
    	error = ValidaForma('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtFechaEntrega', 'La Fecha Solicitada', 'RisDate',
    						'fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtCantidad', 'La Cantidad', 'R');
    	if(!error){                    		
    		return false;
    	}    	
    	
    	if(document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':somDirecDespacho').value == '-'){
    		alert('La Direccion de Despacho es requerida.');
    		return false;
    	}
    	
    	indice++;
    }
    
    return true;
}

//esta funcion se encarga de validar los datos de las entregas de la plantas molinos
function validarEntregasSap(){		                    		                    
    var indice = 0;
    while(document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtFechaEntrega') != null){
    	//valida si ingreso los campos
    	if (document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtAnchoRollo').value == "" && 
	    	(document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtAnchoHojas').value=="" || 
	    	document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtLargoHojas').value== "")){
	    	
			alert("Debe ingresar las dimensiones del producto en la columna Rollos o en la columna Hojas.");
			
			document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtFechaEntrega').focus();
			return false;
		}
		
		//verifica que ingrese Ancho Rollo o (Ancho Hojas y Largo Hojas)
		if (document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtAnchoRollo').value != "" && 
	    	(document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtAnchoHojas').value !="" || 
	    	document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtLargoHojas').value != "")){
	    	
			alert("No puede ingresar las dimensiones del producto en la columna Rollos y Hojas simultaneamente.");
			
			document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtFechaEntrega').focus();
			return false;
		}
    		                    	
    	error = ValidaForma('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtFechaEntrega', 'La Fecha Solicitada', 'RisDate',
    						'fmrPrincipal:tableTsccvEntregasPedido:'+indice+':txtCantidad', 'La Cantidad', 'R');
    	if(!error){    		                	
    		return false;
    	}
    	
    	if(document.getElementById('fmrPrincipal:tableTsccvEntregasPedido:'+indice+':somDirecDespacho').value == '-'){
    		alert('La Direccion de Despacho es requerida.');
    		return false;
    	}
    		
    	indice++;
    }
    
    return true; 
}


//funcion para abrir la ayuda de la pagina en la cual se encuentre el usuaio
function ayuda(direccion){

  var dir = direccion.toString();
  var vec = dir.split("/");
  var pg1 = vec[vec.length-1].toString();
  var pg2 = pg1.split(".");
  var hlp = pg2[0].toString();

  if(hlp == "contenido" || hlp == "inicio"){
    alert("Para poder ver la ayuda\ndebe seleccionar alguna opcion del menu");
  }else{
    if(hlp != "principal"){
    	window.open("../html/"+hlp+".html","","top=150, left=180, width=700, height=600, location=no, status=no, toolbar=no, menubar=no, scrollbars=yes, resizable=no, ,titlebar=olocation=0");
    }
    else{
    	window.open("html/"+hlp+".html","","top=150, left=180, width=700, height=600, location=no, status=no, toolbar=no, menubar=no, scrollbars=yes, resizable=no, ,titlebar=olocation=0");
    }
  }
}

var campoInicial='fmrPrincipal:txtNombre';							
function validar(){
    document.getElementById('fmrPrincipal:txtActivo').value = document.getElementById('fmrPrincipal:somActivo').value != -1 ? document.getElementById('fmrPrincipal:somActivo').value : '';
    
    error = ValidaForma('fmrPrincipal:txtActivo', 'El Activo', 'R', 'fmrPrincipal:txtNombre', 'El Nombre', 'R');			                    			                   			                    
    if (error){
    	if(document.getElementById('fmrPrincipal:btnSave').disabled == false)
        	document.getElementById('fmrPrincipal:btnSave').onclick();
        else
        	document.getElementById('fmrPrincipal:btnModify').onclick();
    }
}