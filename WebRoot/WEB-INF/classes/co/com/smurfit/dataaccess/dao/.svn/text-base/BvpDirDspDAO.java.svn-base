package co.com.smurfit.dataaccess.dao;

import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.sccvirtual.entidades.BvpDirDsp;
import co.com.smurfit.sccvirtual.entidades.BvpDirDspId;
import co.com.smurfit.sccvirtual.vo.ProductoVO;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * A data access object (DAO) providing persistence and search support for
 * BvpDirDsp entities. Transaction control of the save(), update() and delete()
 * operations must be handled externally by senders of these methods or must be
 * manually added to each of these methods for data to be persisted to the JPA
 * datastore.
 *
 * @see lidis.BvpDirDsp
 */
public class BvpDirDspDAO implements IBvpDirDspDAO {
    // property constants

    //public static final BvpDirDspId  ID = "id";
    public static final String ID = "id";

    //public static final BvpDirDspId  ID = "id";
    public static final String PLANTA = "id.codpla";

    private EntityManager getEntityManager() {
        return EntityManagerHelper.getEntityManager();
    }

    /**
     * Perform an initial save of a previously unsaved BvpDirDsp entity. All
     * subsequent persist actions of this entity should use the #update()
     * method. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#persist(Object) EntityManager#persist}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpDirDspDAO.save(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDirDsp entity to persist
     * @throws RuntimeException
     *             when the operation fails
     */
    public void save(BvpDirDsp entity) {
        EntityManagerHelper.log("saving BvpDirDsp instance", Level.INFO, null);

        try {
            getEntityManager().persist(entity);
            EntityManagerHelper.log("save successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("save failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Delete a persistent BvpDirDsp entity. This operation must be performed
     * within the a database transaction context for the entity's data to be
     * permanently deleted from the persistence store, i.e., database. This
     * method uses the
     * {@link javax.persistence.EntityManager#remove(Object) EntityManager#delete}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * BvpDirDspDAO.delete(entity);
     * EntityManagerHelper.commit();
     * entity = null;
     * </pre>
     *
     * @param entity
     *            BvpDirDsp entity to delete
     * @throws RuntimeException
     *             when the operation fails
     */
    public void delete(BvpDirDsp entity) {
        EntityManagerHelper.log("deleting BvpDirDsp instance", Level.INFO, null);

        try {
            entity = getEntityManager()
                         .getReference(BvpDirDsp.class, entity.getId());
            getEntityManager().remove(entity);
            EntityManagerHelper.log("delete successful", Level.INFO, null);
        } catch (RuntimeException re) {
            EntityManagerHelper.log("delete failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Persist a previously saved BvpDirDsp entity and return it or a copy of it
     * to the sender. A copy of the BvpDirDsp entity parameter is returned when
     * the JPA persistence mechanism has not previously been tracking the
     * updated entity. This operation must be performed within the a database
     * transaction context for the entity's data to be permanently saved to the
     * persistence store, i.e., database. This method uses the
     * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
     * operation.
     *
     * <pre>
     * EntityManagerHelper.beginTransaction();
     * entity = BvpDirDspDAO.update(entity);
     * EntityManagerHelper.commit();
     * </pre>
     *
     * @param entity
     *            BvpDirDsp entity to update
     * @return BvpDirDsp the persisted BvpDirDsp entity instance, may not be the
     *         same
     * @throws RuntimeException
     *             if the operation fails
     */
    public BvpDirDsp update(BvpDirDsp entity) {
        EntityManagerHelper.log("updating BvpDirDsp instance", Level.INFO, null);

        try {
            BvpDirDsp result = getEntityManager().merge(entity);
            EntityManagerHelper.log("update successful", Level.INFO, null);

            return result;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("update failed", Level.SEVERE, re);
            throw re;
        }
    }

    public BvpDirDsp findById(BvpDirDspId id) {
        EntityManagerHelper.log("finding BvpDirDsp instance with id: " + id,
            Level.INFO, null);

        try {
            BvpDirDsp instance = getEntityManager().find(BvpDirDsp.class, id);

            return instance;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find failed", Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all  BvpDirDsp entities with a specific property value.
     *
     * @param propertyName
     *            the metaData.name of the  BvpDirDsp property to query
     * @param value
     *            the property value to match
     * @return List< BvpDirDsp> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpDirDsp> findByProperty(String propertyName,
        final Object value) {
        EntityManagerHelper.log("finding  BvpDirDsp instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from  BvpDirDsp model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property metaData.name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    /**
     * Find all BvpDirDsp entities with a specific property value.
     *
     * @param propertyName
     *            the name of the BvpDirDsp property to query
     * @param value
     *            the property value to match
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            number of results to return.
     * @return List<BvpDirDsp> found by query
     */
    @SuppressWarnings("unchecked")
    public List<BvpDirDsp> findByProperty(String propertyName,
        final Object value, final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding BvpDirDsp instance with property: " +
            propertyName + ", value: " + value, Level.INFO, null);

        try {
            final String queryString = "select model from BvpDirDsp model where model." +
                propertyName + "= :propertyValue";
            Query query = getEntityManager().createQuery(queryString);
            query.setParameter("propertyValue", value);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find by property name failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpDirDsp> findById(Object id, int... rowStartIdxAndCount) {
        return findByProperty(ID, id, rowStartIdxAndCount);
    }

    public List<BvpDirDsp> findById(Object id) {
        return findByProperty(ID, id);
    }

    /**
     * Find all BvpDirDsp entities.
     *
     * @param rowStartIdxAndCount
     *            Optional int varargs. rowStartIdxAndCount[0] specifies the the
     *            row index in the query result-set to begin collecting the
     *            results. rowStartIdxAndCount[1] specifies the the maximum
     *            count of results to return.
     * @return List<BvpDirDsp> all BvpDirDsp entities
     */
    @SuppressWarnings("unchecked")
    public List<BvpDirDsp> findAll(final int... rowStartIdxAndCount) {
        EntityManagerHelper.log("finding all BvpDirDsp instances", Level.INFO,
            null);

        try {
            final String queryString = "select model from BvpDirDsp model";
            Query query = getEntityManager().createQuery(queryString);

            if ((rowStartIdxAndCount != null) &&
                    (rowStartIdxAndCount.length > 0)) {
                int rowStartIdx = Math.max(0, rowStartIdxAndCount[0]);

                if (rowStartIdx > 0) {
                    query.setFirstResult(rowStartIdx);
                }

                if (rowStartIdxAndCount.length > 1) {
                    int rowCount = Math.max(0, rowStartIdxAndCount[1]);

                    if (rowCount > 0) {
                        query.setMaxResults(rowCount);
                    }
                }
            }

            return query.getResultList();
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find all failed", Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpDirDsp> findByCriteria(String whereCondition) {
        try {
            String where = ((whereCondition == null) ||
                (whereCondition.length() == 0)) ? "" : ("where " +
                whereCondition);

            final String queryString = "select model from BvpDirDsp model " +
                where;

            Query query = getEntityManager().createQuery(queryString);

            List<BvpDirDsp> entitiesList = query.getResultList();

            return entitiesList;
        } catch (RuntimeException re) {
            EntityManagerHelper.log("find By Criteria in BvpDirDsp failed",
                Level.SEVERE, re);
            throw re;
        }
    }

    public List<BvpDirDsp> findPageBvpDirDsp(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults) {
        if ((sortColumnName != null) && (sortColumnName.length() > 0)) {
            try {
                String queryString = "select model from BvpDirDsp model order by model." +
                    sortColumnName + " " + (sortAscending ? "asc" : "desc");

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        } else {
            try {
                String queryString = "select model from BvpDirDsp model";

                return getEntityManager().createQuery(queryString)
                           .setFirstResult(startRow).setMaxResults(maxResults)
                           .getResultList();
            } catch (RuntimeException re) {
                throw re;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public Long findTotalNumberBvpDirDsp() {
        try {
            String queryString = "select count(*) from BvpDirDsp model";

            return (Long) getEntityManager().createQuery(queryString)
                              .getSingleResult();
        } catch (RuntimeException re) {
            throw re;
        }
    }
    
    //metodo encargado de consultar las direcciones de despacho para un cliente y una planta
    public List<BvpDirDsp> consultarDireccionesClientePlanta(String codCli, String codPla){
    	try{
	    	String query = "SELECT CODDIR, DESDIR, DESPRV, NUMFAX " +
	    				   "FROM BVP_DIR_DSP WHERE LTRIM(RTRIM(CODCLI)) = '"+codCli.trim()+"' AND LTRIM(RTRIM(CODPLA)) = '"+codPla.trim()+"' ";
	    	
	    	List registros=null;
			List<BvpDirDsp> resultado= null;
			
			//Ejecutamos el query
			System.out.println(query.toString());
			registros= getEntityManager().createNativeQuery(query.toString()).getResultList();
			
			if(registros ==  null || registros.size() == 0){
				//Si la consulta no trae registros retornamos null
				return new ArrayList<BvpDirDsp>();
			}
			
			resultado = new ArrayList<BvpDirDsp>();
			for(int i=0; i<registros.size(); i++){
				Object[] reg = (Object[])registros.get(i);
				BvpDirDsp bvpDirDsp = new BvpDirDsp();
				BvpDirDspId bvpDirDspId = new BvpDirDspId();
				
				bvpDirDspId.setCodcli(codCli);
				bvpDirDspId.setCodpla(codPla);
				bvpDirDspId.setCoddir((String)reg[0]);
				bvpDirDspId.setDesdir((String)reg[1]);
				bvpDirDspId.setDesprv((String)reg[2]);
				bvpDirDspId.setNumfax((String)reg[3]);
				bvpDirDsp.setId(bvpDirDspId);
				
				resultado.add(bvpDirDsp);
			}
			
			return resultado;
    	}catch(Exception e){
			return null;
		}
    }
}
