package co.com.smurfit.sccvirtual.entidades;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * TbmBaseOpciones entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "TBM_BASE_OPCIONES", schema = "dbo")
public class TbmBaseOpciones implements java.io.Serializable {

	// Fields

	private Long idOpci;
	private String nombre;
	private String url;
	private String imagenIcono;
	private String descripcion;
	private String activo;
	private Date fechaCreacion;
	private Set<TbmBaseOpcionesGrupos> tbmBaseOpcionesGruposes = new HashSet<TbmBaseOpcionesGrupos>(
			0);
	private List<TbmBaseSubopcionesOpc> tbmBaseSubopcionesOpcs = new ArrayList<TbmBaseSubopcionesOpc>(
			0);

	// Constructors

	/** default constructor */
	public TbmBaseOpciones() {
	}

	/** minimal constructor */
	public TbmBaseOpciones(Long idOpci, String nombre, String activo,
			Date fechaCreacion) {
		this.idOpci = idOpci;
		this.nombre = nombre;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
	}

	/** full constructor */
	public TbmBaseOpciones(Long idOpci, String nombre, String url,
			String imagenIcono, String descripcion, String activo,
			Date fechaCreacion,
			Set<TbmBaseOpcionesGrupos> tbmBaseOpcionesGruposes,
			List<TbmBaseSubopcionesOpc> tbmBaseSubopcionesOpcs) {
		this.idOpci = idOpci;
		this.nombre = nombre;
		this.url = url;
		this.imagenIcono = imagenIcono;
		this.descripcion = descripcion;
		this.activo = activo;
		this.fechaCreacion = fechaCreacion;
		this.tbmBaseOpcionesGruposes = tbmBaseOpcionesGruposes;
		this.tbmBaseSubopcionesOpcs = tbmBaseSubopcionesOpcs;
	}

	// Property accessors
	@Id
	@Column(name = "ID_OPCI", unique = true, nullable = false, precision = 2, scale = 0)
	public Long getIdOpci() {
		return this.idOpci;
	}

	public void setIdOpci(Long idOpci) {
		this.idOpci = idOpci;
	}

	@Column(name = "NOMBRE", nullable = false, length = 40)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "URL", length = 60)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "IMAGEN_ICONO", length = 20)
	public String getImagenIcono() {
		return this.imagenIcono;
	}

	public void setImagenIcono(String imagenIcono) {
		this.imagenIcono = imagenIcono;
	}

	@Column(name = "DESCRIPCION", length = 60)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(name = "ACTIVO", nullable = false, length = 1)
	public String getActivo() {
		return this.activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_CREACION", nullable = false, length = 23)
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tbmBaseOpciones")
	public Set<TbmBaseOpcionesGrupos> getTbmBaseOpcionesGruposes() {
		return this.tbmBaseOpcionesGruposes;
	}

	public void setTbmBaseOpcionesGruposes(
			Set<TbmBaseOpcionesGrupos> tbmBaseOpcionesGruposes) {
		this.tbmBaseOpcionesGruposes = tbmBaseOpcionesGruposes;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tbmBaseOpciones")
	public List<TbmBaseSubopcionesOpc> getTbmBaseSubopcionesOpcs() {
		return this.tbmBaseSubopcionesOpcs;
	}

	public void setTbmBaseSubopcionesOpcs(
			List<TbmBaseSubopcionesOpc> tbmBaseSubopcionesOpcs) {
		this.tbmBaseSubopcionesOpcs = tbmBaseSubopcionesOpcs;
	}

}