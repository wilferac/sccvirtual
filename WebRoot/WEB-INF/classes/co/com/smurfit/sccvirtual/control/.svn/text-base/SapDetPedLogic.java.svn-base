package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.BvpDetPed;
import co.com.smurfit.sccvirtual.entidades.BvpDetPedId;
import co.com.smurfit.sccvirtual.entidades.SapDetPed;
import co.com.smurfit.sccvirtual.entidades.SapDetPedId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvPlanta;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


public class SapDetPedLogic implements ISapDetPedLogic {
    public List<SapDetPed> getSapDetPed() throws Exception {
        List<SapDetPed> list = new ArrayList<SapDetPed>();

        try {
            list = JPADaoFactory.getInstance().getSapDetPedDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveSapDetPed(String numped, String itmped, String subitm,
        String codcli, String despro, String cntped, String undmed,
        String fecsol, String fecdsp, String codpro, String estado,
        String ordcom, String cntdsp, String cntkgs, String arollos,
        String ahojas, String lhojas, String arollo) throws Exception {
        SapDetPed entity = null;

        try {
            if (numped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numped");
            }

            if ((numped != null) &&
                    (Utilities.checkWordAndCheckWithlength(numped, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numped");
            }

            if (itmped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmped");
            }

            if ((itmped != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmped, 3) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmped");
            }

            if (subitm == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "subitm");
            }

            if ((subitm != null) &&
                    (Utilities.checkWordAndCheckWithlength(subitm, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "subitm");
            }

            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (despro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "despro");
            }

            if ((despro != null) &&
                    (Utilities.checkWordAndCheckWithlength(despro, 64) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despro");
            }

            if (cntped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntped");
            }

            if ((cntped != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntped, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntped");
            }

            if (undmed == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "undmed");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            if (fecsol == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecsol");
            }

            if ((fecsol != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecsol, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecsol");
            }

            if (fecdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecdsp");
            }

            if ((fecdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecdsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecdsp");
            }

            if (codpro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpro");
            }

            if ((codpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpro, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpro");
            }

            if (estado == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "estado");
            }

            if ((estado != null) &&
                    (Utilities.checkWordAndCheckWithlength(estado, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "estado");
            }

            if (ordcom == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "ordcom");
            }

            if ((ordcom != null) &&
                    (Utilities.checkWordAndCheckWithlength(ordcom, 15) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ordcom");
            }

            if (cntdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntdsp");
            }

            if ((cntdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntdsp, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntdsp");
            }

            if (cntkgs == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntkgs");
            }

            if ((cntkgs != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntkgs, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntkgs");
            }

            if (arollos == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "arollos");
            }

            if ((arollos != null) &&
                    (Utilities.checkWordAndCheckWithlength(arollos, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "arollos");
            }

            if (ahojas == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "ahojas");
            }

            if ((ahojas != null) &&
                    (Utilities.checkWordAndCheckWithlength(ahojas, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ahojas");
            }

            if (lhojas == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "lhojas");
            }

            if ((lhojas != null) &&
                    (Utilities.checkWordAndCheckWithlength(lhojas, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "lhojas");
            }

            if (arollo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "arollo");
            }

            if ((arollo != null) &&
                    (Utilities.checkWordAndCheckWithlength(arollo, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "arollo");
            }

            SapDetPedId idClass = new SapDetPedId();
            idClass.setNumped(numped);
            idClass.setItmped(itmped);
            idClass.setSubitm(subitm);
            idClass.setCodcli(codcli);
            idClass.setDespro(despro);
            idClass.setCntped(cntped);
            idClass.setUndmed(undmed);
            idClass.setFecsol(fecsol);
            idClass.setFecdsp(fecdsp);
            idClass.setCodpro(codpro);
            idClass.setEstado(estado);
            idClass.setOrdcom(ordcom);
            idClass.setCntdsp(cntdsp);
            idClass.setCntkgs(cntkgs);
            idClass.setArollos(arollos);
            idClass.setAhojas(ahojas);
            idClass.setLhojas(lhojas);
            idClass.setArollo(arollo);

            entity = getSapDetPed(idClass);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }

            entity = new SapDetPed();

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getSapDetPedDAO().save(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteSapDetPed(String numped, String itmped, String subitm,
        String codcli, String despro, String cntped, String undmed,
        String fecsol, String fecdsp, String codpro, String estado,
        String ordcom, String cntdsp, String cntkgs, String arollos,
        String ahojas, String lhojas, String arollo) throws Exception {
        SapDetPed entity = null;

        if (numped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (itmped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (subitm == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (codcli == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (despro == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (cntped == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (undmed == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (fecsol == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (fecdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (codpro == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (estado == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (ordcom == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (cntdsp == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (cntkgs == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (arollos == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (ahojas == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (lhojas == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (arollo == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        SapDetPedId idClass = new SapDetPedId();
        idClass.setNumped(numped);
        idClass.setItmped(itmped);
        idClass.setSubitm(subitm);
        idClass.setCodcli(codcli);
        idClass.setDespro(despro);
        idClass.setCntped(cntped);
        idClass.setUndmed(undmed);
        idClass.setFecsol(fecsol);
        idClass.setFecdsp(fecdsp);
        idClass.setCodpro(codpro);
        idClass.setEstado(estado);
        idClass.setOrdcom(ordcom);
        idClass.setCntdsp(cntdsp);
        idClass.setCntkgs(cntkgs);
        idClass.setArollos(arollos);
        idClass.setAhojas(ahojas);
        idClass.setLhojas(lhojas);
        idClass.setArollo(arollo);

        entity = getSapDetPed(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getSapDetPedDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateSapDetPed(String numped, String itmped, String subitm,
        String codcli, String despro, String cntped, String undmed,
        String fecsol, String fecdsp, String codpro, String estado,
        String ordcom, String cntdsp, String cntkgs, String arollos,
        String ahojas, String lhojas, String arollo) throws Exception {
        SapDetPed entity = null;

        try {
            if (numped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numped");
            }

            if ((numped != null) &&
                    (Utilities.checkWordAndCheckWithlength(numped, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numped");
            }

            if (itmped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "itmped");
            }

            if ((itmped != null) &&
                    (Utilities.checkWordAndCheckWithlength(itmped, 3) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "itmped");
            }

            if (subitm == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "subitm");
            }

            if ((subitm != null) &&
                    (Utilities.checkWordAndCheckWithlength(subitm, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "subitm");
            }

            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (despro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "despro");
            }

            if ((despro != null) &&
                    (Utilities.checkWordAndCheckWithlength(despro, 64) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "despro");
            }

            if (cntped == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntped");
            }

            if ((cntped != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntped, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntped");
            }

            if (undmed == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "undmed");
            }

            if ((undmed != null) &&
                    (Utilities.checkWordAndCheckWithlength(undmed, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "undmed");
            }

            if (fecsol == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecsol");
            }

            if ((fecsol != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecsol, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecsol");
            }

            if (fecdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecdsp");
            }

            if ((fecdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecdsp, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecdsp");
            }

            if (codpro == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codpro");
            }

            if ((codpro != null) &&
                    (Utilities.checkWordAndCheckWithlength(codpro, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codpro");
            }

            if (estado == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "estado");
            }

            if ((estado != null) &&
                    (Utilities.checkWordAndCheckWithlength(estado, 2) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "estado");
            }

            if (ordcom == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "ordcom");
            }

            if ((ordcom != null) &&
                    (Utilities.checkWordAndCheckWithlength(ordcom, 15) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ordcom");
            }

            if (cntdsp == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntdsp");
            }

            if ((cntdsp != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntdsp, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntdsp");
            }

            if (cntkgs == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "cntkgs");
            }

            if ((cntkgs != null) &&
                    (Utilities.checkWordAndCheckWithlength(cntkgs, 16) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "cntkgs");
            }

            if (arollos == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL +
                    "arollos");
            }

            if ((arollos != null) &&
                    (Utilities.checkWordAndCheckWithlength(arollos, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "arollos");
            }

            if (ahojas == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "ahojas");
            }

            if ((ahojas != null) &&
                    (Utilities.checkWordAndCheckWithlength(ahojas, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "ahojas");
            }

            if (lhojas == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "lhojas");
            }

            if ((lhojas != null) &&
                    (Utilities.checkWordAndCheckWithlength(lhojas, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "lhojas");
            }

            if (arollo == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "arollo");
            }

            if ((arollo != null) &&
                    (Utilities.checkWordAndCheckWithlength(arollo, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "arollo");
            }

            SapDetPedId idClass = new SapDetPedId();
            idClass.setNumped(numped);
            idClass.setItmped(itmped);
            idClass.setSubitm(subitm);
            idClass.setCodcli(codcli);
            idClass.setDespro(despro);
            idClass.setCntped(cntped);
            idClass.setUndmed(undmed);
            idClass.setFecsol(fecsol);
            idClass.setFecdsp(fecdsp);
            idClass.setCodpro(codpro);
            idClass.setEstado(estado);
            idClass.setOrdcom(ordcom);
            idClass.setCntdsp(cntdsp);
            idClass.setCntkgs(cntkgs);
            idClass.setArollos(arollos);
            idClass.setAhojas(ahojas);
            idClass.setLhojas(lhojas);
            idClass.setArollo(arollo);

            entity = getSapDetPed(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getSapDetPedDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public SapDetPed getSapDetPed(SapDetPedId id) throws Exception {
        SapDetPed entity = null;

        try {
            entity = JPADaoFactory.getInstance().getSapDetPedDAO().findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("SapDetPed"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<SapDetPed> findPageSapDetPed(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<SapDetPed> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getSapDetPedDAO()
                                  .findPageSapDetPed(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("SapDetPed"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberSapDetPed() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getSapDetPedDAO()
                                  .findTotalNumberSapDetPed();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("SapDetPed Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<SapDetPed> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<SapDetPed> list = new ArrayList<SapDetPed>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getSapDetPedDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    
    //metodo agregado para consultar los pedidos de acuerdo a la informacion ingresada
    public List<SapDetPed> consultarPedidos(String numped, String ordcom,
    		TsccvEmpresas tsccvEmpresas, Date fechaInicial, Date fechaFinal, String codProdCli, String filtroH, String nombreColumna, boolean ascendente) throws Exception{
    	
    	SapDetPed entity = null;
        List<SapDetPed> registros = null;
        
    	try {    		
    		entity = new SapDetPed();
    		SapDetPedId idClass = new SapDetPedId();
            idClass.setNumped(numped);
            idClass.setCodcli(tsccvEmpresas.getCodMas());//codigo cliente
            idClass.setOrdcom(ordcom);
            idClass.setCodpro(codProdCli);//codigo producto cliente

            entity.setId(idClass);
	    	
    		registros = JPADaoFactory.getInstance().getSapDetPedDAO().consultarPedidos(entity, fechaInicial, fechaFinal, filtroH, 
									    						(nombreColumna != null && !nombreColumna.equals("")) ? nombreColumna : "ordcom", 
																(nombreColumna != null && !nombreColumna.equals("")) ? ascendente : true);
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
        
        return registros;
    }
}
