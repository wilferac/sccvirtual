package co.com.smurfit.sccvirtual.control;

import co.com.smurfit.dataaccess.daoFactory.JPADaoFactory;
import co.com.smurfit.dataaccess.entityManager.EntityManagerHelper;
import co.com.smurfit.exceptions.*;
import co.com.smurfit.sccvirtual.entidades.BvpDetDsp;
import co.com.smurfit.sccvirtual.entidades.CarteraResumen;
import co.com.smurfit.sccvirtual.entidades.CarteraResumenId;
import co.com.smurfit.sccvirtual.entidades.TsccvEmpresas;
import co.com.smurfit.sccvirtual.entidades.TsccvUsuario;
import co.com.smurfit.sccvirtual.properties.ArchivoPropiedades;
import co.com.smurfit.utilities.Utilities;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Vector;


public class CarteraResumenLogic implements ICarteraResumenLogic {
    public List<CarteraResumen> getCarteraResumen() throws Exception {
        List<CarteraResumen> list = new ArrayList<CarteraResumen>();

        try {
            list = JPADaoFactory.getInstance().getCarteraResumenDAO().findAll(0);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionInGetAll());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }

    public void saveCarteraResumen(String codcli, String numfac, String clsdoc,
        String fecfac, String fecven, String dias, String monto, String planta,
        String rango) throws Exception {
        CarteraResumen entity = null;

        try {
            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (numfac == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numfac");
            }

            if ((numfac != null) &&
                    (Utilities.checkWordAndCheckWithlength(numfac, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numfac");
            }

            if (clsdoc == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "clsdoc");
            }

            if ((clsdoc != null) &&
                    (Utilities.checkWordAndCheckWithlength(clsdoc, 4) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "clsdoc");
            }

            if (fecfac == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecfac");
            }

            if ((fecfac != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecfac, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecfac");
            }

            if (fecven == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecven");
            }

            if ((fecven != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecven, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecven");
            }

            if (dias == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "dias");
            }

            if ((dias != null) &&
                    (Utilities.checkWordAndCheckWithlength(dias, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH + "dias");
            }

            if (monto == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "monto");
            }

            if ((monto != null) &&
                    (Utilities.checkWordAndCheckWithlength(monto, 14) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "monto");
            }

            if (planta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "planta");
            }

            if ((planta != null) &&
                    (Utilities.checkWordAndCheckWithlength(planta, 4) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "planta");
            }

            if (rango == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "rango");
            }

            if ((rango != null) &&
                    (Utilities.checkWordAndCheckWithlength(rango, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "rango");
            }

            CarteraResumenId idClass = new CarteraResumenId();
            idClass.setCodcli(codcli);
            idClass.setNumfac(numfac);
            idClass.setClsdoc(clsdoc);
            idClass.setFecfac(fecfac);
            idClass.setFecven(fecven);
            idClass.setDias(dias);
            idClass.setMonto(monto);
            idClass.setPlanta(planta);
            idClass.setRango(rango);

            entity = getCarteraResumen(idClass);

            if (entity != null) {
                throw new Exception(ExceptionMessages.ENTITY_WITHSAMEKEY);
            }

            entity = new CarteraResumen();

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getCarteraResumenDAO().save(entity);
            EntityManagerHelper.commit();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void deleteCarteraResumen(String codcli, String numfac,
        String clsdoc, String fecfac, String fecven, String dias, String monto,
        String planta, String rango) throws Exception {
        CarteraResumen entity = null;

        if (codcli == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (numfac == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (clsdoc == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (fecfac == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (fecven == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (dias == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (monto == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (planta == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        if (rango == null) {
            throw new Exception(ExceptionMessages.VARIABLE_NULL);
        }

        CarteraResumenId idClass = new CarteraResumenId();
        idClass.setCodcli(codcli);
        idClass.setNumfac(numfac);
        idClass.setClsdoc(clsdoc);
        idClass.setFecfac(fecfac);
        idClass.setFecven(fecven);
        idClass.setDias(dias);
        idClass.setMonto(monto);
        idClass.setPlanta(planta);
        idClass.setRango(rango);

        entity = getCarteraResumen(idClass);

        if (entity == null) {
            throw new Exception(ExceptionMessages.ENTITY_NULL);
        }

        try {
            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getCarteraResumenDAO().delete(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public void updateCarteraResumen(String codcli, String numfac,
        String clsdoc, String fecfac, String fecven, String dias, String monto,
        String planta, String rango) throws Exception {
        CarteraResumen entity = null;

        try {
            if (codcli == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "codcli");
            }

            if ((codcli != null) &&
                    (Utilities.checkWordAndCheckWithlength(codcli, 8) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "codcli");
            }

            if (numfac == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "numfac");
            }

            if ((numfac != null) &&
                    (Utilities.checkWordAndCheckWithlength(numfac, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "numfac");
            }

            if (clsdoc == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "clsdoc");
            }

            if ((clsdoc != null) &&
                    (Utilities.checkWordAndCheckWithlength(clsdoc, 4) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "clsdoc");
            }

            if (fecfac == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecfac");
            }

            if ((fecfac != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecfac, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecfac");
            }

            if (fecven == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "fecven");
            }

            if ((fecven != null) &&
                    (Utilities.checkWordAndCheckWithlength(fecven, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "fecven");
            }

            if (dias == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "dias");
            }

            if ((dias != null) &&
                    (Utilities.checkWordAndCheckWithlength(dias, 10) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH + "dias");
            }

            if (monto == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "monto");
            }

            if ((monto != null) &&
                    (Utilities.checkWordAndCheckWithlength(monto, 14) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "monto");
            }

            if (planta == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "planta");
            }

            if ((planta != null) &&
                    (Utilities.checkWordAndCheckWithlength(planta, 4) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "planta");
            }

            if (rango == null) {
                throw new Exception(ExceptionMessages.VARIABLE_NULL + "rango");
            }

            if ((rango != null) &&
                    (Utilities.checkWordAndCheckWithlength(rango, 1) == false)) {
                throw new Exception(ExceptionMessages.VARIABLE_LENGTH +
                    "rango");
            }

            CarteraResumenId idClass = new CarteraResumenId();
            idClass.setCodcli(codcli);
            idClass.setNumfac(numfac);
            idClass.setClsdoc(clsdoc);
            idClass.setFecfac(fecfac);
            idClass.setFecven(fecven);
            idClass.setDias(dias);
            idClass.setMonto(monto);
            idClass.setPlanta(planta);
            idClass.setRango(rango);

            entity = getCarteraResumen(idClass);

            if (entity == null) {
                throw new Exception(ExceptionMessages.ENTITY_NOENTITYTOUPDATE);
            }

            entity.setId(idClass);

            EntityManagerHelper.beginTransaction();
            JPADaoFactory.getInstance().getCarteraResumenDAO().update(entity);
            EntityManagerHelper.commit();

            EntityManagerHelper.closeEntityManager();
        } catch (Exception e) {
            EntityManagerHelper.rollback();
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    public CarteraResumen getCarteraResumen(CarteraResumenId id)
        throws Exception {
        CarteraResumen entity = null;

        try {
            entity = JPADaoFactory.getInstance().getCarteraResumenDAO()
                                  .findById(id);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("CarteraResumen"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public List<CarteraResumen> findPageCarteraResumen(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        List<CarteraResumen> entity = null;

        try {
            entity = JPADaoFactory.getInstance().getCarteraResumenDAO()
                                  .findPageCarteraResumen(sortColumnName,
                    sortAscending, startRow, maxResults);
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("CarteraResumen"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    public Long findTotalNumberCarteraResumen() throws Exception {
        Long entity = null;

        try {
            entity = JPADaoFactory.getInstance().getCarteraResumenDAO()
                                  .findTotalNumberCarteraResumen();
        } catch (Exception e) {
            throw new Exception(ExceptionManager.getInstance()
                                                .exceptionFindingEntity("CarteraResumen Count"));
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return entity;
    }

    /**
    *
    * @param varibles
    *            este arreglo debera tener:
    *
    * [0] = String variable = (String) varibles[i]; representa como se llama la
    * variable en el pojo
    *
    * [1] = Boolean booVariable = (Boolean) varibles[i + 1]; representa si el
    * valor necesita o no ''(comillas simples)usado para campos de tipo string
    *
    * [2] = Object value = varibles[i + 2]; representa el valor que se va a
    * buscar en la BD
    *
    * [3] = String comparator = (String) varibles[i + 3]; representa que tipo
    * de busqueda voy a hacer.., ejemplo: where nombre=william o where nombre<>william,
    * en este campo iria el tipo de comparador que quiero si es = o <>
    *
    * Se itera de 4 en 4..., entonces 4 registros del arreglo representan 1
    * busqueda en un campo, si se ponen mas pues el continuara buscando en lo
    * que se le ingresen en los otros 4
    *
    *
    * @param variablesBetween
    *
    * la diferencia son estas dos posiciones
    *
    * [0] = String variable = (String) varibles[j]; la variable ne la BD que va
    * a ser buscada en un rango
    *
    * [1] = Object value = varibles[j + 1]; valor 1 para buscar en un rango
    *
    * [2] = Object value2 = varibles[j + 2]; valor 2 para buscar en un rango
    * ejempolo: a > 1 and a < 5 --> 1 seria value y 5 seria value2
    *
    * [3] = String comparator1 = (String) varibles[j + 3]; comparador 1
    * ejemplo: a comparator1 1 and a < 5
    *
    * [4] = String comparator2 = (String) varibles[j + 4]; comparador 2
    * ejemplo: a comparador1>1  and a comparador2<5  (el original: a > 1 and a <
    * 5) *
    * @param variablesBetweenDates(en
    *            este caso solo para mysql)
    
    *  [0] = String variable = (String) varibles[k]; el nombre de la variable que hace referencia a
    *            una fecha
    *
    * [1] = Object object1 = varibles[k + 2]; fecha 1 a comparar(deben ser
    * dates)
    *
    * [2] = Object object2 = varibles[k + 3]; fecha 2 a comparar(deben ser
    * dates)
    *
    * esto hace un between entre las dos fechas.
    *
    * @return lista con los objetos que se necesiten
    * @throws Exception
    */
    public List<CarteraResumen> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        List<CarteraResumen> list = new ArrayList<CarteraResumen>();

        String where = new String();
        String tempWhere = new String();

        if (variables != null) {
            for (int i = 0; i < variables.length; i++) {
                String variable = (String) variables[i];
                Boolean booVariable = (Boolean) variables[i + 1];
                Object value = variables[i + 2];
                String comparator = (String) variables[i + 3];

                if (value != null) {
                    if (booVariable.booleanValue()) {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " \'" +
                            value + "\' )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " \'" + value + "\' )");
                    } else {
                        tempWhere = (tempWhere.length() == 0)
                            ? ("(model." + variable + " " + comparator + " " +
                            value + " )")
                            : (tempWhere + " AND (model." + variable + " " +
                            comparator + " " + value + " )");
                    }
                }

                i = i + 3;
            }
        }

        if (variablesBetween != null) {
            for (int j = 0; j < variablesBetween.length; j++) {
                String variable = (String) variablesBetween[j];
                Object value = variablesBetween[j + 1];
                Object value2 = variablesBetween[j + 2];
                String comparator1 = (String) variablesBetween[j + 3];
                String comparator2 = (String) variablesBetween[j + 4];

                if (variable != null) {
                    tempWhere = (tempWhere.length() == 0)
                        ? ("(" + value + " " + comparator1 + " model." +
                        variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )")
                        : (tempWhere + " AND (" + value + " " + comparator1 +
                        " model." + variable + " and model." + variable + " " +
                        comparator2 + " " + value2 + " )");
                }

                j = j + 4;
            }
        }

        if (variablesBetweenDates != null) {
            for (int k = 0; k < variablesBetweenDates.length; k++) {
                String variable = (String) variablesBetweenDates[k];
                Object object1 = variablesBetweenDates[k + 1];
                Object object2 = variablesBetweenDates[k + 2];
                String value = null;
                String value2 = null;

                if (variable != null) {
                    try {
                        Date date1 = (Date) object1;
                        Date date2 = (Date) object2;
                        value = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date1);
                        value2 = Utilities.formatDateWithoutTimeInAStringForBetweenWhere(date2);
                    } catch (Exception e) {
                        list = null;
                        throw e;
                    }

                    tempWhere = (tempWhere.length() == 0)
                        ? ("(model." + variable + " between \'" + value +
                        "\' and \'" + value2 + "\')")
                        : (tempWhere + " AND (model." + variable +
                        " between \'" + value + "\' and \'" + value2 + "\')");
                }

                k = k + 2;
            }
        }

        if (tempWhere.length() == 0) {
            where = null;
        } else {
            where = "(" + tempWhere + ")";
        }

        try {
            list = JPADaoFactory.getInstance().getCarteraResumenDAO()
                                .findByCriteria(where);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return list;
    }
    
    
    //metodo encargado de generar el resumen de la cartera agrupando los montos por rango
    public List<Vector<Object>> consultarCartera(Integer codCli) throws Exception{
    	List registros = null;
    	List<Vector<Object>> resultado = new ArrayList<Vector<Object>>();
    	
    	//Se crea un objeto del archivo propiedades		
        ArchivoPropiedades archivoPropiedades = new ArchivoPropiedades();
        //se consulta e listado con las descripciones de los rangos
    	String descripcionesRangos[] = archivoPropiedades.getProperty("LISTA_RANGO_FACTURAS").split(",");
    	
    	try {
    		ITsccvEmpresasLogic tsccvEmpresasLogic = new TsccvEmpresasLogic();
    		TsccvEmpresas tsccvEmpresas = tsccvEmpresasLogic.getTsccvEmpresas(codCli);
    		
    		registros = JPADaoFactory.getInstance().getCarteraResumenDAO().consultarCartera(tsccvEmpresas.getCodSap()); 
    		
    		for(int i=0; i<descripcionesRangos.length; i++){
    			Vector<Object> resumen = new Vector<Object>();
    			boolean tieneValor = false;
    			
    			//se adicciona la descripcion
    			resumen.add(descripcionesRangos[i]);
    			
    			if(registros != null){
	    			for(int j=0; j<registros.size() ; j++){
		    			Object[] reg = (Object[])registros.get(j);
		    			
		    			if(Integer.parseInt(reg[0]+"") == i && ((BigDecimal)reg[1]).intValue() != 0){
		    				resumen.add(reg[0]);
		    				resumen.add(reg[1]);
		    				tieneValor = true;
		    				
		    				break;
		    			}
		    		}
    			}
    			
    			if(!tieneValor){
    				resumen.add(i);
    				resumen.add(new Double(0));
    			}
    			
    			resultado.add(resumen);
    		}
            
            return resultado;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    
    //Metodo encargado de consultar los detalle factura dependiendo de una propiedad
    public List<CarteraResumen> findByProperty(String propertyName, final Object value) throws Exception{
    	try {
            return JPADaoFactory.getInstance().getCarteraResumenDAO().findByProperty(propertyName, value);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    
    //metodo encrgado de consultar el detalle de la factura seleccionada
    public List<CarteraResumen> consultarDetalleFactura(Integer codCli, String rango) throws Exception {
    	try {
    		ITsccvEmpresasLogic tsccvEmpresasLogic = new TsccvEmpresasLogic();
    		TsccvEmpresas tsccvEmpresas = tsccvEmpresasLogic.getTsccvEmpresas(codCli);
    		
            return JPADaoFactory.getInstance().getCarteraResumenDAO().consultarDetalleFactura(tsccvEmpresas.getCodSap(), rango);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
}
